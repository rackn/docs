#!/usr/bin/env bash

declare -A seen

rm -f versions.json
echo "[" >> versions.json
aws s3 ls s3://rackn-docs/ | grep PRE | awk '{ print $2 }' | awk -F/ '{ print $1}' | sort -r -V > g.out
STABLE=$(head -1 g.out)
while read vers
do
    if [[ "$vers" =~ ^v4\.[0-9]+\.[0-9]+$ ]] ; then
        basevers=${vers%.*}
    else
        basevers=$vers
    fi
    if [[ "${seen[$basevers]}" == "true" ]] ; then
        continue
    fi
    echo "$COMMA{ \"version\": \"$vers\", \"title\": \"$vers\", \"aliases\": [">> versions.json
    if [[ "$STABLE" == "$vers" ]] ; then
            echo " \"stable\" " >> versions.json
    fi
    echo " ] }" >> versions.json
    COMMA=","
    seen[$basevers]="true"
done <<< "$(cat g.out | grep -v stable)"
echo "]" >> versions.json
rm -f g.out

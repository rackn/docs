#!/usr/bin/env bash

set -e

# Clean up
./remove-full.sh
rm -rf drpcli drpjq

VERSION=""
if [[ "$CI_COMMIT_BRANCH" != "" ]] ; then
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%%-*}
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%.*}
  VERSION=$CI_COMMIT_BRANCH
fi
if [[ "$CI_COMMIT_TAG" != "" ]] ; then
  CI_COMMIT_TAG=${CI_COMMIT_TAG%%-*}
  CI_COMMIT_TAG=${CI_COMMIT_TAG%.*}
  VERSION=$CI_COMMIT_TAG
fi
if [[ "$VERSION" == "v4" ]] ; then
  VERSION=tip
fi
if [[ "$VERSION" == "" ]] ; then
  VERSION=tip
fi
echo "Using build for version: $VERSION"

# Get version specific docs
#aws s3 cp s3://rackn-base-docs/$VERSION/ . --recursive
# This is broken.  The other trees didn't do the "right" thing
aws s3 cp s3://rackn-base-docs/tip/ . --recursive >/dev/null
cp -r core/src/developers/contents core/src/operators/deployment
rm -rf core/src/developers/contents

# Get drpcli bootstrap cli
curl -fsSL -o drpcli.bootstrap https://rebar-catalog.s3.us-west-2.amazonaws.com/drpcli/v4.12.0/amd64/linux/drpcli
chmod +x drpcli.bootstrap
./drpcli.bootstrap catalog item download drpcli --version=$VERSION
chmod +x drpcli
ln -s drpcli drpjq
rm -rf drpcli.bootstrap

# Make current cli docs
mkdir -p refs/src/resources/cli
./drpcli document refs/src/resources/cli

# Make current lab docs
mkdir -p core/src/resources/labs/labs
labs_location=core/src/resources/labs
curl -o $labs_location/latest.json https://rebar-catalog.s3.us-west-2.amazonaws.com/labs/latest.json
./drpcli contents unbundle $labs_location/latest.json --format=yaml --dest-dir=$labs_location
for i in `ls $labs_location/labs/*.yaml`
do
        f=$(echo $i | sed 's/.yaml/.md/')
        f=$(basename $f)
        echo "Building doc for $i"
        ./drpcli labs document $i > $labs_location/$f
done

mkdir -p refs/src/resources/objects
mkdir -p core/src/operators/deployment/contents

# Until we get something better.
cp -r save_me/rel_notes refs/src/resources/rel_notes

cp drpcli /usr/bin/drpcli
cp drpcli /usr/bin/jq

rm -rf drpcli drpjq

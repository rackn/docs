#!/usr/bin/env bash

set -e

get_rel_path() {
    input_path="$1"
    # Remove trailing slashes
    trimmed_path="${input_path%/}"

    # Count number of slashes, which indicates depth
    count=$(echo "$trimmed_path" | grep -o '/' | wc -l)

    # Construct the relative path using the depth
    rel_path="../"
    for ((i=1; i<=count; i++)); do
        rel_path="$rel_path../"
    done

    # Print the result
    echo "$rel_path"
}

if [[ "$1" == "" ]] ; then
        echo "Specify a file"
        exit 1
fi

changed_file=$1
target_file=${changed_file/src/docs}
cp "$changed_file" "$target_file"

while read -r line ; do
    ofile=$(dirname "$target_file")
    ofile=${ofile:8}
    dots=$(get_rel_path "$ofile")
    id=${line%%::::*}
    repl=${line##*::::}
    repl=${repl/:/-}
    repl=${repl/REPL/$dots}
    sed -i -r "s:\[.*\]\($id\):$repl:g" "$target_file"
done < link_map.data


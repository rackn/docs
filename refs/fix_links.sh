#!/usr/bin/env bash

set -e

get_rel_path() {
    input_path="$1"
    # Remove trailing slashes
    trimmed_path="${input_path%/}"

    # Count number of slashes, which indicates depth
    count=$(echo "$trimmed_path" | grep -o '/' | wc -l)

    # Construct the relative path using the depth
    rel_path="../"
    for ((i=1; i<=count; i++)); do
        rel_path="$rel_path../"
    done

    # Print the result
    echo "$rel_path"
}

rm -rf docs
cp -r src docs

while read -r line ; do
#    cmdstr=$(echo "$line" | sed -r "s/(.*)::::(.*)/sed -i -r \"s:\1:\2:g\"/g")
    find docs -type f | grep '\.md$' | while read -r file ; do
        ofile=$(dirname "$file")
        ofile=${ofile:8}
        dots=$(get_rel_path "$ofile")
        id=${line%%::::*}
        repl=${line##*::::}
        repl=${repl/:/-}
        repl=${repl/REPL/$dots}
        sed -i -r "s:\[.*\]\($id\):$repl:g" "$file"
    done
done < ../core/link_map.data

while read -r line ; do
#    cmdstr=$(echo "$line" | sed -r "s/(.*)::::(.*)/sed -i -r \"s:\1:\2:g\"/g")
    find docs -type f | grep '\.md$' | while read -r file ; do
        ofile=$(dirname "$file")
        ofile=${ofile:8}
        dots=$(get_rel_path "$ofile")
        id=${line%%::::*}
        repl=${line##*::::}
        repl=${repl/:/-}
        repl=${repl/REPL/$dots}
        sed -i -r "s:\[.*\]\($id\):$repl:g" "$file"
    done
done < ../refs/link_map.data


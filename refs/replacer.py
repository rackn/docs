import logging, re, json, os
import subprocess

log = logging.getLogger('mkdocs')

def parse_link_map(path):
    log.info(f"{os.getcwd()} {os.path.abspath(path)}")
    # parse the link map
    if not os.path.exists(path):
        log.warning(f"Missing {path}")
    else:
        try:
            with open(path, "r") as f:
                link_map = json.loads(f.read())
                log.info(f"Located {len(link_map)} refs in {path}")
                return link_map

        except Exception as e:
            log.error(f"Error reading and parsing {path}: {e}")
    return {}

core = False
link_map_core = parse_link_map("./link_map_core.json")
link_map_refs = parse_link_map("./link_map_refs.json")

def remove_first_char_if_match(s):
    if s.startswith('#'):
        return s[1:]
    return s

def replace_link_lookup(name, lookup, path, top, image = False):
    slimlookup = remove_first_char_if_match(lookup)
    if lookup in link_map_core:
        link = link_map_core[lookup]
        if core:
             file = f'{top}/{link["file"]}'
        else:
             file = f'https://docs.rackn.io/stable/redirect/?ref={slimlookup}'
    elif lookup in link_map_refs:
        link = link_map_refs[lookup]
        if core:
             file = f'https://refs.rackn.io/stable/redirect/?ref={slimlookup}'
        else:
             file = f'{top}/{link["file"]}'
    else:
        log.warning(f"Documentation file '{path}' contains unknown slug: {lookup}")
        return f'[{name}]({lookup})'

    if name == "":
        name = link["name"]

    if image:
        return f'![{name}]({file})'
    return f'[{name}]({file})'


def on_page_markdown(markdown, page, **kwargs):
    path = page.file.src_uri
    dir = os.path.dirname(path)
    up = re.sub(r'[^/]+', '..', dir)

    # replace the linkMap for the redirect.md page
    omarkdown = markdown
    if 'const linkMaps = {};' in markdown:
        markdown = markdown.replace(
            'const linkMaps = {};',
            f'const linkMaps = {"{"}core: {json.dumps(link_map_core)}, refs: {json.dumps(link_map_refs)}{"}"};'
        )

    def replace_image(match):
        lookup = re.sub('#', '-', match.group(2))
        return replace_link_lookup(match.group(1), lookup, path, up, True)

    def replace_link(match):
        return replace_link_lookup(match.group(1), match.group(2), path, up, False)

    def replace_run_command(match):
        result = subprocess.run(match.group(1), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return result.stdout.decode()

    pattern = r'\[([^\]]*)\]\((#[^\)]+)\)'
    markdown = re.sub(pattern, replace_link, markdown)

    pattern = r'!\[([^\]]*)\]\((images#[^\)]+)\)'
    markdown = re.sub(pattern, replace_image, markdown)

    pattern = r'!\[([^\]]*)\]\((built_images#[^\)]+)\)'
    markdown = re.sub(pattern, replace_image, markdown)

    pattern = r'<<<<<(.*)>>>>>'
    markdown = re.sub(pattern, replace_run_command, markdown)

    return markdown


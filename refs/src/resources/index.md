---
title: Resources
tags:
- reference
---

# Resources {#rs_resources}

This section contains release notes, drpcli help and model references.

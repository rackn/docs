---
title: Redirect
---

<h1 id="redirectHeader">Docs Redirector</h1>
<div id="redirectContent"></div>

## What is this?

This page provides a method for content documentation with anchor-based linking to reliably link to official documentation pages without needing to have a local development copy of the docs.

## How to use this

This page can be referenced with the `?ref=doc_slug_id` to create a path like `/redirect/?ref=rs_model_template` or `/redirect/?ref=#rs_model_template`

<a href="?ref=rs_model_template">Here is an example link</a> using the `?ref=rs_model_template` query string.

Images and Built Images can be referenced by using `?ref=images-<filename>` or `?ref=built_images-<filename>`.

## How this works

This page contains a map of "slugs" to doc page links compiled from the documentation during the build process.

It can use to take "slugs" like `rs_model_template` and convert it into its respective doc destination.

The JavaScript on this page reads the `?ref=` query string argument from the URL, checks if it exists in the map of "slugs", then redirects a user, such as yourself, to the next page.

<script>
    const redirectHeader = document.getElementById('redirectHeader');
    const redirectContent = document.getElementById('redirectContent');

    // replacer.py automatically replaces the next line with one that contains the link_map.json
    const linkMaps = {};

    const a = document.createElement('a')

    // html black magic to resolve relative paths
    function resolveUrl(origin, path) {
        path = path.replace(/(index)?\.md#/, '#');

        // location.pathname + '../ transforms /<stable|dev>/redirect/ into /<stable|dev>/
        a.href = origin + location.pathname + '../' + path
        return a.href // magic happens
    }

    // querystring parsing
    const query = {};
    try {
        for (const pair of location.search.replace(/^\?/, '').split('&')) {
            const [key, value] = pair.split('=');
            if (!key || !value) continue;
            query[key] = value;
        }
    } catch (err) {
        console.error('error parsing querystring:', err);
    }

    (() => {
        let newPath, newName;

        if (!('ref' in query)) {
            console.info('skipping redirect due to missing ?ref= in request');
            return;
        }

        // resolve '#ref' and 'ref' to their urls
        let ref = query.ref;
        let tref = '#' + ref;
        for (const key in linkMaps) {
            const linkMap = linkMaps[key];
            // core is on docs.rackn.io
            if (key === 'refs') origin = '';
            // refs and others will be on refs.rackn.io
            else origin = `https://docs.rackn.io`;

            if (ref in linkMap) {
                newPath = resolveUrl(origin, linkMap[ref].file);
                newName = linkMap[ref].name;
            }
            else if (tref in linkMap) {
                newPath = resolveUrl(origin, linkMap[tref].file);
                newName = linkMap[tref].name;
            }
        }


        if (!newPath) {
            console.warn('unknown ref', ref);
            redirectHeader.innerText = `Unknown ref id "${ref}"`;
            return
        }

        // update the redirect header
        redirectHeader.innerText = `Redirecting to ${newName}...`;

        // append "Click here if the automatic redirect fails" to the page
        redirectContent.appendChild(document.createTextNode('Click '));

        const a = document.createElement('a');
        a.href = newPath;
        a.innerText = "here";
        redirectContent.appendChild(a);
        setTimeout(() => a.click(), 1000);

        redirectContent.appendChild(document.createTextNode(' if the automatic redirect fails'));
    })();
</script>

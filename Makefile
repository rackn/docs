GH_TOKEN=$(shell cat ./mkdocs.secret)
PWD=$(shell pwd)
DOCKER=$(shell if which podman >/dev/null 2>/dev/null; then echo "podman"; else echo "docker"; fi)
TAG=$(shell date +v%y%m%d-%H%M%S)
MKDOCS_NAME?=rkndocs-$(TAG)

container:
	$(DOCKER) build --build-arg GH_TOKEN=$(GH_TOKEN) -t rackn/mkdocs-material .

setup:
	tools/get-d2.sh
	curl -X GET -o lato.tgz https://s3.us-west-2.amazonaws.com/get.rebar.digital/doc-fonts.tgz
	mkdir -p core
	cd core ; tar -zxvf ../lato.tgz ; cd ..
	rm ./lato.tgz
	@echo "D2 installed in ~/.local/bin; Add to PATH or move d2 to better place"


import 'colors';
import { createHash } from 'crypto';
import { existsSync, mkdir, mkdirSync, statSync } from 'fs';
import { readFile, unlink, writeFile } from 'fs/promises';
import glob from 'glob';
import path from 'path';

/**
 * Takes a content path that might be in a plugin, returns a path to the plugin/content root or null.
 * @param {string} contentPath Path to a content root
 * @returns {string|null} Path to the content or plugin root, or null
 */
export function resolvePluginRoot(contentPath) {
  /// if the content path is not "content", it's not a plugin
  if (path.basename(contentPath) !== 'content') return contentPath;

  const parentPath = path.resolve(contentPath, '..');
  const parentName = path.basename(parentPath);
  const pluginFile = path.resolve(contentPath, `../${parentName}.go`);
  const metaFile = path.resolve(contentPath, 'meta.yaml');

  if (existsSync(metaFile)) return contentPath;

  // if there is no go file with the same name as the parent, it's not a plugin
  if (!existsSync(pluginFile)) return null;

  // return the path of the parent
  return parentPath;
}

/**
 * Gets the root path of a content folder
 * @param {string} filepath Path to a file in the contents folder
 * @returns {string | null} Path to the root of the contents folder to be used with drpcli
 */
export function getContentRoot(filepath) {
  const basename = path.basename(filepath);
  const isYaml = basename.match(/\.ya?ml$/i);
  const isMeta = basename === '._Documentation.meta';
  const isMetaFile = basename.match(/^meta\.ya?ml$/i);

  // When editing the ._Documentation.meta or meta.yaml,
  // the parent folder is the content root
  if (isMeta || isMetaFile)
    return resolvePluginRoot(path.resolve(filepath, '..'));

  // Non-yaml files are ignored
  if (!isYaml) return null;

  // navigate two directories up out of the content subdirectories
  const expectedRoot = path.resolve(filepath, '../..');

  // if there is no meta.yaml or ._Name.meta two directories up, this is not a content folder
  if (
    !existsSync(path.resolve(expectedRoot, 'meta.yaml')) &&
    !existsSync(path.resolve(expectedRoot, '._Name.meta'))
  )
    return null;

  return resolvePluginRoot(expectedRoot);
}

/**
 * Prune invalid paths from a list of paths, returns valid paths and the pruned paths' reasons
 * @param {string[]} paths List of paths to folders
 * @returns {[paths: string[], pruned: {path: string, reason: string}[]]}
 */
export function prunePaths(paths) {
  const pruned = [];
  paths = paths.filter(p => {
    if (!existsSync(p)) {
      pruned.push({ path: p, reason: 'does not exist' });
      return false;
    }

    if (!statSync(p).isDirectory) {
      pruned.push({ path: p, reason: 'is not a directory' });
      return false;
    }

    return true;
  });

  return [paths, pruned];
}

/**
 * Move all files from source path to destination path only if they are different.
 * @param {string} src Source path
 * @param {string} dst Destination path
 */
export function moveChangedFiles(src, dst) {
  return new Promise(resolve => {
    glob(path.join(src, '**/*.md'), (err, matches) => {
      if (err) {
        console.error('error: could not glob in'.red, src.grey);
        process.exit(1);
      }

      Promise.all(
        matches.map(async srcFile => {
          try {
            const relative = path.relative(src, srcFile);
            const dstFile = path.resolve(dst, relative);

            /** @type {string} */
            let srcData;
            /** @type {string} */
            let dstHash;

            // write to the destination file, unlink src file
            const writeDst = async () => {
              srcData ??= await readFile(srcFile);
              mkdirSync(path.dirname(dstFile), { recursive: true });
              await Promise.all([writeFile(dstFile, srcData), unlink(srcFile)]);
              return true;
            };

            if (!existsSync(dstFile)) return await writeDst();

            // compare source and dest hashes
            [srcData, dstHash] = await Promise.all([
              readFile(srcFile),
              hashFile(dstFile),
            ]);
            const srcHash = hash(srcData);
            if (srcHash === dstHash) {
              await unlink(srcFile);
              return false;
            }

            return await writeDst();
          } catch (err) {
            console.error(
              'error: moving'.red,
              srcFile.grey,
              'to'.red,
              dst.grey
            );
            return false;
          }
        })
      )
        .then(successes =>
          successes.length > 0 ? successes.reduce((a, b) => a + b) : 0
        )
        .then(resolve);
    });
  });
}

/**
 * Hashes a string
 * @param {string} plaintext
 * @return {string} hash
 */
export function hash(plaintext) {
  return createHash('sha1').update(plaintext).digest('base64');
}

/**
 * Hashes a file
 * @param {string} file File path
 * @return {Promise<string>} File hash
 */
export async function hashFile(file) {
  return hash(await readFile(file));
}

import { options } from './args.js';
import { monitor } from './monitor.js';
import { moveChangedFiles, prunePaths } from './util.js';
import { existsSync, mkdirSync, statSync, unlinkSync } from 'fs';
import 'colors';
import process from 'process';
import path from 'path';
import { execSync } from 'child_process';
import os from 'os';

// resolve paths from args
const cwd = process.cwd();
options.paths = options.paths.map(p => path.resolve(cwd, p));
options.output = path.resolve(cwd, options.output);

const TEMP_FILE_NAME = path.resolve(os.tmpdir(), 'bundle.yaml');
const TEMP_OUTPUT = path.resolve(os.tmpdir(), 'rackn_docs');
try {
  if (!existsSync(TEMP_OUTPUT)) mkdirSync(TEMP_OUTPUT);
} catch (err) {
  console.error('error: unable to create temp dir'.red, TEMP_OUTPUT.grey);
  process.exit(1);
}

// input validation

const [paths, pruned] = prunePaths(options.paths);
for (const { path, reason } of pruned) {
  console.warn('warning:'.yellow, `invalid path (${reason}): '${path}'`);
}

if (paths.length === 0) {
  console.error('error: no valid input paths provided'.red);
  process.exit(1);
}

if (!existsSync(options.output)) {
  console.error('error: output path does not exist:'.red, options.output);
  process.exit(1);
}

if (!statSync(options.output).isDirectory) {
  console.error('error: output path is not a directory:'.red, options.output);
  process.exit(1);
}

if (options.build) {
  console.error('error: unimplemented'.red);
  process.exit(1);
}

// monitoring

console.info('info:'.cyan, 'outputting docs in', options.output.grey);
console.info('info:'.cyan, 'monitoring contents in');
for (const path of options.paths) console.info(' -'.grey, path.grey);

monitor(paths, async (contentPath, file) => {
  let contentName = path.basename(contentPath);
  if (contentName === 'content')
    contentName = path.basename(path.resolve(contentPath, '..'));
  console.info('info:'.blue, contentName.yellow, 'file changed', file.grey);

  let dir = contentPath;
  if (
    existsSync(path.resolve(contentPath, contentName + '.go')) &&
    existsSync(path.resolve(contentPath, 'content'))
  )
    dir = path.resolve(contentPath, 'content');

  const run = cmd => {
    const out = execSync(cmd, { cwd: dir }).toString();
    if (out.trim().length > 0) console.info('info:'.cyan, cmd.grey, out);
  };

  console.info('info:'.blue, 'bunding and converting', dir.grey);

  try {
    run(`drpcli contents bundle ${TEMP_FILE_NAME}`);
    run(
      `drpcli contents document-md ${TEMP_FILE_NAME} ${
        options.hash ? TEMP_OUTPUT : options.output
      }`
    );
  } catch (err) {
    console.error('error: unable to run drpcli:'.red, err);
    return;
  }

  if (options.hash) {
    console.info('info:'.blue, 'checking changed files...');
    const count = await moveChangedFiles(TEMP_OUTPUT, options.output);
    console.info('info:'.blue, 'moved', count, 'changed files');
  }
});

import chokidar from 'chokidar';
import { getContentRoot } from './util.js';

/**
 * Monitors a set of paths for content changes
 * @param {string[]} paths Paths to monitor
 * @param {(contentPath: string, file: string) => void} callback Handle content updates
 * @returns {chokidar.FSWatcher}
 */
export function monitor(paths, callback) {
  const watcher = chokidar.watch(paths, { persistent: true });

  /** @param {string} file File that changed */
  const onChange = file => {
    const root = getContentRoot(file);
    if (!root) return;

    callback(root, file);
  };

  watcher.on('change', onChange);

  return watcher;
}

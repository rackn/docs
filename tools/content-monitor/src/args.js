import { ArgumentParser, Namespace } from 'argparse';
const version = process.env.npm_package_version;

const parser = new ArgumentParser({
  description:
    'Monitor content and plugin changes from specified folders and output to the doc tree',
});

parser.add_argument('-v', '--version', { action: 'version', version });
parser.add_argument('paths', {
  help: 'Paths to unbundled contents and plugins source',
  type: 'str',
  nargs: '+',
});
parser.add_argument('-o', '--output', {
  help: 'Specify an output directory in the documentation tree',
  type: 'str',
  required: true,
});
parser.add_argument('-b', '--build', {
  help: 'When on, builds all content at the specified path instead of watches for changes',
  default: false,
  action: 'store_true',
});
parser.add_argument('-H', '--hash', {
  help: 'When on, only moves files that have changed hashes',
  default: false,
  action: 'store_true',
});

/** @type {Namespace & {output: string, paths: string[], build: boolean, hash: boolean}} */
export const options = parser.parse_args();

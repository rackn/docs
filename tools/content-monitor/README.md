# Content Monitor

Most effective paired with `make dirtywatch` for incremental content changes.

Simply save your doc changes in the contents or plugins repo and the markdown will be copied to this repo!

## Setup

1. Install node 16+ via [nvm](https://github.com/nvm-sh/nvm)
2. Update drpcli to latest
3. `npm i` in this directory

## Running

`DOCS_REPO` - path to the root of this repo

Commands are run from the root of this repo.

Example run commands:

- `tools/monitor.sh --help` - help
- `tools/monitor.sh -o DOCS_REPO/docs <path> [paths ...]` - monitor the contents and plugins at `path` and `paths`
- `tools/monitor.sh -o ~/git/rackn/docs/docs ~/git/rackn/provision-content ~/git/rackn/provision-plugins`

Note: The extra /docs is necessary as the exploded content outputs files into `developers/contents` and `resources/objects`.

#!/usr/bin/env bash
cd "$(dirname "$0")"/content-monitor || exit
npm start --silent -- "$@"

#!/usr/bin/env bash

D2_VERSION=v0.6.3

if which d2 >/dev/null 2>/dev/null ; then
        echo "D2 already installed"
        exit 0
fi

arch=$(uname -m)
case $arch in
    x86_64|amd64) arch="amd64";;
    arm64|aarch64) arch="arm64";;
    ppc64le) arch="ppc64le";;
    *)
        xiterr 1 "Unknown arch $(uname -m)";;
esac

osfamily=$(grep "^ID=" /etc/os-release | tr -d '"' | cut -d '=' -f 2)
#osversion=$(grep "^VERSION_ID=" /etc/os-release | tr -d '"' | cut -d '=' -f 2)

osclass="linux"
case $osfamily in
    mac*|dar*) osclass="darwin";;
    win*) osclass="windows";;
    *) true;;
esac

rm -f tmp.tgz
wget https://github.com/terrastruct/d2/releases/download/${D2_VERSION}/d2-${D2_VERSION}-${osclass}-"${arch}".tar.gz -O tmp.tgz

tar -zxvf tmp.tgz

(
  cd d2-* || exit
  scripts/install.sh
)
rm -rf tmp.tgz d2-*


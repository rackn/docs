#!/usr/bin/env bash

rm -rf core/src/operators/endpoint/contents/*
cp core/src/resources/labs/index.md /tmp
cp /tmp/index.md core/src/resources/labs/index.md
rm -rf core/src/resources/labs/*
rm -rf core/src/resources/labs/labs/*
rm -rf refs/src/resources/objects/*
rm -rf refs/src/resources/cli/*
rm -rf refs/src/resources/rel_notes/*
rm -rf refs/src/resources/release.md

#!/usr/bin/env bash

set -e

aggregated_entries="{"

COMMA=""
while read -r file ; do
  ofile="${file:4}"
  while read -r line ; do
    entry="$(sed -r "s:[#]+ (.*?) [{](.*?)[}]:\"\2\"\:\{\"name\"\:\"\1\",\"file\"\:\"$ofile\2\"\}:" <<< "$line")"
    aggregated_entries="${aggregated_entries}${COMMA}${entry}"
    COMMA=","
  done < <(grep -E '^[#]+.*{#.*}' "$file")
done < <(find src -type f | grep '\.md$')

cd docs

while read -r file ; do
  ofile=${file:7}
  entry="\"images-$ofile\": { \"file\": \"$file\", \"name\": \"$ofile\" }"
  aggregated_entries="${aggregated_entries}${COMMA}${entry}"
  COMMA=","
done < <(find images -type f)

while read -r file ; do
  ofile=${file:13}
  entry="\"built_images-$ofile\": { \"file\": \"$file\", \"name\": \"$ofile\" }"
  aggregated_entries="${aggregated_entries}${COMMA}${entry}"
  COMMA=","
done < <(find built_images -type f)
cd ..

aggregated_entries="${aggregated_entries} }"

# store the json
echo "$aggregated_entries" > link_map_core.json
cp link_map_core.json ../refs

#!/usr/bin/env bash

mkdir -p src/built_images
for file in d2-images/*.d2 ; do
    echo "file=$file"
    svg=$(basename "$file" | sed 's/.d2/.svg/g')
    d2 -l elk --font-regular=./fonts/Lato-Regular.ttf --font-bold=./fonts/Lato-Bold.ttf --font-italic=./fonts/Lato-Italic.ttf "$file" src/built_images/"$svg"
done


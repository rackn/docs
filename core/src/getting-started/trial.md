---
title: Trial
tags:
  - howto
  - operator
  - trial
---

# Trial {#rs_trial}

To facilitate starting a trial of Digital Rebar Platform (DRP), a pre-built
set of environments are available for initial discovery.

## Hyper-V on Windows {#rs_trial_hyperv}

On Windows 11 Pro system, Hyper-V can be used to instantiate a trial environment.

### Requirements

* Memory: 16GB Minimum (32GB Preferred)
* Disk: 100GB free

### Run the Trial

From a powershell window, you can run the following commands.  Accept when the script asks for admin privileges.

```powershell
Invoke-WebRequest -Uri "https://s3.us-west-2.amazonaws.com/get.rebar.digital/rackn-trial.ps1" -OutFile "rackn-trial.ps1"

.\rackn-trial.ps1
```

!!! warning

    The script can take a while because the Virtual Machine image is about 6GB in size.

The first step downloads a powershell script. The second script runs the script.  You should accept the warning.

The script will do the following things:

* Make sure Hyper-V is installed and setup
* If Hyper-V needs to be installed, the system will ask you to reboot for Hyper-V to be installed properly.
* Rerun the script, c:\rackn-trial\rackn-trial.ps1, after the reboot
* Download a Hyper-V Virtual Machine
* Start the Virtual Machine
* Start one or three test Virtual Machines depending upon memory
* Launch a browser to the DRP UX

Once the browser page opens, accept the self-signed certificate, login with rocketskates/r0cketsk8ts, and fill out
the RackN Trial license form.  At this point, you can explore the trial enviornment and install Alma 9 upon the
newly discovered machines.

### Clean-up the Trial

To clean-up the environment, run the rackn-cleanup.ps1 script in the c:\rackn-trial directory.

This will stop and remove the Hyper-V instances, but leave the RackN DRP Virtual Machine and the rackn-trial.ps1
script.

## AWS {#rs_trial_aws}

From a linux or WSL or Darwin terminal with AWS credential configuration, a script can launch a set of EC2
instances that will allow you to explore the RackN DRP product.

### Requirements

* Bash shell with AWS cli installed
* AWS credentials with EC2 access

### Run the Trial

From the bash shell, run:

```bash
curl -fsSL https://s3.us-west-2.amazonaws.com/get.rebar.digital/aws-run-drp-trial.sh -o aws-run-drp-trial.sh
curl -fsSL https://s3.us-west-2.amazonaws.com/get.rebar.digital/aws-cleanup-drp-trial.sh -o aws-cleanup-drp-trial.sh
chmod +x aws*.sh
./aws-run-drp-trial.sh
```

The script will create a DRP instance in EC2 and start 3 EC2 instances that will PXE boot against the DRP instance.

Once the script finishes, it will print the URL to the DRP Endpoint.  Open a browser to that URL.
Once the browser page opens, accept the self-signed certificate, login with rocketskates/r0cketsk8ts, and fill out
the RackN Trial license form.  At this point, you can explore the trial enviornment and install Alma 9 upon the
newly discovered machines.

### Clean-up the Trial

To remove the EC2 instances, you can run the aws-cleanup-drp-trial.sh script.

### Multiple Users In Same AWS Resources

The scripts take an optional parameter that will create resources with identifiers of ownership to allow
for multiple users in the same domain.


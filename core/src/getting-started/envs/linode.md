---
title: Linode.com
tags:
  - howto
  - operator
  - install
---
  
# DRP Linode {#rs_linode}

This document outlines the installation and configuration of Digital Rebar on a Linode.com Linode (virtual machine).  There are two methods of deploying DRP in Linode, manually by connecting to the console and running a bash script, or by running a StackScript which provides automation at boot, similar to cloud-init.

[This video shows the process](https://www.youtube-nocookie.com/embed/e4Vp3quzlhM)

## Stackscripts

You can use public Stackscripts to quickly setup your DRP endpoint and
attach nodes. They perform the same basic tasks as the code below.

1.  Create an endpoint using [zehicle/drp](https://cloud.linode.com/stackscripts/549453) and supply your
    version and password.
2.  Verify that the DRP Endpoint is available at the
    https://\[endpoint_ip\]:8092
3.  Create a node using [zehicle/drp-node](https://cloud.linode.com/stackscripts/548252) to join nodes to the endpoint. You'll need to know the endpoint_ip.

## Install DRP in Linode

You can use the Centos-8 Linux image. While more is recommended, make sure
that you have at least 1 GB of RAM. You should open ports `8090/tcp`,
`8091/tcp`, and `8092/tcp` to access the DRP server.

!!! example "Bash script to install DRP"

    ``` bash
    #!/bin/bash
    DRP_VERSION='stable'
    DRP_PASSWORD='r0cketsk8ts'
    DRP_USER='rocketskates'

    # Update your public IP Address
    DRP_PUBLIC_ADDRESS='<DRP public address>'

    ### Open the right firewall ports for DRP
    PORTS="8090/tcp 8091/tcp 8092/tcp"
    for i in $PORTS; do
      echo -n "Opening Port: ${i} - "
      firewall-cmd --permanent --add-port="${i}"
    done
    firewall-cmd --reload

    ### Install DRP
    curl -fsSL get.rebar.digital/tip | 
      bash -s -- install \
        --universal \
        --version="${DRP_VERSION}" \
        --ipaddr="${DRP_PUBLIC_ADDRESS}" \
        --drp-password="${DRP_PASSWORD}"
        --drp-user="R{DRP_USER}"
    ```

!!! warning

    Remember to fill in the public IP address of the system

Once the system is online, you can access DRP using https://\<DRP public address\>:8092.

## Join a machine to a DRP Endpoint in Linode {#rs_linode_joinup}

Once you have a DRP endpoint installed, you can create additional Linode instances and join them
to the DRP endpoint.

!!! example "Bash script to run join a mchine to your DRP Endpoint"

    ```bash
    #!/bin/bash
    DRP_IP='<DRP public address>'                # Enter the public IP of your DRP Linode
    DRP_PORT=8091                                # The fileserver port of the DRP endpoint
    drp_endpoint="https://${DRP_IP}:${DRP_PORT}"

    curl -kfsSL --retry 60 --retry-all-errors --retry-delay 5 "${drp_endpoint}"/machines/join-up.sh
    ```

!!! warning

    Remember to fill in the public IP address of your DRP Endpoint
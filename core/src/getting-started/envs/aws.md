---
title: AWS
tags:
  - howto
  - operator
  - install
---
  
# DRP AWS {#rs_aws}

These instructions can also be adapted to work on GCE or other cloud
infrastructure.

## Install DRP in AWS

You can use the Amazon Linux AMI. While more is recommended, make sure
that you have at least 4 GB of RAM. You should open ports `8090/tcp`,
`8091/tcp`, and `8092/tcp`.

``` bash
#/usr/bin/env bash
curl -fsSL get.rebar.digital/tip | bash -s -- install --universal
```

Once the system is online, you can access DRP using https://\<DRP public address\>:8092.

## Join a machine to a DRP Endpoint in AWS {#rs_aws_joinup}

Once you have a DRP endpoint installed in AWS, from another Amazon Linux AMI,
running the following command will add that machine to the DRP endpoint.

``` bash
#/usr/bin/env bash
export drp=[DRP ADDRESS]
timeout 300 bash -c 'while [[ "$(curl -fsSL -o /dev/null -w %{http_code} $drp:8091/machines/join-up.sh)" != "200" ]]; do sleep 5; done' || false

curl -fsSL $drp:8091/machines/join-up.sh | sudo bash --
```

The machines started using this process will register with their
internal IP address.  By including the `aws-discover` stage,
the machines will log their external IP address to the
`cloud/public-ipv4` parameter.

## AWS Machine join at create

When creating an Amazon Instance, you can provide an init-script that will be run initially.

```bash
  #!/bin/bash
  if [[ ! -f /run/cloud-init/instance-data.json ]] ; then
    # missing cloud-init, create a minimal one
    ID=\$(curl -sfL http://169.254.169.254/latest/meta-data/instance-id)
    sudo mkdir -p /run/cloud-init
    echo "{\"v1\": {\"cloud_name\": \"aws\",\"instance_id\": \"\$\${ID}\" } }" | sudo tee /run/cloud-init/instance-data.json
  fi
  curl -kfsSL http://[DRP_IP]:[DRP_PORT]/machines/join-up.sh | sudo bash --
```

Adding this as the init-script for the instance will have the instance automatically join the DRP endpoint.

!!! note

    Remember to replace the DRP_IP and DRP_PORT fields with your public DRP IP address and port.

## PXE AWS Machine

!!! Note

    Once installed, the machine will not PXE boot anymore.

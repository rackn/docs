---
title: Simple Hyper-V Setup
tags:
  - howto
  - operator
  - install
---

This document describes a method in which WSL and Hyper-V can be set up to
establish a simple environment to test Digital Rebar Provision with.

## Prerequisites

You will need

- Windows 10 or 11 Pro, Enterprise, or Education
- PowerShell
- Windows Subsystem for Linux
- [Hyper-V](https://learn.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)

## Setting up WSL

In this section, we will set up a Ubuntu WSL instance that will run DRP.

!!! warning

    If you already have a WSL instance installed that runs Ubuntu, you may
    choose to run DRP in that, or create a fresh instance.

    The following install command below will *not* create a new instance,
    but will instead open the existing one. To create a fresh instance,
    consider exporting your current instance:

    ```sh
    # wsl --export <distro name> <tarball>
    wsl --export ubuntu ubuntu-image.tar

    # wsl --unregister <distro name>
    wsl --unregister ubuntu
    ```

    At this point, you can proceed with the steps below. Once the new instance is installed, you can re-import your old image:

    ```sh
    # wsl --import <distro name> <install location> <exported tarball>
    wsl --import old-ubuntu ./install-location ubuntu-image.tar
    ```

From PowerShell, run the following command to create a WSL instance that runs
Ubuntu:

```sh
wsl --install -d ubuntu
```

Follow the prompts to create your user account, then proceed.

## Install DRP

You may choose to install DRP in production or as an isolated tip install for
experimentation.

### Production install

Follow the steps described in the [Install guide](#rs_install).

### Isolated install

Follow the steps described in the
[Isolated Install guide](#rs_isolated_install).

!!! note

    After [starting Digital Rebar](#rs_manual_start_drp),
    you can attach to the WSL instance from another PowerShell window with

    ```sh
    wsl -d ubuntu
    ```

## DNS warning

!!! warning

    Refer to this section if you are encountering DNS server errors in WSL2.

Later versions of WSL2 on Windows 11 machines enable
[DNS tunneling](https://devblogs.microsoft.com/commandline/whats-new-in-the-windows-subsystem-for-linux-in-may-2024/#memory-storage-and-networking-improvements)
by default. This prevents DRP's built-in DNS server from behaving correctly.

To disable WSL DNS tunneling, create the file `.wslconfig` in your Windows user
home directory (`%USERPROFILE%\.wslconfig`) with the following:

```
[wsl2]
dnsTunneling=false
```

## Configuring DRP with the UX

Follow the [Post-Install Setup](#rs_post_install) to set up your endpoint's
license.

### Configuring the subnet

Once the endpoint has been set up in the UX, navigate to the Subnets page on the
navigation menu on the left.

Click `Create` on top, then click `Use` next to the `eth0` entry in the menu
that appears on the right.

!!! note

    Change the DNS Server setting to `1.1.1.1` or `8.8.8.8` (or another server)
    to ensure external connectivity.

Scroll to the bottom of the editor and click `Save` to commit your Subnet to
DRP.

At this point, your DRP instance has been fully configured and we can now move
onto provisioning machines with Hyper-V.

## Provisioning machines with Hyper-V

To provision new machines, you will use the Hyper-V Manager.

Open Hyper-V Manager and click `New -> Virtual Machine` from the top right.

1. Click `Next` to manually configure the machine.
2. Name the machine and click `Next`.
3. Choose `Generation 2` and click `Next`.
4. Choose an amount of memory (minimum of 1024 MB) to allocate to the machine.
   Click `Next`.
5. Set the Connection to `WSL`. Proceed.
6. Create a virtual hard disk and allocate it some space (minimum of 2 GB).
   Click `Finish` from this point.

!!! warning

    Hyper-V Manager may enable Secure Boot by default on your machine.
    This will prevent your provisioned machines from properly PXE booting
    from DRP.

    To disable it, right-click your newly created machine and select
    `Settings...`, navigate to `Security` on the left, and uncheck
    `Enable Secure Boot`. Finalize your changes by clicking `Apply`,
    then `OK`.

Your provisioned machine is now ready to be run! Double-click it in Hyper-V
Manager and click `Start`. It should boot Sledgehammer from DRP and become
discovered.

You can navigate to the `Machines` view in the UX to ensure it has been
discovered by DRP.

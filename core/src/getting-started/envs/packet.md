---
title: Equinix Metal
tags:
  - howto
  - operator
  - install
---

Using Equinix Metal, deploy and manage systems with DRP.

## Install DRP

* Create a machine. For initial testing, a t1 is sufficient.
  We recommend using Centos as the DRP host O/S.

* From the SSH console, you\`ll need to run the install script adding the
  public IP address of the system.

* To begin, execute the following commands in a shell or terminal:

  ```bash
  export DRP_IP=[PUBLIC IP]
  mkdir drp ; cd drp
  curl -fsSL get.rebar.digital/stable | bash -s -- --universal --ipaddr=$DRP_IP install
  # wait for install to finish
  echo "You can now visit https://$DRP_IP:8092"
  ```

Once DRP is running, perform added confirmation steps with the CLI:

```bash
# install plugin provider from catalog
drpcli catalog item install packet-ipmi
# optional
drpcli bootenvs uploadiso ubuntu-18.04-install &
drpcli bootenvs uploadiso centos-8-install &
echo "You can now use https://$DRP_IP:8092"
```

## Join a machine to a DRP Endpoint

To add machines to the DRP endpoint, we highly recommend using the
Plugin process below. If you\`d like to create machines yourself, then
the following steps are required to configure the IPXE system.

When creating a machine, you must choose the `custom ipxe` O/S type and
set the `IPXE Script URL` to be `http://[DRP IP]:8091/default.ipxe`
where you use the IP address of the DRP server. Equinix Metal will confirm this
URL is accessible.

In addition, you should set `Persist PXE on Reboot` to true under
additional settings. This is very important if you want to be able to
reprovision or reboot systems and maintain DRP control.

If you use the DRP plugin below, it will make these setting
automatically.

Using the Plugin to add a machine to a DRP Endpoint
---------------------------------------------------

### From the UX

The following steps add the Packet-IPMI plugin:

1.  From the Plugins page, Add the Packet-IPMI plugin
2.  From the Packet-IPMI plugin panel, set your API key and Project UUID
3.  Save

After the plugin is created, revisit the Packet-IPMI plugin page. It
will display a `machine create` line that allows you to quickly create
machines that have the correct Param settings for adding machines.

### From the CLI

Make sure you set your information in the exports!

```bash
# configure plugin
# UPDATE THESE: Packet Project Information for Plugin
export PACKET_API_KEY="[packet_api_key]"
export PACKET_PROJECT_ID="[packet_project_id]"
drpcli plugins create `{ "Name": "packet-ipmi",
  "Params": {
    "packet/api-key": "$PACKET_API_KEY",
    "packet/project-id": "$PACKET_PROJECT_ID"
  },
  "Provider": "packet-ipmi"
}`
# verify it worked - should return true
drpcli plugins show packet-ipmi | jq .Available
```

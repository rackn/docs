---
title: DigitalOcean
tags:
  - howto
  - operator
  - install
  - digitalocean
---

# DRP DigitalOcean

This document outlines the installation and configuration of Digital Rebar on a DigitalOcean droplet (virtual machine).  There are two methods of deploying DRP in DigitalOcean, manually by connecting to the console and running a bash script,  or by providing the installation script via "User Data".

## Droplet Requirements

Minimum Droplet settings required for testing:

* Operating System: CentOS 9
* CPU: 2 Cores
* RAM: 4GB

## Install DRP in DigitalOcean

The following Bash script can be used to install DRP.  This script opens the required ports (8090-8092) for DRP, configures SELinux to allow the service to start, obtains the public IP from the DigitalOcean metadata service, then runs the Digital Rebar installation script.

```bash
#!/usr/bin/env bash
    
# Obtain our public IP address from the metadata service
ipaddr=$(curl -sfL http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)

# Install and configure Digital Rebar
    curl -fsSL get.rebar.digital/tip | 
      bash -s -- install --universal \
        --version="tip" \
        --ipaddr="${ipaddr}" \
        --drp-password="r0cketsk8ts" \
        --drp-user="rocketskates"
```

The install script uses the following options:

* `--universal` - This loads the universal components and bootstraps the system, useful for testing.  **This must be the first option**.
* `--ipaddr` - Sets the ip address to the public IP of of the Droplet
* `--drp-password` - Sets the initial login password.
* `--drp-user` - Sets the initial login user.

## Create a Droplet in the Control Panel 

* Refer to the DigitalOcean documentation on how to [Create a Droplet in the Control Panel](https://docs.digitalocean.com/products/droplets/how-to/create/)
* Add the above Bash script to the ["User Data" section](https://docs.digitalocean.com/products/droplets/how-to/provide-user-data/)

## Create Droplet with API and pass install script via User Data

* Obtain a [Personal Access Token](https://docs.digitalocean.com/reference/api/create-personal-access-token/)
* Save the above Bash script as `cloud-init.sh`
* Obtain an ssh key id (or ids)
* Run the following commands to create the Droplet and pass the installation script via User Data.

```bash
# Set our token for use in API commands
export TOKEN="your-personal-access-token"

# Get the IDs of our SSH keys
ssh_key_ids=$(
  curl -X GET -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TOKEN" \
    "https://api.digitalocean.com/v2/account/keys" | 
  jq '.ssh_keys | map(.id)'
)

# Create our Droplet and assign the ID to a variable
droplet_id=$(
  curl -X POST -H 'Content-Type: application/json' \
    -H "Authorization: Bearer $TOKEN" \
    -d '{"name":"drp-server",
        "size":"s-2vcpu-4gb",
        "ssh_keys": '"${ssh_key_ids}"',
        "region":"nyc1",
        "image":"centos-stream-9-x64",
        "monitoring":true}' \
    "https://api.digitalocean.com/v2/droplets" | 
  jq '.droplet.id'
)

# Get our IP address from our Droplet ID
curl -X GET -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TOKEN" \
    "https://api.digitalocean.com/v2/droplets/${droplet_id}" | 
  jq .droplet.networks.v4[].ip_address
```

## Create Droplet with `user_data` to perform installatio

* Obtain a [Personal Access Token](https://docs.digitalocean.com/reference/api/create-personal-access-token/)
* Install `doctl`

```bash
# Install manually
wget https://github.com/digitalocean/doctl/releases/download/v1.101.0/doctl-1.101.0-linux-amd64.tar.gz --directory-prefix ~/
tar xf ~/doctl-1.101.0-linux-amd64.tar.gz --directory ~/
sudo mv ~/doctl /usr/local/bin

# Install with asdf
asdf plugin-add doctl https://github.com/maristgeek/asdf-doctl.git
asdf install doctl latest
asdf global doctl latest
```

* Create your `doctl` config, and enter your access token

```
$ doctl auth init 
Please authenticate doctl for use with your DigitalOcean account. You can generate a token in the control panel at https://cloud.digitalocean.com/account/api/tokens

❯ Enter your access token:  ●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●  ✔

Validating token... ✔
```

* Obtain a list of ssh key IDs

```
ssh_key_ids=$(doctl compute ssh-key list -o json | jq --raw-output 'map(.id) | join(",")')
```

* Create your Droplet and install DRP by passing user data:

```
$ doctl compute droplet create \
  --region nyc1 \
  --image centos-stream-9-x64 \
  --size s-2vcpu-4gb \
  --ssh-keys "${ssh_key_ids}" \
  --user-data-file ./cloud-init.sh \
  --enable-monitoring \
  drp-server
ID           Name          Public IPv4    Private IPv4    Public IPv6    Memory    VCPUs    Disk    Region    Image                  VPC UUID    Status    Tags    Features                    Volumes
405307851    drp-server                                                  4096      2        80      nyc1      CentOS 9 Stream x64                new               monitoring,droplet_agent
```

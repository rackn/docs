---
title: Isolated
tags:
  - developer
  - howto
  - install
---

# Isolated Install {#rs_isolated_install}

This console install (aka `--isolated`) guide provides a developer-focused 
installation that runs DRP from the Linux or MacOS command line instead of systemd.

## Preparation

This guide expects that users to run Digital Rebar in their
primary work environment. This makes it easy to monitor and reset the
Digital Rebar endpoint.

This document refers to the `drpcli` for
manipulating the `dr-provision` service; however, the `--isolated`
installation does *not* install `drpcli` automatically.

Please make sure your environment doesn't have any conflicts or issues
that might cause port conflicts (see [ports](#rs_arch_ports))
or cause PXE booting to fail.

## Install

To begin, execute the following commands in a shell or terminal:

``` bash
mkdir drp ; cd drp
curl -fsSL get.rebar.digital/stable | bash -s -- install --isolated --version=tip
```

The command will pull the *latest* `dr-provision` bundle and checksum
from github, extract the files, verify prerequisites are installed, and
create some initial directories and links.

The [install](http://get.rebar.digital/stable/) script used by our
installs has many additional options that are documented in its help and
explored in other install guides.

Once you have Digital Rebar working in this fashion, you should be able
to upgrade the endpoint by simply replacing the `dr-provision` binary
and restarting the process. The ability to quickly reset the environment
is a primary benefit of this approach.

## Manually Start Digital Rebar {#rs_manual_start_drp}

Once the install has completed, your terminal should provide next steps
similar to those below.

``` bash
# Run the following commands to start up dr-provision in a local isolated way.
# The server will store information and serve files from the ./drp-data directory.

sudo ./dr-provision --base-root=`pwd`/drp-data |& tee log.out
```

## Back to Regular Install

At this point, you should finish configuration and continue [post-install setup](#rs_post_install).

## Clean Up

Once you are finished exploring Digital Rebar Provision in isolated
mode, the system can be cleaned or reset by removing the directory
containing the isolated install. In the previous sections, we used
'drp' as the directory containing the isolated install. Removing
this directory will clean up the installed files.

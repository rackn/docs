---
title: Production
tags:
  - explanation
  - operator
  - install
---
  
# Production Install {#rs_install}

This document describes the production installation process that
is encapsulated by the [install.sh](https://get.rebar.digital/stable) script.

!!! success "TL;DR - From linux prompt as root"

    ``` bash
    curl -fsSL get.rebar.digital/stable | bash -s -- install --universal
    ```

The following steps are roughly followed to install DRP.

## Get Code

The code is delivered by as a TAR GZipped file (though it carries a
`.zip` filename extension) with a sha256sum to validate contents. These
are stored in an AWS S3 bucket and referenced by the catalog found at
[catalog](https://repo.rackn.io/). You can view the Catalog contents
with a simple `curl` command
(`curl -s --compressed https://repo.rackn.io`), and with `jq`, parse it
to find the paths where things are stored. However, parsing the Catalog
file is generally not needed.

There are at least 3 releases to choose from:

- **tip** - This is the most recent code. This is the latest build
  of master. It is bleeding edge and while the project attempts to
  be very stable with master, it can have issues.
- **stable** - This is the most recent **stable** code.
- **v4.6.4** - There will be a set of Semantic Versioning (aka
  semver) named releases. This is just ane example version string.

Previous releases will continue to be available in tag/release history.
For additional information, see [](#rs_release_process).

When using the **install.sh** script, the version can be specified by
the **--drp-version** flag, e.g. *--drp-version=v4.6.4*.

Updating for current version, an example command sequence for Linux
would be:

>     export DRPVERSION="v4.6.4"        # plz check for current ver before blindly using this!!
>     mkdir dr-provision-install
>     cd dr-provision-install
>     # if $DRPVERSION > 4.7 then update to $DRPVERSION.$arch.$os.zip
>     # example: curl -fsSL https://rebar-catalog.s3-us-west-2.amazonaws.com/drp/$DRPVERSION.amd64.linux.zip -o dr-provision.zip
>     curl -fsSL https://rebar-catalog.s3-us-west-2.amazonaws.com/drp/$DRPVERSION.zip -o dr-provision.zip
>     curl -fsSL https://rebar-catalog.s3-us-west-2.amazonaws.com/drp/$DRPVERSION.sha256 -o dr-provision.sha256
>     sha256sum -c dr-provision.sha256
>     tar -xzvf dr-provision.zip        # yes - it's really not a 'zip' file

At this point, the **install.sh** script is available in the **tools**
directory. It can be used to continue the process or continue following
the steps in the next sections.

!!! note

    **tools/install.sh --help** will provide help and context information.
    Specifically, you will need the `--zipfile` option for this installation
    method.

## Install Configuration Options

Using `dr-provision --help` will provide the most complete list of
configuration options. The following **common items are provided for
reference**. *Please note these may change from version to version*,
check the current scripts options with the `--help` flag to verify
current options.

>     --version                Print Version and exit
>     --disable-provisioner    Disable provisioner
>     --disable-dhcp           Disable DHCP
>     --static-port=           Port the static HTTP file server should listen on (default: 8091)
>     --tftp-port=             Port for the TFTP server to listen on (default: 69)
>     --api-port=              Port for the API server to listen on (default: 8092)
>     --dhcp-port=             Port for the DHCP server to listen on (default: 67)
>     --backend=               Storage backend to use. Can be either 'consul' or 'directory' (default: directory)
>     --data-root=             Location we should store runtime information in (default: /var/lib/dr-provision)
>     --static-ip=             IP address to advertise for the static HTTP file server (default: 192.168.124.11)
>     --file-root=             Root of filesystem we should manage (default: /var/lib/tftpboot)
>     --dhcp-ifs=              Comma-separated list of interfaces to listen for DHCP packets
>     --debug-bootenv=         Debug level for the BootEnv System - 0 = off, 1 = info, 2 = debug (default: 0)
>     --debug-dhcp=            Debug level for the DHCP Server - 0 = off, 1 = info, 2 = debug (default: 0)
>     --debug-renderer=        Debug level for the Template Renderer - 0 = off, 1 = info, 2 = debug (default: 0)
>     --tls-key=               The TLS Key File (default: server.key)
>     --tls-cert=              The TLS Cert File (default: server.crt)
>     --systemd=               Run the systemd enabling commands after installation
>     --startup=               Attempt to start the dr-provision service

!!! note

    In pre v4.2 releases, the **dr-provision** requires two applications to
    operate correctly, **bsdtar** and **7z**. These are used to extract the
    contents of iso and tar images to be served by the file server component
    of **dr-provision**

## Running The Server

The **install.sh** script provides three options for running **dr-provision**.

1.  Production mode installations via
    [systemd](https://en.wikipedia.org/wiki/Systemd) (this guide)
2.  [isolated](#rs_isolated_install) for developers running DRP interactively
3.  [container](#rs_container_install) for trial users minimizing their install requirements

The default values install the server and cli in /usr/local/bin. It will
also put a service control file in place. Once that finishes, the
appropriate service start method will run the daemon. The **install.sh**
script prints out the command to run and enable the service.

!!! note

    The default location for storing runtime information is
    `/var/lib/dr-provision` unless overridden by `--data-root`

The default username & password used for administering the *dr-provision* service is:

```
username: rocketskates
password: r0cketsk8ts
```

Please review `--help` for options like disabling services,
logging or paths.

!!! note

    sudo may be required to handle binding to the TFTP and DHCP ports.

Once running, the following endpoints are available:

-   <https://127.0.0.1:8092/swagger-ui> - swagger-ui to explore the API
-   <https://127.0.0.1:8092/swagger.json> - API Swagger JSON file
-   <https://127.0.0.1:8092/api/v3> - Raw api endpoint
-   <https://127.0.0.1:8092/> - Redirects to RackN Portal (available for
    community use)
-   <http://127.0.0.1:8091> - Static files served by http from the
    *test-data/tftpboot* directory
-   <https://127.0.0.1:8090> - Static files served by https from the
    *test-data/tftpboot* directory (4.7 and higher)
-   udp 69 - Static files served from the test-data/tftpboot directory
    through the tftp protocol
-   udp 67 - DHCP Server listening socket - will only serve addresses
    when once configured. By default, silent.
-   udp 4011 - BINL Server listening socket - will only serve bootfiles
    when once configured. By default, silent.

The API, File Server, DHCP, BINL, and TFTP ports can be configured, but
DHCP, BINL, and TFTP may not function properly on non-standard ports.

!!! note

    On MAC DARWIN there is one additional step. You may have to add a route
    for broadcast addresses to work. This can be done with the following
    comand. The 192.168.100.1 is the IP address of the interface that you
    want to send messages through. The install script will make suggestions
    for you.

    ```bash
    sudo route add 255.255.255.255 192.168.100.1
    ```

## Production Deployments

The following items should be considered for production deployments.
Recommendations may be missing so operators should use their best
judgement.

### Non-root Execution

If you are using DHCPD and TFTPD services of DRP, you will need to be
able to bind to port 67 and 69 (respectively). Typically Unix/Linux
systems require root privileges to do this.

!!! note

    DRP doesn't start as root and then drop privileges with a `fork()` to
    another less privileged user by default.

To enable DRP endpoint to run as a non-privileged user and ensure a
higher level of security, it's possible to use the Linux "*setcap*"
(Capabilities) system to assign rights for the *dr-provision* binary to
open low numbered (privileged) ports. The process is relatively simple,
but does (clearly/obviously) require root permissions initially to
enable the capabilities for the binary. Once the capabilities have been
set, the *dr-provision* binary can be run as a standard user.

To enable any non-privileged user to start up the dr-provision binary
and bind to privileged ports 67 and 69, do the following:

!!! tip "*production* mode"

    sudo setcap "cap_net_raw,cap_net_bind_service=+ep"
    /usr/local/bin/dr-provision

!!! tip "*isolated* mode"

    sudo setcap "cap_net_raw,cap_net_bind_service=+ep"
    $HOME/bin/linux/amd64/dr-provision

Start the "dr-provision" binary as an ordinary user, and now it will
have permission to bind to privileged ports 67 and 69.

For automated [upgrade](#rs_upgrade) from within
DRP, the user that is running DRP needs to have the following in
/etc/sudousers. In this example, `drp-user` is the user
running DRP. This will allow DRP to update itself.

```
drp-user ALL=(ALL:ALL) NOPASSWD:/usr/sbin/setcap
```

!!! note

    The *setcap* command must reference the actual binary itself, and can
    not be pointed at a symbolic link. Additional refinement of the
    capabilities may be possible. For extremely security conscious setups,
    you may want to refer to the StackOverflow discussion (eg setting
    capabilities on a per-user basis, etc.):
    <https://stackoverflow.com/questions/1956732/is-it-possible-to-configure-linux-capabilities-per-user>

!!! note

    You must run the *setcap* command after every upgrade of DRP, the
    *setcap* tracks the binary and if it changes, you must rerun for the new
    binary.

### System Logs

The Digital Rebar Provision service logs by sending output to standard
error. To capture system logs, SystemD (or Docker) should be configured
to direct this output to the desired log management infrastructrure.

### Job Log Rotation

If you are using the jobs system, Digital Rebar Provision stores job
logs based on the directory configuration of the system. This data is
considered compliance related information; consequently, the system does
not automatically remove these records.

Operators should set up a job log rotation mechanism to ensure that
these logs to not exhaust available disk space.

### Removal of Digital Rebar Provision

To remove Digital Rebar Provision, you can use the *tools/install.sh*
script to remove programs for a `production` installs. The
*tools/install.sh* script should be run as root or under sudo unless the
`setcap` process was used.

```bash
tools/install.sh remove
```

To remove programs and data use.

```bash
tools/install.sh --remove-data remove
```

## Running the RackN UX Locally

Setting up DRP to host the RackN UX locally is trivial. The DRP server
includes an embedded web server that can host the UX files from a local
directory. The RackN UX can also be set up using any other HTTP server,
however this document only addresses the setup related to using DRP as
the HTTP server.

The RackN UX uses the rackn-license content pack for entitlements so no
external login to the RacKN SaaS is required.

The RackN UX will still attempt to connect the RackN SaaS for updates
and the catalog; however, the system will operate even if these calls
fail. This can be turned off by setting the `ux.core.airgap` option in
the UX Config page to `true`.

-   Air Gap mode - the RackN UX disables all external calls and only
    operates against the local DRP endpoint.

### Setup

Before starting, you'll need a copy of the RackN UX and to have
installed a `rackn-license.json` content package in the DRP server.
These items require a current RackN license - using them without a valid
enterprise or trial license is a copyright violation.

By default, the RackN DRP server will look for the files in a
a directory named `ux` at the same level as the `base-root` directory, e.g.
`/var/lib/dr-provision`. The account running your `dr-server` must
have read permission for this directory. See below for directions to do this
via the DRPCLI.

This default is *NOT* recommened or easy to maintain.  The `install.sh` process
will use different values for exposing the UX.

The dr-provision command line options that are needed to server the UX
are:

* `--local-ui` - This is a path that can be absolute or relative.  It
  defaults to `ux`, but should not be used.  The relative paths are
  relative to the `base-root` option.  This is usually `/var/lib/dr-provision`.
  The install.sh will configure the system to use a value of `tftpboot/files/ux`.
  This is the preferred value and understand by other tooling like the CLI and
  UX.  This will specific what is served by tftp, http, and https requests to
  the `/ux` directory space of the DRP server.
* `--ui-url` - This is the URL that the ux directory will be directed to.  By
  default, this will be <https://portal.rackn.io>.  This will need to be changed
  to `/ux`.  This will cause redirects to the `--local-ui` files.

A common systemd file looks like this:

!!! example "/etc/systemd/system/dr-provision.service.d/local-ui.conf"

    ``` text
    [Service]
    Environment=RS_LOCAL_UI=tftpboot/files/ux
    Environment=RS_UI_URL=/ux
    ```

After the install flags are set, the following steps may be used to
upload the local UX to the endpoint. This can be performed remotely.

!!! example "Install local UX"

    ``` bash
    # This assumes that the systemd configuration options are used.
    drpcli -c [PATH PROVIDED BY RACKN] catalog item install drp-ux
    ```

### Running the UX from DRP

By unpacking the files in the directory refereced by `--local-ui`,
the DRP endpoint will serve that directory as `/local-ui` and `/ux`.

The endpoint will detect file changes so no restart is required if you
update or change the RackN UX files.

If you are using the default port, you will be redirect to the local UX
(served from `https://[DRP IP]/ux`) instead of the RackN portal when you
option the DRP endpoint in a browser.

!!! note

    This will only serve the files for the UX; it will not ensure that
    the UX starts connecting to the current DRP instance. To address that,
    continue below.

### Redirecting URL

If you are hosting a local UX, you should change the DRP endpoint UX
redirect. This is the site that is presented if you visit the DRP
endpoints root URL, `/`, or the official UI url, `/ui`. To use the local
ux, add `--ui-url=/ux` to the `dr-provision` command line arguments.

If you have connect to this DRP Endpoint previously, you may need to
clear the browsers permanent redirect cache to start using the new
feature.


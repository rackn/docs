---
title: Airgap Install
tags:
- howto
- operator
- install
---

# Airgap Install {#rs_airgap_install}

In an air-gapped install, the DRP endpoint will not have access to the internet. In order to have a functional DRP
endpoint, you will need to pre-fetch some packages from the internet. This document will cover what the minimal
requirements will be to get up and running in the air-gapped environment. We will cover what is needed and where to get
it. In this document, it will be assumed you will be using RHEL/CentOS 7 and starting the installation of DRP after you
have completed at least a"Minimal install" of the operating system. The commands being run to pre-fetch content are
being run from an Ubuntu workstation machine with internet access, a web browser, and wget, but any workstation can be
used as long as it has internet access and a way to download and save files from the internet.

## Preparation

The following packages will need to be installed on your air-gapped endpoint:

- curl

## Step By Step

The following commands should be run on the internet accessible workstation. If you do not have access to a UX you will
also need to follow instructions to install UX locally.

- Build the airgap-install.sh script by running the following:

```shell
curl -fsSL get.rebar.digital/stable | bash -s build-airgap <args for airgap>
# example: curl -fsSL get.rebar.digital/stable | bash -s build-airgap --location=dir_name --version=stable --concurrency=16
```

The above will generate a minimal airgap install.  This will generate a self extracting script called `airgap-install.sh` in the location specified.

The following will generate an airgap image with the discovery and image-deploy bootenvs and additional historical discovery bootenvs.

```bash
curl -fsSL get.rebar.digital/stable | bash -s build-airgap --download-iso-bootenvs="sledgehammer,ubuntuhammer,discovery" --download-iso-profiles="sledgehammer-alma8-v4.10.0,sledgehammer-alma9-v4.12.0,ubuntuhammer-v4.11.0,ubuntuhammer-v4.12.0" ...<other args>
```

!!! note

    If the package repositories are empty, you'll need to download the required isos for sledgehammer. Add the appropriate alma-*-install to your --download-iso-bootenvs flag. The alma version varies based on the sledgehammer version you download."

    !!! example

        ```bash
        curl -fsSL get.rebar.digital/stable | bash -s build-airgap --download-iso-bootenvs="sledgehammer,ubuntuhammer,discovery,alma-8-install" ...<other args>
        ```

!!! note

    By default, all Docker contexts are downloaded. To filter them, use the --download-contexts flag and specify the desired Docker contexts.

!!! note

    If you wish to include the DRP UX in the airgap image, you will need to change the catalog used in the build process.  You will need to get the URL
    of the UX catalog from RackN.  With that URL, you can add the following to the `build-airgap` command: `--catalog="<URL of UX Catalog>"`

For a complete list of build-airgap args see [drpcli airgap](#rs_drpcli_airgap) help.

- Once your `airgap-install.sh` script is built, you will need to move it over to your airgapped system and run the
  following:

```shell
./airgap-install.sh install --universal
# this will install drp in /var/lib/dr-provision by default
# a different location can be specified using the --drp-home-dir flag
# all the flags you would normally use with your regular install script can be used here
```

If you also want to run UX locally and have built the script with a UX catalog, you can run

```shell
./airgap-install.sh install --universal --local-ui
```

!!! note

    Please note that you also have the option to just run:

    ```bash
    ./airgap-install.sh explode
    # This will only explode the catalog to the current dir. A location can also be specified
    ./airgap-install.sh extract
    # This will only extract the tarball to the current dir or location specified
    ```

- Now to verify the portal is working. On a machine that can access the air-gapped endpoint open a web browser and
  visit `https://<YOUR IP>:8092/`. By default, a self-signed cert will need to be accepted (Note that you can provide
  your own certs during deployment.) Log in using the default user and password. Once logged in, you will be prompted to
  upload a valid license.

!!! note

    In addition to port 8092, a self signed cert will need to be accepted by
    visiting [https://\<YOUR IP\>:8090/]{.title-ref} so that the local
    catalog can be displayed in the UI.

- At this time you, need to either generate the license from an existing internet accessible DRP endpoint, or by
  contacting RackN support to have a license generated for you. Once you have a valid license you can upload it from the
  UX.
- Once you have uploaded your license, please verify that your self runner has started up successfully. You can do this
  by going to the machines tab. You should only see one machine, and it should show up with a green play button next to
  it. If something has gone wrong during the initial bootstrap process, you should be able to look at logs to figure out
  what went wrong, fix it and restart the workflow.

---
title: Getting Started
tags:
  - tutorial
  - operator
  - install
---

# Getting Started {#rs_quickstart}

This document will guide you through installing and configuring Digital
Rebar. It will also walk through provisioning a guest OS using Digital
Rebar. For this guide we are going to show a basic virtual environment
where all systems, including the digital rebar instance reside on the
same virtual server and network.

!!! success "TL;DR - From linux prompt as root"

    ``` bash
    curl -fsSL get.rebar.digital/stable | bash -s -- install --universal
    ```

We also recommend consulting the [video
guide](https://www.youtube-nocookie.com/embed/KcvYrUoE0) of this
process. Other helpful videos are available including our [advanced features showcase](https://www.youtube.com/playlist?list=PLXPBeIrpXjfigvrXEQIJxXmFdTHqobooH)
(Site uses cookies).

## Prerequisites

Digital Rebar Platform (DRP) resources (_Machines_ in DRP parlance) can
run on physical, virtual, cloud, or container instances. All instances
should have at least the following minimum requirements.

This guide will configure Digital Rebar to use systemd to control the
service.

This graphic shows the basic layout for the quick start system example:

![Basic layout for a Digital Rebar Quick Start](../images/quick_start_network.png){width="100.0%"}

### Hardware

Specs:

- 1 or more CPU cores
- A minimun of 4 GB of memory
- A minimum of 20 GB of space (60 GB is recommended)

### Operating Systems

Specs:

- Linux (Alma 9 is recommended)
- MacOS

### Software

The following packages are needed for installation.

- bash
- curl
- tar
- sha256sum (linux), shasum (MacOS)

### Network

Digital Rebar environments require detailed knowledge of your
network and DHCP environments.

Digital Rebar reserves ports for DHCP services by default so
it is important to either disable any other DHCP service running on the
endpoint host or use the `--disable-dhcp` flag during installation.

All hosts in the subnet should communicate to the Digital Rebar instance using the
ports listed in the [ports](#rs_arch_ports) link.

Consult the "Host Networking Suggestions for Trials" section below for helpful examples.

### Guest VMs

- 1 or more virtual CPU cores
- A minimum of 2 GB of memory (4 is recommended)
- A minimum of 10 GB of space

## Install Digital Rebar Endpoint Host

Run the following command in a shell or terminal:

```bash
curl -fsSL get.rebar.digital/stable | bash -s -- install --universal
```

The command will download the stable version of Digital Rebar, and
content bundles needed to support _Universal_ workflows. It then
extracts files, verifies prerequisites are installed, and creates the
needed directories and links under `/var/lib/dr-provision`.

The [install](http://get.rebar.digital/stable) script has many
additional options that are documented in the `--help` output and
explored in other install guides. You can output the help text with the
following command:

```bash
curl -fsSL get.rebar.digital/stable | bash -s -- --help
```

Once the installation script completes, a Digital Rebar Platform service
(_endpoint_ in DRP parlance) will be running your local host system.

At this point, you should finish configuration and continue [post-install
setup](#rs_post_install).

## Host Networking Suggestions for Trials

The following additional configuration setups are generally required for trial configurations.

### Firewall Example Configuration

The following firewalld configuration is written for _Alma 9_ and may need to be adjusted for other operating systems.

```bash
systemctl start firewalld
firewall-cmd --permanent --add-port=8090-8093/tcp --add-port=53/tcp --add-port=53/udp --add-port=67-69/udp --add-port=4011/udp --add-port=8080/tcp
firewall-cmd --reload
```

### NAT Routing Example Configuration

The following iptables configuration is written for _Alma 9_ and may need to be adjusted for other operating systems.

This example uses _eth0_ as the internet facing interface and _eth1_ as the private network using the DRP default subnet of 192.168.100.0/24.

```bash
# Allow forwarding of related and established connections first
iptables -I FORWARD 1 -i eth1 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
# Allow forwarding of new connections from the internal network to the external network
iptables -I FORWARD 2 -i eth1 -o eth0 -j ACCEPT
# Enable NAT for outbound traffic from the internal network
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE -s "192.168.100.0/24" ! -d 192.168.100.0/24

# Enable Forwarding
sysctl net.ipv4.ip_forward=1
echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/50-ipv4_ip_forward.conf
sysctl -p
```

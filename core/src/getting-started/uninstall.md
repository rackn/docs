---
title: Uninstall
tags:
  - tutorial
  - operator
  - uninstall
---

Once you are finished exploring Digital Rebar, you can uninstall the
service using the following command:

``` bash
curl -fsSL get.rebar.digital/stable | bash -s -- remove
```

!!! note

    `remove` will *not* remove the data files stored in
    `/var/lib/dr-provision`, `/etc/dr-provision`, or
    `/usr/share/dr-provision`. Include the `--remove-data` flag for a full
    clean-up.

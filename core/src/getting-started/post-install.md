---
title: Post-Install Setup
tags:
  - howto
  - operator
  - install
---
  
# Post-Install Setup {#rs_post_install}

Once you have a running DRP endpoint, you will need to do some additional
sets.  These are the same regardless of your installation environment.

Some steps will depend upon if you are running virtual machine, booting
physical servers, or managing cloud resources.

## UX and License

All Digital Rebar operations run locally and behind your firewall. RackN
*never* has direct access to your DRP endpoint.

The Digital Rebar UX is hosted at [RackN](https://portal.rackn.io/) for
most users. RackN does not have access to your data, credentials, or
provisioning APIs. The RackN portal runs as a single-page app *locally*
in your browser. All DRP API calls remain behind your firewall.

To start, open the Digital Rebar API in a browser:

!!! info

    https://\<ip_address_of_your_endpoint\>:8092/

You will be redirected to the [RackN Portal UX](https://portal.rackn.io)
after you visit the Digital Rebar API port and accept the self-signed
certificate generated by the installation.

Login to Digital Rebar:

!!! info

    ```
    username: rocketskates
    password: r0cketsk8ts
    ```

After login, you will be prompted to create and download a extendable
trial license that enables most enterprise feature of Digital Rebar. If
you save the generated license file, you'll be able to use it to bypass
this process in the future.

## Manually Install RackN License

If you have obtained a RackN license file using the process from prior
installs, then you can bypass this step in subsequent resets by uploading
the `rackn-license.json` file via the CLI.

``` bash
drpcli contents upload rackn-license.json
```

## UX System Bootstrap Wizard

On the top left section of the *Info & Preferences* page, the [RackN
Portal](https://portal.rackn.io) shows the steps needed to create a
fully operational Digital Rebar endpoint.

A quick start system will have some initial configuration already done.
Work through the bootstrap check list until all items have green check
marks. The final *Machines* step requires actions outside of Digital
Rebar, consult [provision a machine](#provision-a-machine) for
assistance.

If you prefer command line actions over a UX, all of the UX Wizard steps
can also be performed using the steps below.

## CLI System Bootstrap (optional)

These `drpcli` bootstrapping steps include
the ones suggested by the install script.

To use DRPCLI, you need to set the target endpoint and credentials. This
can be done per call using command line flags; however, most users
prefer to set environment variables for this purpose.

``` bash
export RS_ENDPOINT=https://<ip_address_of_your_endpoint>:8092/
export RS_KEY=rocketskates:r0cketsk8ts
```

First, set a more secure password for your endpoint.

``` bash
mypass="D1gitalR3bar"
drpcli users password rocketskates $mypass
export RS_KEY=rocketskates:$mypass
```

Next, define the provisioning Subnet in Digital Rebar DHCP. Since you
**must** include all of the necessary DHCP boot options to correctly
PXE boot a Machine, we recommend using the UX to create Subnets because
it has logic to determine sane defaults.

The following command line example contains the JSON Subnet and DHCP
definitions. You *MUST* modify the network parameters to match your
environment.

``` bash
###
#  EXAMPLE - please modify the below values according to your environment  !!!
###

echo '{
  "Name": "local_subnet",
  "Subnet": "10.10.16.10/24",
  "ActiveStart": "10.10.16.100",
  "ActiveEnd": "10.10.16.254",
  "ActiveLeaseTime": 60,
  "Enabled": true,
  "ReservedLeaseTime": 7200,
  "Strategy": "MAC",
  "Options": [
    { "Code": 3, "Value": "10.10.16.1", "Description": "Default Gateway" },
    { "Code": 6, "Value": "8.8.8.8", "Description": "DNS Servers" },
    { "Code": 15, "Value": "example.com", "Description": "Domain Name" }
  ]
}' > /tmp/local_subnet.json

vim /tmp/local_subnet.json

drpcli subnets create - < /tmp/local_subnet.json
```

Finally, install one of these popular trial operating systems.

``` bash
drpcli bootenvs uploadiso ubuntu-20.04-install  # optional, requires at least 3G of free space
drpcli bootenvs uploadiso centos-8-install      # optional, requires at least 30G of free space
```

Note that `drpcli bootenvs uploadiso` command is a helper command that
combines two common steps. It fetchs the ISO referenced in the bootenv
from the internet and then uploads the ISO to Digital Rebar. If you
perform these steps many times, we recommend storing the downloaded ISO
media locally and then uploading it using `drpcli isos upload`.

## Provision a Machine {#provision-a-machine}

Create network bootable virtual machine

- with at least 2 Gb of RAM (4 Gb is preferred)
- set to network boot first
- attached to Digital Rebar provisioning network (aka Layer 2 subnet)

!!! note

    Physical machines attached to the network can DHCP PXE boot as well.

Power on the machine. No further action is required.

The machine should boot into the Sledgehammer discovery operating
system. Typically, the machine console looks like:

!!! success

    ```
    Digital Rebar: Sledgehammer ###................................####
    Kernel 3.10.0-693.21.1.el7.x86_64 on an x86_64

    d0c-c4-7a-e5-48-b6 login:
    ```

    Note the version may be different than is listed above.

After the boot is complete, the UX *Machines* page should show the newly
discovered machine.

If you have installed another operating system ISO besides Sledgehammer,
then you can now provision an operating system by setting the target
workflow on the machine:

1.  Select the target machine (indicated with a check in the left most
    box)
2.  Click the "Profiles" bulk action tab
3.  Select the target profile (typically
    `universal-application-ubuntu-20.04.2` or
    `universal-application-centos-8`)
4.  Apply the profile to the machine by clicking the `+` button to the
    right of the profile
5.  Click the "Machines" tab
6.  Select the target workflow from the action list at the top of the
    page (typically `universal-linux-install`)
7.  Press the "Run Workflow" button (looks like a play icon next to
    the workflow list)

You should see immediate updates to the machine's stages and tasks as
Digital Rebar processes the workflow.

After installation completes, you can quickly return to Sledgehammer by
running the `universal-discover` workflow.

## Advanced Features Catalog (next steps)

Digital Rebar is capable of automated workflow management of the boot
process, power control, and much more. This quickstart walks through the
simplest process to get you up and running with a single test install.
Please review the rest of the documentation for further configuration
details and information on automation of your provisioning environment.

The UX Catalog contains a list of available extensions to Digital Rebar
that are avaible for installation. Choosing an item from the Catalog
will automatically download and install the capability. All items run
locally in your Digital Rebar endpoint, no connection to RackN is
required after the download.

Fully automated provisioning control requires use of plugins for Power
Management actions. These are done through the IPMI subsystem, with a
specific IPMI plugin for a specific environments.


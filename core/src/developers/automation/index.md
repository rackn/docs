---
title: Automation
tags:
  - reference
  - developer
---

# Automation {#rs_dev_auto}

The following sections are meant to help individuals extending the automation to address their
workflow needs.

## Content Generation

[](#rs_dev_auto_content_gen) focuses on building content packs, creating objects in the appropriate
locations, and iteratively developing them.

The other sections discuss the specifics building the types of items to build built.

## Pipelines

[](#rs_dev_auto_piplines) focuses on building pipelines for use but others in the system.  Additionally,
this describes who to enable pipeline elements based upon the targeted system.  It also discusses
profile and parameter management within the pipeline system.  The Operator sections focus upon tracking
and debugging failed fails.  The Developer sections discuss pipeline initial construction development.

## Pipeline elements

[](#rs_dev_auto_pe) focuses on building pipeline elements to extend pipelines by adding new entry points
or other custom segments.

## Tasks

[](#rs_dev_auto_tasks) focuses on building tasks.  These can then be injected through flexiflow tasks within
pipeline elements as specified in a pipeline.

## Blueprints

[](#rs_dev_auto_blueprints) focuses on building blueprints for executing tasks on resources in work order mode.

## Triggers

[](#rs_dev_auto_triggers) focuses on building triggers that can be used to generate event or time-based automations.

## Hardware Profiles

[](#rs_dev_auto_hardware_profiles) focuses on baselining systems and turns those baselines profiles
that can be delivered through content packs and automatically applied through automation by the operators.


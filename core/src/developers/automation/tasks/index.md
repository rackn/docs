---
title: Tasks
tags:
  - reference
  - developer
---

# Tasks {#rs_dev_auto_tasks}

This section focuses on building tasks. These can then be injected through flexiflow
tasks within pipeline elements as specified in a pipeline.


---
title: Repo Helpers
tags:
  - reference
  - developer
---

# Repo Helpers {#rs_dev_auto_tasks_repo_helpers}

As an alternative to rolling your own support for local and remote package repository management,
you can write your templates to use our package repository support. This support consists of three parts:

1. Support in the template rendering engine for a parameter named `package-repositories`, which
   contains a list of package repositories that are available for the various Linux distros we support.
2. The `.Repos`, `.MachineRepos`, and `.InstallRepos` functions that are available at template expansion
   time. These return a list of repo objects, and are described in more detail in the template section.
3. The `.Install` and `.Lines` functions available on each repo object.

## The package-repositories Param

The special `package-repositories` parameter must be present in-scope of the current machine in
order for `.Repos`, `.MachineRepos`, and `.InstallRepos` functions to operate correctly. It must
be present either in the `global` profile, a profile attached to the machine's current stage, a
profile attached to a machine, or directly on the machine as a parameter.

A commented example of a `package-repositories` parameter in YAML format:

``` yaml
- tag: "centos-7-install" # Every repository needs a unique tag.
  # A repository can be used by multiple operating systems.
  # The usual example of this is the EPEL repository, which
  # can be used by all of the RHEL variants of a given generation.
  os:
    - "centos-7"
  # If installSource is true, then the URL points directly
  # to the location we should use for all OS install purposes
  # save for fetching kernel/initrd pairs from (for now, we will
  # still assume that they will live on the DRP server).
  # When installSounrce is true, the os field must contain a single
  # entry that is an exact match for the bootenv's OS.Name field.
  installSource: true
  # For redhat-ish distros when installSource is true,
  # this URL must contain distro, component, and arch components,
  # and as such they do not need to be further specified.
  url: "http://mirrors.kernel.org/centos/7/os/x86_64"
- tag: "centos-7-everything"
  # Since installSource is not true here,
  # we can define several package sources at once by
  # providing a distribution and a components section,
  # and having the URL point at the top-level directory
  # where everything is housed.
  # DRP knows how to expand repo definitions for CentOS and
  # ScientificLinux provided that they follow the standard
  # mirror directory layout for each distro.
  os:
    - centos-7
  url: "http://mirrors.kernel.org/centos"
  distribution: "7"
  components:
    - atomic
    - centosplus
    - configmanagement
    - cr
    - dotnet
    - extras
    - fasttrack
    - os
    - rt
    - sclo
    - updates
- tag: "debian-9-install"
  os:
    - "debian-9"
  installSource: true
  # Debian URLs always follow the same rules, no matter
  # whether the OS install flag is set.  As such,
  # you must always also specify the distribution and
  # at least the main component, although you can also
  # specify other components.
  url: "http://mirrors.kernel.org/debian"
  distribution: stretch
  # If the location of the remote kernel and initrd files cannot be found
  # at the location you would get by appending url and the kernel/initd
  # filenames from the BootEnv, you need to use the bootloc field to
  # override where dr-provision should try to get them from.
  # Kernels and initrds must be located directly at this path.
  bootloc: "http://mirrors.kernel.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/debian-installer/amd64/"
  components:
    - main
    - contrib
    - non-free
- tag: "debian-9-updates"
  os:
    - "debian-9"
  url: "http://mirrors.kernel.org/debian"
  distribution: stretch-updates
  components:
    - main
    - contrib
    - non-free
- tag: "debian-9-backports"
  os:
    - "debian-9"
  url: "http://mirrors.kernel.org/debian"
  distribution: stretch-backports
  components:
    - main
    - contrib
    - non-free
- tag: "debian-9-security"
  os:
    - "debian-9"
  url: "http://security.debian.org/debian-security/"
  securitySource: true
  distribution: stretch/updates
  components:
    - contrib
    - main
    - non-free
```

The default `package-repositories` param in `drp-community-content` contains working examples for
every boot environment supported in that content package.


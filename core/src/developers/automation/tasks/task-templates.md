---
title: Task Templates
tags:
  - reference
  - developer
---

# Task Templates {#rs_dev_auto_tasks_task_templates}

There many provided task templates that can be used to extend or help simply a task construction.

TODO: prelude.tmpl
TODO: job-helper.tmpl

## Set Hostname {#rs_dev_auto_tasks_task_templates_set_hostname}

To set the hostname on the post-installed system, include this template. It will work for ubuntu and centos-based
systems. The place the template in the post-install section of the kickstart or the net-post-install.sh script. The
template uses the machine's built in parameters.

``` go
{{ template "set-hostname.sh.tmpl" . }}
```

!!! warning

    While this will work, the better pattern is to use the `set-hostname` task and inject that into the task
    flow, if it is not already there.  The `set-hostname` task uses this template directly.


## Remote Root Access {#rs_dev_auto_tasks_task_templates_remote_root_access}

This template installs an `authorized_keys` file in the root user's home directory. Multiple
keys may be provided. The template also sets the `/etc/ssh/sshd_config` entry,
`PermitRootLogin`. The default setting is `without-password` (keyed access only), but other
values are available (`no`, `yes`, `forced-commands-only`).

``` go
{{ template "access-keys.sh.tmpl" . }}
```

An example profile that sets the keys and `PermitRootLogin`.

``` yaml
Name: root-access
Params:
  access-keys:
    key1:  ssh-rsa abasbaksl;gksj;glasgjasyyp
    key2:  ssh-rsa moreblablabalkhjlkasjg
  access_ssh_root_mode: yes
```

!!! warning

    While this will work, the better pattern is to use the `ssh-access` task and inject that into the task
    flow, if it is not already there.  The `ssh-access` task uses this template directly.


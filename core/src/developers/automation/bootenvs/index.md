---
title: Bootenvs
tags:
  - reference
  - developer
  - blueprint
---

# Bootenvs {#rs_dev_auto_blueprints}

* Describe bootenv overrides

* Describe bootenv iso building

* Describe kickstart/preseed usage

* Describe runner.tmpl inclusion

* Describe kernel parameters



---
title: Object Generate Commands
tags:
  - reference
  - developer
---

# Object Generate Commands {#rs_dev_auto_contents_generate}

Combining the [](#rs_dev_auto_contents_repo_layout) and the [](#rs_dev_auto_contents_layout), the
`drpcli generate` command can be used to create repos, content packs, and objects all from the command line.

The `generate` command is working directory aware and path aware in naming objects.  Using the current working
directory, the path of the object being created can be used to create or target content packs or repos for creating
or working with objects.

A typical layout is a repo with multiple content packs.  Example paths would be:

!!! example

    Command: drpcli generate object-type repo-name/content-name/object-name

    File create: repo-name/content-name/object-type/object-name.yaml

The `drpcli generate` command will use the current working directory and the path to figure
out if a repo should be created, a content pack should created, and what object to initialize.
Path elements can be left off if the command is run from within the tree.

!!! example

    ```sh
    cd repo-name
    cd content-name
    drpcli generate tasks my-awesome-task
    ```

    This will create a new task, `my-awesome-task`, in the `repo-name` repo
    in the `content-name` content pack in the `tasks` directory in a file named
    `my-awesome-task.yaml`.

Additionally, the `generate` command do basic git operations.

The default format for the generate command is `yaml`.  This is easier for developers to read.

!!! example

    In this example, the drpcli generate command is used to create
    the `rack12-config` profile in the `region1` content pack in the `corp-info`
    git repository.

    Since the repository and the content pack don't exist yet, all of them are created.

    The command is run from the `/home/galthaus` directory.

    Comamnd:

    ```sh
    drpcli generate profiles corp-info/region1/rack12-config
    ```

    Output:

    ```
    Creating base directory:      /home/galthaus/corp-info
    Initializing git repository:  /home/galthaus/corp-info
    Writing:                      /home/galthaus/corp-info/.gitignore
    Writing:                      /home/galthaus/corp-info/README.md
    Creating Directory:           /home/galthaus/corp-info/tools
    Writing:                      /home/galthaus/corp-info/tools/build_content.ps1
    Writing:                      /home/galthaus/corp-info/tools/build_content.sh
    Writing:                      /home/galthaus/corp-info/tools/packs
    Making pack directory:        /home/galthaus/corp-info/region1
    Update package list in repo:  /home/galthaus/corp-info/tools/packs
    Writing:                      /home/galthaus/corp-info/region1/README.md
    Writing meta data:            region1
    Linking README.md to Doc meta:/home/galthaus/corp-info/region1/._Documentation.meta
    Clean up meta data:           /home/galthaus/corp-info/region1/meta.yaml
    Creating Prefix Directory:    /home/galthaus/corp-info/region1/profiles
    Writing:                      /home/galthaus/corp-info/region1/profiles/rack12-config.yaml
    Creating commit:              /home/galthaus/corp-info Initial Repo corp-info and Pack region1 Commit
    Adding to commit:             region1/._Documentation.meta
    Adding to commit:             region1/meta.yaml
    Adding to commit:             tools/packs
    Adding to commit:             region1/profiles/rack12-config.yaml
    Adding to commit:             tools/build_content.ps1
    Adding to commit:             tools/build_content.sh
    Adding to commit:             .gitignore
    Adding to commit:             README.md
    Adding to commit:             region1/README.md
    Commit:                       8c4d0c9e702573bd5f95fa040316912d12a98e20
    Set tag v0.0.1:               /home/galthaus/corp-info
    ```

## Repository command (repo)

The `drpcli generate repo` command takes a single argument, the name of the repo to create in the local
directory.  It will create basic build scripts, do an initial commit, and set an initial version tag.

If the command is run from within the already created respository, it will do nothing.

An additional flag can be provided to set the author email for the initial git commit.

## Content Pack command (pack)

The `drpcli generate pack` command takes a single argument, the name of the content pack to create in the local
direcory.  If a path is added to the name, the first element of the path will be assumed to be the repo and it will
be created if not present.

If the command is run from within the already created content pack, it will do nothing.

An additional flag can be provided to set the author email for the initial git commit.

## Generating Objects

Generating objects requires using the object's type and then a name.  Additional path elements are the content pack and repo
in which the object should be placed.  If those are present and not created, they will be created.  The object will be initialized
with default values and comments to explain the fields. 

See the other [](#rs_dev_auto) sections for specific object examples.

## Continuing GIT operations

Using either git or the `drpcli generate git` command, additional commits should be added to track changes and additions.  Once
a good working content pack is achieved, a version tag should be added to indicate a good checkpoint.  the build tools can then
generate the content pack.

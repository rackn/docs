---
title: Contents Layout
tags:
  - reference
  - developer
---

# Contents Layout {#rs_dev_auto_contents_layout}

A RackN DRP content pack has the following file system structure.

* A meta.yaml file that defines the meta data of the content pack
* A directory for each type of object in the content pack.  e.g. profiles, tasks, templates ...

All of this would live in a single directory for the content.

!!! example

    This is a simple single profile content pack.

    ```
    content1
    content1/profiles
    content1/profiles/my-profile.yaml
    content1/meta.yaml
    content1/._Documentation.meta
    content1/README.md
    ```

    `._Documentation.meta` is a symbolic link to `README.md`.

## meta.yaml

The meta data for a content pack has the following fields:

!!! example

    ```yaml
    Author: my name here
    CodeSource: https://github.com/changeme
    Color: pink
    Copyright: Your Copyright here
    Description: A short description
    DisplayName: Richer name of the content pack
    DocUrl: https://github.com/changeme/README.md
    Icon: linux
    License: Your license here
    Name: content1
    Order: "100"
    Prerequisites: ""
    RequiredFeatures: ""
    Source: https://github.com/changeme
    Tags: tag1,tag2,tag3
    ```

The only required fields are the `Name` field.

The meta data file can also include `Version` and `Documentation` but for a normal
filesystem layout these are handled differently.

The `Version` field is often injected at build time to represent the version of the
content pack by tag or other value.

The `Documentation` field is often stored as a `README.md` file and imported by creating
a link from `._Documentation.meta` to the `README.md` file.  This allows for reading
the documentation from within the change control repository.

## Directories for Objects

A directory should exist for each DRP object type.  The directory contains yaml or json files for
each object of that type.  For example, a profile named `my-profile` would exist in the `profiles`
directory in a file named, `my-profile.yaml`.  The `drpcli` contents bundling tools read those files
and generate a single content bundle of those objects.

!!! note

    Templates are special.  If the file ends in `json`, `yaml`, or `yml`, the file is assumed to
    contain the actual DRP template object.  If the file ending is something else, the bundling
    tools assume the contents are the contents to put into a template object.  The name of
    the template will be the filename.  A common naming convention is to use `base-name.type.tmpl`.

    * base-name - A name that describes what the template is
    * type - The type of file, e.g sh, py, json
    * tmpl - The string `tmpl` to indicate that the file is a template in golang format.

    !!! example

        * name-system.sh.tmpl - Bash script that sets the name of the system
        * phone-home.py.tmpl - Pyhton script to phone home
      
## Manual Bundling

Using the `drpcli contents bundle` command, you can build a content pack manually.

!!! example

    ```sh
    drpcli contents bundle ../my-content.yaml --format=yaml
    ```

Once bunlded, the content pack can be uploaded to a DRP endpoint.

!!! example

    ```sh
    drpcli contents upload ../my-content.yaml
    ```

These can also be added into catalogs and used by managers to deploy automatically to endpoints.
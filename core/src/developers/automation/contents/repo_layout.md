---
title: Repo Layout
tags:
  - reference
  - developer
---

# Repo Layout {#rs_dev_auto_contents_repo_layout}

For Infrastructure as Code (Iac) practices, it is useful to store your content packs into
a change control system, e.g. git.

The following documentation will use `git` as our example repository.

A common useful direcotry structure is as follows.

!!! example

    ```
    repo-test
    repo-test/.gitignore
    repo-test/tools
    repo-test/tools/packs
    repo-test/tools/build_content.sh
    repo-test/tools/build_content.ps1
    repo-test/README.md
    ```

In the above example, `repo-test` is our git repository.  A common layout
includes a `.gitignore` file, a `README.md`, and a set of tools files.

## .gitignore

The `.gitignore` ignores toplevel yaml and json files, version definition files, and
a catalog holding file (rebar-catalog).

!!! example

    ```
    /*.yaml
    /*.json
    rebar-catalog
    */._Version.meta
    ```

## Tools Files and Scripts

In the tools directory, there are a couple of build scripts and a contents list file, `packs`.

The build scripts are for bash and powershell.  They assume that `git` is present and Semver
based version tags.

These scripts will use drpcli to get the version from the current git branch and bundle
each content pack listed in the `packs` file setting the version to the value from drpcli.

### tools/build_content.sh File

```sh
#!/usr/bin/env bash

version=$(drpcli generate git version get)

for cpname in `cat tools/packs` ; do
    cd ${cpname}
    rm -f ._Version.meta
    drpcli contents bundle ../${cpname}.json Version=$version
    drpcli contents bundle ../${cpname}.yaml Version=$version --format=yaml
    cd ..
done
```

### tools/build_content.ps1 File

```powershell
# Get the version using drpcli
$version = drpcli generate git version get

# Read the contents of tools/packs
$cpnames = Get-Content -Path "tools/packs"

foreach ($cpname in $cpnames) {
    # Change directory to the current cpname
    Set-Location -Path $cpname

    # Remove the ._Version.meta file if it exists
    Remove-Item -Path "._Version.meta" -Force -ErrorAction SilentlyContinue

    # Bundle the contents into JSON and YAML formats
    drpcli contents bundle "../$cpname.json" Version=$version
    drpcli contents bundle "../$cpname.yaml" Version=$version --format=yaml

    # Return to the previous directory
    Set-Location -Path ".."
}
```

### tools/packs File

The `tools/packs` file is a flat text file with one line for each content pack directory within the repo.
The entry is a directory at the top-level of the repo.

!!! example

    ```
    content1
    ```


## Versioning

Within the repo, branches and tags can be used to track versions.  The `drpcli generate git version get` command
will attempt to find the most recent SEMVER tag, e.g. v4.13.3 and generate a version from it.

If the tag is on the current commit, the value is used directly.

!!! example

    ```
    commit 6d451369a63eb3e9140477f148f58316b7d8170f (HEAD, tag: v4.13.29)
    Merge: 61d5ff9b 10810a0b
    Author: Greg Althaus <greg@rackn.com>
    Date:   Fri Jul 12 17:41:59 2024 +0000

    Merge branch 'sec-update-v413' into 'v4.13.0-branch'

    docs: update changes

    See merge request rackn/docs!288
    ```

    ```sh
    $ drpcli generate git version get
    v4.13.29
    ```

If the current commit is not tag, the number of commits from that most recent tag is generated and
the git hash is with `g` proceeding it is created.

!!! example

    ```
    commit 9f948513a6e7c745b2012fbdfa8c1ac4d971d72b (HEAD -> v4, origin/v4, origin/HEAD)
    Merge: 00e67107 089a8c89
    Author: Greg Althaus <greg@rackn.com>
    Date:   Tue Nov 19 20:02:46 2024 +0000

    Merge branch 'update-changse' into 'v4'

    doc: Update for drp v4.12.29

    See merge request rackn/docs!353
    ```

    ```sh
    $ drpcli generate git version get
    v4.14.0.165+g9f948513a6e7c745b2012fbdfa8c1ac4d971d72b
    ```

These version get automatically injected into the content packs if the tools/build scripts are used.

Additional, CI/CD automation can be used to run the tools and publish the content packs into artifact
repositories.  For example, RackN uses gitlab ci/cd to automatically build and version content packs and
publish them into AWS s3 buckets after generating our public catalog files.

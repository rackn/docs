---
title: Content Generation
tags:
  - reference
  - developer
---

# Content Generation {#rs_dev_auto_content_gen}

`drpcli` provides a set of commands that facilitate content pack generation and
git repo usage.

## Object Generation {#rs_dev_auto_content_gen_object}

For basic objects, you can use the `drpcli <object type> generate` command.  This will generate
to standard out an example object with comments for fields that can specified.  This can be used
to save files into a content pack directory location that could then be bundled.

!!! example

    drpcli profiles generate my-profile --format=yaml

    ```yaml
    ---
    #
    # Profile represents a set of key/values to use in
    # template expansion.
    #
    # There is one special profile named 'global' that acts
    # as a global set of parameters for the system.
    #
    # These can be assigned to a machine's profile list.
    # swagger:model
    #
    #
    # The name of the profile.  This must be unique across all
    # profiles.
    #
    # required: true
    #
    Name: my-profile
    #
    # Description is a string for providing a simple description
    #
    Description: A short description of the profile named my-profile
    #
    # Documentation is a string for providing additional in depth information.
    #
    Documentation: |2

      # my-profile

      A markdown document that describes profile

      ## Prerequisites

      Any prerequisites go here

      ## Usage

      How to use the profile
    #
    # Meta contains the meta data of the object.
    #
    # The type of this field is a key / value map/dictionary.
    # The key type is string.
    # The value type is also string.
    #
    # The general content of the field is undefined and can be an arbritary store.
    # There are some common known keys:
    #
    # * color - The color the UX uses when displaying
    # * icon - The icon the UX uses when displaying
    # * title - The UX uses this for additional display information.  Often the source of the object.
    #
    # Specific Object types use additional meta data fields.  These are described at:
    # https://docs.rackn.io/stable/redirect/?ref=rs_object_metadata
    #
    # Map of string
    Meta: {}
    #
    # Params holds the values of parameters on the object.
    #
    # The field is a key / value store of the parameters.
    # The key is the name of a parameter.  The key is of type string.
    # The value is the value of the parameter.  The type of the value is defined
    # by the parameter object.  If the key doesn't reference a parameter, the
    # type of the object can be anything.
    #
    # The system will enforce the named parameter's value's type.
    #
    # Go calls the "anything" parameters as "interface {}".  Hence, the type
    # of this field is a map[string]interface{}.
    #
    # Map of interface {}
    Params: {}
    #
    # Profiles is an array of profiles to apply to this object in order when looking
    # for a parameter during rendering.
    #
    # Array of string
    Profiles: []
    ```

## File Content Generation {#rs_dev_auto_content_gen_file}

Using the `drpcli generate` commands, a git repository and content packs can be built directly into
the filesystem.

Each file can be edited and customized for your usage.

!!! note

    The documentation is split into 3 sections.

    * [Repo Layout](#rs_dev_auto_contents_repo_layout)
    * [Content Layout](#rs_dev_auto_contents_layout)
    * [Object Generate Commands](#rs_dev_auto_contents_generate)

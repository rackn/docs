---
title: CLI Scripting/Automation
tags:
  - api
  - reference
  - developer
---

# CLI Scripting/Automation {#rs_dev_cli}

The Developer and the Operator often need to run or build automation of the DRP system.  Using `drpcli` to drive actions from scripts are the basis of DRP tasks and deployment automation.

These sections are intended to support that work.

!!! note

    This is intended for running actions to trigger and operate the system in an automated fashion.

    Using drpcli to assist in content generation can be found in [](#rs_dev_auto_content_gen)


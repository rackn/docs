---
title: Reservations Operations
tags:
- operator
- howto
---

# Reservations Operations {#rs_reservation_operations}

## Creating a Reservation {#rs_create_reservation}

It might be necessary to create a reservation. This would be to make sure that a specific MAC Address received a
specific IP Address. Here is an example command.

```shell
drpcli reservations create '{ "Addr": "1.1.1.1", "Token": "08:00:27:33:77:de", "Strategy": "MAC" }'
```

Additionally, it is possible to add DHCP options or the Next Boot server.

```shell
drpcli reservations create '{ "Addr": "1.1.1.5", "Token": "08:01:27:33:77:de", "Strategy": "MAC", "NextServer": "1.1.1.2", "Options": [ { "Code": 44, "Value": "1.1.1.1" } ] }'
```

Remember to add an option 1 (netmask) if a subnet is not being used to fill the default options.


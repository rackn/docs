---
title: Param Operations
tags:
- operator
- howto
---

# Param Operations {#rs_param_operations}

Parameters are simply key/value pairs. However, DRP provides a strong typing model to enforce a specific type to a given
Param. This ensures that Param values are valid elements as designed by the operator.

## Creating a Param

It might be necessary to create a new parameter, an empty Param may be created by doing the following:

```shell
drpcli params create '{ "Name": "fluffy" }'
```

or

```shell
drpcli params create fluffy
```

The system will attempt to use any sent string as the Name of the Param. To be complete, it is required to also speciy
the Type that param must be:

```shell
drpcli params create '{ "Description": "DNS domainname", "Name": "domainname", "Schema": { "type": "string" } }'
```

In this example, the type `string` was defined for the param.

## Deleting a Param

It might be necessary to delete a parameter.

```shell
drpcli params destroy fluffy
```

!!! note

    The destroy operation will fail if the param is in use.

## Editing a Param

It might be necessary to update a Param. An example to add a `type` of `string` to our `fluffy` param above would be:

```shell
drpcli params update fluffy '{ "Schema": { "type": "string" } }'
```


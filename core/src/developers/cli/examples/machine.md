---
title: Machine Operations
tags:
- operator
- howto
---

# Machine Operations {#rs_machine_operations}

A machine is typically a physical bare metal server, as DRP is intended to operate on bare metal infrastructure.
However, it can represent a Virtual Machine instance and provision it equally. DRP does not provide *control plane*
activities for virtualized environments (eg *VM Create*, etc. operations).

## Creating a Machine

It might be necessary to create a machine. Given the IP that the machine will boot as all that is required is to create
the machine and assign a `bootenv`. To do this, do the following:

```shell
drpcli machine create '{ "Name": "greg.rackn.com", "Address": "1.1.1.1" }'
```

This would create the machine named *greg.rackn.com* with an expected IP Address of *1.1.1.1*. *dr-provision* will
create the machine, create a UUID for the node, and assign the bootenv based upon the *
defaultBootEnv* preference.

```shell
drpcli machine create '{ "Name": "greg.rackn.com", "Address": "1.1.1.1", "BootEnv": "ubuntu-16.04-install" }'
```

This would do the same thing as above, but would create the machine with the * ubuntu-16.04-install* bootenv.

!!! note

    The bootenv MUST exist or the create will fail.

To create an empty machine, do the following:

```shell
drpcli machine create jill.rackn.com
```

This will create an empty machine named *jill.rackn.com*.

!!! note

    The *defaultBootEnv* bootenv MUST exist or the create will fail.

## Adding or Removing a Profile to a Machine

It might be necessary to add or remove a profile to or from a machine. To add a profile, do the following:

```shell
drpcli machines addprofile "dff3a693-76a7-49ce-baaa-773cbb6d5092" myprofile
```

To remove a profile, do the following:

```shell
drpcli machines removeprofile "dff3a693-76a7-49ce-baaa-773cbb6d5092" myprofile
```

The machine update command can also be used to modify the list of profile.

## Changing BootEnv on a Machine

It might be necessary to change the bootenv associated with a machine. To do this, do the following:

```shell
drpcli machines bootenv drpcli "dff3a693-76a7-49ce-baaa-773cbb6d5092" mybootenv
```

!!! note

    The bootenv *MUST* exists or the command will fail.

## Rename a Machine {#rs_rename_machine}

By default, machines are given names based on the machines primary network MAC address. Most infrastructure environments
need to rename machines to fall in line with a naming scheme in use with the company. To do that safely, we will use the
existing Machine object information as a baseline to apply a Patch operation to the JSON. This is a two-step process
that is completed in the following example:

1. First lets define the Machine (UUID) that we're going to operate on, and lets get the current name of the machine for
   reference (`fred` in this case).

```shell
# get our machine to operate on
        export UUID="f6ca7bb6-d74f-4bc1-8544-f3df500fb15e"

        # our reference starting point for 'Name'
        drpcli machines show $UUID | jq '.Name'
        "fred"
```

2. We now need to obtain the Machine object JSON tha we are going to apply the patch against.

```shell
 # get current machine object that we want to reference the change against
        drpcli machines show $UUID  > /tmp/machine.json
```

3. Now that we have our reference Machine object, we'll use the `update` option to the `machines` manipulation.

```shell


 # set the name using the reference JSON object
        drpcli machines update $UUID '{ "Name": "wilma" }' --ref /tmp/machine.json

        # outputs
        {
          "Address": "147.75.66.137",
          <...snip...>
          "Name": "wilma",
          <...snip...>
```

Here is a single command example (using our `$UUID` variable above) that does not require temporary files.

```shell
drpcli machines update $UUID '{ "Name": "barney" }' --ref "$(drpcli machines show $UUID)"
```

You can update "unsafely", but if multiple updates occur, you can't guarantee that you're changing what you expected
to (e.g. someone/thing else beat you to the punch). It is almost always a better pattern to ensure you make a Machine
name change with the use of the `--ref` Machine Object.

```shell


 # this is a BAD way to do it - as it does not guarantee atomicity
        drpcli machines update $UUID '{ "Name": "betty" }'

        # outputs
        {
          "Address": "147.75.66.137",
          <...snip...>
          "Name": "betty",
          <...snip...>
```


---
title: drpcli Usage
tags:
- operator
- howto
---

# Profile Operations {#rs_profile_operations}

profiles are simply collections of parameter - they conveniently group multiple parameter for easy consumption by other
elements of the provisioning service.

## Creating a Profile

It might be necessary to create a profile. An empty profile can be created by doing the following:

```shell
drpcli profiles create '{ "Name": "myprofile" }'
```

or

```shell
drpcli profiles create myprofile
```

The system will attempt to use any sent string as the Name of the profile.

Additionally, JSON can be provided to fill in some default values.

```shell
drpcli profiles create '{ "Name": "myprofile", "Params": { "string_param1": "string", "map_parm1": { "key1": "value", "key2": "value2" } } }'
```

Alternatively, you can create profiles from an existing file containing JSON, as follows:

```shell
echo '{ "Name": "myprofile", "Params": { "string_param1": "string", "map_parm1": { "key1": "value", "key2": "value2" } } }' > my_profile.json
drpcli profiles create -< my_profile.json
```

## Deleting a Profile

It might be necessary to delete a profile. It is possible to use the destroy command in the profile CLI, but the profile
must not be in use. Use the following:

```shell
drpcli profiles destroy myprofile
```

## Altering an Existing Profile (including the `global` profile)

It might be necessary to update an existing profile, including **global**. parameter values can be *set* by doing the
following:

```shell
drpcli profiles set myprofile param crazycat to true
# These last two will show the value or the whole profile.
drpcli profiles get myprofile param crazycat
drpcli profiles show myprofile
```

!!! note

    Setting a parameter's value to **null** will clear it from the structure.

Alternatively, the update command can be used to send raw JSON similar to create.

```shell
drpcli profiles update myprofile '{ "Params": { "string_param1": "string", "map_parm1": { "key1": "value", "key2": "value2" }, "crazycat": null } }'
```

Update is an additive operation by default. So, to remove items, **null** must be passed as the value of the key to be
removed.


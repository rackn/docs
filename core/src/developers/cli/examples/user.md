---
title: User Operations
tags:
- operator
- howto
---

# User Operations {#rs_user_operations}

## Creating a User

It might be necessary to create a user. By default, the user will be created without a valid password. The user will
only be able to access the system through granted tokens.

To create a [user](#rs_drpcli_users_create), do the following:

```shell
drpcli users create fred
```

!!! note

    This user will *NOT* be able to access the system without additional admin action.

## Granting a User Token {#rs_grant_token}

Sometimes as an administrator, it may be necessary to grant a limited use and scope access token to a user. To grant a
token, do the following:

```shell
drpcli users token fred
```

This will create a token that is valid for 1 hour and can do anything. Additionally, the CLI can take additional
parameters that alter the token's scope (model), actions, and key.

```shell
drpcli users token fred ttl 600 scope users action password specific fred
```

This will create a token that is valid for 10 minutes and can only execute the password API call on the user object
named *fred*.

To use the token in with the CLI, use the -T option.

```shell
drpcli -T <token> bootenvs list
```

## Deleting a User

It might be necessary to remove a reset from the system. To remove a user, do the following:

```shell
drpcli users destroy fred
```

## Revoking a User's Password

To clear the password from a user, do the following:

```shell
drpcli users update fred '{ "PasswordHash": "12" }'
```

This basically creates an invalid hash which matches no passwords. Issued tokens will still continue to function until
their times expire.

## Secure User Creation Pattern

A secure pattern would be the following:

- Admin creates a new account

```shell
drpcli users create fred
```

- Admin creates a token for that account that only can set the password and sends that token to new user.

```shell
drpcli users token fred scope users action password ttl 3600
```

- New user uses token to set their password

```shell
drpcli -T <token> users password fred mypassword
```


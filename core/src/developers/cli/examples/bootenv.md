---
title: Bootenv Operations
tags:
- operator
- howto
---

# Bootenv Operations {#rs_bootenv_operations}

A `bootenv` is the primary component for an Operating System installation definition. A `bootenv` comprises two primary
pieces:

1. A `bootenv` JSON/YAML specification
2. (usually) an ISO Image that installs that `bootenv`

The JSON/YAML specification will contain a set of definitions for the ISO image. The default distributed `bootenv` specs
use the public mirror repos for the ISO images. You can create a customer `bootenv` with a pointer to your own hosted ISO
images. An example looks something like:

```shell
root@demo:~$ drpcli bootenvs show ubuntu-16.04-install
{
"Available": true,
"Name": "ubuntu-16.04-install",
"OS": {
  "Family": "ubuntu",
  "IsoFile": "ubuntu-16.04.3-server-amd64.iso",
  "IsoSha256": "a06cd926f5855d4f21fb4bc9978a35312f815fbda0d0ef7fdc846861f4fc4600",
  "IsoUrl": "http://mirrors.kernel.org/ubuntu-releases/16.04/ubuntu-16.04.3-server-amd64.iso",
  "Name": "ubuntu-16.04",
<...snip...>
```

This stanza shows the Ubuntu 16.04 `bootenv` along with the associated Mirror HTTP location the ISO will be installed
from.

## Installing a "Canned" BootEnv

Manipulating `bootenv` and template are handled by their own commands. There are some additional helpers especially when
following the layout of the initial install.

To install a provided `bootenv`, do the following from the install location.

```shell
drpcli bootenvs uploadiso ubuntu-16.04-install
```

This is a CLI helper that is not in the API that will read the provided YAML `bootenv` file, upload the included or
referenced template files (from the *templates* peer directory), upload the `bootenv`, and check for an existing ISO in
the ISO repository. If an ISO is not present in the already uploaded list, it will check a local isos directory for the
file. If that is not present and the `bootenv` contains a URL for the ISO, the ISO will attempt to be downloaded to the
local isos directory and then uploaded into Digital Rebar Provision. Once upload, the ISO is "exploded" for access by
machines in the file server file system space.

## Listing Installed BootEnvs

A list of all existing `bootenv` installed on the DRP Endpoint can be obtained with the *list* command. However, you
usually do not wish to see all the JSON values, and a simple `jq` filter can help output just the keys you are
interested in, as follows:

```shell
drpcli bootenvs list | jq -r '.[].Name'

Outputs:
centos-7-install
centos-7.4.1708-install
debian-8-install
debian-9-install
discovery
ignore
local
sledgehammer
ubuntu-16.04-install
```

## Cloning a BootEnv

Sometimes there is a `bootenv` but it is necessary to make changes. These can be handled by template inclusion, but for
now let's just focus on basic "cut and paste" style editing.

```shell
drpcli bootenvs show ubuntu-16.04-install --format yaml > new-file.yaml
# Edit the file
# change the Name field to something new. *MUST DO THIS*
# change the OS->Name field to something new to avoid sharing an iso directory.
# Edit other parameters as needed
drpcli bootenvs create - < new-file.yaml
```

This is a shallow clone. It will reuse the templates unless they are explicitly modified. It is possible to use the *
install* command, but any new templates would need to be added to a *templates* directory in the current directory.

## Creating a BootEnv

It might be necessary to create an empty `bootenv` by doing the following:

```shell
drpcli bootenvs create emtpy_bootenv
```

This `bootenv` will not be *Available*, but will allow for additional editing.

## Editing a BootEnv

It might be necessary to edit a `bootenv`. To do this, get the latest copy with the *show* command. Edit the file as
needed. Then using the *update* command, put the value back. The *--format=yaml* is optional, but I find YAML easier to
edit.

```shell
drpcli bootenvs show discovery --format=yaml > discovery.yaml
# Edit the discovery.yaml as needed
drpcli bootenvs update discovery - < discovery.yaml
```

## Downloading ISOs by URL

While the `uploadiso` command for BootEnvs, Profiles, Stages, and Machines provides a mechanism to download ISOs to
satisfy the ISO requirement of a BootEnv or its customization, it is sometimes helpful to pull the ISO and locally and
distribute this. Often times wget or curl is used for this process. Some URLs require signatures by RackN to get access
to the ISO. The CLI and UX handle this for you. If a signed URL is needed, the `drpcli` provides a sign URL feature to
provide a limited time use URL for download purposes.

```shell
drpcli system signurl <url>
```

This will return if the URL needs to be signed or other errors on failure. If successful, a new URL with a signature
will be provided that is valid for a limited about of time, usually an hour. The command requires access to DRP with a
valid license.


---
title: Preference Operations
tags:
- operator
- howto
---

# Preference Setting {#rs_preference_setting}

By default, Digital Rebar Provision (DRP) attempts to "do no harm". This means that by default, any system that receives
a DHCP lease from the DRP Endpoint, will by default, be set to `local` mode, which means boot from local disks. You must
explicitly change this behavior to enable provisioning activities of Machines.

It is necessary to get or set the preferences for the system, to enable OS Installs (BootEnvs).

```shell
# Show the current preference settings
drpcli prefs list

# Or get a specific one
drpcli prefs get unknownBootEnv
```

Set preference(s):

```shell
# Set a preference
drpcli prefs set unknownBootEnv discovery

# or chain multiples as pairs
drpcli prefs set unknownBootEnv discovery defaultBootEnv sledgehammer defaultStage discover
```

The system does validate values to make sure they are sane, so watch for errors.


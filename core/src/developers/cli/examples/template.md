---
title: Template Operations
tags:
- operator
- howto
---

# Template Operations {#rs_template_operations}

Templates are reusable blocks of code, that are dynamically expanded when used. This allows for very sophisticated and
complex operations. It also allows for carefully crafted Templates to be re-usable across a broad set of use cases.

## Cloning a Template

It might be necessary to create a new template from an existing one. To do this, do the following:

```shell
drpcli templates show net_seed.tmpl | jq -r .Contents > new.tmpl
# Edit the new.tmpl to be what is required
drpcli templates upload new.tmpl as new_template
```

In this case, we are using `jq` to help us out. `jq` is a JSON processing command line filter. JSON can be used to
retrieve the required data. In this case, we are wanting the Contents of the template. We save that to file, edit it,
and upload it as a new template, *new_template*.

It is possible to use the **create** subcommand of template, but often times **upload** is easier.

!!! note

    Remember to add the new template to a bootenv or another template as an embedded template.

## Updating a Template

It might be necessary to edit an existing template. To do this, do the following:

```shell
drpcli templates show net_seed.tmpl | jq -r .Contents > edit.tmpl
# Edit the edit.tmpl to be what is desired
drpcli templates upload edit.tmpl as net_seed.tmpl
```

We use `jq` to get a copy of the current template, edit it, and use the upload command to replace the template. If there
already is a template present, then it can be replaced with the upload command.


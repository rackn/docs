---
title: Stage Operations
tags:
- operator
- howto
---

# Stage Operations {#rs_stage_operations}

Stages are simply key/value pairs. However, DRP provides a strong typing model to enforce a specific type to a given
Stage. This ensures that Stage values are valid elements as designed by the operator.

## Creating a Stage

It might be necessary to create a new stage, an empty Stage may be created by doing the following:

```shell
drpcli stages create '{ "Name": "fluffy" }'
```

or

```shell
drpcli stages create fluffy
```

The system will attempt to use any sent string as the Name of the Stage. To be complete, it is required to also speciy
the Type that stage must be:

```shell
drpcli stages create '{ "Description": "DNS domainname", "Name": "domainname" }'
```

In this example, the type `string` was defined for the stage.

## Deleting a Stage

It might be necessary to delete a stage.

```shell
drpcli stages destroy fluffy
```

!!! note

    The destroy operation will fail if the stage is in use.

## Editing a Stage

It might be necessary to update a Stage. An example to add a `type` of `string` to our `fluffy` stage above would be:

```shell
drpcli stages update fluffy '{ "Description": "description" }'
```


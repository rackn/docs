---
title: Filter Syntax
tags:
- tutorial
- howto
---

# Filter Syntax {#rs_api_filter_expansion}

To filter by indexes, you can use the following stanzas:

- *index* Eq *value*
  This will return items Equal to *value* according to *index*

- *index* Ne *value*
  This will return items Not Equal to *value* according to *index*

- *index* Lt *value*
  This will return items Less Than *value* according to *index*

- *index* Lte *value*
  This will return items Less Than Or Equal to *value* according to *index*

- *index* Gt *value*
  This will return items Greater Than *value* according to *index*

- *index* Gte *value*
  This will return items Greater Than Or Equal to *value* according to *index*

- *index* Re *re2 compatible regular expression*
  This will return items in *index* that match the passed-in regular expression We use the regular expression syntax
  described at https://github.com/google/re2/wiki/Syntax

- *index* Between *lower* *upper*
  This will return items Greater Than Or Equal to *lower* and Less Than Or Equal to *upper* according to *index*

- *index* Except *lower* *upper*
  This will return items Less Than *lower* or Greater Than *upper* according to *index*

- *index* In *comma,separated,list,of,values*
  This will return any items In the set passed for the comma-separated list of values.

- *index* Nin *comma,separated,list,of,values*
  This will return any items Not In the set passed for the comma-separated list of values.

You can chain any number of filters together, and they will pipeline into each other as appropriate.

By default, the list functionality does NOT aggregate parameters for searching. Adding *aggregate=true* will enable
searching of attached profiles for values.

For complex queries, the raw= filter can be used to pass a filter string with template expansion pieces. The raw field
is added to other filters provided The result of expansion should be a space separated set of filter functions from
above.

An example:

```shell
raw={{ .Param "trigger/runner-target" }} Meta.color=pink
```

You can use `filter` to specify a `Filter` object to use as the basis for this query.


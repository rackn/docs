---
title: drpcli Usage
tags:
- operator
- howto
---

# drpcli Usage {#rs_drpcli_usage}

!!! note

    **drpcli** normally outputs JSON formatted objects or an array of objects. To help with some of the command line functions, we use the **jq** tool. This is available for both Linux and Darwin. You can specify output format to be YAML, with the `--format=yaml` flag.

In this doc we will assume that `drpcli` is already somewhere in the path and have setup the environment variables to
access Digital Rebar Provision. See, [](#rs_cli).

The `drpcli` tool is designed to provide a single, easy to use compiled stand-alone binary. It very closely mimics the
API call parameters and usage, and allows for easy "transition" between the use of the CLI binary, and the use of the
API calls.

We assume that you have the binary in your `PATH` somewhere (`/usr/local/bin/drpcli` in "production" mode by default).
If you are using an "isolated" install mode, you will need to link the correct binary in to an appropriate directory in
your PATH. For example:

```shell
ln -s $HOME/bin/linux/amd64/dr-provision $HOME/bin/
ln -s $HOME/bin/linux/amd64/drpcli $HOME/bin/
```

`drpcli` is a self documenting binary file. At any point you can simply hit `<Enter>` to see the current contextual help
output. Some examples:

`drpcli <Enter>`
:   Prints usage for the top-level resources that may be manipulated.

`drpcli templates <Enter>`
:   Prints the resources that may be executed for the `templates` level.

`drpcli templates create <Enter>`
:   Will display the commands associated with just `templates/create/`

Note that the CLI syntax closely follows the API calls. For example:

```shell
drpcli templates create -< some_file.json
```

Would be equivalent to the HTTP REST API resource as follows:

```
https://127.0.0.1:8092/api/v3/templates/create
```

!!! note

    See the [](#rs_autocomplete) for BASH shell auto completion.

## Helpful Tips and Tricks {#rs_dev_drpcli_tips}

!!! note

    **drpcli** can provide additional information about when `show`ing the object.  Using the
    ``--commented`` flag, the json object will contain a `Comments` field that contains
    definitions for the fields.  For yaml objects, the comments will be inlined with the fields.

    When called against an object in the DRP endpoint, the parameters will also have their
    descriptions and documentation included as well.

    !!! example

        This example will show the current commented fields of the global profile.  Since
        this is attached to a DRP Endpoint, the system will also provided the description
        of the parameters in the profile as well.

        ```shell
        drpcli profiles show global --commented --format=yaml
        ```

     This works on all object types.

!!! note

    **drpcli** can `show` only the read-write fields of an object using the ``--reduced`` flag.

    This is helpful for building content packs from existing objects in the DRP endpoint.

    !!! example

        ```shell
        drpcli profiles show global --reduced
        ```


---
title: Developer Guide
tags:
  - reference
  - developer
---

# Developer Guide {#rs_dev}

The following sections are meant to help individuals developing automation or integerations for the
Digital Rebar Platform.

* API Automation
* CLI Scripting/Automation
* Automation Content Creation
* Plugin Construction
* Contributing to RackN Public Repos

!!! note

    The documentation is split into 5 sections.

    * [Geting Started](#rs_quickstart)
    * [Architectural Guide](#rs_arch)
    * [Developer Guide](#rs_dev)
    * [Operator Guide](#rs_ops)
    * [Resources](#rs_res)



---
title: Go Client Library
tags:
  - api
  - developer
---

# Go Client Library {#rs_dev_api_golang}

Much of the RackN DRP product is implemented in go.  For example, `drpcli` is a
multi-platform golang-based tool.  For it to interact with DRP, a library of functions
exists to faciliate that interaction.

Any golang program can include and use this library to drive a DRP endpoint.

The golang code resides in [gitlab](https://gitlab.com/rackn/provision) with generated
go [docs](https://pkg.go.dev/gitlab.com/rackn/provision/v4/api).

## Examples

The `drpcli` program is implemented as a library in the `cli` package within the provision
repository.  You can review the
[code](https://gitlab.com/rackn/provision/-/tree/v4/cli?ref_type=heads)
for ways to call the library.

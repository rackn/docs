---
title: Using CURL
tags:
  - stage
  - developer
  - api
  - howto
---

# Using CURL {#rs_dev_api_curl}

In many cases, it is helpful to test the API using CURL commands.

Here is the basic structure of a curl command using an auth token

```shell
export RS_TOKEN=$(./drpcli users token [username] | jq -r .Token)
curl -H "Authorization: Bearer $RS_TOKEN" --insecure https//[endpoint url]/api/v3/info
```

You can also use user security

```shell
curl --user rocketskates:r0cketsk8ts --insecure https//[endpoint url]/api/v3/info
```

## Uploading ISO, File or Plugin Providers {#rs_dev_curl_iso}

For binary items, Digital Rebar Provision expects either and "application/octet-stream" or "multipart/form-data" for the
POST. Octet is helpful for direct sending from programs like the CLI. Multipart is helpful when sending files from a
webapp where the browser is directly responsible for handling file uploads.

```shell
export RS_TOKEN=$(./drpcli users token [username] | jq -r .Token)
curl -X POST  --insecure \
  -H "Authorization: Bearer $RS_TOKEN" \
  -H "Content-Type: multipart/form-data" \
  -F "file=@[filepath]/[filename]"\
  https://[endpoint url]/api/v3/isos/[filename]
```

## Using PATCH with CURL {#rs_dev_patch}

For updates with the Digital Rebar API, PATCH is strongly preferred over PUT.

Here's an example of updating an object using PATCH

```shell
curl -X PATCH --insecure \
  -H "Authorization: Bearer $RS_TOKEN" \
  -H "Content-Type: application/json" \
  -d '[{"op":"replace", "path":"/Fingerprint/CloudInstanceID", "value":"cloud:314159265"}]' \
  {{.ApiURL}}/api/v3/machines/$RS_UUID
```

---
title: DRP API 
tags:
  - api
  - reference
  - developer
---

# DRP API {#rs_api}

All API calls are available under `/api/v3` based on the Digital Rebar API convention.

## API PUT vs PATCH

RackN strongly recommends API consumers to use PATCH wherever possible instead of PUT. PATCH allows API calls to change
specific object fields and may include tests to prevent race conditions.

For example, drpcli and the UX use JSON PATCH (<https://tools.ietf.org/html/rfc6902>) instead of PUT extensively. PATCH
allows atomic field-level updates by including tests in the update. This means that simultaneous updates do not create "
last in" race conditions. Instead, the update will fail in a predictable way that can be used in scripts.

The DRPCLI facilitates use of PATCH for atomic operations by allowing scripts to pass in a reference (aka pre-modified)
object. If the `-r`
reference object does not match then the update will be rejected.

## API Filters {#rs_api_filters}

The API includes index driven filters for large deployments that can be used to pre-filter requests from the API.

The list of available indexes is provided by the `/api/v3/indexes` and
`/api/v3/indexes/[model]` calls. These objects provide a list of all keys that can be used for filters with some
additional metadata. Additionally, the *Meta* data field on objects can be used in filters as welll, e.g.
Meta.color=pink.

To use the index, simply include one or more indexes and values on the request URI. For example:

```
/api/v3/machines?Runnable=true&Available=true
```

The filter specification allows more complex filters using functions:

- Equal (either works)
    - =
    - Eq(Value)
- Less Than: Lt(Value)
- Less or Equal: Lte(Value)
- Greater Than: Gt(Value)
- Greater or Equal: Gte(Value)
- Not Equal: Ne(Value)
- Regular Expression: Re(Value)
- Ranges:
    - Between(Value1,Value2) (edited)
    - Except(Value1,Value2)

The query string applies ALL parameters are to be applied (as implied by the & separator). All must match to be
returned.

## Filtering by Param Value

The API includes specialized filter behavior for Params that allows deep searching models for Param values.

To filter Machines or Profiles by Param values, pass the Param name and value using the normal Field filter
specification. When the Field is not found, the backend will search model's Params keys and evalute the filter against
the Param value.

## Filtering by Aggregated Param Value

By default, the API does NOT aggregate the values of the parameters through the attached profiles or global profile. By
adding the
[aggregate=true]{.title-ref} string to the Query string in the URL, the API will apply the filter against an aggregated
set of parameters when searching for the object. Additionally, the *Param* field of the object will contain an
aggregated set of parameters as well.

## Filtering with Template Expansion

The API can also use template functions to enhance searches. Using the `raw=<filter string>`, a raw filter string can be
sent to the server for expansion and processing. The raw field will be additive to the other query string filters sent.
Multiple filter elements can be sent in a space delimited format. Here are some examples:

## Leveraging Multi-Site Proxy Forwarding {#rs_api_multisite_proxy_forwarding}

In the manager, API calls to a DRP manager for remote objects are automatically forwarded to the correct attached
endpoint. This allows operators to make API calls to remote endpoints from a centralized manager without knowing the
actual owner of the object. The owning endpoint can be determined for any object by inspecting its `Endpoint` property.

For most cases, no additional information or API notation is required to leverage this functionality. The exception is
creating objects on attached endpoints. The create (aka POST) case requires API callers to specify the target remote
endpoint (the endpoint must be registered or the request will be rejected) to the manager.

To make an explicit API proxy call, prefix the path of the request with the endpoint identity. For example:

```
/[target endpoint id]/api/v3/machines
```

This pattern is also invoked by with the `-u` flag in drpcli.

NOTE: Multi-site is a licensed feature available in DRP v4.5+ and must be enabled for the endpoint.

## Payload Reduction (slim)

Since Params and Meta may contain a lot of data, the API supports the
`?slim=[Params,Meta]` option to allow requests to leave these fields unpopulated. If this data is needed, operators will
have to request the full object or object's Params or Meta in secondary calls.

>     /api/v3/machines?slim=Params,Meta

Only endpoints that offer the `slim-objects` feature flag (v3.9+) will accept this flag.

## Exploration with Curl

You can also interact with the API using `curl`. The general pattern is:

``` shell
# option 1: basic auth user:password
curl -X <method> -k -u <username>:<password> -H 'Content-Type: application/json' -H 'Accept: application/json' https://<endpoint addr>:<port>/api/v3/<opject type>/<object ID>

# option 2: basic auth with token
export RS_TOKEN=$(drpcli -P <password> users token <username>)
curl -X <method> -k --header 'Authorization: Bearer $RS_TOKEN' -H 'Content-Type: application/json' -H 'Accept: application/json' https://<endpoint addr>:<port>/api/v3/<opject type>/<object ID>
```

In the remainder of this section, `<object type>` refers to the lower case, pluralized version of the type of object.
This is `bootenvs` for boot environments, `workflows` for workflows, `machines` for machines, and so on. `<object id>`
refers to the unique identifier for this object, which is generally the `Name`, `ID` or `Uuid` field of an object. You
can also use any unique index in this field, in the form of
`<index name>:<value>`. A common one to use is `Name:machine.name` for Machine objects instead of the Uuid.

The API follows the usual REST guidelines:

- `HEAD /api/v3/<object type>` gets you headers containing basic information about how many of `<object type>` are
  present in the system. You can use filters on this request.
- `GET /api/v3/<object type>` lists all of the objects of the requested type. The result is a JSON array. You can use
  filters on this request.
- `POST /api/v3/<object type>` is a request to create a new object. The body of the payload should be valid JSON for the
  object type.
- `GET /api/v3/<object type>/<object id>` fetches the request object.
- `HEAD /spi/v3/<object type>/<object id>` tests to see if the requested object exists.

## API Exception & Deprecation Notes

There are times when the API and models have exceptions or changes that do not follow the normal pattern. This section
is designed to provide a reference for those exceptions.

This section is intended to provide general information about and functional of the API. We maintain this section for
legacy operators, when possible avoid using deprecated features.

---
tags:
  - api
  - developer
---

## Swagger UI

The Digital Rebar Provision UI includes Swagger to allow exploration and
testing of the API.

To access the Swagger web interface, point your web browser at your DRP
Endpoint IP addres, at `https://<IP_ADDRESS>:8092/swagger-ui`.

Ensure that the input form contains the same IP address reference as
your DRP Endpoint. Click the `Authorize` button to activate either an API
Token authentication or the Username/Password authentication.

!!! note

    When using API Token authentication, you will need to precede your token
    with `Bearer ` to operate properly.  This version of Swagger-UI/Swagger
    does not add it automatically.  An api token can be obtained from the CLI
    or UX.

    !!! example

        Bearer my-base-64-encoded-string-of-token


!!! note

    Due to the limitations of the Swagger specification, there are important
    API optimizations that are not exposed in the
    swagger.json file. Please review the API docs
    for the complete capabilities.

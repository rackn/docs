---
title: Markdown Notes
tags:
  - explanation
---

# Markdown Notes {#rs_markdown_notes}

RackN documentation is written in Markdown as scoped by mkdocs and its plugins.


The following docs are helpful for finding markdown and mkdocs cheats.

* General [setup](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors) of mkdocs features
* Details of custom [markdown] and mkdocs options (https://squidfunk.github.io/mkdocs-material/reference/)
* [Python markdown](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/) information
* [mkdocs User Guide](https://www.mkdocs.org/user-guide/)
* [awesome-pages](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin) plugin for better nav configuraiton
* tags in mkdoc [notes](https://github.com/squidfunk/mkdocs-material/issues/3864) - talks about how they work - DETAILED
* [mike](https://github.com/jimporter/mike) builds the docs in our CI/CD pipeline and provides the versioning by branch.
* mike / gitlab integration [notes](https://github.com/jimporter/mike/issues/25)
* Main [repo](https://gitlab.com/rackn/docs) with docs for local development
* [Giscus](https://giscus.app/) is configured to go to our [repo](https://github.com/rackn/doc-discussions) in github.

## Headers {#rs_md_headers}

Each file should start with a header similar to the following example.

``` yaml
---
title: This is Also the Doc Title (# This is Also the Doc Title)
tags:
  - writers
  - contributing
---
```

Do not use the top-level heading as it is covered in the header.  Headings should start with '##' and so on.

## Tags {#rs_md_tags}

Tags that are allowed can be found in `mkdocs.yml`.  At least one of the following tags should be used first: `operator`, `developer`, `resources`, `reference`, `explanation`, `howto`.

### Tag Sets {#rs_md_tag_sets}

The tags used are agregated into tag sets.  You can find a breakdown of tag sets in `mkdocs.md`.  The `tags/<tag>.md` entries will take tags listed under them and group any docs with both the tag and any subsequent tags.  This populates the `docs/tags/<tag>.md` documents at build.

## Reference Links {#rs_md_ref_links}

While not part of markdown, RackN docs can generate dynamic links that are build as part of the build process.  These are similar to the Sphinx RST links, but have some specific caveats.

### Marking Sections {#rs_md_marks}

Reference links can only be used on section headers.  These have the form of section marker (`###`), a title (`Marking Sections`), and a mark (`{#rs_md_marks}`).

!!! example "Example - All Put Together"

    ``` markdown
    ### Title of section {#rs_section_reference}
    ```

!!! note

    The section can be of any depth to be marked, but must be of the `#` variety and its sub-peers.

!!! warning

    Section titles that use `:` will have that converted to `-` for tagging and display purposes.

!!! warning

    The RST form if underline and similar section notation is not able to support the marks.

### Referencing Marks {#rs_md_ref_marks}

To reference a mark, you will need to use the link format.  The text inside the `[]` is maintained as the
displayed text.  If it is blank, the title from the reference will be used.

!!! example

    ``` markdown
    This is a [link](#rs_md_ref_marks_bb_warn_ok)

    This is a [](#rs_md_ref_marks_bb_warn_ok)
    ```
    !!! note
        The `_bb_warn_ok` is used to keep the reference from rendering in this example.

    The following is the rendered reference markdown.

    ``` markdown
    This is a [link](#rs_md_ref_marks)

    This is a [](#rs_md_ref_marks)
    ```

Additionally, references can cross the `refs` and `core` boundaries.  The system will convert the references to external https links when referencing marks in the other documentation tree.  This is automatically done.

### Image Helpers {#rs_md_image_helper}

To reference an image in the top-level `images` directory, a link helper has been added, `images#`.

!!! example

    ``` markdown
    ![](images_bb#core_services.png)

    ![ports](images_bb#core_services.png)
    ```
    !!! note
        The `_bb` is used to keep the reference from rendering in this example.
    The following is the rendered reference markdown.
    ``` markdown
    ![](images#core_services.png)

    ![ports](images#core_services.png)
    ```

### Built Images

You can place a `d2` file in the `d2-images` directory and the build will generate a file named the same but with an `svg` extension
in the src/built_images directory.  Use the [helper](#rs_md_built_images_helper) to reference the image.

Running the script, build_images.sh, will generate the images for development.  You can additionally use the [VSCode plugin](https://marketplace.visualstudio.com/items?itemName=terrastruct.d2) to help with editing and previewing the images.


### Built Images Helpers {#rs_md_built_images_helper}

To reference an builtimages in the top-level `built_images` directory, a link helper has been added, `built_images#`.

!!! example

    ``` markdown
    ![](built_images_bb#arch-subnet.svg)

    ![subnet definition](built_images_bb#arch-subnet.svg)
    ```
    !!! note
        The `_bb` is used to keep the reference from rendering in this example.
    The following is the rendered reference markdown.
    ``` markdown
    ![](built_images#arch-subnet.svg)

    ![subnet definition](built_images#arch-subnet.svg)
    ```

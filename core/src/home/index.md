---
tags:
  - explanation
---

# Digital Rebar Platform {#rs_drp}

![](../images/dr_provision.png#right){width="300px"}

*simple, fast and open-ecosystem infrastructure automation with strong
Infrastructure as Code (IaC) design.*

[Digital Rebar (aka DRP)](https://rackn.com/products/rebar/) is a
[RackN](https://rackn.com) licensed-core platform with an open ecosystem
suite of both licensed and open
[APLv2](https://gitlab.com/rackn/provision/-/raw/v4/LICENSE)
components. The fourth generation platform provides a simple yet
complete API-driven DNS/DHCP/PXE/TFTP provisioning and workflow system.

Digital Rebar Platform and Ecosystem are designed together to be a
complete data center provisioning, content scaffolding and
infrastructure workflow platform with a cloud native architecture that
completely replaces Cobbler, Foreman, MaaS, Ironic or similar
technologies. DRP offers a single golang binary (less than 30MB) with no
dependencies capable of installation on a laptop, RPi or switch
supporting both bare metal and virtualized infrastructure.

## Feature Overview {#rs_feature_overview}

### Platform Capabilities {#rs_fo_platform_caps}

-   API-driven infrastructure-as-code automation
-   Multi-boot workflows using composable and reusable building blocks
-   Event driven actions via Websockets API
-   Extensible Plug-in Model for public, vendor and internal enhancements
-   Dynamic Workflow Contexts (allows using APIs when agents cannot be run)
-   Distributed Multi-Site Management
-   Integrated Secure Boot, SSO and Highly Available options.
-   Supports ALL orchestration tools including Chef, Puppet,
    Ansible, SaltStack, Bosh, Terraform, etc

### Open Ecosystem Plugins {#rs_fo_eco_plugins}

-   RAID, IPMI, Redfish, and BIOS Configuration
-   Cloud-like pooling capabilities
-   Classification engine for automated workflow

## Contact {#rs_contact}

-   [RackN Support](https://www.rackn.com/support)
-   Chat/messaging via the Digital Rebar `#community` channel is our
    preferred communication method. If you do not have a Slack invite to
    our channel, you can [Request a Slack
    Invite](http://www.rackn.com/support/slack/)
-   [Issues and Features](https://gitlab.com/rackn/provision/issues)
-   Videos on the [DR Provision
    Playlist](https://www.youtube.com/playlist?list=PLXPBeIrpXjfilUi7Qj1Sl0UhjxNRSC7nx)
    provide both specific and general background information.

## Licenses {#rs_licenses}

Digital Rebar Server is proprietary licensed software available in
compiled binary format from [RackN Rebar](https://rackn.com/products/rebar/).

Code for Digital Rebar client and many catalog componets is available
from multiple authors under the [Apache 2
license](https://gitlab.com/rackn/provision/-/raw/v4/LICENSE)

Digital Rebar documentation is available from multiple authors under the
[Creative Commons
license](https://en.wikipedia.org/wiki/Creative_Commons_license) with
Attribution.

    Work licensed under a Creative Commons license is governed by applicable copyright law.
    This allows Creative Commons licenses to be applied to all work falling under copyright,
    including: books, plays, movies, music, articles, photographs, blogs, and websites.
    Creative Commons does not recommend the use of Creative Commons licenses for software.

    However, application of a Creative Commons license may not modify the rights allowed by
    fair use or fair dealing or exert restrictions which violate copyright exceptions.
    Furthermore, Creative Commons licenses are non-exclusive and non-revocable.
    Any work or copies of the work obtained under a Creative Commons license may continue
    to be used under that license.

    In the case of works protected by multiple Creative Common licenses,
    the user may choose either.

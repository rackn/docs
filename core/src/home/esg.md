---
tags:
  - explanation
---
# Editorial Style Guide {#rs_esg}

Purpose: This style guide helps ensure consistency across technical documentation and other written assets for RackN. This is a living document that can and should be updated regularly.

Manual: Chicago Manual of Style, 17th edition

## Cheat Sheet {#rs_esg_cheat}

The table below summarizes key points from the style guide for quick reference while writing. Developers and editors may use the full document for a more detailed QA review.

| Topic | Details |
| ----- | ------- |
| Tone and Style | **Be clear and concise**. Remove wordiness whenever possible. Start sentences with verbs. Avoid adjectives and adverbs.
|                | **Use judgment-free language**. Do not use adverbs like "simply" or "easily" |
|                | **Be friendly but not overly casual**. Avoid stilted or academic language, but don't write too conversationally. Don't use all caps for emphasis. |
|                | **Keep content as "evergreen" as possible**. Avoid using words like "currently" when necessary, say "as of this writing" but avoid writing content that will be quickly outdated. |
|                | **Use 2nd person voice**.
| Formatting     | **Capitalize DRP objects (Machines, Parameters, etc.) only when introducing or defining them, or referring to a component of the navigation**. Otherwise, refer to them in lowercase. |
|                | **Always use "DRP" never "the DRP"
|                | **Special characters: use only single quotes, double quotes, and the minus sign. Avoid using dashes, hyphenated words, and other special characters unless absolutely necessary;** these are difficult to render and can interfere with code. |
|                | **Categories > Pages > Sections**. As of this writing, there are six main categories within the documentation (Home, Getting Started, Other Installation Guides, Features & Design, Operations, and Resources). Each category features multiple pages. Each page is formatted with sections denoted by H1, H2, and H3 headers. |
|                | **Start each page with a short introduction** describing the purpose of the page and, when applicable, who it's for. |
|                | **When writing docs in an external program (Word or Google docs, for example) use consistent formatting within that doc** for headers, bullets, and ordered lists. It should be easy for the developer to see a clear hierarchy of ideas when reviewing your docs.|
|                | **Choose meaningful anchor text for hyperlinks**. For example, don't link to a single word like "here" or "link." Link to relevant keywords, like "more installation options" or "resource brokers." |
|                | **Use tables** for quick-reference guides, or to house detailed numerical information. |
|                | When including **code snippets**, format them with courier new and highlight them in gray |
|                | **Don't navigate in pages**  Use the nav and table of contents for their jobs |

## Full Guide {#rs_esg_fg}

### Abbreviations {#rs_esg_abbrev}

* Write the full term for the abbreviation on first use, followed by the abbreviation in parenthesis.
  Thereafter, use the abbreviation by itself (without parenthesis) for the remainder of the document.
  Don't capitalize the written term unless the term is normally capitalized or a proper noun.
* In headings, use just the abbreviation.
* If the abbreviation is common (OS, USA, etc.) there is no need to write out the full term.
* The abbreviation should be in parentheses after the written term. For example, operating system (OS),
  application programming interface (API), Digital Rebar (DRP), etc.
* Don't create your own abbreviations, stick to standard or established abbreviations.
* Don't use an abbreviation as a verb.

### Capitalization {#rs_esg_caps}

* Use capitalization for proper nouns. Proper nouns include names of companies, places, people,
  products, protocols, languages, trademarks and some technologies. For example, WordPress,
  RackN, Linux, etc. Common nouns should never be capitalized.
* Use capitalization when it's stylistically required, for example when writing most abbreviations.
  For gigabyte (GB), gigahertz (GHz), Application programming interface (API), etc.
* However, never use capitalization for emphasis, to give a word or phrase greater status than others, or randomly.

### Citations {#rs_esg_citations}

* Use reputable sources and link to them when applicable. Don't quote Wikipedia articles and avoid linking to competitors.

### Code blocks and separation blocks {#rs_esg_codeblocks}

When creating code as input or output, follow these guidelines:

* Don't use screenshots; format code as blocks.

``` bash title="Install DRP with universal content"
curl -fsSL get.rebar.digital/stable | bash -s -- install --universal
```

* Format inputs and outputs in separate blocks. where necessary, explain parts of the code
  such as parameters or arguments so the reader knows what to expect once they run that code. see the example below:

``` bash title="Remove installed DRP"
curl -fssl get.rebar.digital/stable | bash -s -- remove #(1)
```

1. That remove will not remove the data files stored in /var/lib/dr-provision, /etc/dr-provision, or
   /usr/share/dr-provision. include the --remove-data flag for a full clean-up.

These are [code blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#usage) with an
[annotation](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#adding-annotations).

Code blocks can specify titles, line numbers, and highlighted regions.

### Contractions {#rs_esg_contractions}

* To avoid an overly formal tone, use contractions. Avoid contractions such as let's or others that can
  be misread as other words. Avoid nonstandard contractions such as that'll, mustn't, mightn't, could've and should've.
* Be consistent. Avoid alternating or mixing contractions with their full counterpart within the same document.
* Don't use contractions with interrogative words such as what, where, how, when, who and why.
* Don't use contractions that combine a verb and a noun, for example, "A quick start system'll have some initial configuration already done"
* Don't use contractions that feature a company, product name or trademark. For example, "No connection to RackN's required after the download"

### Dates {#rs_esg_dates}

* Use the format month, day, year and spell out the month to be clear to international audiences.

    For example: _September 01, 2022_

* Don't use ordinal numbers, **Don't say**: September 1st; use September 1 instead.
* When you add the month, day, and year in a sentence, use the standard date format
  (as described earlier) and add a comma after the year. For example: "September 1, 2021, RackN has released Digital Rebar v4.7 for general use"
* If you include only the month and year, remove commas.

    For example: in September 2021 RackN released Digital Rebar v4.7 for general use.

* If a document requires you to use an all-numeric date format, explain the format before-hand
  and use it consistently throughout the document. For example, if you use 09/01/2022 it may be
  interpreted to mean January 09, 2022 or September 01, 2022. To avoid confusion, describe the
  format used as mm-dd-yyyy. Preferably, use the ISO 8601 format for all-numeric dates which is
  yyyy-mm-dd (2022-09-01).

!!! note

    Do not apply these changes to code snippets, since they may be specific to a command set.

### Email Addresses and Domains {#rs_esg_emails}

If you need to show a domain example, use domains reserved by the Internet Assigned Numbers Authority (IANA)
for use in examples. Example domains are "example.com" and "example.org."

For an example email address, use yourname@example.com. If you need to use an actual email address,
don't expose user's (or your) personal information in the example.

### File Types {#rs_esg_file_types}

When creating RackN content, you'll be required to mention different types of files in the text.
We recommend the following file naming conventions:

* **File's generic name**: for example configuration file or initialization file
* **File's standard abbreviation**: for example,  XML file or PDF file
* **File name extension**: for example .zip file

Here is how to refer to files.

First, does the file have a generic name or standard abbreviation?

* If YES, use that and be consistent through the article.
* If NO, use the file name extension. You may also have to use the file name extension if the standard abbreviation or generic name doesn't fit the context.

Below, find guidelines for formatting file names in RackN text:

| File Type | Guideline | Example |
| --------- | --------- | ------- |
| Configuration | Use configuration files unless you're referring to a specific file. | If a configuration file is present, this file will be scanned for an Endpoint. |
|               | However, in most script/code it should be *config file* | __Start with config file from bottom mkdir mtls__ |
| JSON | Use JSON unless you're referring to a specific file. | Create a new profile with the passed-in JSON or string key |
| | | You can bypass this step in subsequent resets by uploading the rackn-license.json file via the CLI. |
| YAML | Use YAML unless you're referring to a specific file, in which case the extension is .yml or .yaml. | YAML is more suited for configuration files than JSON because it's easier to read and understand. |
| Zip | Use zip for general references and .zip when referring to a specific file. | Any tar zip files will automatically be unzipped, and untarred in the directory you specify. |
| | | The above command will upload the my.zip file to the file's mypath location. |
| XML | Use XML unless referring to a specific file. | This file will be XML data that can be uploaded to your identity provider. |
| | | This metadata.xml file can be upload to your identity provider |
| HTML | Use HTML unless you're referring to a specific file. | Use a terminal window with a watch function to rebuild the HTML document tree. |
| | | ISOs can be downloaded from: https://mirrors.almalinux.org/isos.html |

### IP Addresses {#rs_esg_ips}

* Don't use IP by itself; always use IP address. Note that IP is a standard abbreviation, and for that reason, don't spell out IP on the first use.

### Links and Cross References {#rs_esg_links_cross_refs}

Cross references help the reader navigate an article and find content related to what they're currently reading.
Depending on the location of the cross reference, it can be linked or not linked:

* **When writing docs in a word processor, mark headers or phrases you want to reference internally** by adding a functional hyperlink.
  If a functional hyperlink doesn't exist yet, add a comment describing where the link should go.
* **Don't link the cross reference if you're referring to content within the same section or article** such as examples,
  figures, subsections or tables. Instead, add a simple textual cross reference.
* **If you're referring to a third-party source or another RackN article, include a link to that content**.
   Make sure the article you linked to is up-to-date and that the link itself is active.

The following tips will help you create cross references consistently throughout the article:

* Give the reason for the cross reference at the onset. In other words, open the cross reference
  sentence/paragraph with an explanation of the purpose of the cross reference. The explanation
  should primarily help the reader decide whether or not to open the linked source.
* When referring to a section in the same article, use **following and preceding**. Steer clear of words
  such as aforementioned, above, below, later, or earlier.
* Ensure the anchor text sufficiently describes the link source. The reader should know the destination
  of the content or the information they're going to get from the linked source. In this regard, keep the following in mind:

  * Don't use vague anchor texts such as _more information_ or _click here_.
  * For links at the end of the topic or article, use the title of the linked source as the anchor phrase.
  * For inline links, choose three or four words of the content as the anchor phrase. Ensure these words best describe what the reader will get from the source.
  * If no words can best describe the destination content, add a cross reference sentence and use the title of the destination content as the anchor phrase.
  * Don't enclose the anchor phrase in quotation marks.
  * Don't link to the same source more than once in a topic or article.

### Lists {#rs_esg_lists}

When creating lists, consider whether it should be ordered or unordered. Ordered lists are
numbered and the items must be added in a particular order. 

!!! example "An ordered list with an introductory text"

    _The default drp-community-content pack contains the following BootEnvs:_

    1. _centos-8-install: CentOS 8 (most recent released version)_
    1. _centos-7.4.1708-install: Centos 7.4.1708 (this may change as new versions are released)_
    1. _ubuntu-18.04-install: Ubuntu 18.04_
    1. _debian-8: Debian 8 (Jessie) version_
    1. _debian-9: Debian 9 (Stretch) version_

On the other hand, unordered lists should not follow a particular order and can be delineated by bullets.

!!! example

    The full version string generated by the build tool looks like this:

    * v4.0.1 - A stable patch release
    * v4.0.2-dev.1+gd012b1d6ba892c96c81c24ee93b668591663ca5c - A development tip built at this commit. A potential v4.0.2 candidate.

Provide introductory text to help segue into the list. When creating the introductory content, consider the following:

| Guideline | Examples |
| --------- | -------- |
| End the introductory text with a colon | You can use RackN to accomplish the following tasks:|
| Use the verb include to denote a partial list. Don't end partial lists with words such as to name a few. | Some of the tasks you can perform with RackN include: |
| Avoid quantifying list items in the introductory sentence. The quantifier could jeopardize the integrity of the content should the listed items increase or decrease in future. | **Say**: The following RackN features are available: |
| | **Don't say**: The following four RackN features are available: |
| Don't use weak verbs like "do" in the introductory text. Instead, use stronger and more meaningful verbs such as "perform." | **Say**: You can use RackN to perform the following tasks: |
| | **Don't say**: You can use RackN to do the following tasks: |

For the list's items, follow these guidelines:

| Guideline | Examples |
| --------- | -------- |
| Consistently format introductory sentences in a list. List items should be parallel. In other words, if the first item starts with a fragment, the rest should follow suit. However, you can have a mix of fragments and sentences, provided all items start with fragments. | Using a gerund: |
| | Verifying the source code. |
| | Checking the parameters. |
| | Entering the command. |
| | |
| | Or: |
| | |
| | | Verify the source code. |
| | | Check the parameters. |
| | | Enter the command. |
| Capitalize the first letter of the item, unless the first letter must be lower case. | *Hyper-V |
| | KVM |
| | AWS |
| | | 
| | Or |
| | | 
| | vCloud |
| | ESXi |
| | iPhone |
| When punctuating fragments, use no punctuation at the end of each item. However, if items are sentences, use punctuation at the end of each sentence. | User's Browser |
| **Don't** begin listed items with articles (the, a, or an); nouns or verbs are best. | RackN Portal UX |
| | API call on 8092/upgraded to websocket |
| | |
| | Or |
| | |
| | Obtain the address. |
| | Visit the URL. |
| | Click the button. |
| If you're using a list to describe a connection path or a procedure, use an ordered list and add → at the end of each item, except the last. | The connection path is as follows: |
| | User's Browser –> |
| | RackN Portal UX –> |
| | API call on 8092/upgraded to websocket –> |
| | DRP Endpoint –> |
| | localhost:4822/tcp connection –> |
| | guacd service container –> |
| | ssh to server in question |

### Names {#rs_esg_names}

When writing examples, avoid using real or copyrighted names. For RackN product names and version numbers, follow these guidelines:

* Always spell out and capitalize RackN (note the first and the last letter are capitalized).
* After writing a full product name, you may use an abbreviation if one exists — for example, Digital Rebar Platform, then DRP thereafter.
* If you're creating API documentation, ensure the correct version number in the documentation.

### Notes and other Notation Types {#rs_esg_notes}

Notations are crucial when creating technical documents because they help highlight helpful or important information.
As important as they are, use them sparingly. Use the following guidelines:

* You should use an [admonition](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) unless you can't.
* The notation should be as close as possible to the information it calls out.
* The notation word (**Note**, **Tip**, **Important**, or **Warning**) should be in bold.
  The word should be followed by a full colon and then the notation text in regular font.
* Don't bombard users with notations of the same type. For example, don't follow a Tip with another Tip. Instead use an unordered list or separate paragraphs

Here's how to use different notation types in the article:

!!! note 

    Use it to supplement or emphasize information in the text.

!!! important

    Highlight the most important or essential point of discussion.

!!! warning

    Use it to alert readers of the consequences or hazards of taking certain steps or processes. You
    could warn them of situations they could lose data, disrupt operations or compromise integrity for failing to follow instructions.

!!! tip

    Use this to call out useful information that can help the reader improve processes or
    product performance. It's also used to highlight alternative ways of doing something.
    It also helps draw the attention of readers to shortcuts, procedures or techniques.

Admonitions can also have titles and annotations as well.  Use them to spice up the message or progressively disclose information.

### Numbers {#rs_esg_numbers}

When referring to numbers, spell out any value between zero and nine. If the number is 10 or bigger than 10, use the number.
However, there are some exceptions:

| Exception | Example |
| --------- | ------- |
| Time | 5:45 p.m. |
| Numbers showing dimensions | 3X3 inches |
| Numbers to use as inputs | Type 2 and hit Enter. |
| Numbers showing a software version | It's Debian 8 (Jessie) version not Debian eight (Jessie) version |
| Numbers with symbols | 3% |
| Series of similar items where one of the numbers is 10 or greater. | Unit A requires 3 nodes, Unit B requires 11 nodes, and Unit C requires 7 nodes. |
| Number showing units of measurements or abbreviations | 3mm, 5-inch disk |

In addition, use the following guidelines when writing numbers:

* Don't begin a sentence with a number. If you must begin the sentence with a number,
  spell out the number, unless the number is part of a company, product or service name
  or it qualifies as an exception in the table above. For example, it's correct to say:

    * **Correct**: Ten companies, including RackN, run the world…
    * **Correct**: 99 Digital Research Group audited RackN based on the following parameters

* If a number has five or more digits, use commas to separate the digits. But there are exceptions.
  Don't commas in the following types of numbers, even if they have more than five digits: Page numbers,
  Addresses, literal representations of displayed values or user-entered values, and decimals.
* When writing ranges of numbers, use through to describe an inclusive number range.

      Example: Step 7 through step 11

* If space doesn't allow for that, use en dash instead:

    Example: Step 7-12

* When using "from" or "between" to describe a range, don't use en dashes. Instead, use words to describe the range:

| Use | Don't use |
| --- | --------- |
| From 20 to 30 diagrams | From 20 - 30 diagrams |
| Between 2022 and 2023 | Between 2022 - 2023 |

* Use _n_ (in italics) as the variable when describing a generic or unspecified number.
* Use _x_ (in italics) for each digit in an unknown or unspecified version number.


### Placeholder (variable) text {#rs_esg_placeholders}

Placeholder text refers to an object (account, server, etc) whose name is unknown to RackN.
When writing the syntax for a path or command, placeholder text indicates the type of element a user should include.

For example, **directoryName** instructs the user to add the name of a directory in a command.
When adding placeholder text, follow these guidelines:

* Write placeholder text in italic if it's within regular text and a code sample.
  If you're unable to format text in code samples, enclose the placeholder text in curly brackets
  or angle brackets. Keep formatting consistent throughout the article.
* Write the placeholder text in lowercase. However, when writing multiple-word placeholders, use
  camelCase -- capitalize the first letter of words after the first word. Don't capitalize the first
  word or separate the words with symbols or spaces. For example, use serverName instead of servername. Use
  directoryName not Directoryname.
* The placeholder text should be descriptive and meaningful. In that regard, don't use short form terms as placeholders:
  !!! example

      Use **device** (instead of *dev*), **installationDirectory** (instead of *installDir*).

Explain any placeholder that's not common to the reader or one that might not be understood.
When explaining placeholder text, use the following guidelines:

* Don't use stand-alone phrases or sentence fragments. Explain what the placeholder is and what the
  user should expect once they run the command.

!!! example

    ```bash
    drpcli plugins create /some/path/foo.yaml
    ```

    Where "some/path/foo.yaml" is a YAML (or JSON) formatted configuration that sets the Provider to type ad-auth, and sets appropriate Params values. See the Example Configuration section below for more details.

* When describing more than one type of placeholder text, use an unordered list.

!!! example

    ```bash
    GOOS=linux GOARCH=amd64 tools/build-one.sh cmds/ipmi
    ```

    Cross-compiling with Golang is very easy, and it is controlled with a few basic environment variables:

    * GOOS - defines the Operating System target to build for
    * GOARCH - defines the platform architecture to build for

* Ensure the placeholder in regular text shares identical formatting with the one in the code or command. For example, if the placeholder
  in the code is written in Courier New font + italics + dark gray 1, use the same format when explaining it.

!!! example

    ```bash
    drpcli plugins create /some/path/foo.yaml
    ```

    Where "some/path/foo.yaml" is a YAML (or JSON) formatted configuration that sets the Provider to type ad-auth, and
    sets appropriate Params values. See the Example Configuration section below for more details.

### Plurals {#rs_esg_plurals}

When using plurals use the following guidelines:

* When writing plurals, for brevity, avoid using (s), /s, or /es to indicate the possibility of more than a single item.

!!! example

    **don't say**: Close any system (s) that is/are open.
    
    **Say**: Close any system that is open or Close systems that are open.

* For acronyms, abbreviations or numbers, add lowercase s to denote plurals. Don't add an apostrophe.

!!! example

    CPUs, IDs, 2000s, and APIs

* Don't add "s" if an acronym already represents a plural. For example, use FAQ, not FAQs.
* When forming a plural of a symbol or single letter, add a lowercase "s" and an apostrophe.

!!! example

    \#'s, $'s, or X's.

* Don't add lowercase "s" or an apostrophe when forming plurals of abbreviated units of measurements.

!!! example

    Use 3mm (avoid mm's) or 45 min (avoid mins).

### Possessives {#rs_esg_possessives}

When showing the possessive form of company name, product name, abbreviations, or
inanimate objects, use a prepositional phrase rather than a letter "s" and apostrophe.

!!! example

    **Say**: The layout of the API is intuitive
    **Don't say**: The API's layout is intuitive

### Prepositions {#rs_esg_prepositions}

Place the preposition where it makes most sense, even if that means adding one at the end of a sentence.

!!! example

    **Say**: To learn more, see the documentation for the application you're interacting with.
    **Don't say**: To learn more, see the documentation of the application with which you're interacting.

### Punctuation {#rs_esg_punctation}

Text should be punctuated correctly and consistently, using the following guidelines:

#### Ampersands (&) {#rs_esg_ampersand}

* Don't use ampersands in the text or headings in place of and, unless the ampersand is a symbol on the UI.

#### Colons (:) {#rs_esg_colon}

* Use a colon when introducing a list, table, figure, or example.
* To introduce substeps, bullet lists, or code that the user needs to enter, place a colon at the end of the step.
* Use a colon to separate the initial phrase from the text that follows it in a list item. (Change to admonition if possible)

!!! example

    !!! note 

        The Digital Rebar Platform (DRP) resources (Machines in DRP parlance) can run on physical, virtual, cloud, or container instances.

* Column headers, titles, and headings in tables should not be followed by a colon.
* If a heading or title has a colon, capitalize the first letter after the colon, regardless of its part of speech.

#### Commas (,) {#rs_esg_comma}

* Use a serial comma in a series of three or more items. In other words, place a comma immediately after the penultimate item in the series. Oxford comma!

!!! example

    The API, File Server, DHCP, BINL **,** and TFTP ports can be configured

* If you use a comma to join independent clauses, use a conjunction after the comma.

!!! example

    Click Enter to run the command **, and** then click Allow Fast Save

* Add a comma to start a clause that begins with which (nonrestrictive clause),
  but don't use a comma to start a clause that begins with that (restrictive clause).
* If you have to add a comma after a quotation in the text, add it inside the closing quotation mark.

#### Dashes and Hyphens {#rs_esg_dash}

Avoid using dashes and hyphens in documentation. Instead, break longer sentences into two parts,
use a colon or semicolon if grammatically correct, or use the word "through" to describe a range.
Where you would normally use a compound modifier with a hyphen, ("cloud-only" for example) rework
the sentence to remove the hyphen: "the installation process is cloud only."

**Exceptions**: you can use a hyphen when it is part of an official product name or word. For example, "64-bit" or "re-create."

#### Exclamation Marks {#rs_esg_excl_mark}

Don't use exclamation marks unless they are included in a code snippet.

#### Parentheses () {#rs_esg_parens}

* Don't use parentheses in running text, because they disrupt the flow of sentences and
  distract the reader from the crux of the statement.
* However, there are some special cases where parentheses are absolutely necessary, such as to:
  * Indicate a special character
  * Show an abbreviation
  * Show examples
  * Define a phrase that qualifies a title, step or term

#### Periods (.) {#rs_esg_period}

* Add a period at the end of each sentence and insert a single space after the period.
* If the sentence ends with a quotation mark, put the period inside the closing quotation mark.
* When listing items, use a period if:
  * All the items are sentences.
  * Some or all items are fragments followed by sentences.
* Add periods to abbreviations that could be misread.
* Add a period (with no space) before a file name extension.
* No periods in title and headings

#### Quotation Marks {#rs_esg_quotes}

* Use double quotes sparingly, and only when calling attention to a specific item, or
  conveying spoken words from a source. Reserve single quotes for code, or within double
  quotes to call out a specific word. Do not use quotation marks randomly or for emphasis.

### Tables {#rs_esg_tables}

Use flat tables and keep them simple and straightforward (for example, avoid adding images,
lists, or tables inside of tables). Use the first row and first column for headings, when applicable.

### Telephone numbers {#rs_esg_telephone_numbers}

Use the format XXX-XXX-XXXX when writing domestic telephone numbers, and use the country code,
preceded by a plus sign, when writing international telephone numbers.

### Time {#rs_esg_time}

Use lowercase a.m. and p.m., with periods. (For example, 5:30 a.m.) If it's an exact hour, no "00" is required (for example, 3 p.m.).

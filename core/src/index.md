---
title: Documentation
---

# Documentation {#rs_home}

<div class="grid cards" markdown>

-   :material-clock-fast:{ .lg .middle } __Set up in 15 minutes__

    ---

    Install DRP and get up and running in minutes

    [:octicons-arrow-right-24: Getting Started](getting-started/index.md)<br/>
    [:octicons-arrow-right-24: Trial](getting-started/trial.md)

-   :fontawesome-solid-house:{ .lg .middle } __Architecture - learning starts here__

    ---

    Learn the features and methodolody of DRP.

    [:octicons-arrow-right-24: Architecture](arch/index.md)

-   :material-home-automation:{ .lg .middle } __Developers - building the automation now__

    ---

    Build your own automation and integrations for DRP

    [:octicons-arrow-right-24: Developers](developers/index.md)

-   :fontawesome-solid-train:{ .lg .middle } __Operators - driving the system__

    ---

    Operator DRP for maximum effect.

    [:octicons-arrow-right-24: Operators](operators/index.md)

-   :material-scale-balance:{ .lg .middle } __Resources - information on demand__

    ---

    Release information and FAQs for quick answers to specific questions.

    [:octicons-arrow-right-24: Resources](resources/index.md)

-   :material-scale-balance:{ .lg .middle } __References - specific details__

    ---

    Reference documentation on each workflow, stage, task, parameter in the system.

    [:octicons-arrow-right-24: References](https://refs.rackn.io)


</div>

## Documentation Styles {#rs_doc_styles}

RackN documentation follows the the [Diataxis](https://diataxis.fr/) and [Divio](https://documentation.divio.com/) methodology.  A consise summary
is that there are four types of documentation.

* :fontawesome-solid-book-open-reader: [Explanations](tags/explanation.md)- Articles or sections that describe the intent and methodology of a part of the product.
  These provide information from a perspective understanding the product.
* :fontawesome-regular-clipboard: [How-To](tags/howto.md) - Articles or sections that describe how something could be done.
  These provide methods for implementing features from a perspective of learning how to extend or expand
  the product.
* :fontawesome-solid-list: [Tutorials](tags/tutorial.md) - Articles or sections that describe the specific actions to do something with the product.
  These provide steps or actions to a specific thing from a perspective if operating the specifics of the product.
* :fontawesome-solid-book-atlas: [References](tags/reference.md) - Articles or sections that describe the specifics of a part of the product.
  These provide information from a perspective of looking up a fact or specific detail of the product.

The articles are tagged and grouped to make it easier to see there fit into the scheme.

## Documentation Audiences {#rs_doc_audiences}

Documentation needs an audience.  RackN Documentation defines three audiences.

* :fontawesome-solid-house: [Architecture](tags/architecture.md)- Articles or sections that are intended for architectural discussions.
  These are generally *explanation* articles intended for learning about how the system works and why.
* :fontawesome-solid-train: [Operator](tags/operator.md) - Articles or sections that are intended for the operators of the product.
  These focus on operating, maintaining, and controlling the product.
* :material-home-automation: [Developer](tags/developer.md) - Articles or sections that are intended for developers that are building new
  automation, extending current tooling, or integrating the product into their environment.

The articles are tagged and grouped to make it easier to see there fit into the scheme.

## Using the Documentation {#rs_doc_usage}

The tags help direct you understand the audience and content style, but search provides the methods to find what you are looking for.  A couple of tips
can help get more from your searching.

!!! tip "tags are included"

    Tags are included in the search indexes.  By using a tag, you will get those pages that have that tag listed first in the search list.
    It allows for a fast filtering.

!!! tip "+ means required"

    By prepending a `+` to the term, it makes the term required for the search.  Using this with tags, allows for restricting the first results
    to just that set of pages.  Adding additional terms makes the search more specific.

    !!! example

        ```
        +install +aws
        ```

        Will return first in the list, installation pages for AWS.

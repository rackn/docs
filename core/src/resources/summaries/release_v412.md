---
title: v4.12 [July 2023]
tags:
  - reference
  - operator
  - release
---

Release Date: June 27, 2023

Release Themes: Platform Team Enablement

Executive Summary: Performance and usability improvements including multi-site management, workorder pooling (aka Batch Operation), connection tracking and UX performance. Additional items included the addition of Cisco and Supermicro as a supported hardware OEMs.

## Important Notices

New for this release:

- v4.12 Sledgehammer has been updated for multiple platforms including ARM and Ubuntu and requires an updated ISO
- v4.12 Billing Plugin integrated into Core. If installed, it can be safely removed
- v4.12 ISOs storage is now in a Digital Rebar managed read only file system instead of being mapped to the endpoint's local storage. If needed, it can be disabled by setting `RS_USE_ISOFS` to false.

From prior releases:

- v4.11 Discovery image now defaults to Alma Linux. Users upgrading Digital Rebar should also install and migrate to this new Sledgehammer.
- v4.11 Client certificate authentication is now off by default in v4.10 and v4.11. To enable, add the **--client-cert-auth** flag or the `RS_CLIENT_CERT_AUTH=true` flag to the systemd configuration space. install.sh has been updated for these options.
- v4.10 Debian-8 has been removed from the content pack.
- v4.10 To enable/disable read only Triggers, add the parameter trigger/\<name of trigger\>-\[enabled\|disabled\]: true in the global profile. Search workorder trigger.
- v4.10 Fixed critical bug in event registration that caused system crash if request is malformed. This is a recommended dot upgrade for previous releases and was back ported to v4.6 and later releases.
- v4.10 Interactive labs can be used to explore and learn the latest Digital Rebar features.
- v4.10 EventToAudit plugin allows any event to be mapped to an audit log item.
- v4.9 Digital Rebar cannot run in a container when using features including Contexts, Brokers, Clusters, Multi-Site Manager and High Availability. These features all rely on Digital Rebar managing both its own lifecycle and driving containers. Running in a container prevents Digital Rebar from performing operations required for these features.
- v4.9 work_order_templates (preview in v4.8) have been renamed to be blueprints.
- v4.9 removal of the Centos mirrors require users to use an archival Centos8 mirror for Sledgehammer8.
- v4.9 format for content pack meta data changed, you must use the v4.9 drpcli to bundle v4.9 content.
- v4.8 Workflows relating to Clusters and Cloud_Wrappers have been significantly changed. Operators using those patterns should carefully review their implementation before upgrading.
- v4.8 Cleanup operation offers safer option than destroy by enabling pre-destroy actions by defining **on-delete-workflow**. Operators should migrate destroy to cleanup where possible.
- v4.8 Portal UX editing panels on several screens have been significantly changed. UXViews ability to limit machine field view/edit is no longer enabled in v4.8.
- v4.8 Context Containers need to be rebuilt for v4.8+ Docker Context so as to NOT inject a DRPCLI copy. To ensure that the DRPCLI version matches the DRP endpoint, the Docker Context will now automatically attach the correct DRPCLI to containers during the starting process. Search 4.8 contexts.
- v4.8 added Workflows relating to Clusters and Cloud_Wrappers have been significantly changed. Operators using those patterns should carefully review their implementation before upgrading.
- v4.7 added port 8090 to the list of ports required for provisioning operations. Please verify that port 8090 (default, this can be changed) is accessible for Digital Rebar endpoints.
- v4.7 changed the install zip format, the API-based upgrade of DRP to v4.7+ requires usage of most recent <https://portal.RackN.io> (v4.7 for self-hosted UX users) or the use of DRPCLI v4.6.7+. The v4.7 `install.sh upgrade` process also includes these changes.
- v4.6 changed managing of signed certificates, the process for updating and importing certificates has changed to require using the DRPCLI or API to update certificates. See [Certificate Operations](#rs_cert_ops) for details.

### Vulnerabilities {#rs_release_v4_12_vulns}

None known

### Deprecations {#rs_release_v4_12_deprecations}

The Billing Plugin is no longer required in v4.12 and can be safely removed without data loss. If left installed, it will be automatically disabled.

### Removals {#rs_release_v4_12_removals}

None known

## FEATURES

### Improved documentation and [new source location](https://gitlab.com/rackn/docs)

We have migrated the rebar documentation away from [Read the Docs and RST](https://provision.readthedocs.io/), in favor of a more comprehensive and searchable format. This refactoring includes numerous updates, enhancements, and improvements to our current documentation set. Expect additional search layout improvements to provide better accessibility for all types of users.

The documentation has also migrated from the [Digital Rebar Provision repo](https://gitlab.com/rackn/provision/-/tree/v4/doc) to a [dedicated RackN docs repo](https://gitlab.com/rackn/docs).

### New Hardware Support: Cisco and Supermicro

Cisco has been adde as a supported OEM hardware vendor. This integration leverages Cisco's own libraries and APIs to provide full-featured out-of-band control and update capabilities for Cisco-managed hardware. The primary target of this integration is rack mount hardware; however, we anticipate all Cisco compute platform will be compatible.

Supermicro has been adde as a supported OEM hardware vendor. This integration leverages Supermicro's own tooling including Redfish integration.

### Worker Pools for Work Order Runners

Digital Rebar Work Order system has been significantly expanded for scaling out Work Orders for general use cases. These new capabilities allow operators to schedule large numbers of work orders against a pool of workers rather than relying on the operator to identify specific machines or filters. This allows operators to perform large, long and frequent requests without blocking system operation. It also makes it easier for large items to be decomposed into smaller units then scheduled in the Work Order system.

Since Work Order workers are decoupled from the Digital Rebar server, they can operate on virtual, physical or cloud machines. This dramatically expands scalability of the whole system because Digital Rebar can perform, scale and run operations over a dedicated and flexible set of workers. This general purpose feature enables the Hardware Audit feature anticipated for v4.13

Additional Resources:

- [Batch Operations System Architecture](#rs_batch_architecture)
- [Batch Operations Environment](#rs_batch_operations)
- [Context Worker Architecture](#rs_context_worker_architecture)
- [Context Worker Operations](#rs_context_worker_operations)

### Improved Job Log Rendering and Helpers

To improve usability of job logs, Digital Rebar has several new helper functions that can be used to provide in UX navigation, color coding and syntax highlighting. Only tasks that match the syntax will take advantage of this new functionality. RackN recommends that automation developers take advantage of these capabilities when creating new tasks.

In addition, the UX will now highlight these keyworks with special colors and flags.

These markers can be accessed by including the following sytax in job logs `ux://[object]/[id]` or including the `task-helpers.tmpl` template then using the new `job_[debug|info|warn|error]` functions or using the prefix `DEBUG`, `INFO`, `WARN`, `ERROR` or `FATAL` in your echo commands.

### API Connection Tracking and Runner Status Tracking

Digital Rebar relies extensively on active connections to API and Websocket events for system operation. To improve operators ability to track these connections, v4.12 added an API that enumerates all active API connections. In addition to auditing activity in the system, it provides "machine up" status by tracking the runner's API connection.

This information is used to enhance the machines UX by showing if machines are a connected or not. Disconnected machines indicate that the running cannot accept instructions and the machine cannot be managed by Digital Rebar.

In addition to exposing connect status in the machine pages, the UX provides a new Connections table. This new view allows operators to track all active connections including when they were created and how owns the connection. Connections reflect all API and Websocket interactions and not just the runner.

### General UX Improvements (applies to all DRP versions)

- Job History Tab on Machines
- Adding troubleshooting steps for Guacamole
- Implementing priority ordering for ux-view selection
- Adding invisible character warning for templates
- Improving ad-hoc param searching
- Classifier view rewrite
- "Relations" tab in params, tasks, and stages to view referencing objects.
- Log colorization when a line starts with (FATAL, ERROR, WARN, INFO, DEBUG, or Success)

### Improved large file storage (aka ISOfs or ROFS)

Digital Rebar must routinly maintain large bootenv files (>2 Gb) such as ISOs, TARs other binary artifacts. This can lead to a disproportionate use of storage since the platform must often expand and extract the content of these files multiple times. Release v4.12 addresses this problem by storing these files in a managed read only file system (ROFS) instead of direct to the server's file system. This allows Digital Rebar to limit the storage requires from duplicating large files multiple times.

This technology is automatically integrated into Digital Rebar. For users of previous versions, Digital Rebar will no longer map ISOS into `/tftpboot` storage directly to disk so the API will required to manipulate stored files.

If needed, it can be disabled by setting `RS_USE_ISOFS` to false.

You can review the underlying technology for this new storage mechanism in <https://gitlab.com/rackn/rofs>.

### Terraform Provider moved to Hashicorp Repo and Updated

The updated Digital Rebar Terraform provider leverages Pools and Infrastructure Pipelines. It is the easiest way to run Terraform on bare metal infrastructure. The DRP provider is part of the [Hashicorp Terraform Registry](https://registry.terraform.io/providers/rackn/drp/latest) which makes it even easier to access!

Additional resources:

- Video: <https://youtu.be/yH4vm3GTePM>
- Blog Post: <https://rackn.com/2023/06/06/terraform-bare-metal-made-easy-with-rackn-and-hashicorp/>
- Source Code: <https://gitlab.com/rackn/terraform-provider-drp>
- Documentation: [Provider Goals & Design Considerations](#rs_terraform_provider_design)

### Billing Plugin Integrated into Core Digital Rebar

Simplify bootstrapping for new licensees and enterprise cities with integrated data collection and billing plugin. The core platform provides helpful reporting and analysis tools for all operators to quickly see historical activity within the system. This feature is available for PAYGO, POC, pilot, and Hema users, and also automatically submits daily reports for syncing with enterprise users.

Users can use the Activities table to review the tracked data. This will be available for v4.12+ users, but the data is available historically on earlier versions if you had the Billing plugin installed.

The previous `runaction billing-report` and `runaction submit-report` commands have been retained for backwards compatability.

## Items of Note / Commit Summary

The following release summary was generated from the commit logs. It should be used as a guide rather than an authoritative source.

1. Refactor
    * Refactor UX for better templates for base tasks using new functions.
    * Consolidate varying log lines into one component
    * Rename unrelated LogLine component.
1. Features:
    - Split exit code into sane-exit-code format and add icons.
    - Add support for {{.ProvisionerURL}}.
    - Add more line detailing for job logs.
    - Render admonitions in markdown.
    - Show active non-object pools in the table.
    - Add support for markdown doc rendering.
    - Add nested machines table tab.
    - Allow custom string separators for string array values.
    - Add labels under the resource name to denote state.
    - Add Run workflow bulk action.
    - Add support ticket link buttons in headers.
    - Allow stage editing while workflow is unset.
    - Support route overwriting.
    - Update Universal Application bootenvs.
    - Add RHEL Server 9.1 profiles.
    - Add 9.1 support for alma.
    - Update dr-server install to use a partition layout.
    - Add CentOS Stream 8 and 9 bootenvs and support.
    - Add parameters/collector for Display gohai data.
    - Taask wait, DVS, example enhancements in vmware-lib
    - part-scheme-default changes in drp-community-content
    - bootstrapping support for context container in napalm
    - PhotonOS bootstrapping for container engine in task-library
    - Re-add Documentation and cleanup meta.yaml in napalm
    - Add task breakout template and param in drp-community-content
    - Add VMware vSphere Resource Broker (PXE only) in cloud-wrappers
    - Add napalm to published tree pieces
    - Image-builder: adding a builder for alma linux 8
    - Worker pool: update the triggers that can run in containers to use the event-worker-pool
    - Agent: expose job UUID to running jobs as RS_JOB_UUID
    - Static: add static command for direct static file interaction
    - Archive: add drpcli archive commands
    - Roles: add event as an RBAC option
    - Info: add fields in models.Info to track additional ports
    - Add icon color for Machine creates in clusters.
1. Fixes:
    - Update JSON editor when fields change.
    - Fix single frame lag on JSON editor.
    - Named import fixes.
    - Move shared routine into API.
    - Fix support calendar offset.
    - Various backlogged bug fixes.
    - Add filter field to editor.
    - Make param number editors not full width.
    - Fix missing Add Parameters header in pools editor.
    - Only run bulk actions on visible rows.
    - Fix navigation issues that could arise from UX view overwrite.
    - Fix routes test not having Redux context.
    - Fix missing variable in clusters and brokers.
    - Fix typo and dependency order.
    - Allow join-up.sh to run in $HOME and fall back to /tmp.
    - RST to Markdown docs conversions.
    - Add checks to ensure cluster machines are not InUse before destroying the cluster.
    - Fix tick mark direction and pool InUse orphan preventer.
    - Add more timeouts and status checks around iLO calls.
    - Handle error codes better and fix Python/iDRAC.
    - Add Dell Redfish updater.
    - Fix bootstrap container to reference Docker/Podman check correctly.
    - Logout added to HPE commands and print results in flash
    - Ubuntuhammer: remove cloud init removal, fix errors, and add iso info for release
    - Sledgehammer: python selector in the path
1. Performance improvements:
    - Reduce marshalling overhead when setting params via the API calls
1. Integrations:
    - Terraform Provider for Digital Rebar available via HashiCorp repos
    - RackN publishes an npm module for the Digital Rebar API under @rackn/digitalrebar-api

## Roadmap Items (unprioritized, planned for future release)

- Out of Band Machine Audit - Scan machines via Out of Band APIs using work order system to perform hardware audits.
- Agentless ESXi Install - Required for ESXi v8, This will replaces of the existing ESXi agent (drpy).
- Integrated Simple DNS - provide DNS services for networks
- Restricted Access ISOs - allow authenticated users to download non-public ISOs managed by RackN

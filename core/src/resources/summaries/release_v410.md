---
title: v4.10 [August 2022]
tags:
  - reference
  - operator
  - release
---

Release Date: August 9 2022

Release Themes: Orchestration & Usability

In addition to bug fixes and performance improvements, the release
includes several customer-driven features.

Important Notices {#rs_release_v4_10_notices}
-----------------

New for this release:

-   v4.10 Debian-8 has been removed from the content pack
-   v4.10 To enable/disable read only Triggers, add the parameter
    [trigger/\<name of trigger\>-\[enabled\|disabled\]:
    true]{.title-ref} in the global profile. See
    [](#rs_workorder_trigger).
-   v4.10 Fixed critical bug in event registration that caused system
    crash if request is malformed. This is a recommended dot upgrade for
    previous releases and was back ported to v4.6 and later releases.
-   v4.10 Interactive labs can be used to explore and learn the latest
    Digital Rebar features
-   v4.10 EventToAudit plugin allows any event to be mapped to an audit
    log item

From prior releases:

-   v4.9 Digital Rebar cannot run in a container when using features
    including Contexts, Brokers, Clusters, Multi-Site Manager and High
    Availability. These features all rely on Digital Rebar managing both
    it\'s own lifecycle and driving containers. Running in a container
    prevents Digital Rebar from performing operations required for these
    features.
-   v4.9 work\_order\_templates (preview in v4.8) have been renamed to
    be blueprints
-   v4.9 removal of the Centos mirrors require users to use an archival
    Centos8 mirror for Sledgehammer8.
-   v4.9 format for content pack meta data changed, you must use the
    v4.9 drpcli to bundle v4.9 content
-   v4.8 Workflows relating to Clusters and Cloud\_Wrappers have been
    significantly changed. Operators using those patterns should
    carefully review their implementation before upgrading.
-   v4.8 Cleanup operation offers safer option than destroy by enabling
    pre-destroy actions by defining [on-delete-workflow]{.title-ref}.
    Operators should migrate destroy to cleanup where possible.
-   v4.8 Portal UX editing panels on several screens have been
    signficantly changed. UXViews ability to limit machine field
    view/edit is no longer enabled in v4.8.
-   v4.8 Context Containers need to be rebuilt for v4.8+ Docker Context
    so as to NOT inject a DRPCLI copy. To ensure that the DRPCLI version
    matches the DRP endpoint, the Docker Context will now automatically
    attach the correct DRPCLI to containers during the starting process.
-   v4.8 added Workflows relating to Clusters and Cloud\_Wrappers have
    been significantly changed. Operators using those patterns should
    carefully review their implementation before upgrading.
-   v4.8 added Workflows relating to Clusters and Cloud\_Wrappers have
    been significantly changed. Operators using those patterns should
    carefully review their implementation before upgrading.
-   v4.7 added port 8090 to the list of ports \_[required]() for
    provisioning operations. Please verify that port 8090 (default, this
    can be changed) is accessible for Digital Rebar endpoints.
-   v4.7 changed the install zip format, the API-based upgrade of DRP to
    v4.7+ requires usage of most recent <https://portal.RackN.io> (v4.7
    for self-hosted UX users) or the use of DRPCLI v4.6.7+. The v4.7
    `install.sh upgrade` process also includes theses changes.
-   v4.6 changed mananging of signed certificates, the process for
    updating and importing certificates has changed to require using the
    DRPCLI or API to update certificates. See
    [](#rs_cert_ops) for details.

### Vulnerabilities {#rs_release_v4_10_vulns}

The following vulnerabilities were reported

-   [](#rs_cve_2022_46382)
-   [](#rs_cve_2022_46383)

### Deprecations {#rs_release_v4_10_deprecations}

-   Debian-8 (see removals)

### Removals {#rs_release_v4_10_removals}

-   Debian-8 has been removed to RackN maintained content back due to a
    lack of stable public mirrors

FEATURES
--------

### Drift Detection: Terraform Plan Check and Direct Scanning

Allows operators to proactively monitor managed infratructure for
changes made outside of Digital Rebar control.

Two different drift detection approaches are available: terraform plan
evaluation and direct scanning.

\* Terrform Plan Evaluation leverages the \"plan\" action and stored
Terraform state to analyze managed infrastructure against the expected
state. If differences (aka drift) are detected, alerts are raised. This
allows operators to be notified about or automatically correct
unexpected changes. No special configuration is required, this leverages
inherant behavior of Terraform.

\* Direct Scanning uses infrastructure specific tooling (AWS CLI in this
release) to collect information about the managed infrastructure.
Matches against current Digital Rebar inventory are used to confirm the
accuracy of the tracked machines. Entries without matches are added to
the Digital Rebar inventory as discovered instances with alerts.

Note: it is possible to apply the techniques for AWS drift detection to
other clouds.

### Improvements in Air Gap and Offline Catalog System

Allows operators to build and maintain infrastructure without Internet
access.

While Digital Rebar has supported Air Gap operation since inception,
building a new Digital Rebar in an non-connectivity environment required
significant preparation and experimentation. In v4.10, several features
were added to make it much easier and more predictable to build a
offline/diconnected installation.

These features include:

> -   DRPCLI was enhanced to allow construction of a complete install
>     image for offline deployements
> -   Introduction of a Manager Catalog url list to allow users to
>     update and manage both multiple and local catalogs

### Operator Alerts System

Allows operators to be aware of system events and issues without being
actively connected to the system. This provides a mechanism for
operators to be alerts to system behaior even when they are not logged
into the system.

Introduced a new object type: Alerts. Alerts have fields and times
described in the alerts.go model file. There is a custom action,
ackknowledge that acknowledges the alert. Allows operators to
acknowledge alerts.

Several tasks have been updated to either emit alert targeted events or
directly raise alerts including drift detection, content updates,
endpoint out of disk space, and more.

Notes:

> -   The Alerts system is designed for notification of human operators:
>     it NOT intented as a replacement for tracking and responding to
>     events

via Triggers. Operators may use triggers to monitor events and then
raise Alerts as needed. Operators may also create Triggers that detect
and respond the Alert creation and updates. \* the UX has been updated
to ensure operators are notified about Alerts even if they have not
navigated to the Alerts table.

### UX Integrated Training Labs

Allows operators to learn basic and advanced Digital Rebar skills from
withing Digital Rebar.

The labs provide step-by-step instructions for both basic and new
features of Digital Rebar. This provides a critical in UX way to learn
more about Digital Rebar operations. RackN will continue to add and
expand lab content.

The integrated lab feature includes a \"pop up\" window that allows
operators to use lab training while simultaneously operating the
instructions described in the lab.

The Training Labs are designed to be extended by Digital Rebar operators
to provide implementation specific training guides for their specific
operational requirements. Operators can set the [lab\_urls]{.title-ref}
value to remove or extend the RackN created lab content.

### UX Statistics

Allows operators to quickly analyze key performance meterics related to
Digital Rebar operational characteristics.

There are several areas where the UX has added visualizations:

\* The Overview page includes Task and Blueprint duration whisker
graphs. Revent job activity is plotted using an exponential scale and
shows standard deviation of run times. This allows operators to quickly
identify tasks that are inconsistent or exhibiting unusual behavior.

-   The Info and Preferences page includes three system analytics pie
    charts.
    -   Tasks by Content Pack to help identify if/when operations are
        using tasks outside of RackN or Customer libraries.
    -   Machines by machine/type shows the distribution of physical,
        virtual and container based machines being managed. Clusters and
        Resource Brokers are not included.
    -   Machines by OS class, family, and version shows the distribution
        of operating systems being managed. This can help identify
        systems that are out of compliance.

\* The Info and Preferences page includes also includes a 10-day
activity graph showing Jobs and Active Machines by Day. This graph
provides a quick view into overall system activity,

### UX Flow Visualization

Allows operators to better understand and predict the interconnections
within Digital Rebar automation components.

Digital Rebar pipelines, workflows and triggers represent connected
automation workflows that connect multiple Digital Rebar components
together. While operators can review the objects to learn the
dependencies, new visualizations can make these linkages more intuitive.

To see the visualizations when available, select the \"open flow\" tab
at the bottom right of the objects list page. Once open, the panel can
be resized by dragging the \"\...\" icon at the top of the panel. Any
filters applied to the list view will also be applied to the flow
visualization. It is also possible to drag and resize the graph as
needed.

View Details:

> -   Triggers start from the trigger source and shows the blueprints
>     and tasks that are invoked when a trigger fires.
> -   Pipelines uses the [universal-workflow-chain]{.title-ref}
>     information to show how the various workflows included in a
>     pipeline will likely be processed.
> -   Workflows shows the Stage decomposition of a Workflow and helps
>     visualize how Stages are shared between Workflows.

Note: Blueprint and task metadata field, [flow-comment]{.title-ref} is
used to visualize side effects.

### Advanced Filter Options

Allows operators to perform complex grouped, range and aggregated
queries on Digital Rebar data without having to use external analysis or
overload the server. This significantly enhances operators ability to
perform complex analysis via the API alone.

In addition to doing math in queries, v4.10 allows Parameter expansion
in queries. This allows operators to create queries that incorporate
dynamic information from other parts of Digital Rebar. Operations can
then build API requests that use configuration or state data from the
system itself to enable improved reuse and portability of API calls.

There are four primary new features:

> 1.  Group by - tells the API to collect information about mulltiple
>     objects using the provided fields (can include Params and Meta).
> 2.  Range - allows users to request only high level information
>     instead of detailed grouped results.
> 3.  Param Expand - turns Parameters included in query parameters into
>     the referenced values to allow API call to repond to state and
>     configuration information in Digital Rebar.
> 4.  Aggregate - allows parameter requests to resolve throughout the
>     complete scope. For example, Params available on Profiles or
>     Global are now be returned as part of the object if the Agreggated
>     flag is included.
> 5.  Param Source - allows operators looking at Agreggated params to
>     see where values originate as a new parameter,
>     \"param-source-map\". That contains a key value map of strings.
>     The key is the parameter name,. The value is prefix:key for that
>     source. The map will reference the current object by its
>     prefix:key value. It will be tracked by the alerts feature-flag.
>     Though the presence of the param-source-map parameter also can be
>     used.

Notes

> -   The UX exposes the Aggregate functionality as a switch in the
>     Params views of most objects
> -   These new options enable the backend data collection employeed by
>     the \_UX [Statistics]() feature.

### Enhancments to Parameter Expansion

Allows operators more information about the source of params and to
facilitate mixing existing data and tasks with new data and tasks.

Operators can use the [task-parameter-conversion]{.title-ref} to map
between Parameters dynamically. By adding the parameter to Machine, when
the renderer is instructed to get a Parameter value, the new parameter
will be used to redirect the Parameter resolution.

The [task-parameter-conversion]{.title-ref} parameter is Composed and
Expanded before recursively convert the starting name to the actual
name. if the map had { \"a\": \"b\", \"b\": \"c\" } and parameter a was
looked up the value of parameter c would be used instead.

### Attach to Machine via Proxy (Tech Preview)

Allows operators to use the Digital Rebar Endpoint API as a tunnel to
the managed infrastructure.

This new service uses an instance of the [Apache
Guacamole](https://guacamole.apache.org/) project running in container
as the [guacd-service]{.title-ref} Resource Broker. The implementation
requires that the broker is processing a long running blueprint.

Consult [](#rs_cp_1200)
for instructions to enable this service.

While Guacamole uses 4822 (see [](#rs_arch_guacamole))
is does NOT require that port to be open. Instead, the
Digital Rebar Endpoint tunnels console routes requests to the endpoint
local Guacamole service via the API.

General UX Improvements (applies to all DRP versions) {#rs_release_v4_10_otheritems}
-----------------------------------------------------

-   Timing Analysis Whisker Chart
-   Calculates sources of tasks based on job activity to determine % of
    customized content
-   Visualizes machine activity history for last 10 days
-   Improve rendering for text, code, yaml and JSON data
-   Improved behavior on how new objects appear in various tables
-   Improved visualization of Work Order job activity from Work Order
    page
-   Allow Parameters to show as aggreggated values based on precedence.
    See also [](#rs_parameter_precedence)
-   Fix ExtraClaims field on Tasks view
-   Draggable columns in table views
-   Add line numbers to log entries
-   Fix regression with Racks view
-   In Triggers, improved editors for Chron and Filter params
-   Improved cloud detection for new DRP endpoints, value incorporated
    into Wizard
-   Ability to launch Guacamole terminals for machines
-   Refactored the Catalog page to leverage aggregated catalogs, make it
    more intuitive, and better process dependencies

Other Items of Note
-------------------

-   Universal process now includes Operating System class/family/version
    adds to inventory data
-   Updates to Redhat, Centos and Alma and Rocky Linux pipelines
-   Improvements to Terraform apply processes to better handle edge
    conditions
-   Improved support for Mac M1 systems
-   Improvements to installer including pre-flight checks and air gap
    support
-   To enable/disable read only Triggers, add the parameter
    [trigger/\<name of trigger\>-\[enabled\|disabled\]:
    true]{.title-ref} in the global profile. See
    [](#rs_workorder_trigger).
-   Terraform based Resource Brokers automatically and extensibly
    validate plans using TFLint
-   RackN build Terraform context is pre-loaded with popular cloud
    providers
-   Any event can turned into an audit log item (requires EventToAudit
    plugin)

Roadmap Items (unprioritized, planned for future release)
---------------------------------------------------------

-   Sledgehammer using Ubuntu to enable improved Curtin support
-   Alma Based Sledgehammer - target v4.11, removes dependency on Centos
    for Sledgehammer
-   Updated to Hardware Vendor tool chains - updates RAID/BIOS/Firmware
    to use updated tools from OEMs
-   Agentless ESXi Install - replace/rewrite of the existing ESXi agent
    (drpy) based on upgraded VMware and customer requirements
-   Integrated Simple DNS - provide DNS services for networks
-   Integrated Billing - integrate Billing plugin functionality into
    Digital Rebar core
-   IPv6 DHCP - provide DHCP services for IPv6 networks
-   VMware Cluster Building - coordinate cluster building activities for
    completed vCenter build out including VSAN, NSX-T using vmware lib
    and other tools.
-   Restricted Access ISOs - allow authenticated users to download
    non-public ISOs managed by RackN

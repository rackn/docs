---
title: v4.15 [Q1 2025]
tags:
  - reference
  - operator
  - release
---

# v4.15 [Q1 2025] {#rs_release_v4_15}

Release Date: Q1 2025

Release Themes: TBD

# Executive Summary

TBD

# Important Notices {#rs_release_v4_15_notices}

New for this release:

- v4.15 TBD

From prior releases:

- UPDATE FROM v4.14

- v4.13 Sledgehammer update is required for this release
- v4.13 Improvements in Media Attached Boot (no DHCP & PXE required)
- v4.13 DNS service (aka Zones) natively integrated into Digital Rebar
- v4.13 Audit Create and Update times for objects will be populated going forward. Existing objects will not be updated until new actions are taken.
- v4.13 Demotion of Runner to a non-root user permitted. See v4.13 release for warnings.
- v4.13 Notice: ESXI bootenvs may fail with errors. Change the kernel line from "../../chain.c32" to "../../../chain.c32"
- v4.13 IP allocation management (IPAM) allows operators to better define and manage pools of IP addresses through the subnet definition system.
- v4.13 On-Demand Template Rendering for testing and development
- v4.13 HTTP/HTTPS Network Boot
- v4.13 ESXi8 support. NOTE: Access to signed VIB for SecureBoot requires permission from Broadcom/VMware.
- v4.13 Tech preview of monitoring services: Prometheus, Grafana, and Nagios
- v4.12 Sledgehammer has been updated for multiple platforms including ARM and Ubuntu and requires an updated ISO
- v4.12 Billing Plugin integrated into Core. If installed, it can be safely removed
- v4.12 ISOs storage is now in a Digital Rebar managed read only file system instead of being mapped to the endpoint's local storage. If needed, it can be disabled by setting `RS_USE_ISOFS` to false.
- v4.11 Discovery image now defaults to Alma Linux. Users upgrading Digital Rebar should also install and migrate to this new Sledgehammer.
- v4.11 Client certificate authentication is now off by default in v4.10 and v4.11. To enable, add the **--client-cert-auth** flag or the `RS_CLIENT_CERT_AUTH=true` flag to the systemd configuration space. install.sh has been updated for these options.
- See previous releases for historical items

## Vulnerabilities {#rs_release_v4_15_vulns}

None known

## Deprecations {#rs_release_v4_15_deprecations}

None known

## Removals {#rs_release_v4_15_removals}

None known

# FEATURES

Work in Process Items

These items are under active development, but cannot be confirmed for
delivery in this release. Please consult with RackN if items below are
mission critical for your operation.

# General UX Improvements (applies to all DRP versions) {#rs_release_v4_15_otheritems}

- TBD

# Other Items of Note

- TBD

## Roadmap Items (unprioritized, planned for future release)

- TBD

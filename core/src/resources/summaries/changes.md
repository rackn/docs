---
title: Release Changes
tags:
  - reference
  - operator
  - release
---

# Release Changes {#rs_release_changes}

This document contains the updates against the previous releases.  To see the release contents, please review the release summaries.

This is a summary of the updates made to the published releases.  The sections are organized by RackN release trees.

## Release v4.14 {#rs_rel_v414_updates}

### DRP

#### v4.14.7 (2025/02/05)

  * Remove invalid leases and reservations that would prevent system startup
  * Fix livelock problem with slow managers

#### v4.14.6 (2025/01/29)

  * Update golang vuln in glog and crypto

#### v4.14.5 (2025/01/27)

  * Allow dr-provision to start when prereqs are not met, but still alert the system

#### v4.14.4 (2025/01/14)

  * Fix content pack validation for new errors - bios error was an example

#### v4.14.3 (2025/01/09)

  * Update drpcli version to v4.14.2 to fix go-git sec vuln
  * Fix event trigger data to the proper elements on merge and store in parameter
  * Fix cron-based trigger firing
  * If items on downstream endpoints are removed, manager shoud not loop forever
  * Fix race condition in single node upgrade on directory removal

#### v4.14.2 (2025/01/02)

  * Server can convert secure parameter to insecure on content update.

#### v4.14.1 (2024/12/16)

  * Fix drpcli airgap and catalog function to work better with checksums and windows architecture.
  * Fix drpcli profile and bootenv iso downloading

### UX

#### v4.14.17  (2025/02/12)

  * fix ha check warning

#### v4.14.16  (2025/02/10)

  * bulk action dropdown update

#### v4.14.15  (2025/02/01) - Yes - we typoed

  * Add scrolling in delete dialog
  * tweak SAML login dialog for mobile browsers

#### v4.14.4  (2025/01/29)

  * Fix bootenv field highlighting
  * Reduce download of initial UX by making bootstrap pieces dynamic

#### v4.14.3  (2025/01/29)

  * Fix HA cluster count in status view
  * Fix suspend/resume of laptop with ux open generating a barage of update/checkin requests
  * Fix alert counts in summary dialog
  * Fix refresh buttons on some tables when deleting objects.

#### v4.14.2  (2025/01/23)

  * Fix Alert count field (off by one)
  * Fix cert upload dialog
  * Remove Subnet warning of about DHCP if Proxy is set to true
  * Fix pipeline searching to work cleaner

#### v4.14.1  (2024/12/16)

  * Add configuration option to adjust timezone views

### Provision Content

#### v4.14.10 (2025/02/10)

  * drp-community-content: remove ipmi-scan-results from roles - not in default content

#### v4.14.9 (2025/02/10)

  * flash: fix infinite in hpe flash lists

#### v4.14.8 (2025/02/10)

  * drp-community-content: update default roles for new objects
  * flash: include hpe-helpers for flash-list operations

#### v4.14.7 (2025/01/27)

  * drp-community-content: Reduce the noise of the utility-connectivity-test

#### v4.14.6 (2025/01/27)

  * drp-community-content: Fix crash in utility-connectivity-test

#### v4.14.5 (2025/01/16)

  * drp-community-content: fix dependencies to require v4.14 DRP

#### v4.14.4 (2025/01/14)

  * flash: add ability to reboot in the middle of a flash list.

#### v4.14.3 (2025/01/10)

  * drp-community-content: Fix task-add template for esxi

#### v4.14.2 (2025/01/03)

  * drp-community-content: Fix task-add template for windows
  * classify: Fix classify and classify-stage-list-start for windows
  * validation: Fix validation-start for windows
  * flexiflow: Fix flexiflow-start for windows

#### v4.14.1 (2025/01/02)

  * drp-community-content: revert a parameter back to insecure (it was not needed)
  * task-library: Allow lldp info to be non-json and handle this error

### Plugins

#### v4.14.5 (2025/02/10)

  * image-deploy: Add curtin patch to fix python path.

#### v4.14.4 (2025/01/29)

  * all: Upgrade for golang security issues

#### v4.14.3 (2025/01/20)

  * vmware: Handle config already existing on rebuilds
  * all: Update prereqs for better content control

#### v4.14.2 (2025/01/15)

  * vmware: Update hostname, ntp, and dns setting for 8u3 support

#### v4.14.1 (2025/01/03)

  * eikon: Fix win image deployment in eikon

## Release v4.13 {#rs_rel_v413_updates}

### DRP

#### v4.13.31 (2025/02/05)

  * Remove invalid leases and reservations that would prevent system startup
  * Fix livelock problem with slow managers

#### v4.13.30 (2025/01/29)

  * Update golang vuln in glog and crypto

#### v4.13.29 (2024/12/04)

  * Fix issue with reservation field Subnet and Read-only objects in content packs

#### v4.13.28 (2024/11/21)

  * Fix JWT golang vulnerability
  * Fix virtual media mount of ISOs (NPE)

#### v4.13.27 (2024/11/01)

  * Fix issue with removing consensus members
  * Fix race condition in Manager code

#### v4.13.26 (2024/10/23)

  * Allow ad-auth and saml to always work regardless of license expiration
  * Fix data race when starting a runner and updating contexts
  * Add feature flag for secure param auto encryption on set
  * Emit plugin failure to start messages

#### v4.13.25 (2024/09/30)

  * Fix bootenv override merging by arch type.  x86_64 and amd64 now merge correctly.
  * Update retry in drpcli for catalog urls

#### v4.13.24 (2024/09/17)

  * Update drpcli catalog debug messages

#### v4.13.23 (2024/09/06)

  * Fix crash when running jobs while returning Parameter data

#### v4.13.22 (2024/09/04)

  * Increase performance of concurrent jobs (remove single lock)

#### v4.13.21 (2024/08/30)

  * Increase performance of Job/Machine Patch operations
  * Update `drpcli system upgrade` command to be cluster aware.

#### v4.13.20 (2024/08/20)

  * Fix license machine counting issues

#### v4.13.19 (2024/08/08)

  * Allow self upgrade to handle issues around non-root cases better

#### v4.13.18 (2024/08/08)

  * Fix Filter object to be Paramers so that the task-library can load.

#### v4.13.17 (2024/08/01)

  * Enabled job sweeper to run every hour (like it should have been).  Active systems will notice this less.

#### v4.13.16 (2024/07/29)

  * Fail endpoint when DRP fails to update or all pending actions fail.
  * Fix start-up validation failure around template references
  * Fix MSM memory/crash loop when failing to upgrade a downstream endpoint
  * Reduce artifact syncing to the MSM
  * Streamline endpoint update from MSM

#### v4.13.15 (2024/07/15)

  * Fix ESXi bootenv issue with path expansion (adds backs compatability for older ESXi bootenvs)

#### v4.13.14 (2024/07/12)

  * SECURITY: Denial of service due to improper 100-continue handling in net/http [info](https://pkg.go.dev/vuln/GO-2024-2963)
  * SECURITY: Denial of service via malicious preflight requests in github.com/rs/cors [info](https://pkg.go.dev/vuln/GO-2024-2883)
  * SECURITY: Gin mishandles a wildcard in the origin string in github.com/gin-contrib/cors [info](https://pkg.go.dev/vuln/GO-2024-2955)
  * Handle missing bootenv when handling DHCP Discovers/Requests - caused a crash loop

#### v4.13.13 (2024/06/20)

  * Start DRP if DNS ports are block and log warnings to fix upgrade issues
  * Allow plugins to specify secure parameters
  * Emit more static filesystem errors
  * Fix vulnerability in golang code
  * Handle PTR requests across zones
  * Add DNS wildcard support
  * Fix arm path handling for sledgehammer and other bootenvs
  * Add more stack traces for API panic paths
  * Automatically set machine arch from DHCP packets (if known)
  * Fix license expire calculations

#### v4.13.12 (2024/04/26)

  * Add SkipDAD option on subnets / reservations in objects
  * Fix Ux* models crashing server as params

#### v4.13.11 (2024/04/23)

  * Fix start-fail loop when lease is deleted before machine in same transaction

#### v4.13.10 (2024/04/22)

  * Fix database growth overtime.  2 restarts are required for this fix to be completely installed.
  * Looking up parameters performance improved after addition of IPAM dynamic parameters feature
  * Handle secure parameters when loading plugins.
  * Expand connection cancel error messages
  * Improve performance of catalog construction
  * Fix archive exploding

#### v4.13.9 (2024/04/05)

  * SECURITY: Update net and net/http library to handle vulnerability [info](https://pkg.go.dev/vuln/GO-2024-2687)
  * Fix websocket infinite loop

#### v4.13.8 (2024/03/20)

  * SECURITY: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)

#### v4.13.7 (2024/03/19)

  * Send expanded decrypt URI to the plugin handing secure parameter decryption

#### v4.13.6 (2024/03/13)

  * Fix race condition in endpoint managning backend code of the manager.  This could cause actions to get lost.

#### v4.13.5 (2024/03/11)

  * Update terraform digital ocean integration fix (v4.13.4 - rackn/provision tree)
  * Add ReplaceWritable to the VersionSet Component to allow for the manager to push content with ReplaceWritable flag set.
  * Fix pool operations will restart workflows set if they are the same as existing workflows
  * Enforce that only one universal-application-* profile can be on the system at a time.  This fixes resource-brokers setting pipelines.

#### v4.13.4 (2024/03/04)

  * SECURITY: Fix leak of ad-auth user into job logs.  [CVE-2024-RKN0001](#rs_cve_2024_RKN0001)
  * Handle null ptrs for searching

#### v4.13.3 (2024/03/03)

  * dr-waltool / passive replication (manager) - decouple locking when doing a backup or manager sync.  This will prevent the system from becoming unresponsive on hung or stalled backups.

  * FEATURE: Allow for RBAC to restrict values in a role.

    * e.g. `machines.update:Workflow:univeral-discover.*`
    * This will only allow the user to set the Workflow field on a machine to `universal-discover`.

#### v4.13.2

  * Speed up catalog rebuilds on the manager by reusing checksum calculations

#### v4.13.1

  * Remove a go-routine leak when purging / archiving jobs

### UX

#### v4.13.12  (2024/12/16)

  * Additional log scrolling and view fixes

#### v4.13.11  (2024/08/14)

  * Update log scrolling for activities view to increase performance and usability

#### v4.13.10  (2024/08/05)

  * Handle pool action enablement when clicking in the UX

#### v4.13.9  (2024/05/30)

  * Add confirmation pop-up when changing more than 5 rows or if some rows are not on the current page.

#### v4.13.8  (2024/05/01)

  * Fix job view with long logs

#### v4.13.7  (2024/04/23)

  * Fix task column links on nested tables
  * Fix task log cut-off on failure
  * Remove extra render button and add doc links

#### v4.13.6  (2024/04/22)

  * Fix resource link reference
  * Fix secure parameter infinite loading

#### v4.13.5 (2024/03/27)

  * Fix stage hover crash

#### v4.13.4 (2024/03/08)

  * FEATURE: Add support for brokers providing options to clusters in creation
  * Fix issue with meta data being null

#### v4.13.3 (2024/03/03)

  * Fix reservation and lease tables not updating on events.

#### v4.13.2

  * Update default bash template to not use ()
  * Handle null counts
  * IPMI pop-up should not cover names

#### v4.13.1

  * Fix job template render source differences
  * Fix async closure issue

### Provision Content

#### v4.13.28 (2025/02/10)

  * flash: fix infinite in hpe flash lists

#### v4.13.27 (2025/02/10)

  * drp-community-content: update default roles for new objects
  * flash: include hpe-helpers for flash-list operations

#### v4.13.26 (2025/01/16)

  * flash: supermicro support

#### v4.13.25 (2024/12/16)

  * drp-community-content: Add alma8 profile

#### v4.13.24 (2024/11/01)

  * flash: Revert script update and fix issue with Etag directly

#### v4.13.23 (2024/10/23)

  * flash: Fix issue where HPE minor versions were being read as octal

#### v4.13.22 (2024/10/21)

  * flash: Update the dell redfish python helper script

#### v4.13.21 (2024/10/11)

  * hardware-tooling: fix hardware-base dependence upon future function.  Caused task to fail.

#### v4.13.20 (2024/09/29)

  * drp-community-content: fix sledgehammer passing uuid as boot param

#### v4.13.19 (2024/09/29)

  * cisco-support: Update context version and add bootstrap helper profile - MUST GET NEW CONTAINER

#### v4.13.18 (2024/09/27)

  * hardware-tooling: Add hardware-base task to enable cisco hardware support
  * cisco-support: Update docs for requirements

#### v4.13.17 (2024/08/29)

  * task-library: Add TPM version to inventory (requires redfish and ipmi configuration).  May need to add inventory-check task to pipelines after the ipmi-configure task in pipelines to take full effet.

#### v4.13.16 (2024/08/05)

  * content: Add an alma 8.10 and 9.4 sledgehammer to work around secure boot blacklisting grub images

#### v4.13.15 (2024/07/02)

  * hpe: Add timeout parameters for some ilorest calls

#### v4.13.14 (2024/06/26)

  * drp-community-content: exclude the cdc_eem driver from MAC discovery to prevent duplicate detection failures.

#### v4.13.13 (2024/05/15)

  * hpe-support: remove mcp repo because it is gone for most OSes

#### v4.13.12 (2024/05/01)

  * drp-community-content: fix debian install

#### v4.13.11 (2024/04/29)

  * hpe-support: add parameter to enable ssacli installation `hpe-install-ssacli`
  * flash: fix error message parameter reference.

#### v4.13.10 (2024/04/21)

  * drp-community-content: add missing config file for stage start.

#### v4.13.9 (2024/04/03)

  * drp-community-content: Add support for ignore interfaces and drivers during mac address discovery.  The defaults allow for ignoring the USB Driver used by Supermicro BMC that generate random mac addresses.

#### v4.13.8 (2024/04/01)

  * hpe-support: Don't install a raid tool that conflicts with the raid system

#### v4.13.7 (2024/03/25)

  * drp-community-content: Handle spurious files in the sys net space

#### v4.13.6 (2024/03/22)

  * drp-community-content: discovery can now have a configurable list of macs to ignore by parameter: discovery-mac-ignore-list
  * drp-community-content: Add Supermicro X9 USB BMC NIC MAC to ignore list
  * drp-community-content: Add install_lookup function to helper templates that can look-up packages by distro

#### v4.13.5 (2024/03/11)

  * drp-community-content: add a bootstrap utility blueprint and task to download a set of bootenvs during the bootstrap phase.
  * task-library: Add return-pipeline and return-workflow for the pool-broker to clean up machines on return to the pool.
  * task-library: ansible-playbooks can provide a galaxay requirements file.

#### v4.13.4 (2024/03/08)

  * flash: Handle AMD HPE packages better

#### v4.13.3 (2024/03/03)

  * cloud-wrappers: Update vsphere terraform provider
  * cloud-wrappers: Add helpers for resource brokers when creating machines
  * cloud-wrappers: vsphere machines can set firmware and secure-boot options
  * cloud-wrappers: Add datastore-type to allow for cloud and normal datastore selections
  * drp-community-content: Convert centos-8 to alma-9-min as default OS

#### v4.13.2

  * drp-community-content: remove dependency upon os-other

#### v4.13.1

  * drp-community-content: manager bootstrap should not pull tip packages by default
  * drp-community-content: disk-stress test had a typo that was fixed
  * os-other: fix broken rhel-7.7 bootenv

### Plugins

#### v4.13.23 (2025/01/20)

  * vmware: Handle config already existing on rebuilds

#### v4.13.22 (2025/01/15)

  * vmware: Update hostname, ntp, and dns setting for 8u3 support
  * bios: rework bios config lopp detection.

#### v4.13.21 (2024/10/28)

  * bios: Fix issue with the bioscfg tool not finding the onecli tool

#### v4.13.20 (2024/09/04)

  * ipmi: fix ipmi scan machines to have correct ip and mac.

#### v4.13.19 (2024/08/29)

  * ipmi: Add redfish actions nextboothttp and forceboothttp to support HTTP booting

#### v4.13.18 (2024/08/13)

  * vmware: fix make-esxi.sh to support -8 option

#### v4.13.17 (2024/08/08)

  * bios: Fix issue with dell-pending-config-jobs get set to a structure

#### v4.13.16 (2024/07/12)

  * SECURITY: Denial of service due to improper 100-continue handling in net/http [info](https://pkg.go.dev/vuln/GO-2024-2963)
  * SECURITY: Leak of sensitive information to log files in github.com/hashicorp/go-retryablehttp [info](https://pkg.go.dev/vuln/GO-2024-2947)
  * SECURITY: Azure Identity Libraries Elevation of Privilege Vulnerability in github.com/Azure/azure-sdk-for-go/sdk/azidentity [info](https://pkg.go.dev/vuln/GO-2024-2918)
  * SECURITY: Archiver Path Traversal vulnerability in github.com/mholt/archiver [info](https://pkg.go.dev/vuln/GO-2024-2698)

#### v4.13.15 (2024/07/10)

  * bios: Fix issue with missing function in bios-configure - add setup.tmpl to get helper functions

#### v4.13.14 (2024/07/09)

  * vmware: handle config erasure by vmware tools on upgrade/install of vibs

#### v4.13.13 (2024/05/30)

  * bios: Update parsing of dell pending jobs - fixes bad parameter error

#### v4.13.12 (2024/05/15)

  * vmware: fix make-esxi.sh to work with latest changes and add -8 flag for esxi 8
  * vmware: A standard networking task for advanced networking configurations.

#### v4.13.11 (2024/05/01)

  * vmware: fix checksums in vcf-5.0.0 pipeline
  * vmware: Update network and hostname task to use ParamExpand.

#### v4.13.10 (2024/04/05)

  * SECURITY: Update net and net/http library to handle vulnerability [info](https://pkg.go.dev/vuln/GO-2024-2687)

#### v4.13.9 (2024/04/06)

  * blancco-lun-eraser: Update the plugin with the latest Blancco tooling and workflows

#### v4.13.8 (2024/04/01) - Requires DRP v4.11.32 or higher

  * image-deploy: Add curtin/patches parameter to allow for hacking on curtin
  * image-deploy: allow curtin dd to work on md raid devices
  * raid: Allow bootenv of raid-encryption to be set in the flexiflow list
  * raid: Rework raid-install-tools to allow for future and past raid tools
  * raid: Update the perccli tools
  * raid: Fix perccli to stop if it finds unparsable data
  * raid: Fix perccli-json to handle bus data better
  * azkeyvault: Add support for certificates and update docs

#### v4.13.7 (2024/03/20)

  * SECURITY: all: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)

#### v4.13.6 (2024/03/19) - Requires DRP v4.13.7 or higher

  * awssecman: New plugin that can used AWS security manager to retrieve secrets.
  * azkeyvault: New plugin that can used Azure Key Vaults to retrieve secrets.
  * cmdvault: New plugin that can used command line tools to get secrets.

#### v4.13.5 (2024/03/13)

  * ipmi: Update redfish library and ipmi-status-validation to fail on critial errors and optionally on warnings

#### v4.13.4 (2024/03/11)

  * vmware: update bootenvs with group-by data
  * vmware: improve the esxi-image-install to handle device paths and better error handling

#### v4.13.3 (2024/03/03)

  * raid: fix perccli2 issues around clear and quoted names

#### v4.13.2

  * raid: fix perccli2 to have a force on the clear function

#### v4.13.1

  * ipmi: Fix Lenovo naming in IPMI to set IMM.IMMInfo_Name to the machine name.

### RackN Plugins

#### v4.13.5 (2024/11/01)

  * docker-context: Handle container restart if the container is corruprt on server restart

#### v4.13.4 (2024/07/12)

  * SECURITY: Denial of service due to improper 100-continue handling in net/http [info](https://pkg.go.dev/vuln/GO-2024-2963)
  * docker-context: fix podman mount options for drpcli in the container

#### v4.13.3 (2024/04/05)

  * SECURITY: Update net and net/http library to handle vulnerability [info](https://pkg.go.dev/vuln/GO-2024-2687)

#### v4.13.2 (2024/03/20)

  * SECURITY: all: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)
  * SECURITY: all: Update net library to handle vulnerability
  * SECURITY: all: Update crypto library to handle vulnerability

#### v4.13.1 (2024/03/03)

  * billing: remove from tree because it is embedded in DRP

### Universal

#### v4.13.5 (2024/11/06)

  * Restrict universal/hardware value to valid profile name characters

#### v4.13.4 (2024/09/27)

  * add hardware-base to universal-discover and universal-hardware pre-flexiflow tasks

#### v4.13.3 (2024/07/29)

  * add esxi-standard-network config to the esxi config pipeline elements

#### v4.13.2 (2024/03/22)

  * universal-decommission can now replace some or all of the default decommission tasks.

#### v4.13.1 (2024/03/03)

  * Convert centos-8 to alma-9-min as base OS.
  * Reorder discover classifier to enable specific profiles to be applied instead of general profiles

### Cohesity

#### v4.13.1 (2024/03/20)

  * Add support for the Cohesity 6.6.0d release

## Release v4.12 {#rs_rel_v412_updates}

### DRP

#### v4.12.31 (2025/01/29)

  * Update golang vuln in crypto and archiver

#### v4.12.30 (2024/11/21)

  * Fix JWT golang vulnerability

#### v4.12.29 (2024/11/19)

  * Update go security vulnerabilites for gin in dr-provision

#### v4.12.28 (2024/09/06)

  * Fix crash when running jobs while returning Parameter data

#### v4.12.27 (2024/09/04)

  * Increase performance of concurrent jobs (remove single lock)

#### v4.12.26 (2024/08/29)

  * Increase performance of Job/Machine Patch operations
  * drpcli golang security updates

#### v4.12.25 (2024/04/22)

  * Fix database growth overtime.  2 restarts are required for this fix to be completely installed.

#### v4.12.24 (2024/03/20)

  * SECURITY: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)

#### v4.12.23 (2024/03/19)

  * Send expanded decrypt URI to the plugin handing secure parameter decryption

#### v4.12.22 (2024/03/04)

  * SECURITY: Fix leak of ad-auth user into job logs.  [](#rs_cve_2024_RKN0001)
  * Handle null ptrs for searching

#### v4.12.21 (2024/03/03)

  * dr-waltool / passive replication (manager) - decouple locking when doing a backup or manager sync.  This will prevent the system from becoming unresponsive on hung or stalled backups.
  * Remove a go-routine leak when purging / archiving jobs (drpcli v4.12.9)

#### v4.12.20

  * Allow for removal of duplicate objects across layers

### Provision Content

#### v4.12.16 (2024/11/01)

  * flash: Revert script update and fix issue with Etag directly

#### v4.12.15 (2024/10/23)

  * flash: Fix issue where HPE minor versions were being read as octal

#### v4.12.14 (2024/10/21)

  * flash: Update the dell redfish python helper script

#### v4.12.13 (2024/10/14)

  * task-library: Add TPM version to inventory (requires redfish and ipmi configuration).  May need to add inventory-check task to pipelines after the ipmi-configure task in pipelines to take full effet.

#### v4.12.12 (2024/07/02)

  * hpe: Add timeout parameters for some ilorest calls

#### v4.12.11 (2024/04/01)

  * bios: Don't install a raid tool that conflicts with the raid system

#### v4.12.10 (2024/03/08)

  * flash: Handle AMD HPE packages better

### Plugins

#### v4.12.21 (2025/01/20)

  * vmware: Handle config already existing on rebuilds

#### v4.12.20 (2025/01/15)

  * vmware: Update hostname, ntp, and dns setting for 8u3 support
  * bios: rework bios config lopp detection.

#### v4.12.19 (2024/08/08)

  * bios: Fix issue with dell-pending-config-jobs get set to a structure

#### v4.12.18 (2024/07/09)

  * vmware: handle config erasure by vmware tools on upgrade/install of vibs

#### v4.12.17 (2024/04/01) - Requires DRP v4.11.32 or higher

  * raid: Rework raid-install-tools to allow for future and past raid tools
  * raid: Update the perccli tools
  * raid: Fix perccli to stop if it finds unparsable data
  * raid: Fix perccli-json to handle bus data better

#### v4.12.16 (2024/03/20)

  * SECURITY: all: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)

#### v4.12.15 (2024/03/19) - Requires DRP v4.12.23 or higher

  * awssecman: New plugin that can used AWS security manager to retrieve secrets.
  * azkeyvault: New plugin that can used Azure Key Vaults to retrieve secrets.
  * cmdvault: New plugin that can used command line tools to get secrets.

#### v4.12.14 (2024/03/13)

  * ipmi: Update redfish library and ipmi-status-validation to fail on critial errors and optionally on warnings

### RackN Plugins

#### v4.12.2 (2024/03/20)

  * SECURITY: all: Update protobuf library to handle vulnerability [info](https://github.com/advisories/GHSA-8r3f-844c-mc37)
  * SECURITY: all: Update net library to handle vulnerability
  * SECURITY: all: Update crypto library to handle vulnerability

### Universal

#### v4.12.1 (2024/03/22)

  * universal-decommission can now replace some or all of the default decommission tasks.

## Release v4.11 {#rs_rel_v411_updates}

### DRP

#### v4.11.34 (2025/01/29)

  * Update golang vuln in crypto

#### v4.11.33 (2024/11/21)

  * Fix JWT golang vulnerability
  * Fix Manager race condition on saving endpoints into the database.

#### v4.11.32 (2024/03/19)

  * Send expanded decrypt URI to the plugin handing secure parameter decryption

#### v4.11.31 (2024/03/03)

  * dr-waltool / passive replication (manager) - decouple locking when doing a backup or manager sync.  This will prevent the system from becoming unresponsive on hung or stalled backups.
  * Remove a go-routine leak when purging / archiving jobs (drpcli v4.11.13)

#### v4.11.30

  * Allow for removal of duplicate objects across layers

### Provision Content

#### v4.11.29 (2025/02/10)

  * flash: fix infinite in hpe flash lists

#### v4.11.28 (2025/02/10)

  * drp-community-content: update default roles for new objects
  * flash: include hpe-helpers for flash-list operations

#### v4.11.27 (2025/01/14)

  * flash: add ability to reboot in the middle of a flash list.

#### v4.11.26 (2024/11/01)

  * flash: Revert script update and fix issue with Etag directly

#### v4.11.25 (2024/10/23)

  * flash: Fix issue where HPE minor versions were being read as octal

#### v4.11.24 (2024/10/21)

  * flash: Update the dell redfish python helper script

#### v4.11.23 (2024/10/14)

  * task-library: Add TPM version to inventory (requires redfish and ipmi configuration).  May need to add inventory-check task to pipelines after the ipmi-configure task in pipelines to take full effet.

#### v4.11.22 (2024/07/02)

  * hpe: Add timeout parameters for some ilorest calls

#### v4.11.21 (2024/04/01)

  * bios: Don't install a raid tool that conflicts with the raid system

#### v4.11.20 (2024/03/08)

  * flash: Handle AMD HPE packages better

### Plugins

#### v4.11.19 (2025/01/15)

  * vmware: allow for config file reuse
  * vmware: Update hostname, ntp, and dns setting for 8u3 support
  * bios: rework bios config lopp detection.

#### v4.11.18 (2024/08/08)

  * bios: Fix issue with dell-pending-config-jobs get set to a structure

#### v4.11.17 (2024/07/09)

  * vmware: handle config erasure by vmware tools on upgrade/install of vibs

#### v4.11.16 (2024/04/01) - Requires DRP v4.11.32 or higher

  * raid: Rework raid-install-tools to allow for future and past raid tools
  * raid: Update the perccli tools
  * raid: Fix perccli to stop if it finds unparsable data
  * raid: Fix perccli-json to handle bus data better

#### v4.11.15 (2024/03/19) - Requires DRP v4.11.32 or higher

  * awssecman: New plugin that can used AWS security manager to retrieve secrets.
  * azkeyvault: New plugin that can used Azure Key Vaults to retrieve secrets.
  * cmdvault: New plugin that can used command line tools to get secrets.

#### v4.11.14 (2024/03/13)

  * ipmi: Update redfish library and ipmi-status-validation to fail on critial errors and optionally on warnings

## Release Groupings

The following are the groupings of updates.  There versions travel together.  Where possible, the change notes which specific piece is updated.

### DRP

### UX

### Content Packs

  * batch
  * burnin
  * chef-bootstrap
  * cisco-support
  * classify
  * cloud-wrappers
  * coreos
  * dell-support
  * dev-library
  * drp-community-content
  * drp-community-contrib
  * drp-prom-mon
  * edge-lab
  * flash
  * flexiflow
  * grafana
  * hardware-tooling
  * hashicorp
  * hpe-support
  * image-builder
  * krib
  * kube-lib
  * kubespray
  * lenovo-support
  * nagios
  * napalm
  * os-other
  * packer-builder
  * prometheus
  * proxmox
  * rancheros
  * sledgehammer-builder-centos-7
  * sledgehammer-builder
  * supermicro-support
  * task-library
  * terraform
  * ubuntuhammer-builder
  * validation
  * vmware-lib

### Plugins

  * bios
  * blancco-lun-eraser
  * callback
  * certs
  * eikon
  * endpoint-exec
  * event2audit
  * filebeat
  * image-deploy
  * ipmi
  * netbox
  * packet-ipmi
  * raid
  * slack
  * tower
  * triggers
  * vault
  * virtualbox-ipmi
  * vmware

### RackN Plugins

  * ad-auth
  * agent
  * docker-context
  * rack

### Universal

### Solidfire

### Cohesity

### OpenShift

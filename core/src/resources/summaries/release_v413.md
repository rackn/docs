---
title: v4.13 [February 2024]
tags:
  - reference
  - operator
  - release
---

# v4.13 [February 2024] {#rs_release_v4_13}

Release Date: February 13, 2024

Release Themes: Completing the Infrastructure Stack

# Executive Summary

In this release, Digital Rebar now has all the built-in features to be a complete, stand alone Infrastructure Platform. This goes beyond completing our native DDI (DHCP, DNS, IPAM) suite, it includes monitoring, observability and zero-touch standup of multiple full virtualization managers including vCenter, Nutanix and Proxmox. We have created all the ease of use found in proprietary HCIs appliances without prescribing the hardware, operating system, storage or VM manager.

# Important Notices {#rs_release_v4_13_notices}

New for this release:

- v4.13 Sledgehammer update is required for this release
- v4.13 Improvements in Media Attached Boot (no DHCP & PXE required)
- v4.13 DNS service (aka Zones) natively integrated into Digital Rebar
- v4.13 Audit Create and Update times for objects will be populated going forward. Existing objects will not be updated until new actions are taken.
- v4.13 Demotion of Runner to a non-root user permitted. See release note for WARNINGS!
- v4.13 Notice: ESXI bootenvs may fail with errors. Change the kernel line from "../../chain.c32" to "../../../chain.c32"
- v4.13 IP allocation management (IPAM) allows operators to better define and manage pools of IP addresses through the subnet definition system.
- v4.13 On-Demand Template Rendering for testing and development
- v4.13 HTTP/HTTPS Network Boot
- v4.13 ESXi8 support. NOTE: Access to signed VIB for SecureBoot requires permission from Broadcom/VMware.
- v4.13 Tech preview of monitoring services: Prometheus, Grafana, and Nagios

From prior releases:

- v4.12 Sledgehammer has been updated for multiple platforms including ARM and Ubuntu and requires an updated ISO
- v4.12 Billing Plugin integrated into Core. If installed, it can be safely removed
- v4.12 ISOs storage is now in a Digital Rebar managed read only file system instead of being mapped to the endpoint's local storage. If needed, it can be disabled by setting `RS_USE_ISOFS` to false.
- v4.11 Discovery image now defaults to Alma Linux. Users upgrading Digital Rebar should also install and migrate to this new Sledgehammer.
- v4.11 Client certificate authentication is now off by default in v4.10 and v4.11. To enable, add the **--client-cert-auth** flag or the `RS_CLIENT_CERT_AUTH=true` flag to the systemd configuration space. install.sh has been updated for these options.
- v4.10 Debian-8 has been removed from the content pack.
- v4.10 To enable/disable read only Triggers, add the parameter trigger/\<name of trigger\>-\[enabled\|disabled\]: true in the global profile. Search workorder trigger.
- v4.10 Fixed critical bug in event registration that caused system crash if request is malformed. This is a recommended dot upgrade for previous releases and was back ported to v4.6 and later releases.
- v4.10 Interactive labs can be used to explore and learn the latest Digital Rebar features.
- v4.10 EventToAudit plugin allows any event to be mapped to an audit log item.
- v4.9 Digital Rebar cannot run in a container when using features including Contexts, Brokers, Clusters, Multi-Site Manager and High Availability. These features all rely on Digital Rebar managing both its own lifecycle and driving containers. Running in a container prevents Digital Rebar from performing operations required for these features.
- v4.9 work_order_templates (preview in v4.8) have been renamed to be blueprints.
- v4.9 removal of the Centos mirrors require users to use an archival Centos8 mirror for Sledgehammer8.
- v4.9 format for content pack meta data changed, you must use the v4.9 drpcli to bundle v4.9 content.
- See previous releases for historical items

## Vulnerabilities {#rs_release_v4_13_vulns}

None known

## Deprecations {#rs_release_v4_13_deprecations}

None known

## Removals {#rs_release_v4_13_removals}

A Sledgehammer update is required for this release.

# FEATURE HIGHLIGHTS

## Improvements in Media Attached Boot (no DHCP & PXE required)

_The feature is now generally available. It had been available only as a special request feature._

If the OEM hardware supports media attachment, Operators may boot machines by attaching a Sledgehammer image directly to the BMC interface.

This alternate to network booting allows Operators to eliminate DHCP and TFTP from their networks but increases operational complexity.

## DNS service (aka Zones) natively integrated into Digital Rebar

Digital Rebar v4.13 now includes a built-in DNS service managed using APIs and IaC content. Critical use for ZTP clusters, bootstrapping machines and edge/operations networks.

Operators can define local zones that using template expansion. That automatically maps machines into names (e.g. A records). Of course, you can forward to other DNS too.

## Object Create and Update times

Starting in v4.13, Digital Rebar now tracks both when and who created and then last updated all objects. This allows Operators to determine basic audit history without consulting activity logs.

For objects created in previous versions, the information will be present but left blank.

## Nutanix Cluster Provisioning (Tech Preview)

Operators can use Digital Rebar to fully bootstrap and oeprate Nutanix VM manager clusters.

## Proxmox Cluster Provisioning

Operators can use Digital Rebar to fully bootstrap and operate Proxmox VM manager clusters.

## HTTP/HTTPS Network Boot

Digital Rebar network boot processes can move directly to HTTP and skip the slower and less secure TFTP bootloaders.

This provides a significant performance improvement for customers who have transitioned to UEFI BIOS.

## Demotion of Runner

Demotion of Runner to a non-root user permitted. This allows operators to limit Digital Rebar control of machines after provisioning operations in cases where they are handed over to teams with elevated security concerns. This provides a less extreme option than disabling or removing the DRP runner.

WARNINGS:

- Will cause failures for tasks that assume root access.
- Transitions from non-root back to root user will likely fail
- Available for Linux only

## IP allocation management (IPAM)

IP allocation management (IPAM) allows operators to better define and manage pools of IP addresses through the subnet definition system. Digital Rebar v4.13 now includes a built-in IP allocation manager using APIs and IaC content. Operators can define address pools via DRP DHCP subnets

Critical use for ZTP clusters where addresses need to be defined coherently across multiple interfaces.

## UX: On-Demand Template Rendering for testing and development

To improve debugging, users are not able to render job templates dynamically in the UX. This allows users to use specific machines to validate the code and rendering of templates without having to run jobs.

## UX: Audit Data Report

Digital Rebar v4.12 integrated machine activity tracking directly into the core model. This allows Operators to track provisioning operations performed by DRP over time. The UX now shows this data.

This is also allows preview of data for Activity Based billing customers.

## CloudBuilder and ESXi8 support (including secure boot)

Digital Rebar agent has been updated for ESXi8. NOTE: Access to signed VIB for SecureBoot requires permission from Broadcom/VMware.

## Monitoring services (Tech preview)

Available as container runners managed by Digital Rebar.

- Prometheus for DRP Observability & Performance
- Grafana for Operational Trends & Analysis
- Nagios for Machine Monitoring & Availability (include automatically registering machines with Nagios)

# Roadmap Items (planned for future release)

- Improved Image Deployer (IKON) is being actively tested and is expected for the v4.14 release

# Gitlog Review

```
feat: AD Auth can use the SAM paramter instead just the base account name
feat: add a configurable optional part-scheme for RHEL7 kick start files
feat: Add additional disk references in gohai that can be used in image-deploy or installation paths
feat: add context image pull path, volumes, and state-dir options
feat: Add DNS IPAM support with new models and command lines. Note the bigger feature list.
feat: Add example tasks for install powercli and powershell tools
feat: Add example version sets with DRP versions specified
feat: add expand parameters view
feat: Add helper function for tasks to split lists evenly
feat: Add more HA status info to the get info structure.
feat: Add more tracking stats
feat: Add no_proxy parameter similare to proxy_server parameter
feat: Add on-demand rendering of templates for debugging and development
feat: add runnable button to debug workflow mode view
feat: Add support to raid subsystem for dell-based mnv_cli
feat: Add tasks that can demote/promote the runner from root to another user. Going back to root may not work.
feat: add template renderer views
feat: add the ability to run code servers in the dev-library
feat: Add universal-evaulate to support minimal discovery path to speed up rebuilds (for trusted discovered environments)
feat: Allow API and CLI on-demand template rendering for debugging and development.
feat: Allow bios configure to have multiple sets of data to apply
feat: Allow container startup-options on contexts
feat: Allow for configurable job log overwrite instead of failure on large logs
feat: Allow for disable of LLDP firewall in nics
feat: Allow for duration of extra claims token inside a task
feat: allow for the Dell DSU specification to be easier
feat: Allow for the terraform-apply task to specify init-options
feat: allow ordering profiles in profile lists
feat: Allow runner to have an undefined local context to facailite user demotion feature
feat: allow selinux relabel in a container on startup
feat: Allow tasks to specify the duration of the token generated with extra claims
feat: Allow workorders to be created on locked machines by users
feat: bios configuration can have multiple separate configurations
feat: bundle and unbundle cli commands can specify source and destination directories. Removing requirement to be "in" the directory.
feat: Cert import pages for server certificates
feat: Change consensus start to a two-phase process internally to handle restart failures
feat: change save button color to blue once saved
feat: Collect serial number of machine during IPMI scan
feat: Dell systems will handle pending or running jobs cleaner
feat: Display new stats
feat: Display the create / modification information on objects.
feat: DNS IPAM pages and views
feat: DNS IPAM updates - see big feature item
feat: DRP Runner/Agent can run as PID 1 and reap children appropriately in container environments
feat: drpcli can report create and update user actions on objects
feat: drpcli can watch machine and job logs similar to "tail -f"
feat: drpcli gohai on windows will generate enough data to process data.
feat: drpcli license commands to report on limits and update licenses from the CLI. Includes paygo report generation and submission.
feat: drpcli runner can be installed as non-root user
feat: during image-deploy allow for user-data to be templatized
feat: Esxi8 support
feat: General cleanup of all bootenvs for latest updates
feat: grouping subtables in tables
feat: Rework Bootenv management to enable HTTP/HTTPS booting and better directory management
feat: show webhook url in triggers editor
feat: Tech preview of grafana service running in a container on the DRP endpoint
feat: Tech preview of nagios service running in a container on the DRP endpoint
feat: Tech preview of prometheus service running in a container on the DRP endpoint
feat: track workorder type and owner for batch operations
feat: Update bootenvs for https/http boot feature
feat: Update classifier editor to work with universal classifiers
feat: Update flash processing for HP systems that incorrectly report lack of reboot requirement
feat: Update in_subnet classifier function to take multiple subnets
feat: Update info structure to have more stats elements
feat: update raid support for perccli2 based controllers
feat: Update sledgehammer to include E810 class of nic drivers
feat: User modified and created data for all objects
feat: vmware-lib improves boot order change support and other cleanups/minor enhancements
fix: Add tracking parameters to monitor dell idrac cert installation
fix: allow enforce-sledgehammer to reboot once and then fail if not in sledgehammer
fix: Allow for handling contexts in AWS that can't talk to themselves
fix: Allow for runner to handle reboot and kexec cleaner and avoid looping
fix: bootstrap contexts error not uploading contexts if first is already available
fix: broker schema processing
fix: catalog url null ptr
fix: cluster profiles should be bounded by tenants
fix: column capitalization
fix: Connections List data type return
fix: Correct error in stats accumulation
fix: Correct error in virtual / physical machine counting
fix: curl example references
fix: deadlock in logging during HA consensus failover
fix: expand and fix meta data parameter limitors
fix: filter deselecting on editor open
fix: Fix deadlock in immutable indexes
fix: fix firmware package change in building sledgehammer
fix: fix flash issues when using ilorest 4.6 on HP systems. HP tool chnages in 4.6
fix: fix new return codes from ilorest
fix: fix redfish virtual media attach issue
fix: HA reenrollment was not working
fix: HA replication error around content packs and plugins
fix: ha status view
fix: handle alma and almalinux usage
fix: Handle bios failures cleaner with newer vendor tools
fix: handle ipmi/password with single quote
fix: handle multi-site manager endpoint user information changes
fix: handle multiline rsa key files
fix: icon references
fix: Idle connections could leak in the API client
fix: image-deploy parsing issues in cloud-init on Windows systems
fix: infinite loading issue
fix: inventory issue around virtual memory dimms
fix: IPMI cert installation timing needed modifying
fix: IPMI plugin could crash during scanning
fix: ipmi scan view updated after usage
fix: issue with accessing agent in /curtin directory
fix: issue with clearing last attempted bios configuration
fix: issues with aggregate parameters and searching
fix: machine migration could loss data because of format changes.
fix: make dell flash and bios tasks reboot or fail when pending jobs are still found
fix: Make files explode work like previous versions
fix: Make some template errors soft to avoid restart failures (important for HA startup)
fix: Make sure license counts are start on database load
fix: missing links replaced
fix: missing template flag check
fix: multi-site manager does not require sync users to operate
fix: Multisite manager should allow for AD auth users
fix: multisite manager should reevaulate all endpoints on catalog rebuilds
fix: On startup, allow the static secure fileserver and api server use the same certificate
fix: pool actions should use pool actions instead of machine updates
fix: primitive types should not have double quotes
fix: reduce log noise on some tasks
fix: Remove memory leak in parameter renderer
fix: remove race condition in write layer during content loading
fix: remove socket connection leaks in plugin clients
fix: rework the data stack increase performance and accessibility
fix: some parameter defaults when unset
fix: subnets should not be null referenced
fix: supermicro bios configuration issue with numbers versus number strings
fix: table reload crash
fix: update classfier / ux-views meta data helpers
fix: update content pack prereqs for correctness
fix: Update install.sh for more error checking
fix: Update install.sh to disable checksum processing on local zip files
fix: Update install.sh to handle selinux better
fix: Update install.sh to include better airgap handling
fix: update reservation information on IPMI scanning
fix: Update the cli documentation generator to index and tag appropriately
fix: update timing issue in data store renaming on esxi
fix: update ubuntuhammer references
fix: visualization of some elements to more usable form
perf: Add some object caching to improve references
perf: Client template rendering improvements for performance and garbage collection
perf: Improve general perfomance with many active machines
perf: Improve StringExpand performance
perf: Optimize memory and storage for faster look ups and sorting
perf: Reduce memory consumption in parameter set/get operations
perf: Reduce mutex overhead for stats accumulation
perf: Speed up job purging
perf: update event fowarding to use preprocessed actions and pool checks to reduce calculations
perf: update event system to copy less data
perf: Update index walk functions to iterators
sec: Actions clean function to cull passwords and other sensitive information
sec: Encrypt all secure data upon load and remove that as an option for the user
sec: Update for GO lang vulns. Many published by golang tools
```

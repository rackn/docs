---
title: v3.x
tags:
  - reference
  - operator
  - release
---

!!! note

    Support for this release of Digital Rebar is no longer available.

Historical notes about v3 releases.

v3.6.0 to v3.7.0
----------------

The plugin system has been updated to a new version. All plugins have
been updated to use the new version. After updating to *v3.7.0*, all
plugins must be updated to function. The system will start after update,
but the plugin-providers will not load until they are udpated. Use the
RackN UX to get the updates for the plugins.

The Task subsystem has been updated to default to
[sane-exit-codes]{.title-ref}. This is a change from the default of
[original-exit-codes]{.title-ref}. This was done to address the need of
task authors to match some basic assumptions about exit codes. *1*
should be a fail and not reboot your box.

---
title: v4.1 [Fall 2019]
tags:
  - reference
  - operator
  - release
---

Release Date: missing

Release Themes: missing

Digital Rebar v4.1 release notes are not available at this time.

!!! note

    Support for this release of Digital Rebar is no longer available.

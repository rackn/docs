---
title: Role
tags:
  - reference
  - architecture
  - role
---

# Role {#rs_arch_models_role}

Roles are licensed features. To perform any interaction with a role besides listing them and getting them, you must have a license with the `rbac` feature enabled.

Roles are named lists of claims that can be assigned to a user.

Roles are also partially ordered by the access they grant based on their claims. Role `a` is said to contain role `b` if every claim `b` contains can be satisfied by a claim on `a`.

DRP provides a default `superuser` role that contains the `superuser` claim. By default, the `rocketskates` is assigned this role.

Collectively, roles and claims control what a caller can do with the API.

## Claim {#rs_arch_models_claim}

Claims grant the ability to perform specific actions against specific objects.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli roles fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

## Actions

Two actions have specialized semantics.

<div class="first-column-single-line" ></div>
Action                | Description
----------------------|------------
`action`              | By itself, `action` gives permission for all valid plugin-provided actions.
`action:<actionName>` | This gives permission for the specific action name. If you want to give access to more than one plugin-provided action, you can specify multiple instances in the comma-separated list.
`update`              | By itself, `update` gives permission to update any field (including params, if the final object has them).
`update:<field>`      | The field part after the colon must be a valid RFC6901 JSON pointer to the field you want to allow to be updated. This will allow updated to that field and any subfields it might have. This level of access control works on the JSON representation of the object.

Claims are partially ordered by the access they grant, with the superuser claim `{Scope:"*" Action:"*" Specific:"*"}` granting access to everything and the empty claim `{Scope:"" Action:"" Specific:""}` granting access to nothing. If you have two claims `a` and `b`, claim `a` is said to contain claim `b` if a is capable of satisfying every authentication request `b` is.


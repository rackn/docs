---
title: Identity Provider
tags:
  - reference
  - architecture
  - identity-provider
---

# Identity Provider {#rs_identity_provider}

The identity provider object defines an external authorization provider to access through SAML.

The IDP object defines the URL to the metadata or, if that isn't available, the metadata XML blob itself. This is used to define the security features of the IDP and DRP relationship.

Additionally, fields can be used to customize the user experience.

* `LogoPath` | Defines URL or DRP files icon for display by the UX.
* `DisplayName` | String to display with the logo.

Once identity has been confirmed, the IDP object defines who the user should be added to the system and what accesses that user is allowed.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli identity_providers fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: User
tags:
  - reference
  - architecture
  - user
---

# User {#rs_model_user}

The User Object controls access to the system. The user object contains a name and a password hash for validating access. Additionally, it can be used to generate time-based, function restricted tokens for use in API calls. The template provides a helper function to generate these for restricted machine access in the discovery and post-install process.

The user object is usually created with an unset password. Thus the user will have no access but still be able to access the system through constructed tokens. The cli has commands to set the password for a user.

Users keep track of who is allowed to talk to DRP, and what actions they are allowed to take in the system.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli users fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

In addition to the roles asigned to the user, all users also get a claim that allows them to get themself, change their passwords, and get a token for themselves.



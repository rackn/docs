---
title: Tenant
tags:
  - reference
  - architecture
  - tenant
---

# Tenant {#rs_model_tenant}

Tenants are licensed features. To perform any interaction with a tenant besides listing them and getting them, you must have a license with the `rbac` feature enabled.

Tenants control what objects a user can see via the DRP API.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli tenants fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

Object visibility restrictions based on a tenant are processed before roles are processsed, which means that a role granting access to an object that is not allowed by the tenant will be ignored.

By default, users are not members of a tenant, and can therefore potentially see everything via the API (subject to role based restrictions).


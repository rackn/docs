---
title: Role-Based Access Control
tags:
  - reference
  - architecture
  - user
  - tenant
  - role
  - identity-provider
---

# Role-Based Access Control {#rs_arch_auth_models}

The Role-Based Access Control (rbac) objects control who can access and what they can do.

* [](#rs_model_user) - Defines a local user that can access the system
* [](#rs_arch_models_role) - Defines what a user or token can do in the system
* [](#rs_model_tenant) - Defines a subset of the objects that a user can operate upon

The system also contains an additional object for supporting SAML.

* [](#rs_identity_provider) - Defines a SAML interaction and how to map SAML Users to RBAC objects.

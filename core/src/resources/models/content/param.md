---
title: Param
tags:
  - reference
  - architecture
  - param
---

# Param {#rs_arch_models_param}

The param object is the lowest level building block. It is a simple key/value pair. Each param is a bounded type parameter, and type definition is enforced.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli params fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

Some example types that can be used with Schema:

Type     | Description
---------|------------
integer  | A numerical value (eg `12` or `-3444`)
boolean  | True or False (`true` or `false`)
string   | Textual string (eg `"this is a string!"`)
array    | A series of elements of the same type
map      | A higher-order function that applies a given function to each element of a list, returning a list of results in the same order.


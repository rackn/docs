---
title: Workflow
tags:
  - reference
  - architecture
  - workflow
---

# Workflow {#rs_arch_models_workflow}

A workflow defines a series of stages that a machine should go through.

Workflows have the following fields:

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli workflows fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

When the workflow field on a machine is set, the current task list on the machine is replaced with the results of expanding each stage in the workflow using the following items:

-   stage:stageName
-   bootenv:bootEnvName (if the stage has a non-empty bootenv field)
-   task0...taskN (the content of the stage's `Tasks` field)

Additionally, the `Stage` and `BootEnv` fields of the machine become read-only, as stage and bootenv transitions will occur as dictated by the machine task list, and when the stage changes it does not affect the task list.


---
title: Stage
tags:
  - reference
  - architecture
  - stage
---

# Stage {#rs_arch_models_stage}

Stages are used to define a set of tasks that must be run in a specific order, potentially in a specific bootenv.

Stages may also provided rendered templates while the Machine, Cluster, or Resource Broker is in that stage.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli stages fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


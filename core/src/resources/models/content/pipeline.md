---
title: Pipeline
tags:
  - reference
  - architecture
  - pipeline
---

# Pipeline {#rs_arch_pipeline}

![pipeline image](built_images#arch-pipeline.svg)

A Pipeline is a Profile with:
    
* Name field starts with *universal-application-*

    !!! example

        universal-application-centos-8

* The `universal/application` parameter set to pipeline name (without the *universal-application-*)

    !!! example

        The Pipline `universal-application-centos-8` would have a value of `centos-8`

* Chain of Pipeline Workflows to Apply to the Machine/Cluster/ResourceBroker
    * The Chain is looked up in the default map by `universal/application` value
        * The default map is in the parameter, `universal/workflow-chain-map`.
        * A custom map could be provided directly in this parameter.
    * Or overridden by the *universal/workflow-chain-index-override* parameter
* A set of parameters that will customize the workflows as needed.

A Machine/Cluster/ResourceBroker (referred to as Machine) will execute a pipeline when the pipeline profile is
added to the machine, the machine is in workflow mode, the machine is Runnable, and the
Machine's workflow is set to a workflow in the chain.

The workflow chain is a directed graph with multiple starting points.  The starting points are green diamonds in the picture above.  
The most common starting point is `universal-discover`, but other entry points are possible.  For example, cloud instances
use `universal-start` instead.


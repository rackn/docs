---
title: BootEnv
tags:
  - reference
  - architecture
  - bootenv
---
# BootEnv {#rs_model_bootenv}

The bootenv object defines an environment to boot a machine. It has two main components: an OS information section, and a templates list. The OS information section defines what makes up the installation base for this bootenv. It defines the install ISO, a URL to get the ISO, and SHA256 checksum to validate the image. These are used to provide the basic install image, kernel, and base packages for the bootenv.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli bootenvs fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


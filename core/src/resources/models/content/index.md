---
title: Content Objects
tags:
  - reference
  - architecture
  - task
---

# Content Objects {#rs_provisioning_models}

![content objects image](built_images#arch-content.svg)

Content objects define the how things are going to be done.

The basic unit of work in the system is that Task.  These can be grouped into commonly sequenced lists ina a Stage.  Stages can be sequenced in Workflows or Blueprints.

Blueprints are used when a Machine, Cluster, or Resource Broker is in [Work Order Mode](#rs_arch_work_order_mode).

Workflows are used when a Machine, Cluster, or Resource Broker is in [Workflow Mode](#rs_arch_workflow_mode).

Profiles allow for grouping of Parameters for the Content Objects and Machines, Clusters, or Resource Brokers.

A Pipeline is a special Profile that drives the [universal application process](#rs_universal_arch) following the [process](#rs_arch_usage_pipeline).

Parameters define the type and default value of an instance of it actualy stored in one of the Parameter objects.

Bootenvs define environments that a Machine can be booted into.  [Booting](#rs_arch_usage_boot).  Repo is an internally generated object that allows for Templates to render installation and package repositories.

Templates are data that can be rendered using golang text templating.  These allow rendering Parameters from all the Content objects.


| Field        | Definition |
| -------------|----------- |
<<<<< drpcli contents fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

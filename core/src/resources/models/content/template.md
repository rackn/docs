---
title: Template
tags:
  - reference
  - architecture
  - template
---

# Template {#rs_model_template}

The template object defines a templated content that can be referenced by its ID. The content of the template (or in-line template in a bootenv object) is a [golang text/template](https://golang.org/pkg/text/template/#hdr-Actions) string. The template has a set of special expansions. The normal expansion syntax is:

```
{{ .Machine.Name }}
```

This would expand to the machine's `Name` field. There are helpers for the parameter spaces, the bootenv object, and some miscellaneous functions. Additionally, the normal golang text/template functions are available as well. Things like `range`, `len`, and comparators are available as well. Template inclusion is supported by the following syntax:

```
{{ template "ID of Template" . }}
{{ template .Param.MyFavoriteTemplate . }}
```

The following table lists the current set of expansion custom functions.

<div class="first-column-single-line" ></div>
Expansion                        | Description
---------------------------------|------------
`.ApiHostURL`                    | HTTPS URL to access the DRP API uisng the server hostname.
`.ApiURL`                        | HTTPS URL to access the DRP API.
`.BootParams`                    | Renders the `bootparam` field of the bootenv. Template expansion applies to that field as well.
`.ComposeParam <key>`            | Returns the composed value of a parameter across all param sources instead of just the highest precedence.
`.CurrentJob`                    | Returns the UUID of the job running the template expansion (if rendering in a job context). This handles differentiating figuring out if the task is a work order or not.
`.Env.InstallUrl`                | HTTP URL to the base ISO install directory.
`.Env.JoinInitrds <protocol>`    | A comma separated string of all the initrd files specified in the bootenv reference through the specified protocall (either `tftp` or `http`).
`.Env.OS.Family`                 | An optional string from the bootenv that is used to represent the OS family. Ubuntu preseed uses this to determine Debian  or Ubuntu as an example.
`.Env.OS.Version`                | An optional string from the bootenv that is used to represent the OS version. Ubuntu preseed uses this to determine what version of ubuntu is being installed.
`.Env.PathFor <protocol> <file>` | This references the boot environment and builds a string that presents a either a TFTP or HTTP specifier into exploded ISO space for that file. The protocol is either `tftp` or `http`. The file is a relative path inside the ISO.
`.GenerateToken`                 | This generates a limited use access token for the machine to either update itself, or create a new machine. The token's validity is limited in time by [](#rs_model_prefs).
`.InstallRepos`                  | Returns exactly one repo from the list chosen by `MachineRepos` function that has `installSource` set, and at most one repo from the `MachineRepos` function that has `securitySource` set.
`.Machine.Address`               | The `Address` field of the machine.
`.Machine.CurrentJob`            | Returns the UUID of the job running the template expansion (if rendering in a Job context).<br /><br />Note: use `.CurrentJob` in DRP v4.10+.
`.Machine.HexAddress`            | The `Address` field of the machine in hex (useful for elilo config files).
`.Machine.Name`                  | The FQDN of the machine in the machine object stored in the `Name` field.
`.Machine.Path`                  | A path to a machine's unique space in the file server name space.
`.Machine.ShortName`             | The name part of the FDQN of the machine object stored in the `Name` field.
`.Machine.Url`                   | A HTTP URL that references the machine's unique filesystem space.
`.Machine.UUID`                  | The machine's UUID field.
`.MachineRepos`                  | Returns all repos that have the OS of the machine defined in their os section.
`.ObjectExpand <object>`         | Returns the expanded object from the provided object. This can iterate through arrays and maps.
`.Param <key>`                   | Returns the structure for the specified key for this rendering.
`.ParamAsJSON <key>`             | Return the specified parameter as a JSON string or an error.
`.ParamAsYAML <key>`             | Return the specified parameter as a YAML string or an error.
`.ParamCompose <key>`            | Same as `.ComposeParam`, but matches the other param function naming conventions.
`.ParamComposeExpand <key>`      | Returns the specified parameter after composing and then doing string expansion.
`.ParamExists <key>`             | Returns true if the specified key is a valid parameter available for this rendering.
`.ParamExpand <key>`             | Runs templating on the contents of a parameter before returning it. It iterates through maps and arrays to find expandable strings.
`.ParseURL <segment> <url>`      | Parse the specified URL and return the segment requested. Supported segments follow an be one of the following schemes `https`, `host` (eg: `drp.example.com:8092`), or `path` (eg: `/api/v3/machines`). Note that `host` does not separate name and port.
`.ProvisionerAddress`            | An IP address that is on the provisioner that is the most direct access to the machine.
`.ProvisionerHostURL`            | An HTTP URL to access the base file server root using the server hostname.
`.ProvisionerURL`                | An HTTP URL to access the base file server root.
`.Repos <tag>, <tag>, ...`       | Returns repos as defined by the package-repositories param currently in scope with the matching tags.
`.SecureProvisionerHostURL`      | An HTTPS URL to access the base file server root using the server hostname.
`.SecureProvisionerURL`          | An HTTPS URL to access the base file server root.
`.StringExpand <string>`         | Returns the expanded string from the provided template string.
`.Task.TargetOS`                 | Returns a string that presents the OS of the runner. This can be `esxi`, `linux`, `darwin`, or `windows`.
`.WorkOrder.CurrentJob`          | Returns the UUID of the job running the template expansion (if rendering in a job context).
`.WorkOrder.Machine`             | The Machine's UUID field in the work order.
`.WorkOrder.UUID`                | The work order's UUID field.
`template <string> .`            | Includes the template specified by the string. The string can be a variable<br /><br />Note: `template` does not have a dot (.) in front of it.

!!! note

    `.GenerateToken` generates either a known or unknown token for use by the template to update objects in DRP. The tokens are valid for a limited time as defined by the `knownTokenTimeout` and `unknownTokenTimeout` preferences. The tokens are also restricted to the function they can perform. The known token is limited to only reading and updating the specific machine the template is being rendered for. If a machine is not present during the render, an unknown token is generated that has the ability to query and create machines. These are used by the install process to indicate that the install is finished and that the `local` bootenv should be used for the next boot and during the discovery process to create the newly discovered machine.

!!! note

    `.Machine.Path` is particularly useful for ensuring that templates are expanded into a unique file space for each machine. An example of this is per machine kickstart files. These can be seen in the [ubuntu-16.04-install](https://gitlab.com/rackn/provision-content/blob/v4/content/bootenvs/ubuntu-16.04.yml) bootenv.

With regard to the `.Param` and `.ParamExists` functions, these return the parameter or existence of the parameter specified by the `key` input. The parameters are examined from most specific to global. This means that the Machine object's profile is checked first, then the list of profiles associated with the machine, and finally the global profile. The parameters are stored in a profile.

The default template and bootenv use the following optional (unless noted with "*") parameters.

  Parameter                           | Type             | Description
  ------------------------------------|------------------|-----------
  `ntp_servers`                       | Array of string  | The format is an array of IP addresses in dotted quad format.
  `proxy-servers`                     | Array of objects | See the parameter definition.
  `operating-system-disk`             | String           | A string to use as the default install drive. Examples include `/dev/sda`, or `sda` depending upon kickstart or preseed.
  `access-keys`                       | Map of strings   | The key is the name of the public key. The value is the public key. All keys are placed in the `.authorized_keys` file for the root account.
  `provisioner-default-password-hash` | String           | The password hash for the initial default password, `RocketSkates`
  `provisioner-default-user`          | String           | The initial user to create for ubuntu/debian installs
  `dns-domain`                        | String           | DNS Domain to use for this system's install
  `operating-system-license-key` *    | String           | Windows only
  `operating-system-install-flavor` * | String           | Windows only

For some examples of this in use, see [](#rs_ops) as well as the example profiles in the assets [](#rs_install) directory.

## Sub-templates

A template may contain other templates as described above. The system comes with some pre-existing sub-templates to make kickstart and preseed generation easier. The following templates are available had have some parameters that drive them. The required parameters can be applied through profiles or the machine profile. The templates contain comments with how to use and parameters to set.

## JSON Template Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli templates fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: Repo
tags:
  - reference
  - architecture
  - repo
---

# Repo Object {#rs_arch_models_repo}

During the template rendering, the template-level `.Repos`, `.MachineRepos`, and `.InstallRepos` functions return a list of repo objects that can be used for further template expansion. The repo object contains its own fields and functions that can be used for template expansion.

<div class="first-column-single-line" ></div>
Expansion                |Description
-------------------------|-----------
`.Tag`                   |The tag that uniquely identifies one repository definition. The template-level `.Repos` function takes a list of tags and returns repos that exactly match them.
`.OS`                    |A list of operating systems (in distro-release format) that this repository supports. The template-level `.MachineRepos` function matches this field against the current `.Machine.OS` field to determine which templates are applicable to a machine.
`.URL`                   |The URL to the top of the repository in question. For yum-style repos, it can either point directly to a specific repository (in which case `.Distribution` and `.Components` fields must not be present), or point to a location that contains an appropriately mirrored repo tree for the OS in question (in which case it cannot be used as an `InstallSource` or a `SecuritySource`, and `.Distribution` and `.Components` must be set). For apt-style repos, it must point to the top level of the repository (the level that has `dists` and `pool` as subdirectories), and `.Distribution` and `.Components` must always be defined.
`.PackageType`           |An optional field that can be used to determine what kind of packages the repository returns. It is normally autodetected based on the operating system the repo is being used in.
`.RepoType`              |The type of repository this is. It is optional, and is normally inferred based on the operating system the repo is being used in.
`.InstallSource`         |A boolean value that determines whether this repository should be used as a package source during OS installation. You should have at most one of these per OS install you wish to support.
`.SecuritySource`        |A boolean value that determines whether this repository should be used as a source of security updates that should be applied during an OS install.
`.Distribution`          |A string that corresponds to the OS release version or codename. This must be present for apt-style repos.
`.Components`            |A list of strings that map to any sub-repositories available as part of this repository. Examples are `main`,`contrib`, and `non-free` for apt-based repos.
`.R`                     |A helper function that refers back to the top-level template rendering context.
`.JoinedComponents`      |A helper function that joins the `.Components` list into a space-separated string.
`.UrlFor <component>`    |A helper function that returns an appropriately formatted URL for the passed component.
`.Install`               |A helper function that returns the repo in a format suitable for inclusion in an unattented OS installation file (kickstart, preseed, etc.) The format returned is currently hardcoded depending on the OS type of the machine. That restriction will be lifted in future versions of dr-provision.
`.Lines`                 |A helper function that returns the repo in a format suitable for direct inclusion into a repo definition file (`sources.list`, `/etc/yum.repos.d/some.repo`, etc). The format returned is currently hardcoded based on the OS type of the machine. That restriction will be lifted in future versions of dr-provision.

## Expanding Package Repositories

To expand the repos suitable for OS installation, use:

``` go
{{range $repo := .InstallRepos}}{{$repo.Install}}{{end}}
```

To expand the repos suitable for post-install package management, use:

``` go
{{range $repo := .MachineRepos}}{{$repo.Lines}}{{end}}
```
## Local Repos

By default, local package repositories provided by exploded OS install media is made available as a set of extra repositories that override matching install repositories from the package-repositories param.

These extra repositories will always be present if the ISO associated with a bootenv has been uploaded to DRP. They are treated as if you defined a custom entry in the `package-repositories` param that pointed back to the location the ISO exploded to + the location we expect the package repositories on the ISO to be at. They will always behave as if they were install sources.

!!! example

    ``` yaml
    - tag: <bootenv.Name>
      os: [<bootenv.OS.Name>]
      installSource: true
      securitySource: true
      url: http://<dr-provision address>:8091/<bootenv.OS.Name>/install/<on-media path to repo>
    ```


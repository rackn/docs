---
title: Profile
tags:
  - reference
  - architecture
  - profile
---

# Profile {#rs_model_profile}

The profile object defines a set of key/value pairs (or parameters). All of these may be manipulated by the API. The key space is a free form string and the value is an arbitrary data blob specified by JSON through the API. The common parameters defined in templates can be set on these objects. The system maintains a global profile (called `global`) for setting system wide parameters. They are the lowest level of precedence.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli profiles fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

The profiles are free form dictionaries and default empty. Any key/value pair can be added and referenced.

Other profiles may be created to group parameters together to apply to sets of machines. The machine's profile list allows the administrator to specify an ordered set of profiles that apply to that machine as well.

When the system needs to render a template parameter, the machine's specific parameters are checked, then the order list of profiles stored in the machine object are checked, workorder parameters and profiles are checked, stage parameters and profiles are checked, and finally the `global` profile is checked. The key and its value are used if found in template rendering.  If after all
evaluations are considered, the Parameter object is checked for a default value.

!!! note

        When updating the params section of a profile object, using the API PUT method will replace the Params map with the map from the input object. The API PATCH method will merge the params map in the input with the existing params map in the current profile object. The API POST method on the params subaction will replace the map with the input version.


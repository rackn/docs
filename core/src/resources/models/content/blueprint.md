---
title: Blueprint
tags:
  - reference
  - architecture
  - blueprint
---

# Blueprint {#rs_arch_models_blueprint}

![blueprint picture](built_images#arch-blueprint.svg)

The Blueprint defines a list of tasks or stages that should be executed by a runner.  The Blueprint is used by the Work Order as a reusable task specifier.

The Task list in the Blueprint contains either task names, stage names with a `stage:` prefixed to the name,  or actions.

The Blueprint does not allow for context switching or bootenv changing within the task list.

Unlike Workflows, Blueprints allow for specifying parameters or profiles.  This allows for customization of the tasks without requiring the single stage / task pair pattern.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli blueprints fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


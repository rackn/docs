---
title: Task
tags:
  - reference
  - architecture
  - task
---

# Tasks {#rs_arch_models_task}

Tasks in DRP represent the smallest discrete unit work that the runner can use to perform work on a specific machine. The runner creates and executes a job for each task on the machine.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli tasks fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


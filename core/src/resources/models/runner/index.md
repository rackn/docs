---
title: Runner Objects
tags:
  - reference
  - architecture
  - context
  - machine
  - cluster
  - resource-broker
---

# Runner Objects {#rs_res_objs_runner}

The Runner Objects provide the basis for executing tasks.

The primary objects that track task execution and control runners are:

* [](#rs_model_machine)
* [](#rs_model_cluster)
* [](#rs_model_resource_broker)
* [](#rs_work_order)

These objects maintain the task list, the task index, and parameters to
control and provide the rendered items to execute.

The [](#rs_work_order) provides the task control function when the other
three are in work order mode.

## Execution Environments

By default, the assumption is that the runner objects are running in the Machine's
context.  The Context field is empty.

The [](#rs_arch_models_context) defines container-based execution environments that can be used by
[](#rs_content_docker-context).

## Pools

[](#rs_pooling_arch) and [](#rs_pooling_ops) describe the architectural and operations of Pooling.

The [](#rs_model_pool) is used for non-empheral pools.  The JOIN, LEAVE, ALLOCATE, RELEASE actions and parameters
are defined in the object.



---
title: Pool Object
tags:
  - reference
  - architecture
  - pool
---

# Pool Object {#rs_model_pool}

The Pool Object defines the configuration parts of the pool for non-emphemeral usage.

The Pool contains a PoolTransitionActions data structure for when a machine

* ENTER - EnterActions
* ALLOCATE - AllocateActions
* RELEASE - Release Actions
* EXIT - ExitActions

The PoolTransitionActions has the fields:

* Workflow - string - Workflow defines the new workflow the machine should run
* AddProfiles - list of strings - AddProfiles defines a list of profiles to add to the machine
*	AddParameters - object of key/value pairs - AddParameters defines a list of parameters to add to the machine
*	RemoveProfiles - list of strings - RemoveProfiles defines a list of profiles to remove from the machine
*	RemoveParameters - list of strings - RemoveParameters defines a list of parameters to remove from the machine

The Pool also allows for definition of the parent pool for releasing machine back into that pool.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli pools fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

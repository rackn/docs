---
title: Cluster
tags:
  - reference
  - architecture
  - cluster
---

# Cluster {#rs_model_cluster}

The cluster object defines a special class of machines. While the API presents these as clusters, the underlying system represents them as a machine and all the normal machine-like actions apply to clusters. The cluster is signified by the meta data field `machine-role` set to the value of `cluster`.

The cluster automatically upon creation receives a new profile named the same as the cluster. This profile is added to the cluster object. This profile is intended to hold cluster specific parameters that can be shared with cluster members.

Clusters can run pipelines and jobs. The default context for a cluster at creation is `drpcli-runner`. This will attempt to run within the docker-context of the DRP endpoint.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli clusters fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

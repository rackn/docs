---
title: Resource Broker
tags:
  - reference
  - architecture
  - resource-broker
---

# Resource Broker {#rs_model_resource_broker}

The resource broker object defines a special class of machines. While the API presents these as resource brokers, the underlying system represents them as a machine and all the normal machine-like actions apply to resource broker. The resource broker is signified by the `Meta` data field `machine-role` set to the value of `resource-broker`.

When a new resource broker is created, it receives a new profile named the same as the resource broker.

Resource brokers can run pipelines and jobs. The default context for a cluster at creation is `drpcli-runner`. This will attempt to run within the `docker-context` of the DRP endpoint.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli resource_brokers fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

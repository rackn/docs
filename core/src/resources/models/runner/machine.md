---
title: Machine
tags:
  - reference
  - architecture
  - machine
---

# Machine {#rs_model_machine}

The machine object defines a machine that is being provisioned. The machine is represented by a unique UUID. The UUID is immutable after machine creation. The machine's primary purpose is to map an incoming IP address to a bootenv. The bootenv object provides a set of rendered templates that will be used to boot the machine. The machine provides parameters to the templates. The machine provides configuration to the renderer in the form of parameters and fields. Also, each machine must have a bootenv to boot from. If a machine is created without a bootenv specified, DRP will use the value of the `defaultBootEnv` preference.

Machines are what DRP uses to model a system as it goes through the various stages of the provisioning process. As such, machine objects have many fields used for different tasks.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli machines fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

The `Name` field should contain the FQDN of the node.

The machine object contains an `Error` field that represents errors encountered while operating on the machine. In general, these are errors pertaining to rendering the bootenv.

Machine parameters are defined as a special profile on the machine. The profile stores a dictionary of string keys to arbitrary objects. These could be strings, booleans, numbers, arrays, or objects representing similarly defined dictionaries. The machine parameters are available to templates for expansion.

Machines maintain an ordered list of profiles that are searched in that order, with the `global` profile searched last.

!!! note

    When updating the params part of the embedded profile in the machine object, using the PUT method will replace the params map with the map from the input object. The PATCH API method will merge the params map in the input with the existing params map in the current machine object. The POST method on the params subaction will replace the map with the input version.

A machine is defined by having the meta data field `machine-role` set to `machine` or `self`. For backwards compatability, a machine without a `machine-role` meta data entry is considered a machine.

## Job Monitoring

To get [Websocket](#rs_websockets) events for jobs for a specific machine, the `machine_jobs.*.<uuid>` register can be used.

---
title: Context
tags:
  - reference
  - architecture
  - context
---

# Context {#rs_arch_models_context}

Contexts allow DRP to encapsulate the idea that we can run tasks for a machine in several different places, of which the default is on the machine itself.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli contexts fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

## Plugins that Provide Engines

By convention, a plugin that provides support for using contexts must support the following actions.

### imageUpload
This action runs against the plugin, and takes two arguments: `context/image-name`: which is a string containing the name of the image being uploaded, `context/image-path`, which is a string containing the location the image is being uploaded from.

This action must result in the artifact at `context/image-path being` made available to create execution environments from using `context/image-name`.

In general, `context/image-path` should either be a URL that points to the location of the artifact, or a relative path indicating where the artifact is located at on DRP's static file server.

By convention, images stored on the static file server should be stored at `files/contexts/<plugin-name>/<image-name>`, with no extra file extensions. Doing so will allow the `agentStart` action to automatically load the image on demand, otherwise the image must have already been uploaded before executing the `agentStart` action.

!!! example "Example imageUpload actions."

        ``` shell
        drpcli plugins runaction docker-context imageUpload context/image-name foo:latest context/image-path https://your.image.repo/path/to/foo:latest
        ```
        This will have the `docker-context` plugin load the image `foo:latest` from the upstream repo.
        ``` shell
        drpcli plugins runaction docker-context imageUpload context/image-name foo:latest context/image-path files/contexts/docker-context/foo:latest
        ```
        This will have the `docker-context` plugin load the image `foo:latest` from the DRP static file server at `files/contexts/docker-context/foo:latest`.

### imageExists

This action runs against the plugin, and takes one argument: `context/image-name`, which is the name of the image we are testing to see if it already exists and returns `true` if the image exists, `false` if it does not.

!!! example "Example ImageExists action."

        ``` shell
        drpcli plugins runaction docker-context imageExists context/image-name foo:latest
        ```
        Returns `true` if `foo:latest` is present, and `false` if it is not.

### imageRemove

This action runs against the plugin, and takes one argument: `context/image-name`, which is the name of the image to remove and returns true if either the image was removed or the image did not exist, false and an error otherwise.

!!! example "Example imageRemove."

        ``` shell
        drpcli plugins runaction docker-context imageRemove context/image-name foo:latest
        ```
        This will have `docker-context` remove the image `foo:latest` if it exists.

### agentStart

This action runs against the machine, and takes two arguments: `context/image-name`, which is the name of the image to use, `context/name`, which is the name of the context that should be used to run tasks for the machine the action it was invoked with.

This action causes the engine to start a new execution context based on the image, and arrange for `drpcli processjobs` to be called with the following environment variables set:

-   `RS_UUID`: the UUID of the machine the action was invoked with.
-   `RS_TOKEN`: an auth token suitable for machines running tasks against themselves. Unless otherwise required by the jobs to be run in the context, this token should be the results of calling `/api/v3/machines/\<uuid\>/token?ttl=3y` when the `agentStart` Action is called.
-   `RS_CONTEXT`: The name of the context that the agent should use when listening for machine state changes and job creation requests.
-   `RS_ENDPOINTS`: A space-separated list of all the endpoints that the agent should try to use when connecting to the DRP endpoint. The agent will stop at the first one that it successfully connects to.

In general, you should only need to invoke this action manually when testing new images or testing a new plugin. In the normal course of operation, plugins handle starting and stopping agents automatically based on changes on machine context fields.

!!! example "Example agentStart"

    ``` shell
    drpcli machines runaction Name:test agentStart context/image-name foo:latest context/name foo
    ```
    This will start an agent for the machine running in the `foo` context on the `foo:latest` image.

### agentStop

This action runs against the machine, and takes one argument: `context/name`, which is the name of the context used in the corresponding `agentStart` action.

This action tears down the execution context created by the corresponding `agentStart` action. In general, you should only need to invoke this action manually in the course of testing a new plugin, plugins handle stopping agents automatically based on machine context field state changes.

!!! example "Example agentStop"

        ``` shell
        drpcli machines runaction Name:test agentStop context/name foo
        ```
        This will stop the agent running in the `foo` context for the machine.

Additionally, all plugins that provide support for contexts must subscribe to the event stream that DRP emits. They must watch for machine delete, create, update, and save events to set up and tear down execution environments as appropriate.


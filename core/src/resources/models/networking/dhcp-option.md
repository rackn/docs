---
title: DHCP Option
tags:
  - reference
  - architecture
  - subnet
  - reservation
---

# DHCP Option {#rs_arch_models_networking_dhcp_option}

The DHCP Option object holds templated values for DHCP options that should be returned to clients in response to requests.

<div class="first-column-single-line" ></div>
Field   | Definition
--------|-----------
`Code`  | A byte that holds the numeric DHCP option code. See [RFC 2132](https://tools.ietf.org/html/rfc2132) and friends for what these codes can be.
`Value` | A string that will be template-expanded to form a valid value to return as the DHCP option. Template expansion happens in the context of the source options.

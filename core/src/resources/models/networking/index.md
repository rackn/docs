---
title: Networking Objects
tags:
  - reference
  - architecture
  - dhcp
---

# Network Objects {#rs_arch_models_networking}

![networking objects image](built_images#arch-networking.svg)

These models manage how the DHCP and DNS servers built into DRP work. They determines what IP addresses it can hand out to which systems, what values to set for DHCP options, and how to handle DHCP requests at various points in the lifecycle of any given DHCP lease.  They also determine the record values returned to DNS queries.

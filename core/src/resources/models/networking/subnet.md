---
title: Subnet
tags:
  - reference
  - architecture
  - subnet
---

# Subnet {#rs_arch_models_networking_subnet}

![subnet object image](built_images#arch-subnet.svg)

The subnet object defines the configuration of a single subnet for the DHCP server to process. Multiple subnets are allowed. The subnet can be attached to a local interface on DRP or relayed to DRP via a DHCP relay.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli subnets fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

The subnet object defines the configuration of a single subnet for the DHCP server to process. Multiple subnets are allowed. The subnet can represent a local subnet attached to a local interface (broadcast subnet) to DRP or a subnet that is being forwarded or relayed (relayed subnet) to DRP.

The subnet is uniquely identified by its `Name` field. The subnet defines a CIDR-based range with a specific subrange to hand out for nodes that do not have explicit reservations (the `ActiveStart` thru `ActiveEnd` fields). The subnet also defines the `NextServer` field in the PXE chain. This is usually an IP associated with DRP, but if the provisioner is disabled, this can be any next hop server. The lease times for both reserved and unreserved clients as specified with the `ReservedLeaseTime` and `ActiveLeaseTime` fields. The subnet can also me marked as only working for explicitly reserved nodes using the `ReservedOnly` field.

## Options

The subnet also allows for the specification of DHCP options to be sent to clients. These can be overridden by a reservation's specific options.

Some common options are:

  Type    | #   | Description
  --------|-----|------------
  IP      | 3   | Default Gateway
  IP      | 6   | DNS Server
  IP      | 15  | Domain Name
  String  | 67  | Next Boot File - e.g. ipxe.pxe

!!! note

    Golang template expansion also works in these fields. This can be used to make custom request-based reply options.

    For example, this value in the Next Boot File option (67) will return a file based upon what type of machine is booting. If the machine supports, iPXE then an iPXE boot image is sent, if the system is marked for legacy bios, then ipxe.pxe is returned, otherwise return a 64-bit UEFI iPXE boot loader:

    ```
    {{if (eq (index . 77) "iPXE") }}default.ipxe{{else if (eq (index . 93) "0")}}ipxe.pxe{{else}}ipxe.efi{{end}}
    ```

The data element for the template expansion as represented by the `.` above is a map of strings indexed by an integer. The integer is the option number from the DHCP request's incoming options. The IP addresses and other data fields are converted to a string form (dotted quads or base 10 numerals).

!!! note

    Option 67 is optional. When using DRP as the DHCP server, it will generate a bootfile like the above template expansion.

The final elements of a subnet are the `Strategy` and `Pickers` options. These are described in the DRP API JSON description. They define how a node should be identified (`Strategy`), and the algorithm for picking addresses (`Pickers`). The strategy can only be set to `MAC` currently. This will use the MAC address of the node as its DHCP identifier. Others may show up in time.

## Pickers

The `Pickers` field defines an ordered list of methods to determine the address to hand out. Currently, this will default to `hint`, `nextFree`, and `mostExpired`. The following options are available for the list.

Field         | Description
--------------|------------
`hint`        | Try to reuse the address that the DHCP packet is requesting if it has one. If the request does not have a requested address, `hint` will fall through to the next strategyx, otherwise, it will refuse to try any reamining strategies whether or not it can satisfy the request. This should force the client to fall back to `DHCPDISCOVER` with no requsted IP address. `hint` will reuse expired leases and unexpired leases that match on the requested address, strategy, and token.
`nextFree`    | Within the subnet's pool of active IPs, choose the next free making sure to loop over all addresses before reuse. It will fall through to the next strategy if it cannot find a free IP. It only considers addresses that do not have a lease, whether or not the lease is expired.
`mostExpired` | If no free address is available, use the most expired address first.
`none`        | Do not hand out an address and refuse to try any remaining strategies.

All of the address allocation strategies do not consider any addresses that are reserved, as lease creation will be handled by the reservation instead.

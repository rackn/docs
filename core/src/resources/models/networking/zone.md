---
title: Zone
tags:
  - reference
  - architecture
  - zone
  - dns
---

# Zone {#rs_arch_models_networking_zone}

![zone object image](built_images#arch-zone.svg)

The Zone object defines the configuration for a single DNS zone.  A Zone defines the default TTL and Origin of the records.  A list of
records define the responses.  Most standard record types are supported.

Additionally, a Zone contains a source packet filter to restrict search matches.  This can be used to define different DNS zones by network
region.

The Zone object contains a Priority field that orders the zones to sequence look-ups.  The Zone also has a Continue field to indicate if it should stop
processing at this Zone upon origin match, but not record match.

The Zone objects contains a list of Forwarders to apply send the request.

When a DNS request is arrives, the zones are sorted by Priority.  For each Zone in the system,

* If Filters are present, check for a match.  If match fails, continue to next zone.
* If the Zone's Origin doesn't match, continue to the next zone.
* Check for matching zone data.  If a match is made, return the answer.
* If Forwarders are present, try to get an answer from the forwards.  If an answer is provided, then return that answer.
* Check the Continue flag is true, continue to the next zone.
* Stop processing and return a negivate match.

The Zone records follow the DNS RFC standards and can be imported through the CLI by processing standard zone files.

There are three additional features of Zones.

## Recursive DNS

A common pattern with DNS is having per-machine FQDNs that resolve to the address of the machine through public DNS.

If the Digital Rebar endpoint is located at `drp.example.com`, a NS record can be created on your public DNS provider with value `drp.example.com`.

This will allow a machine with name `machine.drp.example.com` and DRP Zone A record with name `eval:{{.Machine.Name}}` and value `{{.Machine.Address}}` to resolve to the machine's address from public access.

## Auto Reservation Records

When a Reservation record is created, deleted, or updated, the reservation is examined with its optional subnet to construct an A record for that reservation.

For example, a reservation that has directly or through its subnet an IP address, a domain name, and a hostname, will get a record to the containing Zone by matching
the Zone's Origin.

## Auto PTR Records

The PTR records and their in-addr zones are created automatically and updated as the A and AAAA records are updated.  PTR Zones can be created but the order of the zones
needs to be consider to get desired operations.

## Dynamic Records

Digital Rebar Provision is a templating engine and this applies to the Zones as well.  Custom records can be created that have evaluations in them.  This can be used to
dynamically add records based upon Machine, Resource Broker, or Cluster data.  The main use case is A and AAAA record construction.

This ZoneRecord will add an appropriate record for machines that match the Zone's origin.

ZoneRecord:

* Type: A
* Name: `eval:{{.Machine.Name}}`
* Value: `{{.Machine.Address}}`

ZoneRecord with params:

* Type: A
* Name: `eval:{{.Machine.Name}}.{{.Param "dns-domain"}}`
* Value: `{{.Machine.Address}}`

This record says that for any Machine, Cluster, or Resource Broker object that has a Name field that would match the Zone's Origin, add an A record with a value of the Machine's Address field.

The search of objects is caused by the `eval:` value in the `Name` field.  Any of the normal template Machine expansions from the rendering system is available.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli zones fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


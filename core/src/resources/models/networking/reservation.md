---
title: Reservation
tags:
  - reference
  - architecture
  - reservation
---

# Reservation {#rs_arch_models_networking_reservation}

The Reservation Object defines a mapping between a token and an IP address. The token is defined by the assigned strategy. Similar to the subnet object, the only current strategy is `MAC`. This will use the MAC address of the incoming requests as the identity token. The reservation allows for the optional specification of specific options and a next server that override or augment the options defined in a subnet. Because the reservation is an explicit binding of the token to an IP address, the address can be handed out without the definition of a subnet. This requires that the reservation have the netmask option (DHCP option 1) specified. In general, it is a good idea to define a subnet that will cover the reservation with default options and parameters, but it is not required.

Reservations are what the DRP DHCP service uses to ensure that an IP address is always issued to the same device.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli reservations fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

!!! note

    If you create a static reservation, DRP will always respond to the request even if the subnet is disabled.

!!! note

    If the reservation and its subnet, if present, define both option 12 and option 15, this will be used to
    find a matching DNS Zone for automatic A and PTR record creation.

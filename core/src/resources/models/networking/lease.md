---
title: Lease
tags:
  - reference
  - architecture
  - lease
---

# Lease {#rs_arch_models_networking_lease}

The Lease Object defines the ephemeral mapping of a token, as defined by the reservation's or subnet's strategy, and an IP address assigned by the reservation or pulled form the subnet's pool. The lease contains the Strategy used for the token and the expiration time. The contents of the lease are immutable with the exception of the expiration time.

Leases track what IP addresses the system has handed out to what strategy/token pairs.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli leases fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: Object Models
tags:
  - reference
  - architecture
---

# Objects Models {#rs_arch_object_models}

![objects image](built_images#arch-objects.svg)

The object are broken up into groups covering different functions of operation and deployment.

While the diagram and sorting put each object in a single group.  Often times objects will cross groupings to be consumed or drive operations.

!!! example

    Machine objects can create records in DNS Zone objects.

!!! example

    The Machine, Cluster, and Resource Broker objects reference Content objects.

## Data Architecture {#rs_data_architecture}

Digital Rebar Provision uses a fairly simple data model. There are 4 main models for the provisioner server, 4 models
for the DHCP server, and 1 model for the DNS server. Each model has a corresponding API in the [](#rs_api).

The models define elements of the system. The API provides basic CRUD (create, read, update, and delete) operations as
well as some additional actions for manipulating the state of the system. The [](#rs_api) contains that
definitions of the actual structures and methods on those objects. Additionally, the operation will
describe common actions to use and do with these models and how to build them. The [](#rs_cli) describes the
Command Line manipulators for the model.

## Multi-Site Objects

These objects provide the basis for [Managers](#rs_arch_manager) to manage endpoints.

## RBAC Objects

These objects control who and how entities have access to the systems.

## System Objects

These objects control aspects of the individual endpoint artifacts and configuration.

## Content Objects

These objects define what can be done.  They are the operations, provisioning, and flow that machines and clusters are driven through.  They also define the additional actions that can taken against the runner objects as well.

!!! example

    [Provisioning](#rs_arch_usage_provisioning) or [Clustering](#rs_arch_usage_clustering) will use the Content objects to define how something should be installed and configured.

!!! example

    [Batch](#rs_arch_usage_batching) will use the Content objects to define how something should be done.

## Runner Objects

These objects use Content objects to run the tasks.  [Runner](#rs_arch_runner) describe what and when is executed in the system.

## Execution Objects

These objects track the results of Runner objects executing Content objects.

## Networking Objects

These objects define how the system should provide DHCP and DNS services in the environment.

## Other Objects

These objects are added by plugin providers to enhance operations by storing new data.

---
title: Object Metadata
tags:
  - reference
  - architecture
  - metadata
---

### Common Object Metadata {#rs_object_metadata}

Metadata values are used throughout DRP for both UX and operational controls. You can find them in the object hierarchy as `<Object>.Meta`.

Models can contain other common fields that may be present for user edification and API tracking purposes, but that do not affect how dr-provision will use or interpret changes to the objects.

<div class="first-column-single-line" ></div>
Field           | Description
----------------|------------
`ReadOnly`      | A boolean value that indicates whether the object can be modified via the API. This field is set to `True` if the object was loaded from a read-only content layer.
`Description`   | A brief description for the object that is only one line long.
`Documentation` | A longer description of what the object is for and how it should be used that is generally a few lines to a few paragraphs long.

#### UX Related Metadata
<div class="first-column-single-line" ></div>
Field                 | Description
----------------------|------------
`icon: [icon name]`   | Sets the icon for the object.  The icons are available from [fontawesome](https://fontawesome.io/icons/).
`color: [color name]` | Sets the icon color for the object.

#### Validation Metadata

Models also contain common fields that track the validity and availability of individual objects.

<div class="first-column-single-line" ></div>
Field       | Description
------------|------------
`Validated` | A boolean value that indicates whether a given object is semantically valid or not. Semantically invalid objects will never be saved, and if one is returned via the API, it will be populated with a list of messages indicating what is invalid.
`Available` | a boolean value that indicates whether the object is available to be used, not whether it is semantically valid. An object that is invaild can never be available, while an object that is not available can be semantically valid.
`Errors`    | a list of strings that contain any error messages that occurred in the process of checking whether a given object is valid and available. Error messages are designed to be human readable.

!!! note

    Objects are checked for validity and availability on initial startup of DRP (when they are all loaded into memory), and thereafter every time they are updated. You must check each object returned from an API interaction to ensure that it is valid and available before using it.

### Param.Meta

There are several meta fields that can be used to adjust on screen display for params. The following items are commonly used on all object types to provide UX rendering guides.

<div class="first-column-single-line" ></div>
Field                       | Description
----------------------------|------------
`password: [anyvalue]`      | Renders as password that is not encrypted, but obfuscated.
`clipboard: [anyvalue]`     | Provides a copy button so user can copy the param contents to clipboard.
`readonly: [anyvalue]`      | Prevents editing from the UX.
`render: link`              | Adds an <https://%5Bvalue%5D> link field that opens in a new tab.
`render: multiline`         | Presents a textarea for string inputs.
`render: raw`               | Presents textarea to user instead of regular edit and does not reformat.
`downloadable: [file name]` | Provides a download link so user can download the param contents as a file.
`route: [object name]`      | When included, the UX will provide a link to the underlying object as per the param value.

### Machine.Meta Options

<div class="first-column-single-line" ></div>
Field                    | Description | Value
-------------------------|-------------|------
`BaseContext: [context]` | Lets DRP know what the base context is for the machine when going back to a known context.
`machine-role: [role]`   | Set by DRP and should not be edited. It lets DRP know what the type of machine this object is. | `self`, `machine`, `cluster`, or `resource-broker`
`pipeline`               | the name of the executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.
`pipeline-uuid`          | A unique ID for each executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.

### Cluster.Meta controls

<div class="first-column-single-line" ></div>
Field                    | Description | Value
-------------------------|-------------|------
`no-cluster: "true"`     | Tells the cluster and resource broker create that this parameter or profile should not be added to the special profile during object create.
`BaseContext: [context]` | Lets DRP know what the base context is for the machine when going back to a known context
`machine-role: [role]`   | Is set by DRP and should not be edited. It lets DRP know what the type of machine this object is. | `self`, `machine`, `cluster`, or `resource-broker`
`pipeline:`              | The name of the executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.
`pipeline-uuid`"         | Is a unique ID for each executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.

### ResourceBroker.Meta controls

<div class="first-column-single-line" ></div>
Field                    | Description | Value
-------------------------|-------------|------
`no-cluster: "true"`     | Tells the cluster and resource broker create that this parameter or profile should not be added to the special profile during object create.
`BaseContext: [context]` | Lets DRP know what the base context is for the machine when going back to a known context
`machine-role: [role]`   | Set by DRP and should not be edited. It lets DRP know what the type of machine this object is. | `self`, `machine`, `cluster`, or `resource-broker`
`pipeline:`              | The name of the executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.
`pipeline-uuid`          | A unique ID for each executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.

### Pipeline options

The following Meta values are used to help the UX offer an simplified experience for operators.

General guidelines for required components of Pipeline metadata

The chain-map tasks and values are exceptions.

<div class="first-column-single-line" ></div>
Field           | Description | Value
----------------|-------------|------
`pipeline:`     | The name of the executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.
`pipeline-uuid` | A unique ID for each executed pipeline and set by DRP during pipeline execution to allow operators to trace jobs across a pipeline run.

### Workflows.Meta

<div class="first-column-single-line" ></div>
Field       | Description                                  | Value
------------|----------------------------------------------|------
`universal` | Describes the entry point for the workflow.
            | The entry point for pipelines.               | `start`   
            | Performs provision operations for pipelines. | `provision` 

### Profiles.Meta

<div class="first-column-single-line" ></div>
Field         | Description | Value
--------------|-------------|------
`pipeline`    | Used to filter user created pipeline Profiles.<br />Do not include with universal-application profiles. Use value from application.
              | Create/deletes/manages other machines. | `cluster` 
              | Manages the machine assigned.          | `machine` 
              | The type of pipeline.                  | `application`
              | Create/deletes/manages other machines. | `cluster` 
              | Manages the machine assigned.          | `machine` 
`platform`    | The platform or vendor target for the pipeline. | `linode`, `aws`, `google`, `azure`
              |                                                 | `centos`, `rocky`, `ubuntu`, `esxi`, `windows`
`major`       | For versioned systems, this identifies major version.
`minor`       | For versioned systems, this identifies minor version.
`required`    | UX form renderer for parameters that must have values. | Example field value
              |                                                        | `linode/token,linode/root-password`
`optional`    | UX form renderer for parameters that may have values.   | Example field value 
              |                                                        | `linode/region,linode/instance-type,linode/instance-image,linode/root-password`

### Stages.Meta

<div class="first-column-single-line" ></div>
Field                    | Description | Value
-------------------------|-------------|------
`workflow` | UX helper points back to the workflow using this stage.
`phase` | UX helper to identify the phase of the work. | `base`, `pre`, `mid`, `post`
`action` | UX helper to identify to type of work.      | `classification`, `validation`, `flexiflow`

### Tasks.Meta

<div class="first-column-single-line" ></div>
Field          | Description
---------------|------------
`flow-comment` | Used by UX to provide side-effect information in the triggers flow visualization.
`triggered-by` | Set by DRP when a trigger uses a blueprint to create a work order.
               | It can be retrieved in a task using `{{get .WorkOrder.Meta "triggered-by"}}`.

### Blueprints.Meta

<div class="first-column-single-line" ></div>
Field          | Description
---------------|------------
`flow-comment` | Used by UX to provide side-effect information in the triggers flow visualization.

### WorkOrder.Meta

<div class="first-column-single-line" ></div>
Field          | Description
---------------|------------
`triggered-by` | Set by DRP when a trigger uses a blueprint to create a work order.
               | Can be retrieved in a task using `{{get .WorkOrder.Meta "triggered-by"}}`.

### Legacy & Non-Operational Meta

The following Meta fields are often included in objects but has no UX or operational impact on the system:

<div class="first-column-single-line" ></div>
Field           | Description
----------------|------------
`feature-flags` | Legacy setting needed by earlier DRP for backwards compatability.
                | Always set to `sane-exit-codes`
`title`         | Provides optional authorship information. Often set automatically to "User added" by the UX when creating new objects.
`copyright`     | Provides optional authorship information
`type`          | Unimplemented attempt to standardize icon and color information (It should be remove when found).


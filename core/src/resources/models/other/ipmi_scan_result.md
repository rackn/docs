---
title: IPMI Scan Result
tags:
  - reference
  - architecture
  - rack
---

# IPMI Scan Result {#rs_model_ipmi_scan_result}

The [](#rs_content_ipmi) defines a IPMI Scan Result Object that can be used to track the result of an IPMI scan.

Fields are:

*	ScanStartAddress string - Scan start address
*	ScanEndAddress string - Scan end address
*	LowestScannedIndex int -The latest scanned address, this will be used in case the scan stops in the middle for some reason and needs to be restarted
* LowestScannedAddress string - The lowest scanned address which is present at the lowest scanned index
* ScanSize int - The total number of IPs being scanned
*	CreatedUuids []string - A list of UUIDs created as a part of this scan
*	ScanErrors []string - Any errors encountered during the range-scan
*	State State - The state of the current scan, can be one of `running`, `finished` or `canceled`
*	ScanActionJobId string - When an action is created, a job ID is automatically associated with it. This is that job ID
*	ScanStartTime time.Time - The time the range-scan started running.
* ScanEndTime time.Time - The time the range-scan failed or finished.
*	ScanUpdatedTime time.Time - The time the range-scan db object got updated.
* ScanWaitForComplete bool - If set to true, this scan is a blocking call and the RunAction command will wait for the scan to complete
* ScanWaitTimeoutMins time.Duration - If ScanWaitForComplete is set to true, use this to put a limit on how long to wait for the scan to complete

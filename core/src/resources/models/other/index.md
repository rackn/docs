---
title: Other Objects
tags:
  - reference
  - architecture
  - rack
---

# Other objects {#rs_res_objs_other}

Plugins can add additional objects to the system.  Some of these are:

* [](#rs_model_rack) - Defines a Rack that can be used to control machines in a rack
* [](#rs_model_ipmi_scan_result) - Defines the results of an IPMI scan

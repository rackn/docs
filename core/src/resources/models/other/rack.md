---
title: Rack
tags:
  - reference
  - architecture
  - rack
---

# Rack {#rs_model_rack}

The [](#rs_content_rack) defines a Rack Object that can be used to define a rack and manage machines in the rack.

Fields are:

* Machines - an object of Name / Machine 
* Switches - list of TOR switch names associated with this rack
* MachineParams - an object of Key / Value parameters - these are added to the machine in this rack
* ValidationData - an object of Key to Parameter object - this map of serial numbers to key/value pairs for a machine
* ValidationToMachine - This defines the key to parameter to convert validation data to machine parameters
* ValidationToParams - This defines a parameter name that maps to a validating string for that parameter on the machine.


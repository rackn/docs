---
title: Version Set Object
tags:
  - reference
  - architecture
  - version-set
---

# Version Set Object {#rs_model_version_set}

The versionset object defines a set of configuration state that could be applied to an endpoint.

It includes the common fields `Description`, `Documentation`, `Errors`, `Available`, `Valid`, `Meta`, `Endpoint`, and `Bundle`.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli version_sets fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


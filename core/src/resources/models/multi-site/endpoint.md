---
title: Endpoint Object
tags:
  - reference
  - architecture
  - endpoint
---

# Endpoint Object {#rs_model_endpoint}

The endpoint object defines the API connection path (URL, username, and password) to the managed endpoint. As the manager operates, the endpoint object is updated with the state and configuration of the endpoint. The endpoint object also controls if the manager should update configuration by versionsets.

The endpoint object has the standard object fields.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli endpoints fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

The `Params` field includes several params used for endpoint connection details.

Field  | Description                                           | Param Name
-------|-------------------------------------------------------|-----------
       | The param that defines the URL for the endpoint.      | `manager/url`
       | The param that defines the username for the endpoint. | `manager/username`
       | The param that defines the password for the endpoint. | `manager/password`


The `Components` field inclues a list of Elements installed (these are content packages and plugin providers).

Item            | Description                         | Example Values
----------------|-------------------------------------|---------------
`Name`          | Name of the element.                | `burnin`, `bios`
`Type`          | Type of the element.                | `CP`, `PP`
`Version`       | Version of the element (short form) | `tip`, `stable`, `v4.3.2`
`ActualVersion` | Actual Version of the element (long form)


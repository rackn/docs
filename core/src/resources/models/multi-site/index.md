---
title: Multi-Site Objects
tags:
  - reference
  - architecture
  - endpoint
  - version-set
---

# Multi-Site Objects {#rs_res_objs_multisite}

The Multi-Site Objects are used by managers to control and manage downstream DRP endpoints.

* [](#rs_model_endpoint) - Defines an endpoint to manage
* [](#rs_model_version_set) - Defines stackable configuration of the endpoint

The DRP Endpoint running as manager uses the Version Sets to define the configuration
and content / plugin / UX / server versions of the managed endpoint.  The Endpoint objects
define the connection properties for connecting to the Endpoint.

---
title: Execution Objects
tags:
  - reference
  - architecture
  - work-order
  - job
---

# Execution Objects {#rs_res_objs_execution}

The Execution Objects track the results running tasks or the starting of tasks.

The primary task result tracking object is [](#rs_arch_models_job). The [](#rs_arch_models_job)
is an instance of {#rs_arch_models_task} running on a [](#rs_res_objs_runner).

## Triggering Tasks

When the {#rs_res_objs_runner} are not in Work Order Mode, they run tasks and create [](#rs_arch_models_job)
based upon the Pipeline, Workflows, and Stages assigned.  In Work Order Mode, the [](#rs_work_order)
doubles as both the runner and the task information.

The [](#rs_work_order) can be created manually to trigger work.  Tasks can also create work orders
to do work.

Additionally, events or webhooks can be used to create work orders. There are two additional objects
in the system that can generate work orders.

* [](#rs_model_trigger_provider) - Provides methods that can implement triggers
* [](#rs_model_trigger) - Defines the information that creates a work order based upon the [](#rs_model_trigger_provider) function

A trigger can be created using a trigger provider that will generate a work order at a time interval to run a blueprint.

!!! example

    A Cron Trigger could be created using the Cron trigger provider to run a blueprint that checks ticket system for maintenance
    tickets every hour.

!!! example

    A GitHub Trigger could be created using the GitHub Push trigger provider to run a blueprint that rebuilds content in the git repo
    specified and publishes the versioned content to a manager.

    


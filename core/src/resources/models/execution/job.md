---
title: Job
tags:
  - reference
  - architecture
  - job
---

# Job {#rs_arch_models_job}

Jobs are what DRP uses to track the state of running individual tasks on a machine. There can be at most one current job for a machine at any given time.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli jobs fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


| `State`      | Definition |
| -------------|----------- |
| `created`    | This is the state that all freshly-created jobs start at.
| `running` | Jobs are automatically transitioned to this state by the machine agent when it starts executing this job's actions.
| `failed` | Jobs are transitioned to this state when they fail for any reason.
| `finished` | Jobs are transitioned to this state when all their actions have completed successfully.
| `incomplete` | Jobs are transitioned to this state when an action signals that the job must stop and be restarted later as part of its action.

| `ExitState`      | Definition |
| `reboot` | Indicates that the job stopped executing due to the machine needing to be rebooted.
| `poweroff` | Indicates that the job stopped executing because the machine needs to be powered off.
| `stop` | Indicates that the job stopped because an action indicated that it should stop executing.
| `complete` | Indicates that the job finished.

### Job Actions

Once a Job has been created and transitioned to the running state, the machine agent will request that the Templates in the Task for the job be rendered for the Machine and placed into JobActions. JobActions have the following fields:

<div class="first-column-single-line"></div>
Field    | Definition
----------|-----------
`Name`    | The name of the JobAction. It is present for informational and troubleshooting purposes, and the name does not effect how the JobAction is handled.
`Content` | The result of rendering a specific Template from a Task against a Machine.
`Path`    | If present, the Content will be written to the location indicated by this field, replacing any previous file at that location. If Path is not present or empty, then the Contents will be treated as a shell script and be executed.

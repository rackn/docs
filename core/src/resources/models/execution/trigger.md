---
title: Trigger Object
tags:
  - reference
  - architecture
  - trigger
---

# Trigger Object {#rs_model_trigger}

The Trigger Object defines the pieces needed to create a work order to execute by a runner.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli triggers fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


---
title: Trigger Provider
tags:
  - reference
  - architecture
  - trigger-provider
---

# Trigger Provider {#rs_model_trigger_provider}

The Trigger Provider is usually a read-only object provided by the server or the trigger plugin.

These define methods to receive a trigger.  Some are event handlers, webhook processors, or time-based processors.

See [](#rs_content_triggers) for the webhook and other service integration trigger providers.

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli trigger_providers fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: Content
tags:
  - reference
  - architecture
  - content
---

# Content {#rs_model_content}

Content packages (also known as content bundles, or content packs) are the primary way of adding new functionality to DRP. Content packages consist of a collection of objects (tasks, bootenvs, templates, stages, workflows, etc.), along with important metadata about the content package.

!!! example "Example content package"

      ``` yaml
      ---
      meta:
      Author: ""
      CodeSource: ""
      Color: ""
      Copyright: ""
      Description: Default objects that must be present
      DisplayName: ""
      DocUrl: ""
      Documentation: ""
      Icon: ""
      License: ""
      Name: BasicStore
      Order: ""
      Overwritable: true
      Prerequisites: ""
      RequiredFeatures: ""
      Source: ""
      Tags: ""
      Type: basic
      Version: 3.12.0
      Writable: false
      sections:
      bootenvs:
        ignore:
          Available: false
          BootParams: ""
          Bundle: BasicStore
          Description: The boot environment you should use to have unknown machines boot
            off their local hard drive
          Documentation: ""
          Endpoint: ""
          Errors: []
          Initrds: []
          Kernel: ""
          Meta:
            color: green
            feature-flags: change-stage-v2
            icon: circle thin
            title: Digital Rebar Provision
          Name: ignore
          OS:
            Codename: ""
            Family: ""
            IsoFile: ""
            IsoSha256: ""
            IsoUrl: ""
            Name: ignore
            SupportedArchitectures: {}
            Version: ""
          OnlyUnknown: true
          OptionalParams: []
          ReadOnly: false
          RequiredParams: []
          Templates:
          - Contents: |
              DEFAULT local
              PROMPT 0
              TIMEOUT 10
              LABEL local
              {{.Param "pxelinux-local-boot"}}
            ID: ""
            Meta: null
            Name: pxelinux
            Path: pxelinux.cfg/default
          - Contents: |
              #!ipxe
              chain {{.ProvisionerURL}}/${netX/mac}.ipxe && exit || goto chainip
              :chainip
              chain tftp://{{.ProvisionerAddress}}/${netX/ip}.ipxe || exit
            ID: ""
            Meta: null
            Name: ipxe
            Path: default.ipxe
          Validated: false
        local:
          Available: false
          BootParams: ""
          Bundle: BasicStore
          Description: The boot environment you should use to have known machines boot
            off their local hard drive
          Documentation: ""
          Endpoint: ""
          Errors: []
          Initrds: []
          Kernel: ""
          Meta:
            color: green
            feature-flags: change-stage-v2
            icon: radio
            title: Digital Rebar Provision
          Name: local
          OS:
            Codename: ""
            Family: ""
            IsoFile: ""
            IsoSha256: ""
            IsoUrl: ""
            Name: local
            SupportedArchitectures: {}
            Version: ""
          OnlyUnknown: false
          OptionalParams: []
          ReadOnly: false
          RequiredParams: []
          Templates:
          - Contents: |
              DEFAULT local
              PROMPT 0
              TIMEOUT 10
              LABEL local
              {{.Param "pxelinux-local-boot"}}
            ID: ""
            Meta: null
            Name: pxelinux
            Path: pxelinux.cfg/{{.Machine.HexAddress}}
          - Contents: |
              #!ipxe
              exit
            ID: ""
            Meta: null
            Name: ipxe
            Path: '{{.Machine.Address}}.ipxe'
          - Contents: |
              DEFAULT local
              PROMPT 0
              TIMEOUT 10
              LABEL local
              {{.Param "pxelinux-local-boot"}}
            ID: ""
            Meta: null
            Name: pxelinux-mac
            Path: pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}
          - Contents: |
              #!ipxe
              exit
            ID: ""
            Meta: null
            Name: ipxe-mac
            Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
          Validated: false
      params:
        pxelinux-local-boot:
          Available: false
          Bundle: BasicStore
          Description: The method pxelinux should use to try to boot to the local disk
          Documentation: |2-

            On most systems, using 'localboot 0' is the proper thing to do to have
            pxelinux try to boot off the first hard drive.  However, some systems
            do not behave properlydoing that, either due to firmware bugs or
            malconfigured hard drives.  This param allows you to override 'localboot 0'
            with another pxelinux command.  A useful reference for alternate boot methods
            is at https://www.syslinux.org/wiki/index.php?title=Comboot/chain.c32
          Endpoint: ""
          Errors: []
          Meta: {}
          Name: pxelinux-local-boot
          ReadOnly: false
          Schema:
            default: localboot 0
            type: string
          Secure: false
          Validated: false
      roles:
        superuser:
          Available: false
          Bundle: BasicStore
          Claims:
          - action: '*'
            scope: '*'
            specific: '*'
          Description: ""
          Documentation: ""
          Endpoint: ""
          Errors: []
          Meta: {}
          Name: superuser
          ReadOnly: false
          Validated: false
      stages:
        local:
          Available: false
          BootEnv: local
          Bundle: BasicStore
          Description: Stage to boot into the local BootEnv.
          Documentation: ""
          Endpoint: ""
          Errors: []
          Meta:
            color: green
            icon: radio
            title: Digital Rebar Provision
          Name: local
          OptionalParams: []
          Profiles: []
          ReadOnly: false
          Reboot: false
          RequiredParams: []
          RunnerWait: false
          Tasks: []
          Templates: []
          Validated: false
        none:
          Available: false
          BootEnv: ""
          Bundle: BasicStore
          Description: Noop / Nothing stage
          Documentation: ""
          Endpoint: ""
          Errors: []
          Meta:
            color: green
            icon: circle thin
            title: Digital Rebar Provision
          Name: none
          OptionalParams: []
          Profiles: []
          ReadOnly: false
          Reboot: false
          RequiredParams: []
          RunnerWait: false
          Tasks: []
          Templates: []
          Validated: false
      ```

As the above example implies, YAML is the preferred format for shipping content packages as it is generally easier to read and edit than JSON, especially when longer multi-line templates are present. You can also use the `drpcli contents` commands to `unbundle` a content pack for easier editing, and then `bundle` it back up for processing and uploading to a DRP endpoint.

## Metadata

All content packages must have a meta section, which contains a variety of different string values.

### Operational Fields

The following metadata fields have meaning to DRP directly and control how dr-provision will process the content package when loaded.

#### Name

The name of the content package. All content packages must have a name.  Names are not allowed to collide in a running instance of DRP. The name of the content package should either be a single word or a short, hypenated series of words.

#### Version

The version of the content package. Versions are roughly [Semantic Versionioning 2.0.0](https://semver.org) compliant, except that we allow a leading lower-case "v" and disregard everything after and including the first occurance of hyphen (-).

!!! note

    Version is optional. If it is not included, the version of the content package will default to `0.0.0`.

#### RequiredFeatures

A space separated list of features that DRP must provide for the content package to function properly. If you try to load a content package onto a version of DRP that does not include a required feature, the load will fail with an error indicating what features are missing.

This field should be left blank if the content package does not rely on any particular features of DRP.

#### Prerequisites

A comma separated list of other content packages that must be present on the DRP endpoint for this content package to load. Each entry in the prerequisites list must either be the name of a content package, or the name of a content package followed by a colon (:) and a space separated list of version constrints.

If the field is left blank, then this content pack is not considered to rely on any other content packs.

!!! example "Example prerequisites"

    ``` yaml
    ---
    meta:
      Name: one
      Version: v1.2.3
    ```

    ``` yaml
    ---
    meta:
      Name: two
      Version: v1.2.3
      Prerequisites: one
    ```

    ``` yaml
    ---
    meta:
      Name: three
      Version: v1.2.4
      Prerequisites: 'one: >=1.0, two: <2.0.0'
    ```

##### Version Constraints

Prerequisite version constraints are processed according to the following rules.

* The expression `<1.0.0` is less than the value `1.0.0`.
* The expression `<=1.0.0` is less than or equal to the value `1.0.0`.
* The expression `>1.0.0` is greater than the value `1.0.0`.
* The expression `>=1.0.0` is greater than or equal to the value `1.0.0`.
* The expressions `1.0.0`, `=1.0.0`, and `==1.0.0` are equalalent and equal to the value `1.0.0`.
* The expressions `!1.0.0`, and `!=1.0.0` are not equal to `1.0.0`. They exlude the value `1.0.0`.
* The expression `1.0.0 <2.0.0` is greater than `1.0.0`, and less than `2.0.0`, therefore, `1.1.1` and `1.8.7` are valid, but not `1.0.0` or `2.0.0`.
* The expression `<2.0.0 || >=3.0.0`is less than `2.0.0`, or greater than/equal to `3.0.0`, therefore, `1.x.x`, and `3.x.x` are valid, but not `2.x.x`.

!!! note 

    You can combine the AND operator (a space between expressions) and the OR operator (||). The AND operator has higher precedence. 
    !!! important
        It is not possible to override precedence with parentheses.

### Informational Fields

These metadata fields contain information that may be of interest to users of the content package, but they are not required for dr-provision to properly load, or use the content package.

#### Description

A short, one line description of what the content bundle provides.

#### Source

Where the content package is from. This is generally either the author or the organization that produced and maintains the content package. 

!!! warning

    Deprecated in favor of Author and CodeSource.

#### Documentation

Longer information about what the content bundle is and what it does. The documentation field may be either plain text or Restructured Text.

#### DisplayName

The name of the content package as it will de displayed in the Web UI.

#### Icon

The icon that will be used for the content package in the Web UI.

#### Color

The color that the icon will be displayed in the Web UI

#### Author

The original author of the content package.

#### License

The name of the license that everything in the content package is distributed as.

#### Copyright

The copyright holder of the content package.

#### CodeSource

The location that the content pack was loaded from.

#### Order

The order in which the content package will be displayed in the Web UI.

#### Tags

A comma-separated list of tags appropriate for this content package. Mainly used by the Web UI for sorting and filtering purposes.

#### DocUrl

A URL to external documentation about this content package.

Data
----

Content package do not have to define any objects, though they would be little more than placeholders. Objects are defined in a content pack in the Sections part as follows:

``` yaml
---
meta:
  Name: foo
  ...
sections:
  tasks:
    task-name:
      Name: foo
      ...
    ...
  bootenvs:
    bootenv-name:
      Name: bar
      ...
    ...
  ...
```

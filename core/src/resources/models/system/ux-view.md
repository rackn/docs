---
title: UX View
tags:
  - reference
  - architecture
  - ux-view
---

# UX View {#rs_model_ux_view}

The UX View object defines a view that controls

* Navigation
* Table Columns
* Grouping

Other elements can be defined for the view of the ux.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli ux_views fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: Plugin
tags:
  - reference
  - architecture
  - plugin
---

# Plugin {#rs_model_plugin}

The Plugin Object defines a configuration on the plugin provider.  Some plugin providers are
singltons, e.g. [](#rs_content_bios), and others allow for instances, e.g. [](#rs_content_callback).

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli plugins fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: ISO
tags:
  - reference
  - architecture
  - iso
---

# ISO {#rs_model_iso}

The ISO directory in the file server space is managed specially by the ISO API. The API handles upload and destroy functionality. The API also handles notification of the bootenv system to "explode" ISO files that are needed by the bootenv and marking the bootenv as available.

ISOs can be directly placed into the `isos` directory in the file root.


---
title: Interface
tags:
  - reference
  - architecture
  - interface
---

# Interface {#rs_model_interface}

The Interface Object is a read-only object that is used to identify local interfaces and their addresses on the Digital Rebar Provision server. This is useful for determining what subnets to create and with what address ranges. The Subnets part of the [](#rs_portal) uses this to populate possible subnets to create.

Interface objects are provided by DRP as an easy means for the UX and the CLI to enumerate the network interfaces on the DRP server and provide some basic information for building local subnets.

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli interfaces fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


---
title: Alert
tags:
  - reference
  - architecture
  - alert
---

# Alert {#rs_model_alert}

The Alert Object presents an event that the Users should address.

The system can generate alerts internally, through triggers, or manually by API.

An Alert can be create with a flag that indicates if the alert already exists and
has not been acknowledge, the existing alert will be used and the count field is updated.

## Field

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli alerts fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>


---
title: System Objects
tags:
  - reference
  - architecture
  - alert
---

# System Objects {#rs_res_objs_system}

The system has objects that control the general operations of the system.  These are not
directly referenced under other types of objects.

* [](#rs_model_activity) - Tracks the activity of machines in the system. Used for Pay-Go Billing
* [](#rs_model_alert) - A notification requiring attention by a User
* [](#rs_model_content) - The definition of a content pack
* [](#rs_model_file) - The Files on the system
* [](#rs_model_interface) - The local interfaces on the DRP Endpoint
* [](#rs_model_iso) - The ISOs on the system
* [](#rs_model_plugin_provider) - The plugin providers loaded into the system
* [](#rs_model_plugin) - The configuration of plugin provider in the system
* [](#rs_model_prefs) - The preferences of the system
* [](#rs_model_ux_view) - UX View defines UX customizations stored on the server for reuse in the UX


---
title: Activity
tags:
  - reference
  - architecture
  - activity
---

# Activity {#rs_model_activity}

The Activity Object presents a daily activity tracking of machines.  Each
machine builds up a daily history of actions that can be used for Pay-Go billing.

When not doing Pay-Go billing, the activity entries still track usage over time.

## Field

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli activities fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

---
title: File
tags:
  - reference
  - architecture
  - file
---

# Files {#rs_model_file}

The DRP file server has a managed filesystem space. The API defines methods to upload, destroy, and get these files outside of the normal TFTP and HTTP path. The TFTP and HTTP access paths are read-only. The only way to modify this space is through the API or direct filesystem access underneath DRP. The filesystem space defaults to `/var/lib/tftpboot`, but can be overridden by the command line flag `--file-root` (example: `--file-root=$(PWD)/drp-data` can be used with `--isolated` on install). These directories can be directly manipulated by administrators for faster loading times.

This space is also used by the bootenv import process when "exploding" an ISO for use by machines.

!!! note

    Templates are not rendered to the file system. They are in-memory generated on the fly content.


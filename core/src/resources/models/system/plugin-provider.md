---
title: Plugin Provider
tags:
  - reference
  - architecture
  - plugin-provider
---

# Plugin Provider {#rs_model_plugin_provider}

The Plugin Provider Object defines the components inside a plugin provider.  The binary
contains this read-only object and is added to the system when the plugin provider loaded.

Removing the plugin provider does not remove the content, but does invalidate the [](#rs_model_plugin).

## Fields

| Field        | Definition |
| -------------|----------- |
<<<<< drpcli plugin_providers fieldinfo | jq '. | to_entries| .[] |  .key+"|"+(.value | gsub("\n"; "<br/>"))' -r >>>>>

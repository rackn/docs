---
title: Resources
tags:
- reference
---

# Resources {#rs_res}

This section contains security advisories, knowledge base articles, and labs.

!!! note

    The documentation is split into 5 sections.

    * [Geting Started](#rs_quickstart)
    * [Architectural Guide](#rs_arch)
    * [Developer Guide](#rs_dev)
    * [Operator Guide](#rs_ops)
    * [Resources](#rs_res)


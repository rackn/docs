---
title: Can I eliminate reboots with kexec?
tags:
  - reference
  - operator
  - kb
---

# Can I Elimnate Reboots with Kexec {#rs_kb_00018}

Installing operating systems often times requires a reboot of the
Machine several times. For physical (aka *bare*) metal, this reboot
cycle can add many minutes (often times 10 or more minutes) to the
installation process.

When installing Linux operating systems, it may be possible to utilize
the Linux Kernel *kexec* mechanism to "exec" in to the new Kernel
environment without requiring the Machine to pass through a full POST
cycle.

## Solution {#rs_kexec}

Setting the [kexec-ok]{.title-ref} param to [true]{.title-ref} on the
[global]{.title-ref} or machine specific profile allows BootEnvs that
are *kexec* enabled to skip rebooting when changing to that BootEnv. For
example, Sledgehammer enables *kexec* and can be started without a
reboot from Linux environments.

This is a Linux specific feature.

!!! note

    The *kexec* capability is defined by the compiled Linux Kernel
    configuration. Some Linux distributions do not enable *kexec* by
    default. In these cases, you may have to compile a custom kernel version
    with *kexec* enabled. In addition, not all transition states can
    successfully utilize *kexec* even if it is enabled in the kernel.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

- [kexec](https://wiki.archlinux.org/index.php/Kexec)

### Versions

Enabled in the v3.x versions and newer.

### Keywords

sledgehammer, kexec, post, reboot

### Revision Information

```
KB Article     :  kb-00018
initial release:  Wed Jun 10 11:40:54 PDT 2020
updated release:  Wed Jun 10 11:40:54 PDT 2020
```

---
title: Building 'drpcli' client binary and agent.
tags:
  - reference
  - operator
  - kb
---

# Building 'drpcli' client binary and agent. {#rs_kb_00074}

Generally speaking; RackN compiles, tests, and publishes a limited set
of Architectures and OSes for the `drpcli` CLI client binary and
Agent/Runner. You should not need to compile the `drpcli` binary.
However, it may be desirable to cross compile it for different
Architectures and OSes to run the binary from different platforms.

Please note that the ability to cross compile the binary does not mean
that RackN will support alternative Architectures and OSes.

## Solution {#rs_kb_building_drpcli}

The Digital Rebar Platform (DRP) endpoint service and most associated
tools are built using Golang. Golang supports a broad range of
Architectures and Operating Systems for cross compiling binaries. This
solution describes the mechanics of building the `drpcli` binary on
different "dists" (as Golang tends to refer to them).

First start by installing the currently supported version of Golang for
building RackN tooling. You can find this by checking the `go.mod` file:

-   <https://gitlab.com/rackn/provision/-/blob/v4/go.mod>

The line `go 1.18` (for example) defines the minimum supported Golang
version. See the Additional Information for install instructions.

Building for different Golang binary Archictures and OSes is generally
done by setting the environment variables `GOARCH` and `GOOS`
(respectively), otherwise the compile will occur for the current
Architecture and OS platform.

### The Process

``` bash
# clone the current stable branch
git clone https://gitlab.com/rackn/provision.git

# change in to the newly cloned git repo
cd provision

# NOTE: it is advisable to check the list of Branches ('git branch -a')
#       and checkout the most current Stable branch to build from
#       ('git checkout v4.9.0-branch')

# build an ARM64 version of drpcli for MacOS X
GOOS=darwin GOARCH=arm64 tools/build-one.sh cmds/drpcli

# newly built binary:
ls -l bin/darwin/arm64/drpcli
```

You can find a matrix of Golang supported GOOS and GOARCH values at:

-   <https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63>

You can also get the currently installed `go` version of supported
distributions by doing:

``` bash
go tool dist list
```

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

-   Cross Compiling in Golang:
    <https://medium.com/@georgeok/cross-compile-in-go-golang-9f0d1261ee26>
-   Installing Golang: <https://go.dev/doc/install>

### Versions

All

### Keywords

drpcli, agent, runner, cli, build, compile, cross compile

### Revision Information

```
KB Article     :  kb-00074
initial release:  Thu Mar 24 09:45:27 PDT 2022
updated release:  Thu Mar 24 09:45:27 PDT 2022
```

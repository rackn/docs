---
title: Image Deploy with Curtin Partitions and XFS
tags:
  - reference
  - operator
  - kb
---

# Image Deploy with Curtin Partitions and XFS {#rs_kb_00058}

When doing image-deploy, there are a couple of errors that can be
generated when using XFS. These include:

-   grub-install fails because of bad filesystem type
-   Missing / bad rootfs or other filesystem on reboot into running
    system.

This can happen when using Sledgehammer to image deploy Centos 7 or
Ubuntu 18.04 or younger images.

The problem is that Centos 8 has a newer version of XFS that is
incompatible with the XFS running in previous operating systems, e.g.
Centos 7 or Ubuntu 18.04.

This happens when a curtin/partitions parameter is used that sets an XFS
filesystem. This is NOT the default and so is only triggered from this
action.

## Solution {#rs_Image_Deploy_with_Curtin_Partitions_and_XFS}

Within [curtin/partitions]{.title-ref} parameter, an additional variable
will need to be added to all XFS filesystem creation stanzas. This looks
like this (in JSON format):

``` json
"extra_options": [
  "-i",
  "sparse=0"
],
```

This tells the Centos8-based sledgehammer to generate an XFS filesystem
that will work in the older environments.

A complete XFS filesystem stanza would look like this:

``` json
{
 "extra_options": [
   "-i",
   "sparse=0"
 ],
 "fstype": "xfs",
 "id": "disk0-part1",
 "label": "root-partition",
 "type": "format",
 "volume": "root_part"
}
```

You will also need to make sure that your image contains the XFS tools.
If you are using image builder, you can use the
[image-builder/additional-packages]{.title-ref} parameter with a value
that includes [xfsprogs]{.title-ref}.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

- [](#rs_content_image-deploy)
- [](#rs_content_image-builder)

### Versions

v4.6.0+

### Keywords

XFS, curtin, image-deploy

### Revision Information

```
KB Article     :  kb-00058
initial release:  Tue Feb 23 14:19:26 CST 2021
updated release:  Tue Feb 23 14:19:26 CST 2021
```

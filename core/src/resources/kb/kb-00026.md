---
title: Change a Machines Name
tags:
  - reference
  - operator
  - kb
---

# Change a Machines Name {#rs_kb_00026}

This article describes how an operator can attempt to change a
Machine's defined name, inside the deployed Operating System.

!!! note

    Machine name changes must be supported by the *Workflow* being executed,
    to affect the change inside the Operating System. Most RackN and Digital
    Rebar provided content supports this methodology. However, if you are
    customizing your kickstart or preseed templates, this process may not
    work if you have not arranged for these Param values to be utilized.

## Solution {#rs_change_machine_name}

If you wish to update/change a Machine Name, you can do:

``` bash
export UUID="abcd-efgh-ijkl-mnop-qrst"
drpcli machines update $UUID '{ "Name": "foobar" }'
```

!!! note

    You can NOT use the `drpcli machines set ...` construct as it
    only sets Param values. The Machines name is a Field, not a Parameter.
    This will NOT work: `drpcli machines set $UUID param Name to foobar`.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

### Versions

all

### Keywords

content, machine name, change name

### Revision Information

```
KB Article     :  kb-00026
initial release:  Wed Jun 10 12:07:40 PDT 2020
updated release:  Wed Jun 10 12:07:40 PDT 2020
```

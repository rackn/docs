---
title: Render a Kickstart or Preseed
tags:
  - reference
  - operator
  - kb
---

# Render a Kickstart or Preseed {#rs_kb_00031}

This article describes how to render templates used for Operating System
installation. This methodology can be used to help debug errors,
problems, or just generally observe what the Golang template rendered
`kickstart` or `preseed` artifact looks like.

## Solution {#rs_render_kickstart_preseed}

Kickstart and Preseed files only created by request and are not stored
on a filesystem that is viewable. They are dynamically generated on the
fly, and served from the virtual Filesystem space of the Digital Rebar
HTTP server (on port 8091 by default). However, it is possible to render
a kickstart or preseed to evaluate how it is going to operate, or
troubleshoot issues with your config files.

When a machine is in provisioning status, you can view the dynamically
generated preseed or kickstart from the TFTP server (or via the HTTP
gateway). Provisioning status means the Machine has been placed into an
installable BootEnv via a Stage. If (for exaxmple) placed in to
`centos-8-install` Stage, the `compute.ks` can be rendered for the
machine. Or, if placed in to `ubuntu-16.04-install` Stage, the `seed`
can be rendered for the machine.

Get the Machine ID, then use the following constructed URL:

```
MID="7f65279a-7e5c-4e69-af40-dd01af4c5667"
DRP="10.10.10.10"
TYPE="seed"   # seed for ubuntu, or compute.ks for centos

http://${DRP}:8091/machines/${MID}/${TYPE}
```

!!! example "ubuntu/debian"

    `http://10.10.10.10:8091/machines/7f65279a-7e5c-4e69-af40-dd01af4c5667/seed`

!!! example "ubuntu 20.04 and newer"

    `http://10.10.10.10:8091/machines/7f65279a-7e5c-4e69-af40-dd01af4c5667/autoinstall/user-data`

!!! example "centos/redhat"

    `http://10.10.10.10:8091/machines/7f65279a-7e5c-4e69-af40-dd01af4c5667/compute.ks`

!!! tip

    A simple trick ... you can create a non-existent Machine, and place
    that machine in different BootEnvs to render provisioning files for
    testing purposes. For example, put the non-existent Machine in the
    `centos-8-install` Stage, then render the `compute.ks` kickstart URL
    above.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

- [](#rs_arch)

### Versions

all

### Keywords

kickstart, preseed, render template, bootenv, compute.ks, seed

### Revision Information

```
KB Article     :  kb-00031
initial release:  Wed Jun 10 12:28:40 PDT 2020
updated release:  Thur Apr 7 10:40:30 CDT 2022
```

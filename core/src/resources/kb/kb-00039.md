---
title: RBAC - Limit Users to Just Poweron and Poweroff IPMI Controls
tags:
  - reference
  - operator
  - kb
---

# RBAC - Limit Users to Just Poweron and Poweroff IPMI Controls {#rs_kb_00039}

This article describes how to create a limited *role* with *claims*
(rights) to only poweron and poweroff Machines. This example can be used
as the foundating to understand how to add additional Roles with
different Claims capabilities.

## Solution {#rs_rbac_limited_user}

The Role Base Access and Controls subsystem allows an operator to
construct user account permissions to limit the scope that a user can
impact the Digital Rebar Provision system. Below is an example of how to
create a *Claim* that assigns the `Role` named `prod-role` that limits
to only allow IPMI `poweron` and `poweroff` actions. These permissions
are applied to the **specific** set of **scope** *Machines*:

``` bash
drpcli roles update prod-role '"Claims": [{"action": "action:poweron, action:poweroff", "scope": "machines", "specific": "*"}]'
```

Now simply assign this Role to the given users you wish to limit their
permissions on.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

- [](#rs_arch_auth_models)
- [](#rs_arch)

### Versions

all

### Keywords

poweron, poweroff, limited scope user, claims, roles, rbac, role based
authentication controls

### Revision Information

```
KB Article     :  kb-00039
initial release:  Wed Jun 10 12:54:37 PDT 2020
updated release:  Wed Jun 10 12:54:37 PDT 2020
```

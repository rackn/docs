---
title: Generating Scoped Tokens
tags:
  - reference
  - operator
  - kb
---

# Generating Scoped Tokens {#rs_kb_00078}

This document describes how to generate a scoped Token for use with
Digital Rebar Platform (DRP).

DRP tokens are based on JWT (java web token) format, and can specify
very highly scoped actions that allow the bearer of the token specific
rights.

Tokens can be used for integrations to DRP, individual user use for
authentication in place of Username / Password authentication, for
specific entitlement use with extremely limited rights (think "least
privilege") needs, etc.

A generated token may include some or all of the permissions that a
given user has based on their role(s). Tokens can not contain more
privileges than the users role(s) provide.

Token permissions are based on DRP *claims*, which is a triplet that
describes the *Scope* of the claim, the *Action(s)* allowewd, and a
*Specific* filter to refine the *claim*.

## Solution {#rs_generating_scoped_tokens}

Creating a token is pretty straight forward with the CLI, basic syntax:

``` bash
drpcli users token USER ...<token specific options>...
```

The token specific options are where it gets fun. The primary options
are a Time to Live (TTL) value and a Scope/Action/Specific triplet
(called the *Claim*) that defines rights and access permissions for the
token.

Some detailed documentation on Token authentication can be found at:

- <https://docs.rackn.io/en/latest/doc/arch/auth.html#how-authentication-works>
- <https://docs.rackn.io/en/latest/doc/arch/auth.html#claim>

To create a superuser level token with rights to everything based on the
user "`rocketskates`", which is only valid for 1 hour:

``` bash
drpcli users token rocketskates ttl 1h scope '*' action '*' specific '*'
```

!!! note

    You must protect the wildcard from shell expansion.

The wildcards are implied by default if not specified, so you could just
do:

``` bash
drpcli users token rocketskates ttl 1d
```

Valid token TTL descriptions (eg *1h*, *1y*) can be found in the
following Knowledge Base article:

- <https://docs.rackn.io/en/latest/doc/kb/kb-00076.html>

!!! note

    The "`drpcli users token ...`" command outputs a JSON
    structure with a lot of info. The output data structure also is a good
    guideline of what API calls, and VERBs you can use with those calls that
    are available.

The Token itself can be parsed out via jq, by appending:

!!! example 

    ``` bash
    # append shell pipeline to parse out the Token from the JSON response
    drpcli users token rocketskates ttl 1d | jq -r .Token
    ```

Here is a simple example, that creates a Token with only permission to
modify one specific Machine object, which is only valid for 30 days:

!!! example

    ``` bash
    drpcli users token rocketskates ttl 30d \
      scope machines action '*' specific '48de76fd-c5c7-4b05-9f70-52af9a996d38'
    ```

That Token should only have authorizations to make changes to the
specified Machine with the UUID listed. No other Machine objects can be
modified with the use of the Token.

Multiple scope/action/specific triplets ("*claims*" as we call them)
can be supported. Here is a more complex example that replicates
creating a Token that has the same Claims permissions as the
"`readonly`" role:

``` bash
drpcli users token rocketskates ttl 30d \
  scope bootenvs,files,interfaces,isos,jobs,leases,machines,params,reservations,roles,stages,subnets,tasks,templates,users,workflows,profiles action get,list specific '*' \
  scope info action get specific '*' \
  scope objects,preferences,contents,plugin_providers action list specific '*'
```

Additional Information
----------------------

Additional resources and information related to this Knowledge Base
article.

- Token auth with the CLI (`drpcli`) can be done with the `-T` flag
  (eg `drpcli -T $TOKEN ...`)
- Token rights (claims) can only be conferred if the specified
  User/Role has those rights assigned to them
- Tokens are used as "bearer" authentication in API interactions
- Tokens are based on JWT format, but are not interchangeable with
  other services that use JWT tokens
- Tokens are cached by the CLI, when testing, remove the Token cache
  first (`rm -rf $HOME/.cache/drpcli`)

### See Also

- [](#rs_how_auth_works)
- [](#rs_arch_models_claim)
- [](#rs_kb_00076)

### Versions

All Versions

### Keywords

token, bearer, authentication, api, scopes, roles, claims

### Revision Information

```
KB Article     :  kb-00078
initial release:  Sat June 4 07:35:36 PDT 2022
updated release:  Sat June 4 07:35:36 PDT 2022
```

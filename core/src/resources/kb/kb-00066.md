---
title: "DRP Endpoint 'connect: connection refused' or 'i/o timeout' errors"
tags:
  - reference
  - operator
  - kb
---

# DRP Endpoint 'connect: connection refused' or 'i/o timeout' errors {#rs_kb_00066}

This document describes some possible reasons for the following errors
when trying to use `drpcli` to connect to a DRP Endpoint. Possible
errors discussed in this document:

- `Error: Error creating Session: CLIENT_ERROR: Get "https://127.0.0.1:8092/api/v3/users/rocketskates/token": dial tcp 127.0.0.1:8092: connect: connection refused`
- `Error: Error creating Session: CLIENT_ERROR: Get "https://127.0.0.1:8092/api/v3/users/rocketskates/token": dial tcp 127.0.0.1:8092: i/o timeout`

These errors are almost always a result of an intervening Firewall/ACL
that is blocking the connections to the DRP Endpoint API port. The
"Additional Section" shows examples of IPTables commands that can
induce either of these errors as follows:

- `i/o timeout` is caused by use of the `DROP` directive
- `connect: connection refused` is cause by use of the `REJECT` directive

## Solution {#rs_drp-endpoint-connection-refused-or-io-timeout-errors}

Adjust any intervening Firewalls/routers/switch ACLs to not block the
API and other important ports as described in the Server Documentation
[](#rs_arch_ports) section.

For `iptables` on a host, verify that no rules with `DROP` or `REJECT`
(among other possibilities) are blocking traffic.

## Additional Information

Additional resources and information related to this Knowledge Base
article.

Here is an example flow of showing the above error conditions with the
use of `iptables` on the host running the DRP Endpoint service, blocking
the API port.

Exmaple deployment on Ubuntu 20.04.3 system (without any host based
firewalls), then install DRP itself, with:

``` bash
curl -s get.rebar.digital/tip | bash -s -- install --universal
```

This should succeed in a couple of minutes.. Check the service with:

``` bash
systemctl status dr-provision
```

If you did a "developer" or "isolated" install, you may need to
verify the service is running with:

``` bash
ps -ef | grep -v grep | grep dr-provision
```

Once the service is running, `drpcli` should be able to connect and
validate with the command `drpcli info status`. If default admin
username/password has been changed at install time, adjust the `drpcli`
client to use the correct authentication details. For example:

``` bash
drpcli -U admin -P password -E https://127.0.0.1:8092 info status
```

Note the `-E` endpoint specified above is the default URL that the CLI
will attempt to hit. `drpcli` can be run from any remote system,
assuming it has network/security/acl/firewall access to the API port on
the system (8092 by default; see [](#rs_arch_ports)
for more details). If alternate ports specified during the
install; adjust the below tests/statements accordingly.

To replicate the above defined error conditions, using `iptables` and
the following directives:

- `i/o timeout` is caused by use of the `DROP` directive
- `connect: connection refused` is cause by use of the `REJECT`
  directive

First show that the service is up and running with no intervening
firewall

``` bash
drpcli info status
```

``` json
{
  "API": {
    "Alive": true,
    "Enabled": true,
    ... snip ...
}
```

Now, inject the `REJECT` directive, and test again:

``` bash
iptables -A INPUT -p tcp --destination-port 8092 -j REJECT
```

Checking our `info status` CLI call again will result in the *connection
refused* error condition:

``` bash
drpcli info status
```

```
Error: Error creating Session: CLIENT_ERROR: Get "https://127.0.0.1:8092/api/v3/users/rocketskates/token":
dial tcp 127.0.0.1:8092: connect: connection refused
```

Reset the Firewall rules, then test for *i/o timeout* error.

!!! note

    Do not flush the tables like this on a system with configured rulesets,
    this test assumes you are using a development system without any
    "production" (or otherwise) important `iptables` rulesets.

Flush the tables:

``` bash
iptables -F
```

Now, set the rule to `DROP` the traffic, to generate the *i/o timeout*
condition.

``` bash
iptables -A INPUT -p tcp --destination-port 8092 -j DROP
```

Now our `info status` command will fail accordingly:

``` bash
drpcli info status
```

```
Error: Error creating Session: CLIENT_ERROR: Get "https://127.0.0.1:8092/api/v3/users/rocketskates/token":
dial tcp 127.0.0.1:8092: i/o timeout
```

To remove the firewall ruleset, again, flush the rules.

``` bash
iptables -F
```

### See Also

- Server Documentation [](#rs_arch_ports)
  section
- Port binding error in `rs_bind_error`{.interpreted-text
  role="ref"} Knowledge Base article

### Versions

all

### Keywords

i/o timeout, connection refused, failed connection, unable to reach drp
endpoint

### Revision Information

```
KB Article     :  kb-00066
initial release:  Fri Sep 24 18:36:41 PDT 2021
updated release:  Fri Sep 24 18:36:41 PDT 2021
```

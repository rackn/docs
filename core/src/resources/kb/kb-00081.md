---
title: Proxmox VM create fails with tls_process_server_certificate error
tags:
  - reference
  - operator
  - kb
---

# Proxmox VM create fails with tls_process_server_certificate error {#rs_kb_00081}

When using the Proxmox *Resource Broker* to create Machines, the
following error occurs:

```
Error: error creating VM: 596 tls_process_server_certificate: certificate verify failed
```

This typically occurs on initial create, expanding, or shrinking a
*Cluster* which is backed by a Proxmox *Resource Broker*.

The error occurs even when the `proxmox/tls-insecure` Param is set to `true`.

## Solution {#rs_Proxmox_VM_create_fails_with_tlsprocessservercertificate_error}

Proxmox hypervisors are excruciatingly sensitive to the hypervisor node
name, which must be hard coded in the `/etc/hosts` file prior to setup
of the Proxmox Virtual Environment. The hypervisor node name can NOT be
changed after the setup has been performed.

There are two primary Params that are set in the *Cluster* state
tracking Profile. These values **MUST** match the configured hypervisor
node name and the TLS Certificate disposition. They are:

- `proxmox/node`: The initial setup hyprvisor node name
- `proxmox/tls-insecure`: Defines trust of the TLS certificate,
  requires `true` for self-signed certificates

The value in `proxmox/node` must be DNS resolvable, and must exactly
match the initial Proxmox install value. It is NOT possible to set this
to an IP Address value, if the hypervisor node was set to named value
(for example `proxmox01`).

The node name value can ONLY be changed after initial Proxmox hypervisor
setup is complete if there are no Virtual machines or Containers.

Correct the `proxmox/node` value to use the correct node name.

A complete example Job Log output with the failure information is below.
Note that several values are specific to the environment (hypervisor,
cluster name, virtual machine names, etc)

>     Plan: 1 to add, 0 to change, 0 to destroy.
>     proxmox_vm_qemu.drp_machine["raven-0"]: Creating...
>
>     Error: error creating VM: 596 tls_process_server_certificate: certificate verify
>     failed, error status: (params: map[agent:0 args: balloon:1024 bios:seabios
>     boot:order=net0;scsi0 cores:2 cpu:host description:DRP deployed VM on Proxmox node 10.10.1.102 hotplug:network,disk,usb kvm:true machine: memory:4096 name:raven-0 net0:virtio=12:C6:E7:E3:88:C8,bridge=vmbr0 numa:false onboot:false ostype:l26 scsi0:local-lvm:32,cache=writeback,discard=on,iothread=1,ssd=1 scsihw:virtio-scsi-single sockets:1 startup: tablet:true tags: vmid:100])
>
>     with proxmox_vm_qemu.drp_machine["raven-0"],
>     on digitalrebar.tf line 71, in resource "proxmox_vm_qemu" "drp_machine":
>
>     71: resource "proxmox_vm_qemu" "drp_machine" {
>
>     !!!! terraform apply failed! !!!!
>     Removing empty terraform.tfstate file
>     Saving .terraform.lock.hcl file to machine
>     Did not succeed - fail

## Additional Information

Additional resources and information related to this Knowledge Base
article.

### See Also

- [Renaming a PVE Node](https://pve.proxmox.com/wiki/Renaming_a_PVE_node)
- [](#rs_content_proxmox)

### Versions

DRP v4.6.0 and newer, with `proxmox` content installed v4.6.0 or newer

### Keywords

proxmox, hypervisor, node, tls, tls\_process\_server\_certificate,
terraform apply, resource broker, cluster

### Revision Information

```
KB Article     :  kb-00081
initial release:  Tue Nov 22 16:43:43 PST 2022
updated release:  Tue Nov 22 16:43:43 PST 2022
```

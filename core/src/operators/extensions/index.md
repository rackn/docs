---
title: Concepts
tags:
- reference
- operator
---

As an Operator, you will want to audit, use the built resources, and build clusters out of the
provisioned resources.  This section will describe building and using addition DRP Endpoint features
to expand your automation footprint.

* Contexts
* Context workers
* Batch Operations
* Audit
* Clusters
* Brokers


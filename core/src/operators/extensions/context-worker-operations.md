---
title: How-to Set up a Context Worker Node Set
tags:
  - howto
  - operator
---

The purpose of this article is to describe how to set up a Context Worker Node set.

## Creating a Cluster of Worker Nodes {#rs_context_worker_operations}

Building a set of context worker nodes can be easily achieved using
the `universal-application-context-worker-cluster pipeline`.

In the user interface, you can initiate the process by creating a cluster, providing it a name, and then selecting the
`universal-application-context-worker-cluster` pipeline.

Upon selection, a series of fields will appear for you to complete:

- `resource/broker`: This requires a virtual machine broker or pool-broker for machine creation.
- `docker-context/tags`: These are the tags that the context worker set should monitor.

The default configuration of this cluster pipeline will build `CentOS-8` bare metal servers with `Podman` and
the `Docker` contexts installed.

If cloud resources are utilized, the OS required by the broker will be chosen, and `Podman` or `Docker` will be
installed with Docker context.

The end result is a set of machines running a standalone docker-context plugin in a provisioned OS (or cloud instance).
These machines will monitor the `docker-context/tag` parameter to check if it matches the `docker-context/tags` on
itself for creating contexts.

## Creating a Cluster of Context Workers

Creating a set of context workers is straightforward. Simply create a cluster using the context-broker with a
specific `docker-context/tag`. This tag should reference one of the `docker-context/tags` created on the cluster of
worker nodes.

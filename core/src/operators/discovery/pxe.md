---
title: PXE Discovery
tags:
- operator
- howto
---

# PXE Boot {#rs_ops_discovery_pxe}

## Configure Subnet

To enable machines to PXE boot via DRP, configure the relevant subnet in DRP to match the machines' subnet. You can set
this up either through the DRP UI or using drpcli on the command line.

### Configuring subnet from UX

- Navigate to the `Subnets` section of the UX. This is present under the `Networking` section in the nav bar
- Click on `create`
- Choose the relevant subnet or add it manually. Ensure DRP can access this network.

### Configuring subnet from CLI

- Prepare a JSON object detailing your subnet. Refer to the [subnet object](#rs_arch_models_networking_subnet) for the
  required format.
- Once your json object is create, you can use the following DRPCLI command to create your subnet

```shell
echo '{
  "ActiveEnd": "192.168.122.254",
  "ActiveLeaseTime": 3600,
  "ActiveStart": "192.168.122.10",
  "Enabled": true,
  "Meta": {
    "icon": "cloud",
    "color": "black",
    "title": "User added"
  },
  "Name": "example-subnet",
  "OnlyReservations": false,
  "Options": [
    {
      "Code": 3,
      "Value": "192.168.122.1"
    },
    {
      "Code": 6,
      "Value": "192.168.122.143"
    },
    {
      "Code": 15,
      "Value": "test.example.com"
    }
  ],
  "Pickers": [
    "hint",
    "nextFree",
    "mostExpired"
  ],
  "Proxy": false,
  "ReservedLeaseTime": 21600,
  "Strategy": "MAC",
  "Subnet": "192.168.122.143/24",
  "Unmanaged": false
}' > /tmp/example_subnet.json

drpcli subnets create - < /tmp/local_subnet.json
```

Once set up, machines in these subnets are ready to PXE boot into DRP.

## Set Bootenv Preferences (non universal install)

!!! note 
    
    This part can be skipped if the DRP install was done with `--universal` flag. Universal install sets the 
    necessary preferences.

By default, Digital Rebar Provision (DRP) attempts to "do no harm". This means that by default, any system that receives
a DHCP lease from the DRP Endpoint, will by default, be set to `local` mode, which means boot from local disks. You must
explicitly change this behavior to enable provisioning activities of Machines.

```shell
drpcli prefs set unknownBootEnv discovery defaultBootEnv sledgehammer defaultStage discover
```

Once these preferences are set, new machines discovered on the configured subnets will boot into sledgehammer.

---
title: Manual Join Up
tags:
- operator
- howto
---

# Manual Join {#rs_ops_discovery_join_up}

TODO: Describe join-script usage for windows, linux, and esxi.

Another way a machine can be added to DRP is by using the `join-up.sh` script. Some examples of this usage can be found
here:

- [Join a Machine to DRP endpoint on AWS](#rs_aws_joinup)
- [Join a machine to a DRP Endpoint in Linode](#rs_linode_joinup)



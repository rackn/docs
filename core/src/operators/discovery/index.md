---
title: Discovery
tags:
- reference
- operator
---

As an Operator, you want to be able to add infrastructure to a DRP Endpoint.

The Discovery processes are how assets join DRP.

You will need to be able to configure and operate the discovery methods.

## Discover

* PXE-based discovery
* IPMI-Scan discovery
* Join up existing systems
* Cloud-based Created Resources
* LPAR-based systems

* Whoami allows for rediscovery

## Inventory

* Collect information about the systems

## Classification

* Configure classification to automate building of servers


TODO: What to do with vd booting

---
title: Uploading Content and Keys
tags:
  - portal
  - reference
  - catalog-item
  - content
  - context
---

The Portal has different views for the type of data being uploaded:

* [Content Packs, Plugins, and DRP](#content-packs-and-plugins) - Add new objects and features to DRP
* [Files and Archives](#files-and-archives) - Give the endpoint access to new files
* [Context Images](#downloading-context-images) - Provide contexts engines with images
* [ISOS](#files-and-archives) - Give the provisioner access to new ISOs
* [SSH Keys](#ssh-keys) - Add a key to the global profile

These views can be found in the "IAC Artifacts" nav entry:

![Screenshot of the IAC Artifacts section in the sidenav](images/contents_nav.png)

## Content Packs and Plugins {#content-packs-and-plugins}

Content packs and plugins are managed in the "Catalog" sidenav entry.

Portal Link: [portal.rackn.io/#/e/0.0.0.0/catalog](https://portal.rackn.io/#/e/0.0.0.0/catalog)

![Screenshot of Catalog view showing the header and first few rows](images/catalog_preview.png)


The table in the catalog view shows both content that is installed and available content from configured sources. By default, this is from an official RackN Catalog. Hovering over the "Sources" tag in the view's header will show a list of the catalog sources:

![Screenshot of the sources widget in the header of the catalog view](images/catalog_sources.png)

### Install Content {#install-content}

If you don't have a content pack or plugin file, there are a few ways to install content from the configured source:

One at a time, by clicking the green download button.

![Screenshot of the catalog table with a green download button circled in red](images/catalog_download_button.png)

Or many at a time, by selecting multiple row and clicking the "Install" action button.

![Screenshot of the catalog view with both a few selected rows and the Install action button circled in red](images/catalog_download_bulk.png)

The target version to install can be changed by clicking on the version dropdown in the "Available" column.

!!! note

    Content installed this way also installs prerequisites.

!!! note

    You can install DRP upgrades through the catalog by installing the "drp" entry. You should upgrade DRP before upgrading other content. If you install many at a time, the Portal will automatically prioritize DRP.

### Uploading Content {#uploading-content}

If you have a content pack file, plugin file, local UX archive, a DRP Binary, or a URL that links to one of those, you can click the "Upload" action button to see the following modal:

![Screenshot of the content upload modal in the catalog view](images/catalog_upload.png)

!!! note

    The "Download from URL" feature downloads directly to your browser before uploading to DRP. Some files may be too large for this and will need to be uploaded via CLI.

When dragging in a file or downloading from a URL, this wizard will attempt to generate an "Item ID" from the file provided corresponding to the content id within DRP.

Clicking the "Upload" button will finalize the upload.

!!! note

    Content installed this way will not install prerequisites.

### Removing Content {#removing-content}

Removing content, similar to installing multiple content at a time, involves selecting multiple rows and clicking the "Remove" action button. After clicking "Remove" you will be prompted to confirm before finalizing the removal.

![Screenshot of the catalog view with both a few selected rows and the Remove action button circled in red](images/catalog_remove.png)

!!! note

    Removing content will not remove prerequisites and some content must be uninstalled before others for this reason. Errors will appear if the content is in-use or cannot be removed for whatever reason.

## Files and Archives {#files-and-archives}

The files view shows files accessible from DRP's file server.

Portal Link: [portal.rackn.io/#/e/0.0.0.0/files](https://portal.rackn.io/#/e/0.0.0.0/files)

![Screenshot of the files view](images/files_preview.png)

* Clicking on the blue "Copy" icon will copy the file or folder's path to your clipboard.
* Clicking on the blue "#" icon on file rows will copy the Sha256 sum to your clipboard.
* Clicking on the blue "Upload" icon on folder rows open the [Upload Modal](#uploading-files)
* Clicking on a row with a folder icon will expand within the table to show the contents.

![Screenshot of the an expanded folder in the files view](images/files_expand.png)

### Uploading Files {#uploading-files}

Files can be uploaded via an upload modal. This modal can be opened in one of two ways:

* Clicking the "Upload" icon on the right side of folder rows
* Clicking the "Upload" action button

![Screenshot of an upload icon on the right hand side of the files view table circled in red](images/files_upload_icon.png)

![Screenshot of the upload file modal in the files view](images/files_upload.png)

!!! note

    The "File Path" field in the upload modal is automatically filled when using the "Upload" table icon.

The "Explode File" checkbox will tell DRP to compress an uploaded archive at the specified "File Path".

### Removing Files

Files can be removed similar to how other objects are removed from their tables. Select the desired rows, then click the "Delete" button. You will be prompted to confirm before removing the files. Selecting folders that are closed will not select child files or folders. Non-empty folders cannot be deleted.

![Screenshot of the the files view with both a few selected rows and the Delete button circled in red](images/files_remove.png)


### Downloading Context Images {#downloading-context-images}

Context images are stored within DRP's file store. The Portal provides buttons for downloading context images for your convenience. Downloaded Contexts are uploaded to `/contexts/<Engine>/<Image>`.

From the contexts table, clicking the blue download button will start the download. These buttons will turn into green checks when the respective image is already downloaded.

![Screenshot of a download icon circled in red within the Contexts view](images/context_download_table.png)

From the contexts inspector, clicking the download button in the "Image" editor field will start the download.

![Screenshot of a download button circled in red within the Contexts view](images/context_download_inspector.png)

These files can be removed from the Files view.

## ISOs

The Boot ISOs view shows ISOs accessible from DRP's file server.

Portal Link: [portal.rackn.io/#/e/0.0.0.0/isos](https://portal.rackn.io/#/e/0.0.0.0/isos)

![Screenshot of the ISOs view](images/isos_preview.png)


### Uploading ISOs

ISOs can be uploaded via an upload modal opened by clicking the "Upload" action button.

![Screenshot of the upload ISO modal in the ISOs view](images/isos_upload.png)

### Removing ISOs

ISOs can be removed similar to how other objects are removed from their tables. Select the desired rows, then click the "Delete" button. You will be prompted to confirm before removing the iSOs.

![Screenshot of the the ISOs view with both a few selected rows and the Delete button circled in red](images/isos_remove.png)

## SSH Keys {#ssh-keys}

SSH Keys are stored within params and can be added across the system via the global profile.

Global Profile Portal Link: [portal.rackn.io/#/e/0.0.0.0/profiles/global](https://portal.rackn.io/#/e/0.0.0.0/profiles/global)

![Screenshot of the global profile inspector within the profile view](images/global_profile_keys.png)


### Adding Keys

Info & Preferences Portal Link: [portal.rackn.io/#/e/0.0.0.0/system](https://portal.rackn.io/#/e/0.0.0.0/system)

From with the "Info & Preferences" view, an "Add SSH Key" action button can be clicked to open the SSH Key wizard.

![Screenshot of the info & preferences section of the side nav](images/info_nav.png)
![Screenshot of the "Add SSH Key" button in the header of the Info & Preferences View](images/info_add_key.png)

This wizard allows you to either paste in your public key or manually paste it. It will automatically update the global profile's `access-keys` param with your key.

![Screenshot of the Add SSH Key modal in the Info & Preferences View](images/info_add_key_modal.png)

### Removing Keys

SSH keys have to be manually removed from the `access-keys` param on one of:

* The global profile
* A profile on the machine
* A param on the machine

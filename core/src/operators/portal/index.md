---
title: Portal Access
tags:
  - portal
  - reference
---

# Portal {#rs_portal}

## Description

The RackN Portal provides a complete graphical solution to manage
Digital Rebar Provision. It can be accessed at the following locations:

* [portal.rackn.io](https://portal.rackn.io) - Stable
* [tip.rackn.io](https://tip.rackn.io) - Latest
* [test.rackn.io](https://test.rackn.io) - Occasional Experimental Features

The RackN Portal connects to DRP via the REST API and maintains a Websocket connection to receive updates. It provides various tables, inspectors, and some wizards for browsing the state and health of an endpoint. It also provides access to RackN's [self service license manager](#rs_self_service_license) for self-guided trials.

## Access

To use the portal, you must have access to a running DRP endpoint. You can connect to endpoints via the [home page](#home-page), [URL](#url), or [recently used endpoints](#recent-endpoints).

When connecting to an endpoint with a self-signed certificate, that cerificate must be accepted in-browser before accessing DRP. The portal will provide warnings if this certificate is not accepted. Sometimes your browser may need to be restarted if it forgets that you accepted the certificate and it has been open for more than a few days.

### Home Page {#home-page}

The home page has a text area where the endpoint address can be input

![Screenshot of the the endpoint connection form on the home page](images/ux_connect_endpoint.png)

Clicking on the blue arrow button will redirect to <https://portal.rackn.io/#/e/127.0.0.1:8092/>

### URL {#url}

You can manually input your endpoint address into your browser's url bar:

 - drp.example.com -> <https://portal.rackn.io/#/e/drp.example.com/>
 - 127.0.0.1:8092 -> <https://portal.rackn.io/#/e/127.0.0.1:8092/>

### Recent Endpoints {#recent-endpoints}

If you have connected to the desired endpoint before, you can hover the Endpoint address or "Recent Endpoints" dropdown on the top left of the page:

![Screenshot of the recent endpoints dropdown on the top navigation bar](images/ux_recent_endpoints.png)

Clicking on the link to the endpoint will navigate to the portal login page for that endpoint.

Clicking on the "[new endpoint]" button will redirect to the [Home Page](#home-page)

## Login

When accessing an endpoint, you may be prompted to "Click URL to verify". This means the browser does not trust the self-signed certificate present on the endpoint. It is safe to click the endpoint url and accept the cerificate. This indicator will disappear when the endpoint can be reached.

![Screenshot of the login form with a label pointing towards the endpoint url that says Click URL to Verify](images/ux_login_unverified.png)

The "Remember Token" checkbox determines if an authorization token is stored in the browser.

### SAML

If you have properly configured [SAML](#rs_ops_saml) one or more [Identity Providers](#rs_identity_provider), you will see a prompt on the right for those authentication methods. To use one, click the "Login via ProviderName" button on the right.

![Screenshot of the login form with another form to the right with a button indicating an option to login with a Google account](images/ux_login_oauth.png)

## Layout

The portal is primarily navigated via the side navigation and in-interface hyperlinks from tables and "inspectors".

### Side Navigation

![Screenshot of the portal's side navigation, scrolled to the bottom](images/sidenav_info.png)

The side navigation is made up of a few parts. From top to bottom, they are:

* The Endpoint ID and a link it.
* A megaphone icon button that toggles the "Live Event Panel", showing websocket events.
    * This panel will automatically open when an error occurs.
    * This button will flash red when the websocket closes unexpectedly.
* Collapsible categories that contain links to the views within the Portal
* A "Digital Rebar Info" section containing version, license, and architecture info.
* A set of helper links for troubleshooting:
    * **Clear UX Cache** - Resets the more persistent browser object storage for this endpoint. This may fix when icons are not loading inside tables.
    * **Clear Local and Session Storage** - Resets the shallow browser storage for this object. This may fix certain crashes but will clear some user preferences.
    * **Copy Endpoint Info** - Copies a JSON blob containing endpoint metadata (including `drpcli info get`) to clipboard
    * **Copy Endpoint Catalog** - Copies a JSON blob containing all installed content to clipboard
    * **Copy Auth Token** - Copies a DRP Authorization token to clipboard. Note that this token expires when the Portal's configured tokens expire.
    * **Copy Login Link** - Copies a link that will automatically login to this endpoint from portal.rackn.io. The certificate may still need to be accepted.
* A set of policy links for your convenience.

### Table Views

Below is a labeled screenshot of the Machines page, outlining some key features of the interface.

![Screenshot of the machines table in the portal with labels correlating to the text below](images/ux_layout_table.png)

* **Bulk Actions** - A list of queued bulk actions, added by the Actions dropdown.
* **Columns** - Table columns that can be reordered, resized, and clicked to sort the table.
* **Logout** - A button that signs you out.
* **Object Table** - The rows in the table, reflecting objects within the endpoint.
* **Pagination** - Page information for the state of the table.
* **Quick Search** - Enables navigation to other views without using the side nav.
* **Recent Endpoints** - A list of endpoints that have been recently accessed.
* **Side Navigation** - Links to other views in the portal.
* **User Link** - A link to the currently logged-in user's page.
* **View Action Buttons** - Actions specific to this table.

Hyperlinks and some icons inside the table link to other views in the portal.

#### Bulk Actions

The "Actions" dropdown next to the action buttons allows for running bulk actions across all selected objects in the table.

Some of the bulk actions run immediately and others open the bulk actions panel to the left for configuring and queuing multiple actions.

Clicking the "Commit action" button when available will run all queued actions in order.

![Screenshot of the bulk actions panel and dropdown inside the Machines view](images/bulk_actions.png)

### Inspector Views

The `Name` or `Id` column in most tables opens a side panel referred to as the "Inspector". This panel can be closed by clicking the "X" button on the top left of the panel, or by pressing the back button within your browser. The side panel can also be resized via the 3-dot resize gimbal in the middle of the left border of the panel.

The first tab, Editor, shows commonly edited fields for the inspected object.

![Screenshot of the object inspector view with the editor tab focused](images/ux_layout_editor.png)

The final tab, API, shows a YAML or JSON representation of the object, reflecting the data retrieved from the DRP API.

![Screenshot of the object inspector view with the api tab focused](images/ux_layout_editor_api.png)

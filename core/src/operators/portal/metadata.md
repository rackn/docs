---
title: Metadata Fields
tags:
  - portal
  - metadata
  - reference
---

The following items are commonly used on all Object types to provide UX rendering guides:

- `icon: [icon name]` - sets the icon
- `color: [color name]` - set the color

The following items are commonly used on Params to provide UX rendering guides:

- `password: [anyvalue]` - renders as password (NOT encrypted, just obfuscated)
- `clipboard: [anyvalue]` - provides a copy button so user can copy the param contents to clipboard.
- `readonly: [anyvalue]` - does not allow UX editing
- `render: link` - adds an <https://{value}> link field that opens in a new tab.
- `render: multiline` - presents a textarea for string inputs
- `render: raw` - presents textarea to user instead of regular edit, does not reformat
- `downloadable: [file name]` - provides download link so a user can download the param contents as a file. Name of the
  file is the value of the Meta.downloadable.
- `route: [object name]` - when included, the UX will provide a link to the underlying object as per the Param value

A complete list of .Meta values are used throughout Digital Rebar for both UX and operational controls. Please consult
[Object.Meta](#rs_object_metadata).

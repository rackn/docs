---
title: License Management
tags:
  - portal
  - license
  - reference
---

# Self service License Management {#rs_self_service_license}

## Welcome Page

When you login to the Portal on an endpoint without a license, you will be prompted with the following options:

- **Start Trial** - Generate and download a new license
- **Upload License** - Upload an existing license file
- **Recover License** - Download a license from backup codes
- **Support & Contact** - Our support hours

![Screenshot of the RackN Portal welcome page with the Start Trial tab selected](images/ux_license_trial.png)

### Trial License Download {#trial-license-download}

Filling out the Trial License form enters your license into our system. From there it can be converted into paid or home license by request.

By default, all licenses generated through this means are short term **10 machine 10-day bootstrap licenses**. Submitting a **license extension form** will upgraded that license to a **20 machine 90-day trial license**.

Once the trial license form is completed, a copy of the license will be automatically downloaded and you will be shown some "Scratch Codes" and an "Identifier".

These codes can each be used once in the [License Recovery](#license-recovery) tab if you lose access to the license file. Due to this limitation it is recommended that you upload your license file directly rather than use up these codes.

![Preview of the final page of the trial license wizard](images/ux_license_trial_download.png)

### License Upload

If you have already filled out the trial license form or you have downloaded your license manually from the [License Manager](#management), you can upload your license to another endpoint in the Upload tab. Note that you will need to click "Check and Update" in the License Manager to add additional endpoints to your license.

![Preview of the license uploading tab](images/ux_license_upload.png)

### License Recovery {#license-recovery}

As mentioned in [Trial License Download](#trial-license-download), scratch codes that were generated once the trial license form is completed can be used as another means to download a license to your endpoint.

![Preview of the the license recovery tab](images/ux_license_recover.png)

## Management {#management}

The License Manager page assists in acquiring RackN issued licenses and
managing a currently installed license.

![Preview of the license manager page](images/ux_license_bootstrap.png)

A breakdown of license manager page is as follows:

-   **Header**: License owner, license icon, and license version
-   **License Status**: License verified, registration, and renewal
    status. If any of these three icons are an X, an updated license may
    need to be installed either through the "Check and Update License"
    button in the header or by the "Authorize" button in the License
    tab of the RackN Service section.
-   **Installed License**: Overviews the terms of the license. Maximum
    number of machines can be seen next to the \# icon in the
    "Machines" subsection.

A copy of the installed license can be downloaded at any time with the
blue "Download License" button at the top of the view.

### License Extension

Clicking the green "Verify Account" button when an unverified license is present will navigate you to the License Extension Form.

![Preview of the license extension form](images/ux_license_extension_form.png)

Once this form is submitted, you can head back to the License Manager page and click the "Check and Update License" to download the updated license, if available.

![Preview of the license extension form](images/ux_license_extended_trial.png)


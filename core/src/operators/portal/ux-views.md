---
title: UX Views
tags:
- portal
- reference
---

# UX-Views {#rs_ux_views}

UX views defines a set of objects that can be used to configure and customize the UX.

## Basic Operations

This plugin is a schema only at this time. It has no other opreational characteristics. It requires the RackN UX. It is
appareance modification only: it does NOT enforce additional security.

### Role Mapping

To use the UX View plugin, you must define a role that matches the Id of the ux_view. The UX will automatically apply
the ux_view that matches the FIRST role defined for the user. Only the first role is matched.

### Preview Mode

You are able to preview the view when using the RacKN UX for this plugin. This applies the UXView to the currently
operating user and will be automatically reset when the browser is refreshed.

## UXView

### ApplicableRoles

Type: Array of Strings

This field defines what roles should have this view as an option. The UX will present the ux-view in the list of views
that a user with any of these roles.

### Meta.priority

Type: String (but a numeric value)

This field defines the sorting priority for UX Views for selection as initial view and within the UX View Role dropdown.
UX Views with higher priority value will show up sooner in the dropdown and the UX View with the highest priority will
be selected if an initial view was not already selected.

### Airgap

Type: Boolean

Boolean field that overrides the `ux-air-gap` parameter (if set) to enable the UX to be run in Airgap mode.

### LandingPage

Type: String

If set to a string starting with http then it will change the default landing page to Overview with iframe to given URL
instead of system view.

If set to anything else then it will change the default landing page to machines and apply the value provided as the
Filter. Use
`*` to use machines as the landing page without any filter.

Setting LandingPage to `none` will disable the LandingPage and use the system page instead.

### ShowActivation

Default: true

Shows or hides the RackN online license activation panel in License Management.

### BrandingImage

Default: none (uses RackN image)

Replaces the RackN Logo in the top left corner with an image served from the `https://[API endpoint]/files/` path.

To use this feature, you must provide the replacement image (30px high!)
in the files location.

### Menu

Type: Array of Objects

Menus are defined using the group / item hierarchy. They *must* map into existing views and order is preserved.

Required Values:

- Id - unique value for menu item, typically is the name of the view to open
- Title - text shown to the user for the menu item

Optional Values

* Icon - icon for menu item, from <https://react.semantic-ui.com/elements/icon/>
* Color - color for menu item icon, one of: red, orange, yellow, olive, green, teal, blue, violet, purple, pink, brown,
  grey, black
* To - when using filter and non-view Ids, provide a To for the view name
* Filter - the name of a filter defined for the view from the Filter section. Cannot use user-defined views, the
  Filter *must* be a defined view.
* Overwrite - the name of an existing route that this new path `/{Id}` will reference. When used, Filters are applied to
  the table instead of in the route via `?filter={Filter}`
* GroupBy - adds default groupings for the view

The schema is as follows:

```yaml
Menu:
  - Id: "[groupid]"
    Title: "[display name]"
    Items:
      - Id: [ name of existing view ]
      - Title: "[display name]"
      - Icon: "[icon]"
      - Color: "[color]"
      - To: "[override Id for page when using filters]"
      - Filter: "[filter from Filter list to use when showing table]"
      - GroupBy: ["Runnable", "[Object fields to group by]"]
  - Id: system
    Title: System
    Items:
      - Id: overview
        Title: Overview
        Paywall: overview
      - Id: machines
        Title: Machines
      - Id: plugins
        Title: Plugins
      - Id: system
        Title: "Info & Preferences"
```

### Filter

Type: Object

Filters are defined using the same schema as the Application store
`filter` JSON.

The schema is as follows:

```yaml
Filter:
  object-lower-case:
    "[filter name]":
      "[field name]":
        use: true
        value: true
        key: [ field value ]
  machines:
    "Debug On":
      "*Param":
        use: true
        value: true
        key: rs-debug-enable
  templates:
    "[default]":
      "ReadOnly":
        use: true
        value: false
```

### Columns

Type: Object of String Arrays

Currently only the machines view supports custom table columns. The user with a corresponding role will be shown the
listed columns in the same order. Params can be supplied into the table by using `Param:<paramname>`, any
prefixes (`inventory/` in `inventory/CPUs`) will be stripped out.

The schema is as follows:

```yaml
Columns:
  machines:
    - runnable
    - icon
    - locked
    - name'
    - address
    - profiles
    - workflow
    - stage'
    - task
    - bootenv
    - endpoint
    - Param:rs-debug-enable
    - Param:<param name>
```

Custom columns can be transformed to be rendered differently with a third colon. Commas, Colons, and Equals characters
intended to be used as values must be escaped with a backslash.

**Example Columns**:

- Example Default Override: `Custom:Name:name=CustomName,sort=Name`
- Debug with checkmarks: `Param:rs-debug-enable:type=icon,iconName=bug,iconTitle=Debug Enabled,iconMap=true@check`

**Developing a Custom Column**:

Reference the available options list below for comprehensive list of things.

The format of the column is:

- `Custom:FieldPath:options`
- `Param:ParamName:options`

The `FieldPath` is similar to a JQ query for the API's machine object. Here are a few examples of field paths:

- `Name` - Machine Name
- `Meta.color` - color field of meta
- `Params.foo` - value of `foo` param
- `Params.bar.baz` - value of the `baz` field in the `bar` object param

Options are separated by commas (`,`) and assign values with equals (`=`). Here's some examples:

* `key=val,key2=val2` - A demonstration of the encoding (not valid options)
* `name=Foo,path=Params.foo` - Render a "Foo" column that displays the value of the "foo" param
* `name=MFG,logoType=manufacturer,path=Params.manufacturer` - Render hardware manufacturer icon based on
  a `manufacturer` param
* `name=OS,logoType=os,path=Params.os` - Render an operating system icon based on an `os` param

**Available Options**:

* name:
    * description: Column Title
    * default: `paramName` suffix of `paramPrefix/paramName`

* path:
    * description: JSON property path
    * default: `Param.paramName`

* sort:
    * description: Object property to table sort by. must be in api indexes
    * default: none

* type:
    * description: Render Type of Column
    * default: none
    * accepted:
        * icon (references iconName, iconColor, iconTitle, iconMap, isIcon)
        * logo (references logoType)
        * link (references link)
        * route (references route)

* iconName:
    * description: Icon of column icon
    * default: `question`
    * accepted: font awesome icon names

* iconColor:
    * description: Color of column icon
    * default: `black`
    * accepted: semantic ui color names

* iconTitle:
    * description: Hover text of column icon
    * default: none
    * accepted: any string

* iconMap:
    * description: references row value at `path` to get icon. * is default icon.
    * default: none
    * example: `iconMap=*@question;key@value`

* isIcon:
    * description: If the row value is an icon, it uses that value instead
    * default: false
    * example: `isIcon=true`

* logoType:
    * description: rows use logo icons
    * default: `os`
    * accepted:
        * os (operating system icons)
        * manufacturer (manufacturer icons)

* link:
    * description: format for rendering an external link
    * default: `https://{paramName}`
    * example: `link=ftp://{Address}/folder/{Name}`

* route:
    * description: format for rendering an internal link
    * default: none
    * example: `route=machines/{Name}` becomes `/e/127.0.0.1/machines/machine-name`

* separator:
    * description: join string array params or display strings as multiple lines
    * default: none
    * example: `"separator=\\n"` (escaped newline)
        * takes a string with new lines and renders the column with separate lines
        * when `\n`, takes a string array and makes each item a new line
        * otherwise, takes a string array and joins it by the separator

**Available Icons**:

Param value must match one of the following case insensitive regexes:

OS:

- `linux`
- `sledgehammer`
- `discovery`
- `drp`
- `rebar`
- `redhat`
- `rhel`
- `ubuntu`
- `centos`
- `arch`
- `windows`
- `rancher`
- `rancheros`
- `coreos`
- `suse`
- `sles`
- `gentoo`
- `bsd`
- `freebsd`
- `fedora`
- `esxi`

Manufacturer:

- `drp`
- `rebar`
- `virtualbox`
- `vmware`
- `qemu`
- `dell`
- `hpe?`
- `lenovo`
- `supermicro`
- `fujitsu`
- `oracle`
- `cisco`
- `ibm`
- `opencompute`
- `rpi`
- `hitachi`
- `nec`
- `intel`
- `solid ?fire`
- `net ?app`

### Classifiers

Type: List of strings

When creating a new classifier, operators are promoted with an interstitial screen like Params.

Required Components

- Test - the test from the classifier to be run
- Title - friendly title to be used in the dropdown list

Optional Components

- Placeholder - hint for the user about the field
- Icon - icon for the test
- NoAppend - does this test require users to add extra fields
- Regex - regex to be applied to items appended to the test
- Params - seed params and values to be offered to the user (key: value) like other Params
- Continue - sets the continue value; however, if included will hide from user so that it is not user settable

Example:

```yaml
Classifiers:
  - Test: "always"
    Title: "Friendly Description"
    Icon: "rook"
    NoAppend: false
    Placeholder: "hint for the user"
    Regex: "(.*)"
    Params:
      kexec-ok: true
  - Test: "never"
    Title: "Unriendly Description"
    Icon: "castle"
    NoAppend: true
    Regex: "guess this"
    Params:
      adhoc: "never"
```

### WorkflowsRestriction

Type: List of Strings

Provides a way to restrict the Workflows that are available to users to a specific list. Workflows are still visible but
disabled in all pull downlists.

```yaml
WorkflowsRestriction:
  - discovery
```

### StagesRestriction

Type: List of Strings

Provides a way to restrict the Stages that are available to users to a specific list. Stages are still visible but
disabled in all pull downlists.

```yaml
StageRestriction:
  - discover
```

### TasksRestriction

Type: List of Strings

Provides a way to restrict the Tasks that are available to users to a specific list. Tasks are still visible but
disabled in all pull downlists.

```yaml
TasksRestriction:
  - discovery
```

### ProfilesRestriction

Type: List of Strings

Provides a way to restrict the Profiles that are available to users to a specific list. Profiles are still visible but
disabled in all pull downlists.

```yaml
ProfilesRestriction:
  - global
```

### ParamsRestriction

Type: List of Strings

Provides a way to restrict the Params that are available to users to a specific list. Params are still visible but
disabled in all pull downlists.

```yaml
ParamsRestriction:
  - kexec-ok
```

### ParamsRestriction

Type: List of Strings

Provides a way to restrict the params that are available to users to a specific list. Params are still visible but
disabled in all pull downlists.

```yaml
ParamsRestriction:
  - kexec-ok
```

### HideEditObjects

Type: List of Strings

Hides Add / Edit / Clone / Delete buttons for provided objects.

```yaml
HideEditObjects:
  - machines
  - profiles
```

## UX Settings

UX Settings are used to store user, role, and configurations for the UX within DRP. Setting set on users supersede role
settings, and role settings supersede global settings. A user with two or more roles with different role settings for
the same option will receive configuration from only one of the roles.

### UX Option

The ux_options object functions as a key and type definition for a UX configuration. UX Options define a configuration's
ID, type, and scope.

Fields:

- Id (required): An identification string similar to a FQDN in format.
- Type: "number" or "string" or "boolean" - The kind of value this setting expects.
- Default: Default value of this option.
- Global: Whether this option can be configured at global level.
- Role: Whether this option can be configured at role level.
- User: Whether this option can be configured at user level.
- Hidden: Whether this option is configurable within the UX.

### UX Setting

The ux_settings functions as the typed value to the ux_options key in a UX configuration.

Fields (All required):

- Id: The unique identifier for this option. Consists of the Target and Option fields separated by a colon.
- Option: The Id of the option this setting is referencing.
- Target: `global` or `user++rocketskates`/`user++<username>` or `role++superuser`/`role++<rolename>` - The target of
  this setting's configuration.
- Value: The value of this configuration in JSON. Must adhere to the target option's type definition.

---
title: Configuration
tags:
  - portal
  - reference
---

This document is an instructional guide for configuration of the RackN
Portal. To edit UX configurations on DRP versions older than v4.11, the
ux-views plugin must be installed.

## UX Views

The UX Views feature enables the UX to show custom side navigation trees
based on a user's role. This is useful for limiting access to certain
users.

### Role Selection

By default, the first non-readonly view applicable to the current user
will be selected.

If there is more than one UX View that can be applied to the user, a
dropdown will be visible under on the top of the sidenav.

![UX Views Dropdown](images/uxviewdropdown.png)

### Table

The UX Views page can be navigated to by the "UX Views" link in the
Endpoint Admin section of the sidenav.

![UX Views default content](images/uxviews.png)

Each row in the table denotes a different UX View. The columns are as
follows:

* Read Only - Lock icon if true
* Icon - UX View Icon metadata
* Id - The name of this UX View, along with a red triangle if the no user has a role designated to this view.
* Description
* Roles - When a user has this role, they will be shown this UX View
* Landing Page - Default page shown to a user after sign in.

### Side Nav Editing

A UX Views object has `Menu` field which allows for the customization of the side nav items. The default menu will be used when the value of this field is `[]`.

The `Id` of all default menu items is the same as their respective URL routes. (eg. `"Id": "machines"` routes to `#/e/EP_ID/machines`). A list of all available values can be found by clicking on the blue question mark icon in the editor and scrolling to the bottom.

The following JSON describes the features and functionality of the `Menu` field:

```json
[
  {
    // Id must be unique among sections and items
    "Id": "menu_section_id",
    "Title": "Section Title",
    "Description": "Hover tooltip",
    "Items": [
      {
        // "Id" Must be an id from the table below or "To" must be provided
        // If "Id" does not match an existing /e/endpoint_url/<Id>
        // "To" or "Overwrite" must be used
        "Id": "menu_item_id",
        "Title": "Menu Item Title",
        "Description": "Hover tooltip",

        // Optional, will default to the icon for the menu item's object
        "Icon": "fontawesome icon",

        // Optional, one of these colors:
        // red | orange | yellow | olive | green | teal | blue
        // violet | purple | pink | brown | grey | black
        "Color": "red",

        // Optional, passed into the url after '?filter='
        "Filter": "Runnable=Eq(false)",

        // Optional, route passed into '/e/endpoint_url/{{To OR Id}}'
        "To": "machines",

        // Optional, route passed into '/e/endpoint_url/{{Id}}'
        // and renders a Machines table.
        // The "Filter" now is embedded in the table instead of '?filter='
        "Overwrite": "machines"
      }
    ]
  }
]
```

## UX Config

The UX Config feature allows tweaking of some UX functionality
independent from DRP as well as the enabling of some security features.

The UX Config page can be navigated to by the "UX Config" link in the
Endpoint Admin section of the sidenav.

This view is broken down into sections and options. By default,
configurations of other users are not displayed unless either the eye
icon next to "Collapse All" or the "+N more hidden rows" message at
the bottom of an expanded option are clicked.

Each option row is broken down into the following sections:

* Type Icon - "A" for text, "\#" for numeric, and a switch icon for toggle or boolean options.
* Option Name
* Option Description (and unit for numeric options)
* Scope counters
    * Blue globe - number of global settings (at most 1)
    * Purple person circle - number of role settings
    * Green person - number of user settings

![UX Config List](images/uxconfiglist.png)

Each option entry row can be clicked to be expanded or collapsed.

![UX Config List](images/uxconfigedit.png)

Settings can be modified by selecting scope, inputting a value, and
pressing the save button.

Settings can be removed by clicking the red trash can icon on the right
side of the respective rows.

### Inactivity Timer

In the Security -\> Inactivity section of the UX Config page are
configurations for the inactivity timer. This timer will automatically
sign users out after the configured amount of time if there is no mouse
or keyboard activity.

A page refresh must be made after enabling the feature for it to take
effect.

![Inactivity Timer](images/uxconfiginactivity.png)

## Filters

Most table views support table filtering and searching.

By default, typing in the search field will prompt for searching. Pressing enter after entering a search term will filter the table by a fuzzy search on its key.

![Screenshot of the search field populated with the term "centos"](images/filter_search.png)

After focusing the search field, it can also be used to build filters. This can be done by keyboard via arrow keys and tab or by clicking.

Objects that support params support filtering by params by selecting them from this list.

![Screenshot of the search field as a dropdown with one of its indices hovered](images/filter_dropdown.png)

Boolean filter indices will provide "true" and "false" options at your convenience.

Pressing CTRL+Enter will force a prompt to become a filter even if the index does not exist on that object.

![Screenshot of the search field popualted with an index, comparison, and value ](images/filter_type.png)

After enter has been pressed and the table is filtered, the current filters are available to the right of the search field. Clicking or hovering and selecting "edit" on these filters will re-populate the search field.

![Example search filter](images/filter_edit.png)

Toggling the gear icon button will expand the filter options menu:

* Save/Load Button - Allows for saving and loading of filters for this table.
* Aggregate checkbox - Enables aggregate param expansion and filtering on objects with params.
* DRP CLI Preview - Clicking this will copy the DRP CLI representation of table filter to your clipboard.

![Filter Editor](images/filter_expand.png)


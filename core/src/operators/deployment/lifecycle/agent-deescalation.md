---
title: Agent Deescalation
tags:
- operator
- howto
- tutorial
---

# Agent Deescalation {#rs_ops_life_agent_deescalation}

This document details how to change the user of a running agent for linux system.

Using the runbook post flexiflow injection point, the task `change-drp-agent-user`
can be used to change the running user of the agent.

## Requirements

* `drp-agent/username` parameter to be set on the machine
* `drp-agent/username` user to exist on the machine

!!! warning

    Many tasks assume `root` access and may fail if run by a deesclated agent.


!!! warning

    Once transitioned away from `root`, the system may not be able to transition back without
    reinstallation or manual intervention.

## Example Usage

This profile would be an example to add to a machine to deescalate during the last steps of
provisioning.

```yaml
---
Name: deecalate-agent
Params:
  drp-agent/username: fred
  universal/runbook-post-flexiflow:
    - change-drp-agent-user
```

Adding this profile to machine or pipeline will cause the agent to run as `fred` as the last
step in the provisining process.


---
title: Lifecycle
tags:
  - operator
  - reference
---

As an operator, you will need to be able to manage the lifecycle of the DRP endpoint.  This section will focus
on managing a single DRP endpoint.  Most of this can be automated with the Manager functionality.

* Backup / Recovery - Describes how to take a backup and restore a DRP endpoint.
* Upgrade - Describes how to upgrade a DRP endpoint.
* Machine Migration - Describes how to move machines from one endpoint to another.


---
title: Deployment Operations
tags:
  - operator
  - reference
---

As an operator, you will need to be able to deploy a DRP endpoint.  This section addresses the following topics:

* CLI - Obtain and Use the DRP CLI
* Configuration - Describes configuring the general components of the system.  (e.g. DHCP, DNS, ...)
* HA - Describes how to configure and operate a Highly-Available cluster of DRP endpoints
* Lifecycle - Describes lifecycle of a DRP endpoint.  Back-up and recovery, upgrade, and migrating machines between DRP endpoints.
* Manager - Describes how to configure and operate a federated set of DRP endpoints managed by a Manager.
* License Operations - Describes how licensing and operations around enforcement.
* Security - Describes security concerns and best practices
* Content Packs and Plugins - Describes all the options and configuration elements of the content packs and plugins.  Their functions are basic configurations are described in other places, but the full details are here.


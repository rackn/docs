---
title: IPv6 Provisioning 
tags:
- operator
- howto
---

# IPv6 Provisioning {#rs_ops_ipv6}

Digital Rebar supports IPv6 Operation and Provisioning in a variety of ways; however, IPv6 provisioning is not a single
operation. This page explores the different components of the IPv6 provisioning and which are supported by Digital
Rebar.

## Digital Rebar Endpoint IPv6

The Digital Rebar Endpoint (aka server) is IPv6 aware and is dual stacked on all its listening ports. That means it can
talk IPv4 and IPv6 on all the protocols except DHCP (which is protocol specific). All Workflows are designed dual stack
assuming you can get a system to boot into sledgehammer or an installed O/S.

Notes:

1. This is accurate as of the v4.8 release of Digital Rebar and may change as planned features such DHCPv6 are added to
   the product (see below)
2. Operating systems have different levels of IPv6 support independent of Digital Rebar.

## Supporting Components

There are many interlocking components that must be managed in the correct sequence for provisioning to complete. A
major part of using IPv6 only provisioning is choosing components that are able to handle IPv6 traffic. For example,
older provisioning components such as embedded TFTP likely does not support IPv6.

### DHCP v6

If you have a IPv6 PXE/IPXE or can use the media attach approach, then you will still need a mechanism to assign IP
addresses and options such as `Next Server`. Those functions are handled by a DHCPv6 service.

This is a planned feature for Digital Rebar, but has not been implemented.
DHCPv4 and DHCPv6 are different protocols and
the new DHCPv6 service exposed as a distinct service from the existing DHCPv4 APIs.

Until it is a supported feature, operators are responsible to maintaining their own IPv6 DHCP or IPAM. Once an IPv6
address is assigned, then Digital Rebar can continue on using IPv6 to provision and control the system.

### PXE and TFTP

PXE is the primary gap for IPv6 support. Until after 2020, few embedded PXE BIOSes were able to support IPv6 at all.
That means that they still required IPv4 for TFTP, HTTP or HTTPS. For operators looking for IPv6, we recommend working
with your vendor on IPv6 support and using a bootloader that can bypass TFTP. This could be as simple as upgrading the
vendor's PXE BIOS. This is the most reliable approach and using updated BIOS has additional operational benefits.

While Digital Rebar is designed to minimize our dependences on the PXE loader by using a multi-stage boot sequence,
first embedded stage it is still required in most scenarios. See the Media Attach section for alternatives.

### Alternative Bootloaders

A fallback option to upgrading the PXE BIOS is to use their vendor's BMC media attach feature to deliver an updated
bootloader; however, not all vendors support media attach.

This [](#rs_virtual_media_iso_booting) is supported by Digital Rebar and has several potential
advantages:

1. Dynamic updates makes it easier to update the bootloader.
2. Per machine customization makes it possible to adjust the bootloader for different architectures.
3. Injectable configuration makes it possible to eliminate DHCP as a requirement.

RackN does _not_ recommend doing a full O/S install via media attach because the involved protocols and attachment
systems can be less reliable under load or slow networks. Instead, we recommend keeping only the first bootstrap stage
from Sledgehammer in the attached media that can then follow our normal provisioning processes.

## Operational Notes

### Blocking IPv4 Traffic

Operators intent on IPv6 only operation should consider preventing IPv4 routing on their networks; however, this will
expose IPv4 assumptions throughout the operating stack. In these cases, a careful dev, test, production staging plan is
critical to ensure issues are caught pre-production.

### Switching Modes

Since Digital Rebar is dual stack and many network related Tasks were designed primarily for IPv4, they may default to
IPv4 if it is available. Keeping the target system stays on DHCPv6 minimizes these issues; however, this type of
inconsistency should be filed as issue to be corrected.

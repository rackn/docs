---
title: Certificate Operations
tags:
- operator
- howto
---

# Certificate Operations {#rs_cert_ops}

This section will describe how to manage the HTTPS certificates that dr-provision uses for the API
port (8092 by default) and the HTTPS Static FileServer port (8090 by default).

!!! note

    It's important to know if the CA signed certificate you intend to install have been signed
    based on a host and port pair.  If that is the case, you will need separate TLS Certificates
    for the API Port service (typically port 8092) and the Static FileServer service (typically
    port 8090).  Otherwise, a host cert without port specified should work functionally for both
    services.


## API Certificate Management {#rs_cert_ops_api}

In dr-provision versions earlier than 4.6.0, API certificates can only be managed via the `--tls-cert` and `--tls-key`
startup options.

In dr-provision versions 4.6.0 and later, the `--tls-cert` and `--tls-key` startup options are only used for loading
certificates at initial startup time. After that, you must use the `drpcli system certs` or `drpcli files certs`
commands to manage the TLS certificate that the API or Static FileServer uses.

### Get API Certificates

You can fetch the current TLS certificate and private key that the API uses with:

```shell
    drpcli system certs get server-api.crt server-api.key
```

This will retrieve the TLS certificate and private key that the API is using, and saves them in x.509 DER encoded form.

### Set API Certificates

You can update the TLS certificate and private key that the API will use for new connections with:

```shell
    drpcli system certs set server-api.crt server-api.key
```

This will upload the X.509 DER encoded certificate and private key to dr-provision, which (assuming that they are valid)
will be used for any new connections. Additionally, the new certs will be applied cluster-wide if running in an HA
cluster.


## Static FileServer Certificate Management (4.7.0 and higher) {#rs_cert_ops_static}

dr-provision 4.7.0 and higher have a static HTTPS server that will be used as an alternative to serving static files
over HTTP whenever feasible. It uses the `--static-tls-cert` and `--static-tls-key` startup options to load the static
HTTPS certs initially. Afterwards, the `drpcli files certs` commands will manage the static HTTPS certificates in the
same way the `drpcli system certs` commands manage the API certs.


### Get Static FileServer Certificates

You can fetch the current TLS certificate and private key that the Static FileServer uses with:

```shell
    drpcli files certs get server-static.crt server-static.key
```

This will retrieve the TLS certificate and private key that the API is using, and saves them in x.509 DER encoded form.


### Set Static FileServer Certificates

You can update the TLS certificate and private key that the Static FileSystem will use for new connections with:

```shell
    drpcli files certs set server-static.crt server-static.key
```

This will upload the X.509 DER encoded certificate and private key to dr-provision, which (assuming that they are valid)
will be used for any new connections. Additionally, the new certs will be applied cluster-wide if running in an HA
cluster.



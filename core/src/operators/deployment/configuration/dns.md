---
title: DNS Operations 
tags:
- operator
- howto
- dns
---

dr-provision provides a simple DNS query server. Most records are supported and can be returned by the server.

## Endpoint Startup Configuration

Whether the DNS server is enabled and what ports it will listen on are controlled by the following startup options:

1. `--disable-dns` to disable basic DNS functionality.
2. `--dns-port` to change the DNS port from the default of 53 to something else. This is both TCP and UDP.

If none of these options are present, then we default to DNS being enabled on the standard port. The DNS server listens
on all interfaces, and that behaviour cannot be modified at the present time.

!!! warning

    Some services will conflict with DNS.  Common sevices that must be disabled when running DRP DNS server

    * dnsmasq
    * bind

## Runtime Configuration

By default, (without any Zones or Reservations defined) the DNS server will respond with negative matches.

### Zones

A [Zone](#rs_arch_models_networking_zone) defines a set of information that can be returned to DNS queries.

This includes dynamic records built from templatized rendering strings from Machine objects.

### Reservations

A [Reservation](#rs_arch_models_networking_reservation) defines a one-to-one mapping between a specific network
interface to be configured and the IP addressing information that should be applied to that interface.

If the reservation and its optional subnet contain an address, hostname, and a domainname, an A and PTR record will be
created with in a zone that matches the Origin for the system.

## Example Configuration

The following Zone will act an forwarding name server for all queries that aren't matched by other zones. This is a
common final zone in the configuration.

``` yaml
Name: root
Origin: "."
Priority: 10000
Forwarders:
 - 8.8.8.8
```

The following Zone will service the `example.com` domain (as declared in the Origin field). It will also be authoratives
and can be used in delegation.

``` yaml
Continue: false
Name: example.com
Origin: example.com.
Priority: 0
Records:
- Name: .
  TTL: 86400
  Type: SOA
  Value:
  - dns1.example.com.
  - hostmaster.example.com.
  - "762016905"
  - "21600"
  - "3600"
  - "604800"
  - "86400"
- Name: .
  TTL: 86400
  Type: NS
  Value:
  - dns1.example.com.
- Name: .
  TTL: 86400
  Type: NS
  Value:
  - dns2.example.com.
- Name: .
  TTL: 86400
  Type: MX
  Value:
  - "10"
  - mail.example.com.
- Name: .
  TTL: 86400
  Type: MX
  Value:
  - "20"
  - mail2.example.com.
- Name: dns1
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.1
- Name: dns2
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.2
- Name: server1
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.5
- Name: server2
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.6
- Name: ftp
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.3
- Name: ftp
  TTL: 86400
  Type: A
  Value:
  - 10.0.1.4
- Name: mail
  TTL: 86400
  Type: CNAME
  Value:
  - server1.example.com.
- Name: mail2
  TTL: 86400
  Type: CNAME
  Value:
  - server2.example.com.
- Name: www
  TTL: 86400
  Type: CNAME
  Value:
  - server1.example.com.
TTL: 3600
```

## Import Configuration

The `drpcli` command can also be used to import RFC-compliant zone files. For example, existing BIND or other server
files can be imported as zones into the DRP server. This is a create operation. If you wish to make changes to the zone
through the file, you will need to destroy the DRP zone and reimport the file.

The `Name` of the Zone will be based upon the `$Origin` part of the file.

``` bash
drpcli zones import my-zone.txt
```

Example file

``` text
$ORIGIN example.com.
$TTL 86400
@       IN      SOA     dns1.example.com.       hostmaster.example.com. (
                        2001062501 ; serial
                        21600      ; refresh after 6 hours
                        3600       ; retry after 1 hour
                        604800     ; expire after 1 week
                        86400 )    ; minimum TTL of 1 day


        IN      NS      dns1.example.com.
        IN      NS      dns2.example.com.


        IN      MX      10      mail.example.com.
        IN      MX      20      mail2.example.com.


dns1    IN      A       10.0.1.1
dns2    IN      A       10.0.1.2


server1 IN      A       10.0.1.5
server2 IN      A       10.0.1.6


ftp     IN      A       10.0.1.3
        IN      A       10.0.1.4

mail    IN      CNAME   server1
mail2   IN      CNAME   server2


www     IN      CNAME   server1
```

## CLI Operations

The `drpcli` can be used to create, update, and delete zones. Additionally, there are helpers to add and remove records.

### CRUD Operations

Using the `yaml` files above, a zone can be created by:

``` bash
drpcli zones create root.yaml
```

or updated by:

``` bash
drpcli zones update root root.yaml
```

or destroyed by:

``` bash
drpcli zones destroy root
```

or viewed by:

``` bash
drpcli zones show root --format=yaml
```

or view the live records (including dynamic records and reservation records) by:

``` bash
drpcli zones view root --format=yaml
```

The output looks like:

``` yaml
Data:
  .:
    MX:
    - Data:
      - "10"
      - mail.example.com.
      TTL: 86400
      Uuid: zones:example.com
    - Data:
      - "20"
      - mail2.example.com.
      TTL: 86400
      Uuid: zones:example.com
    NS:
    - Data:
      - dns1.example.com.
      TTL: 86400
      Uuid: zones:example.com
    - Data:
      - dns2.example.com.
      TTL: 86400
      Uuid: zones:example.com
    SOA:
    - Data:
      - dns1.example.com.
      - hostmaster.example.com.
      - "762259547"
      - "21600"
      - "3600"
      - "604800"
      - "86400"
      TTL: 86400
      Uuid: zones:example.com
  1.1.1.1:
    PTR:
    - Data:
      - New Record.example.com.
      TTL: 3600
      Uuid: zones:example.com
  3.3.3.3:
    PTR:
    - Data:
      - fred.example.com.
      TTL: 300
      Uuid: machines:34553cb1-7425-465a-a3a9-fb67458506bf
  4.4.4.4:
    PTR:
    - Data:
      - jane.example.com.
      TTL: 3600
      Uuid: reservations:04040404
  10.0.1.1:
    PTR:
    - Data:
      - dns1.example.com.
      TTL: 86400
      Uuid: zones:example.com
  10.0.1.2:
    PTR:
    - Data:
      - dns2.example.com.
      TTL: 86400
      Uuid: zones:example.com
  10.0.1.3:
    PTR:
    - Data:
      - ftp.example.com.
      TTL: 86400
      Uuid: zones:example.com
  10.0.1.4:
    PTR:
    - Data:
      - ftp.example.com.
      TTL: 86400
      Uuid: zones:example.com
  10.0.1.5:
    PTR:
    - Data:
      - server1.example.com.
      TTL: 86400
      Uuid: zones:example.com
  10.0.1.6:
    PTR:
    - Data:
      - server2.example.com.
      TTL: 86400
      Uuid: zones:example.com
  New Record:
    A:
    - Data:
      - 1.1.1.1
      TTL: 3600
      Uuid: zones:example.com
  dns1:
    A:
    - Data:
      - 10.0.1.1
      TTL: 86400
      Uuid: zones:example.com
  dns2:
    A:
    - Data:
      - 10.0.1.2
      TTL: 86400
      Uuid: zones:example.com
  fred:
    A:
    - Data:
      - 3.3.3.3
      TTL: 300
      Uuid: machines:34553cb1-7425-465a-a3a9-fb67458506bf
  ftp:
    A:
    - Data:
      - 10.0.1.3
      TTL: 86400
      Uuid: zones:example.com
    - Data:
      - 10.0.1.4
      TTL: 86400
      Uuid: zones:example.com
  jane:
    A:
    - Data:
      - 4.4.4.4
      TTL: 3600
      Uuid: reservations:04040404
  mail:
    CNAME:
    - Data:
      - server1.example.com.
      TTL: 86400
      Uuid: zones:example.com
  mail2:
    CNAME:
    - Data:
      - server2.example.com.
      TTL: 86400
      Uuid: zones:example.com
  server1:
    A:
    - Data:
      - 10.0.1.5
      TTL: 86400
      Uuid: zones:example.com
  server2:
    A:
    - Data:
      - 10.0.1.6
      TTL: 86400
      Uuid: zones:example.com
  www:
    CNAME:
    - Data:
      - server1.example.com.
      TTL: 86400
      Uuid: zones:example.com
```

The returned object a `map` with a single key, `Data`. The object in the `Data` key is another `map` whose keys are the
values that are to be looked up. The `.` key is the same as the `Origin` of the zone. Special IP address entries are
used as short-hand for the PTR zones for reverse lookups.

Under each lookup zey is a map of record types (e.g. A or AAAA). The object in that section is a list of values for a
single record. DNS can return multiple values for a single lookup. This list provides all return values for that name
and type pair. The `Data` field is a list of strings that will be converted into the record type when being sent to the
requester. Additionally, a `TTL` field defines the actual type for that specific value. The `Uuid` field defines where
that record comes from. The value is read as `<type>:<id>`. Static records have the value, `zones:<zone name>`. Machine
records have the value, `machines:<uuid>` and the Reservation records have the value, `reservations:<ip address in hex>`

### Records

Once a zone is added, records can be added by:

``` bash
# Adds an A/PTR record for greg.example.com to 3.3.3.3
drpcli zones records add example.com A greg 3.3.3.3
```

Additionally, the `ttl` for the record can be provided by adding the `--ttl` flag.

``` bash
# Adds an A/PTR record for greg.example.com to 3.3.3.3
drpcli zones records add example.com A greg 3.3.3.3 --ttl 3600
```

Or adding a dynamic record for machines that have `Name` fields that fall in the example.com domain.

``` bash
# Adds an A/PTR record for all the Machines that match *.example.com in the Name field
drpcli zones records add example.com A "eval:{{.Machine.Name}}" "{{.Machine.Address}}"
```

This record is special and will generate a live entry for each machine that has a name that ends in `example.com` using
the Machine's `Address` field.

Records can be removed by:

``` bash
# Adds an A/PTR record for greg.example.com to 3.3.3.3
drpcli zones records remove example.com A greg 3.3.3.3
```

You can also get the formats and fields of the allowed record types by:

``` bash
drpcli zones records types
```

This will produce something like this:

``` yaml
A:
  Key: 1
  Max: 1
  Min: 1
  Records:
  - Name: A
    Subtype: a
    Type: net.IP
AAAA:
  Key: 28
  Max: 1
  Min: 1
  Records:
  - Name: AAAA
    Subtype: aaaa
    Type: net.IP
AFSDB:
  Key: 18
  Max: 2
  Min: 2
  Records:
  - Name: Subtype
    Subtype: ""
    Type: uint16
  - Name: Hostname
    Subtype: domain-name
    Type: string
...
```

The map is string record type (e.g. `A`) and the object defines the format.

The object has the fields:

* Key - the 16-bit integer type of the record
* Max - the maximum number of fields for this record (-1 means unlimited)
* Min - the minimum number of fields for this record (-1 means no minimum)
* Records - A list of the required fields for this record.
    * Name - the name of the field
    * Type - the type of the field
    * Subtype - helper to clarify additional properties of the type.  (e.g. domain-name for a string type)

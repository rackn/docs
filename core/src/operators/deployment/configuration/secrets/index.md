---
title: Secure Parameter Store
tags:
  - operator
  - reference
---

# Secure Parameter Store {#rs_config_secrets}

As an operator, you will need to be able to configure secret storage for the DRP Endpoint.

By default, a DRP endpoint will locally encrypt secrets at rest and in-memory until needed for a specific operation.

There are additional stores that can be used to handle secure parameters.  All methods allow for parameterized
retrieval of secrets.

* [](#rs_content_vault) - Use Hashicorp Vault as secret store. [](#rs_config_vault)
* [](#rs_content_cmdvault) - Pulls secrets from command line scripts run on the DRP endpoint. [](#rs_config_cmdvault)
* [](#rs_content_azkeyvault) - Pulls secrets and certificates from Azure Cloud. [](#rs_config_azkeyvault)
* [](#rs_content_awssecman) - Pulls secrets and certificates from AWS. [](#rs_config_awssecman)

[](#rs_arch_dep_endpoint_secure_params) has more details on the general architecture.

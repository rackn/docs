---
title: Authentication Operations
tags:
  - operator
  - reference
---

As an operator, you will need to be able to configure access to the DRP Endpoint.

There are multiple ways to allow access to the system.  Each sub-section goes into more detail.

## Embedded Authentication

* user - username/password or token-based authenication

## External Authentication

* certificate - External certificate with DRP authentication information
* ad-auth - Active Directory Integeration
* saml - Identity Provider-based authentication

## Roles

* roles - Define what users are allowed to do

## Tenants

* tenants - Define what users can see


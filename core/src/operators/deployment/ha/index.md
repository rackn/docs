---
title: High Availability
tags:
  - operator
  - reference
---

As an operator, you will need to be able to deploy a highly available DRP endpoint cluster.  This section addresses the following topics:

* Configure - Describes how to configure high availabity
* Status - Describes how to see and understand the status of the system.

TODO: Reference arch sections about zones and HA.


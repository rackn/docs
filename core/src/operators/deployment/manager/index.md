---
title: Manager Operations
tags:
  - operator
  - reference
---

As an operator, you will want to use a manager or tiers of managers to limit visibility and automate lifecycle for
multiple DRP Endpoints at a time.

The following subsections describe:

* Configure - Enable manager functionality and add/remove Endpoints to the manager
* Catalog - Configure and manage catalogs used by the manager to update managed DRP endpoints.
* Version Sets - Define configuring of an endpoint and apply it
* Custom Content - How to manage and share custom content between manager and endpoints.
* TODO: Backup / Recovery - Manager operational components to backup and restore endpoints


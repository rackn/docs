---
title: RackN Licensing Overview
tags:
    - reference
    - operator
---

# RackN Licensing {#rs_license}

This document outlines the RackN limited use and commercial licensing information and initial setup steps necessary to
access licensed entitlements. If you have any questions or concerns, please feel free to contact us on Slack, or email
us at <support@rackn.com>.

If you have a license issued before May 1, 2021, you can identify these licenses because they will end in a
number sequence greater than 4600000. Licenses issued prior to v4.6 will continue to function and can be used
to create v4.6+ licenses; however, new entitlements cannot be added to earlier licenses.  Contact RackN
to generate a new license.

Training Video: <https://www.youtube-nocookie.com/embed/Rpy1Tw0-pUE>

## License Types

### Start-up / Embedded licensing

Embedded licensing of RackN Digital Rebar Platform (DRP) is provided for solely new endpoints to run until you install
get an official use license.

The embedded license is built into the platform and has restricted entitlements:

- 10 machines (10)
- no contexts or pools
- restricted use of plugins
- hard coded expiration date based on binary build date

### Limited Use licensing

Limited use licensing of DRP is provided for individual users, trial and non-commercial teams.

The trial/community license is generated via the processes documented below. Self-service licenses generally start at:

- 20 machines
- 5 contexts
- 5 pools
- 30-days with 1 self-service renewal
- allow access to all publicly available plugins in the RackN catalog.

Enterprise features such as airgap, HA and secure boot are not included in the trial license. Contact the RackN solution
team if you would like to expand your entitlements.

### Commercial Use licensing

Commercial use licensing of DRP is provided to named organizations. License entitlements are enabled on several
dimensions including endpoint ID, unit counts, contexts, pools, plugins and advanced features like HA and secure boot.
The RackN solution team will need to set up an organization with the correct license entitlements for this type of
license.

## License Enforcement Mechanism

The RackN License file restricts operations for a DRP endpoint based on different entitlements. The file contains all
the information needed to validate the service so no external access is required.

### RackN License File

Entitlement files are DRP content packs named `rackn-license`. Installing the content creates a profile with two
variables:

1. `rackn/license` is the signed and encrypted license that is used by the server.
1. `rackn/license-object` is the human-readable version of that license file.

The DRP service looks for the `rackn/license` parameter to verify entitlements.

### Entitlements

The entitlements are verified on several dimensions including:

- Endpoint ID
- Date
- Plugins in use
- Object counts
    - Machines
    - Contexts
    - Pools
    - Subnets
- High availability (HA) enabled
- Secure boot enabled
- Air-gap enabled

## Installing RackN License Files

The RackN License file is a DRP content pack that contains the license key. It can be managed in the system using the
contents API and CLI like any other content pack.

### Verify and Update

The RackN UX integrates with our license generating API and can be used to authenticate and download updated entitlement
files.

### Check a License from the UX

!!! note 

    You must log in to the DRP Endpoint first.

From the "License Manager" page, check the "License Management" panel. The panel performs three levels of checks on your
license:

1. Verified: a valid entitlement file is installed.
1. Registered: the current endpoint is registered in that license.
1. Up-to-date: the version of the file matches the most current version known to RackN.

If the license is not up-to-date then click the "Check and Update License" button to retrieve an updated license from
the RackN entitlement service.

### Generate a New License

!!! note 

    You must log in to the DRP Endpoint first.

The first time that you login to a DRP endpoint from the UX, you will be prompted to either request new license file or
upload an existing license. If you complete the request information then the service creates a short
term `rackn-license` content file and then uploads it to your endpoint.

You will only need to perform this step once because the license file is used to validate your access rather than a
RackN specific login or password. For this reason, it is important to download and store the license file for future
use. Even if a newer license is issued, previous licenses can still be used to validate your identity to the RackN
license service.

!!! note 

    Licenses created via the self-enrollment process have limited time spans. You will need to contact RackN via
    the UX or email to verify your self-enrollment license and expand the entitlement settings.

Once completed, you should see the entitlements in the "License Management" panel.

### Update a License from Existing License

!!! note 

    You must log in to the DRP Endpoint first.

Once a valid license file is installed on a DRP Endpoint, the "Check and Update License" button on the "License Manager"
page can be used to update the license entitlements.

This is typically needed when a new endpoint is being added to an entitlement or RackN has expanded the entitlements and
triggered a new version.

### Save an Entitlements File for Backup

!!! note 

    You must log in to the DRP Endpoint first.

You can download the current entitlement file using the UX by pressing the "Download" button on the "License Manager"
page. You can also use `drpcli contents show rackn-license > rackn-license.json` from the command line.

### Install a License from a File

!!! note 

    You must log in to the DRP Endpoint first.

The DRP entitlements file, typically `rackn-license-org.json`, is just a content pack. You can upload it from
"Catalog Import" in RackN UX or using `DRPCLI contents upload rackn-license-org.json`. Once a valid license is
installed, the key in the license can be used to retrieve an updated license.

### Verify Your License Entitlements

The "License Manager" page will show an overview of the licensed contents, features, and plugin providers of the
installed entitlements file. Please verify you are using the correct organization.

- "Soft" expire is when initial warning messages about subsequent de-licensing of a given feature will occur. At this
  date, the system is considered out of compliance but will continue to operate.
- "Hard" expire is the date that DRP will disable the relevant features or stop accepting add/update requests.

Many licenses, including trial/community licenses, use the
"upto-nodes" module which allows operators to use any licensed content up to the stated number of machines.

### Check or Update an Existing License

To update a license, visit the UX "License Management" page. Click the "Check and Update License" button in the top
right corner of the "License Management" panel. This uses the API described below to update your license including
adding new endpoints.

## Non-UX Update an Existing License

These steps require that you already have a valid RackN license. The information contained in the license is used to
verify your entitlements and to authorize an updated license. It relies on online RackN License management APIs.

!!! note 

    Do not run this in your CI/CD pipeline unless the output license becomes the license key for the successive
    requests.

!!! warning 

    Failure to use this API properly will result in aggressive rate limiting.

Required Header Fields:

- `rackn-endpointid`: DRP endpoint ID
- `Authorization`: License key

The `rackn-endpointid` is the endpoint ID (aka
`drpid`) of the DRP endpoint to be licensed. Licenses are issued per endpoint.

``` bash
#!/usr/bin/env bash
export ENDPOINTID=$(drpcli info get | jq -r .ha_id)
export KEY=$(drpcli profiles get rackn-license param rackn/license | jq -r .)

curl -X POST -H "rackn-endpointid: $ENDPOINTID" \
  -H "Authorization: $KEY" \
  https://cloudia.rackn.io/api/v1/license/update
```

!!! note 

    The license key used in the `Authorization` header should be from the most recently synchronized license to
    avoid rate limiting. This request should only return a license when the license renews, is adding a new endpoint, 
    or is updated by RackN.

Return Codes:

- `200` - The `license.json` output can be uploaded with `drpcli contents upload license.json`
- `304` - The license is up-to-date with the required endpoint included and no action is required.
- `429` - Too many requests from rate limiting or too many repeat attempts using an old license key in
  the `Authorization` header.

### RackN License API Rate Limiting

Note that RackN limits the number of endpoints that can be registered. If you have exceeded that number, the API will
not update the license and will return 304.

!!! note 

    RackN actively monitors license synchronizations to combat leaks. Improper use of this API will be treated as
    if the license was leaked.

Example of proper and intended use of this API:

1. Check if license is up-to-date using the `rackn/license` param.
1. If a new version exists, upload the resulting content pack.
1. Check if license is up-to-date using the `rackn/license` param.
1. Receive a `304: Not Modified` message and keep using the existing license.

Example of improper use of this API: (Will result in rate limiting)

1. Copy the `rackn/license` param from an existing endpoint
1. Use the api to "download the latest license"
1. Re-use the old `rackn/license` key in the Authorization header
1. Use the api to "download the latest license" despite step 2 returning the most recent license (Bad)

### Adding Endpoints to a License

Generally, the UX will add endpoints automatically during the "Check and Update" process on a new endpoint.

If you cannot use the UX to add an endpoint then use the API above. Add endpoints to a license up to your entitlements
by sending a new endpoint with license information validated for a different endpoint.

This will create a new license that can be applied to all endpoints.

### Removing Endpoints from a License

There is no automated process to remove endpoints from a license. Contact RackN if you need to do this.


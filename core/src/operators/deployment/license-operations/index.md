---
title: License Operations
tags:
  - operator
  - reference
---

As an operator, you will need to understand and manage the license of the DRP endpoint.

* License Management - All things license
* Activity Tracking - Activity data in the system and how to see it


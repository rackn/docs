---
title: Security Plans
tags:
  - reference
  - operator
---

# Security Guide {#rs_ops_security}

As an Operator, you will need to be aware of security concerns and issues.

## Recommendations

When considering deployment security, you have multiple layers of security to consider. Here are some areas to work through.

- User Access and Roles
- Machine Access / Control
- File server access
- Secure Parameter Stores

Since Digital Rebar exposes the list of active connections and activities, operators should periodically review to ensure that system access conforms with expectations. While monitoring connections is not practical for active intrusion detection, it will help Operators identify access by unmanaged tokens.

### User Access and Roles

As an administrator, you can create users and roles to restrict the scope of what users can do with the system.

Recommendations:

- Create Roles for each type of user / operator that is needed.
- Replace the rocketskates user with a different local superuser account
- Never remove the local superuser account
- Leverage SSO or SAML with group definitions to facilitate User/Password protection outside of DRP
- Explore certificate-based client authorization for services with embedded roles for tightly scoped service accessors
- All users operation via SSO or SAML and local user accounts are only for emergency.
- When using SSO or SAML, create DRP triggers and alarms to fire when the local user accounts are accessed.

### Machine Access / Control {#rs_ops_deploy_machine_access}

During normal operations, there are security concerns that should be followed to minimize security vulnerabilities.

There are three types of tokens that exist to get access to the system for executing jobs: CREATE, MACHINE and JOB.
Unlike, USER tokens, these are special use tokens designed to narrowly target specific operations.

The first is the discovery CREATE token. This token has only the ability to create a machine in DRP. The purpose of this
token is to allow machines to get added to DRP. As sledgehammer boots or join-up scripts are exectued, the CREATE token is
used to create the machine in the system.

The second is a machine specific token. This token has only the ability to ask for jobs to execute as a runner. The purpose of
this token is to allow a machine runner to connect to DRP and start executing jobs. The MACHINE token does NOT allow for reading
secure parameters or updating parameters. The MACHINE token can only manipulate the state of the current job and update logs.
The MACHINE token does NOT allow for changing the workflows, stages, profiles, tasks, or parameters on the machine.

The third token type, the JOB token, is dynamically generated when a job is sent to a machine.
The JOB token contains with additional capabilities. This JOB token is sent through an SSL connection and is not stored on
the system. The JOB token is also time restricted to reduce the validity window.
RackN provided tasks do not store this task on the system and maintain restricted set of accesses. The tasks can escalate privileges
for the single task. These tokens are short lived by default.

The non-system-generated token type, USER, is designed for user access and must be managed by the operator. While often broad by
default, USER tokens can also be created with narrower authority. RackN recommends following a least privilege strategy for
user tokens. This is especially important for tokens that are used for API integration or other long-term automation.

Recommendations:

- Administrators should periodically review new machines to ensure they are expected assets
- Triggers / Event handlers can be used for generating notifications of new machines
- Tasks should not store the JOB token on the machine
- The runner / agent configuration file should be restricted on the system
- Machines that complete provisioning should be placed in workorder mode or an idle workflow with no tasks.
- User tokens should never be used for machine automation tasks
- If stored or persisted, user tokens should be narrowly scoped
- If using token for API integrations, do not use long duration tokens; instead, plan to generate and rotate access tokens.

### File Server Access

DRP mananaged systems access DRP as a file server for provisioning and discovery. These files can hold access information.
The dynamic filesystem components of the system remove files from view when a machine is not in the proper state to access them.

Recommendations:

- Use firewall rules and network separation to separate the 8090/8091 and the 8092 traffic.
  - 8092 / API traffic is required for operator to DRP endpoint operations.
  - 8092 / 8091 / 8090 are required for machine to DRP endpoint operations.
- Consider what files you place in the files location of the DRP endpoint
- Limit risk of access by ensuring that machines return to stable workflow states in which there is limited or no dynamic data available.
  This is related to making sure that idle machines do not have tasks in their task list. Machines should not be in stages or bootenvs that
  serve files with sensitive data.

Note: By design, Digital Rebar limits user of sensitive information to the smallest possible window. For example, cloud-init API metadata is only available while a machine is being provisioned and, unlike cloud providers, not during normal operational states.

### Secure Parameter Store

Secure parameters in the system will by default be locally encrypted in memory and at rest. These secrets have keys local to the system.
They are maintained separately from the data.

To increase security, a [](#rs_config_secrets) can be used to keep the data secured separately from DRP.

Recommendations:

- Where possible use a secret store to hold secrets
- Use machine specific secrets to reduce compromise exposure

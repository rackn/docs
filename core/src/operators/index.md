---
title: Operator Guide
tags:
- reference
- operator
---

# Operator Guide {#rs_ops}

As an Operator, there are many tasks that you are expected to do.  This section is for you.

Each subsection focuses on various parts of the system and provides possible operations and examples.

* Deployment - Installing, Configuring, Maintaining, and Operating a federated set of enterprise-class DRP endpoints.
* Discovery - Configuring and Operating asset discovery
* Provisioning - Configuring and Operating machine provisioning.  This leverages pipelines to control hardware, operating system, and appliation provisioning.
* Extensions - Configuring and Operating Post provisioning automation
* Portal Operations - Using the UX to operate the whole system.
* Integrations - Integrations for external tooling
* Legacy/Deprecated - Components of the system that have been retired or replaced.
* Trouble Shooting - How to trouble shoot issues and where to start

!!! note

    The documentation is split into 5 sections.

    * [Geting Started](#rs_quickstart)
    * [Architectural Guide](#rs_arch)
    * [Developer Guide](#rs_dev)
    * [Operator Guide](#rs_ops)
    * [Resources](#rs_res)



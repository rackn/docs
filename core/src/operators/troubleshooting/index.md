---
title: Operator
tags:
- reference
- operator
---

As an Operator, there are many situations where you need to figure out what is going on and how to fix it.

Each subsection focuses on various parts of the system and provides trouble shooting hints.

* Deployment - Trouble shooting issues with Installing, Configuring, Maintaining, and Operating a federated set of enterprise-class DRP endpoints.
* Discovery - Trouble shooting Configuring and Operating asset discovery
* Provisioning - Trouble shooting Configuring and Operating machine provisioning.  This leverages pipelines to control hardware, operating system, and appliation provisioning.
* Concepts - Trouble shooting Configuring and Operating Post provisioning automation
* Integrations - Trouble shooting Integrations for external tooling


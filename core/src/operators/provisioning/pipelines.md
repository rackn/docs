---
title: Pipelines
tags:
- operator
- howto
---

# Pipelines {#rs_operators_highlights_pipeline}

A DRP (Digital Rebar Provision) Pipeline refers to an automated, standardized process crafted for infrastructure
automation. It's essentially a specific type of Digital Rebar Profile, which connects to a Universal Application and has
configurable parameters. While Universal Applications serve as general templates with default settings, Pipelines are
tailor-made, allowing operators to customize or enhance those defaults as needed.

The following documents will give you more information on building, maintaining and using pipelines:

- [Pipeline Architecture](#rs_arch_usage_pipeline)
  This document gives you a detailed introduction to pipelines and walks you through creating a simple pipeline.

- [Universal Pipelines](#rs_universal_linux_provision)
  This document talks about universal pipelines and using the universal workflow system to provision linux machines.

- [Automatic Machine Classification in Infrastructure Pipelines](#rs_operators_pipelines_classification)
  Provides an overview of how the `universal-discover` base classifier can automatically marshall configuration data via Profiles dynamically added to the Machine.

- [Universal Hardware Architecture](#rs_universal_hardware)
  This document talks about the pipelines used for hardware management and customization.

- [Universal Workflow Architecture](#rs_universal_arch)
  This document outlines the parameters within the Universal Workflow Pipelines, enabling you to tailor your pipeline
  precisely to your requirements.

- [Universal Workflow Operations](#rs_universal_ops)
  This section delves into how to use the Universal Workflow system.

Some additional documents that would assist with understanding DRP pipelines:

- [DRP Runner Architecture](#rs_arch_runner)
- [DRP Data Architecture](#rs_data_architecture)

---
title: Air-Gapped Installation
tags:
- openshift
- kubernetes
- airgapped
- installation
---

# Air-Gapped Installation {#rs_openshift_airgapped}

Air-gapped OpenShift installations enable deployment in environments with no direct internet access. This feature is currently in preview status and requires careful planning and additional manual steps compared to connected installations.

## Feature Status {#rs_openshift_airgapped_status}

The air-gapped installation capability is under active development. While the foundational components are in place, organizations should note:

1. This feature requires validation in production environments
2. Implementation requires specific manual steps
3. Support is provided on a best-effort basis
4. Procedures may evolve as we gather more production feedback

Organizations interested in air-gapped installations should engage with RackN support early in their planning process.

## Planning Requirements {#rs_openshift_airgapped_planning}

An air-gapped installation requires several key components:

1. A private container registry accessible within your network
2. Storage capacity for all required images (minimum 100GB recommended)
3. The RHCOS installation media uploaded to your DRP endpoint
4. Access to Red Hat's registry to perform initial image mirroring
5. Valid Red Hat OpenShift pull secret

## Image Requirements {#rs_openshift_airgapped_images}

### Required Images {#rs_openshift_airgapped_required_images}

Your air-gapped environment needs these critical components:

1. Red Hat CoreOS (RHCOS) installation media
2. OpenShift container images
3. Red Hat Universal Base Images (UBI)
4. OpenShift operators and dependencies
5. Additional components based on planned workloads

### RHCOS Media Preparation {#rs_openshift_airgapped_rhcos}

Before beginning installation, upload the RHCOS media to DRP:

```bash
# Download RHCOS ISO from Red Hat (on internet-connected system)
curl -O https://mirror.openshift.com/pub/openshift-v4/x86_64/dependencies/rhcos/4.15/latest/rhcos-4.15.6-x86_64-live.x86_64.iso

# Upload to DRP
drpcli isos upload rhcos-4.15.6-x86_64-live.x86_64.iso as openshift-rhcos-4.15.6
```

## Registry Setup {#rs_openshift_airgapped_registry}

Configure your private registry parameters:

```bash
# Set registry URL
drpcli profiles set demo param openshift/external-registry to "registry.example.com:5000"

# Enable registry usage
drpcli profiles set demo param openshift/external-registry-create to true
```

## Image Mirroring {#rs_openshift_airgapped_mirror}

Image mirroring must be performed from an internet-connected system that can also access your private registry. This process uses the OpenShift oc-mirror plugin.

### Install oc-mirror {#rs_openshift_airgapped_mirror_install}

```bash
# Get the oc-mirror plugin
curl -O https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/oc-mirror.tar.gz
tar -xvf oc-mirror.tar.gz
sudo mv oc-mirror /usr/local/bin/
```

### Mirror Images {#rs_openshift_airgapped_mirror_process}

Create an ImageSetConfiguration file:

```yaml
apiVersion: mirror.openshift.io/v1alpha2
kind: ImageSetConfiguration
storageConfig:
  registry:
    imageURL: registry.example.com:5000/mirror/oc-mirror-metadata
    skipTLS: false
mirror:
  platform:
    channels:
      - name: stable-4.15
        minVersion: 4.15.6
        maxVersion: 4.15.6
  operators:
    - catalog: registry.redhat.io/redhat/redhat-operator-index:v4.15
    - catalog: registry.redhat.io/redhat/certified-operator-index:v4.15
  additionalImages:
    - name: registry.redhat.io/ubi8/ubi:latest
  helm: {}
```

Perform the mirroring:

```bash
# Authenticate to registries
oc registry login

# Mirror images
oc mirror --config=imageset-config.yaml docker://registry.example.com:5000
```

## Certificate Management {#rs_openshift_airgapped_certs}

Your private registry requires valid TLS certificates. If using custom certificates:

```bash
# Configure registry certificate
drpcli profiles set demo param openshift/registry-cert to "$(cat /path/to/registry.crt)"
```

## Installation Process {#rs_openshift_airgapped_install}

Once prerequisites are met, initiate the installation:

```bash
# Create cluster configuration
cat > cluster-config.json <<EOF
{
  "Name": "demo",
  "Profile": "universal-application-openshift-cluster",
  "Params": {
    "broker/name": "pool-broker",
    "openshift/pull-secret": "YOUR-PULL-SECRET",
    "openshift/cluster-domain": "k8s.local",
    "openshift/external-registry": "registry.example.com:5000"
  }
}
EOF

# Create cluster
drpcli clusters create cluster-config.json
```

## Troubleshooting {#rs_openshift_airgapped_troubleshooting}

Common issues in air-gapped installations typically involve image availability or registry access. When troubleshooting:

1. Verify registry access from all nodes
2. Confirm image availability in your registry
3. Check certificate validity and trust
4. Review image pull secrets configuration
5. Monitor node-level container engine logs

## Best Practices {#rs_openshift_airgapped_best_practices}

For successful air-gapped deployments:

1. Document all manual steps performed during setup
2. Maintain copies of all configuration files
3. Create a procedure for updating mirrored images
4. Establish monitoring for registry health and capacity
5. Plan for ongoing operator and image updates

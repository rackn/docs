---
title: Advanced Cluster Management
tags:
- openshift
- kubernetes
- acm
- multicluster
---

# Advanced Cluster Management {#rs_openshift_acm}

Red Hat Advanced Cluster Management for Kubernetes (ACM) extends OpenShift's capabilities with multi-cluster management, policy-based governance, and application lifecycle management.

## Prerequisites {#rs_openshift_acm_prereq}

Before deploying ACM, ensure you have:

- An operational OpenShift cluster
- Valid Red Hat subscription
- Sufficient cluster resources:
    - 3+ worker nodes
    - 8GB RAM per worker
    - 4 vCPUs per worker

## Installation {#rs_openshift_acm_install}

### Configuration Parameters {#rs_openshift_acm_config}

Configure ACM installation parameters:

```bash
# Set ACM channel version
drpcli profiles set demo param openshift/acm-channel to "release-2.11"

# Configure namespace
drpcli profiles set demo param openshift/acm-namespace to "open-cluster-management"

# Adjust installation timeouts if needed
drpcli profiles set demo param openshift/acm-install-timeout to "1800"
drpcli profiles set demo param openshift/acm-install-mcm-timeout to "3600"
```

### Deploy ACM {#rs_openshift_acm_deploy}

To deploy ACM, first get your cluster's UUID:

```bash
# Get cluster UUID
CLUSTER_UUID=$(drpcli clusters show demo | jq -r .Uuid)

# Create work order to run ACM installation blueprint
drpcli clusters work_order on $CLUSTER_UUID
drpcli clusters work_order add $CLUSTER_UUID openshift-cluster-acm-install
```

## Usage Guide {#rs_openshift_acm_usage}

### Accessing ACM Features {#rs_openshift_acm_access}

Access ACM through the OpenShift console:

1. Log into the OpenShift web console
2. Navigate to "Home" → "Multicluster"
3. Access features through the console interface:
    - Cluster management
    - Application management
    - Policy enforcement
    - Risk and compliance

### Managing Clusters {#rs_openshift_acm_cluster_mgmt}

#### Importing Existing Clusters {#rs_openshift_acm_import}

Follow these steps to import clusters:

1. In the OpenShift console:
    - Navigate to Home → Multicluster → Infrastructure → Clusters
    - Click "Import cluster"
    - Follow the guided process

2. On the target cluster:
    ```bash
    # Apply the generated import command
    oc apply -f import.yaml
    
    # Verify registration
    oc get managedclusters
    ```

## Troubleshooting {#rs_openshift_acm_troubleshoot}

### Common Issues {#rs_openshift_acm_issues}

Monitor ACM health and diagnose issues:

```bash
# Check MultiClusterHub status
oc get mch -n open-cluster-management

# Review operator status
oc get csv -n open-cluster-management

# Check pod health
oc get pods -n open-cluster-management
```

### Collecting Diagnostics {#rs_openshift_acm_diagnostics}

Gather ACM-specific logs:

```bash
# Use must-gather for ACM
oc adm must-gather --image=registry.redhat.io/rhacm2/acm-must-gather-rhel8:v2.11 --dest-dir=acm-logs
```

## Best Practices {#rs_openshift_acm_best_practices}

Consider these guidelines for ACM usage:

1. Hub Cluster Sizing
    - Monitor resource usage regularly
    - Scale worker nodes based on managed cluster count
    - Consider dedicated nodes for ACM workloads

2. Policy Management
    - Test policies in inform mode before enforcement
    - Use hierarchical policies for complex requirements
    - Document policy decisions and exceptions

3. Cluster Lifecycle
    - Implement consistent naming conventions
    - Use placement rules effectively
    - Maintain backup procedures for ACM configurations

---
title: Security Features
tags:
- openshift
- kubernetes
- security
- configuration
---

# Security Features {#rs_openshift_security}

OpenShift provides robust security capabilities that can be configured and managed through DRP. This guide covers essential security features and their implementation in your OpenShift cluster.

## Security Context Constraints {#rs_openshift_scc}

Security Context Constraints (SCCs) control the actions containers can perform and the resources they can access. OpenShift includes several predefined SCCs, and you can create custom ones for specific security requirements.

### Managing SCCs {#rs_openshift_scc_manage}

Create a custom SCC that enforces specific security requirements:

```yaml
apiVersion: security.openshift.io/v1
kind: SecurityContextConstraints
metadata:
  name: custom-restricted
allowPrivilegedContainer: false
runAsUser:
  type: MustRunAsRange
seLinuxContext:
  type: MustRunAs
fsGroup:
  type: MustRunAs
supplementalGroups:
  type: MustRunAs
users:
- system:serviceaccount:custom-namespace:custom-sa
```

Review and manage SCC assignments:

```bash
# List available SCCs
oc get scc

# View SCC details
oc describe scc restricted

# Add service account to SCC
oc adm policy add-scc-to-user custom-restricted -z custom-sa -n custom-namespace
```

## Certificate Management {#rs_openshift_cert_management}

OpenShift uses certificates extensively for securing communications. Proper certificate management is crucial for cluster security.

### Cluster Certificates {#rs_openshift_cluster_certs}

Monitor and manage cluster certificates:

```bash
# Check certificate status
oc get apiserver

# Review certificate details
oc get secrets -n openshift-config

# Rotate certificates
oc adm certificate approve <csr-name>
```

### Custom Certificates {#rs_openshift_custom_certs}

Configure custom certificates for the ingress controller:

```bash
# Create certificate secret
oc create secret tls custom-certs \
  --cert=path/to/cert.crt \
  --key=path/to/cert.key \
  -n openshift-ingress

# Configure ingress controller
oc patch ingresscontroller.operator default \
  --type=merge -p \
  '{"spec":{"defaultCertificate": {"name": "custom-certs"}}}' \
  -n openshift-ingress-operator
```

## Authentication and Authorization {#rs_openshift_auth}

OpenShift provides flexible authentication and authorization options that can be integrated with your existing identity infrastructure.

### Identity Providers {#rs_openshift_idp}

Configure external authentication:

```yaml
apiVersion: config.openshift.io/v1
kind: OAuth
metadata:
  name: cluster
spec:
  identityProviders:
  - name: LDAP_Provider
    mappingMethod: claim
    type: LDAP
    ldap:
      attributes:
        id: ['dn']
        email: ['mail']
        name: ['cn']
        preferredUsername: ['uid']
      bindDN: "cn=directory manager"
      bindPassword:
        name: ldap-secret
      insecure: false
      url: "ldaps://ldap.example.com/ou=users,dc=example,dc=com?uid"
```

### Role-Based Access Control {#rs_openshift_rbac}

Implement fine-grained access control:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: custom-admin
rules:
- apiGroups: [""]
  resources: ["pods", "services"]
  verbs: ["get", "list", "watch", "create", "update", "delete"]
```

## Network Security {#rs_openshift_network_security}

Secure network communications within and outside your cluster using OpenShift's network security features.

### Network Policies {#rs_openshift_network_policies}

Implement network isolation:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-specific-namespaces
spec:
  podSelector: {}
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          environment: production
  policyTypes:
  - Ingress
```

### Service Mesh Security {#rs_openshift_service_mesh}

When using OpenShift Service Mesh, enhance security with mutual TLS:

```yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: istio-system
spec:
  mtls:
    mode: STRICT
```

## Compliance and Auditing {#rs_openshift_compliance}

OpenShift provides tools for maintaining compliance and auditing security events.

### Compliance Operator {#rs_openshift_compliance_operator}

Deploy and configure the Compliance Operator:

```yaml
apiVersion: compliance.openshift.io/v1alpha1
kind: ScanSetting
metadata:
  name: default
  namespace: openshift-compliance
profiles:
  - name: ocp4-cis
    kind: Profile
    apiGroup: compliance.openshift.io/v1alpha1
```

### Audit Logging {#rs_openshift_audit_logging}

Configure advanced audit logging:

```yaml
apiVersion: config.openshift.io/v1
kind: APIServer
metadata:
  name: cluster
spec:
  audit:
    customRules:
    - group: system:authenticated
      profile: WriteRequestBodies
    - group: system:unauthenticated
      profile: WriteRequestBodies
    profile: WriteRequestBodies
```

## Security Best Practices {#rs_openshift_security_best}

To maintain a strong security posture:

1. Implement the principle of least privilege for all users and service accounts.

2. Regularly review and rotate certificates, credentials, and access tokens.

3. Maintain detailed documentation of all security configurations and procedures.

4. Implement a consistent process for security patch management.

5. Monitor security-related events and alerts continuously.

6. Conduct regular security audits and compliance checks.

7. Maintain backups of all security configurations and credentials.

8. Establish procedures for security incident response.

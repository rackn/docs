---
title: OpenShift Troubleshooting
tags:
- openshift
- troubleshooting
- debugging
- diagnostics
---

# Troubleshooting Guide {#rs_openshift_troubleshooting}

## Common Issues {#rs_openshift_common_issues}

### Installation Failures {#rs_openshift_install_fails}

1. Check API server health:
   ```bash
   # Test API server health
   curl -k https://api.demo.k8s.local:6443/healthz

   # Verify API server version
   curl -k https://api.demo.k8s.local:6443/version
   ```

2. Check node and machine status:
   ```bash
   oc get nodes
   oc get machines
   oc describe node <node-name>
   ```

3. Review events:
   ```bash
   oc get events --sort-by='.metadata.creationTimestamp'
   ```

4. Examine operator status:
   ```bash
   oc get clusteroperators
   oc describe co <operator-name>
   ```

5. Check machine configuration:
   ```bash
   oc get pods -n openshift-machine-config-operator
   oc logs -n openshift-machine-config-operator -l k8s-app=machine-config-server
   ```

6. Check installation logs:
   ```bash
   openshift-install gather bootstrap --dir /root/cluster
   ```

### Network Issues {#rs_openshift_network_issues}

1. Verify DNS resolution:
   ```bash
   # Check API server resolution
   dig api.demo.k8s.local +short
   
   # Check internal API server resolution
   dig api-int.demo.k8s.local +short
   
   # Check application wildcard DNS
   dig *.apps.demo.k8s.local +short
   ```

2. Check pod networking:
   ```bash
   oc get pods -n openshift-sdn
   oc logs -n openshift-sdn -l app=sdn
   oc get network.config.openshift.io cluster -o yaml
   ```

3. Review service endpoints:
   ```bash
   oc get endpoints -A
   oc get svc -A
   ```

4. Test network connectivity:
   ```bash
   oc debug node/<node-name> -- chroot /host ip addr show
   oc debug node/<node-name> -- chroot /host ping <target-ip>
   ```

### Resource Constraints {#rs_openshift_resource_issues}

1. Check resource usage:
   ```bash
   oc adm top nodes
   oc adm top pods --containers=true --all-namespaces
   ```

2. Review quota usage:
   ```bash
   oc get resourcequota -A
   oc describe quota -n <namespace>
   ```

3. Monitor storage:
   ```bash
   oc get pv,pvc --all-namespaces
   oc get volumeattachment
   ```

### Certificate Issues {#rs_openshift_cert_issues}

1. Check certificate status:
   ```bash
   oc get csr
   oc get secret -n openshift-config
   ```

2. Review certificate expiration:
   ```bash
   oc get secret -n openshift-kube-apiserver-operator kube-apiserver-to-kubelet-signer -o jsonpath='{.metadata.annotations.auth\.openshift\.io/certificate-not-after}'
   ```

3. Verify API server certificates:
   ```bash
   oc get apiserver cluster -o yaml
   ```

### Authentication and Authorization {#rs_openshift_auth_issues}

1. Check identity provider configuration:
   ```bash
   oc get oauth cluster -o yaml
   oc get identity
   ```

2. Review role bindings:
   ```bash
   oc get clusterrolebinding
   oc get rolebinding --all-namespaces
   ```

### Registry Issues {#rs_openshift_registry_issues}

1. Check registry status:
   ```bash
   oc get pods -n openshift-image-registry
   oc get configs.imageregistry.operator.openshift.io cluster -o yaml
   ```

2. Review storage configuration:
   ```bash
   oc get pvc -n openshift-image-registry
   oc describe pvc -n openshift-image-registry
   ```

## Collecting Diagnostics {#rs_openshift_diagnostics}

1. Gather must-gather data:
   ```bash
   # General cluster data
   oc adm must-gather

   # Specific component data
   oc adm must-gather --image=registry.redhat.io/rhacm2/acm-must-gather-rhel8:v2.8
   ```

2. Review cluster logs:
   ```bash
   # Control plane logs
   oc logs -n openshift-controller-manager deployment/controller-manager

   # Node logs
   oc adm node-logs <node-name> -u kubelet

   # Specific pod logs
   oc logs -n <namespace> <pod-name> --previous
   ```

3. Export cluster state:
   ```bash
   # Full cluster state
   oc get all -A -o yaml > cluster-state.yaml

   # Specific component state
   oc get nodes -o yaml > nodes-state.yaml
   oc get co -o yaml > operators-state.yaml
   ```

4. Check etcd health:
   ```bash
   oc rsh -n openshift-etcd etcd-<control-plane-node> etcdctl endpoint health
   oc rsh -n openshift-etcd etcd-<control-plane-node> etcdctl endpoint status -w table
   ```

5. Monitor API server metrics:
   ```bash
   oc get --raw /metrics | grep apiserver_request_duration_seconds
   ```

## DRP-Specific Troubleshooting {#rs_openshift_drp_issues}

1. Check DRP machine status:
   ```bash
   drpcli machines show <machine-uuid>
   ```

2. Examine task execution:
   ```bash
   drpcli tasks status <task-uuid>
   drpcli tasks logs <task-uuid>
   ```

## API Health Verification {#rs_openshift_api_health}

Test API server health:
```bash
# Test API server health directly
curl -k https://api.demo.k8s.local:6443/healthz

# Get API server version
curl -k https://api.demo.k8s.local:6443/version
```

## Best Practices {#rs_openshift_troubleshooting_best}

1. Maintain cluster documentation including:
   - Network configuration
   - Storage layout
   - Authentication setup
   - Custom configurations

2. Implement systematic log collection and retention

3. Create and maintain runbooks for common issues

4. Document configuration changes and their rationale

5. Establish clear escalation paths for different types of issues

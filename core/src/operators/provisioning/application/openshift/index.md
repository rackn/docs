---
title: OpenShift Content Pack
tags:
- openshift
- kubernetes
- content
- reference
- operator
---

# OpenShift Content Pack Operator Guide {#rs_openshift_overview}

The OpenShift content pack enables automated deployment and management of OpenShift clusters through Digital Rebar Platform (DRP). This guide focuses on practical operation and maintenance of OpenShift clusters deployed using this content pack.

## Documentation Structure {#rs_openshift_docs}

Our documentation is organized to help you efficiently find the information you need:

- **Getting Started**: Quick start guide and basic concepts
- **Architecture**: Core components and system design
- **Requirements**: Prerequisites and environment setup
- **Operations Guide**: Day-to-day operational tasks
- **Advanced Features**: In-depth capabilities like ACM, disconnected installations, custom configurations, and security
- **Troubleshooting**: Common issues and diagnostics

## Quick Start {#rs_openshift_quickstart}

1. Ensure your environment meets the [minimum requirements](#rs_openshift_requirements)
2. Get your pull secret from [Red Hat OpenShift Cluster Manager](https://console.redhat.com/openshift/install/pull-secret)
3. Deploy using either:
   ```bash
   # CLI deployment
   drpcli clusters create - < <(jq -n \
   --arg pull_secret "$(cat pull-secret.json | jq -c .)" \
   '{
      Name: "demo",
      Profiles: ["universal-application-openshift-cluster"],
      Workflow: "universal-start",
      "Meta": {
        "BaseContext": "oc-cluster",
      },
      Params: {
         "broker/name": "pool-broker",
         "openshift/pull-secret": $pull_secret
      }
   }')
   ```
    Or use the DRP UI:
    - Navigate to Clusters → Add
    - Select "openshift-cluster" pipeline
    - Select "oc-cluster" context
    - Paste your pull secret
    - Click Save

4. Monitor deployment progress in the DRP UI or CLI
5. Access your cluster:
    ```bash
    export KUBECONFIG=/root/cluster/auth/kubeconfig
    oc get nodes
    ```

## Next Steps {#rs_openshift_next_steps}

- Review the [architecture](#rs_openshift_architecture) to understand cluster components
- Learn about [day-to-day operations](#rs_openshift_operations)
- Explore [advanced features](#rs_openshift_advanced_features)
- Check [troubleshooting](#rs_openshift_troubleshooting) for common issues

## Support and Resources {#rs_openshift_support}

For additional assistance:

- Review the [Digital Rebar documentation](https://docs.rackn.io/)
- Consult the [OpenShift documentation](https://docs.openshift.com/)
- Contact RackN support

## License {#rs_openshift_license}

This content pack is licensed under the RackN License. See documentation for details.

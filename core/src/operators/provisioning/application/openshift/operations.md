---
title: OpenShift Operations
tags:
- openshift
- operations
- maintenance
- tasks
---

# Day-to-Day Operations {#rs_openshift_operations}

## Accessing the Cluster {#rs_openshift_access}

### Getting Cluster Credentials {#rs_openshift_credentials}

1. Obtain the kubeconfig:
   ```bash
   drpcli profiles get demo param openshift/kubeconfig > kubeconfig
   export KUBECONFIG="$PWD/kubeconfig"
   ```

2. Get the console URL:
   ```bash
   drpcli profiles get demo param openshift/console
   ```

3. Get the kubeadmin password:
   ```bash
   drpcli profiles get demo param openshift/kubeadmin-password
   ```

## Health Monitoring {#rs_openshift_monitoring}

Essential monitoring commands:
```bash
# Node status and resource usage
oc get nodes
oc adm top nodes

# Workload health
oc get pods --all-namespaces
oc get events --sort-by='.metadata.creationTimestamp'

# Control plane health
oc get clusteroperators

# ACM health (if installed)
oc get pods -n open-cluster-management
oc get multiclusterhub -n open-cluster-management
```

## Common Tasks {#rs_openshift_tasks}

### Adding Worker Nodes {#rs_openshift_add_worker}

1. Update worker names in DRP:
   ```bash
   drpcli profiles set demo param openshift/workers/names to - <<EOF
   [
     "worker1.demo.k8s.local",
     "worker2.demo.k8s.local",
     "worker3.demo.k8s.local",
     "worker4.demo.k8s.local"
   ]
   EOF
   ```

2. Re-run cluster provision to create new nodes:
   ```bash
   drpcli clusters stage demo universal-cluster-provision
   ```

### Removing Nodes {#rs_openshift_remove_node}

1. Create a work order to remove the node:
   ```bash
   drpcli work_orders create '{"Meta": {"cluster-name": "demo", "node-name": "worker2.demo.k8s.local"}, "Blueprint": "openshift-cluster-remove-node"}'
   ```

2. Monitor the work order status:
   ```bash
   drpcli work_orders show [work-order-uuid]
   ```

### Upgrading OpenShift {#rs_openshift_upgrade}

1. Check available updates:
   ```bash
   oc adm upgrade
   ```

2. Initiate upgrade:
   ```bash
   oc adm upgrade --to=<version>
   ```

3. Monitor progress:
   ```bash
   oc get clusterversion
   oc get clusteroperators
   ```

### Managing Advanced Cluster Manager {#rs_openshift_acm_ops}

1. Check ACM operator status:
   ```bash
   oc get csv -n open-cluster-management
   oc get pods -n open-cluster-management
   ```

2. Verify MultiClusterHub status:
   ```bash
   oc get multiclusterhub -n open-cluster-management
   oc get multiclusterhub multiclusterhub -n open-cluster-management -o yaml
   ```

3. Access ACM features:
   ```bash
   # ACM is integrated into the OpenShift console
   # Access it through the OpenShift web console menu
   
   # Check managed clusters
   oc get managedclusters -A
   ```

Note: Advanced Cluster Management features are integrated directly into the OpenShift console. Once ACM is installed, you can access its features through the OpenShift web console's menu under "Home" → "Multicluster".

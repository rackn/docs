---
title: Provisioning
tags:
- reference
- operator
---

As an Operator, you will want to configure hardware, install an operating system, and deploy an application.

These three areas are aggregated into a pipeline for zero-touch automate deployment.

The following sections will allow you to focus each specific topic.

* Pipelines
* Universal - Merge this into Pipelines
* Hardware - Configure and Operate hardware configuration and updating
* OS - Install and Configure an operating system
* Application - Prepare and deploy an application (including platforms, e.g. OpenShift)


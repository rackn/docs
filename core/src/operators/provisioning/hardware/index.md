---
title: Hardware
tags:
- reference
- operator
---

As an Operator, you will want to configure and update hardware.

The following sections describe operations and configuration and classification processes for controling hardware.

* Classification
* Baselining
* Bios
* Raid
* IPMI
* Offline-Tooling
* Repo cloning


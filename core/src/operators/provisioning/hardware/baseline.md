---
title: Hardware Baselining
tags:
- operator
- howto
- backup
---

# Hardware Baselining {#rs_ops_prov_hw_base}

The hardware subsystem has methods to build universal hardware compatiable profiles.  The universal-discover workflow contains a classifier that will automatically apply these profiles if present to configure machines similarly.

The baselining process can also build build-of-material (BOM) validators.

## Set Up

To baseline a machine, you should configure the hardware to match what you want.  It is sometime easier to manually configure a machine and then capture the configuration.  This is not always required, but often matches existing procedures.

PXE boot a machine into sledgehammer and have it stop at universal-discover.  Once the machine is idle, you will need to set some parameters on the machine.

* `universal/baseline/application` - Required parameter that defines the pipeline being recorded.  e.g. vcf-4.0.0
* `validate/record-parameters` - Optional paramter that defines a list of parameters to record.  These could be inventory parameters that will be recorded for validation in a BOM profile.
* `universal/baseline/bom` - The name to use for this BOM.  e.g. silver-compute  If not specified, no BOM field as added to the profile names.

It can be helpful to create a baselining profile that records these parameters for later use.

## Baseline a Machine

To baseline a machine, set the machine's workflow to `universal-baseline` and make sure the machine is Runnable, `Runnable` set to `true`.

Once the workflow is complete, there will be a couple of profiles create on DRP with the information about the BOM and the hardware configuration.

## Validate the Profiles

To validate the profiles:

* Delete the machine and drive the machine through rediscovery
* Configure the `universal/application` parameter to the value used for `universal/baseline/application`
* Reset the workflow to `universal-discover`

This machine will end up with the profiles on the machine.

## Extract Profiles into Content Pack

To apply the profiles across the estate, you should build a content pack for storing these and version it.  This will allow the multi-site manager to deploy them to managed endpoints or your own method for upload content to production servers.  See [](#rs_dev_auto_content_gen) for more details.

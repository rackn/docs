---
title: Hardware Repository Tarball
tags:
- operator
- howto
- hardware-tooling
---

For airgap installations and other environments that do not want to access the hardware vendor's tooling
repositories directly, this document describes a process to build a distributable tar ball that can be
delivered to DRP endpoints and meets the requires of the tooling subsystem.

For this process, you will need a Linux system with the yum/dnf packaging tools.  The only step that
requires this is the create repos step.

## Current Items

RackN in the past has provided tarballs to make this easier for some environments.  RackN is moving
away from this for licensing reasons, but will continue to define the process for this.

A profile is provided in the `hardware-tooling` content pack, `global-hw-repo`.  This profile defines that general
layout of the expected `hw-repo.tgz` file.  This profile is generic and can be reused with this construction
process.  For the Supermicro and Lenovo use cases, parameters may have to be overridden to get the latest images.

## Hardware Repo Layout

The layout of the hardware repo directory in DRP requires that the tarball be exploded so that the files are 
located in the `tftpboot/files/hw_repo` directory.  The tarball should be a compressed tarball including the `hw_repo`
directory.

The files should contain:

* one cli compressed tarball - if Lenovo is used.
    * The location of the file can be overridden by the `lenovo-onecli-airgap-url` parameter.  The default value references a specific version
      in the hardware repo directory.  e.g. `{{.ProvisionerURL}}/files/hw_repo/lnvgy_utl_lxce_onecli01t-3.5.1_rhel_x86-64.tgz`
    * This value can be updated in the global profile or other attached profiles if the current value does not match the built hw_repo.tgz.
* SUM tool compressed tarball - if Supermicro is used.
    * The version of this file can be overridden by the `supermicro-manager-archive` parameter.  An example value is `sum_2.5.2_Linux_x86_64_20210112.tar.gz`.
    * This value can be updated in the global profile or other attached profiles if the current value does not match the built hw_repo.tgz.
* os_dependent YUM/DNF based repository - if Dell and HPE are used
* os_independent YUM/DNF based repository - if Dell and HPE are used

See below for an example file tree.


## Getting Files for the Repo

Files will need to be gathered from the corresponding vendor locations.  Each vendors files are listed separately with their
location.

### Lenovo

The OneCLI tool is delievered in a compress tarball.  The default location for this is at:
<https://datacentersupport.lenovo.com/us/en/solutions/ht116433-lenovo-xclarity-essentials-onecli-onecli>

The linux tools can be found at:
<https://download.lenovo.com/servers/mig/2023/05/19/57616/lnvgy_utl_lxce_onecli02c-4.1.0_linux_x86-64.tgz>

The file should be stored in the top-level directory of the tarball, e.g. hw_repo/lnvgy_utl_lxce_onecli02c-4.1.0_linux_x86-64.tgz

To use, you will need to add to the global profile the following parameter.  Note the value is dependent upon the filename.

``` yaml
lenovo-onecli-airgap-url:  '{{.ProvisionerURL}}/files/hw_repo/lnvgy_utl_lxce_onecli02c-4.5.0_linux_x86-64.tgz'
```

### Supermicro

The SUM tool is a licensed tool from Supermicro.  You will need to contact them to get the latest tarball.

The file should be stored in the top-level directory of the tarball, e.g. hw_repo/sum_2.5.2_Linux_x86_64_20210112.tar.gz

The file can be retrieved through the website url: <https://www.supermicro.com/en/support/resources/downloadcenter/smsdownload?category=SUM>

The bottom entry allows for selection of the Linux bundle.

### Dell

The Dell tools are found at <https://linux.dell.com/repo/hardware/dsu/>.  Dell has two sets of repos, `os_independent` and `os_dependent`.


#### os_independent

The `os_independent` repos contains that DSU tool.  This RPM will need to be pulled from the following location and placed in the `os_independent` directory.

The RPM is at: <https://linux.dell.com/repo/hardware/dsu/os_independent/x86_64/>  From this location, the latest DSU RPM can be found.

``` bash
mkdir -p hw_repo/os_independent
cd hw_repo/os_independent
wget https://linux.dell.com/repo/hardware/dsu/os_independent/x86_64/dell-system-update-2.0.2.0-23.03.00.x86_64.rpm
cd ../..
```

#### os_dependent

For each dependent tree, we will need to get the RPMs needed for this installation.  The RHEL7 release is NOT supported by Dell anymore.  The existing hw_repo.tgz
have a baseline that can be used for backwards compatability, but it will not and should not be updated.

The files under `RHEL8_64/srvadmin` and `RHEL9_64/srvadmin` will need to be downloaded.

``` bash
mkdir -p hw_repo/os_dependent/RHEL8_64
cd hw_repo/os_dependent/RHEL8_64
rm -f srvadmin*
wget --no-verbose --no-parent --recursive --level=1 --no-directories https://linux.dell.com/repo/hardware/dsu/os_dependent/RHEL8_64/srvadmin/
wget https://linux.dell.com/repo/hardware/dsu/os_dependent/RHEL8_64/metaRPMS/srvadmin-idrac-11.0.0.0-5268.el8.x86_64.rpm
rm -f index* robots.txt
cd ../../..
mkdir -p hw_repo/os_dependent/RHEL9_64
cd hw_repo/os_dependent/RHEL9_64
rm -f srvadmin*
wget --no-verbose --no-parent --recursive --level=1 --no-directories https://linux.dell.com/repo/hardware/dsu/os_dependent/RHEL9_64/srvadmin/
wget https://linux.dell.com/repo/hardware/dsu/os_dependent/RHEL9_64/metaRPMS/srvadmin-idrac-11.0.0.0-5268.el9.x86_64.rpm
rm -f index* robots.txt
cd ../../..
```

The `srvadmin-idrac` RPMs will need to be updated based upon the actual version in the `metaRPMS` directory.

### HPE

The HPE repos are different than the DELL repos.  HPE does not provide a single repo for each part.  You must choose your tools as needed.

The following tools are needed:

* hponcfg
* ilorest
* ssacli
* sum
* sut

These often require additional tools that are not in sledgehammer by default.  The following RPM are also needed:

* binutils
* dnf-plugins-core
* libxslt (8 only)
* net-tools
* yum-utils

Not all of those are needed.

These files will need to go into the RHEL8 and RHEL9 directories.

The following script will need to be updated to match the latest files when rebuilding.

``` bash
mkdir -p hw_repo/os_dependent/RHEL8_64
cd hw_repo/os_dependent/RHEL8_64
rm -f ilo* sum* ssacli* sut* hponcfg* yum-utils* net-tools* binutil* dnf-plugins-core* libxslt* python3-dnf*
# ILOREST
wget https://downloads.linux.hpe.com/SDR/repo/ilorest/RedHat/8/x86_64/current/ilorest-4.2.0.0-20.x86_64.rpm
# SUM tool
wget https://downloads.linux.hpe.com/SDR/repo/sum/RedHat/8/x86_64/current/sum-10.3.0-10.rhel8.x86_64.rpm
# ssacli
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/8/x86_64/current/ssacli-6.15-11.0.x86_64.rpm
# sut
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/8/x86_64/current/sut-4.1.0-17.linux.x86_64.rpm
# hponcfg
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/8/x86_64/current/hponcfg-6.0.0-0.x86_64.rpm
# Extra rpms
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/python3-systemd-234-8.el8.x86_64.rpm
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/python3-dnf-plugins-core-4.0.21-19.el8_8.noarch.rpm
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/dnf-plugins-core-4.0.21-19.el8_8.noarch.rpm
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/libxslt-1.1.32-6.el8.x86_64.rpm
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/binutils-2.30-119.el8.x86_64.rpm
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/net-tools-2.0-0.52.20160912git.el8.x86_64.rpm 
wget https://repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/Packages/yum-utils-4.0.21-19.el8_8.noarch.rpm
cd ../../..
mkdir -p hw_repo/os_dependent/RHEL9_64
cd hw_repo/os_dependent/RHEL9_64
rm -f ilo* sum* ssacli* sut* hponcfg* yum-utils* net-tools* binutil* dnf-plugins-core* elfutil*
# No scripting tools in RHEL9
# ILOREST
wget https://downloads.linux.hpe.com/SDR/repo/ilorest/RedHat/9/x86_64/current/ilorest-4.2.0.0-20.x86_64.rpm
# SUM tool
wget https://downloads.linux.hpe.com/SDR/repo/sum/RedHat/9/x86_64/current/sum-10.3.0-10.rhel9.x86_64.rpm
# ssacli
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/9/x86_64/current/ssacli-6.15-11.0.x86_64.rpm
# sut
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/9/x86_64/current/sut-4.1.0-17.linux.x86_64.rpm
# hponcfg
wget https://downloads.linux.hpe.com/SDR/repo/spp-gen11/RedHat/9/x86_64/current/hponcfg-6.0.0-0.x86_64.rpm
# extra rpms
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/python3-systemd-234-18.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/python3-dnf-plugins-core-4.3.0-5.el9_2.noarch.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/dnf-plugins-core-4.3.0-5.el9_2.noarch.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/binutils-gold-2.35.2-37.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/binutils-2.35.2-37.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/elfutils-debuginfod-client-0.188-3.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/elfutils-libelf-0.188-3.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/elfutils-libs-0.188-3.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/net-tools-2.0-0.62.20160912git.el9.x86_64.rpm
wget https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/yum-utils-4.3.0-5.el9_2.noarch.rpm
cd ../../..
```

Once these are built, you may need to also find the following RPMs as well.


## Building the Tarball

Once all the files have been retrieved, you will need to create repos and tar up the results.

### Updating the YUM repositories

This step requires a Linux system with the ability to run hardware repository tools.

``` bash
yum install createrepo -y
```

Once the tools are installed, you will need to update the `os_dependent` and `os_independent`
directories.

This is done by the following commands:

``` bash
cd hw_repo/os_dependent/RHEL7_64
createrepo --database .
cd ../../../hw_repo/os_dependent/RHEL8_64
createrepo --database .
cd ../../../hw_repo/os_dependent/RHEL9_64
createrepo --database .
cd ../../../hw_repo/os_independent
createrepo --database .
cd ../..
```

If other repos are added to the tree, you should update those as well.

### Tar Up The Files

To create the tarball, you will need to run the following command.

``` bash
tar -zcvf hw_repo-20230821.tgz hw_repo
```

This file can then be staged for installation or installed directly.

## Installing the Tarball

Once a tarball is provided, the file can be uploaded to the DRP endpoint with the files API or CLI.

``` bash
drpcli files upload hw_repo-20230821.tgz --explode
```

This file can also be distributed through Version Sets by adding the following entry to the Files section.

``` yaml
## Version set snippet
Files:
- Path: hw_repo-20230821.tgz
  Sha256Sum: f63982ea555cb55c45f7f2d90c4ddfe9ea22ec639d2d9c14366ccd4287ea15ec
  Source: '{{.ProvisionerURL}}/files/hw_repo-20230821.tgz'
  Explode: true
```

The Sha256Sum can be found by running `sha256sum` on the file.

```bash 
$ sha256sum hw_repo.tgz
f63982ea555cb55c45f7f2d90c4ddfe9ea22ec639d2d9c14366ccd4287ea15ec hw_repo.tgz
```

This assumes that the file has been staged on the Manager at the `tftpboot/files/hw_repo-20230821.tgz` location.

## Hardware Repo Layout Example

An example:

``` text
hw_repo
hw_repo/lnvgy_utl_lxce_onecli01l-3.1.2_rhel_x86-64.tgz
hw_repo/lnvgy_utl_lxce_onecli01n-3.2.0_rhel_x86-64.tgz
hw_repo/lnvgy_utl_lxce_onecli01t-3.5.1_rhel_x86-64.tgz
hw_repo/lnvgy_utl_lxce_onecli01v-2.5.0_rhel_x86-64.tgz
hw_repo/lnvgy_utl_lxce_onecli02c-4.1.0_linux_x86-64.tgz
hw_repo/os_dependent
hw_repo/os_dependent/RHEL7_64
hw_repo/os_dependent/RHEL7_64/hp-scripting-tools-11.40-9.rhel7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/hp-scripting-tools-11.60-20.rhel7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/hponcfg-5.6.0-0.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/hponcfg-5.7.0-0.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/ilorest-3.1.1-11.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/ilorest-3.6.0.0-45.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/libxml2-python-2.9.1-6.el7.5.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/libxslt-1.1.28-5.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/net-tools-2.0-0.24.20131004git.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/python-chardet-2.2.1-3.el7.noarch.rpm
hw_repo/os_dependent/RHEL7_64/python-kitchen-1.1.1-5.el7.noarch.rpm
hw_repo/os_dependent/RHEL7_64/repodata
hw_repo/os_dependent/RHEL7_64/repodata/156056476e5df8e03f9a2700e155f150d3c264d163e00713e85519ff974b2d32-other.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/1b7cab981d81b2225bbef36262838c59d7eb9368a0a1fe150126e29096147252-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/1ec3ab01452e7180c35a32b49c844382ac82a41bbc4048b428857100282c8f96-other.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/3353f48418555f3e03e2e8bdee2d8e22963f21eda1b50a3cf5713be24376ba46-primary.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/3cd61282321ef61a85fa43cf42bcff018d491f63250561ced88db91657a2bbe3-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/3d4f88b7569852590a371313a25a291a36c7f3cd6c1e6c1617454a22e77a0db3-primary.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/58518c3da50e1d5647394a3b432f8824b2ba0c5aca2b2e0eaefed9f3cb8e843b-other.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/5c5efcb8d1d0ed00fa914a14f7d6cc459ae5f9ef379b2e0b307d9761c7a458bb-other.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/6650ec19d05a9b1ea290905092ef31a94d58aac069a96d173cd21e18e5fc58b8-primary.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/7e8e97d13fb804e2df24cfeb3787fb3daa82388e0fad0846cb4c88627d008632-primary.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/83d1d9f4e6309e65ba4c777df5eccae1ed810b8034aedf776cc0a5e49d10fe12-filelists.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/967a76a79a2d603271895becb2c2c70f0ed7849d673b403bd4bfdb4ddbe6b69d-filelists.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/97d09cc55e37b2d3b94268a1db91370bfa98bdb8c44d2439b0f291c9c371ef85-other.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/99689a57140bd3636e50b42748e74d50a3c450b918acfe3a3314e035664e0ec5-primary.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/afe47e2a16326ff81e96071708f7fcf6e4956dc9766fb809410aa4a78d33e141-filelists.xml.gz
hw_repo/os_dependent/RHEL7_64/repodata/b37195e5eebd8f9f1ad8bd20e4e668226e665ba7c9f0b03bc3c317ed10bee6ae-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/fd03b155f3833e6b61b66ed11f2edd125e4b711c5e975c6da60b94f6df04a551-other.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/fffbcc2685a8327f173dd4835d34b3089a65e9cddfdfed582e6db9c0fcc98ee8-primary.sqlite.bz2
hw_repo/os_dependent/RHEL7_64/repodata/repomd.xml
hw_repo/os_dependent/RHEL7_64/srvadmin-all-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-argtable2-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-base-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-cm-9.5.0-20.09.00.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-deng-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-deng-snmp-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-hapi-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-idrac-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-idracadm7-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-isvc-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-isvc-snmp-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-itunnelprovider-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-jre-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-marvellib-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-nvme-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-omacore-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-omacs-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-omaksmig-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-omcommon-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-omilcore-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-ominst-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-oslog-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-realssd-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-selinux-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-server-cli-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-server-snmp-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-smcommon-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-smweb-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-standardAgent-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storage-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storage-cli-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storage-snmp-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storageservices-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storageservices-cli-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storageservices-snmp-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storelib-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-storelib-sysfs-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-sysfsutils-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-tomcat-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-webserver-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/srvadmin-xmlsup-9.5.0-4063.16816.el7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/ssacli-4.21-7.0.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/ssacli-5.30-6.0.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/sum-8.7.0-52.rhel7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/sum-9.0.2-2.rhel7.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/sut-2.7.0-26.linux.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/sut-2.9.1-22.linux.x86_64.rpm
hw_repo/os_dependent/RHEL7_64/yum-plugin-ovl-1.1.31-50.el7.noarch.rpm
hw_repo/os_dependent/RHEL7_64/yum-utils-1.1.31-54.el7_8.noarch.rpm
hw_repo/os_dependent/RHEL8_64
hw_repo/os_dependent/RHEL8_64/binutils-2.30-119.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/dnf-plugin-ovl-0.0.3-1.el8.noarch.rpm
hw_repo/os_dependent/RHEL8_64/dnf-plugins-core-4.0.21-19.el8_8.noarch.rpm
hw_repo/os_dependent/RHEL8_64/hp-scripting-tools-11.40-13.rhel8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/hp-scripting-tools-11.60-7.rhel8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/hponcfg-6.0.0-0.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/ilorest-4.2.0.0-20.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/libxslt-1.1.32-6.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/net-tools-2.0-0.52.20160912git.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/python3-dnf-plugins-core-4.0.21-19.el8_8.noarch.rpm
hw_repo/os_dependent/RHEL8_64/python3-systemd-234-8.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/repodata
hw_repo/os_dependent/RHEL8_64/repodata/10cf894538f0919f9c0811559316064bbf9304db3617e01b33756d5dc006c56e-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/2081ba2e35395b97ae39622740f16f55886bc542e1ef016e9ce7ae4cefaf4ae2-primary.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/21395c0c6a7402ed5cc7afb681aca4f81cbe6082cba95c3896abcea567b5df7b-primary.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/223653dcb5cab9bda959211d73d58e541a96a1ae974d970abe26c102df9b1a0a-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/30804c65954c735df61a94f5b272cc28fca9bcd8f10f084fb815e6af352d4dd9-primary.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/3135d1b007bc7c6ffec959c91e88d7dbde7e571cef64310e2c80893a388683a1-primary.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/371aadd8a321d92e7e9517f29efef7cd646421ae51314d66e8bd2b9f73eebc13-other.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/5069bb02f3f5ac32124337857e5b85cfa6bf261697ededfa1dafa2c4c8899a78-other.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/5d7587180c050fc02ce4dcb2b23a254aa7da1a6d019ab1e0078e32bec0fe3511-other.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/749ee9b9f792c04bb9fa304f5430a71fb123e833b162b86972ba4a2cbfda949c-primary.sqlite.bz2
hw_repo/os_dependent/RHEL8_64/repodata/83065ffbb96d60da229ee33dd451fd9f668825d181a02843f1d4166faf982251-other.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/aa127a1ebd203d230f3461d700b6396a8867f067565ea8198993264aa47eca07-filelists.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/b73266e1d0d8d92cfbd4e147580b79de171ded29468691d8ab909f908baaa52d-filelists.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/f90e2169aa53f5da3e894eaf4e2263ed72a686471102885772a2c7b27a67a345-primary.xml.gz
hw_repo/os_dependent/RHEL8_64/repodata/repomd.xml
hw_repo/os_dependent/RHEL8_64/srvadmin-argtable2-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-bossnvmelib-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-broadcomlib-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-deng-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-deng-snmp-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-hapi-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-idrac-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-idracadm7-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-isvc-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-isvc-snmp-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-itunnelprovider-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-jre-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-marvellib-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-nvme-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-omacore-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-omacs-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-omaksmig-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-omcommon-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-omilcore-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-ominst-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-oslog-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-realssd-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-selinux-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-smcommon-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-smweb-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-storage-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-storage-cli-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-storage-snmp-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-storelib-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-storelib-sysfs-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-sysfsutils-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-tomcat-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/srvadmin-xmlsup-11.0.0.0-5268.el8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/ssacli-6.15-11.0.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/sum-10.3.0-10.rhel8.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/sut-4.1.0-17.linux.x86_64.rpm
hw_repo/os_dependent/RHEL8_64/yum-utils-4.0.21-19.el8_8.noarch.rpm
hw_repo/os_dependent/RHEL9_64
hw_repo/os_dependent/RHEL9_64/binutils-2.35.2-37.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/binutils-gold-2.35.2-37.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/dnf-plugins-core-4.3.0-5.el9_2.noarch.rpm
hw_repo/os_dependent/RHEL9_64/elfutils-debuginfod-client-0.188-3.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/elfutils-libelf-0.188-3.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/elfutils-libs-0.188-3.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/hponcfg-6.0.0-0.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/ilorest-4.2.0.0-20.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/net-tools-2.0-0.62.20160912git.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/python3-dnf-plugins-core-4.3.0-5.el9_2.noarch.rpm
hw_repo/os_dependent/RHEL9_64/python3-systemd-234-18.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/repodata
hw_repo/os_dependent/RHEL9_64/repodata/018ec02e132c6ec249bcf000bc23c2a1aa9764e4f31a987f60ff330ae8667a91-primary.sqlite.bz2
hw_repo/os_dependent/RHEL9_64/repodata/35d1014793441afbd114d685a12a61cc4a8ce72023c6350a29fb1675694f7146-other.sqlite.bz2
hw_repo/os_dependent/RHEL9_64/repodata/6170eb66a51c8ecbc805d81181e8ca506b73eff51150d4d2b1f18d2168a34988-other.xml.gz
hw_repo/os_dependent/RHEL9_64/repodata/61acc6cffbd3db8b5f8ff4703e2e2de0e29910404ae37ee3c404ef2d9c32fcc3-filelists.xml.gz
hw_repo/os_dependent/RHEL9_64/repodata/699e16eb85212a4c45f6db8beb18d3dd7236d5df61c92457bad64f9996141e57-filelists.sqlite.bz2
hw_repo/os_dependent/RHEL9_64/repodata/f7e66390cc610e22ac13359a92fea3d9b0009921e3f0b450b4319b9f8cdbe310-primary.xml.gz
hw_repo/os_dependent/RHEL9_64/repodata/repomd.xml
hw_repo/os_dependent/RHEL9_64/srvadmin-argtable2-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-bossnvmelib-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-broadcomlib-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-deng-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-deng-snmp-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-hapi-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-idrac-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-idracadm7-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-isvc-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-isvc-snmp-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-itunnelprovider-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-jre-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-marvellib-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-nvme-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-omacore-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-omacs-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-omaksmig-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-omcommon-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-omilcore-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-ominst-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-oslog-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-realssd-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-selinux-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-smcommon-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-smweb-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-storage-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-storage-cli-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-storage-snmp-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-storelib-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-storelib-sysfs-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-sysfsutils-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-tomcat-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/srvadmin-xmlsup-11.0.0.0-5268.el9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/ssacli-6.15-11.0.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/sum-10.3.0-10.rhel9.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/sut-4.1.0-17.linux.x86_64.rpm
hw_repo/os_dependent/RHEL9_64/yum-utils-4.3.0-5.el9_2.noarch.rpm
hw_repo/os_dependent/repodata
hw_repo/os_dependent/repodata/0033ce4fca31f3758d9e089bd48f040e6cef614ce441d014045265d437c781b4-primary.sqlite.bz2
hw_repo/os_dependent/repodata/30f8d96ca2e0e0998cbf1f0ccbbcc86336ab95b42a99dee7fce6cc76fc4095a5-other.sqlite.bz2
hw_repo/os_dependent/repodata/a80e623f0baed13b680bd83cc066f46030252a2491c9019b1d42f037d4a68910-filelists.xml.gz
hw_repo/os_dependent/repodata/b212f19acce4902e5d09c1337ca91280e37f988f7e8e63e10d59b4874889a57b-other.xml.gz
hw_repo/os_dependent/repodata/c60fab9f937396e9d96f197e4a3d6435e61fe5627e164783a1b0e44db1c4fd41-filelists.sqlite.bz2
hw_repo/os_dependent/repodata/e9837ba5f80b4ec757c83b9bd03e21d0a5a970cf9aa649ab43ec4c00f059cbf2-primary.xml.gz
hw_repo/os_dependent/repodata/repomd.xml
hw_repo/os_independent
hw_repo/os_independent/dell-system-update-2.0.1.0-22.09.00.x86_64.rpm
hw_repo/os_independent/repodata
hw_repo/os_independent/repodata/21406486fa589766316bb9bc7dcf6c94089b8ab35a4c5bb12be9b49ba738c498-primary.sqlite.bz2
hw_repo/os_independent/repodata/4a411f5da636acaa602aa56bd1e1ab9ef47eb49f75e1996ab75b4d1d0b8880bf-primary.sqlite.bz2
hw_repo/os_independent/repodata/4b8c00216cce7fa46f39323fc5794841255a7540c029346a770e3192daa286bb-filelists.sqlite.bz2
hw_repo/os_independent/repodata/b4e6cd93bfeffc4b6883955017bcb8cd038c4eac4092c7b50395cb5911352ee3-other.sqlite.bz2
hw_repo/os_independent/repodata/b6c0de505d9ca6b3b359b7316d9776ecfa0f49124ae94cff97f7a160ec582fff-filelists.xml.gz
hw_repo/os_independent/repodata/bb703764be26295c6b72b837e5b6abc278b150a42a6e48142cdeeaa545701e22-primary.xml.gz
hw_repo/os_independent/repodata/c2a4986a2172fac4e8d7a73974a7de7344ff1147d4a9721749209e69998c9e9e-primary.xml.gz
hw_repo/os_independent/repodata/d54badbfda00ff967f4d7802a93116f1f70950e819e725ab4df67b3c2b0afd83-primary.xml.gz
hw_repo/os_independent/repodata/e8ef3480b09f2b59b54ad9b3dc2f5585e1f19aec14ec21f0a8b2d66e401b575b-primary.sqlite.bz2
hw_repo/os_independent/repodata/f265cc8a2dd3d95c92f35dab3bcdab985456adfe63b067b611fd74560377fb91-other.xml.gz
hw_repo/os_independent/repodata/repomd.xml
hw_repo/sum_2.11.0_Linux_x86_64_20230511.tar.gz
hw_repo/sum_2.5.2_Linux_x86_64_20210112.tar.gz
```

---
title: Universal Workflow Operations
tags:
- reference
- operator
---

# Universal Workflow Operations {#rs_universal_ops}

This section will address usage of the Universal Workflow system. The architecture and implementation of the Universal
Workflow system is described at [](#rs_universal_arch).

## Actions {#rs_universal_actions}

### Applying an Application {#rs_universal_apply_application}

### Building an Application {#rs_universal_build_application}

### Modifying Discovery Classifier {#rs_universal_classification}

### Adding Tasks to a Workflow {#rs_universal_flexiflow}

### Configuring Callbacks {#rs_universal_callback}

### Using Validations {#rs_universal_validation}

## Provided Workflows {#rs_universal_workflows}

The following workflows are the default set of universal workflows. Using these are starting points that do most default
operations needed to operate a data center.

Each workflow defines what it requires on start and set on completion. These actions allow for the workflows to work
together. For example, Discover should set the needed parameters for driving Hardware and the networking in the other
workflows. These are the elements defined by the universal parts of these components.

Universal depends upon:

- Digital Rebar Community Content Pack
- Task Library Content Pack
- FlexiFlow Content Pack
- Validation Content Pack
- Callback Plugin
- Classification Content Pack

Core Universal Workflows are detailed below.

### Discover {#rs_universal_discover}

This workflow handles discovery a machine and inventories the machine. It also will validate the basic initial pieces.
The final piece is classification to drive additional workflows if needed.

NOTE: Discover assumes that it is running in sledgehammer.

The default stages for `discover` are:

- discover
- inventory-minimal
- centos-setup-repos
- ipmi-inventory
- bios-inventory
- raid-inventory
- network-lldp
- inventory

A common set of post `discover` flexiflow tasks would be

- rack-discover
- universal-classify
- universal-host-crt-callback

### Start {#rs_universal_start}

This workflow handles starting a virtual machine or context that does not need a full bare metal discovery (
see [](#rs_universal_discover)). It also will validate the basic initial pieces. The final
piece is classification to drive additional workflows if needed.

NOTE: Unlike Discover, Start does not assume that is running in sledgehammer.

The default stages for `start` are:

- start
- drp-agent
- inventory-minimal
- cloud-inventory
- network-lldp
- inventory

### Hardware IPMI, Flash, RAID and BIOS Configure {#rs_universal_hardware}

This workflow is used to configure hardware and burnin that state. It is also the place where BMC certificates are
loaded.

The workflow assumes that the parameters of configuration have been set.
See [](#rs_universal_classification)
and [](#rs_universal_baseline).

### Build Baseline {#rs_universal_baseline}

This workflow is used outside of a universal workflow to snapshot the hardware state of a machine and builds profiles
named such they work with the universal-classify stage/task.

### ESXi Kickstart, Install, Config {#rs_universal_esxi}

Installs ESXi through multiple steps.

### Solidfire RTFI {#rs_universal_solidfire}

Handles updating and setting up SolidFire systems through RTFI process

### Linux Install {#rs_universal_linux_install}

Handles installing a linux operating system via network boot (kickstart or preseed).

Alternative Provisioning process in which disk images are written directly to media instead of being installed via
kickstart or preseed.

### Linux Join {#rs_universal_linux_join}

Runs standard install processes on Linux systems that are being added to Digital Rebar but where installed by another
system (like a Cloud).

### Image Deploy {#rs_universal_image_deploy}

Image Deploy - Handles deploying an image-based system

### Image Capture {#rs_universal_image_capture}

This is not a universal workflow in itself. By altering the linux-install workflow with the following tasks.... this
goes in the linux post install pre reboot.

- image-reset-package-repos
- image-update-packages
- image-install-cloud-init
- image-builder-cleanup
- image-capture

### Digital Rebar Bootstrap {#rs_universal_bootstrap}

This pipeline is used to bootstrap Digital Rebar Servers.

### Runbook Processing {#rs_universal_runbook}

Provides configuration management operations (e.g.: Ansible, Chef, Puppet, Salt) for installed machines.

### Cluster Provision {#rs_universal_cluster_provision}

Uses Cluster Managers (v4.8+) to manage clusters of machines.

### Resource Broker Provision {#rs_universal_resource_broker}

Uses Resource Brokers (v4.8+) to provision and decommission cloud and virtual infratructure.

### Burnin {#rs_universal_burnin}

Stress testing for system verification and validation before production deployment.

### Decomission {#rs_universal_decomission}

Decomission is used to clear, wipe and otherwise reset machines that are being removed from service.

### Rebuild {#rs_universal_rebuild}

Redrives the system through the process as a reset operation.

### Decommission {#rs_universal_decommission}

Handles decommissioning hardware by removing data from systems and cleans them up for reuse or disposal.

### Maintenance {#rs_universal_maintenance}

Sets the system to go into non-destructive Hardware and returnes toe LocalIdle

### Local {#rs_universal_local}

Starts the existing O/S installed on the system without image or network provisioning.

### SledgehammerWait {#rs_universal_sledgehammer_wait}

SledgehammerWait - End state for waiting or debugging

---
title: ESXi Getting Started Guide
tags:
  - operator
  - howto
---

How to deploy ESXi nodes using a pipeline.

## Prerequisites

This document assumes a working DRP endpoint with at least one machine that can be discovered.

If not done already, install the VMware plugin.

``` bash
 $ drpcli catalog item install vmware
```

## Setup Using Provided Bootenv

You will need to make sure you have the correct `bootenv` and ISOs necessary. There are several `bootenvs` provided by
RackN.

``` bash
$ drpcli bootenvs list | jq -r '.[].Name | select( . | contains("esxi"))'
```

If you find a version that works for you, either provide the ISO requested, or look at the bootenv documentation to
determine where to obtain the ISO.

``` bash
$ drpcli bootenvs show esxi_700u2a-17867351_rkn_vmware-install | jq -r '.Documentation'
```

Download the ISO from the location provided and upload it to DRP.

``` shell
drpcli isos upload RKN-ESXi-7.0U2a-17867351-standard.iso
```

!!! note 

    The ISO filename and sha256sum must match the BootEnv exactly.

## Creating a BootEnv Override

If you are unable to find a bootenv that meets your needs, you can create a parameter that will override a bootenv to
create the bootenv you need without creating an entire bootenv. See the following profile for an example.

``` yaml
Description: esxi 7.0U3l bootenv override
Documentation: |
  This profile will override the `esxi-install` bootenv using the `bootenv-customize` param, replacing the fields below in the bootenv rendering.  Because the ISO doesn't have our VIBs embedded, we set the `esxi/enable-legacy-install` param.
Name: esxi-7.0U3l-21424296
Params:
  bootenv-customize:
    esxi-install:
      OS:
        Name: esxi-7.0U3l-21424296
        SupportedArchitectures:
          amd64:
            IsoFile: VMware-VMvisor-Installer-7.0U3l-21424296.x86_64.iso
            Sha256: 486d3d0cd394a1540d0f1560d887234e58308d3fbda40d74fe88c66fca08e85b
        Version: "703"
  esxi/enable-legacy-install: true
```

!!! note

    When defining an override, the bootenv being override is the new of object.  In the above example,
    the bootenv being override is `esxi-install`.  There are multiple base esxi-install bootenvs.  The
    base bootenv is dependent upon the overall version of ESXi.

    For 6 and 7, you should use `esxi-install`.  For 8, you should use `esxi8-install`.

!!! warning 

    Included is the `esxi/enable-legacy-install: true` parameter. This is not recommended for production. This
    will not work with secure boot installations. It is recommended that you create a custom ISO that includes our VIBs if
    you are unable to find a bootenv that we provide that will work for you.<br /><br />You can find the VIBs on the
    endpoint at `https://ENDPOINT.URL:8090/files/plugin_providers/vmware/scripts/`. Please review VMware documentation on
    how to create custom ISOs.

The `bootenv-customize` parameter takes the target bootenv (`esxi-install` in the example above) and substitutes the
fields below. Please review the json-schema for `bootenv-customize` to see what fields can be overridden.

Upload the ISO provided in the `bootenv-customize` parameter.

``` bash
drpcli isos upload VMware-VMvisor-Installer-7.0U3l-21424296.x86_64.iso 
```

!!! note 

    The example profile above should be attached to either the `global` parameter, or the machine's bootenv must be
    set to `esxi-install` for the bootenv override to provide the bootenv and ISO files.

## Customizing the ESXi Installation

Several parameters are provided to assist with common configuration scenarios. The following is a sample profile that
includes some common configuration parameters.

``` yaml
Name: esxi-config
Description: Example ESXI profile
Documentation: |
  Sets some basic Param values for ESXi.
Params:
  esxi/disk-install-options: --firstdisk --overwritevmfs
  esxi/serial-console: gdbPort=none logPort=none tty2Port=com1
  esxi/shell-local: true
  esxi/shell-remote: true
  esxi/skip-notify: false
  esxi/skip-reboot: false
  esxi/skip-tools: true
```

Upload the new profile to the endpoint.

``` shell
$ drpcli profiles create esxi-example.yaml
```

## Creating the Universal Pipeline

It is possible to create a specific installation type. Below is an example pipeline profile.

``` yaml
Name: universal-application-esxi-7.0U3l-kickstart
Params:
  universal/application: esxi-7.0U3l-kickstart
  universal/workflow-chain-index-override: esxi-kickstart-deploy
  vmware/esxi-version-override: esxi-install
```

The key to declaring a pipeline is creating a profile with a `Name` that starts with `universal-application-`. It should
have the `universal/application` parameter where the string value excludes the `universal-application-` section from
the `Name` field. From the example, it is `esxi-7.0U3l-kickstart`.

The rest of the parameters are optional. The `universal/workflow-chain-index-overrite` parameter tells universal what
workflow to run during the `universal-chain-workflow` stage of a workflow. The `vmware/esxi-version-override` will
override the bootenv version to use. This works in conjunction with `bootenv-override`.

!!! note 

    The last line of the example isn't needed if using a valid bootenv and ISO.

## Putting It All Together

Using the bootenv or bootenv-override profile, ISO, and a pipeline defined, deploy some ESXi nodes.

``` shell
$ drpcli machines addprofile Name:esxi1-node1 universal-application-esxi-7.0U3l-kickstart
$ drpcli machines addprofile Name:esxi-node1 esxi-config
$ drpcli machines workflow Name:esxi1 universal-discover
```

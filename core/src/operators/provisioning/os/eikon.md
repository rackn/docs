---
title: Eikon Image Deploy
tags:
  - reference
  - operator
  - howto
  - tutorial
---

# Eikon Image Deploy {#rs_eikon_image_deploy}

Eikon is a specialized storage and image deployment tool designed to work with Digital Rebar Provision (DRP). It enables
operators to define complex storage configurations and deploy operating system images in an automated, repeatable way
across bare metal systems. Rather than using traditional installation methods, Eikon streamlines deployment through two
primary methods:

1. Raw Disk Images - Complete disk images written directly to storage devices (simpler, less flexible)
2. Rootfs Archives - Filesystem archives applied to custom storage configurations (more complex, highly flexible)

## Understanding eikon/plan {#rs_eikon_image_deploy_eikon_plan}

The `eikon/plan` parameter is the core configuration that defines how `Eikon` will deploy your system. It supports two
primary deployment approaches:

### Raw Image Deployment

The simplest form, where a complete disk image is written directly to disk:

```yaml
eikon/plan:
  disks:
    - name: disk-0               # Readable identifier for the disk
      grub_device: true          # Mark this disk for bootloader installation
      path: /dev/sda             # Target device path
                                 # Can also use persistent paths:
                                 # - /dev/disk/by-id/wwn-0x5002...
                                 # - /dev/disk/by-path/pci-0000...
                                 # - /dev/disk/by-uuid/123a...
      image:
        type: dd-zst             # Image type (dd-raw, dd-gz, dd-zst, etc.)
        url: "https://..."       # Image location
```

### Rootfs Deployment

A more flexible approach that allows custom storage configuration:

```yaml
eikon/plan:
  disks:                    # Physical disks
  vgs:                      # LVM Volume Groups
  swraids:                  # Software RAID arrays
  images:                   # OS images
```

## Initial Setup {#rs_eikon_image_deploy_setup}

Before beginning deployment, you'll need to prepare your environment:

1. Download the RHEL 9 image:
    ```bash
    curl -O https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-rootfs.tar.zst
    ```
2. Upload to DRP:
    ```bash
    drpcli -E https://<drp-host>> files upload rhel9-uefi-rootfs.tar.zst as images/eikon/rhel9-uefi-rootfs.tar.zst
    ```
3. Verify upload:
    ```bash
    drpcli files exists eikon/images/rhel9-uefi-rootfs.tar.zst
    ```
   Should return the checksum header of the file

!!! Note

        While you can use the S3 URLs directly in your
        profiles, like `https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-rootfs.tar.zst` for image url,
        it's recommended to download images and upload them to your DRP server. This significantly improves deployment speed and
        reliability.

## Creating Images for Eikon {#rs_eikon_image_deploy_creating_images}

Eikon supports two types of images: raw disk images and rootfs archives. Here's how to create both types:

### Creating Rootfs Archives

To create a rootfs archive, first install your OS normally on a machine, then:

1. Boot into the installed system
2. Install required compression tools:
    ```bash
    # For RHEL/Alma/CentOS
    dnf install zstd
    
    # For Ubuntu/Debian
    apt install zstd
    ```
3. Create the tar archive:
    ```bash
    cd /
    tar --exclude={proc,sys,tmp,dev,run,var/tmp}/* \
    --exclude={*.tar,root/post-install.log} \
    --numeric-owner \
    --acls \
    --selinux \
    --xattrs \
    --xattrs-include=* \
    -cvf my-rootfs.tar .
    ```
4. Compress the archive
    ```bash
    zstd my-rootfs.tar
    ```

The resulting `my-rootfs.tar.zst` can be used with `Eikon`.

!!! Important

        The tar flags are critical:
        - --numeric-owner: Preserves UIDs/GIDs
        - --acls: Preserves Access Control Lists
        - --selinux: Preserves SELinux contexts
        - --xattrs and --xattrs-include=*: Preserves extended attributes

### Creating Raw Images

Raw images can be created in several ways:

1. Direct from disk:
    ```bash
    dd if=/dev/sda of=my-raw-image.raw bs=4M status=progress
    ```
2. Converting from virtual machine disks:
    ```bash
    # From VMDK
    qemu-img convert -f vmdk -O raw "Virtual Disk.vmdk" system.raw
    
    # From VDI (VirtualBox)
    qemu-img convert -f vdi -O raw "disk.vdi" system.raw
    
    # From QCOW2
    qemu-img convert -f qcow2 -O raw "disk.qcow2" system.raw
    ```

Next, compress the raw image:

```bash
zstd my-raw-image.raw
```

!!! Note

        Using virtual machine disk conversion can be easier than direct disk capture as you can:
        - Create and test the system in a VM first
        - Ensure clean shutdown state
        - Avoid potential issues with physical hardware differences

### Pre-made Images

We provide several pre-made images ready for use with Eikon:

| Image                        | Type   | Description                                                                                              |
|------------------------------|--------|----------------------------------------------------------------------------------------------------------|
| alma9-uefi-rootfs.tar.zst    | Rootfs | [AlmaLinux 9 UEFI rootfs](https://eikon-images.s3.us-west-2.amazonaws.com/alma9-uefi-rootfs.tar.zst)     |
| alma93-uefi-raw.zst          | Raw    | [AlmaLinux 9.3 UEFI raw disk](https://eikon-images.s3.us-west-2.amazonaws.com/alma93-uefi-raw.zst)       |
| rhel8-uefi-raw.gz            | Raw    | [RHEL 8 UEFI raw disk](https://eikon-images.s3.us-west-2.amazonaws.com/rhel8-uefi-raw.gz)                |
| rhel8-uefi-rootfs.tar.zst    | Rootfs | [RHEL 8 UEFI rootfs](https://eikon-images.s3.us-west-2.amazonaws.com/rhel8-uefi-rootfs.tar.zst)          |
| rhel9-uefi-raw.zst           | Raw    | [RHEL 9 UEFI raw disk](https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-raw.zst)               |
| rhel9-uefi-rootfs.tar.zst    | Rootfs | [RHEL 9 UEFI rootfs](https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-rootfs.tar.zst)          |
| ubuntu20-uefi-raw.zst        | Raw    | [Ubuntu 20.04 UEFI raw disk](https://eikon-images.s3.us-west-2.amazonaws.com/ubuntu20-uefi-raw.zst)      |
| ubuntu20-uefi-rootfs.tar.zst | Rootfs | [Ubuntu 20.04 UEFI rootfs](https://eikon-images.s3.us-west-2.amazonaws.com/ubuntu20-uefi-rootfs.tar.zst) |
| ubuntu22-uefi-rootfs.tar.zst | Rootfs | [Ubuntu 22.04 UEFI rootfs](https://eikon-images.s3.us-west-2.amazonaws.com/ubuntu22-uefi-rootfs.tar.zst) |
| ubuntu22.raw.zst             | Raw    | [Ubuntu 22.04 UEFI raw disk](https://eikon-images.s3.us-west-2.amazonaws.com/ubuntu22.raw.zst)           |

Each image has a corresponding test profile in the `eikon` content pack prefixed with `test-eikon-*`. For
example, `test-eikon-uefi-rootfs-disk-plan-rhel9` corresponds to the RHEL 9 UEFI rootfs image.

## Getting Started: Deploying RHEL 9 {#rs_eikon_image_deploy_rhel9}

This guide demonstrates deploying RHEL 9 using a rootfs archive with a custom storage configuration.

### Pre-requisites

1. DRP server version v4.14 or greater installed and configured
2. Eikon and Universal content packs installed
3. Target machine registered with DRP (discovered)
4. RHEL 9 uefi rootfs image downloaded from
   eikon-images [S3 bucket](https://us-west-2.console.aws.amazon.com/s3/buckets/eikon-images)
5. Verified target machine has required disk (/dev/sda) available

### Using Pre-configured Profiles

The eikon content pack includes pre-configured profiles designed to work with our standard images. The profile contains
a pre-defined eikon/plan which is compatible with the rhel9-uefi-rootfs image available in the s3 bucket. Here's an
example profile for RHEL 9 deployment:

```yaml
Name: test-eikon-uefi-rootfs-disk-plan-rhel9
Params:
  eikon/plan:
    disks:
      - name: disk1
        path: /dev/sda
        ptable: gpt
        grub_device: true
        partitions:
          - name: part1
            id: 1
            ptype: vfat
            size: 1G
            fs:
              name: fs1
              fstype: vfat
              mount: /boot/efi
          - name: part2
            id: 2
            ptype: xfs
            size: 1G
            fs:
              name: fs2
              fstype: xfs
              mount: /boot
          - name: part3
            id: 3
            size: 17G
            ptype: LVM2_member
    vgs:
      - name: vg1
        pvs:
          - part3
        lvs:
          - name: lv1
            size: 14G
            fs:
              name: fs3
              fstype: xfs
              mount: /
          - name: lv2
            size: 1G
            fs:
              name: fs5
              fstype: swap
    images:
      - url: "{{.ProvisionerURL}}/files/eikon/images/rhel9-uefi-rootfs.tar.zst"
        type: tar-zst
```

This profile:

* Creates a GPT-partitioned disk
* Configures UEFI boot with /boot/efi partition
* Sets up a separate /boot partition
* Uses LVM for root filesystem and swap space
* Deploys RHEL 9 from a compressed tar archive

!!! Note

        Additional profiles can be found by looking for profiles that start with `test-eikon-*`. There are a combination of raw
        and rootfs profiles. The links to the images for these profiles have been detailed in the `Creating Images for Eikon` section.

### Starting eikon-image-deploy

The `eikon` content pack provides a workflow called `eikon-image-deploy` which can be used in combination with the
profiles
to do an image deploy on your Machine. We will be using this workflow to deploy the image. For more information on the
workflow itself please look at the  [Architecture Docs](#rs_arch_eikon)

Follow these steps to begin eikon-image-deploy:

- Navigate to the test machine in the UX
- Go to the Editor tab
- Under `Collections` click the plus sign next to `Profiles`
- Add the following profiles:
    - sledgehammer-alma9-v4.12.0
    - test-eikon-uefi-rootfs-disk-plan-rhel9
- Under `Workflow Managememt` click on the minus sign next to `Pipeline` and select `eikon-image-deploy`
- Click `Select Workflow` next to `WORKFLOW` and select `universal-image-deploy`
  from the drop-down.

This will kick off the eikon image deploy process. At a high level, this will:

- Boot into sledgehammer
- Configure storage according to the eikon plan provided
- Deploy and extract the RHEL 9 image
- Configure bootloader
- Reboot into the deployed system

### Validating Deployment

Once the workflow completes and the system boots, verify the deployment by:

1. Check storage layout:
    ```bash
    # Verify partitions and LVM setup
    lsblk
    
    # Check LVM configuration
    vgs
    lvs
    
    # Verify mount points
    df -h
    ```
2. Verify OS installation:
    ```bash
    # Check OS version
    cat /etc/os-release
    
    # Verify boot mode
    [ -d /sys/firmware/efi ] && echo "UEFI" || echo "Legacy"
    
    # Check swap configuration
    swapon -s
    ```

In DRP, the system should show up as `Online` (which means it is connected to DRP)

### Common Issues and Troubleshooting

1. Image Not Found:
   Verify that the image being deployed exists in the path suggested in the image URL. In this case, the image is
   expected to be located at `{{.ProvisionerURL}}/files/eikon/images/rhel9-uefi-rootfs.tar.zst`. You can verify this by
   navigating to `<drp-host>:8091/files` and ensuring that the image is presetn in eikon/images.
2. Boot Failures
    - UEFI/Legacy mismatch
        - Currently only UEFI boot is supported by eikon. The test machine's boot mode has to be UEFI
        - Ensure that secure boot is disabled
3. Machine does not show online:
   It could be that the network on the image has not been enabled so enable network on the machine and restart the
   `drp-agent`. Make sure to run the following command as `root`
   ```bash
   systemctl restart drp-agent
   ```
   This should restart the drp-agent and the machine should show up as `Online`

### Additional Assistance

For additional assistance open a zendesk ticket with the following information:

- Capture the output of failed tasks
- Capture system logs
- Note any error messages from the DRP UI




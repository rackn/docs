---
title: Universal Workflow Linux Provision
tags:
- operator
- howto
---

# Universal Workflow Linux Provision {#rs_universal_linux_provision}

This section will address usage of the Universal Workflow system for deploying Linux systems (with DRP v4.8+). The
architecture and implementation of the Universal Workflow system is described
at [](#rs_universal_arch).

## Universal Linux Provision Requirements {#rs_universal_linux_install_requirements}

The following requirements must be met to utilize the Universal Pipelines and Workflows to successfully install Linux
systems:

- Digital Rebar Platform (DRP) v4.8.0 or newer
- DRP Community Content bundle version v4.8.0 or newer
- Universal content bundle v4.8.0 or newer
- An appropriate Operating System ISO installed as per the BootEnv specification for the OS version you are installing

## Universal Pipelines {#rs_universal_pipelines}

Universal *Pipelines* are declarative definitions that describe the end state of a Machine. Within the Digital Rebar
system, *Pipelines* are defined as a profile which
contains the basic param definitions to drive the *Pipeline*. Pipelines are not
separate objects in the system.

The primary *Param* that is used to describe the declarative state of the Machine is:

- `universal/application`

Additional Params may be used to configure or supplement the final declarative state.

In the case of the Linux Provision/Install, additional values need to be set since the core
Workflow (`universal-linux-install`) is a generic installer workflow that needs to be configured to tell it **what**
Linux distro and version to install.

### Specifying Linux Distro and Version {#rs_universal_linux_install_distro}

The `universal-linux-install` workflow in the Universal Linux Pipeline supports multiple distro and versions of Linux.
To differentiate each install, appropriate Params must be set to define the Distro and version. The Param essentially
references a bootenv, which contains the Distro,
version, and appropriate install templates for the automated install program (eg kickstart, preseed, or autoinstall).

Additionally, the workflow chain map needs to be adjusted to use the correct set of chained Workflows from the default
map set.

The Params are as follows:

- `linux/install-bootenv-override`: the Linux BootEnv to install
- `universal/workflow-chain-index-override`: set this to `linux-install`

### Prebuilt Linux Install Profiles {#rs_universal_linux_profiles}

To help make the process easier to use, DRP contains several *Profiles* which are set to install the supported selection
of Linux Distros and versions.

To view these, visit the *Profiles* menu in the Web UI, and filter for all profiles wthat begin
with `universal-application-`. You can also perform this operation at the command line, with the following `drpcli`
syntax.

```shell
drpcli profiles list | jq -r '.[] | select( .Name | test("universal-application-.*")) | .Name'
```

An example Profile for a Photon 4 Linux OS install Pipeline contains the following Params:

-     `universal/application`: photon-4
-     `linux/install-bootenv-override`: photon-4-c001795b8-install
-     `universal/workflow-chain-index-override`: linux-install

### Customizing the Operating System Install {#rs_universal_linux_customization}

All the standard DRP mechanisms for customizing the installed OS are supported in this path. Generally speaking; this is
done by setting *Param* values on the Machine object, either directly, or as attached Profiles (or also in the `global`
Profile).

For example, if the Machine you are installing to requires that the OS be installed on `/dev/sdb` instead of the
default `/dev/sda`; the installers support setting the Param `operating-system-disk` to an appropriate value.

### Using the Pipeline {#rs_universal_linux_using_pipeline}

To use the Pipeline simply perform the following steps for a given Machine:

- Add the Pipeline Profile to the Machine (eg `universal-application-photon-4`)
- Set the Machine to one of the starting Workflows in the Pipeline; usually `universal-discover`

!!! note

    If the Machine is already in the starting Workflow (eg `universal-discover`), you must clear and reset the Workflow. This can be accomplished by using the Web UI "*Actions*" --> "*Restart Workflow*" selection; with the Machine's checkbox selected (the left most checkbox)

The machine will then Chain through the defined Workflows as specified by the `linux-install` chain map. At the time of
this writing, that chainmap example looks like:

```shell
# Example workflow chain map for Linux Installations
linux-install:
  universal-discover: universal-hardware
  universal-hardware: universal-burnin
  universal-burnin: universal-linux-install
  universal-linux-install: universal-runbook

# the below are NOT installable Workflows, they are included here
# for completeness sake of the chain maps full set of values
  universal-start: universal-runbook
  universal-maintenance: universal-discover
  universal-rebuild: universal-discover
```

This is specified as the `default` values in the `universal/workflow-chain-map` Param.

The chain map is a series of right side rules, which if in that currently named Workflow, moves to the next workflow
defined on the left hand side, until no further map entries match. In this example, the Machine will end in
the `universal-runbook` Workflow, assuming starting from `universal-discover`.

The chaining action is performed by the Workflow Stage and Task named `universal-chain-workflow`.

Important notes on other chain maps starting workflows:

**universal-start**

The `universal-start` Workflow is designed for public cloud use systems, and does not include the actual Linux Install
workflows in the chain map, as it is assumed that a public Cloud virtual machine has been instantiated with and
Operating System already.

**universal-decommision**

**universal-maintenance**

Both the decommision and maintenance Workflows are designed to take an existing (already) installed system to a
Sledgehammer BootEnv for performing maintenance tasks. These workflows will modify the Machine object and the changes are
not compatible with Installation workflows.

### Additional Pipeline Capabilities

Infrastructure Pipelines utilize standard `Workflows` that are built with consistent
extensible operations.  Some of the operations that are supported in Pipelines are
as follows:

- [Machine inventory collection](#rs_inventory_functions)
- [Hardware Lifecycle Management](#rs_universal_hardware)
- [Automatic Machine Classification in Infrastructure Pipelines](#rs_operators_pipelines_classification)
- External RESTful API interactions (Callbacks)
  - See "Developer" --> "Contents" --> "Callback" for an overview of Callbacks
  - [Using Universal Callbacks](#rs_kb_00075)
- Extensible validation rules:
  - [Using Validation in Universal Workflows](#rs_universal_validation)
  - See "Developer" --> "Contents" --> "Validation" for an overview of the Validation system
- Flexible task injection:
  - See "Developer" --> "Contents" --> "Flexiflow" for an overview of the Flexiflow task injection system

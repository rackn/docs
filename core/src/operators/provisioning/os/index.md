---
title: OS
tags:
- reference
- operator
---

As an Operator, you will want to install and configure an operating system.

The following sections will allow you to focus each specific topic.

* Eikon
* image-deploy
* image-deploy-storage
* image building
* os installation
* os configuration
* linux provisioning
* esxi


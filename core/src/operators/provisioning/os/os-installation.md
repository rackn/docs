---
title: OS Installation
tags:
- operator
- howto
---

# OS Installation {#rs_ops_prov_os_installation}

Digital Rebar Provision (DRP) is a robust platform tailored for automating bare metal provisioning. A core feature of
DRP is its ability to facilitate the installation of various operating systems seamlessly.

- [Linux Install](#rs_kb_00061)
  This guide details the utilization of the universal content pack for Linux installations with DRP.

- [ESXi Usage](#rs_esxi)
  Get insights on using ESXi with DRP here. Just a heads-up: for the actual ESXi installation steps, you'll want to
  check the official ESXi documentation.

- [Image Deploy](#rs_imagedeploy)
  This resource outlines the procedures for deploying system images using the DRP image deploy plugin.

More information OS Installation architecture can be found at [here](#rs_image_deploy_arch).

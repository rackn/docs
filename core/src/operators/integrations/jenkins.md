---
title: Jenkins Integration
tags:
- explanation
- architecture
---
# Building a Jenkins Pipeline with Digital Rebar Terraform Provider

This document provides a guide on how to construct a Jenkins Pipeline that leverages the Digital Rebar Terraform provider to deploy an application.

## Overview

This guide will walk you through the process of creating a Jenkins Pipeline that automates the deployment of an application using the Digital Rebar Provision (DRP) provider for Terraform.

## Prerequisites

Before diving into the details, ensure you have the following set up:

1. **Deployed Jenkins Instance**: An installed and configured Jenkins instance, where we will be setting up our pipeline.
2. **Deployed Digital Rebar Instance**: A running instance of Digital Rebar Provision (DRP), which will provide the infrastructure for deploying our application.
3. **Terraform Configured on the Jenkins Build Server**: Terraform should be installed and properly configured on the same server Jenkins is running. Jenkins will execute Terraform commands to manage the infrastructure on Digital Rebar.
4. **Networking**: The Jenkins instance will need to be able to "talk" to the DRP instance via API.

## Key Topics

* **Jenkins Overview**: A brief introduction to Jenkins and its role in Continuous Integration/Continuous Deployment (CI/CD).
* **Terraform and Digital Rebar Provision**: An overview of Terraform and the Digital Rebar Provision (DRP) provider, focusing on their role in infrastructure management.
* **Creating a Jenkins Pipeline**: Steps on how to create a Jenkins Pipeline, including scripting a `Jenkinsfile`.
* **Integrating Terraform with Jenkins**: How to include Terraform scripts in your Jenkins Pipeline and leverage the DRP provider to manage infrastructure.
* **End-to-End Example**: A practical example that ties everything together - using Jenkins and Terraform DRP provider to deploy an application.
* **Troubleshooting and Best Practices**: Common issues, their solutions, and best practices when creating a Jenkins Pipeline with Terraform and the DRP provider.

By the end of this guide, you will have a clear understanding of how to use Jenkins in combination with Terraform and the Digital Rebar Provision provider to automate application deployment and infrastructure management tasks.


## Jenkins Overview and Its Role

[Jenkins](https://www.jenkins.io/) is a widely used open-source automation server that enables developers to reliably build, test, and deploy their software. It is the backbone of any modern Continuous Integration/Continuous Deployment (CI/CD) system.

In our setup, Jenkins will play a crucial role in orchestrating the entire application deployment lifecycle, from code checkout to resource teardown:

1. **Polling Source Code Management (SCM)**: Jenkins is configured to poll our SCM (or it could be set up to trigger builds via a webhook), checking for changes in our repository. Once a change is detected, it triggers the pipeline job.

2. **Checking Out the Code**: At the start of the pipeline job, Jenkins checks out the latest code from the SCM. This code includes the application source code and the Terraform scripts.

3. **Building the Application**: After the code checkout, Jenkins will execute the build stage for our application. This could involve compiling the source code, running tests, or packaging the application, depending on the nature of the application.

4. **Provisioning Infrastructure with Terraform**: Once the application is built, Jenkins uses Terraform to request a resource from the Digital Rebar Provision (DRP). This resource could be a virtual machine, a container, or any other infrastructure component that DRP can provide. The Terraform scripts define what resources are requested and how they're configured.

5. **Deploying the Application**: With the infrastructure in place, Jenkins then deploys the built application onto the provisioned resource.

6. **Orchestrating the Teardown**: After the application is successfully deployed and all necessary tests are passed, Jenkins coordinates the teardown of the infrastructure, again using Terraform. This ensures that resources are not left hanging around, keeping the environment clean and minimizing costs.

In summary, Jenkins provides a centralized system that can manage our application's entire lifecycle. Its integration with tools like Terraform and Digital Rebar Provision enables a powerful, automated CI/CD pipeline that can greatly improve the efficiency of software development and deployment.


## Terraform and Digital Rebar Provision (DRP) Overview

[Terraform](https://www.terraform.io/) is an open-source tool by HashiCorp that allows you to define and provide data center infrastructure using a declarative configuration language. It treats infrastructure as code, which means that development practices can be used, such as versioning, automated testing, and continuous delivery.

[Digital Rebar Provision (DRP)](https://rackn.com/rebar) is a next-generation, API-driven DHCP/PXE provisioning and IPAM solution. DRP provides a highly configurable service for dynamically provisioning operating systems and software layering solutions.

In our setup, the roles of Terraform and DRP are as follows:

**Terraform's Role**: Terraform is tasked with requesting and managing the resources required for our application deployment from DRP. It does this through scripts that describe the desired state of the infrastructure. These scripts utilize the DRP provider, which allows Terraform to make API calls to DRP and manage resources.

**DRP's Role**: DRP acts as the provider of the infrastructure that our application will be deployed onto. It must have a pre-configured pool of resources ready to be provisioned. When Terraform requests a resource, DRP allocates a suitable resource from its pool and returns the necessary information back to Terraform. This could be the IP address of a provisioned virtual machine, and other metadata.

In summary, Terraform and DRP work together to provide an automated method of provisioning, managing, and tearing down the infrastructure required for our application deployment. This allows us to handle infrastructure as code, improving efficiency and consistency in our deployment process.

## Creating a Jenkins Pipeline

A Jenkins Pipeline is a suite of Jenkins features and plugins that supports implementing and integrating continuous delivery pipelines into Jenkins. Here are the steps to create a Pipeline job in Jenkins:

1. **Jenkins Home Page**: Navigate to your Jenkins instance home page. This typically will be on `http://<Jenkins-Server-IP>:8080`.

2. **New Item**: Click on the 'New Item' link in the left navigation.

3. **Enter Item Name and Select Pipeline**: In the display box, provide a name for your pipeline (for example, 'HelloWorldPipeline'). Below that, select the 'Pipeline' option. Click 'OK' to proceed.

4. **Configure the Pipeline**: In the configuration page, you have a lot of options that can be left as default for now.

   In the 'Pipeline' section at the bottom, you should choose the 'Pipeline script from SCM' option. This will allow Jenkins to fetch the pipeline script (known as a Jenkinsfile) from your Source Code Management (SCM) system of choice (e.g., Git, SVN).

   After selecting the SCM option, you will need to provide the Repository URL and the path to the Jenkinsfile in your repository. If your repository is private, you'll need to add credentials. In this guide, we will be using a Jenkinsfile stored in the SCM.

5. **Save the Pipeline**: Click 'Save' to finish setting up your pipeline.

Your pipeline job is now created and ready to run. You can execute the pipeline by clicking 'Build Now' from the pipeline view.

For more detailed information and advanced options, you can refer to the official Jenkins Pipeline [documentation](https://www.jenkins.io/doc/book/pipeline/).


## Integrating Terraform with Jenkins

1. **Using Environment Variables**: Control Terraform's behavior within Jenkins by setting environment variables in the Jenkins job configuration. For instance, `TF_CLI_ARGS` can be used to specify command-line arguments for Terraform.

2. **Pipeline Job**: Utilize the `sh` step in your Jenkinsfile to execute Terraform commands. These often include `terraform init`, `terraform plan`, and `terraform apply`.


## End To End Example

The following Jenkinsfile is a minimal example 


```groovy
pipeline {
    agent any

    environment {
        TF_CLI_ARGS_init = "-input=false"
        TF_CLI_ARGS_apply = "-input=false -auto-approve"
    }

    stages {
        stage('Checkout') {
            steps {
                git 'git@github.com:YOUR_USER_PROJ/helloworld-golang.git'
            }
        }
        
        stage('Build Go App') {
            steps {
                sh 'mkdir -p ${GOPATH}/src/github.com/YOUR_USER_PROJ'
                sh 'cp -R helloworld-golang/ ${GOPATH}/src/github.com/YOUR_USER_PROJ'
                sh 'go build -o myapp ${GOPATH}/src/github.com/YOUR_USER_PROJ/helloworld-golang/main.go'
            }
        }        

        stage('Terraform Init') {
            steps {
                sh 'terraform -chdir=terraform init'
                sh 'pwd'
                sh 'ls -lah'
            }
        }

        stage('Terraform Plan') {
            steps {
                sh 'terraform -chdir=terraform plan'
            }
        }

        stage('Terraform Apply') {
            steps {
                sh 'terraform -chdir=terraform apply'
                sh 'terraform output machine_ip > ip.txt'
            }
        }

        stage('Deploy App') {
            steps {
                script {
                    def ip = readFile('ip.txt').trim()
                    sh "scp myapp user@${ip}:/path/to/deploy"
                }
            }
        }

        stage('Post-deployment actions') {
            steps {
                sh 'terraform -chdir=terraform destroy -auto-approve'
                // Add any post-deployment actions here, e.g., Slack notifications
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
```
## Understanding the Jenkinsfile

Here's a breakdown of the Jenkinsfile used in this setup:

The provided Jenkinsfile consists of the following stages:

### `agent any`

This line means that Jenkins can run the pipeline on any available agent.

### `environment`

This block is used to define environment variables for the use in this pipeline. `TF_CLI_ARGS_init` and `TF_CLI_ARGS_apply` are used to prevent interactive approval in the terminal during the Terraform stages.

### `stages`

This is the heart of the pipeline and contains multiple stages:

- `Checkout`: The code is checked out from the provided GitHub repository using the `git` command.

- `Terraform Init`: This stage runs the `terraform init` command inside the `terraform` directory of your checked-out code. The `pwd` and `ls -lah` commands are used to print the working directory and list all files. This is useful for debugging.

- `Terraform Plan`: Runs the `terraform plan` command inside the `terraform` directory to generate an execution plan.

- `Terraform Apply`: Applies the execution plan generated in the previous stage using the `terraform apply` command.

- `Post-deployment actions`: Any post-deployment steps are executed here. Currently, it destroys the created resources using the `terraform destroy -auto-approve` command.

### `post`

This block is executed after all the stages have run. The `cleanWs()` command is used to clean the workspace after the job is done, irrespective of whether the job succeeded or failed.


## Troubleshooting

If you encounter issues while setting up or running your Jenkins pipeline with the Digital Rebar Terraform provider, try the following steps:

1. **Review Jenkins Build Logs:** 

   The first place to look when a build fails is the console output produced by Jenkins for that build. Here you will find detailed logs about what Jenkins did, information from your `Jenkinsfile` execution, as well as output from Terraform commands. Look for any error messages or stack traces that may indicate what went wrong.

2. **Check Terraform Configuration:**

   Make sure your Terraform files (`*.tf`) do not have syntax errors. You can check for syntax errors locally by running `terraform validate` in the directory that contains your Terraform files.

3. **Validate DRP API Access:**

   If the Terraform steps are failing, ensure that Terraform can communicate with the DRP API. Check the DRP API URL, and the credentials being used for any typos or errors. You may test the connectivity by performing a simple curl command to the DRP API endpoint from your Jenkins server, or using drpcli.

4. **Ensure Adequate Resources in DRP:**

   If your `terraform apply` fails with errors about not being able to allocate resources, it might be due to DRP not having sufficient resources to fulfill the request. Ensure that you have sufficient resources (like machines, IP addresses, etc.) available in your DRP.

5. **Examine the state of resources in DRP:**

   If your Terraform configuration is trying to manipulate a resource that doesn't exist or is in an unexpected state, Terraform might fail. Use the DRP web interface or API to examine the current state of the resources your Terraform configuration is trying to use.

Remember, always verify the changes made by Terraform plan before proceeding with Terraform apply. This will give you an overview of the actions Terraform will perform and can help you spot potential issues before they occur.


---
title: DRP Architectural Concepts
tags:
  - explanation
  - architecture
---

# DRP Concepts {#rs_arch_concepts}

Digital Rebar Provision provides additional functional elements that implement the system of control and orchestration.

* Runners
* Clusters
* Batches

Represent additional functions that act as building blocks for features that the operator uses.


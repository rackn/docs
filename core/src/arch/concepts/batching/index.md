---
title: Batching Usage Architecture
tags:
  - explanation
  - architecture
---

# Batching Usage Architecture {#rs_arch_usage_batching}

This section describes the architecture and components used in batching.

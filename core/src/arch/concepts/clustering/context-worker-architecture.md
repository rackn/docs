---
title: Context Worker Architecture
tags:
  - explanation
  - architecture
---

# Context Worker Architecture {#rs_context_worker_architecture}

The following article discusses the architecture behind the creation of a cluster of context workers.

The DRP Endpoint possesses the capacity to run and manage contexts for machines, clusters, and resource-brokers with the machine's Context field.
This enables the transfer of the task execution environment from the runner on the machine to a container operating on the DRP Endpoint.
For clusters and resource-brokers, these are virtual entities that only exist as container-backed machines.

The `docker-context` plugin provider handles this feature in the DRP endpoint. It monitors machine events to create, update, or delete these contexts.
However, the functionality remains until the desired number of containers surpasses the specifications of the DRP Endpoint.

During normal operation and usage, the ephemeral container used for temporarily executing a set of tasks for a machine does not pose a burden on the DRP Endpoint. Yet, this changes with the concept of long-lasting worker pools, which cater to use cases like status monitoring, periodic audits, or batch operations.

Specific examples include:

- A pool of context workers handling external service integrations to limit or queue actions and reduce burst load on the service.
- A pool of context workers dealing with ongoing periodic daily out-of-band analysis, such as hardware audit to detect non-compliance.
- A pool of context workers managing distributed CI/CD operations (batch integration tests against multiple systems).

These context workers may impose a load that exceeds the DRP Endpoint's capacity.

## Context Worker Nodes

With the cluster construct, a set of machines (virtual or physical) can be configured to run the `docker-context` plugin as a
service controlling Docker or Podman. The `docker-context` plugin is launched with an identifier (usually the machine's name).
If built from a cluster, it is typically based on the cluster's name.

The `docker-context` plugin monitors the same machine events as the original `docker-context` plugin. When it detects a machine, cluster,
or resource-broker event with the `docker-context/owner` parameter set to its name, it will handle that request. The default `docker-context`
operating in the DRP endpoint is named `docker-context` for self-identification.

## Context Worker Node Sets

A Context Worker Node can simultaneously provide contexts for multiple use cases. The only requirement is that the Machine,
Cluster, or Resource Broker has the `docker-context/owner` set to the name of a Context Worker Node. In addition, it would be
beneficial to define sets of context worker nodes to ensure operational resources and manage congestion. To facilitate this,
each context worker node features a `docker-context/tags` parameter.

The `docker-context/tags` parameter is a list of strings that signify which sets of context work nodes should consider this
node for execution. While this doesn't affect the `docker-context` plugin, the setup of a worker must have some tags for scheduling.
The `context-broker` will use this for scheduling.

## Context Broker Update

To enable a cluster to create context workers across a set of nodes, the context broker, `context-broker`, has been updated to
accommodate an additional parameter, `docker-context/tag`. When the cluster is requesting machines, the `docker-context/tag`
is employed to select the set of Context Worker Nodes for scheduling the requested context workers.

If `docker-context/tag` is not set, the default `docker-context` operating on the DRP Endpoint will be used. Otherwise,
all the machines with the matching tag and a `docker-context/member-name` parameter set to its own name will be used to
distribute the requested machines. The `docker-context/member-name` is used as a live check to ensure the docker-context is running.

## Building a Cluster of Context Workers

When creating a cluster, the cluster should be configured with at least the following parameters:

- `resource/broker`: context-broker (or a broker that follows these semantics)
- `docker-context/tag`: a tag to indicate which set of nodes will host the workers
- `cluster/count`: used to define the number of workers

Furthermore, the cluster should run a pipeline on each member or a task after the creation of the machine to put the machines in work order mode.

These parameters will guide the creation of context workers across the system.

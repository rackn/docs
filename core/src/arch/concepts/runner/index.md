---
title: DRP Runner Architectural Design
tags:
  - explanation
  - architecture
  - runner
---

# DRP Runner Architectural Design {#rs_arch_runner}

This section describes the ways in which work can get done.  We will discuss task execution, ways to run tasks, and how tasks are tracked.  Topics include:

* Types of Execution Environments
* Pipelines/Workflows and Work Orders
* What is a machine?

![runner diagram](runner.svg "Runner Architecture")
[comment]: <> (The source is at: https://docs.google.com/drawings/d/1yrpBfifGtoDIN1Ym26aZ0wcOo11tRIkB0mbVWXCYwC4/edit)

The Runner (or Agent) is the engine of computation in Digital Rebar Provision.  The agent can run in many different locations for different purposes.

!!! note

    Runners are currently implemented as:

    * `drpcli` running the `processjobs` action
    * a python-based runner called `drpy` for the ESXi environment.

!!! note

    `drpcli` is supported on many platforms and architectures.

    * linux/amd64
    * darwin/amd64
    * windows/amd64
    * linux/arm64
    * darwin/arm64
    * linux/ppc64le


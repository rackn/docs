---
title: DRP Runner Mode
tags:
  - explanation
  - architecture
  - runner
---

This section describes the two modes of execution of the Runner and how they provide different types of execution.

![Runner Modes](ww_compare.svg "Runner Modes")
[comment]: <> (The source is at: https://docs.google.com/drawings/d/1ugmPnRn3zkHAvu4fuhWzDrwUnKvDDd7TivS8hY0jEYk/edit)


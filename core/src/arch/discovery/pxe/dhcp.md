---
title: DHCP Architecture
tags:
- explanation
- architecture
---

# DHCP Architecture {#rs_dhcp_arch}

The DRP Endpoint contains a DHCP server.  The DHCP server follows the RFCs with one exception.
The DRP server will use the MAC address of the requests to track clients instead of the client ID.
This allows the DRP endpoint track multiple bootloaders and installers operating on the same machine
without wasting multiple IP addresses.

The DHCP server listens on the DHCP server port on all interfaces.  No packet is actually processed
until a subnet is configured.

Once a subnet is configured, the DHCP server will process the packets to respond with addresses.
The Subnet has multiple configuration operations.  It can define a network range, but require
reservations for the system to hand out addresses.  The Subnet can also be configured to Proxy
DHCP which does not assign addresses, but passes out options for booting.

The Subnet allows for profiles and parameters to be associated with machines that
have been handed out from that range.

The Lease Object tracks the addresses assigned to machines.  The Lease Object tracks
expiration times and states.  When an address is to be assigned, the Lease Object is
also assigned to the machine.

The DHCP server also issues events when addresses are assigned.  The IPMI scan system uses this
event to discover BMCs that are getting addresses through DHCP.

The Reservation Object can be used to bind an IP address to a MAC address.  The Reservation Object
does not require a Subnet to deliver an address.  The Reservation Object allows for profiles
and parameters to be assocatied with the machine matching the MAC address.

Expired leases will be held for returning machines, but can be recycled if the address pool
runs out of free addresses.


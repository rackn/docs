---
title: External DHCP Architecture
tags:
- explanation
- architecture
---

# External DHCP Architecture {#rs_ext_dhcp_arch}

The DRP Endpoint DHCP server can be turned off.  The DRP Endpoint can be still manage machine
state by having the external DHCP server use the DRP server as the next boot server.

This requires that iPXE be used and that the bootfile served to iPXE is `default.ipxe`.  The
`default.ipxe` file is a special iPXE configuration file that will generate machine specific
boot options.

See [](#rs_op_dep_config_ext_dhcp) for configuration details.


---
title: IPMI Scanning
tags:
  - explanation
  - architecture
---

# IPMI Scanning {#rs_arch_dep_ipmi_scanning}

IPMI Scanning is a system provided by the IPMI plugin.  The plugin registers the IPMI Scan Result
object that tracks results of an IPMI scan.

A scan is triggered by the creation of an IPMI Scan object with a range of IP addresses.
The scan uses credentials configured on the IPMI plugin to test to see if redfish can be
used to log into BMC.  Upon successful login, a Machine is created with the discovered
information.  Additionally, the machine is recorded upon the IPMI scan result object.

Additional triggers can be created that will power on or pxe boot the machine upon discovery.


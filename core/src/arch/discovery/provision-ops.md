---
title: Discovery and Provisioning Operations
tags:
- explanation
- architecture
---

# Discovery and Provisioning Operations {#rs_image_deploy_arch}

PXE Provisioning requires hand-offs between multiple services and the nodes. Generally, these flows start with network
discovery (DHCP)
providing a "next boot" instruction to the server's pre-execution environment (PXE) that can download a very limited
operating system using a very simple file transfer protocol (TFTP). That next operating system can then take more
complex actions including using HTTP to download further files.

In the flow graphics, green steps indicate user actions.

## Automated Discovery {#automated-discovery}

The automated discovery flow uses the specialized Discovery and Sledgehammer Boot Environments (BootEnvs). These
BootEnvs work with the default next boot to self-register machines with Digital Rebar Provision using the Machines API.

This recommended process allows operators to start with minimal information about the environment, so they can
[install an OS](#os-installation). Without discovery, operators must create machines in advance following the
[manual registration](#manual-registration) process.

![](images#boot_discover.png)

## Manual Registration {#manual-registration}

!!! note

    This workflow requires users to know their machine information in advance.

    This is the typical workflow for Cobbler users in which machine definitions are determined in advance using
    spreadsheets, stone tablets, Ouija boards or other 1990s data center inventory management techniques.

    Two advantages to this approach is that it skips the discovery boot process; consequently, it is faster and can be run
    without DHCP.

![](images#install_known.png)

## OS Installation {#os-installation}

Once systems have been discovered using [automated discovery](#automated-discovery) or registered
using [manual registration](#manual-registration), their machine BootEnv can be set to an O/S installation image. The
recommended BootEnvs for these images includes an automatic machines API call that sets the machine to boot to local
disk (BootEnv = local).

![](images#install_discovered.png)

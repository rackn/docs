---
title: Discovery
tags:
  - reference
  - architecture
---

# Discovery {#rs_machine_discovery}
There are several ways a system is discovered.  This document provides descriptions of how each of these occur.

!!! note
    For this document, the use of "system" and "machine" are not interchangeable. The term "system" is used to refer to the physical system, VM or container. The "machine" is the object stored on the DRP endpoint and describes the system being discovered.

## PXE Boot
The typical way a machine is discovered is by PXE booting to the sledgehammer boot environment and running tasks to identify the machine before running detailed inventory, classification and validation before driving the machine through application pipelines.

### Overview
1. The system is powered on and the BIOS or EFI is configured to boot from the network.
    1. The system and endpoint exchange DHCP [DORA](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol#Operation).
    1. A combination of TFTP and/or HTTP services on the endpoint along with preferences that set default workflow, stage, bootenv and unknown bootenv help drive decisions on what is served to the system.  When installing DRP on the endpoint using  `--universal`, the install sets the default preferences in a way that drives the system to use the discovery bootenv which includes the sledgehammer boot files.
1. If the machine is known to the endpoint, the machine's bootenv is evaluated. 
    * If it is not set, the endpoint's default bootenv preference is used.
    * If the machine is not known to the endpoint, the endpoint's unknown bootenv preference is used to serve the correct files.
1. The machine PXE chainloads from the endpoint or gets files from http depending on the capabilities.
1. Once booted to sledgehammer, an agent is brought up and the join-up.sh script is run.
1. The join-up.sh script gathers information such as known UUID, memory, Cloud ID,serial number, mac addresses, and other system information, it then passes the information to the endpoint.
1. The endpoint reviews what it knows of hardware, MACs, generates a score, and either returns that the system is known or not by providing the machine object including the appropriate API token if found.
    * If the system is not known, the join-up.sh script attempts to create the machine.
1. After the join-up.sh script completes, the agent is updated, configured and started on the system.  The agent is also called the machine runner.

## Universal Discovery Workflow {#arch_discovery_universal_workflow}
From here, discovery is similar for all methods.

### Overview
1. The endpoint updates workflow, stages, tasks, and templates to run through the universal-discover workflow as defined by the default workflow preference.
1. The machine runner takes offered files from endpoint file server, stages them locally and runs through them in the order defined on the machine object's tasklist.  For the universal-discover workflow, the following occurs at a high level.
    1. start-callback
        * Interact with external services using basic API calls before the discover process is about to begin.
    1. pre-flexiflow
        * Inject tasks prior to discovery.
    1. inventory
        * Discover detailed information on the system including firmware, bios, raid, network and chassis information.
    1. classify
        * Use the rack plugin or custom query to help determine what the system is and how to drive the machine further towards end-state.
    1. post-flexiflow
        * inject tasks after discovery.
    1. validation
        * Validate the system using custom scripts.  Currently, does nothing.
    1. complete-callback
        * Interact with external services using basic API after the discover process is complete.
    1. workflow-chain
        * Notify the endpoint to continue to the next, defined workflow.

## Read More

### Architectural Documentation
* [Provisioning Architecture](#rs_arch_usage_provisioning)

### Operational Documentation
* [Configuring the Server](#rs_server_configure)
* [IPMI Machine Discovery](#rs_ipmi_machine_discovery)
* [IPMI Plugin Operations Documentation](#rs_ipmi_usage)
* [Preference Setting](#rs_preference_setting)

### Reference Documentation
* [Whoami](https://docs.rackn.io/stable/operators/whoami/#fingerprint-match-criteria)

---
title: Deployment Architectural Design
tags:
  - explanation
  - architecture
---

# High Availability {#rs_arch_ha}

This section will describe the concepts of High Availablity

* General Architecture of HA System
* Availability Zones

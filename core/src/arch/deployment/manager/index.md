---
title: Manager Architecture
tags:
- explanation
- architecture
---

# Manager Architecture {#rs_arch_manager}

Digital Rebar Provision provides a mechanism to manage multiple endpoints
using a single pane of glass approach.

This section will focus on the architecture of the manager system. The
operational methods can be found at [Manager Operations](#rs_manager_ops).

Manager provide two basic functions:

- Single pane of glass-like aggregated API operations
- Aggregated management of additional Digital Rebar Provision endpoints.

By registering the endpoints with the manager, the manager provides a
set of API endpoints that aggregate the machines from all those
endpoints. This allows filtering and operating on machines (and other
objects) across multiple endpoints from the one API target.

Additionally, the manager can act as an update and configuration
controller for the attached endpoints. Using Infrastructure as Code
(IaC) methods, the configuration of downstream endpoints can be defined
and automated. The manager can also maintain and provide a catalog of
resources to install and use.

## Manager Hierarchy

The Manager system allows for chained endpoints with multiple levels
of management. The Endpoint object defines what endpoints the
Manager manages.

Once an Endpoint object is created in the Manager, the
Manager will use the streaming synchronous replication system to
maintain a local instance of the endpoint's data to answer read operations.
All write and action operations will be forwarded to the owning endpoint.

Because of the replication of objects, a Manager registered to a
Manager will allow the top-level manager to control the endpoints
attached to the downstream manager. All communication will be funnelled
through the manager to allow for networking isolation or separation.

For example, if we have 7 Endpoints, A, B, C, D, R1, R2, and M1. A, B,
C, D are leaf endpoints managing machines. We have two regional endpoint managers,
R1 and R2. A and B are managed by R1 and C and D are managed
by R2. Manager M1 is managing R1 and R2.

``` mermaid
graph TD
  M[M1] --> L[R1];
  M --> R[R2];
  L --> A[A];
  L --> B[B];
  R --> C[C];
  R --> D[D];
```

In this example, R1 has Endpoint objects for A and B. R2 has
Endpoint objects for C and D. M1 has Endpoint objects for R1 and
R2.

Because of the object replication, M1 will see the Endpoint objects
for A, B, C, and D from R1 and R2. This will enable those Endpoints
to be operated on by M1 (both single pane of glass and configuration).

M1 will present all Machine objects from all endpoints. R1 will
present Machine objects from A, B, and R1. R2 will presents
Machine objects from C, D, and R2.

## Manager Functions

The two main functions of manager are discussed here.

### Single Pane of Glass

When making API requests, the manager will provide results from all the
attached endpoints. Additionally, all objects have a field,
Endpoint, that is populated with the Endpoint Id (the High
Availability Id) of the owning endpoint. API requests made to objects
from attached endpoints are automatically forward to the correct
endpoint. See [API Proxy](#rs_api_multisite_proxy_forwarding) for more
information about using this automatic API behavior explicitly.

Only the local objects are replicated up to the manager, objects
provided by content packages and plugins are not replicated to the
manager. It is assumed that the manager will have all the content
packages and plugins loaded to resolve dependencies and parameters.

### Configuration Management

The manager system adds a new object, VersionSet, that allows for
the definition of configuration, content packages, plugins, files, and
isos in a defined versioned set.

Applying a list of VersionSets to an Endpoint will cause the manager to
ensure that the state defined by the version sets is applied to the
system.

Manager works with the High Availability system. A set of DRP endpoints
in an HA cluster are operated on as a single endpoint. The configuration
is replicated across all cluster members. The only exception is the
version of DRP itself. The UX provides mechanisms to update the cluster
members version of DRP. Once DRP is updated, the High Availability
system will replicate remaining content.

The Manager uses a local catalog to push configuration elements to the
downstream endpoints. The catalog can be built with CLI commands. The
local catalog can also be extended to contain custom user content. These
can be reference by version sets to push out to endpoints.

## Manager Configuration

The following sections define the implementation details of the Manager
system.

Any DRP Endpoint with a license with the manager feature can become a
manager. Whether an endpoint operates as a manager is controlled by the
preference, *manager*. Setting this preference to *true* will allow
the endpoint to operate as a manager. Setting it to *false* (the
default) will cause the Manager functions to stop. VersionSet and
Endpoint Objects will be maintained while not acting as a manager.

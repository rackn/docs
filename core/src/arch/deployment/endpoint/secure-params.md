---
title: Secure Parameter Architecture
tags:
  - explanation
  - architecture
---

# Secure Parameter Architecture {#rs_arch_dep_endpoint_secure_params}

Secure parameters store sentivie data.  A secure parameter must be defined
with a Parameter object within DRP.  The Parameter object defined the type
of data and if the parameter is secure.

## Internal Secure Parameters

The default internal secure parameter is stored internally by encrypting
the data with multiple keys in the system.

The CLI, UX, or API can be used to set a secure parameter.  The secure parameter
will be encrypted if not already presented as an encrypted object.  Regardless of
the type of the paremter, the resulting stored object will look like this:

```json
{
  "Key": "zUr2oqpK2zS/ie58pr7L1V6Kl3GEAdiC6ajZsWFqqlw=",
  "Nonce": "4i4Dp8bqRStwTBD1R0OJd585mqxDut9a",
  "Payload": "BwmxVoc1A23bRxUKYz0x1mU3kv4T"
}
```

To decrypt a secure parameter, the user or token must have the `getSecure` object action.

To store a value in a secure parameter, the user or token must have the `updateSecure` object action.

## External Secure Parameters

The DRP Endpoint also can defer resolution of the secure parameter to an external source.  The
source is provided by a plugin that registers a `decrypt` action.

The `decrypt` action requires a parameter provided on the action.  The `decrypt/lookup-uri` parameter
is provided as input to the plugin.  Additional plugin parameters may be sent as requested by the
`decrypt` registration data in the plugin.  These additional parameters are provided by the Plugin
object as configuration.

The DRP Endpoint will use the data in the object stored in the Parameter.  The key, `LookupUri`,
defines the lookup URI that the plugin will use to find the actual value.  Each plugin can
define different parameters, but the general format is the same.

Within the `LookupUri` value, the string may also contain template expansions that are valid
for machines.  This allows for the secure parameters to reference actual machine values.

!!! example

    This example shows a vault plugin URI that get the key `foo` from the object at path `hello`.

    ```json
    {
      "LookupUri": "vault://foo?path=hello"
    }
    ```


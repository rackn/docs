---
title: Test Automation
tags:
  - reference
  - developer
---

# Test Automation

## Airgap

Test automation for airgap installation using a cluster to spin up multiple machines to do airgap install on, then destroying the machines.  The goal is to provide reusable bits to provide testing of other components.

```d2 layout="elk"
...@docs/assets/classes/all

endpoint: {
    label: endpoint
    trigger -> work_order: blueprint
    trigger.class: trigger
    work_order.class: work_order
}

endpoint.work_order -> self.work_order

self: {
    label: self-runner
    work_order -> create-cluster
    work_order.class: work_order
    create-cluster.class: task
}

contract: {
    shape: class
    test-profile: the-profile
    more-profiles: ...
    test-param: this-param
    more-params: ...
}

contract -> self.work_order

self.create-cluster -> cluster.create-cluster

cluster: {
    label: cluster context
    create-cluster
    create-cluster -> validate_results: work_order
    validate_results -> report
    report -> cleanup
    cleanup -> destroy: worfklow
    destroy -> timeout
}

cluster.destroy -> rb_context.rb_work_order

universal-application-base-cluster: {
    shape: class
    params: things
}

universal-application-base-cluster -> cluster.create-cluster

cluster.cluster_provision -> rb_context.rb_work_order

rb_context: {
    label: resource_broker context
    rb_work_order -> broker_provision: work_order mode
    broker_provision -> machine: terraform
    rb_work_order.class: workorder
    machine: {
        label: machine1...n
        style: {
            multiple: true
        }
    }
}

rb_context.machine -> machine_context.airgap_test

machine_context: {
    label: machine1 context
    airgap_test -> pipeline: update params
    pipeline -> workflow_complete 
}

machine_context.workflow_complete -> cluster.workflow
```

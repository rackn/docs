---
title: How Authentication Works
tags:
  - explanation
  - architecture
---

# How Authentication Works {#rs_how_auth_works}

The authentication system uses the User, Role, and Tenant objects to
define a multi-tenant RBAC system.

## User Tokens {#rs_auth_user_tokens}

User tokens are created by accessing `GET /api/v3/users/:username/token`.
By default, a token created
using this method can act on behalf of user. This includes rights for
the user to get information about themself, change their password, and
fetch a token for themselves, and any access granted by additional roles
the user has. Users can restrict access that a token generated in this
method by passing an optional comma-separated list of Roles as a
parameter during the API request, and any requested Roles that would
increase the scope of the allowed Claims will be silently dropped.

## Machine Tokens {#rs_auth_machine_tokesn}

Certain common machine usage patterns (discovery, running tasks, etc.)
also need to interact with the API, and hence need a Token that
authorizes them to perform those actions. These tokens have a fixed set
of permissions:

-   Machine Discovery: This token has the ability to create and get
    Machines, and nothing else. It is needed to allow Sledgehammer to
    create a machine for itself during initial system discovery.
-   Machine Operations: This token gives a Machine the ability to modify
    itself, get stages and tasks, create events, create a reservation
    and modify a reservation for the machine's address, and create and
    manage Jobs for itself.

These machine tokens are generated as part of template expansion via the
`.GenerateToken` command (which generates tokens that expire according to
the unknownTokenTimeout and knownTokenTimeout preferences), and the
`.GenerateInfiniteToken` command, which generates a Machine Operations
token that expires in 3 years and is intended to grant long-term access
for the task runner. These tokens cannot be generated by any other
means.

## How Tokens Are Checked {#rs_auth_token_checking}

1.  A request is made to the API. If the request contains
    `Authorization: Bearer`, that token is used. If the
    request contains `Authorization: Basic`, the contained
    username/password is checked and used to create a one-use Token.
2.  Claims are created based on the API path requested and the HTTP
    method. For example:
    - `GET /api/v3/users` creates a Claim of `{"Scope":"users","Action":"list","Specific":"*"}`
    - `GET /api/v3/users/bob` creates a Claim of `{"Scope":"users","Action":"get","Specific":"bob"}`
    - `PATCH /api/v3/bootenvs/fred` that wants to update OS.Name and OS.IsoName creates a Claim of
```json
[
    {"Scope":"bootenvs","Action":"update:OS.Name","Specific":"fred"},
    {"Scope":"bootenvs","Action":"update:OS.IsoName","Specific":"fred"}
]
```
3.  The token is checked to make sure it is still valid based on the
    system Secret, the user Secret, and the grantor Secret. If any of
    these have changed, or the token has expired, the API will return
    a 403.
4.  The list of created Claims is tested to see if it is contained by
    any one of the Roles contained in the Token, or by any direct Claims
    contained in the Token. If all of the created Claims are satisfied,
    the request is considered to be authorized, otherwise the API will
    return a 403.
5.  The API carries out the request and returns an appropriate response.

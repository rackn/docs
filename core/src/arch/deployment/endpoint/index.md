---
title: DRP Endpoint Architectural Design
tags:
  - explanation
  - architecture
---

# DRP Endpoint Architectural Design {#rs_arch_deployment_endpoint}

!!! note
    TODO:: Add Endpoint Picture here

This section will describe the core concepts of the Endpoint.  Topics include:

* Secure Parameter Architecture

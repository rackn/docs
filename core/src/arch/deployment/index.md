---
title: Deployment Architectural Design
tags:
  - explanation
  - architecture
---

# Deployment Architecture Design {#rs_arch_deployment}

This section will describe how the core concepts are organized and deployed.  Topics include:

* General Endpoint Components
* Multi-site Management - multiple data centers, edge locations, and regions in highly available configurations.
* High-Availablity - Reduce outage windows

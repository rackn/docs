---
title: Architectural Guide
tags:
  - explanation
  - architecture
---

# Architectural Guide {#rs_arch}

Digital Rebar Provision is a flexible infrastructure automation platform that can be examined from multiple perspectives. In this document, we'll explore a few different ways of looking at the architecture of the system.  we will also explore common usage patterns and how the various elements enable that usage.

![Digital Rebar Provision Core Architecture](high_core.svg "Core Architecture")

The diagram above illustrates the core architecture of Digital Rebar Provision. The following sections will provide a more detailed explanation of each component.

## The "What"

Digital Rebar Provision is designed around the concept of Infrastructure as Code (IaC), which means that the system is driven by object definitions that describe the desired state of the infrastructure being managed. These object definitions are provided by Plugin Providers and Content Packs, which can be thought of as the building blocks that define the "What" of the system.

## The "How"

Endpoints provide an environment for tracking the state of the infrastructure being built. They enable Digital Rebar Provision to determine "How" to provision the infrastructure based on the desired state defined by the Plugin Providers and Content Packs.

## The "Where"

Runners are the engines of execution for Digital Rebar Provision. They can be run in multiple locations to cover a diverse set of infrastructure requirements. This makes it possible to use Digital Rebar Provision in a variety of environments, from local development environments to large-scale production deployments.

## The "When"

Digital Rebar Provision includes a variety of control interfaces that allow for zero-touch operations and user interactions. Timers, events, and webhooks can be used to trigger actions automatically, while the API interfaces enable users to interact with the system directly.

By understanding these core concepts, you can gain a deeper appreciation for the flexibility and power of Digital Rebar Provision.

!!! note

    The documentation is split into 5 sections.

    * [Geting Started](#rs_quickstart)
    * [Architectural Guide](#rs_arch)
    * [Developer Guide](#rs_dev)
    * [Operator Guide](#rs_ops)
    * [Resources](#rs_res)


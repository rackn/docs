---
title: Burnin
tags:
  - reference
  - developer
---

## Burnin

This diagram provides a high-level description of burnin.

```d2 layout="elk"
...@docs/assets/classes/all
machine: |md
    ### Assumptions
    * Machine is in Sledgehammer
    * Can only run in machine context
    * Cloudy machines will skip
    * Runs in most pipelines.
    * Not run in universal-start
|

runner: {
    class: runner

    universal-discover.class: workflow

    universal-discover: {
        direction: down
        start-callback.class: task
        pre-flexiflow.class: task
        burnin.class: task
        burnin-reboot.class: task
        post-flexiflow.class: task
        classify.class: task
        validation.class: task
        complete-callback.class: task
        workflow-chain.class: task

        start-callback -> pre-flexiflow -> burnin -> burnin-reboot -> post-flexiflow -> classify -> validation -> complete-callback -> workflow-chain
    }
}

machine -- runner

burnin-notes: |md
    * works like validation and flexiflow
    * can replace tasks
    * has simple cpu, disk and memory tests
    * Sets skip-only done once
|

burnin-notes -- runner.universal-discover.burnin

burnin-reboot-notes: |md
    * counted reboot test, that's it.
    * sets skip-only done once.
|

burnin-reboot-notes -- runner.universal-discover.burnin-reboot
```

---
title: Provisioning Usage Architecture
tags:
  - explanation
  - architecture
---

# Provisioning Usage Architecture {#rs_arch_usage_provisioning}

This section describes the architecture and components used in provisioning.


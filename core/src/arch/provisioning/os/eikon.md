---
title: Eikon Architecture
tags:
  - explanation
  - architecture
---

# Eikon Architecture {#rs_arch_eikon}

Eikon is a storage and image deployment orchestrator designed to automate and simplify operating system deployments. By
managing storage layouts, RAID, LVM, and boot configurations, Eikon streamlines complex deployment processes into a
declarative, repeatable workflow.

## Core Features

1. **Storage Layout Management**:
    - Handles disk partitioning, software RAID (using `mdadm`), and LVM setup.
    - Supports advanced configurations including custom filesystems and multi-device setups.

2. **Image Deployment**:
    - Supports both raw image and root filesystem archive deployments.
    - Preserves metadata, security attributes, and extended filesystems during rootfs extraction.

3. **Boot Configuration**:
    - Automates bootloader setup for UEFI and legacy systems.
    - Supports RHEL and Debian-based distributions.

## Eikon Workflow

Eikon follows a structured deployment process that handles different operating systems paths. The workflow begins with
system discovery and boots into the Sledgehammer environment. After basic preparation and disk configuration, it
evaluates the target OS type and follows one of three paths:

1. RHEL-like Systems (RHEL/Alma/Rocky):
    - Deploys and mounts image
    - Prepares and configures chroot environment
    - Sets up bootloader
    - Configures cloud-init

2. Debian-like Systems (Debian/Ubuntu):
    - Similar to RHEL path but with distribution-specific bootloader handling
    - Manages filesystem and initialization configurations

3. Unknown/Windows Systems:
    - Simplified path focusing on basic configuration
    - Handles cloud-init setup and system probing

All paths converge at completion stage before final boot into the local environment.

```mermaid
graph TD
    Start([DRP Workflow: eikon-image-deploy]) --> A{{"stage: discover"}}
    A --> B[Boot to Sledgehammer]
    B --> C{{"stage: eikon-image-deploy"}}
    C --> D[eikon-prep]
    D --> E[erase-hard-disks]
    E --> F[eikon-image-deploy]
    F --> G[eikon-mount]
    G --> H[eikon-update-inventory]
    H --> I[eikon-evaluate-target]
    I -->|RHEL/Alma/Rocky| R[RHEL Path Tasks]
    I -->|Debian/Ubuntu| D1[Debian Path Tasks]
    I -->|Unknown/Windows| U[Unknown OS Path Tasks]
    R --> R1[eikon-prep-chroot]
    R1 --> R2[context:unmanaged-chroot-agent]
    R2 --> R3[eikon-chroot-evaluate]
    R3 --> R4[eikon-chroot-agent-install]
    R4 --> R5[eikon-chroot-security]
    R5 --> R6[eikon-chroot-rhelish-grub]
    R6 --> R7[eikon-chroot-fstab]
    R7 --> R8[eikon-chroot-initfs]
    R8 --> R9[eikon-pause]
    R9 --> R10[context:]
    R10 --> R11[eikon-stop-agent]
    R11 --> R12[eikon-cloud-init-config]
    R12 --> R13[eikon-umount]
    D1 --> D2[eikon-prep-chroot]
    D2 --> D3[context:unmanaged-chroot-agent]
    D3 --> D4[eikon-chroot-evaluate]
    D4 --> D5[eikon-chroot-agent-install]
    D5 --> D6[eikon-chroot-security]
    D6 --> D7[eikon-chroot-debianish-grub]
    D7 --> D8[eikon-chroot-fstab]
    D8 --> D9[eikon-chroot-initfs]
    D9 --> D10[eikon-pause]
    D10 --> D11[context:]
    D11 --> D12[eikon-stop-agent]
    D12 --> D13[eikon-cloud-init-config]
    D13 --> D14[eikon-umount]
    U --> U1[eikon-cloud-init-config]
    U1 --> U2[eikon-mount-probe]
    U2 --> U3[eikon-pause]
    U3 --> U4[eikon-umount]
    R13 --> K{{"stage: complete"}}
    D14 --> K
    U4 --> K
    K --> L[bootenv:local]
```

## Core Components

### Storage Manager

Manages:

* Physical disk operations (partitioning, formatting)
* Logical Volume Management (LVM)
* Software RAID configurations (mdadm)

When you configure your storage layout in an eikon/plan, the storage manager orchestrates all the necessary operations
in the correct order. For example, in a complex configuration with RAID and LVM, it knows to:

* Create physical partitions first
* Build RAID arrays if specified
* Initialize LVM physical volumes
* Create volume groups and logical volumes
* Create and mount filesystems

### Image Handler

The image handler supports two categories of image formats:

Raw Disk Images:

- Uncompressed (dd-raw)
- Compressed with gzip (dd-gz), bzip2 (dd-bz2), xz (dd-xz), zstd (dd-zst)
- Archived raw images: tar (dd-tar), tar+gzip (dd-tgz), tar+xz (dd-txz), tar+bzip2 (dd-tbz)

Filesystem Archives:

- Uncompressed tar with attribute preservation (tar)
- Compressed archives: tar+gzip (tgz), tar+xz (txz), tar+bzip2 (tbz), tar+zstd (tar-zst)
- All archive types preserve:
    - Numeric UIDs/GIDs
    - Extended attributes
    - ACLs
    - SELinux contexts

### Boot Manager

Responsible for:

- Configuring UEFI and Legacy boot modes.
- Installing and managing bootloaders for RHEL and Debian-based systems.

## Deployment Methods

Eikon provides two distinct ways to deploy systems:

1. Raw Image Deployment - Best for pre-configured systems requiring exact replication
2. Rootfs Archive Deployment - Best for systems requiring custom partition layouts or RAID/LVM configurations

### Raw Image Deployment

In this method, Eikon writes a complete system image directly to the target disk:

```yaml
eikon/plan:
  disks:
    - name: disk-0
      grub_device: true
      path: /dev/sda
      image:
        type: dd-zst
        url: "https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-raw.zst"
```

### Rootfs Archive Deployment

This method supports granular control over storage configurations:

```yaml
eikon/plan:
  disks:
    - name: disk1
      path: /dev/sda
      ptable: gpt
      grub_device: true
      partitions:
        - name: boot_efi    # EFI System Partition
          id: 1
          size: 1G
          fs:
            fstype: vfat
            mount: /boot/efi
        - name: root_part   # Root Partition
          id: 2
          size: 17G
          ptype: LVM2_member
  vgs:
    - name: vg1            # LVM Volume Group
      pvs:
        - root_part
      lvs:
        - name: root       # Root Logical Volume
          size: 14G
          fs:
            fstype: xfs
            mount: /
        - name: swap       # Swap Logical Volume
          size: 1G
          fs:
            fstype: swap
  images:
    - url: "https://eikon-images.s3.us-west-2.amazonaws.com/rhel9-uefi-rootfs.tar.zst"
      type: tar-zst
```

---
title: Image Deploy
tags:
  - reference
  - developer
---

# Image Deploy {#rs_arch_prov_os_image_deploy}

This diagram provides a high-level description of Image Deploy.

```d2 layout="elk"
...@docs/assets/classes/all

machine: |md
    ### Assumptions
    * Machine is in Sledgehammer
    * Assumes cloud-init is included in image.
    * Injects DRP binaries and configuration files for DRP and cloud-init.
    * Ends in image OS.
|

runner: {
    class: runner

    universal-discover.class: workflow

    universal-discover: {
        direction: down
        callback.class: task
        pre-flexiflow.class: task
        burnin.class: task
        burnin-reboot.class: task
        post-flexiflow.class: task
        classify.class: task
        validation.class: task
        callback.class: task
        workflow-chain.class: task
    }
}

machine -- runner

burnin-notes: |md
    * works like validation and flexiflow
    * can replace tasks
    * has simple cpu, disk and memory tests
    * Sets skip-only done once
|

burnin-notes -- runner.universal-discover.burnin

burnin-reboot-notes: |md
    * counted reboot test, that's it.
    * sets skip-only done once.
|

burnin-reboot-notes -- runner.universal-discover.burnin-reboot
```

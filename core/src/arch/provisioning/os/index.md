---
title: OS
tags:
  - explanation
  - architecture
---

# OS {#rs_arch_prov_os}

This section describes the architecture and components used in provisioning operating systems.


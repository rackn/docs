---
title: OpenShift Requirements
tags:
- openshift
- requirements
- prerequisites
- planning
---

# OpenShift Requirements {#rs_openshift_requirements}

Before deploying OpenShift using the Digital Rebar content pack, ensure your environment meets the following requirements.

## Hardware Requirements {#rs_openshift_hw_requirements}

### Control Plane Nodes {#rs_openshift_control_plane_hw}
- **Quantity**: 3 nodes (required for HA)
- **CPU**: 4 vCPUs minimum per node
- **RAM**: 16GB minimum per node
- **Storage**: 100GB minimum per node

### Worker Nodes {#rs_openshift_worker_hw}
- **Quantity**: 2 nodes minimum
- **CPU**: 2 vCPUs minimum per node
- **RAM**: 8GB minimum per node
- **Storage**: 100GB minimum per node

### Load Balancer Nodes {#rs_openshift_lb_hw}
- **Quantity**: 1 node minimum (2+ recommended for HA)
- **CPU**: 2 vCPUs minimum per node
- **RAM**: 4GB minimum per node
- **Storage**: 20GB minimum per node

## Network Requirements {#rs_openshift_network_requirements}

### Network Segments
All network segments must be unique and non-overlapping:

1. **Machine Network**: 172.21.0.0/20 (default)
    - Must be routable within your infrastructure
    - Used for node communication

2. **Service Network**: 172.30.0.0/16 (default)
    - Used internally by Kubernetes
    - Does not need to be routable

3. **Pod Network**: 10.128.0.0/14 (default)
    - Used for container networking
    - Managed by OVN-Kubernetes

### Required Ports {#rs_openshift_ports}

#### Load Balancer Ports
- 6443/tcp: Kubernetes API
- 22623/tcp: Machine Config Server
- 80/tcp: HTTP traffic
- 443/tcp: HTTPS traffic
- 8443/tcp: HTTPS traffic (used by some services)

#### Node Ports
- 2379-2380/tcp: etcd (control plane only)
- 10250/tcp: Kubelet
- 10257/tcp: kube-controller-manager
- 10259/tcp: kube-scheduler
- 9000-9999/tcp: Host level services
- 30000-32767/tcp: NodePort service range

## Software Requirements {#rs_openshift_sw_requirements}

### Red Hat Requirements {#rs_openshift_rh_requirements}
- Valid Red Hat OpenShift subscription
- Pull secret from [Red Hat OpenShift Cluster Manager](https://console.redhat.com/openshift/install/pull-secret)

### Digital Rebar Requirements {#rs_openshift_drp_requirements}
- Digital Rebar Platform (DRP) v4.14.0 or later
- DRP Community Content Bundle v4.14.0 or later
- Configured resource broker (typically pool-broker)

## DNS Requirements {#rs_openshift_dns_requirements}

Either use DRP-managed DNS (default) or configure external DNS with the following records:

| Record Type | Name | Value |
|------------|------|--------|
| A | api.$cluster_name.$base_domain | Load Balancer IP |
| A | api-int.$cluster_name.$base_domain | Load Balancer IP |
| A | *.apps.$cluster_name.$base_domain | Load Balancer IP |
| A | $node_name.$cluster_name.$base_domain | Node IP |

## Internet Access {#rs_openshift_internet_requirements}

### Connected Installation
- Access to Red Hat container registries
- Access to Red Hat update repositories
- Access to OpenShift mirror sites

### Disconnected Installation
- Local registry with mirrored OpenShift images
- Local package repository
- Configure using:
  - openshift/external-registry
  - openshift/external-registry-create
  - openshift/external-registry-update


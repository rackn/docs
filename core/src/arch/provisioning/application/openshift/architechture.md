---
title: OpenShift Architecture
tags:
- openshift
- architecture
- design
---

# OpenShift Architecture {#rs_openshift_architecture}

## Component Overview {#rs_openshift_components}

The OpenShift deployment consists of several key components:

1. **DRP Control Node**
    - Manages cluster deployment
    - Handles node provisioning
    - Coordinates cluster formation
    - Optional DNS management

2. **Load Balancers**
    - Entry point for cluster access
    - Routes API and application traffic
    - Handles high availability needs

3. **Bootstrap Node**
    - Temporary control plane
    - Initializes cluster configuration
    - Converts to worker node after initialization

4. **Control Plane Nodes**
    - Manage cluster state
    - Run etcd database
    - Handle API requests
    - Schedule workloads

5. **Worker Nodes**
    - Run application workloads
    - Scale based on demand
    - Handle pod networking
    - Provide compute resources

## Network Architecture {#rs_openshift_network}

Three distinct network ranges:

1. **Machine Network** (172.21.0.0/20)
    - Physical node addressing
    - Infrastructure services
    - Must be routable

2. **Service Network** (172.30.0.0/16)
    - Kubernetes services
    - Internal load balancing
    - Not externally routable

3. **Pod Network** (10.128.0.0/14)
    - Container workloads
    - Pod-to-pod communication
    - Managed by OVN-Kubernetes


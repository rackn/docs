---
title: universal-runbook
tags:
  - reference
  - developer
---

## universal-runbook

This diagram provides a high-level description of burnin.

```d2 layout="elk"
...@docs/assets/classes/all
machine: |md
    ### Assumptions
    * Machine is in local OS
    * Ready for application deploy phase
    * Is the end to most pipelines
    * default is a noop action
    * Cluster too
|

runner: {
    class: runner

    universal-runbook.class: workflow

    universal-runbook: {
        direction: down
        start-callback.class: task
        pre-flexiflow.class: task
        during-flexiflow.class: task
        post-flexiflow.class: task
        classify.class: task
        validation.class: task
        complete-callback.class: task
        workflow-chain.class: task

        start-callback -> pre-flexiflow -> during-flexiflow -> post-flexiflow -> classify -> validation -> complete-callback -> workflow-chain
    }
}
```

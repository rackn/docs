*[HTML]: Hyper Text Markup Language
*[W3C]: World Wide Web Consortium
*[DHCP]: Dynamic Host Configuration Protocol
*[DNS]: Domain Name System
*[DRP]: Digital Rebar Provision
*[IaC]: Infrastructure as Code
*[FTP]: File Transfer Protocol
*[TFTP]: Trivial File Transfer Protocol
*[PXE]: Preboot Execution Environment
*[RAID]: GREG: Fill in
*[IPMI]: GREG: Fill in
*[SSO]: Single Sign-On
*[HA]: High Availability
*[AWS]: Amazon Web Services
*[GCE]: Google Cloud Engine
*[iDRAC]: integrated Dell Remote Access Controller
*[iLo]: HPE's integrate Lights Out
*[XCC]: Lenovo's Xclarity Controller
*[BMC]: Baseboard Management Controller
*[WSL]: Windows Subsystem for Linux
*[Workflow]: A collection of Stages for Runners to execute. Search: +workflow +reference
*[Workflows]: A collection of Stages for Runners to execute. Search: +workflow +reference
*[User]: An object to track a login to the system. Search: +user +reference
*[Users]: An object to track a login to the system. Search: +user +reference
*[Stage]: A collection of Tasks, Parameters, Profiles, and Templates for Runners to execute. Search: +stage +reference
*[Stages]: A collection of Tasks, Parameters, Profiles, and Templates for Runners to execute. Search: +stage +reference
*[Machine]: An object that represents a piece of infrastructure under management with an associated Runner. Search: +machine +reference
*[Machines]: An object that represents a piece of infrastructure under management with an associated Runner. Search: +machine +reference
*[Cluster]: An object that represents a collection of infrastructure with an associated Runner. Search: +cluster +reference
*[Clusters]: An object that represents a collection of infrastructure with an associated Runner. Search: +cluster +reference
*[Resource Broker]: An object that represents a service or interface with an associated Runner. Usually used to allow for allocation of infrastructure. Search: +resource-broker +reference
*[Resource Brokers]: An object that represents a service or interface with an associated Runner. Usually used to allow for allocation of infrastructure. Search: +resource-broker +reference
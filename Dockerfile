FROM docker.io/squidfunk/mkdocs-material

RUN apk add bash make wget curl
RUN apk add curl && apk upgrade
ADD Makefile .
RUN mkdir -p tools
ADD tools/get-d2.sh tools/get-d2.sh
RUN make setup
ARG GH_TOKEN
RUN pip install git+https://${GH_TOKEN}@github.com/squidfunk/mkdocs-material-insiders.git@9.5.35-insiders-4.53.13
#RUN pip install git+https://${GH_TOKEN}@github.com/squidfunk/mkdocs-material-insiders.git@9.5.17-insiders-4.53.5
#RUN pip install git+https://github.com/squidfunk/mkdocs-material.git@spike/search-preview-2
RUN pip install pillow cairosvg
RUN pip install mkdocs-glightbox
RUN pip install mkdocs-literate-nav
RUN pip install mkdocs-section-index
RUN pip install mike
RUN pip install mkdocs-d2-plugin
RUN pip install mkdocs-mermaid2-plugin

v4.4.0-alpha03 {#rs_rel_notes_drp-plugins_v4.4.0-alpha03}
==============

>     commit 7682f04d10d2f6a49b771b33c83830d1ea6f2ed0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jun 29 14:26:11 2020 -0500
>
>         build: Update to v4.4.0-alpha03
>
>     M   go.mod
>     M   go.sum
>
>     commit 327fe9e6e63e8bc92f2ba80141b7928561903905
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 25 13:49:44 2020 -0500
>
>         feat(ipmi): if asked to config user, always set username/password
>
>     M   cmds/ipmi/content/templates/ipmi-configure.sh.tmpl
>
>     commit db3bde775f6c07202f72ba4fc194d75e8662e11e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 24 11:42:15 2020 -0500
>
>         build: add aws cli to travis
>
>     M   .travis.yml
>
>     commit 6bc7c30b978032001af97b86deb854e520c813b7
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Jun 23 17:39:09 2020 -0700
>
>         enhance(kvm-test): Remove 'tablet' type USB device
>
>     M   cmds/kvm-test/libvirt.go
>
>     commit 4de9bd4b42d068d9eb5a3987a655fff4712d94d0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 23 15:20:42 2020 -0500
>
>         docs(rel_notes): remove duplicate refs
>
>     M   tools/build_rel_notes.sh
>
>     commit 0c077e31024d0f98982663b0048f48c3c605c4ae
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 21:00:28 2020 -0500
>
>         feat(bios): Add bios-inventory stage to gather info.
>
>     A   cmds/bios/content/stages/bios-inventory.yml
>
>     commit 66bf39702757338cd3ba3b79b8998dffe8a4ff35
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Jun 18 21:04:29 2020 -0700
>
>         enhance(image-deploy): Fixes for Windows deployments
>
>     M   cmds/image-deploy/content/params/image-deploy.admin-password.yaml
>     M   cmds/image-deploy/content/params/image-deploy.admin-username.yaml
>     M   cmds/image-deploy/content/stages/image-deploy-cloud-init.yaml
>     M   cmds/image-deploy/content/templates/curtin-windows-cloudinit-finalize.tmpl
>
>     commit f26d477f09f8f50c55f1c25b91a8ab15137d5445
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 14:06:25 2020 -0500
>
>         feat(callback): create esxi and windows callback tasks
>
>     M   cmds/callback/content/tasks/callback-task.yaml
>
>     commit d438bf076f51270a77d8bf6c2b93f96a5caee8b6
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 00:40:15 2020 -0500
>
>         feat(bios): add ilo specific config xml injector.
>
>     A   cmds/bios/content/params/bios-ilo-configuration.yaml
>     M   cmds/bios/content/stages/bios-configure-example.yml
>     M   cmds/bios/content/stages/bios-configure.yml
>     A   cmds/bios/content/tasks/bios-ilo-configuration.yaml
>
>     commit 56a50f1ef21b762100f343821a2c48d2419b7d1a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 16 13:48:57 2020 -0500
>
>         build: turn on auto rel_notes
>
>     M   .travis.yml
>     A   tools/build_rel_notes.sh
>
>     End of Note

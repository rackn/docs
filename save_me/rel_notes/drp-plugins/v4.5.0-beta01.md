v4.5.0-beta01 {#rs_rel_notes_drp-plugins_v4.5.0-beta01}
=============

>     commit 374e39d52cebd1af3a4cb05b6c1f63bc29be1f28
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 22 21:50:38 2020 -0500
>
>         build: update to v4.5.0-beta01
>
>     M   go.mod
>     M   go.sum
>
>     commit 300b819affcf9d48a5ce4e4560dcab908254013c
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Sep 9 14:51:01 2020 -0500
>
>         fix(vmware): remove unsupported and update sha
>
>         Fixes issue with stale unsupported bootenvs being listed
>         Updates SHA to match current ISOs in AWS wit latest builds of DRPY
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     D   cmds/vmware/content/bootenvs/esxi_650u2-10719125-A07_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u2-8294253-A00_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383-A01_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-14320405-A03_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-14320405_lenovo.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-15256549_lenovo.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10764712-A04_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u2-13981272-A02_dell.yaml
>     M   cmds/vmware/content/bootenvs/esxi_700-15843807_dell.yaml
>     M   cmds/vmware/content/bootenvs/esxi_700-15843807_hpe.yaml
>
>     commit 7a9dd412536c08a96a523dbe80164b05f06a4468
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Sep 4 12:20:25 2020 -0500
>
>         fix(filebeat): Turn on autostart because defaults work.
>
>     M   cmds/filebeat/filebeat.go
>
>     commit d08da87fb41c07053bb13a7cfe059bcd80a80418
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 1 17:53:44 2020 -0500
>
>         fix(filebeat): reenable event registration
>
>     M   cmds/filebeat/filebeat.go
>
>     commit 9129230ff412b48d16639cb14509b39f007846f1
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Fri Aug 21 13:01:37 2020 -0500
>
>         fix(vmware): esxi 7.0 dell bootenv had wrong sha
>
>         Updated the Dell 7.0 bootenv to reflect the SHA of the ISO in the s3 bucket
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     M   cmds/vmware/content/bootenvs/esxi_700-15843807_dell.yaml
>
>     commit f1edb6fb150c198d83da76c889c08f71e05a650a
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Thu Aug 20 16:36:54 2020 -0500
>
>         fix(vmware): SHA256 was wrong on hpe 7 bootenv
>
>         Updated SHA to match the sha of the ISO in the s3 bucket.
>         Fixes: #226
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     M   cmds/vmware/content/bootenvs/esxi_700-15843807_hpe.yaml
>
>     commit c3257ca5a350926ae34aae8a198a967d278b0e7f
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Aug 17 18:00:16 2020 -0700
>
>         enhance(image-deploy): Add more info to 'image-deploy/image-type' Doc field
>
>     M   cmds/image-deploy/content/params/image-deploy.image-type.yaml
>
>     commit 39ee0768cc5d8851ada1a0b023f5e422d3031ada
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Aug 5 15:34:43 2020 -0500
>
>         fix(vmware): added missing bootenvs for 67u3
>
>         The bootenvs for esxi 6.7u3 were missing due to access to the offline bundles.
>         This change introduces the 6.7u3 general release with the agent added.
>         Adds the 6.7u3 info the the bootenv file builder
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     A   cmds/vmware/content/bootenvs/esxi_670u3-20190801001_vmware.yaml
>     A   cmds/vmware/content/bootenvs/esxi_670u3-s20190801001_vmware.yaml
>     A   cmds/vmware/content/bootenvs/esxi_670u3-x20190801001_vmware.yaml
>     A   cmds/vmware/content/bootenvs/esxi_670u3-xs20190801001_vmware.yaml
>     A   cmds/vmware/content/templates/esxi_670u3-20190801001_vmware.boot.cfg.tmpl
>     A   cmds/vmware/content/templates/esxi_670u3-s20190801001_vmware.boot.cfg.tmpl
>     A   cmds/vmware/content/templates/esxi_670u3-x20190801001_vmware.boot.cfg.tmpl
>     A   cmds/vmware/content/templates/esxi_670u3-xs20190801001_vmware.boot.cfg.tmpl
>     M   cmds/vmware/embedded/scripts/make-esxi.sh
>
>     commit 990cab37357f240ecefbc29ff29f884c52b01666
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Wed Aug 5 10:12:03 2020 -0700
>
>         enhance(bios): Add BIOS Ops doc link to Overview doc
>
>     M   cmds/bios/content/._Documentation.meta
>
>     commit 9fec30a8c24c25a9350c19e29cda4a73cba7308c
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Aug 5 10:33:29 2020 -0500
>
>         fix(vmware bootnvs): remove broken bootenvs
>
>         as of v4.4 plugin we require the agent be part of the iso. This
>         change removes all the old bootenvs that still removes bootenvs
>         that refrence my.vmware
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     D   cmds/vmware/content/bootenvs/esxi_550u3b-3248547_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_600u2-3620759_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_600u3a-5572656_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650a-4887370_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u1-7388607_hpe.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u2-8294253_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383-v430_fujitsu.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383-v431_fujitsu.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383_cisco.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383_hitachi_blade-ha8000.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383_hitachi_ha8000v-gen10.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383_hpe_pregen9.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-13932383_nec_standard.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-14293459_vmware_rollup.yaml
>     D   cmds/vmware/content/bootenvs/esxi_650u3-14320405_hpe.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670-8169922_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_cisco.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_fujitsu.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_hitachi_blade-ha8000.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_nec.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_nec_r120h-t120h-r110j.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-10302608_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-110302608_lenovo.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-11675023_hitachi_ha8000v-gen10.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u1-11675023_hpe.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u2-13006603_cisco.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u2-13006603_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u2-13644319_nec_r120h-t120h-r110j.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u2-13644319_nec_standard.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-13981272-A03_dell.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_cisco.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_fujitsu.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_hpe.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_nec_r120h-t120h-r110j.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_nec_standard.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-14320388_vmware.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3-15160138-20200407_lenovo.yaml
>     D   cmds/vmware/content/bootenvs/esxi_670u3b-15160138_vmware.yaml
>
>     commit 8dce3b6704aa11f0a33e55fcfc567e7d8c2cd38f
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Fri Jul 31 11:38:10 2020 -0500
>
>         fix(esxi-set-insecure-password): make py3 compat
>
>         Changed print to print() to make the call support python 3
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     M   cmds/vmware/content/templates/esxi-set-insecure-password.sh.tmpl
>
>     End of Note

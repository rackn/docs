v4.6.15 {#rs_rel_notes_drp-plugins_v4.6.15}
=======

>     commit 72659b4f75f35a1add05f40b520981c4151e09cc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Oct 10 10:15:56 2022 -0500
>
>         fix(bios): lenovo needs a 30s sleep to fix deassert
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   cmds/bios/content/tasks/bios-lenovo-configuration.yaml
>
>     End of Note

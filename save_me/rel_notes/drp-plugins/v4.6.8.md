v4.6.8 {#rs_rel_notes_drp-plugins_v4.6.8}
======

>     commit c869391095ac598ee826fbe1614d183b91ae9946
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Wed Dec 15 14:20:41 2021 -0500
>
>         fix(callback): adding debug option to enable request/response logging
>
>     M   cmds/callback/callback.go
>     M   cmds/callback/content/._Documentation.meta
>     M   cmds/callback/content/params/callback.auths.yaml
>     M   cmds/callback/content/params/callback.callbacks.yaml
>
>     commit dcb4e76ad5eb113ad619a0d2fb8ac50e2d8b6824
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Fri Sep 3 14:17:05 2021 -0400
>
>         fix(ci): updating trigger
>
>     M   .gitlab-ci.yml
>
>     commit a4f2623850b3613283fff82e7b1e7d3429caac82
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Fri Sep 3 14:09:06 2021 -0400
>
>         test
>
>     M   .gitlab-ci.yml
>
>     commit ba078387b1f39583c31ca0cfd7edc58e10a45e15
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Thu Sep 2 18:16:28 2021 -0400
>
>         fix(ci): triggering the right rackn-saas branch
>
>     M   .gitlab-ci.yml
>
>     commit 8b2aebb291b21d75346261e6127ec695fc2f7df0
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Thu Sep 2 14:16:40 2021 -0400
>
>         ci: adding ci config for v4.6
>
>     A   .gitlab-ci.yml
>
>     commit 32008c86ca488570259a720fe80ceb31cd618b31
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 2 09:02:07 2021 -0500
>
>         feat(gitlab): Redirect support and fixups for Gitlab.
>
>         Add the appropriate replace directives as a bridge for the Gitlab
>         migration. Fix up the build to account for the transitive breakage of
>         how we invoke drpcli as part of the build process.
>
>     M   go.mod
>     M   go.sum
>     M   tools/build.sh
>     A   tools/drpcli.go
>
>     End of Note

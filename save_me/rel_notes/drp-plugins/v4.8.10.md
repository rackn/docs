v4.8.10 {#rs_rel_notes_drp-plugins_v4.8.10}
=======

>     commit 16404ce8cb2352e8fedd08f3cfdd97bd4ea952a5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Oct 11 15:33:30 2022 -0500
>
>         fix(ipmi): Update the onecli ipmi calls into batchs for performance.
>
>     M   cmds/ipmi/content/templates/ipmi-lenovo-tools.sh.tmpl
>
>     End of Note

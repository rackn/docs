v4.10.1 {#rs_rel_notes_drp-plugins_v4.10.1}
=======

>     commit f4e98113414b84072d2b1c6c1400a02603b68e34
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 20 11:47:00 2022 -0500
>
>         fix: ipmi-install-cert needs tool()
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   cmds/ipmi/content/templates/ipmi-install-cert-key.sh.tmpl
>
>     commit f275a1d62f0a93eafcb89d5c082e82addbf5971f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 29 13:37:08 2022 -0500
>
>         build: try again on build to version docs
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   tools/build_rel_notes.sh
>     M   tools/publish.sh
>
>     commit 264af70ce101cffbce895f50c0fb68f9742f097d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 29 12:51:09 2022 -0500
>
>         build: update docs to be built always into branch specific locations
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   tools/build_rel_notes.sh
>     M   tools/publish.sh
>
>     commit e1c04a10c6b43a4f62c928b6ff308f82db536d2a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 8 17:17:00 2022 -0500
>
>         build: update to v4.10.0
>
>     M   go.mod
>     M   go.sum
>
>     commit 62907e1ac4bb42d9ca0f6c2cec9d53fd4a558e41
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Aug 5 12:16:20 2022 -0500
>
>         feat(triggers): Add generic json trigger
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     A   cmds/triggers/content/params/json-trigger.key.yaml
>     A   cmds/triggers/content/params/json-trigger.secret.yaml
>     A   cmds/triggers/content/trigger_providers/json-trigger-webhook.yaml
>     A   cmds/triggers/json-trigger-webhook.go
>     M   cmds/triggers/triggers.go
>
>     commit 815da001454debc16ff0fc4b400377aaba0c752b
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Jul 7 15:24:32 2022 -0500
>
>         feat(audit): Add event2audit plugin
>
>         This plugin will add audit logs to dr-provision for every event
>         it is configured to recieve via its plugin configuration.
>
>     A   cmds/event2audit/content/meta.yaml
>     A   cmds/event2audit/content/params/event2audit_events.yaml
>     A   cmds/event2audit/event2audit.go
>
>     commit 8fd27ec16010a6519e1aa4407a303cd70eeaadc7
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jul 20 12:09:38 2022 -0500
>
>         fix(ipmi): Add a bmc reset after setting the dell certs
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   cmds/ipmi/content/templates/ipmi-dell-tools.sh.tmpl
>
>     commit 0eb4573806cfe4aefff97be0821cf918e3e7619b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jul 20 10:56:20 2022 -0500
>
>         fix: add params to ignore dns-servers and ntp-servers params in ipmi-configure
>
>         Some teams assume that ntp-servers and dns-servers are for the OS and
>         want to use bios configuration to set these values.  Setting the ignore
>         parameters will cause the ipmi-configure task to not set them.
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     A   cmds/ipmi/content/params/ipmi.configure.ignore-dns-servers.yaml
>     A   cmds/ipmi/content/params/ipmi.configure.ignore-ntp-servers.yaml
>     M   cmds/ipmi/content/templates/ipmi-dell-tools.sh.tmpl
>     M   cmds/ipmi/content/templates/ipmi-hpe-tools.sh.tmpl
>     M   cmds/ipmi/content/templates/ipmi-lenovo-tools.sh.tmpl
>
>     commit c0296f1db96f2898bb3de679588ee742be9c0e8d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jul 19 19:42:49 2022 -0500
>
>         doc: youtube fixes
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   cmds/ipmi/content/meta.yaml
>
>     commit a042713e8d7c9f8a32920ffa6f4377ab6fc8dffc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jul 19 17:16:56 2022 -0500
>
>         doc: fix youtube references
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   cmds/ipmi/content/meta.yaml
>
>     commit 6fec251814d2cd7990a7328d4420a259789df18e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jun 20 12:45:49 2022 -0500
>
>         fix(ipmi): switch keyload to first for dell to see if it works
>
>         Address zd#920
>
>     M   cmds/ipmi/content/templates/ipmi-dell-tools.sh.tmpl
>
>     commit 709455f2eef9753d701d099751ab6e25b9270fa1
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jun 17 16:53:21 2022 -0500
>
>         fix(ipmi): use proper ssl update commands for dell
>
>     M   cmds/ipmi/content/templates/ipmi-dell-tools.sh.tmpl
>
>     commit 2eb9bd7cae23e34df7ae7b7147d547849bf5dc6b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 16 13:46:29 2022 -0500
>
>         fix(ipmi): make sure the cat output ends up in a file.
>
>     M   cmds/ipmi/content/templates/ipmi-dell-tools.sh.tmpl
>     M   cmds/ipmi/content/templates/ipmi-lenovo-tools.sh.tmpl
>
>     commit 1aaa4a22654b483ef43a749d3100495b58efa4dc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 8 14:42:07 2022 -0500
>
>         doc: fix rel notes and don't publish docs unless tip
>
>     M   tools/build_rel_notes.sh
>     M   tools/publish.sh
>
>     commit 15604889d73c0c222fc7c9a09a3f811a30ef813a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 19 10:13:06 2022 -0500
>
>         fix(vmware): fixes issue in zd842 - .ParamExpand shoud be $.ParamExpand
>
>     M   cmds/vmware/content/tasks/esxi-patch-install.yaml
>
>     commit 5f9b85c82055918f537f0d22054b73c0c1d35f07
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Apr 6 22:09:18 2022 -0500
>
>         feat(vmware): change patch update task
>
>         Change the patch install task to allow installs of vibs where before it only allowed updates
>         Changed to support ZD 795 & 842
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     A   cmds/vmware/content/params/esxi-patch-action.yaml
>     M   cmds/vmware/content/tasks/esxi-patch-install.yaml
>
>     commit df2a40cc2f5360878148440d3678ea1053cf3434
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Tue Mar 8 16:19:32 2022 -0500
>
>         feat(triggers): implementing unimplemented triggers
>
>     M   cmds/triggers/kaholo-trigger-lacework-alert-webhook.go
>     M   cmds/triggers/logic-monitor-trigger-alert-webhook.go
>     M   cmds/triggers/logzio-trigger-alert_webhook.go
>     M   cmds/triggers/monday-trigger-status_update_webhook.go
>     M   cmds/triggers/oci-monitoring-trigger-alert-webhook.go
>     M   cmds/triggers/okta-trigger-alert.go
>     M   cmds/triggers/okta-trigger-verify.go
>     M   cmds/triggers/pager-duty-trigger-incident_webhook.go
>     M   cmds/triggers/pingdom-trigger-webhook-alert.go
>     M   cmds/triggers/prometheus-trigger-alert-webhook.go
>     M   cmds/triggers/prtg-trigger-post-notification.go
>     M   cmds/triggers/send-grid-trigger-event.go
>     M   cmds/triggers/slack-button-trigger-webhook-button.go
>     M   cmds/triggers/sumologic-trigger-alert-webhook.go
>     M   cmds/triggers/unbounce-trigger-webhook-submit.go
>     M   cmds/triggers/vsts-trigger-pr-webhook.go
>     M   cmds/triggers/vsts-trigger-push-webhook.go
>     M   cmds/triggers/zabbix-trigger-webhook.go
>     M   cmds/triggers/zapier-trigger-webhook-post.go
>     M   go.mod
>     M   go.sum
>
>     commit 60a60aab8919f792be188307837486c6be7f5fd1
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Mar 26 10:58:32 2022 -0500
>
>         fix: bios should store no config on tool failure and not invalidate machine.
>
>     M   cmds/bios/content/tasks/bios-current-config.yml
>
>     commit a6a79e2463911ade08386deee053cc211a9775ec
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Mar 21 12:39:26 2022 -0500
>
>         Update for go 1.18 compat and enhanced plugin cleanup
>
>     M   go.mod
>     M   go.sum
>
>     commit 38f038b12098a7fa702733951a6e70eb28ee7dca
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Mar 9 12:00:46 2022 -0600
>
>         feat(callback): Add ErrorBody so that the error body is include on errors
>
>     M   cmds/callback/callback.go
>     M   cmds/callback/content/params/callback.auths.yaml
>     M   cmds/callback/content/params/callback.callbacks.yaml
>
>     commit 0d669ce54f5e29b2326e969843194f6a79d2028c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Mar 9 08:37:48 2022 -0600
>
>         doc: update a little more vault work
>
>     M   cmds/vault/content/meta.yaml
>
>     commit ee2bf8d850fb829a10e7cedf3f8660cd24e1f1f7
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Mar 8 16:41:56 2022 -0600
>
>         doc: more vault tweaks
>
>     M   cmds/vault/content/meta.yaml
>
>     commit 2039d90a3ec080bb82c8dcaf28cb069121244e25
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Mar 8 12:38:29 2022 -0600
>
>         doc: fix vault doc formatting
>
>     M   cmds/vault/content/meta.yaml
>
>     commit e0f21a1a3dadd56c17c658fc00d5685fbc3d9bff
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Sat Mar 5 13:31:33 2022 -0600
>
>         build(darwin): Add Apple Silicon support
>
>         Let the plugins be built for apple silicon.  This also updates the
>         gitlab-ci.yml to use the new gitlab images.
>
>     M   .gitlab-ci.yml
>     M   go.mod
>     M   go.sum
>     M   tools/build.sh
>
>     End of Note

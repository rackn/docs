v4.8.7 {#rs_rel_notes_drp-plugins_v4.8.7}
======

>     commit 31b29bd2861c7070c50cd15913c6ea85973e1023
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jul 27 14:52:11 2022 -0500
>
>         Bump json-iterator for Go 1.18 compat
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

v4.2.5 {#rs_rel_notes_drp-plugins_v4.2.5}
======

>     commit c52cb2ba96a3fea6df17502684da344a488154cc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Feb 6 12:31:47 2020 -0600
>
>         Fix the mtu setting of the vmk0 call.
>
>     M   cmds/vmware/content/tasks/esxi-set-network.yaml
>
>     End of Note

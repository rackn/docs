v4.7.5 {#rs_rel_notes_drp-plugins_v4.7.5}
======

>     commit d7688cf4ab724fca05100272b15d6b9e1dbf96c8
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 19 10:13:06 2022 -0500
>
>         fix(vmware): fixes issue in zd842 - .ParamExpand shoud be $.ParamExpand
>
>     M   cmds/vmware/content/tasks/esxi-patch-install.yaml
>
>     End of Note

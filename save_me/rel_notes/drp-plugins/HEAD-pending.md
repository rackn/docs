: Wed Oct 12 10:19:58 2022 -0500

> feat(vulncheck): Add automated vulnerability checking
>
> We now will run automated vulnerability checks against built
> artifacts.

> M go.mod M go.sum M tools/build-one.sh M tools/test.sh
>
> commit f6a4da73a131b8c69ec0c5f98bbe5bba652985ce Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Tue Oct 11 17:00:42 2022 -0700
>
> > fix(vmware): Fix manage users localcli to esxcli
>
> M cmds/vmware/content/templates/esxi-manage-users.sh.tmpl
>
> commit 087ecb0590c416f2c0f3fa249a2e5e8081e00e9f Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Tue Oct 11 15:33:30 2022 -0500
>
> > fix(ipmi): Update the onecli ipmi calls into batchs for performance.
>
> M cmds/ipmi/content/templates/ipmi-lenovo-tools.sh.tmpl
>
> commit 2b5f5f1eb216c2b6fc7566f64a8aec673bb995d7 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Oct 10 10:15:56 2022 -0500
>
> > fix(bios): lenovo needs a 30s sleep to fix deassert
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M cmds/bios/content/tasks/bios-lenovo-configuration.yaml
>
> commit b38293589a62236214af0b6faec6ab4dbb787af0 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Sat Oct 8 07:47:09 2022 -0700
>
> > enhance(vmware): Add service restart to manager users
>
> M cmds/vmware/content/templates/esxi-manage-users.sh.tmpl
>
> commit b72fb724ac4b410464682d7e5805a26386c75739 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Sun Sep 4 16:41:38 2022 -0500
>
> > feat(raid,bios,ipmi): Use flexiflow to replace full tasks
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M cmds/bios/content/meta.yaml A
> cmds/bios/content/params/bios-baseline-tasks.yaml A
> cmds/bios/content/params/bios-configure-tasks.yaml A
> cmds/bios/content/params/bios-inventory-tasks.yaml A
> cmds/bios/content/params/bios-reset-tasks.yaml M
> cmds/bios/content/stages/bios-baseline.yml M
> cmds/bios/content/stages/bios-configure.yml M
> cmds/bios/content/stages/bios-inventory.yml A
> cmds/bios/content/stages/bios-reset.yml A
> cmds/bios/content/tasks/bios-reset.yml A
> cmds/ipmi/content/params/ipmi.configure-tasks.yaml A
> cmds/ipmi/content/params/ipmi.configure.vendor-template.yaml A
> cmds/ipmi/content/params/ipmi.install-cert-tasks.yaml A
> cmds/ipmi/content/params/ipmi.inventory-tasks.yaml M
> cmds/ipmi/content/stages/ipmi-configure.yaml M
> cmds/ipmi/content/stages/ipmi-install-cert.yaml M
> cmds/ipmi/content/stages/ipmi-inventory.yaml M
> cmds/ipmi/content/templates/ipmi-vendor.sh.tmpl M
> cmds/raid/content/meta.yaml A
> cmds/raid/content/params/raid-baseline-tasks.yaml A
> cmds/raid/content/params/raid-configure-tasks.yaml A
> cmds/raid/content/params/raid-enable-encryption-tasks.yaml A
> cmds/raid/content/params/raid-inventory-tasks.yaml A
> cmds/raid/content/params/raid-reset-tasks.yaml M
> cmds/raid/content/stages/raid-baseline.yml M
> cmds/raid/content/stages/raid-configure.yaml M
> cmds/raid/content/stages/raid-enable-encryption.yaml M
> cmds/raid/content/stages/raid-inventory.yml M
> cmds/raid/content/stages/raid-reset.yaml
>
> commit f0abe3102b279367401d3c66cd5e02df8a19425c Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Fri Sep 30 10:04:36 2022 -0500
>
> > fix(ipmi/quirks): Refactor IPMI quirk support.
> >
> > IPMI quirk handling was a hodgepodge from the original Digital
> > Rebar. Refactor it from a YAML blob processed by some hideous bash
> > into some less hideous JSON processed by bash and jq.
>
> A cmds/ipmi/content/params/ipmi-quirklist.yaml M
> cmds/ipmi/content/tasks/ipmi-configure.yaml M
> cmds/ipmi/content/tasks/ipmi-inventory.yaml M
> cmds/ipmi/content/templates/ipmi-load-quirks.sh.tmpl M
> cmds/ipmi/content/templates/ipmi-process-quirks.sh.tmpl A
> cmds/ipmi/content/templates/ipmi-quirks.json.tmpl D
> cmds/ipmi/content/templates/ipmi-quirks.yaml.tmpl
>
> commit 8a8a0c48152e79aa7c6c6c37f77c4457b1c025f4 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Tue Sep 20 11:47:00 2022 -0500
>
> > fix: ipmi-install-cert needs tool()
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M cmds/ipmi/content/templates/ipmi-install-cert-key.sh.tmpl
>
> commit 5309885b8f5c06babadbb902739f832ba74b6032 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Tue Sep 13 22:45:59 2022 +0000
>
> > fix(vmware): Add in missing Stage for esxi-manage-users
>
> A cmds/vmware/content/stages/esxi-manage-users.yaml
>
> commit 9e477911d3f3feb7013a78d9f1b19e2a2959b577 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Fri Sep 2 12:48:55 2022 -0700
>
> > enhance(vmware): Add local user management to ESXi
>
> A cmds/vmware/content/blueprints/esxi-manage-users.yaml A
> cmds/vmware/content/params/esxi-manage-users-ignore-list.yaml A
> cmds/vmware/content/params/esxi-manage-users.yaml A
> cmds/vmware/content/tasks/esxi-manage-users.yaml A
> cmds/vmware/content/templates/esxi-manage-users.sh.tmpl M
> cmds/vmware/content/workflows/esxi-install.yaml
>
> commit d26a406835b698aedbc8616b18c7cce73302f9e7 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Thu Sep 1 06:08:55 2022 -0700
>
> > fix(vmware): Fixes ESXi bootenv ISO validation checks
>
> M cmds/vmware/content/templates/vmware-esxi-selector.tmpl
>
> commit 32af7b895ff2e02f76238a2d1172dada8a1d5842 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 13:37:08 2022 -0500
>
> > build: try again on build to version docs
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M tools/build\_rel\_notes.sh M tools/publish.sh
>
> commit f3898710eabb7632c829ecb2af7008f2161c2fc1 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 12:51:09 2022 -0500
>
> > build: update docs to be built always into branch specific locations
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M tools/build\_rel\_notes.sh M tools/publish.sh
>
> commit d7b8813eb8ad2e5667987230541e7a36c45ac005 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 22 13:20:19 2022 -0500
>
> > feat(triggers): Add tracking of trigger counts for audit
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M cmds/triggers/app-optics-trigger-alert-webhook.go M
> cmds/triggers/azure-monitor-trigger-alert-webhook.go M
> cmds/triggers/bitbucket-trigger-webhook-pr.go M
> cmds/triggers/bitbucket-trigger-webhook-push.go M
> cmds/triggers/datadog-trigger-alert\_webhook.go M
> cmds/triggers/dynatrace-trigger-alert\_webhook.go M
> cmds/triggers/git-lab-trigger-mr-webhook.go M
> cmds/triggers/git-lab-trigger-webhook-push.go M
> cmds/triggers/github-trigger-webhook-pr.go M
> cmds/triggers/github-trigger-webhook-push.go M
> cmds/triggers/jira-trigger-issue-update-webhook.go M
> cmds/triggers/jira-trigger-new-issue-webhook.go M
> cmds/triggers/json-trigger-webhook.go M
> cmds/triggers/kaholo-trigger-lacework-alert-webhook.go M
> cmds/triggers/logic-monitor-trigger-alert-webhook.go M
> cmds/triggers/logzio-trigger-alert\_webhook.go M
> cmds/triggers/monday-trigger-status\_update\_webhook.go M
> cmds/triggers/oci-monitoring-trigger-alert-webhook.go M
> cmds/triggers/okta-trigger-alert.go M
> cmds/triggers/okta-trigger-verify.go M
> cmds/triggers/pager-duty-trigger-incident\_webhook.go M
> cmds/triggers/pingdom-trigger-webhook-alert.go M
> cmds/triggers/prometheus-trigger-alert-webhook.go M
> cmds/triggers/prtg-trigger-post-notification.go M
> cmds/triggers/send-grid-trigger-event.go M
> cmds/triggers/slack-button-trigger-webhook-button.go M
> cmds/triggers/sumologic-trigger-alert-webhook.go M
> cmds/triggers/unbounce-trigger-webhook-submit.go M
> cmds/triggers/vsts-trigger-pr-webhook.go M
> cmds/triggers/vsts-trigger-push-webhook.go M
> cmds/triggers/zabbix-trigger-webhook.go M
> cmds/triggers/zapier-trigger-webhook-post.go M
> cmds/triggers/zendesk-trigger-webhook.go M go.mod M go.sum
>
> commit f7d3e53242da081f8823a8d427d35adf5e135383 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Sun Aug 21 21:34:06 2022 -0500
>
> > build: Update go mod
>
> M go.mod M go.sum
>
> commit 3baccc3b65ebb7693c69a9b3a52fe5cca88f45e2 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Sun Aug 14 04:53:35 2022 +0000
>
> > enhance(vmware): Add selective NTP config tooling based on installed
> > ESXi version
>
> A cmds/vmware/content/blueprints/esxi-set-ntp.yaml A
> cmds/vmware/content/stages/esxi-set-ntp.yaml M
> cmds/vmware/content/tasks/esxi-set-ntp.yaml A
> cmds/vmware/content/templates/esxi-set-ntp-via\_esxcli.sh.tmpl A
> cmds/vmware/content/templates/esxi-set-ntp-via\_ntpconf.sh.tmpl
>
> commit 333b77af940b4b1d65ad0df8e56dd27c3a44b0c1 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Fri Aug 12 01:57:01 2022 +0000
>
> > enhance(vmware): Update NTP handling based on new VMware
> > requirements
>
> M cmds/vmware/content/params/esxi-ntp-conf.yaml M
> cmds/vmware/content/tasks/esxi-set-ntp.yaml D
> cmds/vmware/content/templates/esxi-example-firstboot-complex.ks.tmpl
>
> End of Note

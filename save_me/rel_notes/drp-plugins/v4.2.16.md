v4.2.16 {#rs_rel_notes_drp-plugins_v4.2.16}
=======

>     commit a5c70444233acf7e514658e0aa264a95df1b7dec
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 1 14:09:46 2020 -0500
>
>         fix(vmware): update to vib v0.9.1 - clean dir + crash fix
>
>     M   cmds/vmware/download-vibs.sh
>
>     commit df6faf7beb6ff9acfb840a3694e1a3925b8de381
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 1 11:31:20 2020 -0500
>
>         feat(vmware): add task to update Machine Address field on network change
>
>         Added new parameter esxi/skip-update-address to control
>         whether the esxi-set-network task should update the
>         machine's address field to match the new address.
>
>         The default is true - don't do it.  Setting the parameter
>         to false will update the field.
>
>     A   cmds/vmware/content/params/esxi-skip-update-address.yaml
>     M   cmds/vmware/content/tasks/esxi-set-network.yaml
>
>     End of Note

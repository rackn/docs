v1.12.0 {#rs_rel_notes_drp-content_v1.12.0}
=======

>     commit 56183f65d22ba322ba67cdba0a878cc5c72a3b1d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Dec 14 09:33:48 2018 -0600
>
>         Modify join-up.sh to search RS_UUID in kernel params first.
>
>     M   content/bootenvs/discovery.yml
>
>     commit 96912b75da83e131456905985ec9364da353ab34
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Dec 10 12:42:05 2018 -0600
>
>         Allow the join-up.sh script to use IP as a
>         matcher of existing systems.
>
>     M   content/bootenvs/discovery.yml
>
>     commit 86d3f86a17f6007a7801eee5f3211344d14a5880
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Dec 7 10:40:47 2018 -0600
>
>         This change does a couple of things.
>
>         1. Fixes centos 7.5 to 7.6.  There were some oversights
>            in the previous PR.
>         2. Updates sledgehammer to centos 7.6 for amd64.  Arm rebuild will be
>            required.
>         3. Sledgehammer now has two new kernel variables that can
>            be used to alter start.
>
>         The two vars are:
>         provisioner.postportdelay
>            which causes a wait in seconds after the link has been brought
>            up, but hasn't started DHCP.  Defaults to unset.
>         provisioner.wgetretrycount
>            which is the number of times to retry the stage2 wget before
>            dropping to ash.  The default is 10.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     M   content/params/package-repositories.yaml
>     R061    content/stages/centos-7.5.1804.yml  content/stages/centos-7.6.1810.yml
>
>     commit c96f7ae6ad8ed73289afa93cfd02f500de808629
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Dec 5 23:15:39 2018 -0600
>
>         Fix centos7 update breaking krib.
>         Update k8s version to include recent secuirty fix.
>
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>     M   krib/templates/kubernetes-install.sh.tmpl
>
>     commit 540cfbee89426308629b742831b288f10e9c8479
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Dec 3 12:41:41 2018 -0600
>
>         Update centos 7 bootenv to 1810
>
>     D   content/bootenvs/centos-7.5.1804.yml
>     A   content/bootenvs/centos-7.6.1810.yml
>     M   content/bootenvs/centos-7.yml
>
>     commit 601f65cc3405a7b20d349a5c053c9455f67c08a0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Nov 29 13:44:38 2018 -0600
>
>         Handle delete and lookups better depending upon token type.
>
>     M   content/bootenvs/discovery.yml
>
>     commit 9b88af03fbdfac2b2182bb52790ca63bcdef2eb5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Nov 29 09:54:51 2018 -0600
>
>         Add CUMULUS ZTP Script comment.
>
>     M   content/bootenvs/discovery.yml
>
>     commit 39fa4185601aaf85c35573ffcc205296bd8205c8
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Nov 20 22:00:52 2018 -0600
>
>         fix multi-arch
>
>     M   krib/._RequiredFeatures.meta
>
>     commit 994f441c02a7d9576506ae5f4f3b7b6e6930b6a9
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Nov 20 21:58:56 2018 -0600
>
>         add multiarch back
>
>     M   content/._RequiredFeatures.meta
>
>     commit d186752f8d2aedce5c832cebaf37601308bf698c
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Nov 20 21:57:40 2018 -0600
>
>         cleanup and add multiarch back
>
>     M   krib/._RequiredFeatures.meta
>     M   krib/tasks/krib-kubevirt.yaml
>
>     commit fdd36e036de6aec7f245c7e10b5207065a9aea24
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Nov 20 21:53:57 2018 -0600
>
>         as per @faniand, remove unneeded packages. host check is optional
>
>     M   content/._RequiredFeatures.meta
>     M   krib/tasks/krib-kubevirt.yaml
>
>     commit 6759f3d4bf8cc9408e03ae53c013b2b50732b2b3
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Nov 17 10:50:09 2018 -0600
>
>         tweak join-up script to check for existing from name instead of just rs_uuid file
>
>     M   content/bootenvs/discovery.yml
>
>     commit 1cab75770710510cd9f2a64f7c87ea53623a30ce
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Nov 17 10:50:09 2018 -0600
>
>         tweak join-up script to check for existing from name instead of just rs_uuid file
>
>     M   content/bootenvs/discovery.yml
>
>     commit 4bb3b28ec7e63cb5004a9f7d34a13d6842ce5f2e
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Nov 16 17:36:22 2018 -0600
>
>         working first pass for KubeVirt.io stage
>
>     A   krib/stages/krib-kubevirt.yaml
>     A   krib/tasks/krib-kubevirt.yaml
>     A   krib/templates/kubevirt.cfg.tmpl
>
>     commit bd4705d752760ac8f9cd93c2663d1e092340aaa6
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Nov 13 17:02:33 2018 -0600
>
>         Update sledgehammer for latest ipv6 delays and fixes.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit c96cea4a566492daca1da8d1d5e72f71b53691fc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Nov 13 16:46:35 2018 -0600
>
>         Only us the first global scope inet6 address.
>         If DHCP and SLAAC enabled, record the DHCP one.
>         It shows up first (most of the time...)
>
>     M   content/bootenvs/discovery.yml
>
>     commit e942ead7e6e8496f4c0c5bfaedcfb45ea1900d0a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 19:14:56 2018 -0600
>
>         Another change.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit dcb7ec07ac316693b3934b0f576010214b23fd02
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 15:05:50 2018 -0600
>
>         Update sledgehammer for /64 on ipv6 address
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 73f1afc472907ac231684819c99746323c3037e8
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 15:03:57 2018 -0600
>
>         Make sure socat is installed.
>
>     M   krib/templates/kubernetes-install.sh.tmpl
>
>     commit 41c1bcbb093494282baf6e62dbc8c029568b1924
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 14:37:08 2018 -0600
>
>         Add docker specific version.
>
>     A   krib/params/docker-version.yaml
>     M   krib/templates/docker-install.sh.tmpl
>
>     commit 20e42264e63781aa01b731b7c126b9f76188c06d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 12:50:58 2018 -0600
>
>         Deal with docker issues
>
>     M   krib/templates/docker-install.sh.tmpl
>
>     commit 45a0f48a642092627aeabf4b9b492027489caed4
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 12 09:44:41 2018 -0600
>
>         Update krib and friends to work with CoreOS
>
>     M   content/templates/setup.tmpl
>     A   krib/params/krib-cluster-cni-version.yaml
>     A   krib/params/krib-cluster-crictl-version.yaml
>     M   krib/stages/krib-live-wait.yaml
>     M   krib/templates/docker-install.sh.tmpl
>     M   krib/templates/etcd-config.sh.tmpl
>     M   krib/templates/krib-dashboard.sh.tmpl
>     M   krib/templates/krib-helm-init.sh.tmpl
>     M   krib/templates/kubernetes-install.sh.tmpl
>     M   krib/templates/mount-disks.sh.tmpl
>     M   krib/workflows/krib-reset-cluster.yaml
>
>     commit 5760dfe22989a0762558ddf811354057a2f774e2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Nov 11 08:44:55 2018 -0600
>
>         Update stretch to 9.6
>
>     M   content/bootenvs/debian-9.yml
>
>     commit cfe88855a050655b146c25792bc6bc90897a47f9
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Nov 9 17:08:53 2018 -0600
>
>         Update sledgehammer for more IPV6 changes
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit e7ac2ae6cf0b7b9e068829707f236417f0d4a53e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Nov 8 13:36:14 2018 -0600
>
>         Update sledgehammer for an IPv6 issue
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit e829064976d180eb0af0e45d038f2e9e66a0c676
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 31 00:43:01 2018 -0500
>
>         Fix krib in tip
>
>     M   krib/templates/docker-install.sh.tmpl
>     M   krib/templates/kubernetes-install.sh.tmpl
>
>     commit 460a0ca46623a4ce1bda1ddb91b5cc1c117e35ac
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Oct 30 17:35:49 2018 -0500
>
>         add google discovery stage
>
>     A   content/stages/gce-discover.yaml
>     A   content/tasks/gce-discover.yaml
>
>     commit 58e81362a833b3ec5923e12e6151f2251bc72286
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Oct 29 17:10:58 2018 -0500
>
>         fix address, validate script on AWS
>
>     M   content/bootenvs/discovery.yml
>     A   content/params/cloud-public-hostname.yaml
>     M   content/params/cloud-public-ipv4.yaml
>     D   content/params/cloud-public-ipv6.yaml
>     M   content/tasks/aws-discover.yaml
>
>     commit 99fefd09d347b41fade7a801dd1d64aaf01ef0fb
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Oct 29 17:04:53 2018 -0500
>
>         version in community changed...
>
>         need to bump version
>
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>
>     commit aabc28c57755e5b5e4309130b05e932409e42a05
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Oct 26 10:41:16 2018 -0500
>
>         Allow sledgehammer to not always reset the workflow/tasks index
>         on startup.  If start-over is set to false, leave the task list alone.
>         Otherwise, operate as normal.  This will also work for all bootenvs
>         that use reset-workflow template.
>
>         The start-over flag is removed from the machine if directly set upon it.
>         This allows for a one time effect.
>
>     A   content/params/start-over.yaml
>     M   content/templates/reset-workflow.tmpl
>
>     commit 0d3795e0433f59ebad10149c82f4dcfbe3b54c4e
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 25 13:46:50 2018 -0500
>
>         cleanup join-up to be cloud agnostic, not tested
>
>     M   content/bootenvs/discovery.yml
>     R069    content/params/cloud-region.yaml    content/params/cloud-availability-zone.yaml
>     A   content/params/cloud-public-ipv4.yaml
>     A   content/params/cloud-public-ipv6.yaml
>     M   content/tasks/aws-discover.yaml
>
>     commit e5302bdd4d9c367c7df5a684056050c958357a94
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 25 09:28:04 2018 -0500
>
>         WIP changing model to use simpler join and aws stages
>
>     M   content/bootenvs/discovery.yml
>     A   content/params/cloud-region.yaml
>     A   content/stages/aws-discover.yaml
>     A   content/tasks/aws-discover.yaml
>
>     commit 65febe0ec0af89ff94552347a562ec4ae2b2a06c
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Oct 22 20:58:01 2018 -0500
>
>         add join-up script for discovery that allows AWS machines to join from the unknown machine bootenv
>
>     M   content/bootenvs/discovery.yml
>     A   content/params/cloud-instance-id.yaml
>     A   content/params/cloud-instance-type.yaml
>     A   content/params/cloud-vendor.yaml
>
>     commit 4bda05e71d3e31806f34592e1e351bb0b562ee8b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Oct 22 17:50:25 2018 -0500
>
>         Add provisioner.portdelay=X as an option in kernel-console.
>         This will cause sledgehammer to wait X seconds if present
>         after starting the link to wait for port delay switches.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit a88bb89c7e1e90f4e76bef4a43adc065a1d63794
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Oct 16 21:06:07 2018 -0500
>
>         Update to new sledgehammer with better /etc/resolv.conf handling.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 54d723baa88c82a205d4b78fe9a1f90208967de4
>     Author: galthaus <galthaus@austin.rr.com>
>     Date:   Sat Oct 13 18:18:36 2018 -0500
>
>         Fix typo
>
>     M   content/bootenvs/sledgehammer.yml
>
>     commit b6a74644a08dc92e768a7ec4208f3ccbe8fb4eb0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Oct 12 11:19:21 2018 -0500
>
>         Update sledgehammer to ipv6 cap and in s3
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 17988cba8f5fddbb6921efcc045cc48e1660f0c2
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 13 12:51:46 2018 -0500
>
>         More multiarch fixes
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     M   content/params/package-repositories.yaml
>     M   content/templates/default-grub.tmpl
>     M   content/templates/setup.tmpl
>
>     commit 7ade2b39020eb3ac3a153938a3ccedfea0b537c0
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Sep 11 13:41:50 2018 -0500
>
>         Swizzle up discovery and centos-7-install bootenvs to be multiarch aware
>
>     M   content/._RequiredFeatures.meta
>     M   content/bootenvs/centos-7.yml
>     M   content/bootenvs/discovery.yml
>     M   content/params/kernel-console.yaml
>     M   content/params/package-repositories.yaml
>     M   content/templates/centos-7.ks.tmpl
>     A   content/templates/default-grub.tmpl
>
>     commit 9339d799f405969ffc0dd7ea81c636e49c4b6806
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Oct 9 23:04:04 2018 -0500
>
>         skip daemon if no param defined
>
>     M   krib/templates/docker-install.sh.tmpl
>
>     commit e15b6eace7ef439b6c63ebcda8a5825a8399b706
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Oct 9 22:50:28 2018 -0500
>
>         remove insecure default
>
>     M   krib/params/docker-daemon.yaml
>     M   krib/templates/krib-contrail.sh.tmpl
>
>     commit 2b69383efafdf337a8193c59e4eb759a3719676b
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Oct 8 09:14:45 2018 -0500
>
>         undo typo in config
>
>     M   krib/templates/contrail.cfg.tmpl
>
>     commit 7e0e8a22c617f8f089cf4edd7358cccf73477142
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sun Oct 7 23:03:30 2018 -0500
>
>         use daemon config instead of contrail reg hack
>
>     D   krib/params/contrail-docker-registery.yaml
>     A   krib/params/docker-daemon.yaml
>
>     commit 332481f8802011615c3ecf10013394e493ce83dd
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sun Oct 7 23:02:07 2018 -0500
>
>         change icons
>
>     M   krib/params/contrail-docker-registery.yaml
>     M   krib/stages/krib-contrail.yaml
>     M   krib/tasks/krib-contrail.yaml
>
>     commit 02652ea627470dcc50e66203195aa38235edd39f
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sun Oct 7 22:59:59 2018 -0500
>
>         move docker daemon change to docker install
>
>     M   krib/templates/docker-install.sh.tmpl
>     M   krib/templates/krib-contrail.sh.tmpl
>
>     commit 7e3753b1a0230cdb0adb1cf6024894ecb7cb226b
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Oct 5 21:50:11 2018 -0500
>
>         tweaks from debug
>
>     M   krib/templates/contrail.cfg.tmpl
>     M   krib/templates/krib-contrail.sh.tmpl
>
>     commit e69398771c2304777d8bbd0a0adbbf6bc240b54b
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Oct 5 19:38:43 2018 -0500
>
>         update for new release
>
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>
>     commit 49066dd0e51be80bcfd046374b31f8fd4ad8fbd2
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Oct 5 19:30:19 2018 -0500
>
>         cleanups from testing
>
>     M   krib/tasks/krib-contrail.yaml
>     M   krib/templates/krib-contrail.sh.tmpl
>
>     commit 9d364816722c85f2428e2a74cd2e21d2e0006bab
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Oct 5 19:02:29 2018 -0500
>
>         remove helm, put contrail install in right place.  NOT TESTED
>
>     M   krib/tasks/krib-contrail.yaml
>     M   krib/templates/krib-contrail.sh.tmpl
>
>     commit b7afca68cbf03e5772d75fd2d7c2a9b434687497
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Oct 5 18:47:41 2018 -0500
>
>         WIP contrail bones for basic install
>
>     A   krib/params/contrail-docker-registery.yaml
>     A   krib/stages/krib-contrail.yaml
>     A   krib/tasks/krib-contrail.yaml
>     A   krib/templates/contrail.cfg.tmpl
>     A   krib/templates/krib-contrail.sh.tmpl
>
>     commit 8d0f58b58f6b828fd3bf46045c4b65e3239ad602
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Oct 4 18:19:18 2018 -0500
>
>         Update to ipv6 capable sledgehammer.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 446fcbdf1a45234cdb2665a36ebe2c5019a95c0e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 3 22:03:57 2018 -0500
>
>         Changes to support curl with ipv6 addresses.
>
>         Also udpate the IP discovery to use an IPv6 address as well.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     M   content/templates/setup.tmpl
>     M   krib/templates/krib-config.sh.tmpl
>     M   krib/templates/krib-helm.sh.tmpl
>
>     End of Note

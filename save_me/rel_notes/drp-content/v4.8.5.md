v4.8.5 {#rs_rel_notes_drp-content_v4.8.5}
======

>     commit f35959872927d2b46c74e52a788dd975da82cbfe
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Feb 2 20:24:08 2022 -0600
>
>         fix(content.package-repo): centos 8 mirror fixes
>
>         Fix the CentOS 8 mirror debacle by using vault as a workaround
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     M   content/params/package-repositories.yaml
>
>     End of Note

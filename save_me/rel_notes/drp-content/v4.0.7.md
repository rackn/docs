v4.0.7 {#rs_rel_notes_drp-content_v4.0.7}
======

>     commit 8229e62978079ef9b719b72e2ad830fdc9edbef9
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Aug 13 23:41:25 2019 +1200
>
>         Protect mounted filesystems
>
>     M   krib/tasks/krib-dev-reset.yaml
>
>     commit bc0e9b0e3976b96bcd023380c9c7208e9bf8fbbf
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Aug 13 22:32:22 2019 +1200
>
>         Improve ceph resetting
>
>     M   krib/tasks/krib-dev-reset.yaml
>
>     commit e9ca777f68bff570aafb431efbf57561faeae612
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Aug 13 10:30:06 2019 +1200
>
>         Permit reset with custom user/password
>
>     A   krib/params/unsafe-rs-password.yaml
>     A   krib/params/unsafe-rs-username.yaml
>     M   krib/tasks/krib-dev-hard-reset.yaml
>     M   krib/tasks/krib-dev-reset.yaml
>
>     commit f7a77cb0c3ed0092e059450dd32ca9ae37989c87
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Sun Aug 11 14:40:25 2019 +1200
>
>         As you weeeeesh
>
>     D   krib/templates/krib-consul-install.sh-orig-deleteme.tmpl
>
>     commit 3282a9bc8c4d685b451b23393445c5b30c8722d9
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Sun Aug 11 14:39:07 2019 +1200
>
>         Standardize Ceph manifests to 1.0
>
>     A   krib/templates/krib-consul-install.sh-orig-deleteme.tmpl
>     M   krib/templates/krib-rook-ceph.sh.tmpl
>
>     End of Note

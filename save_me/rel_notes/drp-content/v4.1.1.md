v4.1.1 {#rs_rel_notes_drp-content_v4.1.1}
======

>     commit 8eedafd853f00af0398b8b98d35872e8d514d0d3
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Oct 14 20:09:12 2019 -0500
>
>         Check for install RPM and skip install if already present
>
>     M   flash/tasks/hpe-firmware-flash-list.yml
>
>     commit d61c843abbfd5628fa041f9f8a87cf9bdc88c14a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Oct 7 15:34:41 2019 -0500
>
>         Add more fields that others can have fun with.
>
>     M   task-library/params/inventory-collect.yaml
>     A   task-library/params/inventory-dimm-sizes.yaml
>     A   task-library/params/inventory-dimms.yaml
>     A   task-library/params/inventory-nic-info.yaml
>
>     commit 084cc40779a3e98368a308c24ec0f67974f1c1c2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 2 14:53:48 2019 -0500
>
>         Add more pieces to the default inventory system.
>
>     M   task-library/params/inventory-collect.yaml
>
>     commit f9a3a1e3866424c0a870643940e1eb965a628b30
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 2 14:55:42 2019 -0500
>
>         Update to v4.1.0 provision
>
>     M   go.mod
>     M   go.sum
>
>     commit 806e85f62f210bbc7501ebf9d5d9e9a6b7b539e3
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 27 11:58:45 2019 -0700
>
>         Add flexiflow workflow capability
>
>     M   flexiflow/._Documentation.meta
>     A   flexiflow/params/flexiflow-maintenance-mode.yaml
>     A   flexiflow/stages/flexiflow-workflow.yaml
>     A   flexiflow/stages/test-flexiflow-start.yaml
>     A   flexiflow/stages/test-flexiflow-stop.yaml
>     A   flexiflow/stages/test-flexiflow-workflow.yaml
>     A   flexiflow/tasks/flexiflow-workflow.yaml
>     A   flexiflow/tasks/test-flexiflow-start.yaml
>     A   flexiflow/tasks/test-flexiflow-stop.yaml
>     A   flexiflow/workflows/test-flexiflow-workflow-include.yaml
>     A   flexiflow/workflows/test-flexiflow-workflow-main.yaml
>
>     commit cf443a14cd971cc1025a52827b21c3dc2951bf83
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Sep 27 13:02:06 2019 -0500
>
>         Add flexiflow to published pieces
>
>     M   tools/pieces.sh
>
>     commit bc0e90798875ffdeb3e7c0bd606080554916b319
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 27 10:54:23 2019 -0700
>
>         Add flexiflow content pack
>
>     A   flexiflow/._Author.meta
>     A   flexiflow/._CodeSource.meta
>     A   flexiflow/._Color.meta
>     A   flexiflow/._Copyright.meta
>     A   flexiflow/._Description.meta
>     A   flexiflow/._DisplayName.meta
>     A   flexiflow/._DocUrl.meta
>     A   flexiflow/._Documentation.meta
>     A   flexiflow/._Icon.meta
>     A   flexiflow/._License.meta
>     A   flexiflow/._Name.meta
>     A   flexiflow/._Order.meta
>     A   flexiflow/._Source.meta
>     A   flexiflow/._Tags.meta
>     A   flexiflow/params/flexiflow-list-parameter.yaml
>     A   flexiflow/params/test-flexiflow-parameter.yaml
>     A   flexiflow/stages/flexiflow-stage.yaml
>     A   flexiflow/stages/test-flexiflow-stage.yaml
>     A   flexiflow/tasks/flexiflow-start.yaml
>     A   flexiflow/tasks/flexiflow-stop.yaml
>     A   flexiflow/tasks/test-flexiflow-task1.yaml
>     A   flexiflow/tasks/test-flexiflow-task2.yaml
>
>     commit 97c5da7f0855ffb4c6a2be2720f502974b3cd4ac
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 21:01:59 2019 -0700
>
>         fix validatin spelling error
>
>     M   validation/._Documentation.meta
>
>     commit bde716bdc9aa19ce42a36184a7b25062ea9ba86e
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 20:41:53 2019 -0700
>
>         remove errant ':wq' file
>
>     D   test-validation/:wq
>
>     commit 3ff664a83b51c564cbc4d5827db5897e82213598
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 20:36:05 2019 -0700
>
>         fix stage/task/job prefix message part
>
>     M   validation/templates/validation-lib.tmpl
>
>     commit c1c20321664514d5c7a8dc979fd43a954bdab480
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 20:19:55 2019 -0700
>
>         Update ._Documentation with Return codes
>
>     M   validation/._Documentation.meta
>
>     commit 18049b574e78951699fe229ca2be0f8c3cc2f162
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 19:49:50 2019 -0700
>
>         cleanups to test validation
>
>     A   test-validation/params/test-validation-fail-and-ignore.yaml
>     A   test-validation/params/test-validation-fail-at-stage-end.yaml
>     A   test-validation/params/test-validation-fail-immediately.yaml
>     D   test-validation/params/test-validation-remdiated.yaml
>     A   test-validation/params/test-validation-remediated-fail-immediately.yaml
>     A   test-validation/params/test-validation-remediated.yaml
>     A   test-validation/params/test-validation-success.yaml
>     M   test-validation/stages/test-validation-fail-and-ignore.yaml
>     M   test-validation/stages/test-validation-fail-at-stage-end.yaml
>     M   test-validation/stages/test-validation-fail-immediately.yaml
>     M   test-validation/stages/test-validation-remediated-fail.yaml
>     M   test-validation/stages/test-validation-success.yaml
>     M   test-validation/tasks/test-validation-fail-and-ignore.yaml
>     M   test-validation/tasks/test-validation-fail-immediately.yaml
>     D   test-validation/tasks/test-validation-set-remediated.yaml
>
>     commit 9a4bd23d25a0f61d489c7a89127e4ea6d0816aec
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 18:46:30 2019 -0700
>
>         Add test content for validation framework
>
>     A   test-validation/._Author.meta
>     A   test-validation/._CodeSource.meta
>     A   test-validation/._Color.meta
>     A   test-validation/._Copyright.meta
>     A   test-validation/._Description.meta
>     A   test-validation/._DisplayName.meta
>     A   test-validation/._DocUrl.meta
>     A   test-validation/._Documentation.meta
>     A   test-validation/._Icon.meta
>     A   test-validation/._License.meta
>     A   test-validation/._Name.meta
>     A   test-validation/._Order.meta
>     A   test-validation/._Prerequisites.meta
>     A   test-validation/._Source.meta
>     A   test-validation/._Tags.meta
>     A   test-validation/:wq
>     A   test-validation/params/test-validation-remdiated.yaml
>     A   test-validation/stages/test-validation-fail-and-ignore.yaml
>     A   test-validation/stages/test-validation-fail-at-stage-end.yaml
>     A   test-validation/stages/test-validation-fail-immediately.yaml
>     A   test-validation/stages/test-validation-remediated-fail.yaml
>     A   test-validation/stages/test-validation-success.yaml
>     A   test-validation/tasks/test-validation-clear-remediated.yaml
>     A   test-validation/tasks/test-validation-fail-and-ignore.yaml
>     A   test-validation/tasks/test-validation-fail-at-stage-end.yaml
>     A   test-validation/tasks/test-validation-fail-immediately.yaml
>     A   test-validation/tasks/test-validation-set-remediated.yaml
>     A   test-validation/tasks/test-validation-success.yaml
>     A   test-validation/workflows/test-validation-fail-and-ignore.yaml
>     A   test-validation/workflows/test-validation-fail-at-stage-end.yaml
>     A   test-validation/workflows/test-validation-fail-immediately.yaml
>     A   test-validation/workflows/test-validation-remediated-fail.yaml
>     A   test-validation/workflows/test-validation-success.yaml
>
>     commit 55543adf807875b25a1a2478adc4a8b0d4b493b3
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 26 18:45:11 2019 -0700
>
>         Cleanup for return code changes
>
>     A   validation/params/validation-errors-ignore.yaml
>     A   validation/tasks/validation-clear-errors-ignore.yaml
>     D   validation/tasks/validation-test-fail.yaml
>     D   validation/tasks/validation-test-fatal.yaml
>     D   validation/tasks/validation-test-success.yaml
>     M   validation/templates/validation-lib.tmpl
>
>     commit 9f99303bd8bcbcad08e7ddf6fd9fe596a14e208b
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 26 09:14:03 2019 -0500
>
>         operating-system-disk has a sane default, so rely on it existing
>
>     M   content/templates/centos-7.ks.tmpl
>     M   content/templates/part-scheme-default.tmpl
>
>     commit 74c24764320ae833358175a50399fd2055eeb71c
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 26 09:12:13 2019 -0500
>
>         Silence some overly loud scripts and tasks
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     M   content/tasks/gohai.yaml
>
>     commit b104a1fa51f53b034b0f3b6a14b26322f6b7ecff
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 26 09:11:06 2019 -0500
>
>         kernel-console and kernel-options params have defaults, no need to wrap them in if blocks
>
>     M   content/bootenvs/centos-7.6.1810.yml
>     M   content/bootenvs/centos-7.yml
>     M   content/bootenvs/debian-8.yml
>     M   content/bootenvs/debian-9.yml
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     M   content/bootenvs/ubuntu-16.04.yml
>     M   content/bootenvs/ubuntu-18.04-arm64-hwe.yml
>     M   content/bootenvs/ubuntu-18.04.yml
>     M   contrib/bootenvs/centos-7.3.1611.yml
>     M   coreos/bootenvs/coreos-2135.6.0-disk.yaml
>     M   coreos/bootenvs/coreos-2135.6.0-live.yaml
>     M   os-other/bootenvs/redhat-6.5.yml
>     M   os-other/bootenvs/redhat-7.0.yml
>     M   rancheros/bootenvs/rancheros-disk.yaml
>     M   rancheros/bootenvs/rancheros-latest.yaml
>     M   sledgehammer-builder/bootenvs/build-sledgehammer.yaml
>
>     commit 90994234a63a2366a4c5460df5247dbe96eaeeaa
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Sep 24 12:52:53 2019 -0700
>
>         Cleanup doc some
>
>     M   validation/._Documentation.meta
>     M   validation/params/validate-nic-required-up.yaml
>     M   validation/stages/validation-post-discover.yaml
>     M   validation/tasks/validate-nic-counts.yaml
>
>     commit 10c5dff8f78604db0e16b241db4588ec0d2a10bc
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Sep 23 21:18:11 2019 -0700
>
>         Update validation documentation
>
>     M   validation/._Documentation.meta
>     M   validation/params/validation-errors.yaml
>     M   validation/params/validation-list-parameter.yaml
>     M   validation/params/validation-post-discover.yaml
>     M   validation/params/validation-post-hardware.yaml
>     M   validation/params/validation-post-install.yaml
>     M   validation/stages/validation-post-discover.yaml
>     M   validation/stages/validation-post-hardware.yaml
>     M   validation/stages/validation-post-install.yaml
>     M   validation/tasks/validation-start.yaml
>     M   validation/tasks/validation-stop.yaml
>     M   validation/tasks/validation-test-fail.yaml
>     M   validation/tasks/validation-test-fatal.yaml
>     M   validation/tasks/validation-test-success.yaml
>
>     commit d95a108516e8b39d8d3f00443f32a15a2cc9dc81
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Tue Sep 24 10:38:18 2019 +0700
>
>         downgrade to 1.15.4 for better chart compatibility
>
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>
>     commit f296da76331d2aa19e3bd9ee81c849c5d3525676
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Sep 23 09:52:51 2019 -0700
>
>         remove change-stage/map, add workflows
>
>     D   image-builder/profiles/image-builder-ubuntu-14.04-start.yaml
>     D   image-builder/profiles/image-builder-ubuntu-16.04-start.yaml
>     D   image-builder/profiles/image-builder-ubuntu-18.04-start.yaml
>     A   image-builder/workflows/image-builder-ubuntu-bionic.yaml
>
>     commit c7242901831b417acfb674d98fe10d54d0f8e9d2
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Mon Sep 23 23:48:59 2019 +0700
>
>         move download functions to task-library
>
>     M   krib/templates/docker-install.sh.tmpl
>     M   krib/templates/krib-lib.sh.tmpl
>     A   task-library/templates/download-tools.tmpl
>
>     commit 6f8b66cf256db584c093e7809d18f1e71e7f15f6
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Sep 23 09:47:48 2019 -0700
>
>         Remove the Bionic Beaver beta bootenv (BBBB) references
>
>     M   image-builder/profiles/image-builder-ubuntu-18.04-start.yaml
>     M   image-builder/stages/ubuntu-18.04-install-builder.yaml
>
>     commit a007e7ac4d1cacda09fd332b087a582bfee932a5
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sun Sep 22 00:06:22 2019 +0700
>
>         remove not needed serviceaccount re-applying
>
>     M   krib/templates/krib-dashboard.sh.tmpl
>
>     commit d6e0d36142890c656d87c2d6d2b8dcce7a8a19d7
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sat Sep 21 23:56:50 2019 +0700
>
>         lint dashboard admin cat yaml
>
>     M   krib/templates/krib-dashboard.sh.tmpl
>
>     commit 3bda77d7da2c932d42e51ed4c4ca7fd4faac2af0
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sat Sep 21 22:44:46 2019 +0700
>
>         calico 3.9 update
>
>     M   krib/params/provider-calico-config.yaml
>
>     commit 2525c6fa50a198160f2fb42de883e85712c1e073
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sat Sep 21 22:41:58 2019 +0700
>
>         cni 0.8.2 update
>
>     M   krib/params/krib-cluster-cni-version.yaml
>
>     commit 634f1f6567ca0e5865f6210dbc7bacd46a59660f
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sat Sep 21 22:39:13 2019 +0700
>
>         update various versions
>
>     M   krib/params/docker-version.yaml
>     M   krib/params/etcd-version.yaml
>     M   krib/params/krib-cluster-cni-version.yaml
>     M   krib/params/krib-cluster-crictl-version.yaml
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>
>     commit 42415e79daffbb4d11a12cc0786279207df4c096
>     Author: Adam Lemanski <adam.lemanski@gmail.com>
>     Date:   Sat Sep 21 22:13:04 2019 +0700
>
>         use download & with_backoff function for docker script
>
>     M   krib/templates/docker-install.sh.tmpl
>
>     commit 801eef9ae41e168af570d7b3866b7e9825fb18b2
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 20 14:33:58 2019 -0700
>
>         fix bad version references (7.6.19908)
>
>     M   content/bootenvs/centos-7.7.1908.yml
>
>     commit 5a9608310a3500bf78c9d43332595e56f0c670cd
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 20 14:29:07 2019 -0700
>
>         add missed stage for 7.7.1908
>
>     A   content/stages/centos-7.7.1908.yml
>
>     commit 3353494ffc1ae480943da62711170edd0cd801e4
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 20 14:10:39 2019 -0700
>
>         add centos-7.7.1908, shift c7 to it
>
>     M   content/bootenvs/centos-7.6.1810.yml
>     A   content/bootenvs/centos-7.7.1908.yml
>     M   content/bootenvs/centos-7.yml
>
>     commit c1b9ca761e1910e40fb5679bada0f99630ebae47
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 20 13:42:29 2019 -0700
>
>         add documentation pieces
>
>     M   validation/._Documentation.meta
>     M   validation/tasks/validate-nic-counts.yaml
>
>     commit c32d52a4af2140ac41ee8f8bfe89d10db5f77728
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 20 13:34:35 2019 -0700
>
>         Move validate nic counts to validation
>
>     A   validation/params/validate-nic-counts.yaml
>     A   validation/params/validate-nic-required-up.yaml
>     A   validation/tasks/validate-nic-counts.yaml
>
>     commit f389f08280114aa10bf192bcf9d811d06bf2c12a
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Sep 19 22:26:32 2019 -0700
>
>         add validate_nic_counts to validation libarary
>
>     M   validation/templates/validation-lib.tmpl
>
>     commit 475a0a1b3ec87d9e3f7e38b14150cf2fd74c8c69
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 15:19:17 2019 +1200
>
>         Restart containerd if it was already running
>
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit 1b0074fb6563fa7e4d72435f7059a76931dd3790
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 15:10:40 2019 +1200
>
>         Override ExecStart properly
>
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit 28976d20849167f66e763388c1a8914116fc815e
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 15:02:51 2019 +1200
>
>         Be idempotent
>
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit 308261904882f6f63f35a57efd3a44546e882a9c
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 15:00:13 2019 +1200
>
>         Fix wrong param
>
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit 22f34f1ad12a5b164e9edd763ffe9af2e5ee30b1
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 14:54:01 2019 +1200
>
>         Damn those races
>
>     M   krib/templates/consul-agent-install.sh.tmpl
>     M   krib/templates/consul-server-install.sh.tmpl
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit e2c228ec239444e625223dd64ce70c5eb9e75cf8
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Wed Sep 18 13:45:33 2019 +1200
>
>         Add option to customize loglevel
>
>     A   krib/params/containerd-loglevel.yaml
>     M   krib/templates/containerd-install.sh.tmpl
>
>     commit 1eb7599e23e6ecdbf7c35616067ec87ba5619c88
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Sep 17 10:47:56 2019 +1200
>
>         Bump vault version
>
>     M   krib/params/vault-version.yaml
>
>     commit a99198a7e74bb3946a830f05f73fe6f380f1f213
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Sep 17 10:47:12 2019 +1200
>
>         Update consul version to 1.6.1
>
>     M   krib/params/consul-version.yaml
>
>     commit e54b434b15ff724be64d1aeb8a364cc86261e07d
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Tue Sep 17 10:45:54 2019 +1200
>
>         Change consul agent to DEBUG mode
>
>     M   krib/templates/consul-agent.json.tmpl
>
>     commit 6e868554153437a76edda67b4237fb6aff0c275e
>     Author: David Young <davidy@funkypenguin.co.nz>
>     Date:   Sat Aug 31 12:09:53 2019 +1200
>
>         Dont commit bundled krib.yaml
>
>     M   .gitignore
>
>     End of Note

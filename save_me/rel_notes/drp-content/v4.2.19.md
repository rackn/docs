v4.2.19 {#rs_rel_notes_drp-content_v4.2.19}
=======

>     commit 6031390d74082ce38424d4b6c673c4aa07e0e303
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 8 10:53:41 2020 -0500
>
>         fix(flash): after resetting mc wait for it to take effect
>
>     M   flash/tasks/lenovo-firmware-flash-list.yml
>
>     End of Note

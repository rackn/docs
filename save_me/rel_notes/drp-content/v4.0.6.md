v4.0.6 {#rs_rel_notes_drp-content_v4.0.6}
======

>     commit ce7a0a790bfc0b10f2d83c5d3830547f399af397
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Aug 10 15:38:09 2019 -0500
>
>         Remove drbundler usage and convert to drpcli
>
>     M   tools/package.sh
>
>     commit 53072854f826312c06e144b2d603790fa913efe5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Aug 10 13:28:42 2019 -0500
>
>         Fix doc links
>
>     M   burnin/._DocUrl.meta
>     M   classify-tests/._DocUrl.meta
>     M   classify/._DocUrl.meta
>     M   content/._DocUrl.meta
>     M   contrib/._DocUrl.meta
>     M   coreos/._DocUrl.meta
>     M   dell-support/._DocUrl.meta
>     M   dev-library/._DocUrl.meta
>     M   drp-prom-mon/._DocUrl.meta
>     M   flash/._DocUrl.meta
>     M   hardware-tooling/._DocUrl.meta
>     M   hpe-support/._DocUrl.meta
>     M   image-builder/._DocUrl.meta
>     M   krib/._DocUrl.meta
>     M   kubespray/._DocUrl.meta
>     M   lenovo-support/._DocUrl.meta
>     M   opsramp/._DocUrl.meta
>     M   os-other/._DocUrl.meta
>     M   rancheros/._DocUrl.meta
>     M   rose/._DocUrl.meta
>     M   task-library/._DocUrl.meta
>     M   terraform/._DocUrl.meta
>
>     End of Note

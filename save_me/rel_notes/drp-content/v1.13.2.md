v1.13.2 {#rs_rel_notes_drp-content_v1.13.2}
=======

>     commit 5e060684d6a9a1201a792c7567e894ad45f56554
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 16 16:41:38 2019 -0500
>
>         Add A lock stage that can lock a machine
>
>     A   content/stages/lock-machine.yaml
>     A   content/tasks/lock-machine.yaml
>
>     commit 204185d19fbda02529fb7a0273c3f7103271f080
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 16 16:18:18 2019 -0500
>
>         Update discovery default.ipxe to exit fail from ipxe
>         if it ever gets control back from an ipxe file.
>
>         This allows the bios/uefi to continue booting systems.
>
>     M   content/bootenvs/discovery.yml
>
>     commit dacc1abb288c76867afd22debef816ee8566d819
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon May 13 21:59:31 2019 -0500
>
>         Use what ROb choose for the UX
>
>     R095    content/params/ux-catalog-url.yaml  content/params/catalog_url.yaml
>
>     commit ead6c37f84d0ccfb57dc0fcfd7b1068a7b485d21
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 10 15:47:40 2019 -0500
>
>         Revert "Arrange for Sledgehammer to be able to load from a disk along with being able to load from the network"
>
>         This reverts commit 5615578c1a9a5639cc4df1054b9d2bce4565d3b9.
>
>     M   sledgehammer-builder/tasks/sledgehammer-stage-bits.yaml
>
>     commit a2568ab7915e78fd57c4a639b4961f19b318360e
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 10 15:43:48 2019 -0500
>
>         Add the ability to boot Sledgehammer from a local disk.
>
>         This is mainly indended to be used for testing and other uses.
>
>     A   content/stages/write-sledgehammer-to-disk.yaml
>     A   content/tasks/write-sledgehammer-to-disk.yaml
>
>     commit 56e92d2bb997a345e42d3d0891d44ef63fa43753
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 10 15:32:16 2019 -0500
>
>         Fix busted booting via ipxe in discovery mode, and add a default for operating-system-disk
>
>     M   content/bootenvs/discovery.yml
>     M   content/params/operating-system-disk.yaml
>
>     commit 5615578c1a9a5639cc4df1054b9d2bce4565d3b9
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 10 09:19:23 2019 -0500
>
>         Arrange for Sledgehammer to be able to load from a disk along with being able to load from the network
>
>     M   sledgehammer-builder/tasks/sledgehammer-stage-bits.yaml
>
>     commit 9c2aaeee4dc01dfde69d1c3dda9d8104b707b01d
>     Author: Adam Lemanski <adam.lemanski@make-a-bag.com>
>     Date:   Tue May 7 10:42:49 2019 +0700
>
>         update docker, etcd, cni, crictl, kubernetes
>
>     M   krib/params/docker-version.yaml
>     M   krib/params/etcd-version.yaml
>     M   krib/params/krib-cluster-cni-version.yaml
>     M   krib/params/krib-cluster-crictl-version.yaml
>     M   krib/params/krib-cluster-kubernetes-version.yaml
>
>     commit 6a3394f8b2b83ceeff1bc62f986a69540a7c4ad1
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 2 16:57:05 2019 -0500
>
>         Have SLedgehammer always update the Address on the machine if it is not what we expect
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit a3e77f9c332f5c0d9b33c937edce1d29e11b4f92
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed May 1 16:53:01 2019 -0500
>
>         Fix up grub support to account for EFI
>
>     M   content/bootenvs/discovery.yml
>     M   content/templates/default-grub.tmpl
>
>     commit 426f5df7485381475c46e30e0b288acc48744e26
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Apr 8 11:12:45 2019 -0500
>
>         There is no updated systemd for centos altarches that I can see.
>
>     M   sledgehammer-builder/tasks/sledgehammer-stage-bits.yaml
>
>     commit aa647bb9cb6255dd8cb0269788528d3915b6bbf1
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed May 1 13:55:25 2019 -0500
>
>         Rename file to good name
>
>     R100    content/params/ux-catalog.url.yaml  content/params/ux-catalog-url.yaml
>
>     commit 95ca38c0db320b64f69b1b6b0499b109b3928165
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed May 1 13:54:53 2019 -0500
>
>         Add ux-catalog-url as well.
>
>     A   content/params/ux-catalog.url.yaml
>
>     commit 15e8234fb3c75704b0200196fd7152f07e004fb5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed May 1 11:33:41 2019 -0500
>
>         Add ux-air-gap parameter
>
>     A   content/params/ux-air-gap.yaml
>
>     commit aa0252fbc9d334924930dc0c1229ce062f2fb80f
>     Author: Ben Agricola <bagricola@squiz.co.uk>
>     Date:   Wed May 1 12:55:09 2019 +0200
>
>         Make sure docker config dir exists before trying to create daemon.json
>
>         Signed-off-by: Ben Agricola <bagricola@squiz.co.uk>
>
>     M   krib/templates/docker-install.sh.tmpl
>
>     commit 02105256f2f085aa96253a27e1b02d48ef034937
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Apr 29 15:33:01 2019 -0500
>
>         Always reset the OS for sledgehammer
>
>     M   content/tasks/enforce-sledgehammer.yaml
>
>     commit 335442f7088f25c8cd030f16b58b6bb11b5e83ca
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Apr 29 15:29:15 2019 -0500
>
>         Set the OS to centos-7 when in sledgehammer.
>
>     M   content/tasks/enforce-sledgehammer.yaml
>
>     commit 3577a8bae0643a3db475ef5d1b6322b4c30bd333
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Apr 29 15:23:30 2019 -0500
>
>         Add stage that allows the repos to be configured in centos based systems
>         based upon the package-repositories
>
>     A   content/stages/centos-setup-repos.yaml
>     M   content/templates/centos-drp-only-repos.sh.tmpl
>
>     commit 3ff3d05b9252b2c9a6b67b096837296e7a0b3554
>     Author: Adam Lemanski <adam.lemanski@make-a-bag.com>
>     Date:   Thu Apr 25 09:19:55 2019 +0700
>
>         update calico to version 3.6
>
>     M   krib/params/provider-calico-config.yaml
>
>     commit 22198bd2ccec28bd6ad699f05e80e748f0631819
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 23 10:00:34 2019 -0500
>
>         Remove greg-play
>
>     D   greg-play/._Description.meta
>     D   greg-play/._Name.meta
>
>     commit b98b8b6c81f1b57c8e82e70223ef2a5f1c4ba999
>     Author: Adam Lemanski <adam.lemanski@make-a-bag.com>
>     Date:   Mon Apr 22 14:02:39 2019 +0700
>
>         use -o since -O is used to set remote name as file name
>
>     M   krib/templates/etcd-config.sh.tmpl
>
>     commit 010e488540673ec65f2281064d15ee1dc3b9ad91
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Apr 17 21:23:24 2019 -0500
>
>         Add proxy setting template.
>
>     A   content/templates/set-proxy-servers.sh.tmpl
>
>     commit d0f5177bbd96024890a326da572ea1729013c8f5
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Apr 12 19:41:30 2019 -0400
>
>         add hostname param
>
>     A   content/params/hostname.yaml
>
>     commit 3d14c153e3c99ae669a49bef8e53d36a40fdcb0b
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Apr 12 00:45:23 2019 -0400
>
>         add dns search domains and servers
>
>     A   content/params/dns-search-domains.yaml
>     A   content/params/dns-servers.yaml
>
>     End of Note

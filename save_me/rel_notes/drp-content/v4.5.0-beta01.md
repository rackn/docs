v4.5.0-beta01 {#rs_rel_notes_drp-content_v4.5.0-beta01}
=============

>     commit f3142387e7f2412b4864f3b0950da72221e65ba9
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 22 21:48:16 2020 -0500
>
>         build: update travis
>
>     M   .travis.yml
>
>     commit 532e6f0b6d65acdc0b0f4e9b06a7c5923dc98020
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 22 21:39:24 2020 -0500
>
>         build: update go mod to v4.5.0-beta01
>
>     M   go.mod
>     M   go.sum
>
>     commit a0c317dc132d95cfbe7e1a8a13cbe9d02077ba46
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 22 21:34:56 2020 -0500
>
>         feat(centos8): add kickstart extra ifs for centos8
>
>     A   content/params/kickstart.extra-ifs.yaml
>     M   content/templates/centos-8.ks.tmpl
>
>     commit a98fcf08f1cdcee22db5d939c34d826866020c92
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Sep 22 08:46:00 2020 -0500
>
>         fix(sledgehammer): Add back increased wget timeouts.
>
>         This adds the increased timeouts for wget back to the sledgehammer
>         stage 1 initramfs.
>
>         It also adds an override for a centos 8 based Sledgehammer.  This will
>         become the default version of Sledgehammer in a future release of
>         Rebar.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>     A   content/profiles/sledgehammer-0b8f38aeecc5a40f915758d9af47102fee6fb83e.yaml
>
>     commit 0b14dfae0aeac42ba584d3e0d188188e69d4a036
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Sep 21 17:10:54 2020 -0500
>
>         doc: Fix typo
>
>     M   content/params/sledgehammer.working-python.yaml
>
>     commit 5936461a6ce19f539229484ed226cbd75901e587
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Sep 18 11:09:15 2020 -0500
>
>         feat(sledgehammer): Bring back the Centos7 Sledgehammer builder.
>
>         This is more of an insurance policy thing than anything else, for
>         folks that will continue to need a CentOS 7 based Sledgehammer.  This
>         incarnation does not include the customizability refactoring that the
>         cent8 Sledgehamemr builder has -- those can be cross-ported if there
>         is sufficient demand.
>
>     A   sledgehammer-builder-centos-7/._Author.meta
>     A   sledgehammer-builder-centos-7/._Color.meta
>     A   sledgehammer-builder-centos-7/._Copyright.meta
>     A   sledgehammer-builder-centos-7/._Description.meta
>     A   sledgehammer-builder-centos-7/._DisplayName.meta
>     A   sledgehammer-builder-centos-7/._Documentation.meta
>     A   sledgehammer-builder-centos-7/._Icon.meta
>     A   sledgehammer-builder-centos-7/._License.meta
>     A   sledgehammer-builder-centos-7/._Name.meta
>     A   sledgehammer-builder-centos-7/._Prerequisites.meta
>     A   sledgehammer-builder-centos-7/._RequiredFeatures.meta
>     A   sledgehammer-builder-centos-7/._Source.meta
>     A   sledgehammer-builder-centos-7/._Tags.meta
>     A   sledgehammer-builder-centos-7/bootenvs/build-sledgehammer.yaml
>     A   sledgehammer-builder-centos-7/params/sledgehammer.extra-ifs.yaml
>     A   sledgehammer-builder-centos-7/stages/sledgehammer-build.yaml
>     A   sledgehammer-builder-centos-7/tasks/sledgehammer-stage-bits.yaml
>     M   sledgehammer-builder/tasks/sledgehammer-create-image.yaml
>     M   tools/pieces.sh
>
>     commit 57d07e8243fafffb33c3725c7cc3e3e6a03d1b10
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 26 12:30:13 2020 -0500
>
>         feat(sledge8): Update cent8 sledgehammer to use python2 by default.
>
>         This also adds an extra dependency that the Dell firmware update process
>         requires.  Still needs testing on an HP box.
>
>         It also adds a task/param combo for setting the default Python interpreter
>         on a running Sledgehammer.
>
>     A   content/params/sledgehammer.working-python.yaml
>     A   content/tasks/sledgehammer-set-working-python.yaml
>
>     commit 4fe1cd0d52f23fe692bd6d75fa5ae239c1050fc2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Sep 17 15:48:28 2020 -0500
>
>         fix(task-library): update dr-server-install to handle some races
>
>         This addresses some creating of race conditions.
>         Updates the HA pieces to work better with manager.
>
>     M   task-library/params/dr-server-ha-token.yaml
>     M   task-library/tasks/dr-server-install.yaml
>     M   task-library/tasks/terraform-apply.yaml
>     D   task-library/templates/dr-server-ha.conf.tmpl
>
>     commit bd848c70eb1008ce4668a041092ecf51c1f6233d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Sep 16 12:34:07 2020 -0500
>
>         fix(image-builder): add prereq on os-other for image-builder
>
>     M   image-builder/._Prerequisites.meta
>
>     commit 071cda6d6b6d5644f560b35f737b62a65f576b0c
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Sep 11 13:39:40 2020 -0500
>
>         refactor(task-library): install ansible if missing
>
>         feature(task-library): ansible-playbooks-local can use templates in addition to github
>
>     M   task-library/params/ansible-playbooks.yaml
>     M   task-library/tasks/ansible-inventory.yaml
>     M   task-library/tasks/ansible-playbooks-local.yaml
>     A   task-library/templates/ansible-playbooks-test-playbook.yaml.tmpl
>
>     commit 4a7a5b0cdcff7df2f14d6c3c0bb2ab37a53e88c5
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Sep 11 11:55:06 2020 -0500
>
>         fix(task-library): ansible-playbooks-local needed param but it was not defined
>
>     A   task-library/params/ansible-playbooks.yaml
>     M   task-library/tasks/ansible-playbooks-local.yaml
>
>     commit fbff53fc2f9f1ccfe258bb91fa4dd2f63d7d47b0
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 4 20:45:56 2020 -0700
>
>         remove task-library by popular review
>
>     M   flash/._Prerequisites.meta
>
>     commit bbf8edbdd1be7cc4488ccd3257a8d2760c0e1f31
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri Sep 4 15:43:56 2020 -0700
>
>         enhance(flash): Add additional Prerequisites
>
>     M   flash/._Prerequisites.meta
>
>     commit e1155818af206d5fbcf32b8545834e1cb26803ff
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Wed Sep 2 20:51:42 2020 -0700
>
>         enhance(dell-support): Add DSU Mirroring tools, Fix Repo URLs
>
>     M   dell-support/params/dell-dsu-base-url.yml
>     M   dell-support/params/dell-dsu-block-release.yml
>     A   dell-support/params/dell-dsu-mirror-debug.yml
>     A   dell-support/params/dell-dsu-mirror-path.yml
>     A   dell-support/params/dell-dsu-mirror-releases.yml
>     A   dell-support/params/dell-dsu-skip-keys.yml
>     A   dell-support/params/dell-dsu-skip-releases.yml
>     A   dell-support/stages/dell-dsu-repo-mirror.yml
>     A   dell-support/tasks/dell-dsu-repo-mirror.yml
>     M   dell-support/tasks/dell-dsu-setup.yml
>
>     commit b04d96c02cd8fb20b41614b0d517092c29c750ab
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Aug 31 17:03:06 2020 -0500
>
>         bug(task-library): fix overly restrictive regex in Terraform version check
>
>     M   task-library/tasks/terraform-apply.yaml
>
>     commit e2acb505aa195d5e7d7d3d2a48a6bcea91cb37bf
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Aug 31 12:33:09 2020 -0700
>
>         enhance(image-builder): Add CentOS 8 image build/capture supoport
>
>     M   image-builder/templates/image-builder-cleanup.sh.tmpl
>     A   image-builder/workflows/image-builder-centos8.yaml
>
>     commit f4ca613dc67b927481b755defd2e6417b053e55e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 31 10:49:57 2020 -0500
>
>         feat(rpi4): Update sledgehammer for rpi4 with 8GB
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit d5f971ee2b1310645782cece4d260a855450ce95
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Aug 28 11:07:31 2020 -0500
>
>         doc(cloud-wrappers): Fix bad header heirarchy.
>
>     M   cloud-wrappers/._Documentation.meta
>
>     commit 975f52be8a647430eb0d3695365f9c27d35d72f4
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Aug 28 09:51:55 2020 -0500
>
>         feat(proxmox): add proxmox to build
>
>     M   tools/pieces.sh
>
>     commit 90853ae2501d0d11cf5298c8959bd194fa65da39
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Aug 27 22:29:42 2020 -0700
>
>         lotsa enhancements and restructuring
>
>     D   proxmox/content/params/lab-drp-external-network.yaml
>     D   proxmox/content/params/lab-drp-internal-network.yaml
>     A   proxmox/content/params/network-add-nat-bridge-template.yaml
>     A   proxmox/content/params/network-convert-interface-to-bridge-template.yaml
>     A   proxmox/content/params/network-simple-brdige-with-addressing-template.yaml
>     A   proxmox/content/params/proxmox-data-profile.yaml
>     A   proxmox/content/params/proxmox-drp-timeout-kill-switch.yaml
>     M   proxmox/content/params/proxmox-flexiflow-buster-install.yaml
>     A   proxmox/content/params/proxmox-install-drp-on-hypervisor.yaml
>     A   proxmox/content/params/proxmox-lab-drp-external-dns.yaml
>     A   proxmox/content/params/proxmox-lab-drp-external-domainname.yaml
>     A   proxmox/content/params/proxmox-lab-drp-external-interface.yaml
>     A   proxmox/content/params/proxmox-lab-drp-external-subnet.yaml
>     R089    proxmox/content/params/lab-drp-install-packages.yaml    proxmox/content/params/proxmox-lab-drp-install-packages.yaml
>     A   proxmox/content/params/proxmox-lab-drp-internal-interface.yaml
>     R067    proxmox/content/params/lab-drp-internal-subnet.yaml proxmox/content/params/proxmox-lab-drp-internal-subnet.yaml
>     M   proxmox/content/params/proxmox-lab-drp-sshkey-private.yaml
>     M   proxmox/content/profiles/EXAMPLE-lab-profile.yaml
>     A   proxmox/content/stages/proxmox-drp-install.yaml
>     A   proxmox/content/stages/proxmox-drp-provision-drp.yaml
>     A   proxmox/content/stages/proxmox-generate-ssh-key.yaml
>     R088    proxmox/content/stages/lab-drp-network.yaml proxmox/content/stages/proxmox-lab-drp-network.yaml
>     M   proxmox/content/tasks/network-add-nat-bridge.yaml
>     M   proxmox/content/tasks/network-convert-interface-to-bridge.yaml
>     A   proxmox/content/tasks/network-simple-bridge-with-addressing.yaml
>     A   proxmox/content/tasks/proxmox-drp-install.yaml
>     A   proxmox/content/tasks/proxmox-drp-provision-drp.yaml
>     A   proxmox/content/tasks/proxmox-generate-ssh-key.yaml
>     M   proxmox/content/tasks/proxmox-lab-destroy-users.yaml
>     R087    proxmox/content/tasks/lab-drp-network.yaml  proxmox/content/tasks/proxmox-lab-drp-network.yaml
>     M   proxmox/content/tasks/proxmox-lab-network.yaml
>     A   proxmox/content/templates/network-add-nat-bridge.cfg.tmpl
>     A   proxmox/content/templates/network-convert-interface-to-bridge.cfg.tmpl
>     A   proxmox/content/templates/network-simple-bridge-with-addressing.cfg.tmpl
>     M   proxmox/content/workflows/proxmox-buster-install.yaml
>     A   proxmox/content/workflows/proxmox-install-and-setup.yaml
>     R085    proxmox/content/workflows/lab-centos-image.yaml proxmox/content/workflows/proxmox-lab-centos-image.yaml
>     R079    proxmox/content/workflows/lab-drp-setup.yaml    proxmox/content/workflows/proxmox-lab-drp-setup.yaml
>     M   proxmox/content/workflows/proxmox-setup-lab.yaml
>     D   proxmox/proxmox-rackn.yaml
>
>     commit b019f1e10ec33fadcd0bb0266bb00d122a337c6a
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Aug 25 07:40:32 2020 -0700
>
>         Lots of fixes and enhancements
>
>     M   proxmox/content/._Description.meta
>     M   proxmox/content/._DisplayName.meta
>     M   proxmox/content/._Name.meta
>     A   proxmox/content/._Prerequisites.meta
>     A   proxmox/content/params/lab-drp-internal-subnet.yaml
>     A   proxmox/content/params/proxmox-flexiflow-buster-install.yaml
>     A   proxmox/content/params/proxmox-lab-nat-bridge.yaml
>     A   proxmox/content/params/proxmox-lab-nat-subnet.yaml
>     A   proxmox/content/params/proxmox-storage-device.yaml
>     A   proxmox/content/params/proxmox-storage-name.yaml
>     A   proxmox/content/profiles/EXAMPLE-lab-profile.yaml
>     A   proxmox/content/profiles/EXAMPLE-pkt-profile.yaml
>     A   proxmox/content/stages/flexiflow-buster-install.yaml
>     A   proxmox/content/stages/network-add-nat-bridge.yaml
>     A   proxmox/content/stages/network-convert-interface-to-bridge.yaml
>     A   proxmox/content/stages/proxmox-admin-account.yaml
>     A   proxmox/content/stages/proxmox-buster-installer.yaml
>     A   proxmox/content/stages/proxmox-create-storage.yaml
>     M   proxmox/content/tasks/lab-drp-network.yaml
>     A   proxmox/content/tasks/network-add-nat-bridge.yaml
>     A   proxmox/content/tasks/network-convert-interface-to-bridge.yaml
>     A   proxmox/content/tasks/proxmox-admin-account.yaml
>     A   proxmox/content/tasks/proxmox-buster-installer.yaml
>     A   proxmox/content/tasks/proxmox-create-storage.yaml
>     A   proxmox/content/tasks/proxmox-debconf-set-selections.yaml
>     M   proxmox/content/tasks/proxmox-lab-createnodes.yaml
>     A   proxmox/content/workflows/proxmox-buster-install.yaml
>     A   proxmox/content/workflows/proxmox-only-install.yaml
>
>     commit 3ccf2d7db5ec424df9583a9c64584c39dc516303
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 26 12:30:13 2020 -0500
>
>         feat(sledge8): Update cent8 sledgehammer to use python2 by default.
>
>         This also adds an extra dependency that the Dell firmware update process
>         requires.  Still needs testing on an HP box.
>
>     M   sledgehammer-builder/bootenvs/build-sledgehammer.yaml
>     A   sledgehammer-builder/params/sledgehammer-default-python.yaml
>     M   sledgehammer-builder/tasks/sledgehammer-prepare-for-image-creation.yaml
>
>     commit 5b4c34b7df5b97370237b43e6ca3db148eb95a0b
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Sun Aug 23 16:06:09 2020 -0700
>
>         fix(debian-10): Fix Debian Buster BootEnv for netcfg
>
>     M   content/bootenvs/debian-10.yml
>     A   content/params/debian-buster-netcfg.yaml
>
>     commit e3c48a4d5c230223337f8c395e6087a6aac89a13
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Aug 19 10:52:07 2020 -0500
>
>         doc(task-library): add clearer description to bootstrap-contexts
>
>     M   task-library/tasks/bootstrap-contexts.yaml
>
>     commit a8a80dc5b52daf09031a1ed438128e73f474a0f3
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Aug 18 17:03:07 2020 -0500
>
>         fix(edge-lab): correct duplicate template name
>
>     R097    edge-lab/tasks/edge-lab-bootstrap-nfs.yaml  edge-lab/tasks/edge-lab-setup-nfs.yaml
>
>     commit 76e4f17eb6b2490c27dc195af5e6224921940f03
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Aug 18 12:49:47 2020 -0500
>
>         refactor(task-lib): do not dump TF state into joblog
>
>     M   task-library/tasks/terraform-apply.yaml
>
>     commit 226cadd6d621904e0973d089985c48601cd84a71
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 17 22:20:02 2020 -0500
>
>         build: publish packer-builder
>
>     M   tools/pieces.sh
>
>     commit 1924a74af38f4461d90ca3dac6c1e47d7def09da
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Aug 14 16:57:23 2020 -0500
>
>         fix(contexts-runners): update pull to use v44 DRPCLI
>
>     M   integrations/docker-context/ansible-dockerfile
>     M   integrations/docker-context/runner-dockerfile
>     M   integrations/docker-context/terraform-dockerfile
>
>     commit ff61d35fc34864f41008e9d4df8fa4f95b8987c4
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Aug 14 14:31:17 2020 -0500
>
>         fix(task-lib): add retry for joinup to handle intermittent failures
>
>     M   task-library/tasks/ansible-join-up.yaml
>
>     commit fb57086412071a6d15365d5f547d92802fb97b35
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Aug 14 12:22:45 2020 -0500
>
>         fix(cloud-wrapper): check min tag length for linode
>
>     M   cloud-wrappers/tasks/cloud-validate.yaml
>
>     commit a2341e6ea84f0832f981f610ed689e6cf2c99ab9
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 13 15:33:04 2020 -0500
>
>         fix(task-library): allow a wider match test in license for dr-server-install
>
>     M   task-library/tasks/dr-server-install.yaml
>
>     commit 1b9b9c77836eece60556b9bc95c7adfe0e1b62d5
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Aug 12 10:58:05 2020 -0500
>
>         fix(task-library): terraform 0.13 breaks on legacy syntax, provide upgrader
>
>         fix(task-library): terraform 0.13 breaks on legacy syntax, provide upgrader safer
>
>     M   task-library/tasks/terraform-apply.yaml
>
>     commit 969ab2c798aa5a332d6f03ff83a4f5aa42312f9a
>     Author: Shane Gibson <sygibson@gmail.com>
>     Date:   Tue Aug 11 03:44:54 2020 +0000
>
>         Update README / ._Documentation.meta info
>
>     M   packer-builder/README.md
>
>     commit 6fafca93e07830d69e5eb3f8e238fe0b8a143bb6
>     Author: Shane Gibson <sygibson@gmail.com>
>     Date:   Tue Aug 11 02:59:46 2020 +0000
>
>         fix(packer-builder): Fix JSON defs for post-processor and VNC
>
>     M   packer-builder/packer/windows-10-1903.json
>     M   packer-builder/packer/windows-10-1909-uefi.json
>     M   packer-builder/packer/windows-10-1909.json
>     M   packer-builder/packer/windows-10.json
>     M   packer-builder/packer/windows-2012-r2.json
>     M   packer-builder/packer/windows-2016.json
>     M   packer-builder/packer/windows-2019-uefi.json
>     M   packer-builder/packer/windows-2019.json
>
>     commit ff201a971507f905535f3907faa4c3ca12bd24da
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Aug 7 14:49:02 2020 -0500
>
>         doc(task-library): document discover requirement for join-up
>
>         doc(task-library): document discover requirement for join-up + @glathaus comments
>
>         doc(task-library): document discover requirement for join-up + @glathaus comments2
>
>     M   .gitignore
>     M   task-library/workflows/discover-joinup.yaml
>
>     commit c65d4b35ef8478f593f9eb8a1f0887ddecb9991e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jul 31 11:07:10 2020 -0500
>
>         Revert "fix(drp-community-content): update sledgehammer with wget timeout increase"
>
>         This reverts commit a7d4691ad9637ad9508079539bf8030cb7dbf2ee.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 0a670a467b65ff35a148a4373aaa3e63b41e6e0b
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Jul 31 10:00:50 2020 -0500
>
>         Revert "fix(drp-community-content): update sledgehammer with wget timeout increase"
>
>         This reverts commit a7d4691ad9637ad9508079539bf8030cb7dbf2ee.
>
>         This Sledgeahmmer update was built with the cent8-based builder, and breaks
>         everyhting that relies on Python.
>
>     M   content/bootenvs/discovery.yml
>     M   content/bootenvs/sledgehammer.yml
>
>     commit 010907828430ce8ec95137095fd11a730c3ebe9f
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Jul 30 10:05:43 2020 -0700
>
>         bug(doc): Minor content fixes across content packs
>
>     M   content/bootenvs/ubuntu-20.04-install.yml
>     M   os-other/params/oscap-configuration.yaml
>     M   os-other/params/redhat-rhsm-organization.yaml
>     M   vmware-lib/content/._Documentation.meta
>     M   vmware-lib/content/params/govc-ova-location.yaml
>     M   vmware-lib/content/stages/govc-vsan-claim-disks.yaml
>     M   vmware-lib/content/stages/govc-wait-for-ova.yaml
>
>     End of Note

v4.9.6 {#rs_rel_notes_drp-content_v4.9.6}
======

>     commit 930deeecb82d030186fb64704d16902b7f3c2d86
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue May 3 13:01:02 2022 -0700
>
>         enhance(drp-community-content): Remove SHA256 sum for Debian ISOs - v490 branch
>
>     M   content/bootenvs/debian-10.yml
>
>     End of Note

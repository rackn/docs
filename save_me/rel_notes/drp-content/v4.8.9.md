v4.8.9 {#rs_rel_notes_drp-content_v4.8.9}
======

>     commit f9f291440a622aff82aaecd843fcb0f79686dede
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Tue Mar 22 14:11:05 2022 -0500
>
>         fix(flash) added proxy template to tasks
>
>         Added the proxy template to the tasks where it was missing.
>         Fixes zd834
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     M   flash/tasks/dell-firmware-flash-list.yml
>     M   flash/tasks/hpe-firmware-flash-list.yml
>     M   flash/tasks/lenovo-firmware-flash-list.yml
>
>     commit d9faf76699ee11ffc4f57b0814fcd81fd0999ce9
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Tue Mar 8 16:54:11 2022 -0500
>
>         fix(ci) fixing gitlab runner image
>
>     M   .gitlab-ci.yml
>
>     End of Note

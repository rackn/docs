v4.6.5 {#rs_rel_notes_drp-content_v4.6.5}
======

>     commit d1390f41ae643f59e6a39f552a8f8215af302928
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Jun 25 13:11:22 2021 -0500
>
>         fix(burnin): Tweak the disk stress test task.
>
>         Ditch incremental status updates when running badblocks.  We run them
>         all in parallel in the background, and status messages are kinda
>         pointless in that mode.
>
>         Expose a param that lets badblocks bail early after a set number of errors.
>         The default is to have it try to scan everything anyways.
>
>     A   burnin/params/burnin-disk-max-errors.yml
>     M   burnin/tasks/disk_stress.yml
>
>     End of Note

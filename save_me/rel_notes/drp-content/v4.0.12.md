v4.0.12 {#rs_rel_notes_drp-content_v4.0.12}
=======

>     commit 00a8fa49d67947b6667831de17f80142896ae7c0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Aug 27 15:31:04 2019 -0500
>
>         Only pick one setup command
>
>     M   flash/tasks/flash-list.yml
>
>     End of Note

v4.7.10 {#rs_rel_notes_drp-content_v4.7.10}
=======

>     commit d35398a72d9a95bd242d1ae745a3c145ccf3da29
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Nov 18 15:53:05 2022 -0600
>
>         fix: Make -g only present on hpe flash list when FORCE is set.
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   flash/tasks/hpe-firmware-flash-list.yml
>
>     End of Note

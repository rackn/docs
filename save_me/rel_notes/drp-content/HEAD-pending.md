nt/workflows/proxmox-install-and-setup.yaml

:   M proxmox/content/workflows/proxmox-only-install.yaml M
    task-library/params/cloud-cost-lookup.yaml A
    task-library/params/cluster-provision-kill-switch.yaml A
    task-library/params/cluster-provision-timeout.yaml A
    task-library/params/cluster-wait-filter.yaml M
    task-library/params/terraform-debug-plan.yaml M
    task-library/params/terraform-map-instance-name.yaml M
    task-library/params/terraform-map-ip-address.yaml A
    task-library/params/terraform-map-mac-address.yaml M
    task-library/params/terraform-map-private-ip-address.yaml A
    task-library/params/terraform-map-target-node.yaml M
    task-library/params/terraform-plan-instance-resource-name.yaml M
    task-library/tasks/cluster-provision.yaml M
    task-library/tasks/cluster-wait-for-members.yaml M
    task-library/tasks/inventory-cost-calculator.yaml M
    task-library/tasks/terraform-apply.yaml A
    task-library/templates/cluster-provision.sh.tmpl A
    task-library/templates/synchronize.sh.tmpl A
    task-library/templates/terraform-apply.sh.tmpl A
    task-library/templates/terraform-tf.vars.tmpl

    commit 121ad71dea013e1470045f3e10e982b3d108c266 Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Mon Oct 17 14:18:38
    2022 -0500

    > fix(ubuntuhammer): More correctness fixes
    >
    > -   Fix silently ignored bugs in the Ubuntuhammer build scripts
    > -   Fix up package and service management lists to match what
    >     actually exists on a default Ubuntu install.
    > -   Properly ignore drp-agent stuff during squashfs generation.

    M content/bootenvs/ubuntuhammer.yml M
    ubuntuhammer-builder/params/ubuntuhammer.disable-services.yaml M
    ubuntuhammer-builder/params/ubuntuhammer.unwanted-packages.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-image.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-stage-1.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-stage2.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-prepare-for-image-creation.yaml

    commit ae6ecb467fcc727985dd06294ffc55e250a72d13 Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Fri Oct 14 14:04:49
    2022 -0500

    > fix(ubuntuhammer): Shrink things down by removing unwanted
    > packages
    >
    > Zap snapd, cloud-init, and other unwanted packages from
    > ubuntuhammer.

    M ubuntuhammer-builder/params/ubuntuhammer.unwanted-packages.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-place-stage1-assets.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-prepare-for-image-creation.yaml

    commit 5c8c72de827cb49d1d04b0b26117c0c230cdb26d Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Fri Oct 14 13:06:07
    2022 -0500

    > fixup(ubuntuhammer): Fix up Ubuntuhammer builder.
    >
    > Fix the ubuntuhammer install bootenv to not reference the on-ISO
    > bootloaders since they are useless for our purposes anyways.
    >
    > Do not include the 4 gig swap image in the squashfs. Having one is
    > really pointless.

    M ubuntuhammer-builder/bootenvs/build-ubuntuhammer.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-image.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-stage-1.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-place-stage1-assets.yaml

    commit f6bd5be2191ec50bfb2f71b58a946da55dfca06e Author: Michael Rice
    \<<michael@michaelrice.org>\> Date: Sat Oct 15 23:52:45 2022 -0500

    > fix(ubuntuhammer-builder): added kexec-tools mdev
    >
    > Removed mdev stuff from dev so disk can be detected added the
    > kexec tools
    >
    > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>

    M ubuntuhammer-builder/bootenvs/build-ubuntuhammer.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-prepare-for-image-creation.yaml

    commit bf68f3f1288f8e0090b11b0c8d672b7ed2e1fae5 Author: Michael Rice
    \<<michael@michaelrice.org>\> Date: Wed Aug 24 16:24:56 2022 -0500

    > feat(content): added ubuntuhammer bootenv
    >
    > Adding ubuntuhammer bootenv
    >
    > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>

    A content/bootenvs/ubuntuhammer.yml

    commit de67babefc91eb625c4b69eb59952f43c13779ac Author: Michael Rice
    \<<michael@michaelrice.org>\> Date: Wed Oct 5 15:39:57 2022 -0500

    > fix(ubuntuhammer-builder): fix startup services
    >
    > adds aditional cleanup to ubuntuhammer removes cloud-init from
    > startup
    >
    > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>

    M ubuntuhammer-builder/params/ubuntuhammer.disable-services.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-create-stage2.yaml M
    ubuntuhammer-builder/tasks/ubuntuhammer-prepare-for-image-creation.yaml

    commit 9d72a8d227462c2ab5bb4eac25332de143155e27 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Fri Oct 14 14:54:56 2022 -0500

    > fix(dev-library): Fix WorkOrder dependency in dev-library tasks

    M dev-library/tasks/always-fails.yaml M
    dev-library/tasks/dev-cluster-cleanup.yaml M
    dev-library/tasks/dev-raise-alerts.yaml

    commit 94c59d600393992f6c43e2e6237912801e76a8e3 Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Fri Sep 23 16:36:12
    2022 -0500

    > fix(bootorder): Add regex-based fallback code for missing
    > BootCurrent
    >
    > Some terrible UEFI firmwares do not expose the BootCurrent UEFI
    > firmware variable like they should. This adds some
    > user-configurable fallback heuristics to try and pick the
    > \"right\" boot variable in that case.

    A content/params/current-boot-entry-heuristic-regex.yaml M
    content/tasks/record-current-uefi-boot-entry.yaml M
    content/tasks/reorder-uefi-boot-order.yaml M go.mod M go.sum M
    tools/publish.sh

    commit c972f7b5f1e125671f28fd79754c129bc06f2890 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Thu Aug 25 16:33:59 2022 -0400

    > feat(content): add support for ol 7.9, 8.6, and 9.0
    >
    > There are no public mirrors that include second stage initrd. We
    > only support via local repo.
    >
    > There are no minimal ISOs, so only larger ISOs supported.
    >
    > Closes rackn/product-backlog\#249
    >
    > fix(content): cleanup fedora tab alingment in ks-base-pkg param

    A content/bootenvs/ol-7-dvd-install.yml A
    content/bootenvs/ol-7.9-dvd-install.yml A
    content/bootenvs/ol-8-dvd-install.yaml A
    content/bootenvs/ol-8.6-dvd-install.yaml A
    content/bootenvs/ol-9-dvd-install.yaml A
    content/bootenvs/ol-9.0-dvd-install.yaml M
    content/params/kickstart-base-packages.yaml M
    content/params/linux-install-bootenv-map.yaml M
    content/params/linux-install-bootenv.yaml M
    content/params/package-repositories.yaml M
    content/templates/select-kickseed.tmpl

    commit 953d11ec590a20ccc5197ac18bd7967520bce196 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Oct 3 12:21:00 2022 -0500

    > fix(task-library): make bootstrap-container-engine handle
    > enable/start of service safer
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M task-library/tasks/bootstrap-container-engine.yaml

    commit 8a062427088ea97743a3d1654026d2e6a9bc904a Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Sun Sep 4 16:39:55 2022 -0500

    > feat(flash): Use flexiflow to alter the flash path.
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M flash/meta.yaml A flash/params/flash-tasks.yaml M
    flash/stages/flash.yml M flash/tasks/flash-discover.yml A
    flash/templates/flash-choice.sh.tmpl

    commit e9d5c5a9b6dea4c849eb6f8e64027cfbb661e5d1 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Wed Sep 21 22:49:18 2022 -0500

    > fix: ansible-apply and ansible-playbook assumed root ssh access
    > and needs to be configurable
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M task-library/templates/cluster-ansible-playbook.tmpl

    commit 47117290e2bdab3780b889de3951211c7fb1b827 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Wed Sep 21 13:34:10 2022 -0700

    > enhance(task-library): Move govc-commands to use heredoc for
    > command processing

    M vmware-lib/content/tasks/govc-commands.yaml

    commit 39e6cbf586453ed9d197d168d6692c157f69f634 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Sep 19 15:20:43 2022 -0500

    > feat(task-library): Update ansible to work blueprints/triggers
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    A task-library/blueprints/ansible-apply.yml M
    task-library/blueprints/ansible-run-playbook-local-on-machine.yml A
    task-library/blueprints/cluster-run-blueprint-on-each-member.yml A
    task-library/params/ansible-connection-local.yaml A
    task-library/params/cluster-for-all-blueprint.yaml A
    task-library/params/cluster-for-all-cluster.yaml A
    task-library/params/cluster-for-all-profile.yaml M
    task-library/stages/ansible-playbooks-local.yaml M
    task-library/tasks/ansible-apply.yaml M
    task-library/tasks/ansible-playbooks-local.yaml A
    task-library/tasks/ansible-playbooks.yaml A
    task-library/tasks/blueprint-to-cluster-members.yaml M
    task-library/templates/ansible-playbooks-test-playbook.yaml.tmpl M
    task-library/templates/cluster-ansible-playbook.tmpl A
    task-library/triggers/blueprint-to-cluster-members.yaml

    commit 2cdc7597ca1682165fa1236e1eee0a00fc3262f8 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Sep 20 14:31:01 2022 -0700

    > fix(vmware-lib): Restructure mklabel to run before
    > getUsableSectors

    M vmware-lib/content/tasks/esxi-datastore-parted.yaml

    commit 110cc3ce238ff48201023a03fbe6771444d529cc Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Mon Sep 19 18:48:05 2022 -0700

    > enhance(vmware-lib): Add datastore create inside ESXi

    M vmware-lib/content/params/esxi-datastore-mappings.yaml A
    vmware-lib/content/stages/esxi-datastore-parted.yaml A
    vmware-lib/content/tasks/esxi-datastore-parted.yaml M
    vmware-lib/content/tasks/govc-datastore-manage.yaml

    commit 92f75a5f5dec11f28c38a055a0ccad64e36d14de Author: Michael Rice
    \<<michael@michaelrice.org>\> Date: Thu Sep 1 14:54:37 2022 -0500

    > feat(content): Added usermod task to set uid
    >
    > In Ubuntu 20.04 and newer autoinstall based deployments there is
    > no way to set the UID of the rocketskates user at install time.
    > This task adds support to be able to set that UID post install
    >
    > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>

    A content/params/linux-usermod-skip.yaml A
    content/stages/modify-user.yaml A content/tasks/modify-user.yaml A
    content/templates/modify-user.sh.tmpl

    commit 8bee391ed6bfc23ce9aaca1329f854fad2e966cf Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Tue Sep 6 13:52:19 2022 -0500

    > feat(filters): Add UX filters to the system for others to use

    A content/filters/blueprint-aws-instances.yaml A
    content/filters/blueprint-bare-metal.yaml A
    content/filters/blueprint-brokers.yaml A
    content/filters/blueprint-clusters.yaml A
    content/filters/blueprint-local-drp.yaml A
    content/filters/blueprint-local-self-runners.yaml A
    content/filters/blueprint-machines.yaml A
    content/filters/blueprint-self-runners.yaml

    commit 9583f2aa9d56d32238a967b516c517682fffb382 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Sep 6 11:41:44 2022 -0700

    > enhance(drp-community-content): Add safety rails for
    > empty-gpt-tables

    M content/params/zero-hard-disks-for-os-install.yaml M
    content/tasks/empty-gpt-tables.yaml

    commit 1d8c8aab2e2654525450eb68ec250afdcdd1e5ec Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Sun Sep 4 17:40:54 2022 -0500

    > feat(ux-views): move ux-views content into DRP content
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    A content/filters/uxv-debug.yaml A
    content/filters/uxv-not-runnable.yaml A
    content/filters/uxv-runnable.yaml A
    content/filters/uxv-writable-bootenvs.yaml A
    content/filters/uxv-writable-catalog\_items.yaml A
    content/filters/uxv-writable-endpoints.yaml A
    content/filters/uxv-writable-params.yaml A
    content/filters/uxv-writable-plugins.yaml A
    content/filters/uxv-writable-profiles.yaml A
    content/filters/uxv-writable-reservations.yaml A
    content/filters/uxv-writable-roles.yaml A
    content/filters/uxv-writable-stages.yaml A
    content/filters/uxv-writable-subnets.yaml A
    content/filters/uxv-writable-tasks.yaml A
    content/filters/uxv-writable-templates.yaml A
    content/filters/uxv-writable-tenants.yaml A
    content/filters/uxv-writable-users.yaml A
    content/filters/uxv-writable-ux\_views.yaml A
    content/filters/uxv-writable-version\_sets.yaml A
    content/filters/uxv-writable-workflows.yaml M content/meta.yaml A
    content/roles/operator.yaml A content/roles/readonly.yaml A
    content/ux\_options/cloudia.inbox.enabled.yaml A
    content/ux\_options/cloudia.inbox.handle.yaml A
    content/ux\_options/cloudia.inbox.interval.yaml A
    content/ux\_options/cloudia.inbox.secret.yaml A
    content/ux\_options/ux.catalog.dev\_url.yaml A
    content/ux\_options/ux.catalog.stable\_url.yaml A
    content/ux\_options/ux.catalog.tip\_url.yaml A
    content/ux\_options/ux.core.airgap.yaml A
    content/ux\_options/ux.cosmetic.navbar\_color.yaml A
    content/ux\_options/ux.editor.show\_whitespace.yaml A
    content/ux\_options/ux.security.inactivity.duration.yaml A
    content/ux\_options/ux.security.inactivity.enabled.yaml A
    content/ux\_options/ux.security.token.check\_interval.yaml A
    content/ux\_options/ux.security.token.lifetime.yaml A
    content/ux\_options/ux.security.token.renew\_period.yaml A
    content/ux\_views/operator.yaml A content/ux\_views/readonly.yaml A
    content/ux\_views/reference.yaml A content/ux\_views/superuser.yaml

    commit c99103fca1e03649f2bf16267fe4a552b6c87a3b Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Fri Sep 2 15:59:16 2022 -0500

    > fix(aws-cloud): resolves issue \#451 VPC not used in Security
    > Group

    A cloud-wrappers/params/aws-vpc.yaml M
    cloud-wrappers/profiles/resource-aws-cloud.yaml M
    cloud-wrappers/templates/cloud-provision-aws-security-group.tf.tmpl

    commit f54db0f35b7ed03491e28cdf84f8f4195f008247 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Thu Sep 1 15:09:53 2022 -0500

    > feat(dr-server-install): Allow for the license to be passed in
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    A task-library/params/dr-server-install-license.yaml M
    task-library/tasks/dr-server-install.yaml

    commit a712942ca3c251f9508fc9e49a120d25c58ea832 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Thu Sep 1 11:30:06 2022 -0500

    > fix: try to allow spaces in names. There are other problems in the
    > server.
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M task-library/tasks/cluster-provision.yaml

    commit ded1f55b4976c7079cb622c99ee27d842dfb604f Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Wed Aug 31 13:37:54
    2022 -0500

    > Make HPe gen detection more robust

    M go.mod M go.sum M hpe-support/params/hpe-repo-urls.yml M
    hpe-support/params/hpe-system-gen.yml M
    hpe-support/tasks/hpe-tools-install.yml

    commit fd2b518271c0acb53e220f9049017ca5c5df406c Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 13:32:54 2022 -0500

    > build: try again on doc publishing
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M tools/build\_rel\_notes.sh M tools/publish.sh

    commit 5d8c45a91f5316fc617332f80e1fc76216cc7a42 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 12:45:55 2022 -0500

    > feat: Update docs to build into branch relative trees
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M tools/build\_rel\_notes.sh M tools/publish.sh

    commit 631f4b5a7457da19e0015f2110a5006d7b5cfa9f Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 12:42:10 2022 -0500

    > fix: add missing isourl
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M content/profiles/sledgehammer-alma8-v4.10.0.yaml

    commit ee253d86acbc86899df1fdb80a873822d15760c6 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Thu Aug 25 16:04:19 2022 -0700

    > fix(vmware-lib): Fix govc ova stage task, enhance doc

    D vmware-lib/content/params/govc-skip-ova-stage.yaml M
    vmware-lib/content/tasks/govc-stage-ova.yaml

    commit 91113f40e6b2d481b9434847ca510f266a6401ef Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Wed Aug 24 20:21:59 2022 -0700

    > fix(drp-community-content): Fix the \'empty-gpt-tables\' Task for
    > Contents concat issue

    M content/tasks/empty-gpt-tables.yaml

    commit dcb6dcc22eeef74e7ed56b6f5015cc9679676962 Author: Michael Rice
    \<<michael@michaelrice.org>\> Date: Wed Aug 24 15:30:55 2022 -0500

    > fix(content): fix space in name
    >
    > fixed a space being in the file name that shouldnt be there in
    > rocky 9 bootenv
    >
    > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>

    R100 content/bootenvs/rocky-9.0-dvd-install .yaml
    content/bootenvs/rocky-9.0-dvd-install.yaml

    commit fd800f49a2cd4ba29e01d7b10d55ae85303e1a09 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Wed Aug 24 13:59:03 2022 -0500

    > feat(sledgehammer-update): update to alma-8 sledgehammer
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M content/bootenvs/discovery.yml M content/bootenvs/sledgehammer.yml
    M content/params/package-repositories.yaml A
    content/profiles/sledgehammer-alma8-v4.10.0.yaml A
    content/profiles/sledgehammer-centos8-v4.10.0.yaml M go.mod M go.sum
    A sledgehammer-builder/bootenvs/build-sledgehammer-8.yaml A
    sledgehammer-builder/stages/sledgehammer-build-8.yaml A
    sledgehammer-builder/workflows/sledgeahmmer-build-8.yaml

    commit a13950a8b6ea3e4abb0389dd354ddcd1cc1eda2a Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Wed Aug 24 10:44:44
    2022 -0500

    > fix(inventory): Derp, fix indentation of the JSON blob in
    > inventory.yaml

    M task-library/tasks/inventory.yaml

    commit 8b40ae5a50b3a2a15fa74840cbec163c39614649 Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Thu Apr 14 10:54:55
    2022 -0500

    > fix(sledgehammer): Update sledgehammer-builder.
    >
    > This updates the CentOS 8 Sledgehammer builder to build on top of
    > Alma Linux 8 instead of CentOS (since CentOS 8 is EOL and no
    > longer maintained). It also updates stage1 to use the distro
    > native modprobe and insmod instead of the Busybox ones, since
    > certain newer kernel modules use the softdep mechanism to pull in
    > optional dependencies, but the code baked into Busybox does not
    > understand soptdeps and that causes early-stage boot failures.

    M content/bootenvs/alma-8-install.yaml M
    content/bootenvs/alma-8.5-install.yaml M
    content/params/package-repositories.yaml M
    content/tasks/enforce-sledgehammer.yaml M
    content/templates/control.sh.tmpl M go.mod M go.sum M
    sledgehammer-builder/bootenvs/build-sledgehammer.yaml M
    sledgehammer-builder/tasks/sledgehammer-create-stage-1.yaml M
    sledgehammer-builder/tasks/sledgehammer-customize.yaml M
    sledgehammer-builder/tasks/sledgehammer-place-stage1-assets.yaml

    commit ff02f165e317c0d811eb0a5196bcdfee16c20330 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Aug 23 18:14:38 2022 -0700

    > enhance(vmware-lib): Fix create loop check, add doc, add circuit
    > breaker, add examples

    M vmware-lib/content/.\_Documentation.meta A
    vmware-lib/content/profiles/EXAMEPLE-vcf-sddc-cloud-builder.yaml A
    vmware-lib/content/templates/EXAMPLE-gamble-vcf-bringup.json.tmpl M
    vmware-lib/content/templates/vcf-sddc-manage.sh.tmpl

    commit 400a5f5aa258a1bae1acf4766942841a8f2facf6 Author: Victor
    Lowther \<<victor.lowther@gmail.com>\> Date: Tue Aug 23 15:02:42
    2022 -0500

    > fix(inventory): Fix inventory validation event fail.
    >
    > The previous version of this was not safe against single-quotes
    > embedded in the machine object the event was created against, and
    > the inventory event was did not indicate that it was actually an
    > inventory-validation-failed event.
    >
    > Furthermore, the machine object that was being embedded in the
    > event had a high chance of being stale or blowing past Unix
    > command line length limits. Strip it out for now \-- consumers of
    > the event can refetch the machine object themselves.

    M task-library/tasks/inventory.yaml

    commit 5fa149a7c6208fdb9decff33dfa5981ddbf5bd75 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Tue Aug 23 12:51:06 2022 -0500

    > fix(alerts): typos fixed

    M task-library/tasks/alerts-low-disk.yaml

    commit 1b0d7582a0d9d7382585631e5b24c72beba548a3 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Tue Aug 23 12:36:16 2022 -0500

    > refactor(content): include machine name and type in base ENV setup

    M content/templates/setup.tmpl

    commit b57c4389906f5a51e1e33ab3ca1bcbf0d2023604 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Mon Aug 22 22:21:47 2022 +0000

    > enhance(vmware-lib): new features, code cleanup, rework
    > validations

    A vmware-lib/content/params/vcf-sddc-create-iterations.yaml A
    vmware-lib/content/params/vcf-sddc-manage-ignore-validations.yaml M
    vmware-lib/content/params/vcf-sddc-manage-operation.yaml A
    vmware-lib/content/params/vcf-sddc-operation-id-override.yaml A
    vmware-lib/content/params/vcf-sddc-operation-id.yaml M
    vmware-lib/content/templates/vcf-sddc-manage.sh.tmpl A
    vmware-lib/examples/validation-results.json

    commit 50c17598ec26186a3eb5c1b3faf1ca0f572db80c Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Mar 1 15:02:40 2022 -0800

    > enhance(drp-community-content): Add Debian 11 Support to DRP
    > Community Content

    A content/stages/debian-11-install.yaml M
    content/workflows/debian-base.yaml

    commit 6fdf33770578a1e34b05434e2db77e8795c23a27 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Sat Aug 20 13:55:29 2022 -0500

    > fix: remove debian-11-boot, fix package-repositories security

    D content/bootenvs/debian-11-boot.yml M
    content/bootenvs/debian-11-install.yml M
    content/params/package-repositories.yaml

    commit ea10681758a22f849d3a8362c709672e43bde96c Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Mon Aug 1 11:35:59 2022 -0400

    > add debian 11 to package repositories

    M content/params/package-repositories.yaml

    commit 790fc238de5b9f704a2adcacc587e480f308d0d0 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Fri Jul 29 13:41:01 2022 -0400

    > more debian work

    A content/bootenvs/debian-11-boot.yml R089
    content/bootenvs/debian-11.yml
    content/bootenvs/debian-11-install.yml

    commit bcfe469c403678877e1909c93eb3bb7f586ed7de Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Thu Jul 28 14:50:26 2022 -0400

    > fixed iso

    M content/bootenvs/debian-11.yml

    commit 55ccc6160a4bd22d47645ac274cc9b466c2844c1 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Thu Jul 28 14:47:35 2022 -0400

    > Update bootenv.
    >
    > update params

    M content/bootenvs/debian-11.yml M
    content/params/linux-install-bootenv-map.yaml M
    content/params/linux-install-bootenv.yaml

    commit 36469eb2099fb3b0f94e11361a978b97732d9756 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Thu Jul 28 10:59:07 2022 -0400

    > attempt to use debian 10 preseed

    M content/templates/select-kickseed.tmpl

    commit 6ef931eeb6fad229873b1425a70b5c5c621b1868 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Thu Jul 28 10:55:45 2022 -0400

    > initial stab at debian 11 bootenv

    A content/bootenvs/debian-11.yml

    commit 571ff8abbbf8f5cf937d6314de138019bd3ccfe7 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Wed Aug 17 17:13:02 2022 -0700

    > fix(task-library): bootstrap container engine fixes, add Fedora

    M task-library/tasks/bootstrap-container-engine.yaml

    commit 7f507c5495517fcdbd5c1547049376ba6ef7bb7c Author: Isaac
    Hirschfeld \<<gitlab@reheatedcake.io>\> Date: Mon Aug 15 16:53:24
    2022 +0000

    > fix: fix a typo in task-library

    M task-library/tasks/bootstrap-manager.yaml

    commit 9e7718c3abf690079817d6e6e7072dadc7ac4d80 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Aug 9 17:06:33 2022 -0700

    > enhance(vmware-lib): v4 - Better validate return handling

    M vmware-lib/content/templates/vcf-sddc-manage.sh.tmpl

    commit a78a33721943c1671b0bfa28397cb57c326222e1 Author: Shane Gibson
    \<<shane@rackn.com>\> Date: Tue Aug 9 11:36:17 2022 -0700

    > fix(vmware-lib): Fix quoting, enhance Param Meta, cleanup report
    > output

    M vmware-lib/content/params/vcf-sddc-bringup.yaml M
    vmware-lib/content/templates/vcf-sddc-manage.sh.tmpl

    End of Note

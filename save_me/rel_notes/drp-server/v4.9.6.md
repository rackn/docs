v4.9.6 {#rs_rel_notes_drp-server_v4.9.6}
======

>     commit 92d632bbc7e887e760e34d1dd499fed072f200d2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed May 4 10:38:23 2022 -0500
>
>         fix(build): update to v4.9.6 cli
>
>     M   go.mod
>     M   go.sum
>
>     commit a79e65e2306768fa8ddb214376840a71103df474
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 12 16:19:43 2022 -0500
>
>         fix(auth): allow for ad-auth to retain secrets from ADAUTH
>
>     M   frontend/frontend.go
>
>     End of Note

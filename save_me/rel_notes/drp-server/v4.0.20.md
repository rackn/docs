v4.0.20 {#rs_rel_notes_drp-server_v4.0.20}
=======

>     commit 4a99b434851a6020722fe35bc83882a4bcc79169
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 3 09:58:42 2019 -0500
>
>         Update to v4.0.20
>
>     M   go.mod
>     M   go.sum
>
>     commit 3ef4f79aebfa65f026bbdb9c79954cd574077508
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 29 15:11:59 2019 -0500
>
>         Add token endpoint for machines, and make token TTL handling in the API take things besides a raw number of seconds
>
>     M   backend/dataTracker.go
>     M   backend/machines.go
>     M   backend/timeParse.go
>     M   clitest/machines_test.go
>     M   clitest/test-data/output/TestUserCli/users.token.rocketskates.scope.all.ttl.cow.action.list.specific.asdgag/stderr.expect
>     M   frontend/machines.go
>     M   frontend/users.go
>
>     commit d8fc5da4bf59f35470c05c00d83f1076c7569f8f
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 29 13:18:45 2019 -0500
>
>         Move token generation to the Machine model
>
>     M   backend/machines.go
>     M   backend/renderData.go
>
>     commit 13a5ab15d6e42c43ed590d686734e76c55aeaecd
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 29 12:58:41 2019 -0500
>
>         Force should be allowed to change stage and bootenv too.
>
>     M   backend/machines.go
>
>     commit fa7d2dca9a3aad99a59a108af36b03fa2c502c4e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Aug 28 11:07:39 2019 -0500
>
>         Add profile-in-profile to the feature-list
>
>     M   backend/dataTracker.go
>     M   clitest/test-data/output/TestAuth/info.get/stdout.expect
>     M   clitest/test-data/output/TestUserCli/users.token.rocketskates.scope.all.ttl.330.action.list.specific.asdgag/stdout.expect
>     M   clitest/test-data/output/TestUserCli/users.token.rocketskates/stdout.expect
>
>     commit 25224995c5bc6108539926518d2daa2968a6d56b
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Aug 27 16:10:16 2019 -0500
>
>         Fix a couple of corner cases in log replay
>
>         It was possible for log replay to fail due to being unable to
>         open a file during phase 1 of log replay.  This can happen if we are
>         replaying at the same time as another routine is creating a new WAL
>         while the system is under heavy load.  Since this is an otherwise
>         harmless condition, do not stop playback when it happens.
>
>         Make replayLogs run until it has cleaned out all the logs, and adjust how
>         we calculate the number of outstanding logs to replay to be tolerant of
>         potential bad math.
>
>         Also make replyLogs die and take the rest of the system if it encounters
>         a potential dataloss scenario, instead of blithely trying to continue.
>
>         Make replay under pressure be more aggressive about pausing everything when
>         we accumulate too much transaction backlog.  Instead of spinning off a
>         goroutine to wait in that case, stop the whole world right then and wait
>         for everything to finish processing before resuming operations.
>
>     M   backend/requestTracker.go
>     M   backend/stack.go
>     M   backend/wal.go
>
>     End of Note

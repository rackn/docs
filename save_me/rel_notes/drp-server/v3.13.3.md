v3.13.3 {#rs_rel_notes_drp-server_v3.13.3}
=======

>     commit 2d673c9903863765e3398c7a2c0b1b7e96c856b3
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Apr 5 12:37:20 2019 -0500
>
>         Stop and start plugins as required bu license changes
>
>     M   midlayer/controller.go
>     M   midlayer/plugin.go
>
>     commit 2c6adb32f14be4a0fb825c67a60057b89961c752
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Apr 4 14:39:06 2019 -0500
>
>         Reenable unit testing of the server package error handling
>
>     M   server/server.go
>     M   server/server_test.go
>
>     commit 7d0d4bb660ade6b3faf8083d0dbd8012672a8491
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Apr 4 14:05:07 2019 -0500
>
>         Fix race condition when loading the router group for plugins.
>
>     M   midlayer/controller.go
>     M   server/server.go
>
>     commit d962a502c9807fe3aa1a5aae50a2db14ce57daea
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Apr 3 14:58:04 2019 -0500
>
>         WIP making plugins able to reload with cross plugin/content pack dependencies
>
>     M   backend/dataTracker_test.go
>     M   backend/stack.go
>     M   midlayer/controller.go
>     M   midlayer/fake_midlayer_server_test.go
>     M   midlayer/plugin.go
>     M   server/server.go
>     M   server/server_test.go
>
>     End of Note

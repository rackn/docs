v4.6.13 {#rs_rel_notes_drp-server_v4.6.13}
=======

>     commit 46d0ae909591fbf4f08fda91ba22e4ce7d44c267
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 18 12:32:20 2022 -0500
>
>         Bump deps and fix build for v4.6.x
>
>     M   clitest/common_test.go
>     M   clitest/test-data/output/TestFilesCli/files.upload.common_test.go.as.greg/stdout.expect
>     M   clitest/test-data/output/TestIsosCli/isos.upload.common_test.go.as.greg/stdout.expect
>     M   datastack/streamingSync_test.go
>     M   go.mod
>     M   go.sum
>     M   tools/multitest.sh
>     A   tools/test_prereqs.sh
>
>     End of Note

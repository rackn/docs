v3.0.0 {#rs_rel_notes_drp-server_v3.0.0}
======

>     commit aebff0868e730727dd38a10989f3c68fef024906
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Apr 12 00:29:43 2017 -0500
>
>         Add tags swagger.json helper to work on doc generation.
>
>     M   server/swagger.base.yml
>
>     commit 7e94a7075826f6b88832be3e13fe949c42deef5e
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Mon Apr 10 18:16:06 2017 -0500
>
>         preferences editing, bootenv from parent
>
>     M   embedded/assets/ui/render.js
>
>     commit 830dc0fd0711d698fee2f11527790bbd821988a7
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Mon Apr 10 16:06:11 2017 -0500
>
>         improve subnet preseed and interface cleanup
>
>     M   embedded/assets/ui/render.js
>
>     commit 507ed7369019ae0a648d338ebfcad8d8ed327520
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Apr 10 12:50:07 2017 -0500
>
>         Start some dhcp specific test cases.
>
>     M   midlayer/dhcp.go
>     M   midlayer/dhcp_test.go
>     M   server/server.go
>
>     commit 84723e4d294d0ad954cd49538b51b4bdbc9e35e6
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Mon Apr 10 00:01:43 2017 -0500
>
>         allow user:password instead of token
>
>     M   embedded/assets/ui/render.js
>
>     commit 3ec36ddb755e476873c80d7eb8768f13e1637dd9
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Sun Apr 9 23:03:21 2017 -0500
>
>         cosmetic change
>
>     M   embedded/assets/ui/index.html
>
>     commit 807d7774dc13c0b343272b05cc3354def3e9af09
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Sun Apr 9 22:53:51 2017 -0500
>
>         not easy to make test detect if dev NPM still referenced
>
>     M   embedded/assets/ui/index.html
>     M   frontend/frontend_test.go
>
>     commit 6700850863d3c7db44adf17e75a63cf594653dee
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Sun Apr 9 22:34:29 2017 -0500
>
>         correct tests
>
>     M   frontend/frontend_test.go
>
>     commit 7dcecda1cb7f338a92c135f8cb55b561f7acbb41
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Sun Apr 9 22:24:01 2017 -0500
>
>         correct missed rocket-skates refernces
>
>     M   embedded/assets/ui/index.html
>     M   embedded/assets/ui/render.js
>
>     commit fdd92a1dec4766e384f94d752477648e697a6685
>     Author: Rob @zehicle Hirschfeld <rob@rackn.com>
>     Date:   Sun Apr 9 22:19:29 2017 -0500
>
>         change UI to accept tokens, consolidate back to single render.js page
>
>     M   embedded/assets/ui/index.html
>     D   embedded/assets/ui/render-bootenvs.js
>     D   embedded/assets/ui/render-machines.js
>     M   embedded/assets/ui/render.js
>     M   frontend/frontend_test.go
>
>     commit fbbfbf971d3e6605c5c7d0f92acfad20a13578c8
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Apr 9 22:03:19 2017 -0500
>
>         Make the version 100% built.
>
>     M   version.go
>
>     commit dfd01537e54d2f89dfa78b20dde7f73fa05eb280
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Apr 8 18:24:10 2017 -0500
>
>         First pass at auto-versioning - more to come.
>
>     M   version.go
>
>     commit 56ba59b40e958bd10713375f0f272ec8be1be609
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Apr 7 14:51:56 2017 -0500
>
>         Add some install / quickstart tools that can be called from
>         curl | bash - need to be tested and documented in a future
>         pull request.
>
>     M   version.go
>
>     commit 798d4ec26e053a01d72c00fd51e318dae2159aac
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Apr 7 11:48:51 2017 -0500
>
>         Bump up the version
>
>     M   version.go
>
>     commit 8d2eef5208160c3dbd0fe760289a6731b1d2dea9
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Apr 7 11:35:27 2017 -0500
>
>         Remove PasswordHash from User by call Sanitize function if
>         it exists on the models.
>
>     M   frontend/frontend.go
>     M   frontend/machines.go
>     M   frontend/users_test.go
>
>     commit 7b21c2a27784f6a993ca65e120134680e350d372
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Apr 7 11:08:53 2017 -0500
>
>         Update swagger meta data to understand bearer auth.
>         This is for #151.
>
>     M   embedded/assets/ui/index.html
>     M   server/server.go
>     M   server/swagger.base.yml
>
>     End of Note

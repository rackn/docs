v3.0.4 {#rs_rel_notes_drp-server_v3.0.4}
======

>     commit 7c183b4a0119e5545de1c20f297dce395eabc2cc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 18 00:17:18 2017 -0500
>
>         Add the ability to change the user's password.
>         Fix some bugs around password setting.
>         Add unit tests for it.
>
>         Document user and user token operations.
>
>     M   backend/user.go
>     M   frontend/users.go
>     M   frontend/users_test.go
>
>     commit b65cd98a376f4eeb02d47ff1f296e72e340c778d
>     Author: JJ Asghar <jj@chef.io>
>     Date:   Tue May 16 17:48:35 2017 -0700
>
>         Added esxi-650a
>
>         Newer version of esxi-650a is out. I haven't tested it this yet, but i
>         believe this should work via your framework.
>
>         Signed-off-by: JJ Asghar <jj@chef.io>
>
>     A   assets/bootenvs/esxi-650a.yml
>     A   assets/templates/esxi-650a.boot.cfg.tmpl
>     A   assets/templates/esxi-650a.ipxe.cfg.tmpl
>
>     commit 8c3641bd492f3e18739e8a4db2dc972d7cecc4bf
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon May 15 07:40:26 2017 -0500
>
>         Tell ubuntu to use local or not for the initial
>         install as well.
>
>     M   assets/templates/net_seed.tmpl
>
>     commit 8707238fbf725809c259bebcd6733aa4c8b64070
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue May 9 12:17:34 2017 -0500
>
>         Add locks to avoid race - not really needed, but ...
>
>     M   backend/conncache.go
>     M   backend/conncache_test.go
>
>     commit 2c1657dcb17c50324738adc6903aab18406160cd
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue May 9 11:43:36 2017 -0500
>
>         Add unit tests for conncache.
>
>         Fix timeout cache bug found with tests.
>
>     M   backend/conncache.go
>     A   backend/conncache_test.go
>
>     commit 2443f5e0ef467d4cb06de77c8825e18987a6964a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat May 6 22:21:29 2017 -0500
>
>         Add comments for the index-based get and generate
>         docs for them.
>
>     M   backend/bootenv.go
>     M   backend/index/index.go
>     M   backend/index/index_test.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/preference.go
>     M   backend/profiles.go
>     M   backend/reservation.go
>     M   backend/subnet.go
>     M   backend/template.go
>     M   backend/user.go
>
>     commit ce5ab98b3959f9c167784e0c5f5703987edc015b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 5 16:00:16 2017 -0500
>
>         Update the docs and swagger information for the filter
>         index stuff.
>
>     M   frontend/bootenvs.go
>     M   frontend/leases.go
>     M   frontend/machines.go
>     M   frontend/profiles.go
>     M   frontend/reservations.go
>     M   frontend/subnets.go
>     M   frontend/templates.go
>     M   frontend/users.go
>
>     commit 83b923f21a05e4cc0896037ecf023077b8db63ed
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 5 12:03:31 2017 -0500
>
>         Alter CLI input to handle advanced queries.
>
>     M   frontend/bootenvs.go
>
>     commit 62bdffe9c428295559904eeda3d7f561a9400d4f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri May 5 11:39:00 2017 -0500
>
>         Add advanced list sorting/filtering functions.
>
>     M   frontend/bootenvs_test.go
>     M   frontend/frontend.go
>
>     commit bd10fa3ebbd9c4f8763560b33a5b7fd6e90813ae
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 5 10:42:47 2017 -0500
>
>         Add Unique to indexes, enforce it, and let Frontend use it.
>
>         This adds a flag to index.Maker that indicates whether values in an
>         index should be unique.  That flag is enforced by a BeforeSave hook on
>         each item in the DataTracker.  FetchOne has been updated to allow
>         fetching items on a unique index if the key is passed in the format of
>         index:unique_value
>
>     M   backend/bootenv.go
>     M   backend/dataTracker.go
>     M   backend/index/index.go
>     M   backend/index/index_test.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/preference.go
>     M   backend/profiles.go
>     M   backend/reservation.go
>     M   backend/subnet.go
>     M   backend/template.go
>     M   backend/user.go
>     M   frontend/frontend.go
>     M   frontend/machines.go
>     M   frontend/profiles.go
>     M   frontend/users.go
>
>     commit a2f4f568cf6ceb3e724cf0c1c39f198d7c7a7018
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 4 23:29:40 2017 -0500
>
>         Add cli support, more swagger pieces, and unit tests for cli.
>
>     M   frontend/bootenvs.go
>     M   frontend/leases.go
>     M   frontend/machines.go
>     M   frontend/profiles.go
>     M   frontend/reservations.go
>     M   frontend/subnets.go
>     M   frontend/templates.go
>     M   frontend/users.go
>
>     commit a83438c210d8e96b7ae967cc70fd4039da141dfd
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 15:52:20 2017 -0500
>
>         Oops, fix my misreading of the spec for bool comparison.
>
>     M   backend/bootenv.go
>
>     commit 340934b76551207d5e711118c0dfafb6d4c2b47c
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 15:39:14 2017 -0500
>
>         Fix boolean comparison logic in bootenv indexes
>
>     M   backend/bootenv.go
>
>     commit 503976e0c2b9531e4728b106aa74da1ba98108e5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 4 14:03:16 2017 -0500
>
>         Add swagger annotations about search parts.
>
>     M   frontend/bootenvs.go
>     M   frontend/leases.go
>     M   frontend/machines.go
>     M   frontend/profiles.go
>     M   frontend/reservations.go
>     M   frontend/subnets.go
>     M   frontend/templates.go
>     M   frontend/users.go
>
>     commit efc2a8a584c07c9dc162099ee9194711394fdbf8
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 13:18:12 2017 -0500
>
>         Add tests for index funcs.
>
>     A   backend/index/index_test.go
>
>     commit cb9635a88c569e53844952d2665acd212e8ae6a4
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 13:18:36 2017 -0500
>
>         Fix up a few logic errors in the index funcs
>
>     M   backend/index/index.go
>
>     commit a946dd7f1e84cf00b2aff872e509c5aba6e514fb
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 11:21:38 2017 -0500
>
>         Have Limit and Offset throw an error at negative numbers
>
>     M   backend/index/index.go
>
>     commit b0da5391aa6b1dc2349a60dc871564071e705ec5
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 11:20:15 2017 -0500
>
>         Make Limit and Slice not panic if num is too big
>
>     M   backend/index/index.go
>
>     commit 67f943cef391b7924e4506778818205b68c30445
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 4 11:53:58 2017 -0500
>
>         Add filter/sort to our list and start swagger definitions.
>
>     M   frontend/bootenvs.go
>     M   frontend/bootenvs_test.go
>     M   frontend/frontend.go
>
>     commit 8fa111f30fdd3713037e2129418387d7024c3cd4
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 4 09:12:33 2017 -0500
>
>         Update index API to make it easier to use from the frontend.
>
>     M   backend/bootenv.go
>     M   backend/dataTracker.go
>     M   backend/index/index.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/preference.go
>     M   backend/profiles.go
>     M   backend/reservation.go
>     M   backend/subnet.go
>     M   backend/template.go
>     M   backend/user.go
>     M   frontend/frontend.go
>     M   frontend/frontend_test.go
>
>     commit a48f83619b0ad408378666c1c8bca68828767da6
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Apr 25 14:49:19 2017 -0500
>
>         Add an indexing layer.
>
>         This adds an indexing layer at backend/index.  It should be used as the
>         basis for allowing basic query params via the list APIs
>
>     M   backend/bootenv.go
>     M   backend/dataTracker.go
>     A   backend/index/index.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/preference.go
>     M   backend/profiles.go
>     M   backend/reservation.go
>     M   backend/subnet.go
>     M   backend/template.go
>     M   backend/user.go
>     M   frontend/frontend.go
>     M   frontend/frontend_test.go
>     M   server/server.go
>
>     commit 64dcdc8af49263f3ccb6daad20adea1895197ab7
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 5 10:51:51 2017 -0500
>
>         Oops, forgot to add fix to assets.go
>
>     M   server/assets.go
>
>     commit 3033d9f2502ed611fe5b899881491a7374167007
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri May 5 10:28:57 2017 -0500
>
>         Fix building commands to allow for automated refactoring
>
>     R100    cmds/dr-provision.go    cmds/dr-provision/dr-provision.go
>     D   cmds/drpcli-docs.go
>     D   cmds/drpcli.go
>
>     End of Note

v4.10.2 {#rs_rel_notes_drp-server_v4.10.2}
=======

>     commit 57b5d1339e16dbec84a49fd3ff0647034c653f6c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Aug 14 00:03:34 2022 -0500
>
>         build: Update to latest 4.10.2 cli
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   go.mod
>     M   go.sum
>
>     commit 14826d5541215091733fe0bd3345dfc524ee4198
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Aug 13 16:22:40 2022 -0500
>
>         fix: handle '.' in the path
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   backend/fs.go
>
>     End of Note

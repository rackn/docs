v4.7.20 {#rs_rel_notes_drp-server_v4.7.20}
=======

>     commit 85a3b5a8be38225b4909935fd803133e90604926
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 12 16:19:43 2022 -0500
>
>         fix(auth): allow for ad-auth to retain secrets from ADAUTH
>
>     M   frontend/frontend.go
>
>     commit b811b64e7d26cc2e77f43ee2ac3f69986d1824f1
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Apr 25 14:36:41 2022 -0500
>
>         fix(transact): Make transaction unblocking more resilient.
>
>         When iterating over transactions to unblock when a transaction is released,
>         consider all possible transactions that can be unblocked, not just the
>         ones that are blocked directly by the transaction being released.  To make
>         this process more efficient, refactor how blocked transactions are tracked
>         to make this accounting simpler.
>
>         While we are at it, refactor flag handling to avoid some race conditions.
>
>     M   transact/interlockTracker.go
>     M   transact/tx.go
>
>     End of Note

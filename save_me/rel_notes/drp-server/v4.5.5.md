v4.5.5 {#rs_rel_notes_drp-server_v4.5.5}
======

>     commit 444a462439104eff3abf97af17be59cf21f8af4a
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jan 12 17:33:53 2021 -0600
>
>         feat(ha): 3 seconds to sync artifacts iis too short.
>
>         Especially when synchronizing large immages over narrow pipes for the
>         first time.
>
>     M   go.sum
>     M   server/server.go
>
>     End of Note

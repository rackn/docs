v3.4.1 {#rs_rel_notes_drp-server_v3.4.1}
======

>     commit e1362b86f578b440699645a6ebbd8c48e965251c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Dec 4 20:51:00 2017 -0600
>
>         Fix for #469 - add an option to override a replace directory.
>         This defaults to replace in the base root dir.  Files in
>         that location will override the embedded files.
>
>     M   server/assets.go
>     M   server/assets_test.go
>     A   server/extract_test/explode_iso.sh
>     A   server/extract_test/files/jq
>     M   server/server.go
>
>     End of Note

v4.7.4 {#rs_rel_notes_drp-server_v4.7.4}
======

>     commit 8107b7c0ea785e0c85d2ac1d14e1cd45f9926258
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 13 13:21:37 2021 -0500
>
>         fix(datastack): Handle different data types in the humanize function.
>
>     M   datastack/humanize.go
>
>     End of Note

v4.6.9 {#rs_rel_notes_drp-server_v4.6.9}
======

>     commit 404e21f0c6441b6be80e5d11249432151e8e9d3e
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Aug 31 18:04:19 2021 -0500
>
>         fix(waltool): Oops, waltool recover was too zealous about what it threw away
>
>     M   consensus/raftStore.go
>
>     End of Note

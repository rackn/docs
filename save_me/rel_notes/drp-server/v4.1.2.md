v4.1.2 {#rs_rel_notes_drp-server_v4.1.2}
======

>     commit 06e46c9c1252666a75b28c395e191e37c5eedf0d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Oct 25 11:07:52 2019 -0500
>
>         Update to v4.1.2
>
>     M   go.mod
>     M   go.sum
>
>     commit dcaf93e9f91269c997a21713eb08c6f0a3185001
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 23 18:31:34 2019 -0500
>
>         Update RS_DISABLE_BINL to RS_DISABLE_PXE to match the actual flag.
>
>     M   server/server.go
>
>     commit fbced29a79bcce32fdc5b9f7b92e2b47301d3dfe
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Oct 22 21:52:12 2019 -0500
>
>         Convert internal bsdtar calls to the embedded bsdtar
>         based upon OS/ARCH
>
>         Update to 4.1.2-beta
>
>     M   frontend/file_common.go
>     M   frontend/system.go
>     M   go.mod
>     M   go.sum
>
>     commit 7ceba5d7783e5116228cde2535117636b93e2822
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Oct 22 21:06:44 2019 -0500
>
>         Convert zip file to tgz, but leave it named zip to
>         work with all the tools.
>
>     M   tools/package.sh
>
>     commit 8e1d948c282c6083654d1edda1e32720f5c777a7
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Oct 22 13:16:39 2019 -0500
>
>         Add a statically linked bsdtar for linux amd64
>
>     M   .gitignore
>     A   bsdtar/build.sh
>     A   bsdtar/docker/Dockerfile
>     A   bsdtar/docker/Dockerfile.aarch64
>     M   clitest/test-data/output/TestAuth/plugins.list.611601b3efac342fd10027372140fe8c/stdout.expect
>     M   clitest/test-data/output/TestAuth/plugins.list.e8e0775e692adbcb8acdf3799178655c/stdout.expect
>     M   clitest/test-data/output/TestLoadIncrementer/plugin_providers.show.dangerzone/stdout.expect
>     M   clitest/test-data/output/TestPluginCli/plugins.list.2/stdout.expect
>     M   clitest/test-data/output/TestPluginCli/plugins.list.3/stdout.expect
>     M   clitest/test-data/output/TestPluginCli/plugins.list.4/stdout.expect
>     M   clitest/test-data/output/TestPluginCli/plugins.list.5/stdout.expect
>     M   clitest/test-data/output/TestPluginCli/plugins.list/stdout.expect
>     M   clitest/test-data/output/TestPluginProviderCli/plugin_providers.list.2/stdout.expect
>     M   clitest/test-data/output/TestSecureParams/d2e09e883fb98f066207b265d9807a4b/stdout.expect
>     M   embedded/assets.go
>     A   embedded/assets/bsdtar.amd64.linux
>     A   embedded/assets/bsdtar.arm64.linux
>     M   embedded/assets/explode_iso.sh
>     M   embedded/assets_test.go
>
>     commit f52f7fbf2d5a6a23a01fca961e4a4ab3b07e0339
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Oct 9 08:27:10 2019 -0500
>
>         Start adding ONIE support
>
>     A   midlayer/onie.go
>
>     commit 0813b3a006e63e357feff78ef3e2de36708846a6
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Oct 21 15:41:44 2019 -0700
>
>         Remove windows from extract_iso.sh, not supported anymore
>
>     M   embedded/assets/explode_iso.sh
>
>     commit 913b59a37bf2025be29dacc78d675eecf1aa414c
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Oct 21 13:37:20 2019 -0500
>
>         trim underbrow
>
>     M   CONTRIBUTING.rst
>
>     commit 1b922cfa8503fdaf58ae3e108b1d0fcd96b97464
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Oct 19 12:51:11 2019 -0500
>
>         fix icon
>
>     M   cmds/dangerzone/dangerzone.go
>
>     commit 3be6e0f4c7c3f94bea341b7ffd34b77161400755
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Oct 19 12:48:05 2019 -0500
>
>         update contributing info
>
>     M   CONTRIBUTING.rst
>
>     commit 3c323e46d52250591c71c796fe0cf9403e5140b8
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Oct 17 12:27:53 2019 -0500
>
>         Make wal writing more resistant to skipping commit ids
>
>     M   backend/stack.go
>     M   backend/wal.go
>
>     commit 95eda2d14a3878130e2dd7ed769d03ffe28c7200
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Oct 14 16:52:05 2019 -0500
>
>         Update to v4.1.1
>
>     M   go.mod
>     M   go.sum
>
>     commit b496fa083901900d954f159658bcaa6c39f72fda
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Oct 4 10:20:49 2019 -0500
>
>         First pass at implementing support for elevated token privs on a per-task basis
>
>     M   backend/dataTracker.go
>     M   backend/jobs.go
>     M   backend/jwt-utils.go
>     M   backend/machines.go
>     M   backend/plugin.go
>     M   backend/renderData.go
>     M   backend/renderData_test.go
>     M   backend/roles.go
>     M   backend/task.go
>     M   clitest/fixInteractive.sh
>     M   clitest/tasks_test.go
>     M   clitest/test-data/output/TestAuth/info.get/stdout.expect
>     M   clitest/test-data/output/TestCorePieces/jobs.indexes/stdout.expect
>     M   clitest/test-data/output/TestCorePieces/tasks.indexes/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.create.bc23e5eb509145146e60c0681d79687e/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.currentlog.Name.bob.2/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.currentlog.Name.bob/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.deletejobs.Name.bob/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.destroy.Name.bob/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.jobs.create.Name.bob.2/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.jobs.create.Name.bob/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.jobs.state.Name.bob.to.finished.2/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.jobs.state.Name.bob.to.finished/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.processjobs.Name.bob.6307a6ab9ebdd05260eb0569a16f2b10.2/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.processjobs.Name.bob.6307a6ab9ebdd05260eb0569a16f2b10.3/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.processjobs.Name.bob.6307a6ab9ebdd05260eb0569a16f2b10/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.04c403041948ed2647cb2dbd3921decb/stderr.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.116282f528e96cafbe0645098a068578/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.2d9628e416e081a20ff5a59fcf944e45/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.80bcac53cf6e23e0449942197e591d5e/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.9daf9d749faf7974c8140e1128611201/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.ac23a73ca4c3e6601e9b3b0c486cf278/stderr.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.b163fc73031096a00d25696999749882/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/machines.update.Name.bob.e2b2172af53ece0b710b2cc00dfe5986/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.3d318eb3d24e0d141ae6ef50c9fa5ef8/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.416f18fb9dc46b108e9d06571a7d1709/stderr.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.531d31d2bf87ade06a1b16c125c73544/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.9c60d982356df43599739a7b7e2b3458/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.adfcf452bdf1fe1bdd4ee0ac59f5d7e1/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.b3c472afeaebff03848ad1b7aac1d3b2/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.de8d8a847507a34220f8ff818a1aa85a/stderr.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.create.nothingBurger/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.destroy.extraClaim/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.destroy.extraRole/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.destroy.nothingBurger/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.list.Elevated=false/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.list.Elevated=true/stdout.expect
>     A   clitest/test-data/output/TestTasksWithElevatedTokens/tasks.list/stdout.expect
>     M   clitest/test-data/output/TestUserCli/users.token.rocketskates.scope.all.ttl.330.action.list.specific.asdgag/stdout.expect
>     M   clitest/test-data/output/TestUserCli/users.token.rocketskates/stdout.expect
>     M   frontend/frontend.go
>     M   frontend/machines.go
>     M   frontend/prefs.go
>     M   frontend/users.go
>     M   go.mod
>     M   go.sum
>
>     commit 4b54b9814c4eecef13a9371d40e1c06ad5f2da8c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Oct 2 15:13:06 2019 -0500
>
>         Update to v4.1.0 pieces
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

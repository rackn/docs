v3.11.0 {#rs_rel_notes_drp-server_v3.11.0}
=======

>     commit e5008287a751f9ccea02cf1b51a73aa941ffca08
>     Author: Ben Agricola <bagricola@squiz.co.uk>
>     Date:   Thu Sep 6 14:37:59 2018 +0100
>
>         Fix user.ChangePassword incorrectly invalidating future tokens
>
>         Signed-off-by: Ben Agricola <bagricola@squiz.co.uk>
>
>     M   backend/user.go
>     M   backend/user_test.go
>
>     commit 538c510da7d19f63dbae1a3469aca81b823abb8d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 30 14:20:41 2018 -0500
>
>         Allow Patch to work of KEY:value id pieces
>
>     M   frontend/frontend.go
>
>     commit df3b1e571ec0b06a90d105ce8651b042b8336832
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 27 14:16:14 2018 -0500
>
>         Add more module support
>
>     M   embedded/assets/ipxe-arm64.efi
>     M   embedded/assets/ipxe.efi
>     M   embedded/assets/ipxe.lkrn
>     M   embedded/assets/ipxe.pxe
>     M   ipxe/build.sh
>
>     commit d03eb89b858f9298e6681857e92bf77a406fcb1d
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Aug 24 09:03:45 2018 -0500
>
>         Consolidate lease expire time code and via handling for reservations.
>
>     M   backend/dhcpUtils.go
>
>     commit 2b6aa6878ee2810ee70999d86850d3f23fbb1140
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 23 15:30:25 2018 -0500
>
>         Add support in the backend for subnets consist of many point2point links.
>
>         This adds a new picker named "point2point", that makes the address
>         allocation strategy always create a lease that specifies a network
>         config consisting of a /31 that contains the GIADDRas the router and
>         the remote node and the other address in that subnet as the address of
>         the system making the request.
>
>     M   backend/dhcpUtils.go
>     M   backend/dhcpUtils_test.go
>     M   backend/subnet.go
>     M   midlayer/dhcp.go
>
>     commit ddff52e5013de351626f0f4549a999e9e0dd15a2
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 23 11:02:56 2018 -0500
>
>         Move option consolidation code into the backend
>
>     M   backend/dhcpUtils.go
>     M   midlayer/abp.go
>     M   midlayer/dhcp.go
>     M   midlayer/pxe.go
>
>     commit d86c008d102dca429a851ab0e003bea3fed21b16
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Aug 16 09:34:22 2018 -0500
>
>         Add back missing continue so we get all of the potential errors
>
>     M   backend/dataTracker.go
>
>     commit 6e19c6fff4aff95f2769e07f80f53c0141a3c0a1
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 15 17:51:18 2018 -0500
>
>         Better error messages for displaying corrupt data, along with remedial actions
>
>     M   backend/dataTracker.go
>     M   midlayer/fake_midlayer_server_test.go
>
>     commit 83c43a490fe8077d810861c2d293628ccfafe8a3
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 15 17:12:12 2018 -0500
>
>         Move the DataStack to the backend to allow for better error reporting
>
>     M   backend/bootenv_test.go
>     M   backend/dataTracker.go
>     M   backend/dataTracker_test.go
>     M   backend/dhcpUtils_test.go
>     M   backend/interfaces_test.go
>     M   backend/jwt_utils_test.go
>     M   backend/lease_test.go
>     M   backend/machines_test.go
>     M   backend/param_test.go
>     M   backend/preference_test.go
>     M   backend/profiles_test.go
>     M   backend/renderData_test.go
>     M   backend/reservation_test.go
>     R062    midlayer/stack.go   backend/stack.go
>     M   backend/stage_test.go
>     M   backend/subnet_test.go
>     M   backend/task_test.go
>     M   backend/template_test.go
>     M   backend/user_test.go
>     M   backend/workflow_test.go
>     M   frontend/content.go
>     M   midlayer/controller.go
>     M   server/server.go
>
>     commit 2e621e7f8c0729fdec79f2b4fc79a00753c63a12
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 15 14:54:35 2018 -0500
>
>         Add code for cleaning up corrupt or missing data
>
>     M   backend/dataTracker.go
>     M   server/server.go
>
>     commit 2f135dd66ff14638ecfa6bdd09a508cb2b6a66f5
>     Author: Ido Samuelson <ido.samuelson@gmail.com>
>     Date:   Wed Aug 15 01:21:00 2018 -0600
>
>         The URL of the public signing key has changed!
>
>         Any previous reference to https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub should be updated with immediate effect to https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub.
>
>     M   Dockerfile
>
>     commit 28639b5b3b7786ff422d29a0878676d7c3f26aaf
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Aug 10 11:22:52 2018 -0500
>
>         Add better errors for template failures in content packs
>
>     M   backend/dataTracker.go
>
>     commit 7e55240614f288bfe8c91ca48baeb9488d5fbc52
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 9 12:13:07 2018 -0500
>
>         Send events on file serving from tftp and http;
>
>     M   backend/event.go
>     M   backend/requestTracker.go
>     M   midlayer/static.go
>     M   midlayer/tftp.go
>     A   utils/event_handler.go
>
>     commit 84e2a7f23ae415a322f5a6d70e9f5969a8c4f169
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Aug 6 16:51:29 2018 -0500
>
>         Start threading Principal information through dr-provision.
>
>         Principal is what we will use to determine who (or what) initiated any
>         given action that might result in something being updated.
>
>     M   backend/bootenv.go
>     M   backend/event.go
>     M   backend/requestTracker.go
>     M   backend/stage.go
>     M   backend/subnet.go
>     M   frontend/frontend.go
>     M   frontend/isos.go
>     M   midlayer/controller.go
>     M   midlayer/dhcp.go
>     M   midlayer/plugin.go
>     M   midlayer/static.go
>     M   midlayer/tftp.go
>     M   server/server.go
>
>     commit 1428259d6445321f5db66de036066269bfbe7529
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Aug 6 15:29:09 2018 -0500
>
>         Fix up broken lease finding logic
>
>     M   backend/dhcpUtils.go
>     M   backend/dhcpUtils_test.go
>     M   backend/machines.go
>     M   midlayer/dhcp.go
>
>     commit 401512c1acd5bfb9b6662bc06f2ed8bce9d96f18
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Aug 6 12:02:38 2018 -0500
>
>         More Reservation updates and tests.
>
>         Reservations model has grown a Subnet field to allow scoped
>         Reservations to keep track of what Subnet they are associated with,
>         and the Create validation has been updated to force sane-ish scoping
>         rules for when and where Reservations with identical Strategies and
>         Tokens can be created.
>
>     M   backend/reservation.go
>
>     commit 59ef45a2918393f17e5cad5481a8068c062a48ed
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Aug 6 10:45:57 2018 -0500
>
>         Flesh out DHCP code to handle scoped leases.
>
>     M   backend/dhcpUtils.go
>     M   backend/subnet.go
>
>     commit 96e8cda0dcaa1352f5c03b183b4aa148bc18d5e4
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Aug 3 13:07:38 2018 -0500
>
>         Start working on scoped reservations.
>
>         This commit adds a new Scoped flag to Reservations, which signals that
>         the Reservation so created is only valid within a Subnet that has a
>         matching covered address range.  It will allow the DHCP system to
>         properly handle the case where we have multiple Reservations in
>         different Subnets for the same Token, which can happen when tagged
>         VLAN interfaces are in play on the client.
>
>         This pull request only implements the model and backend validation
>         changes, along with an update to the Subnet logic to disallow changes
>         to the Subnet field.  The changes to the DHCP system will come later.
>
>     M   backend/reservation.go
>     M   backend/subnet.go
>
>     commit fc3b47e21a20bc135b9c1e7e749ed4066cd99897
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Aug 1 11:02:16 2018 -0500
>
>         Add support for building ISO ipxe boot images
>
>     A   ipxe/build_iso.sh
>     M   ipxe/docker/Dockerfile
>     M   ipxe/docker/build_ipxe.sh
>     A   ipxe/embed_default.ipxe
>
>     commit 0c49f0aae70149480d609eb056b67fef9604fe13
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 31 15:29:40 2018 -0500
>
>         Apple support netboots High Sierra via NetBoot and NetInstall methods.
>
>     M   backend/bootenv.go
>     M   midlayer/abp.go
>     M   midlayer/dhcp.go
>
>     commit bdcf91a0400d0c8df36c14f305ae5012e4008884
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jul 30 16:58:37 2018 -0500
>
>         Make a couple more fixups to apple boot support, start adding DHCP unit tests
>
>     M   midlayer/abp.go
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0000.logs-expect
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0000.request
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0000.response-expect
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0001.logs-expect
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0001.request
>     A   midlayer/dhcp-tests/0003-apple-nbsp-select/0001.response-expect
>
>     commit ab6a71986389283dbe4423bcd27e61dd3c1968c1
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jul 30 15:59:51 2018 -0500
>
>         Start removal of hardwired chainload into iPXE apple boot.
>
>     M   midlayer/abp.go
>     M   midlayer/dhcp.go
>
>     commit 5ba3e1d8a62be2051fa113830d08941a63538ed0
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 24 15:06:52 2018 -0500
>
>         MacPro6,1 boots to Sledgehammer
>
>     M   backend/bootenv.go
>     M   backend/renderData.go
>     A   midlayer/abp.go
>     M   midlayer/dhcp.go
>     M   midlayer/dhcpUtil.go
>     M   midlayer/dhcp_test.go
>     A   midlayer/pxe.go
>
>     End of Note

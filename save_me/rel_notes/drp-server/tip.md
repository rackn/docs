tip {#rs_rel_notes_drp-server_tip}
===

>     commit be5c72634a4d19618725a6d742b8edf52d7cb2fe
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jul 22 10:22:21 2019 -0500
>
>         Fix plugin provider upload to not fail if the UX doesn't send a valid
>         name.
>
>     M   backend/plugin_controller.go
>
>     commit 329ccf57764f949cf95b92d7ca084711c1a450c1
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Jul 21 20:09:33 2019 -0500
>
>         Allow the plugins to have default parameter values that can be decoded.
>
>     M   backend/param.go
>     M   backend/plugin.go
>
>     commit 25f02e9a3a77e16d42070fe28e1db12f2c6b7d6b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jul 19 15:08:08 2019 -0500
>
>         Machine is not unique in job index
>
>     M   backend/jobs.go
>
>     commit 9e6c7b8ba5575f878e1523efa0f42a68a37eb993
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Jul 19 08:39:15 2019 -0500
>
>         GenerateProfileToken needs to have secure get/update
>
>     M   backend/renderData.go
>
>     commit 07126cb0e50ea6360229513b671b9b1a5c798956
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Jul 18 11:27:20 2019 -0500
>
>         Allow the hint picker to reuse leases with the same Strategy and Token
>
>     M   backend/subnet.go
>
>     commit da8fcd727a7ab076195b9118c64a88a2ae0bd57d
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Jul 18 09:56:08 2019 -0500
>
>         More thuroughly clean up conflicting leases at startup time
>
>     M   backend/dataTracker.go
>
>     commit 1060c0ff2faca56afac2cd464ec8d9ea32634b9f
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Jul 18 09:01:36 2019 -0500
>
>         Add HEAD method to api/v3/jobs/uuid/log endpoint
>
>     M   frontend/jobs.go
>
>     commit 325a069ee279e3a0e8a794c44992830446996559
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jul 3 14:32:59 2019 -0500
>
>         Refactor some of the parameter passing inside the server to
>         pass around an info strucuture as it has all the data and more.
>
>         From this info structure, allow the templates to render the info
>         fields and run the HasFeature function.
>
>     M   backend/dataTracker.go
>     M   backend/dataTracker_test.go
>     M   backend/license.go
>     M   backend/renderData.go
>     M   backend/renderData_test.go
>     M   backend/requestTracker.go
>     M   backend/stack.go
>     M   frontend/endpoint.go
>     M   frontend/frontend.go
>     M   frontend/info.go
>     M   midlayer/abp.go
>     M   midlayer/fake_midlayer_server_test.go
>     M   server/server.go
>
>     commit df4f126de40efeec0ce047b7775bdf9567a750fb
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Jun 27 17:58:33 2019 -0700
>
>         fix 'slready' spelling error
>
>     M   backend/bootenv.go
>
>     commit 5b222ff7f5d60393786f659bd873483aea1669bb
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 27 10:49:50 2019 -0500
>
>         Make file upload backwards compat with old UX.
>
>     M   frontend/file_common.go
>
>     commit c5d78ce1a347f6fdf8197a9139161fbc595c257a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 26 10:25:51 2019 -0500
>
>         build it before checking it in.
>
>     M   frontend/file_common.go
>
>     commit 9130341c6dfc446abe411886e437f71d785c7f79
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 26 10:09:35 2019 -0500
>
>         Don't use the multipart data to set the filename.  The name comes from
>         the path.
>
>     M   frontend/file_common.go
>
>     commit 03ce6d51b3fe52f684695840e4c45a3baaa477dc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 25 22:24:45 2019 -0500
>
>         Refactor isos and files to use the same code on the frontend.
>         This is a prepatory step to adding offset on POST to
>         allow partial writes.
>
>     A   frontend/file_common.go
>     M   frontend/files.go
>     M   frontend/frontend.go
>     M   frontend/isos.go
>
>     commit 22ec3392c946d515ba83bb7abddbc22d7c6cf85d
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jun 24 13:25:40 2019 -0500
>
>         Add regex filters to most applicable fields.
>
>         Wherever we can reasonably be certain we are filtering by something
>         string-ish, allow a re2 compatible regex to be used via the new Re
>         index operator.
>
>     M   backend/bootenv.go
>     M   backend/index/index.go
>     M   backend/jobs.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/param.go
>     M   backend/plugins.go
>     M   backend/profiles.go
>     M   backend/raw_model.go
>     M   backend/reservation.go
>     M   backend/roles.go
>     M   backend/stage.go
>     M   backend/subnet.go
>     M   backend/task.go
>     M   backend/template.go
>     M   backend/tenants.go
>     M   backend/user.go
>     M   backend/workflow.go
>     M   frontend/frontend.go
>     M   frontend/indexes.go
>
>     commit 5eee0bf623777ce46bde1974afa3683dd35fb8e5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 22 12:43:58 2019 -0500
>
>         Allow stages to have action:, bootenv:, and stage:
>         This is mostly for actions and operational consistency.
>         BootEnv also has the BootEvn field and changing stage
>         this was would be weird but should work.
>
>     M   backend/stage.go
>     M   backend/stage_test.go
>
>     commit 7485c8a2e305e77862e3984fce59628bc727158c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 22 11:13:59 2019 -0500
>
>         Fix a lock issue with AutoStart, also set plugin auto
>         created meta data from pluginporvider .  Set plugin
>         provider meta data from internal content.
>
>     M   backend/plugin.go
>     M   backend/plugin_controller.go
>
>     commit f8a2789ee06339668a3c7351259860649a78e3dc
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 22 09:07:18 2019 -0500
>
>         Give PluginProviders an AutoStart capability to allow them
>         be autoconfigured but still editable.  This will allow
>         manager to update existing plugins.
>
>     M   backend/plugin.go
>     M   backend/plugin_controller.go
>
>     commit 9f4461c240be07aa0e2ec17832d0a3cc577cbf9d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jun 21 18:45:02 2019 -0500
>
>         Add Partial field to objects to indicate that the object is
>         only partially provided (used on slim and params list filters)
>
>         Add params field to the list queries on Params.  This overrides
>         slim=Params and only returns the specified parameters in the object
>         when listed.  This works also on the GET version of the object as well.
>
>     M   frontend/frontend.go
>     M   frontend/machines.go
>     M   frontend/params.go
>     M   frontend/plugins.go
>     M   frontend/profiles.go
>     M   frontend/stages.go
>
>     commit ef9fbfca142636a851c790916772c639aa00d189
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Jun 21 12:54:48 2019 -0500
>
>         Remove consul and boltdb support
>
>     M   backend/stack.go
>     M   midlayer/ha.go
>     M   server/server.go
>
>     commit 49ae767bcea14e27f98f115659f4fed807db26cd
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Jun 21 09:47:17 2019 -0500
>
>         Pull the store code into digitalrebar.
>
>         This will make it easier to get rid of stuff we no longer use, such as
>         Consul and Bolt.
>
>     M   backend/bootenv.go
>     M   backend/dataTracker.go
>     M   backend/dataTracker_test.go
>     M   backend/index/index_test.go
>     M   backend/jobs.go
>     M   backend/lease.go
>     M   backend/machines.go
>     M   backend/param.go
>     M   backend/plugin_controller.go
>     M   backend/plugins.go
>     M   backend/preference.go
>     M   backend/profiles.go
>     M   backend/raw_model.go
>     M   backend/requestTracker.go
>     M   backend/reservation.go
>     M   backend/roles.go
>     M   backend/stack.go
>     M   backend/stage.go
>     M   backend/subnet.go
>     M   backend/task.go
>     M   backend/template.go
>     M   backend/tenants.go
>     M   backend/user.go
>     M   backend/workflow.go
>     M   frontend/content.go
>     M   frontend/frontend.go
>     M   midlayer/fake_midlayer_server_test.go
>     M   server/server.go
>
>     commit 06ee4ee236dbf15f0b3940063433f94ec9f6cd6a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 19 22:54:08 2019 -0500
>
>         Add an exists HEAD operation for files and isos that we
>         can get the checksum of the files to determine is we need to
>         upload them again.
>
>     M   frontend/files.go
>     M   frontend/isos.go
>
>     commit 68089f68a8cda8762ba8405228add395d9636e5b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 13 15:29:35 2019 -0500
>
>         Add a .ParamExpand function that will expand the string again.
>
>     M   backend/renderData.go
>     M   backend/renderData_test.go
>
>     commit 1e57a608a9de93ded6803616eee8703cfcde4136
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 11 16:50:41 2019 -0500
>
>         Add option 82 sub 11 handling
>
>     A   midlayer/dhcp-tests/0004-option82-reflection/0003.logs-expect
>     A   midlayer/dhcp-tests/0004-option82-reflection/0003.request
>     A   midlayer/dhcp-tests/0004-option82-reflection/0003.response-expect
>     M   midlayer/dhcp.go
>
>     commit 8a81bc096a3904ba6fc1d38137b72895b3924619
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 11 15:39:32 2019 -0500
>
>         Add option 82 suboption 5 handling
>
>     A   midlayer/dhcp-tests/0004-option82-reflection/0002.logs-expect
>     A   midlayer/dhcp-tests/0004-option82-reflection/0002.request
>     A   midlayer/dhcp-tests/0004-option82-reflection/0002.response-expect
>     M   midlayer/dhcp.go
>     M   midlayer/fake_midlayer_server_test.go
>
>     commit a6549ca042ea503b53ae85e228860bf6402764fd
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 11 11:18:03 2019 -0500
>
>         Add In and Nin filters for indexes
>
>         In test if see if an item matches any of a given set of values, and
>         Nin tests to see if an item does not match all of the values.
>
>     M   backend/index/index.go
>     M   frontend/frontend.go
>
>     commit 5eab1c9343e5d6263354a6d61053ac916fb02f94
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jun 10 11:45:16 2019 -0500
>
>         Add indexes for Params and Profiles on objects that have them.
>
>         These Unordered indexes take a comma-seperated list of profile and
>         parameter names that must all be present on the object in order for
>         the test to pass.
>
>     M   backend/machines.go
>     M   backend/plugins.go
>     M   backend/profiles.go
>     M   backend/stage.go
>
>     commit efbd7485a4c0ec6082ea3604b269652957b1d859
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jun 10 10:14:21 2019 -0500
>
>         Fix user crash of ad-auth lookups
>
>     M   backend/user.go
>
>     commit 6f4fb082d37bdbd86b6c259f381f30da790827f1
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 8 13:03:44 2019 -0500
>
>         It is a query parameter for explode not a URL parameter
>
>     M   frontend/files.go
>
>     commit 45d87cc32160b9570939ff8aa8ffc5b6fa299acf
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 8 12:23:43 2019 -0500
>
>         Agent directory can now contribute to code coverage
>
>         Add explode option for file upload to explode tarballs.
>
>     M   frontend/files.go
>
>     commit b3d3e7372d5c2007dd71f430115f29b8e6830a50
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jun 5 13:54:45 2019 -0500
>
>         Fix missing condition when building MachineRepos for debian-ish systems
>
>     M   backend/renderData.go
>
>     commit 2a3c21dc7bed6ae031335f221b77bb33d9e68c67
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jun 5 10:31:25 2019 -0500
>
>         Add Unordered flag to indexes and make sure it is set appropriately.
>
>     M   backend/index/index.go
>     M   frontend/indexes.go
>
>     commit ddeaa3edfa7f2c8551e6921cada95577361247d8
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 4 21:20:05 2019 -0500
>
>         Update the rest of the code to use unsorted indexes for booleans
>
>     M   backend/bootenv.go
>     M   backend/jobs.go
>     M   backend/machines.go
>     M   backend/param.go
>     M   backend/raw_model.go
>     M   backend/stage.go
>     M   backend/subnet.go
>     M   frontend/frontend.go
>
>     commit 21d57ef94f5c56e99a2d03b8245a4c2abd4c9aad
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 4 20:21:54 2019 -0500
>
>         Add ability to work with non-sorting indexes
>
>     M   backend/index/index.go
>     M   backend/index/index_test.go
>
>     commit 2fc6e6492b8e29cd5aa0a85db4b55b46a384aed6
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jun 4 10:42:21 2019 -0500
>
>         Fix spurious 500 errors when authentication fails for whatever reason.
>
>         This also should take care of weird content-type errors that happen when
>         we return 403 errors.
>
>     M   backend/requestTracker.go
>     M   frontend/frontend.go
>
>     commit a1032c01a4fa20a928fd6010b32649a43a82b965
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed May 29 10:58:10 2019 -0500
>
>         Plugins to backend, and Machines can change boot targets.
>
>     M   backend/dataTracker.go
>     M   backend/dataTracker_test.go
>     M   backend/machines.go
>     R094    midlayer/plugin.go  backend/plugin.go
>     R072    midlayer/actions.go backend/plugin_actions.go
>     R092    midlayer/controller.go  backend/plugin_controller.go
>     R099    midlayer/messaging.go   backend/plugin_messaging.go
>     R094    midlayer/messaging_client.go    backend/plugin_messaging_client.go
>     R099    midlayer/messaging_server.go    backend/plugin_messaging_server.go
>     M   backend/requestTracker.go
>     M   frontend/actions.go
>     M   frontend/frontend.go
>     M   frontend/job_create.go
>     M   midlayer/fake_midlayer_server_test.go
>     M   server/server.go
>
>     commit a2fcf9c1144fb1f65126ead95623126d8aa2c797
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed May 29 09:17:04 2019 -0500
>
>         Unexport several functions that should not be exported in the midlayer
>
>     M   midlayer/actions.go
>     M   midlayer/controller.go
>     M   midlayer/plugin.go
>
>     commit 0513c9f0683ea05d204d0338279abee020ad532e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 30 19:21:24 2019 -0500
>
>         Undo websocket debug.  Too much
>
>     M   frontend/websocket.go
>
>     commit 7063c3c8b3d46ae2d8842194248d1c25157f4839
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 30 19:17:21 2019 -0500
>
>         Allow client.Close() to actually close instead of hang.
>
>         Add debug to the websocket debug path
>
>         Lock the get info call to fix a concurrent map update.
>
>     M   frontend/websocket.go
>
>     commit dee1edda1295f9f72d79cd8b1e99eb662f18e48a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed May 29 09:57:22 2019 -0500
>
>         Enable CORS for static server
>
>     M   midlayer/static.go
>
>     commit db97007564a4071547b66b5a14cad11f2887c0ac
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue May 28 15:35:33 2019 -0500
>
>         Fix startup core
>
>     M   backend/requestTracker.go
>     M   midlayer/controller.go
>
>     commit 0e501c067ab9f3478dd75fd70cf09f68e41a19f2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue May 28 14:34:22 2019 -0500
>
>         Pass more info to plugins
>
>     M   midlayer/controller.go
>     M   midlayer/messaging.go
>
>     commit 9e7d25fe56103bab55862142bf032dffc175b79b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat May 25 12:07:57 2019 -0500
>
>         Allow the force parameter to be applied to machine create objects.
>         Update layers to allow the value to get to the machien create code
>         in the backend.
>
>     M   backend/machines.go
>     M   frontend/machines.go
>
>     commit aa6cd07e7ee573f102aa248c01d7840623de4283
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 23 15:46:36 2019 -0500
>
>         Allow the runner to list reservations.
>
>         This enables tasks to create reservations iff they are not already
>         created.
>
>     M   backend/renderData.go
>
>     commit 128dd32adf66fe4894bf0ad2804752619c70b07f
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu May 23 15:44:42 2019 -0500
>
>         Allow passing Secure params as arguments to runaction calls.
>
>         We already allow overriding all other Params for runaction calls, and
>         there is not a good reason to continue to do so for params that are
>         secure, as they will be secure in transit via the API.
>
>     M   frontend/actions.go
>
>     commit 1c426a2f47420e3b9f6d1ef411021fb96242a4a3
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue May 21 10:51:32 2019 -0500
>
>         Take a stab at fixing log levels for plugins.
>
>         This also allows messages emittend on stderr from the log to go
>         straight to stderr on dr-provision
>
>     M   frontend/prefs.go
>     M   midlayer/controller.go
>     M   midlayer/messaging.go
>     M   server/server.go
>
>     End of Note

v3.2.1 {#rs_rel_notes_drp-server_v3.2.1}
======

>     commit 94419da38918f8e096d0e2df732a5003e72d7049
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Nov 6 10:19:22 2017 -0600
>
>         Fix busted Stage revalidation on Bootenv validation update
>
>         Stages were not properly clearing their validation state before
>         revalidating when the availability of their associated BootEnv was
>         updated.
>
>         This change also updates how Stages are validated to allow them to be
>         flagged as valid (and not available) as long as their are no obvious
>         syntactic issues with the Stage.  In particular, we no longer require
>         that the associated BootEnv, Task, Template, or Profiles actually
>         exist, although we do theck to make sure that the Template list is at
>         least structurally correct.
>
>     M   backend/bootenv.go
>     M   backend/dataTracker_test.go
>     M   backend/stage.go
>     M   backend/stage_test.go
>
>     End of Note

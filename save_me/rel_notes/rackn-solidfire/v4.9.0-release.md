v4.9.0 {#rs_rel_notes_rackn-solidfire_v4.9.0}
======

>     commit ae6c34648f0d1f80643c9f1606afbcfd8e5ae1fe
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Mar 1 21:26:50 2022 -0600
>
>         build: update to v4.9.0
>
>     M   go.mod
>     M   go.sum
>
>     commit 79d6c2b1563db77c4127b7e0c12c0ecf6775ed2c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Dec 7 16:43:33 2021 -0600
>
>         Release v4.9.0 Initial
>
>     M   README.rst
>
>     commit fbdeaaaf68163e5dc9e6c9558c549ee181c5db4a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Dec 7 16:43:09 2021 -0600
>
>         build: update to v4.8.0
>
>     M   go.mod
>     M   go.sum
>
>     commit bdba3fd817baa45ad11f8c677390e375e017eee6
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Dec 4 19:53:51 2021 +0000
>
>         Update solidfire context icon
>
>     M   content/contexts/solidfire.yaml
>
>     commit 2c49a9a57ee8b9c14b31ac3d8ab0e8170be3295d
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Thu Oct 21 16:01:49 2021 -0400
>
>         feat(context): update solidfire context
>
>     M   content/contexts/solidfire.yaml
>
>     commit 7a8166fb9f6c79d900b4762c3856e62267c2788e
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Wed Oct 13 10:36:21 2021 -0400
>
>         feat(context): update context
>
>     M   content/contexts/solidfire.yaml
>
>     commit 5546dace1fcae130588ab37129387c40e59fc769
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Tue Sep 7 15:45:58 2021 -0400
>
>         feat(gitlab): Migrate module to gitlab
>
>     M   content/._CodeSource.meta
>     M   go.mod
>     M   go.sum
>     M   tools/drpcli.go
>     M   version.go
>
>     commit 3b29768a1bf595243ada50cf28b4449b4179be5c
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Fri Sep 3 14:39:46 2021 -0400
>
>         fix(ci): cleaning up trigger
>
>     M   .gitlab-ci.yml
>
>     commit 93105d99cbc962bc9b86dde16ebdab87b5bde472
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Fri Sep 3 10:35:16 2021 -0500
>
>         feat(gitlab): go.mod replace and CLI build fixups for gitlab
>
>     M   go.mod
>     M   go.sum
>     A   tools/drpcli.go
>     M   tools/package.sh
>
>     commit 8aa64770d8f8b6bb759175b1b6680ee5d68d3bdc
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Thu Sep 2 18:21:49 2021 -0400
>
>         fix(ci): triggering the right rackn-saas branch
>
>     M   .gitlab-ci.yml
>
>     commit 8110924dfface0d38db31139e3c6eba193e284e1
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Wed Sep 1 13:39:32 2021 -0400
>
>         build: gitlab ci
>
>     A   .gitlab-ci.yml
>
>     End of Note

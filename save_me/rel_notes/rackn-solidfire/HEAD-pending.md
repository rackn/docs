.6

> Fixes rackn/provision-content\#449

> M content/contexts/solidfire.yaml
>
> commit 0d703f980267d874a0628e695b8b31aa1dc42a16 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Tue Jun 21 18:09:35 2022 -0500
>
> > fix(solidfire): fix context name
>
> M content/profiles/bootstrap-solidfire.yaml
>
> commit 4dd9e11cb238d6b492a360e8c268486acdc28eff Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Jun 20 02:19:32 2022 -0500
>
> > feat(bootstrap): add profile to bootstrap solidfire contexts
> >
> > Fixes rackn/product-backlog\#216
>
> A content/profiles/bootstrap-solidfire.yaml
>
> commit b5fbb3de9b76a954c3962419fb6ada07b1643706 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Wed Jun 8 14:51:26 2022 -0500
>
> > doc: fix rel\_notes and don\'t publish unless on tip
>
> M tools/build\_rel\_notes.sh M tools/package.sh
>
> commit 8f7ae8c3d7b90f4bf999f2551074e539dcecdaf7 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Tue Mar 8 16:43:12 2022 -0500
>
> > fix(ci) fixing gitlab runner image
>
> M .gitlab-ci.yml
>
> End of Note

v4.11.0-alpha00 {#rs_rel_notes_rackn-solidfire_v4.11.0-alpha00}
===============

>     commit 40fc0dcab6294e3eb4d6c4df7830cd2043d8e7e2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 8 17:40:41 2022 -0500
>
>         Release v4.11.0 Initial
>
>     M   README.rst
>
>     commit c5032991d1a30e3976c7bb3f6f835f17612117b8
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 8 17:40:32 2022 -0500
>
>         build: update to v4.10.0
>
>     M   go.mod
>     M   go.sum
>
>     commit 23679ea71dc0ea40e689a0878ac549ec57855e17
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jul 20 09:01:30 2022 -0500
>
>         build: update remarshal call
>
>         Signed-off-by: Greg Althaus <galthaus@austin.rr.com>
>
>     M   tools/publish.sh
>
>     commit d5ead79e6e7c93e73e297f3eda17d8135076fc8a
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Wed Jul 20 09:34:46 2022 -0400
>
>         feat(context): update context
>
>         Updated to v1.2.6
>
>         Fixes rackn/provision-content#449
>
>     M   content/contexts/solidfire.yaml
>
>     commit 0d703f980267d874a0628e695b8b31aa1dc42a16
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 21 18:09:35 2022 -0500
>
>         fix(solidfire): fix context name
>
>     M   content/profiles/bootstrap-solidfire.yaml
>
>     commit 4dd9e11cb238d6b492a360e8c268486acdc28eff
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jun 20 02:19:32 2022 -0500
>
>         feat(bootstrap): add profile to bootstrap solidfire contexts
>
>         Fixes rackn/product-backlog#216
>
>     A   content/profiles/bootstrap-solidfire.yaml
>
>     commit b5fbb3de9b76a954c3962419fb6ada07b1643706
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 8 14:51:26 2022 -0500
>
>         doc: fix rel_notes and don't publish unless on tip
>
>     M   tools/build_rel_notes.sh
>     M   tools/package.sh
>
>     commit 8f7ae8c3d7b90f4bf999f2551074e539dcecdaf7
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Tue Mar 8 16:43:12 2022 -0500
>
>         fix(ci) fixing gitlab runner image
>
>     M   .gitlab-ci.yml
>
>     End of Note

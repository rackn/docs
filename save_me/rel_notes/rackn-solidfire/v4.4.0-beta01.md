v4.4.0-beta01 {#rs_rel_notes_rackn-solidfire_v4.4.0-beta01}
=============

>     commit 2d49b72728a60fabd59af6aed696de4ad5d84578
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jul 20 21:06:52 2020 -0500
>
>         build: update v4.4.0-beta01
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

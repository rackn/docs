v4.6.0 {#rs_rel_notes_rackn-solidfire_v4.6.0}
======

>     commit 0052dc2e536136ad218055d0e03c029abd5835a4
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Apr 29 23:40:59 2021 -0500
>
>         biuld: update to v4.6.0
>
>     M   go.mod
>     M   go.sum
>
>     commit 001ca58fd2b21987baf8ed309425bd6cb860d5ea
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Apr 26 10:15:12 2021 -0500
>
>         fix(monitorWait): make wait in rtfi-monitor configurable.
>
>         The old code used to error out the solidfire-monitor-rtfi task
>         after going 30 minutes without being able to contact the Solidfire
>         API on the node being reimaged.  Bump that default timeout to 60,
>         and make it configurable.
>
>     M   .gitignore
>     A   content/params/solidfire.monitor-wait.yaml
>     M   content/tasks/solidfire-monitor-rtfi.yaml
>
>     commit 00d6f6e16c19c12e8ede60fa9aa9e87aa04a4a7f
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Sat Mar 20 11:46:05 2021 -0700
>
>         fix(doc): Doc tweaks to fix broken RST
>
>     M   content/tasks/solidfire-test-eligibility.yaml
>
>     commit 080de58a67f529243676cc8930c1e369f147439a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Feb 23 17:43:52 2021 -0600
>
>         fix(solidfire): Make sure HOME is set for jq
>
>     M   content/tasks/solidfire-configure-network.yaml
>     M   content/tasks/solidfire-hack-up-network-json.yaml
>     M   content/tasks/solidfire-monitor-rtfi.yaml
>     M   content/tasks/solidfire-release-reservation.yaml
>     M   content/tasks/solidfire-reserve-our-address.yaml
>     M   content/tasks/solidfire-test-eligibility.yaml
>     M   content/tasks/solidfire-wait-for-api.yaml
>
>     commit 6f289ef091c52170edb36ea859ab991034bc9c60
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Sep 24 23:58:16 2020 -0500
>
>         Release v4.6.0 Initial
>
>     M   README.rst
>
>     commit 88d7a34ffa92681909d8005c42bf646509a39696
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Sep 24 23:57:16 2020 -0500
>
>         build: update to v4.5.0
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

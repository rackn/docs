v4.5.3 {#rs_rel_notes_rackn-cohesity_v4.5.3}
======

>     commit 540f40723c702c085d733446cd235201c242ba4c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jan 28 14:45:48 2021 -0600
>
>         fix(cohesity) put back old items
>
>     A   content/tasks/cohesity-copy-release.yaml
>     A   content/tasks/cohesity-create-user.yaml
>     R100    content/tasks/cohesity-partition-data-6.3.1.yaml    content/tasks/cohesity-partition-home-6.3.1.yaml
>     A   content/tasks/cohesity-partition-home.yaml
>
>     End of Note

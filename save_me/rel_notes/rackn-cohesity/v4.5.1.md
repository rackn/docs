v4.5.1 {#rs_rel_notes_rackn-cohesity_v4.5.1}
======

>     commit 88f8e94ba3e757a2c1169b5634d659c028816bd0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Sep 25 10:55:03 2020 -0500
>
>         build: update go version to 1.13
>
>     M   .travis.yml
>
>     commit 17f9e5266be9ee540b4cd69ae4bbea913631a01f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Sep 24 23:51:44 2020 -0500
>
>         build: add readme
>
>     A   README.md
>
>     commit 9f93446059b786e278da1c6ad24aff82d7a2918b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Sep 24 23:49:18 2020 -0500
>
>         build: update to v4.5.0
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

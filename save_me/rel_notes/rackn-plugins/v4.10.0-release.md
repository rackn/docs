c8d612f5b9e7114399552857008af048ce1067

:   Author: Greg Althaus \<<galthaus@austin.rr.com>\> Date: Tue Mar 1
    21:17:31 2022 -0600

    > build: update to v4.9.0

    M go.mod M go.sum

    commit 2cd3ea21fd3785738ac7990f49ad54a4bbe604dc Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Feb 28 23:30:25 2022 -0600

    > fix: convert to single meta.json file

    M cmds/ad-auth/ad-auth.go D cmds/ad-auth/content/.\_Author.meta D
    cmds/ad-auth/content/.\_CodeSource.meta D
    cmds/ad-auth/content/.\_Color.meta D
    cmds/ad-auth/content/.\_Copyright.meta D
    cmds/ad-auth/content/.\_Description.meta D
    cmds/ad-auth/content/.\_DisplayName.meta D
    cmds/ad-auth/content/.\_DocUrl.meta D
    cmds/ad-auth/content/.\_Documentation.meta D
    cmds/ad-auth/content/.\_Icon.meta D
    cmds/ad-auth/content/.\_License.meta D
    cmds/ad-auth/content/.\_Name.meta D
    cmds/ad-auth/content/.\_Order.meta D
    cmds/ad-auth/content/.\_Source.meta D
    cmds/ad-auth/content/.\_Tags.meta A cmds/ad-auth/content/meta.yaml M
    cmds/agent/agent.go D cmds/agent/content/.\_Author.meta D
    cmds/agent/content/.\_CodeSource.meta D
    cmds/agent/content/.\_Color.meta D
    cmds/agent/content/.\_Copyright.meta D
    cmds/agent/content/.\_Description.meta D
    cmds/agent/content/.\_DisplayName.meta D
    cmds/agent/content/.\_DocUrl.meta D
    cmds/agent/content/.\_Documentation.meta D
    cmds/agent/content/.\_Icon.meta D cmds/agent/content/.\_License.meta
    D cmds/agent/content/.\_Name.meta D cmds/agent/content/.\_Order.meta
    D cmds/agent/content/.\_Source.meta D
    cmds/agent/content/.\_Tags.meta A cmds/agent/content/meta.yaml M
    cmds/billing/billing.go D cmds/billing/content/.\_Author.meta D
    cmds/billing/content/.\_CodeSource.meta D
    cmds/billing/content/.\_Color.meta D
    cmds/billing/content/.\_Copyright.meta D
    cmds/billing/content/.\_Description.meta D
    cmds/billing/content/.\_DisplayName.meta D
    cmds/billing/content/.\_DocUrl.meta D
    cmds/billing/content/.\_Documentation.meta D
    cmds/billing/content/.\_Icon.meta D
    cmds/billing/content/.\_License.meta D
    cmds/billing/content/.\_Name.meta D
    cmds/billing/content/.\_Order.meta D
    cmds/billing/content/.\_Tags.meta A cmds/billing/content/meta.yaml D
    cmds/docker-context/content/.\_Author.meta D
    cmds/docker-context/content/.\_Color.meta D
    cmds/docker-context/content/.\_Copyright.meta D
    cmds/docker-context/content/.\_Description.meta D
    cmds/docker-context/content/.\_DisplayName.meta D
    cmds/docker-context/content/.\_DocUrl.meta D
    cmds/docker-context/content/.\_Documentation.meta D
    cmds/docker-context/content/.\_Icon.meta D
    cmds/docker-context/content/.\_Name.meta D
    cmds/docker-context/content/.\_Order.meta D
    cmds/docker-context/content/.\_Prerequisites.meta D
    cmds/docker-context/content/.\_Tags.meta A
    cmds/docker-context/content/meta.yaml M
    cmds/docker-context/docker.go D cmds/rack/content/.\_Author.meta D
    cmds/rack/content/.\_CodeSource.meta D
    cmds/rack/content/.\_Color.meta D
    cmds/rack/content/.\_Copyright.meta D
    cmds/rack/content/.\_Description.meta D
    cmds/rack/content/.\_DisplayName.meta D
    cmds/rack/content/.\_DocUrl.meta D
    cmds/rack/content/.\_Documentation.meta D
    cmds/rack/content/.\_Icon.meta D cmds/rack/content/.\_License.meta D
    cmds/rack/content/.\_Name.meta D cmds/rack/content/.\_Order.meta D
    cmds/rack/content/.\_Source.meta D cmds/rack/content/.\_Tags.meta A
    cmds/rack/content/meta.yaml M cmds/rack/rack.go D
    cmds/tenant-controller/content/.\_Author.meta D
    cmds/tenant-controller/content/.\_CodeSource.meta D
    cmds/tenant-controller/content/.\_Color.meta D
    cmds/tenant-controller/content/.\_Copyright.meta D
    cmds/tenant-controller/content/.\_Description.meta D
    cmds/tenant-controller/content/.\_DisplayName.meta D
    cmds/tenant-controller/content/.\_DocUrl.meta D
    cmds/tenant-controller/content/.\_Documentation.meta D
    cmds/tenant-controller/content/.\_Icon.meta D
    cmds/tenant-controller/content/.\_License.meta D
    cmds/tenant-controller/content/.\_Name.meta D
    cmds/tenant-controller/content/.\_Order.meta D
    cmds/tenant-controller/content/.\_Tags.meta A
    cmds/tenant-controller/content/meta.yaml M
    cmds/tenant-controller/tenant-controller.go D
    cmds/ux-views/content/.\_Author.meta D
    cmds/ux-views/content/.\_CodeSource.meta D
    cmds/ux-views/content/.\_Color.meta D
    cmds/ux-views/content/.\_Copyright.meta D
    cmds/ux-views/content/.\_Description.meta D
    cmds/ux-views/content/.\_DisplayName.meta D
    cmds/ux-views/content/.\_DocUrl.meta D
    cmds/ux-views/content/.\_Documentation.meta D
    cmds/ux-views/content/.\_Icon.meta D
    cmds/ux-views/content/.\_License.meta D
    cmds/ux-views/content/.\_Name.meta D
    cmds/ux-views/content/.\_Order.meta D
    cmds/ux-views/content/.\_Source.meta D
    cmds/ux-views/content/.\_Tags.meta A cmds/ux-views/content/meta.yaml
    M cmds/ux-views/ux-views.go M go.mod M go.sum M tools/build-one.sh

    commit 8411e04c2f3446cdb9aa2c507f6b6e53afb6a504 Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Wed Jan 12 13:28:00 2022 -0500

    > fix(docker-context): build content and context typo

    M cmds/docker-context/content/contexts/drpcli-runner.yaml M
    cmds/docker-context/docker.go

    commit bbaac28c0dc1f5e318b976a6553239053d7d6dbb Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Tue Jan 11 14:22:37 2022 -0500

    > feat(docker-context): container info parameter
    >
    > feat(docker-context): move content to files

    A cmds/docker-context/content/.\_Author.meta A
    cmds/docker-context/content/.\_Color.meta A
    cmds/docker-context/content/.\_Copyright.meta A
    cmds/docker-context/content/.\_Description.meta A
    cmds/docker-context/content/.\_DisplayName.meta A
    cmds/docker-context/content/.\_DocUrl.meta A
    cmds/docker-context/content/.\_Documentation.meta A
    cmds/docker-context/content/.\_Icon.meta A
    cmds/docker-context/content/.\_Name.meta A
    cmds/docker-context/content/.\_Order.meta A
    cmds/docker-context/content/.\_Prerequisites.meta A
    cmds/docker-context/content/.\_Tags.meta A
    cmds/docker-context/content/contexts/drpcli-runner.yaml M
    cmds/docker-context/docker.go

    commit 34da08af09ba966c49ab09c2c63fd6f4cbb2b409 Author: Meshiest
    \<<gpg@reheatedcake.io>\> Date: Fri Dec 17 13:21:01 2021 -0600

    > fix(ux-views): superuser view default columns always latest

    M cmds/ux-views/content/ux\_views/superuser.yaml

    commit c4b7567be86f96f859ef8fa0b271abe5ee75fa3c Author: Tim Bosse
    \<<tim@rackn.com>\> Date: Fri Dec 17 12:23:56 2021 -0500

    > fix(docker-context): Update entrypoint for docker

    M cmds/docker-context/docker.go

    End of Note

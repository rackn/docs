v4.3.0 {#rs_rel_notes_rackn-plugins_v4.3.0}
======

>     commit e84361a346cf7c6e39c465c6d5cb7029e2cb777a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 4 11:56:17 2020 -0500
>
>         build: Update to v4.3.0 tags
>
>     M   .travis.yml
>     M   go.mod
>     M   go.sum
>
>     commit 5b0ddf7866259c96e8c480446130fc9cb3f5b805
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 2 12:25:31 2020 -0500
>
>         build: update gitignore
>
>     M   .gitignore
>
>     commit a256940d0ed1ab6ec4358bf718043169d738343e
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu May 21 21:02:29 2020 -0500
>
>         feat(rack): Allow production network to be specified by prodnet column
>
>         This replaces the "host" always choice and replaces it with a column
>         that lets you choose what is on which production network.
>
>     M   cmds/rack/rack-actions.go
>     M   cmds/rack/rack.go
>
>     commit 861aa34c13cda2fecd9afedaf75b3b4e737d6b7c
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue May 5 18:11:35 2020 -0700
>
>         doc(ad-auth): Additional doc update for ad-auth plugin
>
>     M   cmds/ad-auth/content/._Documentation.meta
>     M   cmds/ad-auth/content/params/ad-auth.ad-tls.yaml
>     M   cmds/ad-auth/content/params/ad-auth.ad-url.yaml
>     M   cmds/ad-auth/content/params/ad-auth.additional-dns.yaml
>     M   cmds/ad-auth/content/params/ad-auth.base-dn.yaml
>     M   cmds/ad-auth/content/params/ad-auth.user-activity-check.yaml
>     M   cmds/ad-auth/content/params/ad-auth.user-activity-window.yaml
>
>     commit 8ad10943a2540c608954e21018c2a1c2e09ebeff
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Sat May 2 22:28:41 2020 -0700
>
>         docs(ad-auth): Remove header and reference
>
>     M   cmds/ad-auth/content/._Documentation.meta
>
>     commit 11dead9cd0a519b5151c4b46f7b3236fd4555e07
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat May 2 22:03:07 2020 -0500
>
>         build: v4.3.0-beta6 update
>
>     M   go.mod
>     M   go.sum
>
>     commit 340ddbe0de95d5af44a02820d9b2b3c28ccfbdc2
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Sat May 2 12:24:01 2020 -0700
>
>         doc(ad-auth): Restructure main doc, correct username/password details.
>
>     M   cmds/ad-auth/content/._Documentation.meta
>     M   cmds/ad-auth/content/params/auth.password.yaml
>     M   cmds/ad-auth/content/params/auth.username.yaml
>
>     commit a376d3310c6245f994abb6573721ae6c594dfae6
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Sat May 2 11:02:35 2020 -0700
>
>         Fix a couple of typos.
>
>     M   cmds/ad-auth/content/._Documentation.meta
>
>     commit e0bc8eb3c51362587a19d3d309ae050093ddb497
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Fri May 1 17:26:24 2020 -0700
>
>         doc(ad-auth): Enhance doc with examples and formatting cleanup
>
>     M   cmds/ad-auth/content/._Documentation.meta
>     M   cmds/ad-auth/content/params/ad-auth.ad-tls.yaml
>     M   cmds/ad-auth/content/params/ad-auth.ad-url.yaml
>     M   cmds/ad-auth/content/params/ad-auth.additional-dns.yaml
>     M   cmds/ad-auth/content/params/ad-auth.base-dn.yaml
>     M   cmds/ad-auth/content/params/ad-auth.default-role.yaml
>     M   cmds/ad-auth/content/params/ad-auth.deny-if-no-groups.yaml
>     M   cmds/ad-auth/content/params/ad-auth.group-roles-map.yaml
>     M   cmds/ad-auth/content/params/ad-auth.groups.yaml
>     M   cmds/ad-auth/content/params/ad-auth.user-activity-check.yaml
>     M   cmds/ad-auth/content/params/ad-auth.user-activity-window.yaml
>     M   cmds/ad-auth/content/params/auth.password.yaml
>     M   cmds/ad-auth/content/params/auth.username.yaml
>
>     commit 1757c607ebc76856eb8e51f028bf197566c0929b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Apr 29 22:07:46 2020 -0500
>
>         docs(ad-auth): Clarify deny if no group funtionality
>
>     M   cmds/ad-auth/content/._Documentation.meta
>
>     commit f1ff238291cde53b4ceae5ea49910172ab38346a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 28 15:39:07 2020 -0500
>
>         fix(agent): Fix the name of agent to be agent
>
>     M   cmds/agent/content/._Name.meta
>
>     commit 3501732108ae28b53f9f435469d2ddeb83a07a93
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 28 14:56:15 2020 -0500
>
>         doc(ad-auth): Add an oversection and tweak a couple of parameters
>
>     A   cmds/ad-auth/content/._Documentation.meta
>     M   cmds/ad-auth/content/params/ad-auth.default-role.yaml
>     M   cmds/ad-auth/content/params/ad-auth.deny-if-no-groups.yaml
>
>     commit bcd65d3d8ab96821846519e0d89d5590e23c081c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 21 18:08:34 2020 -0500
>
>         fix(rack): build break slipped through
>
>     M   cmds/rack/rack.go
>
>     commit 7d18e289be6609cf8126aa6ac598ce93009a752b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Apr 21 17:55:01 2020 -0500
>
>         docs(ux-views): fix doc formatting issues
>
>     M   cmds/ux-views/content/._Documentation.meta
>
>     commit e8549ab13dd1e177c91d81d898fec3f0bb67725d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Mar 25 13:32:18 2020 -0500
>
>         Trim spaces from , separated lists
>
>     M   cmds/rack/rack.go
>
>     commit cd882d57843f7c35736b460d4c50694ac82aa101
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Mar 3 12:56:58 2020 -0600
>
>         Fix v4.3.0-beta2
>
>     M   go.mod
>     M   go.sum
>
>     commit 9d5e65795362433857f7a58f65c46ff6312bb878
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Mon Feb 17 16:26:42 2020 -0600
>
>         add cloudia handle option to ux-views plugin
>
>     A   cmds/ux-views/content/ux_options/cloudia.inbox.handle.yaml
>
>     End of Note

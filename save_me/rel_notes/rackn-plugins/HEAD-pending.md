<us@austin.rr.com>\>

:   Date: Sun Sep 4 17:39:05 2022 -0500

    > feat(ux-views): remove ux-views from the build/tree - moving to
    > drp
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    D cmds/ux-views/content/filters/uxv-debug.yaml D
    cmds/ux-views/content/filters/uxv-not-runnable.yaml D
    cmds/ux-views/content/filters/uxv-runnable.yaml D
    cmds/ux-views/content/filters/uxv-writable-bootenvs.yaml D
    cmds/ux-views/content/filters/uxv-writable-catalog\_items.yaml D
    cmds/ux-views/content/filters/uxv-writable-endpoints.yaml D
    cmds/ux-views/content/filters/uxv-writable-params.yaml D
    cmds/ux-views/content/filters/uxv-writable-plugins.yaml D
    cmds/ux-views/content/filters/uxv-writable-profiles.yaml D
    cmds/ux-views/content/filters/uxv-writable-reservations.yaml D
    cmds/ux-views/content/filters/uxv-writable-roles.yaml D
    cmds/ux-views/content/filters/uxv-writable-stages.yaml D
    cmds/ux-views/content/filters/uxv-writable-subnets.yaml D
    cmds/ux-views/content/filters/uxv-writable-tasks.yaml D
    cmds/ux-views/content/filters/uxv-writable-templates.yaml D
    cmds/ux-views/content/filters/uxv-writable-tenants.yaml D
    cmds/ux-views/content/filters/uxv-writable-users.yaml D
    cmds/ux-views/content/filters/uxv-writable-ux\_views.yaml D
    cmds/ux-views/content/filters/uxv-writable-version\_sets.yaml D
    cmds/ux-views/content/filters/uxv-writable-workflows.yaml D
    cmds/ux-views/content/meta.yaml D
    cmds/ux-views/content/roles/operator.yaml D
    cmds/ux-views/content/roles/readonly.yaml D
    cmds/ux-views/content/ux\_options/cloudia.inbox.enabled.yaml D
    cmds/ux-views/content/ux\_options/cloudia.inbox.handle.yaml D
    cmds/ux-views/content/ux\_options/cloudia.inbox.interval.yaml D
    cmds/ux-views/content/ux\_options/cloudia.inbox.secret.yaml D
    cmds/ux-views/content/ux\_options/ux.catalog.dev\_url.yaml D
    cmds/ux-views/content/ux\_options/ux.catalog.stable\_url.yaml D
    cmds/ux-views/content/ux\_options/ux.catalog.tip\_url.yaml D
    cmds/ux-views/content/ux\_options/ux.core.airgap.yaml D
    cmds/ux-views/content/ux\_options/ux.cosmetic.navbar\_color.yaml D
    cmds/ux-views/content/ux\_options/ux.editor.show\_whitespace.yaml D
    cmds/ux-views/content/ux\_options/ux.security.inactivity.duration.yaml
    D
    cmds/ux-views/content/ux\_options/ux.security.inactivity.enabled.yaml
    D
    cmds/ux-views/content/ux\_options/ux.security.token.check\_interval.yaml
    D cmds/ux-views/content/ux\_options/ux.security.token.lifetime.yaml
    D
    cmds/ux-views/content/ux\_options/ux.security.token.renew\_period.yaml
    D cmds/ux-views/content/ux\_views/operator.yaml D
    cmds/ux-views/content/ux\_views/readonly.yaml D
    cmds/ux-views/content/ux\_views/reference.yaml D
    cmds/ux-views/content/ux\_views/superuser.yaml D
    cmds/ux-views/test-data/Cli0101/stderr.actual D
    cmds/ux-views/test-data/Cli0101/stdout.actual D
    cmds/ux-views/test-data/Cli0102/stderr.actual D
    cmds/ux-views/test-data/Cli0102/stdout.actual D
    cmds/ux-views/test-data/Cli0103/stderr.actual D
    cmds/ux-views/test-data/Cli0103/stdout.actual D
    cmds/ux-views/test-data/Cli0104/stderr.actual D
    cmds/ux-views/test-data/Cli0104/stdout.actual D
    cmds/ux-views/test-data/Cli0105/stderr.actual D
    cmds/ux-views/test-data/Cli0105/stdout.actual D
    cmds/ux-views/test-data/Cli0106/stderr.actual D
    cmds/ux-views/test-data/Cli0106/stdout.actual D
    cmds/ux-views/tests/.gitignore D
    cmds/ux-views/tests/test-data/Cli0101/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0101/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0102/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0102/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0103/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0103/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0104/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0104/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0105/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0105/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0106/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0106/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0107/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0107/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0108/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0108/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0201/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0201/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0202/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0202/stdout.expect D
    cmds/ux-views/tests/test-data/Cli0203/stderr.expect D
    cmds/ux-views/tests/test-data/Cli0203/stdout.expect D
    cmds/ux-views/tests/ux-view\_test.sh D
    cmds/ux-views/ux-views-object.go D cmds/ux-views/ux-views.go

    commit b89caef2b16ec2dc25a8205b610c61bf70426c54 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 13:38:26 2022 -0500

    > build: docs to version specific tree
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M tools/build\_rel\_notes.sh M tools/publish.sh

    commit 2f4d5970c317f7d99df005502eda006bfd0c3af4 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 12:54:05 2022 -0500

    > build: update docs into branch specific pieces
    >
    > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>

    M tools/build\_rel\_notes.sh M tools/publish.sh

    End of Note

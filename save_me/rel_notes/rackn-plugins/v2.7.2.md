v2.7.2 {#rs_rel_notes_rackn-plugins_v2.7.2}
======

>     commit bf3869fdbf575ed63dd04b7804439e71f958f84b
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Apr 8 15:58:58 2019 -0500
>
>         Do not fail if we cannot delete the IPMI plugin object at validation time.
>
>         Most likely, the delete failed bacause the plugin does not exist, as is proper.
>
>         In the unlikely event that we fail to delete a user-provided ipmi
>         plugin, we will fail at content layer load time.
>
>     M   cmds/ipmi/ipmi.go
>
>     End of Note

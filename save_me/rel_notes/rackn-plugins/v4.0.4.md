v4.0.4 {#rs_rel_notes_rackn-plugins_v4.0.4}
======

>     commit 7000d6a062788c15ea0a27db1cc1978ae1dc179a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 15 09:25:55 2019 -0500
>
>         uPdate the sum file
>
>     M   go.sum
>
>     commit bbd624f00e70372f483f0b549993061bd60b690c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 15 09:24:17 2019 -0500
>
>         Revert bad push
>
>     M   cmds/manager/endpoint-actions.go
>
>     commit a1d00065ae585ea78ad6ae1cd355da0034f2cb2f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 15 00:47:38 2019 -0500
>
>         Put back the timeout in seconds
>
>     M   cmds/manager/endpoint-actions.go
>
>     commit 2abbfa6a538b40472f3874be6f5ed6fc37dd4b35
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 12 19:48:08 2019 -0500
>
>         Undo timeout loop debugging
>
>     M   cmds/manager/endpoint-actions.go
>
>     commit 3cf6aef12dbb3568b462fafa285a6ec664b43119
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 12 19:38:07 2019 -0500
>
>         Update unit tests for v4.
>
>         Validate that machines can be force updated/created.
>
>     M   cmds/manager/endpoint-actions.go
>     M   cmds/manager/tests/manager_test.sh
>     M   cmds/manager/tests/rackn-catalog.json
>     M   cmds/manager/tests/test-data/Cli00010/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01010/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01060/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01180/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01310/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01570/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01590/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01610/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01720/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01730/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01760/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01770/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01780/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01790/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01821/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01822/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01823/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01830/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01840/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01850/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01851/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01852/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01853/stdout.expect
>     M   cmds/manager/tests/test-data/Cli01910/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02005/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02080/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02100/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02110/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02180/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02210/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02211/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02212/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02213/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02214/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02220/stdout.expect
>     M   cmds/manager/tests/test-data/Cli02330/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03005/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03007/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03008/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03019/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03022/stdout.expect
>     M   cmds/manager/tests/test-data/Cli03029/stdout.expect
>     M   cmds/manager/tests/test-data/Cli04000/stdout.expect
>     M   cmds/manager/tests/test-data/Cli04010/stdout.expect
>     M   go.mod
>     M   go.sum
>
>     End of Note

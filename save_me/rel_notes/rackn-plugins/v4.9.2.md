v4.9.2 {#rs_rel_notes_rackn-plugins_v4.9.2}
======

>     commit b82af413f275cfd1980b7db91d4050226cd65bfa
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jul 27 15:11:53 2022 -0500
>
>         Pull in latest provision code
>
>     M   go.mod
>     M   go.sum
>
>     End of Note

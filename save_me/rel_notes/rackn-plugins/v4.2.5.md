v4.2.5 {#rs_rel_notes_rackn-plugins_v4.2.5}
======

>     commit c7e1e60a0e5abea626699a178160a71aded325e0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Feb 6 14:51:10 2020 -0600
>
>         Clean more hostname
>
>     M   cmds/rack/rack-actions.go
>
>     commit 5c290a7b715e462142a9e8e34069a1a67ca2b571
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Feb 6 14:44:42 2020 -0600
>
>         Parameters using mac in name will be tested with lower case.
>
>         If DNS-domain is specified, when naming a host, use the dns-domain
>         value.
>
>     M   cmds/rack/rack-actions.go
>
>     End of Note

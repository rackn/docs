v4.8.1 {#rs_rel_notes_rackn-plugins_v4.8.1}
======

>     commit 68bca47f3fe99db4333e74a5b7f474371c7e7114
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jul 19 14:10:17 2022 -0500
>
>         fix: Fallback on the bind mount to copying if container create fails.
>
>         This is a global option so if docker or podman is updated, the
>         plugin should be restart to take advantage of the new supported flags.
>
>     M   cmds/docker-context/docker.go
>
>     commit 99ecd21eaba16629fc1f9a7a5ffd3804c567b072
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 19 13:15:29 2022 -0500
>
>         fix(docker-context): Use `ro` for `readonly` when running Podman.
>
>         Podman has had support for read-only bind-mounts for a long time, assuming
>         the kernel supports it.  It uses the `ro` flag for this, following ages-old
>         mount and fstab conventions.  Docker chose to use the `readonly` flag for this
>         instead.  Podman added suport for `readonly` only with the 2.0.0 release.
>
>         When we are using podman, use `ro` unconditionally for read-only bind mounts.
>
>     M   cmds/docker-context/docker.go
>
>     commit eaa14161910efb93723628f577c7cc86a666ba9f
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Tue Mar 8 16:26:48 2022 -0500
>
>         fix(ci) fixing gitlab runner image
>
>     M   .gitignore
>     M   .gitlab-ci.yml
>
>     commit aee12166688731b0e81a58c52a8b607ae5abff68
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Dec 7 16:32:04 2021 -0600
>
>         build: update to v4.8.0
>
>     M   go.mod
>     M   go.sum
>
>     commit 5d84b6928a640ebb6194695510573eb01f9eac4c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Dec 5 21:18:14 2021 -0600
>
>         fix(docker-context): update to work on MAC and work around the mount issues
>
>     M   cmds/docker-context/docker.go
>
>     commit 2bddc59da6c81d07fbe59f95af21dcd8c5c64c29
>     Author: Meshiest <gpg@reheatedcake.io>
>     Date:   Fri Dec 3 14:37:58 2021 -0600
>
>         fix(ux-views): superuser view default nav is always latest
>
>     M   cmds/ux-views/content/ux_views/superuser.yaml
>
>     commit 23a8e149f66c11f513f4bd0c5fc08aecab15147c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Nov 22 14:27:29 2021 -0600
>
>         fix(docker-context): handle drpcli updates appropriately
>
>     M   cmds/docker-context/docker.go
>
>     commit a1ada3632f5fea09d3bf2e3ee2c91771409f83ee
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Thu Oct 21 16:03:18 2021 -0400
>
>         feat(docker-context): update drpcli-runner
>
>         fix(docker-context): ID should be Id
>
>     M   cmds/docker-context/docker.go
>
>     commit cc25405c746637bbfad94081b6945a6dd6c5abb4
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Tue Oct 5 14:12:22 2021 -0400
>
>         Update documentation and context details
>
>     M   cmds/docker-context/docker.go
>
>     commit a939aaf52bb156503b61afb3d2338bc71d9d7daf
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Oct 5 00:07:38 2021 -0500
>
>         fix: update to provision
>
>     M   cmds/agent/k8s.go
>     M   go.mod
>     M   go.sum
>
>     commit f83f1abd04599845e589bb71bfc27704a20b6015
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Mon Oct 4 16:58:48 2021 -0400
>
>         feat(containers): update locations
>
>     M   cmds/docker-context/docker.go
>
>     commit 06694b0b4d513963e44c0bddae286e172447fb78
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Sep 28 15:11:57 2021 -0500
>
>         fix: update for clusters resourcebrokers
>
>     M   cmds/docker-context/docker.go
>
>     commit 92eb76a1dd7376816eb26d82287b463304f3d83d
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Wed Sep 8 17:54:03 2021 -0400
>
>         feat(gitlab): Migrate module to gitlab
>
>     M   README.md
>     M   cmds/ad-auth/ad-auth.go
>     M   cmds/ad-auth/auth.go
>     M   cmds/ad-auth/content/._CodeSource.meta
>     M   cmds/ad-auth/tests/ad_auth_test.sh
>     M   cmds/agent/agent.go
>     M   cmds/agent/content/._CodeSource.meta
>     M   cmds/agent/k8s.go
>     M   cmds/billing/billing-object.go
>     M   cmds/billing/billing.go
>     M   cmds/billing/content/._CodeSource.meta
>     M   cmds/docker-context/docker.go
>     M   cmds/rack/content/._CodeSource.meta
>     M   cmds/rack/rack-actions.go
>     M   cmds/rack/rack-object.go
>     M   cmds/rack/rack.go
>     M   cmds/tenant-controller/content/._CodeSource.meta
>     M   cmds/tenant-controller/tenant-controller.go
>     M   cmds/tenant-controller/tests/tc_test.sh
>     M   cmds/ux-views/content/._CodeSource.meta
>     M   cmds/ux-views/ux-views-object.go
>     M   cmds/ux-views/ux-views.go
>     M   go.mod
>     M   go.sum
>     M   tools/build-one.sh
>
>     commit 63a492c52582c38e318546b35e2f91a34eafad0e
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Fri Sep 3 14:38:05 2021 -0400
>
>         fix(ci): cleaning up trigger
>
>     M   .gitlab-ci.yml
>
>     commit 54205900b5bfac4ae9f5cfea8d2fe244fcc2bd0d
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Thu Sep 2 18:19:28 2021 -0400
>
>         fix(ci): triggering the right rackn-saas branch
>
>     M   .gitlab-ci.yml
>
>     commit 3d1f6570e7f62b68bf69ec52fbeb7b11ec41bc1a
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Thu Sep 2 16:18:16 2021 -0400
>
>         ci: gitlab ci
>
>     A   .gitlab-ci.yml
>
>     commit 704dc79ae83ebb8ea79dd77f60739693c7af199f
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Thu Sep 2 14:01:12 2021 -0500
>
>         feat(gitlab): go.mod replace and CLI build fixups for gitlab
>
>     M   go.mod
>     M   go.sum
>     M   tools/build.sh
>
>     End of Note

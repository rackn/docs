v4.3.3 {#rs_rel_notes_rackn-plugins_v4.3.3}
======

>     commit a4773495cf9cb3c0924746d7a1248988607517f7
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Aug 10 09:36:48 2020 -0500
>
>         fix(rack): get the required parameters from the ma.Params not the model
>
>     M   cmds/rack/rack-actions.go
>     M   cmds/rack/tests/rack_test.sh
>     M   cmds/rack/tests/test-data/Cli0550/stdout.expect
>     M   cmds/rack/tests/test-data/Cli0560/stdout.expect
>     M   cmds/rack/tests/test-data/Cli0590/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0601/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0601/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0602/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0602/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0603/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0603/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0604/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0604/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0605/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0605/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0606/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0606/stdout.expect
>     A   cmds/rack/tests/test-data/Cli0607/stderr.expect
>     A   cmds/rack/tests/test-data/Cli0607/stdout.expect
>     M   cmds/rack/tests/test-data/Cli0810/stdout.expect
>
>     commit ce10f150b12b1465e41fb15d9586c9c838f3275d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Aug 8 01:32:15 2020 -0500
>
>         feat(rack): add rack/no-overrides so discoverRack can keep existing values
>
>     M   cmds/rack/tests/test-data/Cli0810/stdout.expect
>
>     commit 5d700b0dcba326aa331036b46c0f3e43cef9eafb
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 16 13:56:49 2020 -0500
>
>         build: add build rel_notes
>
>     M   .gitignore
>
>     End of Note

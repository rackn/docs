4e64cecc1ecd96cf71126a8960c1b

:   Author: Zander Franks \<<zander@zanderf.net>\> Date: Fri Jun 24
    12:45:04 2022 -0500

    > feat(labs): post-meeting styling changes

    M app/views/Labs/components/LabViewer.tsx M
    app/views/Labs/components/Labels.tsx M app/views/Labs/flow/node.tsx
    M app/views/Labs/index.tsx M app/views/Labs/types.ts

    commit 33cf36eb534fcb3fbbbf8146ba367285be83efd8 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 23 16:33:59 2022 -0500

    > fix(labs): use redux to store lab completion instead of context

    M app/rootReducer.ts M app/views/Labs/components/LabViewer.tsx A
    app/views/Labs/components/MarkButton.tsx M
    app/views/Labs/components/StepModal.tsx D app/views/Labs/context.tsx
    M app/views/Labs/flow/node.tsx M app/views/Labs/index.tsx A
    app/views/Labs/reducer.ts A app/views/Labs/selectors.ts

    commit bf4bdd1a069b23f94698efe4780ff3ad4d72db99 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 23 15:23:37 2022 -0500

    > feat(catalog): preliminary new getCatalog func for new catalog
    > urls

    M app/api/index.tsx M app/api/knownParams.ts M
    app/views/Endpoint/features/info/selectors.ts

    commit d8c52f984c3789c85bbf122b7141e6b1f165c8e8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 23 14:11:16 2022
    -0500

    > feat(quacamole): add machine option to query string

    M app/features/guacamole/types.ts

    commit 4cb29d0f26781fd5bd017291c30ba226431fb2d3 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 23 12:52:42 2022 -0500

    > fix(labs): small styling changes

    M app/views/Labs/components/LabOutlet.tsx M
    app/views/Labs/flow/node.tsx

    commit b7af718dd256604803a5d51c5671576a00f6697d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 23 11:48:14 2022 -0500

    > feat(labs): mark labs as complete

    M app/views/Labs/components/LabViewer.tsx A
    app/views/Labs/context.tsx M app/views/Labs/flow/node.tsx M
    app/views/Labs/index.tsx

    commit fbcc4b1c32fca9c29a6e553eb38f94345e2bcc1f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 23 11:29:01 2022
    -0500

    > feat(guacamole): add types for each protocol options, remove zoom
    > css

    M app/css/style.css M app/features/guacamole/types.ts

    commit 98add2cf2fb3b947b2d4d1de131a22cab121e577 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 15:47:38 2022
    -0500

    > refactor(guacamole): convert http tunnel from xhr to fetchapi

    M app/features/guacamole/util/tunnel.ts

    commit 2282a7458ac23636e4e1ad11db348b0aab37477d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 22 14:30:57 2022 -0500

    > fix(labs): circular reference, modal bleeding offscreen

    M app/views/Endpoint/features/modal/components/Modal.tsx A
    app/views/Labs/components/Details.tsx M
    app/views/Labs/components/LabViewer.tsx M
    app/views/Labs/components/StepModal.tsx

    commit e8248d26a03d189969e18d8afa21cd946b00c031 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 22 13:37:38 2022 -0500

    > feat(labs): explanation in labs flow

    M app/views/Labs/flow/node.tsx M app/views/Labs/index.tsx

    commit 9748493e11a5a3d45e537e99dcfe737e2abada49 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 22 13:25:42 2022 -0500

    > feat(labs): buttons in modal header

    M app/views/Endpoint/features/modal/components/Modal.tsx M
    app/views/Endpoint/features/modal/index.tsx M
    app/views/Labs/components/LabViewer.tsx M
    app/views/Labs/components/StepModal.tsx

    commit ece402e5151d7f1bcd258aee1a44546bebd82814 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 11:59:59 2022
    -0500

    > fix(guacamole): fix http tunnel

    M app/features/guacamole/util/tunnel.ts

    commit 3771e7316828f1cb940a8b9d1c5edc1d17de1c3c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 22 11:47:46 2022 -0500

    > fix(labs): styling change to modal

    M app/views/Labs/components/StepModal.tsx

    commit f44a0e342f3629a5ac672819d32c5e7f6b462eff Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 11:15:25 2022
    -0500

    > fix(guacamole): bind callback on xmlhttprequest, minor tunnel
    > refactor

    M app/features/guacamole/util/tunnel.ts

    commit 42c7f66aa691de6c8d12379867c36b0c315b746e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 11:00:37 2022
    -0500

    > fix(guacamole): event handler memory leak fixes, changes on http
    > tunnel

    M app/features/guacamole/Client.tsx M
    app/features/guacamole/util/clipboard.ts M
    app/features/guacamole/util/connect.ts M
    app/features/guacamole/util/mouse.ts M
    app/features/guacamole/util/tunnel.ts

    commit f3b66ccffcb0087c073fce5eb3eec635876a92eb Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 09:49:46 2022
    -0500

    > fix(guacamole): fix js errors from merge

    M app/features/guacamole/Client.tsx M
    app/features/guacamole/types.ts

    commit b54591d60ff822a85bcd77b4be29280e0058319e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 22 09:45:30 2022
    -0500

    > feat(guacamole): add auth token support

    M app/features/guacamole/Client.tsx M
    app/features/guacamole/types.ts

    commit 6e3009c65d1ace9e846b56d998273a6bd913e198 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Wed Jun 22 09:45:19 2022 -0500

    > feat(guac): convert to pass token and security changes..

    M app/features/guacamole/Client.tsx M
    app/features/guacamole/types.ts M
    app/features/guacamole/util/connect.ts

    commit bf998509516a4579519f724f90496db9b4049c3b Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Tue Jun 21 18:52:02 2022 -0500

    > fix(guac): change defaults to no http tunnel and fix wss

    M app/features/guacamole/Client.tsx M
    app/features/guacamole/util/connect.ts

    commit fa5e8474ce32b4356f2023feb69ada4b757530e5 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 21 16:53:32 2022
    -0500

    > feat(guacamole): WIP client

    A app/features/guacamole/Client.tsx A
    app/features/guacamole/types.ts A
    app/features/guacamole/util/clipboard.ts A
    app/features/guacamole/util/connect.ts A
    app/features/guacamole/util/mouse.ts A
    app/features/guacamole/util/tunnel.ts M
    app/views/Endpoint/routes.tsx M package-lock.json M package.json M
    webpack.config.js

    commit 14a6653c3fd5f5b945e7b89d7a4755c2e6fc777c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 21 15:53:53 2022 -0500

    > feat(labs): steps modal prototype

    M app/views/Endpoint/features/modal/components/Modal.tsx M
    app/views/Endpoint/features/modal/index.tsx M
    app/views/Labs/components/LabViewer.tsx A
    app/views/Labs/components/StepModal.tsx M app/views/Labs/index.tsx

    commit 04fbd24d4994482864589eabc67139f4533f083b Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 21 13:53:13 2022 -0500

    > fix(labs): summary sections

    M app/views/Labs/components/LabViewer.tsx M app/views/Labs/index.tsx

    commit bdafa1b2261050dbf506a1ff21e9139d103a4972 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 21 13:50:20 2022 -0500

    > feat(labs): styling changes, lab jsonschema

    M .vscode/extensions.json M app/views/Labs/components/LabViewer.tsx
    M app/views/Labs/index.tsx A app/views/Labs/schema.json M
    app/views/Labs/types.ts

    commit 1277e7f5bfd2e88a3acd9ee2e9f60666099de07f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 21 11:00:26 2022 -0500

    > fix(modals): remove test button

    M app/views/Endpoint/views/System/panes/Preferences.tsx

    commit 8f61a495452ff7996d603793854a41becff0a59c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Jun 20 15:54:09 2022 -0500

    > fix(modals): constrain modal position on initial render

    M app/views/Endpoint/features/modal/components/Modal.tsx

    commit 82ce2d9af436de4c12959cd5f828d8f03e74ccf4 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Jun 20 15:45:40 2022 -0500

    > feat(modals): bounds checking, closing, props passing

    M app/api/index.tsx M
    app/views/Endpoint/features/modal/components/Modal.tsx M
    app/views/Endpoint/features/modal/index.tsx A
    app/views/Endpoint/features/modal/modals/message.tsx M
    app/views/Endpoint/views/System/panes/Preferences.tsx

    commit ba2c74c4e1ee6b15f6d829a53bb2b4ccffbfbc35 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Jun 20 14:00:12 2022 -0500

    > feat(modals): initial prototype

    M app/index.html A
    app/views/Endpoint/features/modal/components/Modal.tsx A
    app/views/Endpoint/features/modal/index.tsx M
    app/views/Endpoint/index.tsx M
    app/views/Endpoint/views/System/panes/Preferences.tsx

    commit 78f835942fcc18c50d4242d25d3f88b415eb39ce Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Jun 20 14:43:42 2022
    -0500

    > fix(tasks): remove default Path on task create editor

    M app/views/Endpoint/views/Stages/components/TemplatesEditor.jsx

    commit 7f5fddc1bee11bc0efaad14c8fef37d30164415a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Jun 20 14:40:02 2022
    -0500

    > fix(system): add loading chart to OS pie

    M app/layout/Resizer.tsx M
    app/views/Endpoint/views/System/components/BundlePie.tsx M
    app/views/Endpoint/views/System/components/MachineTypePie.tsx M
    app/views/Endpoint/views/System/components/OSPie.tsx

    commit 5561f1ed8f7543a1116c64270a5c4be37a048716 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Jun 20 12:22:04 2022 -0500

    > fix(labs): nudge flow viewer over by default

    M app/views/Labs/index.tsx

    commit 8a5d7120255b2070e583f700da76a19822999e67 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Sun Jun 19 12:35:08 2022
    -0500

    > fix(templates): fix template create route

    M app/views/Endpoint/views/Templates/routes.tsx

    commit 0f5a4cf36de5d8a82f8b3432ea52260091f17951 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 16 16:06:27 2022
    -0500

    > fix(system): fix os pie chart slice counts

    M app/views/Endpoint/views/System/components/OSPie.tsx

    commit 860143b470dfcd9e9fd8b47abe0042c838eb42b9 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 16 15:30:05 2022
    -0500

    > feat(system): add os pie chart to info & prefs page

    M app/features/statistics/components/PieChart.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/System/components/BundlePie.tsx M
    app/views/Endpoint/views/System/components/MachineTypePie.tsx A
    app/views/Endpoint/views/System/components/OSPie.tsx M
    app/views/Endpoint/views/System/panes/InstalledContent.tsx

    commit c3e246a1c2eecfe5b3cbe965824ade566afedc6f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 16 14:51:10 2022 -0500

    > feat(labs): clean up react flow usage

    M app/views/Labs/index.tsx

    commit 287ffa5594ea7a33adf021c07655e48e83aa4948 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 16 14:45:47 2022 -0500

    > feat(labs): markdown for various view fields

    M app/layout/Markdown.jsx M app/views/Labs/components/LabViewer.tsx
    M app/views/Labs/index.tsx

    commit 47305fd8811047bf17900a237ccf758ef8f60c0b Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 16 13:42:31 2022 -0500

    > feat(labs): lab viewer prototype

    A app/layout/UnstyledLink.tsx M app/routes.tsx A
    app/views/Labs/components/LabOutlet.tsx A
    app/views/Labs/components/LabViewer.tsx A
    app/views/Labs/components/Labels.tsx A app/views/Labs/flow/node.tsx
    M app/views/Labs/index.tsx M app/views/Labs/types.ts

    commit 7fe09049ce9c34e66d0214ff580b3cd1686ab7d3 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 15 12:08:34 2022 -0500

    > fix(work\_orders): add meta tab to editor

    M app/views/Endpoint/views/WorkOrders/WorkOrderEditor.tsx

    commit cc6f0a7d907066615ad3bd3c961d8916dc977d7f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 15 12:05:22 2022 -0500

    > feat(labs): prototype schema

    M app/monaco/FormattedElem.tsx M app/views/Labs/index.tsx A
    app/views/Labs/types.ts

    commit 9e314b128784d8e205a07c52087767b6f9c1230b Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 15 11:56:36 2022
    -0500

    > feat(legal): add open source attribution

    M app/config/routes.ts M app/routes.tsx M
    app/views/Endpoint/views/Registration/panes/Trial.jsx M
    app/views/Endpoint/widgets/NavFooterLicense.jsx M
    app/views/Home/components/FooterLinks.jsx A
    app/views/Legal/Attribution.tsx R099 app/layout/Terms.jsx
    app/views/Legal/Terms.jsx M package-lock.json M package.json M
    webpack.config.js

    commit 811851ac1d436b591caa3ed87319363ae6abf480 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Tue Jun 14 16:29:10 2022 -0500

    > doc(system): change wording on warning message, avoid expensive

    M app/features/statistics/components/PerformanceWarning.tsx

    commit 8df379e24f943ac41b8cf2cccf4a728467b0a7af Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 14 14:50:16 2022
    -0500

    > fix(overview): switch value and percent order in overview pie
    > chart stats

    M app/views/Endpoint/views/Overview/components/Pie.tsx

    commit f431c00672ef7799bb324caf42e51ac422556f4d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 14 11:44:03 2022
    -0500

    > fix(triggers): add enabled switch to create editor

    M app/views/Endpoint/views/WorkOrders/TriggerCreateEditor.jsx

    commit cf1797c1ca1541fd14ff49590e23c2e4fc6b3a94 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 14 11:12:34 2022 -0500

    > chore: merge in master, use react router 6

    M app/routes.tsx M app/views/Labs/index.tsx

    commit 4cb0063b3f37021d0fe0fa552a1464fa7f1b40e3 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 14 10:39:31 2022
    -0500

    > style(triggers): add type to callback

    M app/views/Endpoint/widgets/TriggerEventButtons.tsx

    commit 49a82f696b680c5c35aabc358b8fe531004b341f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 14 10:36:01 2022
    -0500

    > feat(triggers): add responsivity to quick trigger menu items

    M app/views/Endpoint/widgets/TriggerEventButtons.tsx

    commit 337d1f8f0aae43ca9d335f0bea23c5a8ccf8b0da Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 14 09:46:39 2022
    -0500

    > fix(alerts): fix typo in Acknowledged field

    M app/views/Endpoint/views/Alerts/Editor.tsx

    commit c14aeb798b4185c5e3b6a81090436687c32a569c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Jun 13 14:09:21 2022 -0500

    > feat(triggers): add trigger buttons to nav bar with matching
    > event-trigger param

    M app/layout/Wrapper.jsx M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    A app/views/Endpoint/widgets/TriggerEventButtons.tsx

    commit 1adee110defcee633d9f92acb6672434e8445727 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Jun 10 15:46:42 2022
    -0500

    > fix(wo): useCallback on createSelectors

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx M
    app/views/Endpoint/views/WorkOrders/components/ROTriggerEnabled.tsx

    commit 339bebfcbf43199e25385cb9f0e3de90f7ff52de Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri Jun 10 13:50:26 2022 -0500

    > fix(triggers): proper enabling/disabling with global triggers
    > enabled/disabled

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx
    M app/views/Endpoint/features/info/selectors.ts M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx M
    app/views/Endpoint/views/WorkOrders/components/ROTriggerEnabled.tsx

    commit eb4c22e6e99708ee3046e7e59eafc7995dcb18cb Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri Jun 10 12:00:07 2022 -0500

    > fix(triggers): trigger enabled condition

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx
    M app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx A
    app/views/Endpoint/views/WorkOrders/components/ROTriggerEnabled.tsx

    commit e34bef52f9492710b8fb185558d7d572cf9ed325 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri Jun 10 11:47:29 2022 -0500

    > fix(triggers): enable and disable read-only triggers

    M app/api/knownParams.ts M app/api/socket.ts M
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx
    M app/views/Endpoint/features/info/reducer.ts M
    app/views/Endpoint/features/info/selectors.ts M
    app/views/Endpoint/features/license/actions.ts M
    app/views/Endpoint/index.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx

    commit 41049efb7e21b49e6cf1fc0b12b8a01be8b6592f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri Jun 10 11:06:44 2022 -0500

    > fix(triggers): use raw filter

    M app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx

    commit b4677126e5f6bd577f74e8b5646d75d279c0f6d5 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri Jun 10 10:52:15 2022 -0500

    > fix(triggers): multiple triggers to the same blueprint in
    > visualizer

    M app/views/Endpoint/features/flow/components/Visualizer.tsx

    commit 17c8593958cc86c5ea9876acfb4c4a7b99ce0f9c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 9 15:57:12 2022 -0500

    > feat(system): checkmark feedback when pref is saved

    M app/views/Endpoint/views/System/panes/Preferences.tsx

    commit fc6ae2973ddac219e883bb92ea91c0501b4e9d27 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 9 15:35:03 2022 -0500

    > feat(system): refactor/redesign preferences pane

    M app/layout/Grid.tsx D
    app/views/Endpoint/views/System/panes/Preferences.jsx A
    app/views/Endpoint/views/System/panes/Preferences.tsx

    commit 390e2618e22809d93196f66d5f9bd91a151739a2 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 9 14:35:44 2022
    -0500

    > chore: remove old and completed TODOs

    M app/views/Endpoint/components/Components.jsx M
    app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/components/TableHead.tsx M
    app/views/Endpoint/views/Contents/index.jsx M
    app/views/Endpoint/views/Params/components/ParamRow.jsx

    commit 39d1d1a79523e9ce7594c83239c6b45a19dffdde Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 9 11:21:56 2022
    -0500

    > refactor(table): remove unnecessary memos from column formats

    M app/api/DigitalRebar.ts M app/api/knownParams.ts M
    app/api/offbrandjQuery.ts M
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon\_id.tsx
    M app/views/Endpoint/features/table\_view/columns/gen\_link\_id.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_object.tsx
    M
    app/views/Endpoint/features/table\_view/columns/icon\_available.tsx
    M app/views/Endpoint/features/table\_view/columns/icon\_endpoint.tsx
    M app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx M
    app/views/Endpoint/features/table\_view/columns/icon\_readonly.tsx M
    app/views/Endpoint/features/table\_view/columns/util\_selection.tsx
    M app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/util.tsx M
    app/views/Endpoint/views/Alerts/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx R054
    app/views/Endpoint/views/BootEnvs/columns/icon\_iso\_url.jsx
    app/views/Endpoint/views/BootEnvs/columns/icon\_iso\_url.tsx M
    app/views/Endpoint/views/BootEnvs/columns/text\_iso\_file.jsx M
    app/views/Endpoint/views/Contexts/columns/icon\_install\_context.jsx
    D app/views/Endpoint/views/Endpoints/columns/enum\_actions.jsx A
    app/views/Endpoint/views/Endpoints/columns/enum\_actions.tsx D
    app/views/Endpoint/views/Endpoints/columns/icon\_apply.jsx A
    app/views/Endpoint/views/Endpoints/columns/icon\_apply.tsx D
    app/views/Endpoint/views/Endpoints/columns/icon\_connectionStatus.jsx
    A
    app/views/Endpoint/views/Endpoints/columns/icon\_connectionStatus.tsx
    D app/views/Endpoint/views/Endpoints/columns/link\_drpId.jsx A
    app/views/Endpoint/views/Endpoints/columns/link\_drpId.tsx R066
    app/views/Endpoint/views/Endpoints/columns/link\_portal.jsx
    app/views/Endpoint/views/Endpoints/columns/link\_portal.tsx D
    app/views/Endpoint/views/IdentityProviders/columns/link\_idpId.jsx A
    app/views/Endpoint/views/IdentityProviders/columns/link\_idpId.tsx M
    app/views/Endpoint/views/Jobs/columns/icon\_resource.tsx R066
    app/views/Endpoint/views/Jobs/columns/icon\_state.jsx
    app/views/Endpoint/views/Jobs/columns/icon\_state.tsx R050
    app/views/Endpoint/views/Jobs/columns/icon\_work\_order.jsx
    app/views/Endpoint/views/Jobs/columns/icon\_work\_order.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_machine.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_task.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_uuid.tsx M
    app/views/Endpoint/views/Jobs/columns/text\_runTime.tsx R056
    app/views/Endpoint/views/Leases/columns/icon\_expired.jsx
    app/views/Endpoint/views/Leases/columns/icon\_expired.tsx R054
    app/views/Endpoint/views/Leases/columns/text\_token.jsx
    app/views/Endpoint/views/Leases/columns/text\_token.tsx R054
    app/views/Endpoint/views/Machines/columns/enum\_profiles.jsx
    app/views/Endpoint/views/Machines/columns/enum\_profiles.tsx M
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Machines/columns/icon\_context.tsx R050
    app/views/Endpoint/views/Machines/columns/icon\_locked.jsx
    app/views/Endpoint/views/Machines/columns/icon\_locked.tsx M
    app/views/Endpoint/views/Machines/columns/icon\_pool.tsx D
    app/views/Endpoint/views/Machines/columns/icon\_runnable.jsx A
    app/views/Endpoint/views/Machines/columns/icon\_runnable.tsx D
    app/views/Endpoint/views/Machines/columns/icon\_womode.jsx A
    app/views/Endpoint/views/Machines/columns/icon\_womode.tsx M
    app/views/Endpoint/views/Machines/columns/link\_address.tsx R058
    app/views/Endpoint/views/Machines/columns/link\_icon\_pipeline.jsx
    app/views/Endpoint/views/Machines/columns/link\_icon\_pipeline.tsx
    R090 app/views/Endpoint/views/Machines/columns/link\_icon\_tasks.jsx
    app/views/Endpoint/views/Machines/columns/link\_icon\_tasks.tsx M
    app/views/Endpoint/views/Machines/columns/link\_workflow.tsx R050
    app/views/Endpoint/views/Params/columns/icon\_store.jsx
    app/views/Endpoint/views/Params/columns/icon\_store.tsx D
    app/views/Endpoint/views/Params/columns/text\_default.jsx A
    app/views/Endpoint/views/Params/columns/text\_default.tsx R054
    app/views/Endpoint/views/Pipelines/columns/link\_pipeline\_name.jsx
    app/views/Endpoint/views/Pipelines/columns/link\_pipeline\_name.tsx
    D app/views/Endpoint/views/Pools/columns/gen\_pool\_actions.jsx A
    app/views/Endpoint/views/Pools/columns/gen\_pool\_actions.tsx R052
    app/views/Endpoint/views/Raid/columns/enum\_drives.jsx
    app/views/Endpoint/views/Raid/columns/enum\_drives.tsx R058
    app/views/Endpoint/views/Subnets/columns/link\_toggle\_name.jsx
    app/views/Endpoint/views/Subnets/columns/link\_toggle\_name.tsx M
    app/views/Endpoint/views/Templates/columns/text\_content.jsx M
    app/views/Endpoint/views/Tenants/columns/enum\_members\_icons.jsx M
    app/views/Endpoint/views/Tenants/columns/enum\_users\_icons.jsx M
    app/views/Endpoint/views/UXViews/columns/enum\_applicable\_roles.jsx
    R066 app/views/Endpoint/views/UXViews/columns/icon\_apply.jsx
    app/views/Endpoint/views/UXViews/columns/icon\_apply.tsx R053
    app/views/Endpoint/views/UXViews/columns/link\_id.jsx
    app/views/Endpoint/views/UXViews/columns/link\_id.tsx R051
    app/views/Endpoint/views/VersionSets/columns/icon\_apply.jsx
    app/views/Endpoint/views/VersionSets/columns/icon\_apply.tsx R058
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_uuid.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_wo\_task.tsx D
    app/views/Endpoint/views/WorkOrders/columns/text\_cron.jsx R089
    app/views/Endpoint/views/Workflows/columns/enum\_icon\_stages.jsx
    app/views/Endpoint/views/Workflows/columns/enum\_icon\_stages.tsx

    commit eaff640bac1ce3dbae50a816ef110eeb04a8b1d0 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 8 16:00:35 2022 -0500

    > perf: fixes based on whyDidYouRender

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx

    commit 8568ef5d42e02a4e479c653b99f1754fd33d8c41 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 8 15:51:17 2022 -0500

    > fix(redux): removed unnecessary console log

    M app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/views/Machines/MachinesView.tsx

    commit 50142b97fdeaf3c17aafb202793f2f89044bf612 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 8 15:42:35 2022 -0500

    > fix(redux): selector cache limits

    M app/views/Endpoint/features/dependencies/selectors.ts M
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/selectors.ts M
    app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/reducer.ts M
    app/views/Endpoint/views/Contexts/columns/icon\_install\_context.jsx
    M app/views/Endpoint/views/Contexts/components/ImageField.jsx M
    app/views/Endpoint/views/Endpoints/columns/link\_portal.jsx M
    app/views/Endpoint/views/Jobs/columns/icon\_resource.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_machine.tsx M
    app/views/Endpoint/views/Leases/Editor.tsx M
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Machines/columns/link\_icon\_pipeline.jsx M
    app/views/Endpoint/views/Machines/components/ActivityIcon.tsx M
    app/views/Endpoint/views/Machines/tabs/BrokerMachinesTab.tsx M
    app/views/Endpoint/views/Machines/tabs/ClusterMachinesTab.tsx M
    app/views/Endpoint/views/Machines/tabs/DebuggerTab.tsx M
    app/views/Endpoint/views/Params/columns/icon\_store.jsx M
    app/views/Endpoint/views/Params/columns/text\_default.jsx M
    app/views/Endpoint/views/Raid/columns/enum\_drives.jsx M
    app/views/Endpoint/views/Workflows/tabs/JobsTab.tsx

    commit 5c8e54898ba44380796837b6317f27e3e0db7430 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 8 14:39:14 2022 -0500

    > refactor(tables): refactor remaining columns to use selectors

    M app/config/routes.ts M app/layout/SearchContainer.jsx M
    app/views/Endpoint/features/editor/selectors.ts M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/views/Alerts/View.tsx M
    app/views/Endpoint/views/Alerts/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_acknowledgeUser.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_principal.tsx M
    app/views/Endpoint/views/BootEnvs/columns/icon\_iso\_url.jsx M
    app/views/Endpoint/views/BootEnvs/columns/text\_iso\_file.jsx M
    app/views/Endpoint/views/Contexts/columns/icon\_install\_context.jsx
    M app/views/Endpoint/views/Endpoints/View.jsx M
    app/views/Endpoint/views/Endpoints/columns/enum\_actions.jsx M
    app/views/Endpoint/views/Endpoints/columns/icon\_apply.jsx M
    app/views/Endpoint/views/Endpoints/columns/icon\_connectionStatus.jsx
    M app/views/Endpoint/views/Endpoints/columns/link\_drpId.jsx M
    app/views/Endpoint/views/Endpoints/columns/link\_portal.jsx M
    app/views/Endpoint/views/IdentityProviders/columns/link\_idpId.jsx M
    app/views/Endpoint/views/Jobs/View.jsx M
    app/views/Endpoint/views/Jobs/columns/icon\_resource.tsx M
    app/views/Endpoint/views/Jobs/columns/icon\_state.jsx M
    app/views/Endpoint/views/Jobs/columns/icon\_work\_order.jsx M
    app/views/Endpoint/views/Jobs/columns/link\_machine.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_task.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_uuid.tsx M
    app/views/Endpoint/views/Jobs/columns/text\_runTime.tsx M
    app/views/Endpoint/views/Leases/columns/icon\_expired.jsx M
    app/views/Endpoint/views/Leases/columns/text\_token.jsx M
    app/views/Endpoint/views/Params/columns/icon\_store.jsx M
    app/views/Endpoint/views/Params/columns/text\_default.jsx M
    app/views/Endpoint/views/Pipelines/columns/link\_pipeline\_name.jsx
    M app/views/Endpoint/views/Pools/columns/gen\_pool\_actions.jsx M
    app/views/Endpoint/views/Raid/columns/enum\_drives.jsx M
    app/views/Endpoint/views/Roles/View.jsx M
    app/views/Endpoint/views/Subnets/columns/link\_toggle\_name.jsx M
    app/views/Endpoint/views/Templates/columns/text\_content.jsx M
    app/views/Endpoint/views/Tenants/columns/enum\_members\_icons.jsx M
    app/views/Endpoint/views/Tenants/columns/enum\_users\_icons.jsx M
    app/views/Endpoint/views/UXViews/columns/enum\_applicable\_roles.jsx
    M app/views/Endpoint/views/UXViews/columns/icon\_apply.jsx M
    app/views/Endpoint/views/UXViews/columns/link\_id.jsx M
    app/views/Endpoint/views/VersionSets/View.jsx M
    app/views/Endpoint/views/VersionSets/columns/icon\_apply.jsx M
    app/views/Endpoint/views/WorkOrders/WorkOrdersView.jsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_toggle\_name.jsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_uuid.tsx M
    app/views/Endpoint/views/WorkOrders/columns/link\_wo\_task.tsx M
    app/views/Endpoint/views/WorkOrders/columns/text\_cron.jsx M
    app/views/Endpoint/views/Workflows/columns/enum\_icon\_stages.jsx M
    app/views/Endpoint/views/Workflows/components/NestedJobTable.tsx

    commit 297d51d574fff6f401fad670d33d6df18006c36d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 8 12:34:11 2022
    -0500

    > refactor(table): support for ux views partial columns and more
    > table columns

    M
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon\_id.tsx
    M app/views/Endpoint/features/table\_view/columns/gen\_link\_id.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_object.tsx
    M
    app/views/Endpoint/features/table\_view/columns/icon\_available.tsx
    M app/views/Endpoint/features/table\_view/columns/icon\_endpoint.tsx
    M app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx M
    app/views/Endpoint/features/table\_view/columns/icon\_readonly.tsx M
    app/views/Endpoint/features/table\_view/columns/util\_selection.tsx
    M app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/util.tsx M
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx

    commit 16a98483c9700260fd3c5eaa4ca5b8279778c5da Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 8 11:56:58 2022
    -0500

    > feat(table): allow for partial object selection in table cells,
    > update machine columns

    M app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/reducer.ts M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/columns/enum\_profiles.jsx M
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Machines/columns/icon\_context.tsx M
    app/views/Endpoint/views/Machines/columns/icon\_locked.jsx M
    app/views/Endpoint/views/Machines/columns/icon\_pool.tsx M
    app/views/Endpoint/views/Machines/columns/icon\_runnable.jsx M
    app/views/Endpoint/views/Machines/columns/icon\_womode.jsx M
    app/views/Endpoint/views/Machines/columns/link\_address.tsx M
    app/views/Endpoint/views/Machines/columns/link\_icon\_pipeline.jsx M
    app/views/Endpoint/views/Machines/columns/link\_icon\_tasks.jsx M
    app/views/Endpoint/views/Machines/columns/link\_workflow.tsx

    commit 94f921595cd12b3cfaa0992a73f90942cd256eee Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 8 10:33:59 2022
    -0500

    > lint: fix eslint errors

    M app/views/Endpoint/features/table\_view/reducer.ts M
    app/views/Endpoint/views/Templates/View.jsx

    commit a6a3f5ebc3e1b9fab8ad42ec54ab699f4096f5a8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 8 09:56:06 2022
    -0500

    > feat(editor): make debugger tab use partial object

    M app/views/Endpoint/views/Machines/tabs/DebuggerTab.tsx

    commit 28b2a1311676d951d1a65787a200b9bc3b262234 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Jun 7 17:08:39 2022 -0500

    > refactor(editors): editor refactor and create editor fixes

    M app/api/cloudia.tsx M
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/editor/selectors.ts M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    M app/views/Endpoint/features/editor/tabs/ParamEditor/index.tsx M
    app/views/Endpoint/features/editor/util.ts M
    app/views/Endpoint/features/flow/flow.ts M
    app/views/Endpoint/views/Alerts/Editor.tsx M
    app/views/Endpoint/views/Alerts/components/EditorFields.tsx M
    app/views/Endpoint/views/Alerts/components/EditorHeader.tsx M
    app/views/Endpoint/views/Alerts/routes.tsx M
    app/views/Endpoint/views/BootEnvs/CreateEditor.jsx M
    app/views/Endpoint/views/BootEnvs/Editor.tsx M
    app/views/Endpoint/views/BootEnvs/routes.tsx M
    app/views/Endpoint/views/Contexts/CreateEditor.jsx M
    app/views/Endpoint/views/Contexts/Editor.tsx M
    app/views/Endpoint/views/Contexts/components/ImageField.jsx M
    app/views/Endpoint/views/Contexts/routes.tsx M
    app/views/Endpoint/views/Endpoints/Editor.tsx M
    app/views/Endpoint/views/Endpoints/routes.tsx M
    app/views/Endpoint/views/IdentityProviders/CreateEditor.jsx M
    app/views/Endpoint/views/IdentityProviders/Editor.tsx M
    app/views/Endpoint/views/IdentityProviders/routes.tsx M
    app/views/Endpoint/views/Jobs/Editor.tsx M
    app/views/Endpoint/views/Jobs/tabs/JobLogTab.tsx M
    app/views/Endpoint/views/Leases/Editor.tsx M
    app/views/Endpoint/views/Machines/ClusterCreateEditor.tsx M
    app/views/Endpoint/views/Machines/ClusterEditor.tsx M
    app/views/Endpoint/views/Machines/MachineCreateEditor.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerCreateEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx M
    app/views/Endpoint/views/Machines/components/ActivityIcon.tsx M
    app/views/Endpoint/views/Machines/components/BlueprintRunnerField.tsx
    M app/views/Endpoint/views/Machines/components/EditorFields.tsx M
    app/views/Endpoint/views/Machines/components/MachineEditorHeader.tsx
    M
    app/views/Endpoint/views/Machines/components/RequiredParamFields.tsx
    M app/views/Endpoint/views/Machines/routes.tsx M
    app/views/Endpoint/views/Machines/tabs/BrokerMachinesTab.tsx M
    app/views/Endpoint/views/Machines/tabs/ClusterMachinesTab.tsx M
    app/views/Endpoint/views/Machines/tabs/DebuggerTab.tsx M
    app/views/Endpoint/views/Params/CreateEditor.jsx M
    app/views/Endpoint/views/Params/Editor.tsx M
    app/views/Endpoint/views/Params/routes.tsx M
    app/views/Endpoint/views/Pipelines/CreateEditor.jsx M
    app/views/Endpoint/views/Pipelines/Editor.tsx M
    app/views/Endpoint/views/Pipelines/components/WorkflowChain.jsx M
    app/views/Endpoint/views/Pipelines/routes.tsx M
    app/views/Endpoint/views/Plugins/CreateEditor.jsx M
    app/views/Endpoint/views/Plugins/Editor.tsx M
    app/views/Endpoint/views/Plugins/routes.tsx M
    app/views/Endpoint/views/Pools/CreateEditor.jsx M
    app/views/Endpoint/views/Pools/Editor.tsx M
    app/views/Endpoint/views/Pools/routes.tsx M
    app/views/Endpoint/views/Profiles/CreateEditor.jsx M
    app/views/Endpoint/views/Profiles/Editor.tsx M
    app/views/Endpoint/views/Profiles/routes.tsx M
    app/views/Endpoint/views/Racks/CreateEditor.jsx M
    app/views/Endpoint/views/Racks/Editor.tsx M
    app/views/Endpoint/views/Racks/routes.tsx M
    app/views/Endpoint/views/Raid/CreateEditor.jsx M
    app/views/Endpoint/views/Raid/Editor.tsx M
    app/views/Endpoint/views/Raid/components/VolumesField.jsx M
    app/views/Endpoint/views/Raid/routes.tsx M
    app/views/Endpoint/views/Reservations/CreateEditor.jsx M
    app/views/Endpoint/views/Reservations/Editor.tsx M
    app/views/Endpoint/views/Reservations/routes.tsx M
    app/views/Endpoint/views/Roles/CreateEditor.jsx M
    app/views/Endpoint/views/Roles/Editor.tsx M
    app/views/Endpoint/views/Roles/routes.tsx M
    app/views/Endpoint/views/Stages/CreateEditor.jsx M
    app/views/Endpoint/views/Stages/Editor.tsx M
    app/views/Endpoint/views/Stages/components/TemplatesEditor.jsx M
    app/views/Endpoint/views/Stages/routes.tsx M
    app/views/Endpoint/views/Subnets/CreateEditor.jsx M
    app/views/Endpoint/views/Subnets/Editor.tsx M
    app/views/Endpoint/views/Subnets/components/EditorFields.jsx M
    app/views/Endpoint/views/Subnets/routes.tsx M
    app/views/Endpoint/views/Tasks/CreateEditor.jsx M
    app/views/Endpoint/views/Tasks/Editor.tsx M
    app/views/Endpoint/views/Tasks/routes.tsx M
    app/views/Endpoint/views/Templates/CreateEditor.jsx M
    app/views/Endpoint/views/Templates/Editor.tsx M
    app/views/Endpoint/views/Tenants/CreateEditor.jsx M
    app/views/Endpoint/views/Tenants/Editor.tsx M
    app/views/Endpoint/views/Tenants/routes.tsx M
    app/views/Endpoint/views/UXViews/CreateEditor.jsx M
    app/views/Endpoint/views/UXViews/Editor.tsx M
    app/views/Endpoint/views/UXViews/routes.tsx M
    app/views/Endpoint/views/Users/CreateEditor.jsx M
    app/views/Endpoint/views/Users/Editor.tsx M
    app/views/Endpoint/views/Users/components/PasswordField.tsx M
    app/views/Endpoint/views/Users/routes.tsx M
    app/views/Endpoint/views/VersionSets/CreateEditor.jsx M
    app/views/Endpoint/views/VersionSets/Editor.tsx M
    app/views/Endpoint/views/VersionSets/routes.tsx M
    app/views/Endpoint/views/WorkOrders/BlueprintCreateEditor.jsx M
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.tsx M
    app/views/Endpoint/views/WorkOrders/TriggerCreateEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggerEditor.tsx M
    app/views/Endpoint/views/WorkOrders/TriggerProviderCreateEditor.jsx
    M app/views/Endpoint/views/WorkOrders/TriggerProviderEditor.tsx M
    app/views/Endpoint/views/WorkOrders/WorkOrderEditor.tsx M
    app/views/Endpoint/views/WorkOrders/routes.tsx M
    app/views/Endpoint/views/WorkOrders/tabs/WorkOrderActivityTab.tsx M
    app/views/Endpoint/views/Workflows/CreateEditor.jsx M
    app/views/Endpoint/views/Workflows/Editor.tsx M
    app/views/Endpoint/views/Workflows/routes.tsx M
    app/views/Endpoint/views/Workflows/tabs/JobsTab.tsx

    commit a4e308607c0782d69551a53783d1e086e7acdeaa Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 7 12:21:02 2022
    -0500

    > feat(editor): add deprecated flag to EditorContext.object

    M app/views/Endpoint/features/editor/context.tsx

    commit ec1f13e496564f59f9c2257c2810377a1db36f88 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Jun 7 12:19:08 2022
    -0500

    > refactor(editor): migrate some useEditorContext to
    > useEditorSelector, other tabs changes

    M .prettierrc M app/routes.tsx M
    app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/components/nameCollision.tsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/editor/hooks.ts A
    app/views/Endpoint/features/editor/selectors.ts R080
    app/views/Endpoint/features/editor/tabs/NestedEditor/index.jsx
    app/views/Endpoint/features/editor/tabs/NestedEditor/index.tsx M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    M app/views/Endpoint/views/Alerts/CreateEditor.tsx M
    app/views/Endpoint/views/Alerts/Editor.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/routes.tsx M
    app/views/Endpoint/views/Pipelines/routes.tsx M
    app/views/Endpoint/views/Plugins/routes.tsx M
    app/views/Endpoint/views/Profiles/routes.tsx M
    app/views/Endpoint/views/Stages/routes.tsx M
    app/views/Endpoint/views/WorkOrders/routes.tsx

    commit 8cd3052719c34f770a6459f9edf0c69fe95446b7 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Jun 6 15:42:36 2022
    -0500

    > fix: remove debug logs

    M app/api/analytics.tsx M
    app/views/Endpoint/features/table\_view/filter.ts

    commit be72415bb215bb9115157cf4bd20e71ce3e4434a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Jun 6 15:39:13 2022
    -0500

    > refactor(editor): convert editor tab links to relative

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/routes.tsx M
    app/views/Endpoint/views/Alerts/Editor.tsx M
    app/views/Endpoint/views/BootEnvs/Editor.tsx M
    app/views/Endpoint/views/Contexts/Editor.tsx M
    app/views/Endpoint/views/Endpoints/Editor.tsx M
    app/views/Endpoint/views/IdentityProviders/Editor.tsx M
    app/views/Endpoint/views/Jobs/Editor.tsx M
    app/views/Endpoint/views/Leases/Editor.tsx M
    app/views/Endpoint/views/Machines/ClusterEditor.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx M
    app/views/Endpoint/views/Params/Editor.tsx M
    app/views/Endpoint/views/Params/routes.tsx M
    app/views/Endpoint/views/Pipelines/Editor.tsx M
    app/views/Endpoint/views/Pipelines/routes.tsx M
    app/views/Endpoint/views/Plugins/Editor.tsx M
    app/views/Endpoint/views/Pools/Editor.tsx M
    app/views/Endpoint/views/Profiles/Editor.tsx M
    app/views/Endpoint/views/Racks/Editor.tsx M
    app/views/Endpoint/views/Raid/Editor.tsx M
    app/views/Endpoint/views/Reservations/Editor.tsx M
    app/views/Endpoint/views/Roles/Editor.tsx M
    app/views/Endpoint/views/Stages/Editor.tsx M
    app/views/Endpoint/views/Subnets/Editor.tsx M
    app/views/Endpoint/views/Tasks/Editor.tsx M
    app/views/Endpoint/views/Tenants/Editor.tsx M
    app/views/Endpoint/views/UXViews/Editor.tsx M
    app/views/Endpoint/views/UXViews/columns/link\_id.jsx M
    app/views/Endpoint/views/Users/Editor.tsx M
    app/views/Endpoint/views/VersionSets/Editor.tsx M
    app/views/Endpoint/views/VersionSets/routes.tsx M
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.tsx M
    app/views/Endpoint/views/WorkOrders/TriggerEditor.tsx M
    app/views/Endpoint/views/WorkOrders/TriggerProviderEditor.tsx M
    app/views/Endpoint/views/WorkOrders/WorkOrderEditor.tsx M
    app/views/Endpoint/views/Workflows/Editor.tsx

    commit 7912bc710369c80775d248716a499ff59475884c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Jun 6 14:50:39 2022
    -0500

    > refactor: migrate remaining views to react-router v6

    M app/api/DigitalRebar.ts M app/api/analytics.tsx M
    app/layout/SideNav/SideNav.tsx M app/routes.tsx M
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/table\_view/components/TableView.tsx M
    app/views/Endpoint/routes.tsx M
    app/views/Endpoint/views/Catalog/Catalog.jsx M
    app/views/Endpoint/views/Catalog/index.jsx M
    app/views/Endpoint/views/Machines/routes.tsx M
    app/views/Endpoint/views/Machines/tabs/ProfileTab.tsx R050
    app/views/Endpoint/views/Params/Editor.jsx
    app/views/Endpoint/views/Params/Editor.tsx M
    app/views/Endpoint/views/Params/View.jsx A
    app/views/Endpoint/views/Params/routes.tsx A
    app/views/Endpoint/views/Params/tabs/MetaTab.tsx R066
    app/views/Endpoint/views/Pipelines/Editor.jsx
    app/views/Endpoint/views/Pipelines/Editor.tsx M
    app/views/Endpoint/views/Pipelines/View.jsx M
    app/views/Endpoint/views/Pipelines/components/WorkflowChain.jsx A
    app/views/Endpoint/views/Pipelines/routes.tsx R065
    app/views/Endpoint/views/Plugins/Editor.jsx
    app/views/Endpoint/views/Plugins/Editor.tsx M
    app/views/Endpoint/views/Plugins/View.jsx A
    app/views/Endpoint/views/Plugins/routes.tsx R075
    app/views/Endpoint/views/Pools/Editor.jsx
    app/views/Endpoint/views/Pools/Editor.tsx M
    app/views/Endpoint/views/Pools/View.jsx A
    app/views/Endpoint/views/Pools/routes.tsx R059
    app/views/Endpoint/views/Profiles/Editor.jsx
    app/views/Endpoint/views/Profiles/Editor.tsx M
    app/views/Endpoint/views/Profiles/View.jsx A
    app/views/Endpoint/views/Profiles/routes.tsx R070
    app/views/Endpoint/views/Racks/Editor.jsx
    app/views/Endpoint/views/Racks/Editor.tsx M
    app/views/Endpoint/views/Racks/View.jsx A
    app/views/Endpoint/views/Racks/routes.tsx R063
    app/views/Endpoint/views/Raid/Editor.jsx
    app/views/Endpoint/views/Raid/Editor.tsx M
    app/views/Endpoint/views/Raid/View.jsx A
    app/views/Endpoint/views/Raid/routes.tsx R069
    app/views/Endpoint/views/Reservations/Editor.jsx
    app/views/Endpoint/views/Reservations/Editor.tsx M
    app/views/Endpoint/views/Reservations/View.jsx A
    app/views/Endpoint/views/Reservations/routes.tsx R062
    app/views/Endpoint/views/Roles/Editor.jsx
    app/views/Endpoint/views/Roles/Editor.tsx M
    app/views/Endpoint/views/Roles/View.jsx A
    app/views/Endpoint/views/Roles/routes.tsx R062
    app/views/Endpoint/views/Stages/Editor.jsx
    app/views/Endpoint/views/Stages/Editor.tsx M
    app/views/Endpoint/views/Stages/View.jsx A
    app/views/Endpoint/views/Stages/routes.tsx R074
    app/views/Endpoint/views/Subnets/Editor.jsx
    app/views/Endpoint/views/Subnets/Editor.tsx M
    app/views/Endpoint/views/Subnets/View.jsx A
    app/views/Endpoint/views/Subnets/routes.tsx R068
    app/views/Endpoint/views/Tasks/Editor.jsx
    app/views/Endpoint/views/Tasks/Editor.tsx M
    app/views/Endpoint/views/Tasks/View.jsx A
    app/views/Endpoint/views/Tasks/routes.tsx R096
    app/views/Endpoint/views/Templates/Editor.jsx
    app/views/Endpoint/views/Templates/Editor.tsx M
    app/views/Endpoint/views/Templates/View.jsx A
    app/views/Endpoint/views/Templates/routes.tsx R065
    app/views/Endpoint/views/Tenants/Editor.jsx
    app/views/Endpoint/views/Tenants/Editor.tsx M
    app/views/Endpoint/views/Tenants/View.jsx A
    app/views/Endpoint/views/Tenants/routes.tsx M
    app/views/Endpoint/views/UXConfig/View.jsx R076
    app/views/Endpoint/views/UXViews/Editor.jsx
    app/views/Endpoint/views/UXViews/Editor.tsx M
    app/views/Endpoint/views/UXViews/View.jsx A
    app/views/Endpoint/views/UXViews/routes.tsx R064
    app/views/Endpoint/views/Users/Editor.jsx
    app/views/Endpoint/views/Users/Editor.tsx M
    app/views/Endpoint/views/Users/View.jsx A
    app/views/Endpoint/views/Users/routes.tsx R076
    app/views/Endpoint/views/VersionSets/Editor.jsx
    app/views/Endpoint/views/VersionSets/Editor.tsx M
    app/views/Endpoint/views/VersionSets/View.jsx A
    app/views/Endpoint/views/VersionSets/routes.tsx R055
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.jsx
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.tsx M
    app/views/Endpoint/views/WorkOrders/BlueprintsView.jsx R100
    app/views/Endpoint/views/WorkOrders/TriggersCreateEditor.jsx
    app/views/Endpoint/views/WorkOrders/TriggerCreateEditor.jsx R069
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx
    app/views/Endpoint/views/WorkOrders/TriggerEditor.tsx R100
    app/views/Endpoint/views/WorkOrders/TriggerProvidersCreateEditor.jsx
    app/views/Endpoint/views/WorkOrders/TriggerProviderCreateEditor.jsx
    R063 app/views/Endpoint/views/WorkOrders/TriggerProvidersEditor.jsx
    app/views/Endpoint/views/WorkOrders/TriggerProviderEditor.tsx M
    app/views/Endpoint/views/WorkOrders/TriggerProvidersView.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersView.jsx R075
    app/views/Endpoint/views/WorkOrders/WorkOrdersEditor.jsx
    app/views/Endpoint/views/WorkOrders/WorkOrderEditor.tsx M
    app/views/Endpoint/views/WorkOrders/WorkOrdersView.jsx A
    app/views/Endpoint/views/WorkOrders/routes.tsx A
    app/views/Endpoint/views/WorkOrders/tabs/BlueprintApplyTab.tsx A
    app/views/Endpoint/views/WorkOrders/tabs/BlueprintWOTab.tsx A
    app/views/Endpoint/views/WorkOrders/tabs/WorkOrderActivityTab.tsx
    R059 app/views/Endpoint/views/Workflows/Editor.jsx
    app/views/Endpoint/views/Workflows/Editor.tsx M
    app/views/Endpoint/views/Workflows/View.jsx A
    app/views/Endpoint/views/Workflows/routes.tsx A
    app/views/Endpoint/views/Workflows/tabs/JobsTab.tsx

    commit cb617a4e3115a4ba8921344e861286611a954e1b Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Jun 3 15:14:51 2022
    -0500

    > refactor: migrate more views to react-router v6

    M app/api/DigitalRebar.ts M app/api/index.tsx M
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/table\_view/components/TableView.tsx M
    app/views/Endpoint/routes.tsx M
    app/views/Endpoint/views/Alerts/View.tsx M
    app/views/Endpoint/views/Alerts/routes.tsx R061
    app/views/Endpoint/views/BootEnvs/Editor.jsx
    app/views/Endpoint/views/BootEnvs/Editor.tsx M
    app/views/Endpoint/views/BootEnvs/View.jsx A
    app/views/Endpoint/views/BootEnvs/routes.tsx A
    app/views/Endpoint/views/BootEnvs/tabs/TemplateTab.tsx R059
    app/views/Endpoint/views/Contexts/Editor.jsx
    app/views/Endpoint/views/Contexts/Editor.tsx R100
    app/views/Endpoint/views/Contexts/ImageField.jsx
    app/views/Endpoint/views/Contexts/components/ImageField.jsx A
    app/views/Endpoint/views/Contexts/routes.tsx A
    app/views/Endpoint/views/Contexts/tabs/MetaTab.tsx M
    app/views/Endpoint/views/Endpoints/CreateEditor.jsx R083
    app/views/Endpoint/views/Endpoints/Editor.jsx
    app/views/Endpoint/views/Endpoints/Editor.tsx M
    app/views/Endpoint/views/Endpoints/View.jsx M
    app/views/Endpoint/views/Endpoints/components/ExtractButton.jsx A
    app/views/Endpoint/views/Endpoints/routes.tsx R073
    app/views/Endpoint/views/IdentityProviders/Editor.jsx
    app/views/Endpoint/views/IdentityProviders/Editor.tsx M
    app/views/Endpoint/views/IdentityProviders/View.jsx A
    app/views/Endpoint/views/IdentityProviders/routes.tsx M
    app/views/Endpoint/views/Inbox/Inbox.jsx R080
    app/views/Endpoint/views/Jobs/Editor.jsx
    app/views/Endpoint/views/Jobs/Editor.tsx M
    app/views/Endpoint/views/Jobs/View.jsx A
    app/views/Endpoint/views/Jobs/routes.tsx A
    app/views/Endpoint/views/Jobs/tabs/JobLogTab.tsx A
    app/views/Endpoint/views/Jobs/tabs/JobTemplatesTab.tsx R070
    app/views/Endpoint/views/Leases/Editor.jsx
    app/views/Endpoint/views/Leases/Editor.tsx M
    app/views/Endpoint/views/Leases/View.jsx A
    app/views/Endpoint/views/Leases/routes.tsx M
    app/views/Endpoint/views/Machines/ClusterEditor.tsx M
    app/views/Endpoint/views/Machines/ClustersView.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/MachinesView.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokersView.tsx A
    app/views/Endpoint/views/Machines/routes.tsx A
    app/views/Endpoint/views/Machines/tabs/BrokerMachinesTab.tsx A
    app/views/Endpoint/views/Machines/tabs/ClusterMachinesTab.tsx A
    app/views/Endpoint/views/Machines/tabs/DebuggerTab.tsx A
    app/views/Endpoint/views/Machines/tabs/ProfileTab.tsx

    commit d4d889d9dc9b62ad484e3ac648ca3530c9ed59d2 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 2 16:28:07 2022
    -0500

    > refactor(alerts): refactor alerts to use new router code

    M app/views/Endpoint/components/Navigation.tsx M
    app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/editor/components/EditorNavigation.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/tabs/MetaEditor/index.tsx M
    app/views/Endpoint/routes.tsx M
    app/views/Endpoint/views/Alerts/Editor.tsx A
    app/views/Endpoint/views/Alerts/routes.tsx M
    app/views/Endpoint/views/Contents/components/RSTDocsLazy.jsx

    commit 7c3133d8717f6a3e8ba3464beb2fdb787a29bdc5 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 2 15:22:31 2022
    -0500

    > deps: upgrade react-router to v6, partial migration

    M app/App.tsx M app/api/analytics.tsx M app/api/index.tsx M
    app/layout/SearchContainer.jsx M app/layout/SideNav/SideNav.tsx A
    app/routes.tsx D app/views/Endpoint/Router.tsx A
    app/views/Endpoint/components/Navigation.tsx M
    app/views/Endpoint/features/editor/tabs/NestedEditor/index.jsx M
    app/views/Endpoint/features/info/selectors.ts M
    app/views/Endpoint/features/table\_view/components/Buttons.tsx M
    app/views/Endpoint/features/table\_view/components/TableView.tsx M
    app/views/Endpoint/features/ux\_views/reducer.ts M
    app/views/Endpoint/index.tsx A app/views/Endpoint/routes.tsx M
    package-lock.json M package.json

    commit fb05a5e364240b622b4bc7ebe8d4f663bec96000 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 2 14:09:06 2022
    -0500

    > fix(endpoints): fix disable version set extract button

    M app/views/Endpoint/views/Endpoints/components/ExtractButton.jsx

    commit 2c4e3e67adca21e520c12922307b2dc91fda011f Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Thu Jun 2 13:13:17 2022 -0500

    > feat(inventory-os): show extended OS info on machine editor if
    > available

    M app/views/Endpoint/views/Machines/MachineEditor.tsx

    commit 1720f1051483700345dd7b18b950dfeb48726f86 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Thu Jun 2 13:13:17 2022 -0500

    > feat(inventory-os): show extended OS info on machine editor if
    > available

    M app/views/Endpoint/views/Machines/MachineEditor.tsx

    commit 6f7327d5e97ed90d7ea8a3c12c3fcc1b19703b66 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Jun 2 10:35:47 2022 -0500

    > feat(labs): styling prototype

    A app/views/Labs/index.tsx

    commit ee0f928524f85e703c6ca3f755dccd617bfa4e98 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 2 10:09:15 2022
    -0500

    > deps: upgrade react, babel, and some other dependencies

    M app/index.tsx M package-lock.json M package.json

    commit 441658dab23d69678f7eb190ec2089a376e39617 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Jun 2 09:01:35 2022
    -0500

    > fix(ux\_views): add landing page field to reducer

    M app/views/Endpoint/features/ux\_views/reducer.ts

    commit dda395b42e196736e5a2197d34433e0e40492100 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 1 11:34:23 2022
    -0500

    > fix(system): css fixes and tweaks, memory leak prevention

    M app/features/statistics/components/PerformanceWarning.tsx M
    app/layout/Flex.tsx M app/layout/Grid.tsx M
    app/views/Endpoint/views/System/components/BundlePie.tsx M
    app/views/Endpoint/views/System/components/JobHistogram.tsx M
    app/views/Endpoint/views/System/components/MachineTypePie.tsx M
    app/views/Endpoint/views/System/panes/Diagnostics.tsx M
    app/views/Endpoint/views/System/panes/InstalledContent.tsx

    commit 9cd22add56b1322480a8b02847974486cbb09221 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 1 10:28:00 2022
    -0500

    > feat(overview): use recharts for overview jobs and active machines

    A app/features/statistics/components/PerformanceWarning.tsx M
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx M
    app/views/Endpoint/views/System/components/JobHistogram.tsx M
    app/views/Endpoint/views/System/panes/InstalledContent.tsx M
    package-lock.json M package.json

    commit 520b284cccdbb695ab2d0d4533f2c260a8fcf29b Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Jun 1 09:37:48 2022 -0500

    > feat(overview): rough charts implementation, add more charts

    R100 app/views/Endpoint/views/Overview/components/BoxPlot.tsx
    app/features/statistics/components/BoxPlot.tsx A
    app/features/statistics/components/Chart.tsx R088 app/layout/EKG.jsx
    app/features/statistics/components/EKG.tsx A
    app/features/statistics/components/PieChart.tsx A
    app/features/statistics/util/svgpath.ts A app/layout/Grid.tsx M
    app/views/Endpoint/views/Overview/components/TaskRuntimeStats.tsx D
    app/views/Endpoint/views/Overview/components/Tau.tsx M
    app/views/Endpoint/views/Overview/panels/ClusterStatusPanel.jsx M
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx M
    app/views/Endpoint/views/System/components/BundlePie.tsx A
    app/views/Endpoint/views/System/components/JobHistogram.tsx A
    app/views/Endpoint/views/System/components/MachineTypePie.tsx M
    app/views/Endpoint/views/System/panes/InstalledContent.tsx

    commit 25986d2bf02138f198426dc56cb227eb3fea9a2f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 1 09:31:11 2022
    -0500

    > feat(blueprints,trigger\_providers): add documentation tabs

    M app/views/Endpoint/views/WorkOrders/BlueprintEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggerProvidersEditor.jsx

    commit 7cc797856ba4a2eabab4552e416667d0c459b6be Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Jun 1 09:27:27 2022
    -0500

    > feat(triggers): css improvements to flow visualizer

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.tsx A
    app/views/Endpoint/features/flow/oldFlow.ts.old

    commit 92bf71ac915bd53921f1bceda5e97e2b9231a12e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue May 31 16:12:46 2022
    -0500

    > build: replace clean-webpack-plugin with built-in clean feature

    M package-lock.json M package.json M webpack.config.js

    commit b2cc5ae7d979f799bd1d835d735fa2052c9e86a8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue May 31 16:12:22 2022
    -0500

    > feat(flow): minor css adjustments

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.tsx

    commit 51989661be569ddbb108f1b191d1df80c553c9d9 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 31 09:25:42 2022 -0500

    > fix: remove endpoint on home page title

    M app/api/index.tsx

    commit c98dcf4faf19bbe6e6a15a94eb1a6507e5dc70ae Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri May 27 16:43:32 2022 -0500

    > feat(overview): box plot outliers clickable

    M app/views/Endpoint/views/Overview/components/BoxPlot.tsx M
    app/views/Endpoint/views/Overview/components/TaskRuntimeStats.tsx M
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx

    commit a3860a8c991b45c01b77172e70b54c2ac39f6791 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri May 27 16:07:03 2022 -0500

    > feat(blueprints): add work orders tab to editor

    M app/views/Endpoint/views/WorkOrders/BlueprintEditor.jsx

    commit c21684218599aefe48cda97d57a4071af5b00ecd Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri May 27 16:00:30 2022 -0500

    > fix(triggers): move open/close button to right

    M app/views/Endpoint/features/flow/components/Visualizer.tsx

    commit 49c4e2a574909a68768d033526f0e6d5c53bf795 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri May 27 15:53:21 2022 -0500

    > chore: remove ViewOld.jsx

    D app/views/Endpoint/views/System/ViewOld.jsx

    commit 87f740200816fc65fccbe6d7e22082a80b5832d1 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Fri May 27 13:06:48 2022 -0500

    > feat(system): internal rewrite of info & preferences page

    M .vscode/settings.json M
    app/views/Endpoint/views/Catalog/MaintMode.tsx A
    app/views/Endpoint/views/System/Context.tsx A
    app/views/Endpoint/views/System/View.tsx R100
    app/views/Endpoint/views/System/View.jsx
    app/views/Endpoint/views/System/ViewOld.jsx A
    app/views/Endpoint/views/System/panes/BootstrapWizard.tsx R098
    app/views/Endpoint/views/System/DiagnosticsPane.jsx
    app/views/Endpoint/views/System/panes/Diagnostics.tsx A
    app/views/Endpoint/views/System/panes/EndpointHealthCheck.tsx A
    app/views/Endpoint/views/System/panes/EndpointSubscriptions.tsx A
    app/views/Endpoint/views/System/panes/HighAvailabilityStatus.tsx A
    app/views/Endpoint/views/System/panes/InstalledContent.tsx R100
    app/views/Endpoint/views/System/PreferencesPane.jsx
    app/views/Endpoint/views/System/panes/Preferences.jsx

    commit 2ed1b88546f98bab51f9fbb9bbe4bdbcca8d0840 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu May 26 16:12:26 2022 -0500

    > feat(info): task bundles pie chart

    A .vscode/drpux.code-snippets R075
    app/views/Endpoint/views/Overview/components/Pie.jsx
    app/views/Endpoint/views/Overview/components/Pie.tsx M
    app/views/Endpoint/views/Overview/components/TaskRuntimeStats.tsx A
    app/views/Endpoint/views/Overview/components/Tau.tsx M
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx M
    app/views/Endpoint/views/System/View.jsx A
    app/views/Endpoint/views/System/components/BundlePie.tsx

    commit 589f34e5b0541728f6d56bbe68a1f93bc8a868f5 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu May 26 10:53:50 2022
    -0500

    > feat(overview): add button to confirm displaying stats, add flag
    > check

    M app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx

    commit 24ecc3ea5ae6cfa76a32fd39cfea5b842d5adf7d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed May 25 16:48:13 2022 -0500

    > feat(overview): add tabs and options to runtime stats panel

    M app/views/Endpoint/views/Overview/components/BoxPlot.tsx M
    app/views/Endpoint/views/Overview/components/TaskRuntimeStats.tsx M
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx

    commit 2c1311ae62216cd94add903ef2e30c897a407d09 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed May 25 14:31:07 2022 -0500

    > feat(overview): runtime stats for work orders and jobs

    A app/views/Endpoint/views/Overview/components/BoxPlot.tsx A
    app/views/Endpoint/views/Overview/components/TaskRuntimeStats.tsx M
    app/views/Endpoint/views/Overview/index.jsx A
    app/views/Endpoint/views/Overview/panels/TaskRuntimePanel.tsx M
    app/views/Endpoint/views/Tasks/Editor.jsx

    commit 889ecd81f5b08bc3f86de74ba0b2442e665aa441 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed May 25 11:48:45 2022
    -0500

    > feat(table): filter created objects by base and input filter

    M app/views/Endpoint/features/table\_view/context.tsx A
    app/views/Endpoint/features/table\_view/filter.ts

    commit a15d92bea97959f372ddbdafbfd5f3f54c4e918b Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed May 25 10:17:41 2022 -0500

    > fix(machines): hide breakpoint button from debugger row

    M app/views/Endpoint/views/Machines/components/Debugger/Debugger.jsx
    M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit 3072f163276447bf96edcd60d5940d50442ae78f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed May 25 10:10:10 2022
    -0500

    > lint: fix types on nested workorder table

    M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit acebcd99924c5ddc713c654ea5651d5b1993ea3a Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed May 25 10:07:32 2022 -0500

    > fix(machines): debugger expand code semantics

    M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit ea8a18fb2e7da1beeba682a590cdeaf2e5994999 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu May 19 13:15:42 2022 -0500

    > feat(machines): caret expand/collapse for activity debugger rows

    M .vscode/extensions.json M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit 166a4be1fb68f7f0abc2704744098dace41fd9e1 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu May 19 11:18:36 2022 -0500

    > feat(machines): button to expand debugger row in activity table

    M .vscode/extensions.json M app/api/index.tsx M
    app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/util.tsx M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit 069e4628cc355f9e0e9f136d528c281a58db9e4d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 15:35:43 2022 -0500

    > fix(tables): memoize filtered table head columns and filler
    > elements

    M app/views/Endpoint/features/table\_view/components/Table.tsx

    commit 5946114c22ad49fc7adfae0f775e7833ff1460eb Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 15:16:43 2022 -0500

    > fix(machines): remove debug line

    M app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit bb40cc9002a446db2339c2e1f0ddea7c4b76dd06 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 15:15:33 2022 -0500

    > feat(machines): debugger rows beneath workorder activity rows

    M app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/util.tsx M
    app/views/Endpoint/views/Machines/components/Debugger/Debugger.jsx M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx

    commit 9b45052bc14dc8e73e1dcc8bee4e25d453c15a91 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 13:25:06 2022 -0500

    > fix(params): remove unused import

    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx

    commit 5f84089af4411751715ebe7849716cf16f783265 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 13:24:41 2022 -0500

    > fix(params): remove link if param source is current object

    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx

    commit 41834edb6e459c1f14da42b7684b7a3d5a41afd8 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 13:08:04 2022 -0500

    > feat(params): display where a param originates in aggregate view

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorFieldParts.tsx
    M app/views/Endpoint/features/editor/tabs/ParamEditor/index.tsx

    commit 0c05055247798a975395113ebe1b71eff680ee94 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue May 17 11:48:07 2022 -0500

    > feat(triggers): trigger required params in visualizer, other ui
    > changes

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.tsx M
    app/views/Endpoint/views/WorkOrders/components/TemplateTaskList.tsx

    commit adce17d5c630561d728109c2f9009d8269f98213 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon May 16 15:08:59 2022
    -0500

    > fix(alerts): fix nav notification word wrap

    M app/views/Endpoint/widgets/SoftErrors.tsx

    commit 5371779e12f5c7cde5261ff7065facdacd61e59e Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon May 16 12:00:05 2022 -0500

    > fix(triggers): remove debug log

    M app/views/Endpoint/features/flow/components/Visualizer.tsx

    commit 15db486666c440cf2745f315b18e617149ba5ca5 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon May 16 11:56:37 2022 -0500

    > feat(triggers): flow-comment nodes, layout changes

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.tsx

    commit 9c8f4877ffa07e0ccd86e41b8e0dc6fcc52b26d0 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon May 16 10:05:28 2022 -0500

    > feat(machines): add apply blueprint button to activity tabs in wo
    > mode

    M app/views/Endpoint/views/Machines/ClusterEditor.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx M
    app/views/Endpoint/views/Machines/components/BlueprintRunnerField.tsx

    commit 3b4f790b7d0f89f484ec5560f19d96af8d6316af Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed May 11 15:48:13 2022
    -0500

    > feat(alerts): add alerts nav menu item for unresolved alerts

    M app/api/index.tsx M app/api/socket.ts M
    app/layout/SideNav/style.tsx M
    app/views/Endpoint/features/info/reducer.ts M
    app/views/Endpoint/features/info/selectors.ts M
    app/views/Endpoint/views/Alerts/components/AlertLevel.tsx M
    app/views/Endpoint/widgets/NavHeaderEndpoint.tsx M
    app/views/Endpoint/widgets/SoftErrors.tsx

    commit 964d831b413528fce67d3404a28d867ddf934962 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue May 10 17:03:36 2022
    -0500

    > feat(alerts): WIIP redux state for alerts

    M app/views/Endpoint/features/auth/actions.ts M
    app/views/Endpoint/features/editor/util.ts M
    app/views/Endpoint/features/info/actions.ts M
    app/views/Endpoint/features/info/reducer.ts R070
    app/views/Endpoint/widgets/SoftErrors.jsx
    app/views/Endpoint/widgets/SoftErrors.tsx

    commit 0909e64dcccf154441c80412d5750c1ec1e78b05 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Fri May 6 22:49:26 2022 -0500

    > fix(alerts): fix coloration cases

    M app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx

    commit 0ad7d239a9d950932e39a48ed343e10e62acaaa9 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri May 6 16:00:30 2022
    -0500

    > fix(alerts): add links for ack user and principal

    M app/api/DigitalRebar.ts A
    app/views/Endpoint/views/Alerts/columns/text\_acknowledgeUser.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_principal.tsx

    commit 189197a4abd7d569c9dcded82687c10cab1e1c0b Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Fri May 6 15:13:51 2022 -0500

    > fix(alerts): missed file

    A app/views/Endpoint/views/Alerts/columns/text\_acknowledgeUser.tsx

    commit 21e8c9dc982b1363d759710a42a28a706849a9b2 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Fri May 6 13:57:46 2022 -0500

    > fix(alerts): more tweaks

    M app/views/Endpoint/views/Alerts/View.tsx M
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_note.tsx

    commit c49efc7910df3d02fd9f0853057bf9c3428f3388 Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Fri May 6 13:54:22 2022 -0500

    > feat(alerts): update formatting for alerts page

    M app/api/icons.ts M app/config/menu.ts M
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.tsx
    M app/views/Endpoint/views/Alerts/Editor.tsx M
    app/views/Endpoint/views/Alerts/View.tsx M
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx M
    app/views/Endpoint/views/Alerts/components/AlertLevel.tsx

    commit a87646a3844eb2232b04a9f89a873748a89b0a37 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri May 6 13:01:35 2022
    -0500

    > feat(alerts): re-add not acknowledged message in ack time column

    M
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.tsx
    M app/views/Endpoint/views/Alerts/View.tsx

    commit a8ae08262713d9fb7f34f95279bf4f3a7813a271 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri May 6 12:59:21 2022
    -0500

    > fix(alerts): fix hover tooltips for times

    M app/views/Endpoint/views/Alerts/View.tsx

    commit 0edaf28c1813e0cc804c6e92670493d2e856525a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri May 6 12:47:28 2022
    -0500

    > refactor(alerts): minor cleanup, update copyright, minor features

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/table\_view/components/Buttons.tsx R088
    app/views/Endpoint/views/Alerts/CreateEditor.jsx
    app/views/Endpoint/views/Alerts/CreateEditor.tsx R095
    app/views/Endpoint/views/Alerts/Editor.jsx
    app/views/Endpoint/views/Alerts/Editor.tsx R071
    app/views/Endpoint/views/Alerts/View.jsx
    app/views/Endpoint/views/Alerts/View.tsx M
    app/views/Endpoint/views/Alerts/columns/gen\_id\_name.tsx M
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_contents.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_note.tsx M
    app/views/Endpoint/views/Alerts/columns/text\_principal.tsx A
    app/views/Endpoint/views/Alerts/components/AcknowledgeAction.tsx M
    app/views/Endpoint/views/Alerts/components/AlertLevel.tsx M
    app/views/Endpoint/views/Alerts/components/AlertLevelDropdown.tsx M
    app/views/Endpoint/views/Alerts/components/EditorFields.tsx M
    app/views/Endpoint/views/Alerts/components/EditorHeader.tsx M
    app/views/Endpoint/views/Inbox/Inbox.jsx M
    app/views/Endpoint/views/System/View.jsx

    commit e419c0ac89c2e791008872e2fef15780fb4bf4ee Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Fri May 6 11:40:04 2022 -0500

    > fix: spelling and test in HA test

    M app/views/Endpoint/views/System/View.jsx

    commit f6c8e2c70e0685832b102215970223f76e054ea7 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu May 5 20:56:03 2022 -0500

    > feat(alerts): custom levels dropdown in alert create editor

    M app/views/Endpoint/views/Alerts/CreateEditor.jsx M
    app/views/Endpoint/views/Alerts/Editor.jsx M
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx A
    app/views/Endpoint/views/Alerts/components/AlertLevel.tsx A
    app/views/Endpoint/views/Alerts/components/AlertLevelDropdown.tsx D
    app/views/Endpoint/views/Alerts/components/AlertSeverity.tsx

    commit 7c2f6db8ca553da6fb7eccee976167ea337f8c72 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Thu May 5 13:48:02 2022 -0500

    > fix(format): forgot to run prettier first

    M app/views/Endpoint/views/Alerts/components/AlertSeverity.tsx

    commit dc71306afff6175773900a1a5ff4af9304462c92 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Thu May 5 13:25:37 2022 -0500

    > feat(alerts): Allow of non-standard alert levels

    M app/views/Endpoint/views/Alerts/components/AlertSeverity.tsx

    commit e1c014146836b12104fd9265786f972291472312 Author: Zander
    \<<me@zanderf.net>\> Date: Thu May 5 13:05:49 2022 -0500

    > fix(alerts): fallback to WARN for missing alert levels

    M app/views/Endpoint/views/Alerts/components/AlertSeverity.tsx

    commit f96d0069fa8071401dabece8935e2eac50e388a4 Author: Zander
    \<<me@zanderf.net>\> Date: Thu May 5 13:02:02 2022 -0500

    > feat(alerts): small UX/styling changes

    M app/views/Endpoint/views/Alerts/CreateEditor.jsx M
    app/views/Endpoint/views/Alerts/Editor.jsx M
    app/views/Endpoint/views/Alerts/View.jsx M
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx A
    app/views/Endpoint/views/Alerts/components/AlertSeverity.tsx

    commit 814091e237187540d86f98bb4b7899d6bc682f60 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu May 5 12:13:54 2022
    -0500

    > fix(logs): fix adding new lines null ptr

    M app/views/Endpoint/views/Logs/hooks.jsx

    commit 49cbbac8001b4496cb417fa02d831744d8041830 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Wed May 4 15:07:26 2022 -0500

    > feat(alerts): Add an alerts menu item and table

    M app/api/DigitalRebar.ts M app/api/database.ts M app/api/icons.ts M
    app/api/index.tsx M app/assets/eula/community.html M
    app/assets/eula/gdpr.html M app/assets/eula/rackn.html M
    app/assets/license.html M app/config/menu.ts M app/config/routes.ts
    M app/index.html M app/layout/GenericContainer.css M
    app/layout/SearchContainer.jsx M app/layout/Table/Table.css M
    app/layout/UXLoader/UXLoader.css M app/views/Endpoint/Router.tsx M
    app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/alerts.tsx
    M app/views/Endpoint/features/table\_view/components/Buttons.tsx A
    app/views/Endpoint/views/Alerts/CreateEditor.jsx A
    app/views/Endpoint/views/Alerts/Editor.jsx A
    app/views/Endpoint/views/Alerts/View.jsx A
    app/views/Endpoint/views/Alerts/columns/gen\_id\_name.tsx A
    app/views/Endpoint/views/Alerts/columns/icon\_acknowledged.tsx A
    app/views/Endpoint/views/Alerts/columns/text\_contents.tsx A
    app/views/Endpoint/views/Alerts/columns/text\_level.tsx A
    app/views/Endpoint/views/Alerts/columns/text\_note.tsx A
    app/views/Endpoint/views/Alerts/columns/text\_principal.tsx A
    app/views/Endpoint/views/Alerts/components/EditorFields.tsx A
    app/views/Endpoint/views/Alerts/components/EditorHeader.tsx M
    app/views/Endpoint/views/Contents/index.jsx M
    app/views/Endpoint/views/Registration/Registration.css

    commit 17c679bb5695bc2c4419424dd61228bf87a1a83f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue May 3 15:31:25 2022
    -0500

    > feat(info): add diagnostics json downloader

    M app/views/Endpoint/views/System/DiagnosticsPane.jsx

    commit aed7e0f7b2393f69cec2069a246704c34d76109b Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon May 2 15:03:19 2022
    -0500

    > fix(tenants): fix tenant membership typo, fix machine filter null
    > ptr

    M app/views/Endpoint/views/Tenants/columns/enum\_members\_icons.jsx
    M app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx

    commit c0abcbb7ad30cd1378f387ce8cb9be8ebf06c41a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon May 2 15:02:26 2022
    -0500

    > feat(flow): animate trigger edges instead of task edges

    M app/views/Endpoint/features/flow/components/Visualizer.tsx

    commit bdd72e7f101c6791715853f32cc783a9173550ef Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 29 14:05:02 2022
    -0500

    > feat(contents): add meta field to contents browser

    M app/views/Endpoint/views/Contents/index.jsx

    commit e778c179895a9ecbaedf1efd3258a81a2ff25e94 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 25 16:12:46 2022
    -0500

    > fix(triggers): put editor panel over flow panel

    M app/views/Endpoint/views/WorkOrders/TriggersView.jsx

    commit 9b3b5136fdc4695bf810f408d5e1eae98d9b3485 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 25 14:51:17 2022
    -0500

    > feat(flow): add task rendering

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersView.jsx

    commit 046ea275c804973f6d0af9d968e7e9a5d2085c7b Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 21 13:44:05 2022 -0500

    > feat(triggers): visualizer node population prototype

    M app/views/Endpoint/features/flow/components/Visualizer.tsx D
    app/views/Endpoint/features/flow/nodeTypes.ts A
    app/views/Endpoint/features/flow/nodeTypes.tsx D
    app/views/Endpoint/features/flow/reducer.ts D
    app/views/Endpoint/features/flow/selectors.ts M
    app/views/Endpoint/index.tsx M app/views/Endpoint/reducer.ts M
    app/views/Endpoint/views/WorkOrders/TriggersView.jsx M tsconfig.json

    commit 0025681fe67e1f0d53b8c080b991ca83963d553a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Apr 19 16:14:04 2022
    -0500

    > feat(editor): add aggregate params toggle to endpoints, plugins,
    > profiles, stages, triggers, and workorders

    M app/views/Endpoint/views/Endpoints/Editor.jsx M
    app/views/Endpoint/views/Plugins/Editor.jsx M
    app/views/Endpoint/views/Profiles/Editor.jsx M
    app/views/Endpoint/views/Stages/Editor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/WorkOrdersEditor.jsx

    commit 1755a0b2d3fb67f96d1ca8badc70d9490645b543 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Apr 19 15:52:17 2022
    -0500

    > feat(editor): aggregate params view switch

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/editor/hooks.ts M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamAddHeader.tsx
    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorFieldParts.tsx
    M app/views/Endpoint/features/editor/tabs/ParamEditor/index.tsx M
    app/views/Endpoint/views/Machines/ClusterEditor.tsx M
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx M
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.jsx

    commit 2cf25beb9f6123cf06ceaf241327dbce79caa30c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 18 14:18:23 2022
    -0500

    > fix(catalog): maint mode selector: return false instead of empty
    > fragment

    M app/views/Endpoint/views/Catalog/MaintMode.tsx

    commit befb563f3db2102b368484e47f2187a4f289f913 Author: Zander
    \<<me@zanderf.net>\> Date: Mon Apr 18 12:56:50 2022 -0500

    > feat(catalog): add maintenance mode selector

    M app/views/Endpoint/views/Catalog/Catalog.jsx A
    app/views/Endpoint/views/Catalog/MaintMode.tsx M
    app/views/Endpoint/views/System/PreferencesPane.jsx

    commit 53bf3f6d75dfbc19930785a42c81ede1f13aa4e1 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 14 15:57:36 2022 -0500

    > feat: page titles based on current route

    M app/api/index.tsx M app/config/routes.ts

    commit aab38f08c0a18cb83efc07e0b7ef06b515ca2d1f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Apr 14 14:12:54 2022
    -0500

    > feat(flow): continued generic node generation work

    M app/views/Endpoint/features/flow/components/Visualizer.tsx M
    app/views/Endpoint/features/flow/nodeTypes.ts M
    app/views/Endpoint/features/flow/reducer.ts M tsconfig.json

    commit a072ca1626a10ab255921138afae58f29314e3de Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 14 10:32:24 2022 -0500

    > feat(flow): close and open button

    M app/views/Endpoint/features/flow/components/Visualizer.tsx

    commit 744c08a971888bab14f3c8df56394f6524c0162e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Apr 14 09:37:58 2022
    -0500

    > feat(flow): wip procedural node generation

    M .vscode/settings.json M
    app/views/Endpoint/features/flow/components/Visualizer.tsx A
    app/views/Endpoint/features/flow/nodeTypes.ts D
    app/views/Endpoint/features/flow/nodes/nodeTypes.ts M
    app/views/Endpoint/features/flow/reducer.ts M tsconfig.json

    commit ce516584f6228b7bf77ceaf906da94339d44abc8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 11 16:15:17 2022
    -0500

    > feat(flow): redux state, initial preview of persistent flow view

    M app/api/index.tsx M app/layout/Resizer.tsx R100
    app/views/Endpoint/features/editor/flow/components/CustomEdges.tsx
    app/views/Endpoint/features/flow/components/CustomEdges.tsx R100
    app/views/Endpoint/features/editor/flow/components/FlowNode.tsx
    app/views/Endpoint/features/flow/components/FlowNode.tsx R095
    app/views/Endpoint/features/editor/flow/components/Toolbar.tsx
    app/views/Endpoint/features/flow/components/Toolbar.tsx A
    app/views/Endpoint/features/flow/components/Visualizer.tsx A
    app/views/Endpoint/features/flow/flow.ts A
    app/views/Endpoint/features/flow/nodes/nodeTypes.ts A
    app/views/Endpoint/features/flow/reducer.ts A
    app/views/Endpoint/features/flow/selectors.ts M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/index.tsx M app/views/Endpoint/reducer.ts M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 15803756ad703f6ee5adb12dec62bd1a52cc2445 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 11 14:58:58 2022
    -0500

    > refactor(table): refactor resizer code to separate component,
    > remove old LegacyPanel

    M app/layout/Resizer.tsx M
    app/views/Endpoint/features/table\_view/components/TableHead.tsx M
    app/views/Endpoint/features/table\_view/components/TableView.tsx D
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 11e14a2dad60632f3761122ac16fbc3f3b7d8f98 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 7 15:46:03 2022 -0500

    > feat(editor): resizer vertical prop

    R050 app/layout/Resizer.jsx app/layout/Resizer.tsx M
    app/views/Endpoint/features/table\_view/components/BulkPanel.tsx M
    app/views/Endpoint/features/table\_view/components/TableHead.tsx M
    app/views/Endpoint/features/table\_view/components/TableView.tsx M
    app/views/Endpoint/views/Contents/index.jsx M
    app/views/Endpoint/views/Machines/MachinesView.tsx

    commit 33c6201114047004f8a1df5677662f4066a4543e Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 7 11:48:45 2022 -0500

    > fix(editor): user-select: none when resizing a resizer

    M app/layout/Resizer.jsx

    commit 330e79ba818e80dfdff79bfd68fa5d41d2564634 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Apr 7 11:42:26 2022 -0500

    > fix(triggers): search and dropdown bar styling

    M app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx

    commit f3d2adc70932d98b6104c7d425ebe96418c14fe3 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Apr 6 14:10:25 2022
    -0500

    > fix(license): fix backup license file being empty

    M app/views/Endpoint/views/Registration/panes/Trial.jsx

    commit 6fa58787b44f23b2954eb99a5de64b3731e3848e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Apr 6 10:48:20 2022
    -0500

    > feat(table): add support for meta filtering

    M
    app/views/Endpoint/features/table\_view/components/Filter/Search.tsx

    commit eb6fe5cf0800a3080c2400bab32a2cad42f29ade Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Apr 6 10:45:29 2022
    -0500

    > feat(triggers): quick-add filter buttons

    M app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx

    commit 828c9471aec88b3b6476f3977a00270d4a7c798a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Apr 5 15:59:28 2022
    -0500

    > feat(trigger): filter preview table

    M app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx M
    app/views/Endpoint/features/table\_view/components/Filter/FilterList.tsx
    M app/views/Endpoint/features/table\_view/components/TableFilter.tsx
    M
    app/views/Endpoint/features/table\_view/components/TablePaginate.tsx
    M app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/reducer.ts M
    app/views/Endpoint/views/Machines/components/NestedMachineTable.tsx
    M app/views/Endpoint/views/WorkOrders/TriggersCreateEditor.jsx M
    app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx M
    app/views/Endpoint/views/WorkOrders/components/NestedMachineApplyTable.tsx

    commit 14f16bf1080f46bb5caf6ec7fda7b5d5c3dc4bb2 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Apr 5 13:23:05 2022
    -0500

    > feat(triggers): improved machine filter editing

    M
    app/views/Endpoint/features/table\_view/components/Filter/FilterList.tsx
    M
    app/views/Endpoint/features/table\_view/components/Filter/Search.tsx
    M app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/components/CronField.tsx D
    app/views/Endpoint/views/WorkOrders/components/MachineFilter.jsx A
    app/views/Endpoint/views/WorkOrders/components/MachineFilter.tsx

    commit 46a03ae080789049b3c95335ff385a0910b18adc Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 4 11:46:08 2022
    -0500

    > fix(tasks): move extra claims under templates

    M app/views/Endpoint/views/Tasks/Editor.jsx

    commit 88c9e13326afc0dde7caa39ca57bffda60d1d463 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 4 11:32:54 2022
    -0500

    > feat(triggers): move params from tab to editor page

    M app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx

    commit eae9ba6516ff8b1e3c3b48f754f0dc86a8903780 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 4 11:21:42 2022
    -0500

    > fix(editor): fix SearchDropdown autofocus not focusing on open

    M app/views/Endpoint/features/editor/components/SearchDropdown.tsx M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamAddHeader.tsx

    commit 4a71fc3e7bb61db80bfbc49cdb4868e7eefce81c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 4 10:48:31 2022
    -0500

    > fix(tasks,jobs): add back ExtraClaims field with Claims editor

    M app/views/Endpoint/views/Jobs/Editor.jsx M
    app/views/Endpoint/views/Roles/components/ClaimsList.jsx M
    app/views/Endpoint/views/Tasks/Editor.jsx

    commit 3544f34c1b873ba62cb9aea05a1bf4d2cb09edf8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Apr 4 10:15:46 2022
    -0500

    > fix(system): hide cloud drp link when no self runners are present

    M app/views/Endpoint/views/System/View.jsx

    commit f06bdeaf1418beef2a4589307db76675dccb457d Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Fri Apr 1 11:14:17 2022 -0500

    > fix(cloud-detect): help user disable cloud detection from wizard

    M app/views/Endpoint/views/System/View.jsx

    commit cc6baa80f785a90a4f5559b87c1dfa49193da970 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 1 10:58:01 2022
    -0500

    > fix(blueprints): add border radius to dragged list items

    M
    app/views/Endpoint/views/WorkOrders/components/TemplateTaskList.tsx

    commit 84df7214b48408fd4f2ff24cef177d7ea50b87a3 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 1 10:53:41 2022
    -0500

    > feat(blueprints): migrate TemplateTaskList to typescript, add
    > draggable list feature

    M app/layout/Drag/List.tsx R057
    app/views/Endpoint/views/WorkOrders/components/TemplateTaskList.jsx
    app/views/Endpoint/views/WorkOrders/components/TemplateTaskList.tsx

    commit b609b59fea656e097f6972381048854f03cccb88 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 1 10:33:19 2022
    -0500

    > feat(workflows): use drag ordered list in workflow create

    M app/views/Endpoint/views/Workflows/CreateEditor.jsx

    commit 2cdf362fd7e2424f2929ba1d8ad5621562bd0f38 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 1 10:26:23 2022
    -0500

    > fix(editor): allow drag ordered lists with duplicate items

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/views/Workflows/Editor.jsx M
    app/views/Endpoint/views/Workflows/components/OrderedList.tsx

    commit 2d2b9e63f4cfe07ef84350f5d1602c09b23dfe36 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Apr 1 09:51:59 2022
    -0500

    > fix(editor): fix drag ordered list z-index issue

    M app/layout/Drag/List.tsx M
    app/views/Endpoint/views/Workflows/components/OrderedList.tsx

    commit d2340a0459373372ba38b211ab351fe0556cc757 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 31 16:26:31 2022
    -0500

    > feat(editor): WIP draggable ordered list field

    M app/api/DigitalRebar.ts A app/layout/Drag/List.tsx M
    app/views/Endpoint/features/editor/components/SearchDropdown.tsx M
    app/views/Endpoint/views/Stages/Editor.jsx R053
    app/views/Endpoint/views/Workflows/components/OrderedList.jsx
    app/views/Endpoint/views/Workflows/components/OrderedList.tsx

    commit 7fb0afcbdc3381956ea344cff90e49e4e7b6862d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 31 13:02:41 2022
    -0500

    > feat(table): subtle ux css for table header column dragging

    M app/views/Endpoint/features/table\_view/components/TableHead.tsx

    commit 7f927b52affe2de0cfb803b5c3755dc89aba7b35 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 30 16:10:43 2022
    -0500

    > fix(table): fix issue dragging columns to last index

    M app/views/Endpoint/features/table\_view/components/TableHead.tsx

    commit c6485435a36869e340944864ec40430bbcf69a88 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 30 16:07:30 2022
    -0500

    > fix(table): fix header css inverted case for dragging

    M app/views/Endpoint/features/table\_view/components/TableHead.tsx

    commit 63f1bb6237e56da0f008e0e8c1df21805c67bdc1 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 30 16:05:46 2022
    -0500

    > feat(table): more intuitive math for calculating when to drag
    > columns

    M app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/components/TableHead.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/util.tsx

    commit f6f4669da47e49d11f19a11453e1846432fcc38c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 30 14:47:08 2022
    -0500

    > feat(table): draggable columns

    M
    app/views/Endpoint/features/table\_view/columns/util\_selection.tsx
    M app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/components/TableHead.tsx M
    app/views/Endpoint/features/table\_view/components/TableRow.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/ux\_views/reducer.ts M package-lock.json
    M package.json

    commit b94929ba8842a54c6d9a66ad58d94ba6bf7c3839 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 29 14:30:51 2022
    -0500

    > refactor(event tool): fix the event tool appearance after
    > refactor, memoize lines

    M app/views/Endpoint/features/event\_tool/components/EventTool.tsx

    commit 4496a256f83a8e53cac7540c2978586e95e45794 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 24 17:30:02 2022 -0500

    > feat(triggers): flow editor toolbar layout prototype

    A app/views/Endpoint/features/editor/flow/components/Toolbar.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 8209564942a34a3003a01f59e80f998889c84f2f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 24 16:07:15 2022 -0500

    > feat(triggers): flow node header buttons (info, jump to editor)

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx

    commit eac7a5c94d8e549a8cb5b49f3f75cff6bb6689aa Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 15:50:50 2022
    -0500

    > feat(flow): strange edge removal

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 852e35e7bca44a710a26773639ce8637af310f8d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 15:33:30 2022
    -0500

    > feat(flow): edge type checking and css

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx

    commit dfe886c2c0ee497377fd05e0c7fcc0cbd460ef5a Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 24 15:27:41 2022 -0500

    > fix(triggers): small flow css change

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx

    commit 5b9ebc9b15973dcc4a2b8d756f60d248226f1c0d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 15:21:03 2022
    -0500

    > feat(flow): custom edge types

    A app/views/Endpoint/features/editor/flow/components/CustomEdges.tsx
    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit ecd8662d990178e57696b5cf0aa89dddb50fae6d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 11:59:24 2022
    -0500

    > fix: fix missing type in create for racks and ux-views, fix ux
    > views create

    M app/views/Endpoint/views/Racks/View.jsx M
    app/views/Endpoint/views/UXViews/CreateEditor.jsx M
    app/views/Endpoint/views/UXViews/View.jsx

    commit 5ce38e0165baedc5cdcc31ffe10cc3609a5a74bc Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 11:40:05 2022
    -0500

    > fix(editor): remove prefix / on object create

    M app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/views/Racks/CreateEditor.jsx

    commit bb916605dcda6855477c26c27859e57c11ca6d82 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 10:28:14 2022
    -0500

    > fix(leases): fix expire times cutting out timezone

    M app/views/Endpoint/views/Leases/columns/icon\_expired.jsx

    commit 070c031ce7c5f270b08a18528e840052a7234058 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 24 10:04:03 2022
    -0500

    > fix(racks): fix create save requiring name instead of id, fix null
    > ptrs in undefined param values

    M app/views/Endpoint/views/Racks/CreateEditor.jsx M
    app/views/Endpoint/views/Racks/components/ValidationToMachine.jsx M
    app/views/Endpoint/views/Racks/components/ValidationToParams.jsx

    commit 068661f8ea15291a9caa07e047862e9d005b91ac Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 23 15:50:11 2022
    -0500

    > feat(flow): capitalized object types, dot background

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit aa85ddf8a079cbc2c77f3d6f23df33dbd3299819 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 23 15:47:38 2022
    -0500

    > feat(flow): add vertical handles

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit eb3eca464693e25cdf318bcff7b6e90d17f5ecba Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 23 15:20:42 2022
    -0500

    > fix(triggers): remove errored imports

    M app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit d9fe6840b45dd5d4dd6d2563bd6e8d6ff8e112a4 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 23 15:18:39 2022
    -0500

    > feat(flow): changed node headers to allow outputs

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 908116ba1685b7945abe1518e0c0a182dbc5669f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 22 15:08:33 2022
    -0500

    > fix(triggers): fix react-flow edges not working

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 0c21711a562f18e3bec51cda1310af59e490b7d6 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 21 16:34:37 2022
    -0500

    > feat(triggers): draggable flow nodes, dynamic inputs

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/editor/flow/components/FlowNode.tsx R086
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx
    app/views/Endpoint/views/WorkOrders/TriggersEditor.tsx

    commit 485de82bbaccb98b274eceac3161df24ed065168 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 21 15:08:42 2022
    -0500

    > refactor(resources): refactor clusters and resource brokers views
    > to typescript

    M app/views/Endpoint/features/table\_view/columns/icon\_endpoint.tsx
    M app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx
    R090 app/views/Endpoint/views/Machines/ClusterCreateEditor.jsx
    app/views/Endpoint/views/Machines/ClusterCreateEditor.tsx R094
    app/views/Endpoint/views/Machines/ClusterEditor.jsx
    app/views/Endpoint/views/Machines/ClusterEditor.tsx R086
    app/views/Endpoint/views/Machines/ClustersView.jsx
    app/views/Endpoint/views/Machines/ClustersView.tsx M
    app/views/Endpoint/views/Machines/MachinesView.tsx R086
    app/views/Endpoint/views/Machines/ResourceBrokerCreateEditor.jsx
    app/views/Endpoint/views/Machines/ResourceBrokerCreateEditor.tsx
    R094 app/views/Endpoint/views/Machines/ResourceBrokerEditor.jsx
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.tsx R086
    app/views/Endpoint/views/Machines/ResourceBrokersView.jsx
    app/views/Endpoint/views/Machines/ResourceBrokersView.tsx M
    app/views/Endpoint/views/Machines/components/RequiredParamFields.tsx

    commit da88b7cd8a29df39bd4182850581697b67dbeb54 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 21 14:50:15 2022
    -0500

    > refactor(table): migrate table columns to typescript

    M app/api/DigitalRebar.ts M app/api/socket.ts R088
    app/views/Endpoint/features/event\_tool/components/EventButton.jsx
    app/views/Endpoint/features/event\_tool/components/EventButton.tsx
    R082
    app/views/Endpoint/features/event\_tool/components/EventTool.jsx
    app/views/Endpoint/features/event\_tool/components/EventTool.tsx M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon.tsx
    M
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon\_id.tsx
    R063
    app/views/Endpoint/features/table\_view/columns/gen\_link\_id.jsx
    app/views/Endpoint/features/table\_view/columns/gen\_link\_id.tsx
    R064
    app/views/Endpoint/features/table\_view/columns/gen\_link\_object.jsx
    app/views/Endpoint/features/table\_view/columns/gen\_link\_object.tsx
    R078
    app/views/Endpoint/features/table\_view/columns/icon\_available.jsx
    app/views/Endpoint/features/table\_view/columns/icon\_available.tsx
    R082
    app/views/Endpoint/features/table\_view/columns/icon\_endpoint.jsx
    app/views/Endpoint/features/table\_view/columns/icon\_endpoint.tsx D
    app/views/Endpoint/features/table\_view/columns/icon\_locked.jsx
    R075 app/views/Endpoint/features/table\_view/columns/icon\_meta.jsx
    app/views/Endpoint/features/table\_view/columns/icon\_meta.tsx R070
    app/views/Endpoint/features/table\_view/columns/icon\_readonly.jsx
    app/views/Endpoint/features/table\_view/columns/icon\_readonly.tsx
    R100 app/views/Endpoint/features/table\_view/columns/index.js
    app/views/Endpoint/features/table\_view/columns/index.ts R065
    app/views/Endpoint/features/table\_view/columns/text\_bundle.jsx
    app/views/Endpoint/features/table\_view/columns/text\_bundle.tsx
    R072
    app/views/Endpoint/features/table\_view/columns/text\_description.jsx
    app/views/Endpoint/features/table\_view/columns/text\_description.tsx
    R097
    app/views/Endpoint/features/table\_view/columns/util\_selection.jsx
    app/views/Endpoint/features/table\_view/columns/util\_selection.tsx
    M app/views/Endpoint/views/Pipelines/View.jsx

    commit 4489b7f1aef49c11de50b79860f33a1f023640b6 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 21 12:47:19 2022
    -0500

    > refactor(redux): migrate to typescript, move actions into feature
    > folders

    M app/api/cloudia.tsx M app/api/index.tsx M app/config/menu.ts M
    app/layout/Table/Filter.jsx M app/layout/Table/Row.jsx M
    app/layout/Wrapper.jsx M app/rootReducer.ts M app/store.ts R100
    app/views/Endpoint/actions/meta.jsx app/views/Endpoint/actions.ts D
    app/views/Endpoint/actions/auth.jsx A
    app/views/Endpoint/features/auth/actions.ts R093
    app/views/Endpoint/features/auth/HeaderAuthButtons.tsx
    app/views/Endpoint/features/auth/components/HeaderAuthButtons.tsx M
    app/views/Endpoint/features/auth/reducer.ts M
    app/views/Endpoint/features/auth/selectors.ts M
    app/views/Endpoint/features/dependencies/reducer.ts M
    app/views/Endpoint/features/dependencies/selectors.ts R100
    app/views/Endpoint/actions/event\_tool.jsx
    app/views/Endpoint/features/event\_tool/actions.ts M
    app/views/Endpoint/features/event\_tool/components/EventButton.jsx M
    app/views/Endpoint/features/event\_tool/components/EventTool.jsx M
    app/views/Endpoint/features/event\_tool/reducer.ts M
    app/views/Endpoint/features/event\_tool/selectors.ts R069
    app/views/Endpoint/actions/inbox.jsx
    app/views/Endpoint/features/inbox/actions.ts R091
    app/views/Endpoint/features/inbox/NavNotification.tsx
    app/views/Endpoint/features/inbox/components/NavNotification.tsx M
    app/views/Endpoint/features/inbox/reducer.ts M
    app/views/Endpoint/features/inbox/selectors.ts R082
    app/views/Endpoint/actions/info.jsx
    app/views/Endpoint/features/info/actions.ts M
    app/views/Endpoint/features/info/reducer.ts M
    app/views/Endpoint/features/info/selectors.ts R077
    app/views/Endpoint/actions/license.jsx
    app/views/Endpoint/features/license/actions.ts R084
    app/views/Endpoint/features/license/ExceedWarning.jsx
    app/views/Endpoint/features/license/components/ExceedWarning.tsx
    R094 app/views/Endpoint/features/license/ExpireWarning.jsx
    app/views/Endpoint/features/license/components/ExpireWarning.tsx
    R089 app/views/Endpoint/features/license/HeaderExpireWarning.jsx
    app/views/Endpoint/features/license/components/HeaderExpireWarning.tsx
    R084 app/views/Endpoint/features/license/HeaderUpdateNotice.jsx
    app/views/Endpoint/features/license/components/HeaderUpdateNotice.tsx
    R091 app/views/Endpoint/features/license/NavNotification.jsx
    app/views/Endpoint/features/license/components/NavNotification.tsx M
    app/views/Endpoint/features/license/reducer.ts M
    app/views/Endpoint/features/license/selectors.ts R100
    app/views/Endpoint/actions/machine\_status.jsx
    app/views/Endpoint/features/machine\_status/actions.ts R069
    app/views/Endpoint/features/machine\_status/reducer.jsx
    app/views/Endpoint/features/machine\_status/reducer.ts R074
    app/views/Endpoint/features/machine\_status/selectors.jsx
    app/views/Endpoint/features/machine\_status/selectors.ts R083
    app/views/Endpoint/features/meta/selectors.jsx
    app/views/Endpoint/features/meta/selectors.ts M
    app/views/Endpoint/features/meta/url\_reducer.ts M
    app/views/Endpoint/features/meta/version\_reducer.ts R100
    app/views/Endpoint/features/table\_view/actions.tsx
    app/views/Endpoint/features/table\_view/actions.ts R100
    app/views/Endpoint/features/table\_view/hooks.tsx
    app/views/Endpoint/features/table\_view/hooks.ts R099
    app/views/Endpoint/features/table\_view/reducer.tsx
    app/views/Endpoint/features/table\_view/reducer.ts R099
    app/views/Endpoint/features/table\_view/selectors.tsx
    app/views/Endpoint/features/table\_view/selectors.ts R072
    app/views/Endpoint/actions/ux\_views.jsx
    app/views/Endpoint/features/ux\_views/actions.ts R089
    app/views/Endpoint/features/ux\_views/Nav.jsx
    app/views/Endpoint/features/ux\_views/components/Nav.tsx R086
    app/views/Endpoint/features/ux\_views/RoleSelector.jsx
    app/views/Endpoint/features/ux\_views/components/RoleSelector.tsx
    R077 app/views/Endpoint/features/ux\_views/reducer.jsx
    app/views/Endpoint/features/ux\_views/reducer.ts R086
    app/views/Endpoint/features/ux\_views/selectors.jsx
    app/views/Endpoint/features/ux\_views/selectors.ts M
    app/views/Endpoint/index.tsx R100 app/views/Endpoint/reducer.tsx
    app/views/Endpoint/reducer.ts R100
    app/views/Endpoint/features/selector.ts
    app/views/Endpoint/selectors.ts M
    app/views/Endpoint/views/Endpoints/CreateEditor.jsx M
    app/views/Endpoint/views/Inbox/Inbox.jsx M
    app/views/Endpoint/views/Inbox/InboxModal.jsx M
    app/views/Endpoint/views/License/View.jsx M
    app/views/Endpoint/views/License/components/UploadModal.jsx M
    app/views/Endpoint/views/Login/Login.jsx M
    app/views/Endpoint/views/Machines/ClustersView.jsx M
    app/views/Endpoint/views/Machines/MachinesView.tsx M
    app/views/Endpoint/views/Machines/ResourceBrokersView.jsx M
    app/views/Endpoint/views/Registration/panes/Recovery.jsx M
    app/views/Endpoint/views/Registration/panes/Trial.jsx M
    app/views/Endpoint/views/Registration/panes/Upload.jsx M
    app/views/Endpoint/views/UXConfig/View.jsx M
    app/views/Endpoint/views/UXViews/columns/icon\_apply.jsx M
    app/views/Endpoint/views/Users/components/PasswordField.tsx M
    app/views/Endpoint/widgets/NavFooterInfo.jsx R083
    app/views/Endpoint/widgets/NavHeaderEndpoint.jsx
    app/views/Endpoint/widgets/NavHeaderEndpoint.tsx M
    app/views/Endpoint/widgets/SoftErrors.jsx

    commit d97fa2e8ed1f100ac0e90881a8fd484de0eac419 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 21 11:59:46 2022
    -0500

    > refactor(jobs): migrate jobs columns and some monaco components to
    > typescript

    R056 app/monaco/FormattedElem.jsx app/monaco/FormattedElem.tsx R094
    app/monaco/languages.jsx app/monaco/languages.ts R074
    app/views/Endpoint/views/Jobs/columns/icon\_resource.jsx
    app/views/Endpoint/views/Jobs/columns/icon\_resource.tsx R072
    app/views/Endpoint/views/Jobs/columns/link\_machine.jsx
    app/views/Endpoint/views/Jobs/columns/link\_machine.tsx R071
    app/views/Endpoint/views/Jobs/columns/link\_uuid.jsx
    app/views/Endpoint/views/Jobs/columns/link\_uuid.tsx R084
    app/views/Endpoint/views/WorkOrders/components/NestedMachineApplyTable.jsx
    app/views/Endpoint/views/WorkOrders/components/NestedMachineApplyTable.tsx
    R079
    app/views/Endpoint/views/Workflows/components/NestedJobTable.jsx
    app/views/Endpoint/views/Workflows/components/NestedJobTable.tsx

    commit 92a608acc7f3199a16a51c2d24d54ef22d8730fc Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 14:49:43 2022
    -0500

    > refactor(params): migrate param editor to typescript

    M app/views/Endpoint/features/dependencies/reducer.ts M
    app/views/Endpoint/features/editor/hooks.ts R082
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamAddHeader.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamAddHeader.tsx
    R089
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.tsx
    R082
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorFieldParts.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorFieldParts.tsx
    R074
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamGroupHeader.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamGroupHeader.tsx
    R085 app/views/Endpoint/features/editor/tabs/ParamEditor/const.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/const.tsx R062
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/boolean.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/boolean.tsx
    R063
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/number.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/number.tsx
    R052
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/object.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/object.tsx
    R089
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/string.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/string.tsx
    R092 app/views/Endpoint/features/editor/tabs/ParamEditor/index.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/index.tsx R081
    app/views/Endpoint/features/editor/tabs/ParamEditor/util.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/util.ts

    commit 849803068e159f7d21c9f4a594e6b408a28b77dc Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 13:29:59 2022
    -0500

    > refactor: migrate some reducers to typescript

    R068 app/views/Endpoint/features/event\_tool/reducer.jsx
    app/views/Endpoint/features/event\_tool/reducer.ts R100
    app/views/Endpoint/features/event\_tool/selectors.jsx
    app/views/Endpoint/features/event\_tool/selectors.ts R090
    app/views/Endpoint/features/inbox/NavNotification.jsx
    app/views/Endpoint/features/inbox/NavNotification.tsx R061
    app/views/Endpoint/features/inbox/reducer.jsx
    app/views/Endpoint/features/inbox/reducer.ts R100
    app/views/Endpoint/features/inbox/selectors.jsx
    app/views/Endpoint/features/inbox/selectors.ts R054
    app/views/Endpoint/features/license/reducer.jsx
    app/views/Endpoint/features/license/reducer.ts R100
    app/views/Endpoint/features/license/selectors.jsx
    app/views/Endpoint/features/license/selectors.ts

    commit d58b7386c49f75d250079ef877ea6a62998c2484 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 11:54:42 2022
    -0500

    > fix(editor): add some comments, fix name required proptype

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx

    commit a27a244a98f87454ba314006d3b8683944ad1d32 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 10:52:32 2022
    -0500

    > refactor(machines): refactor machine editor and create editor to
    > typescript

    M app/api/DigitalRebar.ts A app/api/knownParams.ts M
    app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx R081
    app/views/Endpoint/features/editor/tabs/MetaEditor/index.jsx
    app/views/Endpoint/features/editor/tabs/MetaEditor/index.tsx M
    app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/views/Jobs/columns/link\_task.tsx R096
    app/views/Endpoint/views/Machines/MachineCreateEditor.jsx
    app/views/Endpoint/views/Machines/MachineCreateEditor.tsx R097
    app/views/Endpoint/views/Machines/MachineEditor.jsx
    app/views/Endpoint/views/Machines/MachineEditor.tsx M
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx
    R053 app/views/Endpoint/views/Machines/const.jsx
    app/views/Endpoint/views/Machines/const.tsx R071
    app/views/Endpoint/views/WorkOrders/columns/link\_uuid.jsx
    app/views/Endpoint/views/WorkOrders/columns/link\_uuid.tsx A
    app/views/Endpoint/views/WorkOrders/columns/link\_wo\_task.tsx M
    app/views/Endpoint/views/WorkOrders/components/NestedMachineApplyTable.jsx

    commit 1f1853d800d371c454dd11f3e5dd79f84c681d2e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 09:53:57 2022
    -0500

    > fix(racks): fix name collision check

    M app/views/Endpoint/views/Racks/CreateEditor.jsx

    commit a2e4812c445392d4504707802a013b350137d34c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 18 09:46:03 2022
    -0500

    > refactor(machines): migrate more columns to typescript

    M app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx R074
    app/views/Endpoint/views/Machines/columns/icon\_context.jsx
    app/views/Endpoint/views/Machines/columns/icon\_context.tsx R079
    app/views/Endpoint/views/Machines/columns/icon\_pool.jsx
    app/views/Endpoint/views/Machines/columns/icon\_pool.tsx M
    app/views/Endpoint/views/Machines/columns/link\_address.tsx R071
    app/views/Endpoint/views/Machines/columns/link\_workflow.jsx
    app/views/Endpoint/views/Machines/columns/link\_workflow.tsx M
    package.json

    commit 95fd4e497045b506e7a5c749dbcf97bc9b075ee6 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 17 18:34:50 2022 -0500

    > fix(triggers): flow editor style changes

    M app/views/Endpoint/features/editor/flow/components/FlowNode.tsx

    commit a5d469973ffc76e862b12afb82f3d6c78e2fb945 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 17 18:12:01 2022 -0500

    > feat(triggers): react flow editor prototype

    A app/views/Endpoint/features/editor/flow/components/FlowNode.tsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    package-lock.json M package.json

    commit aeddf003320bccb1243c3d5bfedefa37bce21fa1 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 17 15:50:19 2022
    -0500

    > lint(table): fix sortField typing

    M app/views/Endpoint/features/table\_view/util.tsx

    commit 253d7cd57f4b03195472859075289099a62b34dd Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 17 15:37:37 2022
    -0500

    > refactor(table): migrate table code to typescript

    M app/rootReducer.ts R069
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.jsx
    app/views/Endpoint/features/table\_view/columns/gen\_enum\_objects.tsx
    R096
    app/views/Endpoint/features/table\_view/components/Filter/CliDemo.jsx
    app/views/Endpoint/features/table\_view/components/Filter/CliDemo.tsx
    R080
    app/views/Endpoint/features/table\_view/components/Filter/FilterList.jsx
    app/views/Endpoint/features/table\_view/components/Filter/FilterList.tsx
    R097
    app/views/Endpoint/features/table\_view/components/Filter/SavePopup.jsx
    app/views/Endpoint/features/table\_view/components/Filter/SavePopup.tsx
    R093
    app/views/Endpoint/features/table\_view/components/Filter/Search.jsx
    app/views/Endpoint/features/table\_view/components/Filter/Search.tsx
    R100
    app/views/Endpoint/features/table\_view/components/Filter/const.jsx
    app/views/Endpoint/features/table\_view/components/Filter/const.ts
    R097
    app/views/Endpoint/features/table\_view/components/TableFilter.jsx
    app/views/Endpoint/features/table\_view/components/TableFilter.tsx
    R091
    app/views/Endpoint/features/table\_view/components/TableHead.jsx
    app/views/Endpoint/features/table\_view/components/TableHead.tsx
    R095
    app/views/Endpoint/features/table\_view/components/TablePaginate.jsx
    app/views/Endpoint/features/table\_view/components/TablePaginate.tsx
    M app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/selectors.tsx

    commit 4a9d6f4bf5a93ab1f8378125c945266ca31a0b6d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 17 14:03:43 2022 -0500

    > feat(triggers): add documentation tab

    M app/views/Endpoint/views/WorkOrders/TriggerProvidersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/components/TemplateTaskList.jsx

    commit 434d102172d95b9c9545ab2a21ac0d30d7606965 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 17 11:51:10 2022
    -0500

    > fix(login): fix login not showing loading when pressing enter

    M app/views/Endpoint/views/Login/Login.jsx

    commit 4dafb6c755b2c36fffe1683c0d7eed31e4cb56a5 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 16 16:03:17 2022
    -0500

    > fix(table): fix a redundant change made in the previous commit

    M app/views/Endpoint/features/table\_view/util.tsx

    commit ab8885ad5fb20d5195e8a902cc89aa086a2f42c0 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 16 15:57:26 2022
    -0500

    > refactor(table): refactor some table rendering code to typescript

    M app/api/offbrandjQuery.ts R098 app/views/Endpoint/Router.jsx
    app/views/Endpoint/Router.tsx R091
    app/views/Endpoint/features/table\_view/components/Table.jsx
    app/views/Endpoint/features/table\_view/components/Table.tsx M
    app/views/Endpoint/features/table\_view/context.tsx R080
    app/views/Endpoint/features/table\_view/util.js
    app/views/Endpoint/features/table\_view/util.tsx

    commit 74459459d58c78449f6238c4e24edf7857963f51 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 16 15:33:40 2022
    -0500

    > refactor(machines): refactor machine columns and nested tables to
    > typescript

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/dependencies/hooks.ts M
    app/views/Endpoint/features/dependencies/reducer.ts M
    app/views/Endpoint/features/dependencies/selectors.ts R087
    app/views/Endpoint/features/editor/tabs/ParamEditor/style.jsx
    app/views/Endpoint/features/editor/tabs/ParamEditor/style.tsx R074
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon.jsx
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon.tsx
    M app/views/Endpoint/features/table\_view/context.tsx R053
    app/views/Endpoint/views/Jobs/columns/link\_task.jsx
    app/views/Endpoint/views/Jobs/columns/link\_task.tsx R054
    app/views/Endpoint/views/Jobs/columns/text\_runTime.jsx
    app/views/Endpoint/views/Jobs/columns/text\_runTime.tsx R084
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.jsx
    app/views/Endpoint/views/Machines/columns/gen\_id\_name.tsx R069
    app/views/Endpoint/views/Machines/columns/link\_address.jsx
    app/views/Endpoint/views/Machines/columns/link\_address.tsx M
    app/views/Endpoint/views/Machines/components/ActivityIcon.tsx M
    app/views/Endpoint/views/Machines/components/NestedMachineTable.tsx
    R071
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.jsx
    app/views/Endpoint/views/Machines/components/NestedWorkOrderTable.tsx
    R091
    app/views/Endpoint/views/Machines/components/RequiredParamFields.jsx
    app/views/Endpoint/views/Machines/components/RequiredParamFields.tsx

    commit be52162397698bd731dff4cc4b9ad61b700f9a72 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 16 14:48:06 2022
    -0500

    > refactor(machines): machine editor typescript conversions

    M app/api/DigitalRebar.ts M app/api/database.ts M
    app/layout/Copy.tsx M
    app/views/Endpoint/features/documentation/help\_hover/index.tsx M
    app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx R074
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon\_id.jsx
    app/views/Endpoint/features/table\_view/columns/gen\_link\_icon\_id.tsx
    M app/views/Endpoint/views/Machines/columns/icon\_runnable.jsx R081
    app/views/Endpoint/views/Machines/components/ActivityIcon.jsx
    app/views/Endpoint/views/Machines/components/ActivityIcon.tsx R087
    app/views/Endpoint/views/Machines/components/BlueprintRunnerField.jsx
    app/views/Endpoint/views/Machines/components/BlueprintRunnerField.tsx
    R093 app/views/Endpoint/views/Machines/components/EditorFields.jsx
    app/views/Endpoint/views/Machines/components/EditorFields.tsx R084
    app/views/Endpoint/views/Machines/components/MachineEditorHeader.jsx
    app/views/Endpoint/views/Machines/components/MachineEditorHeader.tsx
    R087
    app/views/Endpoint/views/Machines/components/NestedMachineTable.jsx
    app/views/Endpoint/views/Machines/components/NestedMachineTable.tsx
    R084
    app/views/Endpoint/views/Machines/components/NestedProfileEditor.jsx
    app/views/Endpoint/views/Machines/components/NestedProfileEditor.tsx

    commit a85fd8de4effa9f4761653ab3a6ba81cb9d88fc8 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 16 11:14:25 2022
    -0500

    > refactor(table): convert tableview files to typescript, migrate
    > machines table

    M app/api/DigitalRebar.ts R072 app/layout/SideNav/SideNav.jsx
    app/layout/SideNav/SideNav.tsx R097 app/layout/SideNav/style.jsx
    app/layout/SideNav/style.tsx M app/views/Endpoint/Router.jsx M
    app/views/Endpoint/features/dependencies/hooks.ts M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/table\_view/components/BulkPanel.tsx
    R082 app/views/Endpoint/features/table\_view/components/Buttons.jsx
    app/views/Endpoint/features/table\_view/components/Buttons.tsx R091
    app/views/Endpoint/features/table\_view/components/TableRow.jsx
    app/views/Endpoint/features/table\_view/components/TableRow.tsx R084
    app/views/Endpoint/features/table\_view/components/TableView.jsx
    app/views/Endpoint/features/table\_view/components/TableView.tsx M
    app/views/Endpoint/features/table\_view/context.tsx R095
    app/views/Endpoint/views/Machines/MachinesView.jsx
    app/views/Endpoint/views/Machines/MachinesView.tsx M
    app/views/Endpoint/views/Machines/actions.tsx M
    app/views/Endpoint/views/Machines/columns/link\_icon\_tasks.jsx R090
    app/views/Endpoint/views/Machines/components/ActionDropdown.jsx
    app/views/Endpoint/views/Machines/components/ActionDropdown.tsx R086
    app/views/Endpoint/views/Machines/hooks/usePluginActions.jsx
    app/views/Endpoint/views/Machines/hooks/usePluginActions.tsx M
    package-lock.json M package.json

    commit ad699def274b16fa7ba2d79b0c187281a121a5d2 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 15 15:32:29 2022
    -0500

    > refactor(table): bulk actions refactor to typescript

    M app/api/DigitalRebar.ts M
    app/views/Endpoint/features/editor/components/SearchDropdown.tsx
    R078
    app/views/Endpoint/features/table\_view/components/BulkPanel.jsx
    app/views/Endpoint/features/table\_view/components/BulkPanel.tsx M
    app/views/Endpoint/features/table\_view/context.tsx M
    app/views/Endpoint/features/table\_view/hooks.tsx R085
    app/views/Endpoint/views/Machines/actions.jsx
    app/views/Endpoint/views/Machines/actions.tsx

    commit ece4343ec96e86b6d1169481e7cf9fe12926e104 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 15 13:53:53 2022
    -0500

    > refactor(editor): convert SearchDropdown to typescript, finishing
    > editor typescript refactor

    M app/views/Endpoint/features/dependencies/reducer.ts M
    app/views/Endpoint/features/editor/components/EditorFields.tsx R076
    app/views/Endpoint/features/editor/components/SearchDropdown.jsx
    app/views/Endpoint/features/editor/components/SearchDropdown.tsx

    commit d022bd5a638ef63310336e481f6bc85a4d0ff7db Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 15 12:38:00 2022
    -0500

    > refactor(table): migrate to typescript

    M app/api/DigitalRebar.ts R083
    app/views/Endpoint/features/table\_view/actions.jsx
    app/views/Endpoint/features/table\_view/actions.tsx R070
    app/views/Endpoint/features/table\_view/context.jsx
    app/views/Endpoint/features/table\_view/context.tsx R092
    app/views/Endpoint/features/table\_view/hooks.jsx
    app/views/Endpoint/features/table\_view/hooks.tsx R064
    app/views/Endpoint/features/table\_view/reducer.jsx
    app/views/Endpoint/features/table\_view/reducer.tsx D
    app/views/Endpoint/features/table\_view/selectors.jsx A
    app/views/Endpoint/features/table\_view/selectors.tsx

    commit 7b47dad9f63ff10cde13d08b7e54bd74de0666bb Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 15 10:59:42 2022
    -0500

    > refactor(editor): add default types to withEditorField

    D app/views/Endpoint/features/editor/components/EditPanel.jsx M
    app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx R091
    app/views/Endpoint/features/editor/components/nameCollision.jsx
    app/views/Endpoint/features/editor/components/nameCollision.tsx M
    app/views/Endpoint/features/editor/context.tsx

    commit 216e47e555b00db4027dca4d1af609a2378ed460 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 14 13:09:51 2022 -0500

    > fix(editor): show bundle for editor objects

    M app/views/Endpoint/features/editor/components/EditorParts.tsx D
    app/views/Endpoint/features/editor/components/HeaderInfo.jsx A
    app/views/Endpoint/features/editor/components/HeaderInfo.tsx M
    app/views/Endpoint/views/Machines/components/MachineEditorHeader.jsx

    commit b7f8e3524c8bc17ce70af973e3827a40c23d156d Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 14 12:48:15 2022 -0500

    > feat(jobs): add line numbers to logs

    M app/views/Endpoint/views/Jobs/components/JobLog.jsx

    commit bf1180069bd6031180416820483d70e5f808b7e5 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 14 11:48:47 2022 -0500

    > fix(catalog): add misc category for untagged catalog items

    M app/api/index.tsx M app/views/Endpoint/views/Catalog/Catalog.jsx M
    app/views/Endpoint/views/Catalog/ToggleOptions.jsx

    commit a6087d1df99729866b18ffa7f5af177c9f8146a5 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 14 11:22:37 2022 -0500

    > fix(editor): more documentation fixes

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/job.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/profiles.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/tasks.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/workflows.tsx
    M app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/table\_view/hooks.jsx M
    app/views/Endpoint/views/BootEnvs/Editor.jsx M
    app/views/Endpoint/views/Raid/components/VolumesField.jsx

    commit 61955cde131625a5efeddb5cc8948aad45467d6d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 10:30:34 2022
    -0500

    > fix(raid): fix raid editor not working when fields are missing

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/views/Raid/components/VolumesField.jsx

    commit 4d7ae494dc340ec9bdf1bc9f7a9167d0559d9a1f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 10:30:34 2022
    -0500

    > fix(raid): fix raid editor not working when fields are missing

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/views/Raid/components/VolumesField.jsx

    commit b5ac7132a936b5db328759647c828a0d164e58fd Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 10:08:23 2022
    -0500

    > fix(work\_orders): add bundle to wo/bp/trigger tables, add
    > documentation tab to version sets

    M app/views/Endpoint/views/VersionSets/Editor.jsx M
    app/views/Endpoint/views/VersionSets/View.jsx M
    app/views/Endpoint/views/WorkOrders/BlueprintsView.jsx M
    app/views/Endpoint/views/WorkOrders/TriggerProvidersView.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersView.jsx

    commit 504165fec7cad2f8955c3dd7910d1632e87e9224 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 10:04:54 2022
    -0500

    > fix(work\_orders): fix help text

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/work\_orders.tsx

    commit 65edddde5be0b300644445270cc39ff5d9d2e6f7 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 09:56:48 2022
    -0500

    > fix(resources): update clusters and resource brokers help area

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx

    commit a8a0225b72f4f1bd8d1e56ef68e0c7ccc98d5b91 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 14 09:52:00 2022
    -0500

    > fix(resources): fix pipelines hover help

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx
    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/views/Machines/components/EditorFields.jsx

    commit 4abbcb964f030e9ea2e94513367210afac13be33 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 14:01:01 2022
    -0600

    > fix: remove debug log

    M app/views/Endpoint/features/editor/components/EditorParts.tsx

    commit 5612ba0537abd5b22a5ccfdb3b69c5cbf4c8c86d Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 13:47:18 2022
    -0600

    > docs: add help for jobs, version\_sets, work\_orders, and
    > endpoints

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/endpoints.tsx
    A
    app/views/Endpoint/features/documentation/help\_hover/objects/job.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/ux\_views.tsx
    A
    app/views/Endpoint/features/documentation/help\_hover/objects/version\_sets.tsx
    A
    app/views/Endpoint/features/documentation/help\_hover/objects/work\_orders.tsx
    M app/views/Endpoint/features/editor/components/EditorParts.tsx

    commit 88f7094ecd2827af8337c5e913e9fc2e7d5d0769 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 13:26:20 2022
    -0600

    > fix(uxviews): remove deprecated Machinefields input

    M app/views/Endpoint/views/UXViews/CreateEditor.jsx M
    app/views/Endpoint/views/UXViews/Editor.jsx

    commit 9244ba81c122fe8ac36ed152b37288e9c7100426 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 12:08:34 2022
    -0600

    > refactor(users): move password field into separate file

    M app/api/index.tsx M app/views/Endpoint/views/Users/Editor.jsx A
    app/views/Endpoint/views/Users/components/PasswordField.tsx

    commit b78a262731eca78dc99fdd5298e4d0b4bb2986f2 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 12:05:03 2022
    -0600

    > fix(editor): labels that can be clicked have pointer cursors

    M app/views/Endpoint/features/editor/components/EditorParts.tsx

    commit 0659bbac6c9c85be462f8785c54674fb2528884b Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 12:02:57 2022
    -0600

    > fix(help): only show object help on the editor tab

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/views/BootEnvs/Editor.jsx M
    app/views/Endpoint/views/Contexts/Editor.jsx M
    app/views/Endpoint/views/Endpoints/Editor.jsx M
    app/views/Endpoint/views/IdentityProviders/Editor.jsx M
    app/views/Endpoint/views/Jobs/Editor.jsx M
    app/views/Endpoint/views/Leases/Editor.jsx M
    app/views/Endpoint/views/Machines/ClusterEditor.jsx M
    app/views/Endpoint/views/Machines/MachineEditor.jsx M
    app/views/Endpoint/views/Machines/ResourceBrokerEditor.jsx M
    app/views/Endpoint/views/Params/Editor.jsx M
    app/views/Endpoint/views/Pipelines/Editor.jsx M
    app/views/Endpoint/views/Plugins/Editor.jsx M
    app/views/Endpoint/views/Pools/Editor.jsx M
    app/views/Endpoint/views/Profiles/Editor.jsx M
    app/views/Endpoint/views/Racks/Editor.jsx M
    app/views/Endpoint/views/Raid/Editor.jsx M
    app/views/Endpoint/views/Reservations/Editor.jsx M
    app/views/Endpoint/views/Roles/Editor.jsx M
    app/views/Endpoint/views/Stages/Editor.jsx M
    app/views/Endpoint/views/Subnets/Editor.jsx M
    app/views/Endpoint/views/Tasks/Editor.jsx M
    app/views/Endpoint/views/Tenants/Editor.jsx M
    app/views/Endpoint/views/UXViews/Editor.jsx M
    app/views/Endpoint/views/Users/Editor.jsx M
    app/views/Endpoint/views/VersionSets/Editor.jsx M
    app/views/Endpoint/views/WorkOrders/BlueprintEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggerProvidersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx M
    app/views/Endpoint/views/WorkOrders/WorkOrdersEditor.jsx M
    app/views/Endpoint/views/Workflows/Editor.jsx

    commit c9347d01074380c7e5a3fbac77472b1f2e5050f0 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 11:39:51 2022
    -0600

    > fix(nav): add racks back, update copyright year

    M app/config/menu.ts M
    app/views/Endpoint/widgets/NavFooterLicense.jsx M
    app/views/Home/components/FooterLinks.jsx

    commit 90f6d218f737a4e8f2068a900b9fb1d9fc3e6e26 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Fri Mar 11 11:37:07 2022
    -0600

    > docs(uxviews): add help hovers

    M app/config/menu.ts M
    app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/ux\_views.tsx
    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/ux\_views/Nav.jsx M
    app/views/Endpoint/views/UXViews/CreateEditor.jsx M
    app/views/Endpoint/views/UXViews/Editor.jsx

    commit 530dcddae0f4da7f161ce678c0f10fe5168627ae Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 10 15:53:41 2022
    -0600

    > docs(identity\_providers): add help for identity providers

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/identity\_providers.tsx
    M app/views/Endpoint/views/IdentityProviders/Editor.jsx

    commit 0c48fd0e5121d1a3f63a17f0e57c44865de3b3c4 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 10 15:47:40 2022
    -0600

    > docs(subnets): add help for subnets

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/subnets.tsx
    M app/views/Endpoint/views/Subnets/CreateEditor.jsx M
    app/views/Endpoint/views/Subnets/Editor.jsx

    commit 0c056e421c1e4b374c5680778f568af46a8cac12 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 10 15:39:15 2022
    -0600

    > docs(blueprints): add help for blueprints

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/blueprints.tsx

    commit 55cd40fd819545950280001764787d2d74bdc21f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 10 15:34:11 2022
    -0600

    > feat(jobs): add exit state field

    M app/views/Endpoint/views/Jobs/Editor.jsx

    commit c15f433db220ae35ae6c27190337c18ee2bf5d94 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 10 09:50:54 2022
    -0600

    > fix(classifier): bodge fix for classifier

    M app/views/Endpoint/components/GenericListView.jsx M
    app/views/Endpoint/views/Classifier.jsx

    commit 0bf89b00c511a6110dc146301fc2530c707a3714 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 9 14:02:40 2022
    -0600

    > feat(editor): allow nested profile editors to preview any profile

    M app/views/Endpoint/features/editor/tabs/NestedEditor/index.jsx M
    app/views/Endpoint/views/Machines/components/NestedProfileEditor.jsx

    commit ef680e2c3d1cc368048f42ad8b85a97063156d20 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 8 14:13:38 2022
    -0600

    > fix(inbox): increase contrast for unread inbox messages

    M app/views/Endpoint/views/Inbox/Inbox.jsx

    commit 895b2afbefb26da5cc3782167aaad0539833c9d0 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 8 10:16:47 2022
    -0600

    > fix(editor): make create editors full width

    M app/views/Endpoint/features/editor/components/EditorParts.jsx M
    app/views/Endpoint/views/Machines/MachineCreateEditor.jsx M
    app/views/Endpoint/views/Templates/CreateEditor.jsx

    commit a63a1063e75ec5f336ec2cc02fe4f1cb37cd3bdf Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 8 10:09:46 2022
    -0600

    > chore: fix typo in imports, remove jquery dep

    M
    app/views/Endpoint/views/IdentityProviders/components/GroupToRolesField.jsx
    M app/views/Endpoint/views/Racks/components/ValidationToMachine.jsx
    M app/views/Endpoint/views/Racks/components/ValidationToParams.jsx M
    app/views/Endpoint/views/Raid/components/VolumesField.jsx M
    app/views/Endpoint/views/Stages/components/TemplatesEditor.jsx M
    app/views/Endpoint/views/Tenants/components/MembershipEditor.jsx M
    app/views/Endpoint/views/VersionSets/components/ComponentsTable.jsx
    M package-lock.json M package.json

    commit 3dd15341d58bd04299fea4bbc00edf6a0912802f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 16:40:00 2022 -0600

    > feat(tenants): add help fields to tenants

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/tenants.tsx
    M app/views/Endpoint/views/Pools/CreateEditor.jsx M
    app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx

    commit a2388509de0a64c43743dcd8a815e2d06621a824 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 16:26:25 2022 -0600

    > feat(trigger\_providers): add help fields to trigger providers

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/trigger\_providers.tsx

    commit ada3459c86ff15948854fb376fd8754abe0b297f Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 16:17:54 2022 -0600

    > fix(clusters, brokers): show correct object help areas

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx M
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx

    commit 96d55ae085c0c441a038bad295ad3a6bb7809fa5 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 16:03:14 2022 -0600

    > fix(leases): field help names

    M app/views/Endpoint/views/Leases/Editor.jsx

    commit d61da73f8aedb6c4adc05690e290d7b1907883c3 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 16:00:47 2022 -0600

    > fix(editor): CSS changes

    M app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorFieldParts.jsx

    commit 0fca30ad334ed3718f6505b41a75da8ab965fd7e Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 15:59:49 2022 -0600

    > feat(triggers): add MergeDataIntoParams help field

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx

    commit d0fb6615eaf5d6e90b16a50e4a3cbff2f0a6f3c4 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 15:58:53 2022 -0600

    > feat(leases): add help fields to leases

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/leases.tsx

    commit bc0deeef6790768fca885bd43ece23f537017301 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 7 15:53:51 2022
    -0600

    > fix(triggers): fix a typo in Store Data field

    M app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx

    commit 9d180695b200f4ac029b1fea0cec407bef3e169c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Mar 7 15:24:34 2022 -0600

    > feat(pools): add help fields to pools

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/pools.tsx

    commit bdf05fdaa1c63024de3efe2219f60ebf70ffcb4e Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 7 15:17:31 2022
    -0600

    > fix(pools): fix create using incorrect type and id
    >
    > Signed-off-by: Isaac Hirschfeld \<<gpg@reheatedcake.io>\>

    M app/views/Endpoint/views/Pools/CreateEditor.jsx

    commit 4859e7dd3955810b3361f4a38eea15d90e32d77c Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Mon Mar 7 09:46:42 2022
    -0600

    > feat(params): \'cron\' render type for params

    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/components/ParamEditorField.jsx
    M
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/string.jsx
    M app/views/Endpoint/views/Params/CreateEditor.jsx M
    app/views/Endpoint/views/Params/Editor.jsx M
    app/views/Endpoint/views/Params/constants.jsx D
    app/views/Endpoint/views/WorkOrders/components/CronField.jsx A
    app/views/Endpoint/views/WorkOrders/components/CronField.tsx

    commit df43e40943808307344db99f9618f067513f1e38 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 17:42:04 2022 -0600

    > fix(stages): remove deprecated RunnerWait field

    M app/views/Endpoint/views/Stages/CreateEditor.jsx M
    app/views/Endpoint/views/Stages/Editor.jsx

    commit 0a8f3b00728ff4b872c5925fa6e0ed51c07f132c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 17:40:56 2022 -0600

    > feat(roles): add field help to roles

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/roles.tsx

    commit 734c674aeb107ae47cffc7c8c08ac16e54cba931 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 16:36:47 2022 -0600

    > feat(tasks): add field help to tasks

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/tasks.tsx

    commit a29aad2afa3504415f120b9e3660cb57e36d03c8 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 16:32:07 2022 -0600

    > feat(stages): add field help to stages

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/stages.tsx

    commit 0719f870f544fb11ba3c5b488ac7f0f67c6ef6be Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 16:22:24 2022 -0600

    > feat(workflows): add field help to workflows

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/workflows.tsx

    commit 8b51012f853fe9af472878f42f432287b28e9e7f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 3 14:06:48 2022
    -0600

    > refactor(help): import with type to avoid recursive import

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/bootenvs.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/contexts.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/params.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/profiles.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/reservations.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx
    M
    app/views/Endpoint/features/documentation/help\_hover/objects/users.tsx

    commit 402cafaa8331de9056d694c811f3fe6448b7d3f9 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 14:02:27 2022 -0600

    > feat(reservations): add help fields to reservations

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/reservations.tsx

    commit 3fc63213d729fbb9879674a6f9b7430a8dc2e836 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Thu Mar 3 13:53:01 2022 -0600

    > feat(editor): string help fields parse as markdown

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx

    commit 09cf5bd54d007ffa0e7b13edffdf953f8055d6ef Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 3 13:17:09 2022
    -0600

    > refactor(editor): refactor EditorFields to typescript

    R063 app/monaco/InlineJSONMonacoEditor.jsx
    app/monaco/InlineJSONMonacoEditor.tsx R071
    app/monaco/InlineMonacoEditor.jsx app/monaco/InlineMonacoEditor.tsx
    D app/monaco/hooks.jsx A app/monaco/hooks.ts M
    app/views/Endpoint/features/dependencies/hooks.ts R072
    app/views/Endpoint/features/editor/components/EditorFields.jsx
    app/views/Endpoint/features/editor/components/EditorFields.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/editor/hooks.ts M
    app/views/Endpoint/features/editor/tabs/ParamEditor/fieldTypes/object.jsx

    commit a5ffe0dea349f668936781943b40e47fbddb692a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Thu Mar 3 12:21:59 2022
    -0600

    > refactor(editor): migrate editorparts to typescript

    M app/api/DigitalRebar.ts M app/layout/Flex.tsx R074
    app/views/Endpoint/features/editor/components/EditorParts.jsx
    app/views/Endpoint/features/editor/components/EditorParts.tsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/editor/hooks.ts M
    app/views/Endpoint/views/Machines/components/MachineEditorHeader.jsx

    commit 86f8132a29526ad4e1bff82290e4e6f7b33cb242 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 16:59:05 2022 -0600

    > fix(triggers): StoreDataInParameter field using wrong name

    M app/views/Endpoint/views/WorkOrders/TriggersEditor.jsx

    commit a71bf8beb9d3d8b276fafdc9bb2b3e29c7d6cbde Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 16:55:35 2022 -0600

    > feat(triggers): add help fields for triggers

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/triggers.tsx

    commit 18177bf85efa226255f1280197d17de3249004b0 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 16:43:22 2022 -0600

    > feat(bootenvs): add help fields for bootenvs

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/bootenvs.tsx

    commit ba9d863ef9bdb2ddbcddf3110362d1953614bbc7 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 16:05:36 2022 -0600

    > feat(contexts): add more help fields to contexts

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/contexts.tsx

    commit 6d0bc90fabc7f4d9e77220d3a086ea19f60af0c9 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 15:57:42 2022 -0600

    > feat(params): add more help fields to params

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/params.tsx

    commit 1b9751b694ef9428a9e0357e78dfe5c9f4469224 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 15:57:02 2022 -0600

    > fix(editor): help popup hoverable, style change

    M app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit 1af7de2106aa588bb62de5b25f03491e05d1eac2 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Wed Mar 2 15:50:19 2022 -0600

    > feat(profiles): add more help fields to profiles

    M app/views/Endpoint/features/documentation/help\_hover/index.tsx A
    app/views/Endpoint/features/documentation/help\_hover/objects/profiles.tsx

    commit 9a1300e3f38e8c8b80a09c34cc49a7c959fd4841 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 2 14:58:10 2022
    -0600

    > fix(trial): use different recaptcha library

    M app/config/cloudia.ts M app/css/style.css M
    app/views/Endpoint/views/Registration/panes/Trial.jsx M
    package-lock.json M package.json

    commit 8a2745bd0a992ebf1ec40e6882842d70c2138e1a Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Wed Mar 2 10:34:36 2022
    -0600

    > fix: replace deprecated yaml safeLoad call with load to fix
    > license uploading

    M app/views/Endpoint/views/Catalog/Catalog.jsx M
    app/views/Endpoint/views/License/components/UploadModal.jsx M
    app/views/Endpoint/views/Registration/panes/Upload.jsx

    commit c2cf8ce6cb1fc649691ecd132f991b100cb1dad2 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Tue Mar 1 22:11:33 2022 -0600

    > doc: tweak

    M README.md

    commit ff7d65d1ca4bacbf51ac57e82c1c0721b5e880b5 Author: Greg Althaus
    \<<galthaus@austin.rr.com>\> Date: Tue Mar 1 21:22:31 2022 -0600

    > Release v4.10.0 Initial

    M README.md

    commit 8c289f8f9e21559f2e22dbd7ae632b2a5556487f Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 1 14:06:07 2022
    -0600

    > feat(machines): add more help fields to machines

    M
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx
    M app/views/Endpoint/views/Machines/MachineEditor.jsx M
    app/views/Endpoint/views/Machines/components/EditorFields.jsx

    commit ec9730931046721e47ecfdf85aa7c39acb126481 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 13:51:36 2022 -0600

    > fix(editor): move selector out of EditorBody
    >
    > also, renames ACE Editor -\> Monaco Editor

    M app/config/settings.ts M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit b1b762ac976adba6738a2d8bfe696785d6dee236 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 13:47:18 2022 -0600

    > feat(editor): add ux config option for object help

    M app/config/settings.ts M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit 4757a8c0a87b55be09cd3061105da7a93df05ad7 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 12:54:06 2022 -0600

    > fix(editor): help icon and React.FC in HelpContent

    R079 app/views/Endpoint/features/documentation/help\_hover/index.ts
    app/views/Endpoint/features/documentation/help\_hover/index.tsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit 0830ba6d857e7630408d16d8aca6833801b98886 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 12:21:49 2022 -0600

    > feat(editor): object and field help can be specified as functions

    M app/views/Endpoint/features/documentation/help\_hover/index.ts A
    app/views/Endpoint/features/documentation/help\_hover/objects/users.tsx

    commit 5fb367aa0fa1cc6681945bd7d30a588638ccfa79 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 11:56:11 2022 -0600

    > feat(editor): optional object help areas

    M app/views/Endpoint/features/documentation/help\_hover/index.ts M
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx
    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx M
    app/views/Endpoint/features/editor/context.tsx M
    app/views/Endpoint/features/table\_view/context.jsx

    commit f3cfe4b5053f4191e69fa618475937670ef55d19 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 11:02:40 2022 -0600

    > fix(editor): field help area semantics

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit e876afcd6ad83d6a7fa1bb488f08e1fa1661e916 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Tue Mar 1 10:54:40 2022 -0600

    > feat(editor): add close button to field help areas

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit dd3e43e52a3fc4d5597e7beebe89f18951fba582 Author: Isaac
    Hirschfeld \<<gpg@reheatedcake.io>\> Date: Tue Mar 1 10:09:51 2022
    -0600

    > feat(editor): allow help tooltips to be sourced from a feature
    > folder

    A app/views/Endpoint/features/documentation/help\_hover/index.ts A
    app/views/Endpoint/features/documentation/help\_hover/objects/machines.tsx
    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx M
    app/views/Endpoint/features/editor/hooks.ts M
    app/views/Endpoint/views/Machines/MachineEditor.jsx

    commit 8164851f5eb5e0bd79d95a10a63035ed8405664c Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Feb 28 17:52:11 2022 -0600

    > feat(machines): basic field help

    M app/views/Endpoint/features/editor/tabs/MetaEditor/index.jsx M
    app/views/Endpoint/views/Machines/MachineEditor.jsx

    commit 739493740f10962ada669fbb8f493a782b83b755 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Feb 28 17:46:42 2022 -0600

    > feat(editor): field help, helpArea should coexist

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    commit b8113b9a8458055a27897f9743b610afe45b72c0 Author: Zander
    Franks \<<zander@zanderf.net>\> Date: Mon Feb 28 17:28:11 2022 -0600

    > feat(editor): field help, helpArea props

    M app/views/Endpoint/features/editor/components/EditorFields.jsx M
    app/views/Endpoint/features/editor/components/EditorParts.jsx

    End of Note

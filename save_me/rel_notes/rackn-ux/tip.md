ttons.tsx

:   M app/views/Endpoint/views/WorkOrders/WorkOrdersView.jsx A
    app/views/Endpoint/views/WorkOrders/components/ReworkButton.tsx

    commit 2043aa2c033161074537b33de9912365f1c97c4b Author: Rob
    Hirschfeld \<<rob@rackn.com>\> Date: Tue Jul 12 12:46:08 2022 -0500

    > feat(work\_orders): add a resubmit button to the work\_orders view
    > uses selected wo to creata a new WO

    M app/views/Endpoint/features/table\_view/components/Buttons.tsx M
    app/views/Endpoint/views/WorkOrders/WorkOrdersView.jsx

    End of Note

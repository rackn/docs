v4.7.11 {#rs_rel_notes_rackn-ux_v4.7.11}
=======

>     commit aa1c55bb468fb0dfadb28353f66f4ae24dc4ebb2
>     Author: Isaac Hirschfeld <gpg@reheatedcake.io>
>     Date:   Fri Feb 4 09:25:41 2022 -0600
>
>         fix(racks): fix racks view mutating redux state
>
>     M   app/views/Endpoint/views/Racks/Panel.jsx
>
>     End of Note

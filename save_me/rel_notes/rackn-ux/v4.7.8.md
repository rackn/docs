v4.7.8 {#rs_rel_notes_rackn-ux_v4.7.8}
======

>     commit c1d38e49dce612bf7999ae4e40d767e3e7bdd707
>     Author: Meshiest <gpg@reheatedcake.io>
>     Date:   Mon Oct 4 10:24:39 2021 -0500
>
>         fix(ux_view): fix missing inbox on ux views with empty menus
>
>     M   app/views/Endpoint/features/ux_views/Nav.jsx
>     M   app/views/Endpoint/features/ux_views/reducer.jsx
>     M   app/views/Endpoint/views/UXViews/Panel.jsx
>     M   app/views/Endpoint/views/UXViews/columns/icon_apply.jsx
>
>     commit 43aec614a670f7ccfc0582c027d193450b4831dc
>     Author: Meshiest <gpg@reheatedcake.io>
>     Date:   Mon Oct 4 10:45:27 2021 -0500
>
>         feat(search): better keybinds (ctrl+shift+f), hiding menu on search, clear with space
>
>     M   app/views/Endpoint/features/table_view/components/Filter/Search.jsx
>
>     End of Note

v1.8.9 {#rs_rel_notes_rackn-ux_v1.8.9}
======

>     commit a3f7bb6abf95caaa84dd820b18740b3f4e1e55b0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 1 17:26:57 2019 -0500
>
>         Fix lint
>
>     M   app/api/index.jsx
>     M   app/views/Endpoint/Content.jsx
>
>     commit 3a9a6735f363510a5f5ab523dfd14370f7b01a5c
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 1 16:59:34 2019 -0500
>
>         Update version checking to start to work with v4
>
>     M   app/api/index.jsx
>
>     commit a983833565859a24615f9e4d1287927570ac8447
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Thu Aug 1 16:33:38 2019 -0500
>
>         Fix broken profiles edit
>
>     M   app/views/Endpoint/GenericListView.jsx
>
>     commit 5aa1b9d9a7b804edf92794c527db411d3b1e2a9e
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Thu Aug 1 13:59:47 2019 -0500
>
>         potentially fix login issue
>
>     M   app/views/Endpoint/index.jsx
>
>     commit 2a04017890f435d07879da7506811044294051e2
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Thu Aug 1 13:02:05 2019 -0500
>
>         Change click value to click button for toggle visibility
>
>     M   app/views/Endpoint/Params.jsx
>
>     End of Note

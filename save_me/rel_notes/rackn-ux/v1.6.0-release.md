v1.6.0 {#rs_rel_notes_rackn-ux_v1.6.0}
======

>     commit 42e51bbbaba49d66c3ee1a2b34d46e68676a61f5
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Nov 29 19:16:02 2018 -0600
>
>         streamline list
>
>     M   app/views/Catalog/List.jsx
>
>     commit d4f82dd068ad7f7758a21fc9fe945031f7bc9379
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Nov 29 19:02:37 2018 -0600
>
>         remove price data from catalog
>
>     M   app/views/Catalog/List.jsx
>
>     commit b36b6b51da9fbf4812c3b5074b3cfe1510827301
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Nov 13 19:38:17 2018 -0600
>
>         use file name even if not a known ISO
>
>     M   app/views/Endpoint/Isos.jsx
>
>     commit 64a6524cd82f54bb970e1cf00deeaaf00261fcf3
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Oct 31 16:37:43 2018 -0500
>
>         add filter by machine button to jobs
>
>     M   app/views/Endpoint/GenericListView.jsx
>     M   app/views/Endpoint/Jobs.jsx
>
>     commit 4fdbd03d3299edc41b7256fabf24bd09f31061a0
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Oct 31 14:59:33 2018 -0500
>
>         improve column layout - remove bad transition concept
>
>     M   app/layout/Table.jsx
>     M   app/views/Endpoint/Jobs.jsx
>
>     commit bd964d9efa5d5aa09267433b08748ef5a95c1b1c
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Oct 31 13:46:31 2018 -0500
>
>         make column layout more intuitive, change log info for transitions
>
>     M   app/views/Endpoint/Jobs.jsx
>
>     commit aa79ae0a3f735227cef71da6b82209002c6981e6
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 18 16:20:41 2018 -0500
>
>         allow closing stages tray on workflow page, remove selected behavior since it's now in the item
>
>     M   app/views/Endpoint/Workflows.jsx
>
>     commit 3e9a012fc007c3080a8dd4c462ffdbe386069bf6
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 18 11:15:01 2018 -0500
>
>         allow editing stages in workflow
>
>     M   app/views/Endpoint/Workflows.jsx
>
>     commit d1ab31b8c21eb8beb4470a5a1a802713aa118349
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 18 10:27:42 2018 -0500
>
>         improve stages rendering:
>
>     M   app/views/Endpoint/Workflows.jsx
>
>     commit fa21df6ce8fdbab0cfe22a34769ed7123c9e84e6
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Oct 17 14:29:23 2018 -0500
>
>         use machine.Status to tell if pool activity is in process
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit e5a82d82878812b27879f1cdec42cd2a4444c303
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Wed Oct 17 12:05:59 2018 -0500
>
>         allocate/return should have individual workflows, fix split bug
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit 7e54cf5cc4653fef75de06880218bca6aa8061fa
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Oct 16 11:53:34 2018 -0500
>
>         do not force refresh anymore
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit fc52e223e0c4797ffa00fc1ccdb105928863dddc
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Tue Oct 16 11:45:44 2018 -0500
>
>         add allocate machines if pool matches workflow name
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit 1723108d24c3decc3e8c5b490bbaa2818e7a1ed0
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Oct 13 14:50:22 2018 -0500
>
>         fix pool change not caught, provide remove/release actions
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit 80b87092793658d090ceb31294c933f5107d6668
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Oct 12 11:52:46 2018 -0500
>
>         Add filter to not crash on the arch support.
>
>     M   app/views/Endpoint/BootEnvs.jsx
>
>     commit f3511ff6695d8afeec48062c28843aebc56d979e
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 11 18:08:30 2018 -0500
>
>         move pool section to top of page
>
>     M   app/views/Endpoint/Overview.jsx
>
>     commit 99efc26c4f79a83d8429bfa949df0f7d5ef03008
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Oct 11 18:05:07 2018 -0500
>
>         show pools status on overview page
>
>     M   app/api/index.jsx
>     M   app/views/Endpoint/Overview.jsx
>
>     commit 0838e105e24fc63942f84fba4b2186f60b3e6198
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Sat Sep 29 18:34:44 2018 -0500
>
>         show SAML errors
>
>     M   app/views/index.jsx
>
>     commit af4e61d2ceb8be61307065f376f5b48302c56a8e
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Sep 27 11:01:06 2018 -0500
>
>         fix linter
>
>     M   app/layout/index.jsx
>
>     commit a8454ad602a8b72e0d17128645bfb226e06247e5
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Sep 27 10:47:35 2018 -0500
>
>         catch and use cognito tokens in URL
>
>     M   app/api/Cognito.jsx
>     M   app/api/index.jsx
>     M   app/layout/index.jsx
>     M   app/views/index.jsx
>     M   package.json
>
>     commit 80741bc2e95dee507a8a8aa58825024615c92333
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Sep 26 15:08:30 2018 -0500
>
>         Allow reservation time to be placed into the UX object.
>
>     M   app/views/Endpoint/Reservations.jsx
>
>     commit e015b2c323447367288b0c3e01aafbc4595670f9
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Sep 26 14:24:49 2018 -0500
>
>         Fix an issue with loading pieces incrementally.
>
>     M   app/views/Endpoint/Machines.jsx
>
>     commit 9583bb8ec56d95b3022b6b302ebaf6e7dc137185
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Sep 26 14:12:08 2018 -0500
>
>         Address PR #866 in the DRP tree
>
>         Add links to bootenvs in the various machines and stages pages.
>
>     M   app/views/Endpoint/Machines.jsx
>     M   app/views/Endpoint/Stages.jsx
>
>     commit 9d65e253547b4e8b7d5b94c7b446fcc2602eafb2
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Sep 17 17:25:49 2018 -0500
>
>         missed protecting .versions in new code for legacy path
>
>     M   app/views/Endpoint/Content.jsx
>
>     commit d67c32d334197862c7fcda47635235636f6edcee
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Sep 13 15:55:18 2018 -0500
>
>         WIP accept redirect URL from Cognito
>
>     M   app/render.jsx
>     M   app/views/User/Login.jsx
>     M   app/views/index.jsx
>     M   package.json
>
>     End of Note

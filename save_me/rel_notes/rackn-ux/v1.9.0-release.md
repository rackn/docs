v1.9.0 {#rs_rel_notes_rackn-ux_v1.9.0}
======

>     commit e029324456b4010a54b93da33cba558bf228879b
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 10:46:55 2019 -0500
>
>         Support older versions of ux views
>
>     M   app/api/index.jsx
>     M   app/views/Endpoint/UXViews.jsx
>     M   app/views/Endpoint/index.jsx
>
>     commit 777deef45b89e3d2c4e4160c475f8aa5460bb07b
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 10:14:21 2019 -0500
>
>         Fix preview in uxviews table
>
>     M   app/views/Endpoint/UXViews.jsx
>     M   app/views/Endpoint/index.jsx
>
>     commit 8dee8fdf9b0588f7d7bf9a9be0c69e4344661aba
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 10:10:51 2019 -0500
>
>         Session storage of uxview role
>
>     M   app/api/index.jsx
>     M   app/views/Endpoint/index.jsx
>
>     commit 7e0600732ad4a80530722b0317185be58e7e3e71
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 10:02:14 2019 -0500
>
>         UX Views applicable roles support, better role switching
>
>     M   app/api/index.jsx
>     M   app/layout/Table.jsx
>     M   app/views/Endpoint/GenericListView.jsx
>     M   app/views/Endpoint/UXViews.jsx
>     M   app/views/Endpoint/Users.jsx
>     M   app/views/Endpoint/index.jsx
>
>     commit 1db4beb2c7551f641d47aa43f9d811b26e7e7140
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 09:15:56 2019 -0500
>
>         Add title prop to logo
>
>     M   app/layout/Logo.jsx
>     M   app/layout/Table.jsx
>
>     commit b825666fd0cabca3ffd82718f3a6f654f0331e1e
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Aug 27 09:14:32 2019 -0500
>
>         Center align logo header icons
>
>     M   app/layout/Table.jsx
>
>     End of Note

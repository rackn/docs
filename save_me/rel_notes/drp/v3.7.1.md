v3.7.1 {#rs_rel_notes_drp_v3.7.1}
======

>     commit 7f990db9c93eff28c8430c3560ef155981efa584
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Feb 26 10:04:14 2018 -0800
>
>         spelling errors
>
>     M   doc/Swagger.rst
>
>     commit e996aee1e135502825fe39c49275090eeb106ae4
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Feb 26 11:50:54 2018 -0600
>
>         Clean up troubleshooting bootfile options.
>
>     M   doc/faq-troubleshooting.rst
>
>     commit ace6f366da177f7c8d1ede768a7b1b85285b3933
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Feb 26 11:43:49 2018 -0600
>
>         Fix stable portal link.
>
>     M   doc/upgrade.rst
>
>     commit 7d68b281bd723e82f1f396c4c68491a1e6b4c04f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Feb 26 11:42:01 2018 -0600
>
>         Remove spaces from ends of lines
>
>     M   doc/Swagger.rst
>     M   doc/arch/design.rst
>     M   doc/arch/dhcp.rst
>     M   doc/arch/provision.rst
>     M   doc/cli.rst
>     M   doc/cli_commands.rst
>     M   doc/configuring.rst
>     M   doc/deployment.rst
>     M   doc/dev/dev-cli.rst
>     M   doc/dev/dev-curl.rst
>     M   doc/dev/dev-docs.rst
>     M   doc/dev/dev-server.rst
>     M   doc/faq-troubleshooting.rst
>     M   doc/install.rst
>     M   doc/integrations/ansible.rst
>     M   doc/integrations/krib.rst
>     M   doc/integrations/websocket.rst
>     M   doc/operation.rst
>     M   doc/os-support/linuxkit.rst
>     M   doc/quickstart.rst
>     M   doc/ui.rst
>     M   doc/upgrade.rst
>     M   doc/workflows.rst
>
>     commit ea6025ca41a31cac53f37075804cdbdb9762779d
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Feb 26 11:35:23 2018 -0600
>
>         More tweaks and make it prettier.
>
>     M   doc/dev/dev-docs.rst
>     M   doc/faq-troubleshooting.rst
>     M   doc/install.rst
>     M   doc/integrations/ansible.rst
>     M   doc/integrations/krib.rst
>     M   doc/integrations/websocket.rst
>     M   doc/operation.rst
>     M   doc/quickstart.rst
>     M   doc/upgrade.rst
>
>     commit 40aa4aefcaae41ea819bd53db1e04d76dea1fcf8
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Feb 26 11:09:04 2018 -0600
>
>         Fix type stages to stage on machine drpcli call
>         Add files to gitignore to cleanup that view.
>         Add remove option and enhancements to install.sh
>         Update docs some more about the remove command.
>
>     M   .gitignore
>     M   doc/quickstart.rst
>
>     commit 269f59747f87995f4761224c994deb60de5b6a75
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Mon Feb 26 09:28:53 2018 -0800
>
>         fix some silly spelling errors
>
>     M   doc/install.rst
>
>     commit 034fc40f52ef0a00e20efb31a7b07ee5d0ddc43a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Feb 24 00:42:50 2018 -0600
>
>         Start changes for ease install comments.
>
>     M   doc/install.rst
>     M   doc/quickstart.rst
>
>     commit d0ed983f3070767a6e7c0af52885369de86bdf43
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Feb 23 11:20:04 2018 -0600
>
>         Fix option setting for existing options in subnet.
>         The API change to remove the pointer lists broke this.
>
>     M   cli/subnet.go
>     M   cli/subnet_test.go
>     M   cli/test-data/output/TestSubnetCli/subnets.get.john.option.6.2/stderr.expect
>     A   cli/test-data/output/TestSubnetCli/subnets.get.john.option.6.2/stdout.expect
>     A   cli/test-data/output/TestSubnetCli/subnets.get.john.option.6.3/stderr.expect
>     A   cli/test-data/output/TestSubnetCli/subnets.set.john.option.6.to.67/stdout.expect
>
>     commit 20615cce9ceefd77350caaac51f60adf1ce831f7
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Thu Feb 22 17:36:12 2018 -0800
>
>         Update to quickstart and faq for v3.7.0
>
>     M   doc/faq-troubleshooting.rst
>     M   doc/quickstart.rst
>
>     End of Note

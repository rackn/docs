v4.3.3 {#rs_rel_notes_drp_v4.3.3}
======

>     commit 2156259fc8d3ea297ebea0c01a87b87ac73db37b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 23 14:39:03 2020 -0500
>
>         docs(rel_notes): fix duplicate refs.
>
>     M   tools/build_rel_notes.sh
>
>     commit 884a0572c335afb17ff2e940620dae9d912755ed
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jun 22 22:24:01 2020 -0500
>
>         fix(whoami): Ignore obviously terrible DMI information
>
>         So, it turns out there are lists of known lying serial and asset
>         information that we should filter out when calculating a fingerprint.
>         Filter that stuff out.
>
>     M   models/whoami.go
>
>     commit 672880046baf764f2bc87236b11ec3db658ca7ea
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 16 12:51:46 2020 -0500
>
>         build: lock 4.3.0 to drpserver v4.3.2
>
>     M   tools/test.sh
>
>     End of Note

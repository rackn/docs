v4.4.0 {#rs_rel_notes_drp_v4.4.0}
======

>     commit cd1dd8387bb483bca2537fc43a6b0eeaaba684e2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jul 29 16:28:43 2020 -0500
>
>         fix(catalog,cli): allow tip to update if actual version changes
>
>     M   cli/catalog.go
>
>     commit 8b7da0ce1c438b07fba076369f0d8d0b4acc2204
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jul 29 12:06:58 2020 -0500
>
>         fix(template): Relax ID validation to allow names that begin with a number
>
>     M   models/template.go
>     M   models/utils.go
>
>     commit fb40dda0ad750cb2ee7d86df804c8ec16a540ee7
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jul 29 09:21:27 2020 -0500
>
>         fix(bootenvTest): Fix up to match current serverside reporting
>
>     M   api/bootenv_test.go
>
>     commit 01dbe16ea44070a5ca04d049803115bccb123af9
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 28 15:10:54 2020 -0500
>
>         fix(templateInfo): Export PathTmpl and LinkTmpl fields
>
>     M   models/templateInfo.go
>
>     commit 849cc7ee762cc688df994e09a6d1672c519f1958
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 28 15:10:03 2020 -0500
>
>         doc(bootenvs-customize): Add basic documentation for bootenv-customize param
>
>     M   doc/arch/provision.rst
>     M   models/endpoint.go
>
>     commit 2fd9651987b2468dd23cd9d5a8f27daecd6a8137
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jul 24 12:29:16 2020 -0500
>
>         fix(api,tests): API should not assume Endpoint is ignored.
>
>     M   api/events_test.go
>
>     commit 13b8176e3d25cf69a79ee45aaebc4a52cb992f38
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Fri Jul 10 17:52:47 2020 -0500
>
>         bug(provision): install.sh create-self ID does not match runner name due to : to - change
>
>     M   tools/install.sh
>
>     commit 14d681f8a79c4d5e1ae71cfe3f89fee6561e776b
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Fri Jul 10 13:36:11 2020 -0500
>
>         doc(params): add new params meta route field to models docs
>
>     M   doc/arch/models.rst
>
>     commit 717a0119e12b785ae257b79327fb5254c3ece933
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jul 7 15:39:35 2020 -0500
>
>         docs: Add KB about Proxy DHCP with UEFI failing in 4.4
>
>     A   doc/kb/kb-00043.rst
>
>     commit 4cc1b4b633b9f0f22f945d18bdea122b94e5a8fc
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Jul 7 13:31:23 2020 -0700
>
>         fix(docs): Fix KB generator script 'label'
>
>     M   tools/docs-make-kb.sh
>
>     commit 85a338f14ec048bce275567f6e9bafe3f9bd05e3
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jul 7 10:50:56 2020 -0500
>
>         fix(manager): Fix endpoint name validator to include macs
>
>     M   models/endpoint.go
>     M   models/utils.go
>
>     commit 5359d5b8c9162631666a976ef19922bbe769a4e7
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun Jun 28 14:39:22 2020 -0500
>
>         feat(manager): add debug layer inspector
>
>     M   cli/debug.go
>     M   doc/cli/drpcli_debug.rst
>
>     commit 797c5ba88b947951b21cc5eae51c967829ba287a
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Feb 19 23:05:46 2020 -0600
>
>         feat(manager): Embedded the manager into the core
>
>         Initial commit for embedding manager into the core.
>
>         This adds the objects into the models and updates docs
>         and unit tests for new features.
>
>     M   api/bootenv_test.go
>     M   api/client.go
>     M   api/machines_test.go
>     M   api/objects_test.go
>     A   cli/catalog_items.go
>     A   cli/endpoints.go
>     A   cli/test-data/output/TestCorePieces/catalog_items.indexes/stderr.expect
>     A   cli/test-data/output/TestCorePieces/catalog_items.indexes/stdout.expect
>     A   cli/test-data/output/TestCorePieces/endpoints.indexes/stdout.expect
>     A   cli/test-data/output/TestCorePieces/version_sets.indexes/stdout.expect
>     A   cli/version_sets.go
>     M   doc/cli/drpcli.rst
>     M   doc/cli/drpcli_catalog_items.rst
>     A   doc/cli/drpcli_catalog_items_create.rst
>     A   doc/cli/drpcli_catalog_items_destroy.rst
>     A   doc/cli/drpcli_catalog_items_exists.rst
>     A   doc/cli/drpcli_catalog_items_indexes.rst
>     A   doc/cli/drpcli_catalog_items_list.rst
>     A   doc/cli/drpcli_catalog_items_show.rst
>     A   doc/cli/drpcli_catalog_items_update.rst
>     A   doc/cli/drpcli_catalog_items_wait.rst
>     A   doc/cli/drpcli_endpoint_connections.rst
>     A   doc/cli/drpcli_endpoint_connections_action.rst
>     A   doc/cli/drpcli_endpoint_connections_actions.rst
>     A   doc/cli/drpcli_endpoint_connections_add.rst
>     A   doc/cli/drpcli_endpoint_connections_create.rst
>     A   doc/cli/drpcli_endpoint_connections_destroy.rst
>     A   doc/cli/drpcli_endpoint_connections_exists.rst
>     A   doc/cli/drpcli_endpoint_connections_get.rst
>     A   doc/cli/drpcli_endpoint_connections_indexes.rst
>     A   doc/cli/drpcli_endpoint_connections_list.rst
>     A   doc/cli/drpcli_endpoint_connections_meta.rst
>     A   doc/cli/drpcli_endpoint_connections_meta_add.rst
>     A   doc/cli/drpcli_endpoint_connections_meta_get.rst
>     A   doc/cli/drpcli_endpoint_connections_meta_remove.rst
>     A   doc/cli/drpcli_endpoint_connections_meta_set.rst
>     A   doc/cli/drpcli_endpoint_connections_params.rst
>     A   doc/cli/drpcli_endpoint_connections_remove.rst
>     A   doc/cli/drpcli_endpoint_connections_runaction.rst
>     A   doc/cli/drpcli_endpoint_connections_set.rst
>     A   doc/cli/drpcli_endpoint_connections_show.rst
>     A   doc/cli/drpcli_endpoint_connections_update.rst
>     A   doc/cli/drpcli_endpoint_connections_wait.rst
>     A   doc/cli/drpcli_endpoints.rst
>     A   doc/cli/drpcli_endpoints_action.rst
>     A   doc/cli/drpcli_endpoints_actions.rst
>     A   doc/cli/drpcli_endpoints_add.rst
>     A   doc/cli/drpcli_endpoints_create.rst
>     A   doc/cli/drpcli_endpoints_destroy.rst
>     A   doc/cli/drpcli_endpoints_exists.rst
>     A   doc/cli/drpcli_endpoints_get.rst
>     A   doc/cli/drpcli_endpoints_indexes.rst
>     A   doc/cli/drpcli_endpoints_list.rst
>     A   doc/cli/drpcli_endpoints_meta.rst
>     A   doc/cli/drpcli_endpoints_meta_add.rst
>     A   doc/cli/drpcli_endpoints_meta_get.rst
>     A   doc/cli/drpcli_endpoints_meta_remove.rst
>     A   doc/cli/drpcli_endpoints_meta_set.rst
>     A   doc/cli/drpcli_endpoints_params.rst
>     A   doc/cli/drpcli_endpoints_remove.rst
>     A   doc/cli/drpcli_endpoints_runaction.rst
>     A   doc/cli/drpcli_endpoints_set.rst
>     A   doc/cli/drpcli_endpoints_show.rst
>     A   doc/cli/drpcli_endpoints_update.rst
>     A   doc/cli/drpcli_endpoints_wait.rst
>     A   doc/cli/drpcli_version_sets.rst
>     A   doc/cli/drpcli_version_sets_action.rst
>     A   doc/cli/drpcli_version_sets_actions.rst
>     A   doc/cli/drpcli_version_sets_create.rst
>     A   doc/cli/drpcli_version_sets_destroy.rst
>     A   doc/cli/drpcli_version_sets_exists.rst
>     A   doc/cli/drpcli_version_sets_indexes.rst
>     A   doc/cli/drpcli_version_sets_list.rst
>     A   doc/cli/drpcli_version_sets_runaction.rst
>     A   doc/cli/drpcli_version_sets_show.rst
>     A   doc/cli/drpcli_version_sets_update.rst
>     A   doc/cli/drpcli_version_sets_wait.rst
>     M   models/bootenv.go
>     M   models/catalogItem.go
>     M   models/context.go
>     A   models/endpoint.go
>     M   models/info.go
>     M   models/job.go
>     M   models/lease.go
>     M   models/machine.go
>     M   models/param.go
>     M   models/plugin.go
>     M   models/pool.go
>     M   models/profile.go
>     M   models/raw_model.go
>     M   models/reservation.go
>     M   models/role.go
>     M   models/stage.go
>     M   models/subnet.go
>     M   models/task.go
>     M   models/template.go
>     M   models/tenant.go
>     M   models/user.go
>     M   models/utils.go
>     M   models/validate.go
>     A   models/version_set.go
>     M   models/workflow.go
>
>     commit 5b2ba7b0e4f9bdc1af8f8c4f1d4a393c30afbba9
>     Author: Meshiest <meek.mesh@gmail.com>
>     Date:   Tue Jun 30 14:46:00 2020 -0500
>
>         docs(airgap): add ux config section to airgap config, fix some typos
>
>     M   doc/operations/airgap.rst
>
>     commit e9836ff807de28ce17a16c5ac9aff30d9d5f44d7
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Mon Jun 29 12:12:24 2020 -0500
>
>         fix(pool): Fix pool validate to generate consistent errors
>
>     M   models/pool.go
>
>     commit 56c07eb0300f3c53ba1bc291805fa5718530726f
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jun 22 12:25:18 2020 -0500
>
>         feat(override-bootenvs): Add basic bootenv override functions.
>
>         We need a mechanism to allow for dynamic override of bootenvs to reign
>         in the bootenv explosions being caused by ESXi and others, and to
>         allow for user customizations of bootenvs that do not result in having
>         to copy everything that uses those bootenvs.
>
>         Along the way, switch to using deepcopy instead of JSON for model
>         cloning purposes.
>
>     M   go.mod
>     M   go.sum
>     M   models/bootenv.go
>     M   models/utils.go
>
>     commit e7e0f8d21301571512d73b3dffac77dfa8960fe0
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Wed Jun 24 12:40:16 2020 -0500
>
>         fix(pool) - add validate to the model
>
>     M   models/pool.go
>
>     commit f1435ca2ff0c11f42749ff7e8eafbce49f4e744a
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Wed Jun 24 10:00:02 2020 -0700
>
>         bug(doc): Fix spelling, add install example
>
>     M   doc/setup/kvm.rst
>
>     commit 8dd0f82e875c00dec4e0c74c1a59140eaf9dba4c
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Wed Jun 24 09:54:39 2020 -0700
>
>         bug(doc): Fix broken RST in KVM setup doc
>
>     M   doc/setup/kvm.rst
>
>     commit e35ea0c2b691326cace4cc56d560b7eb8eb9e703
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Jun 23 19:29:50 2020 -0700
>
>         enhance(doc): Add KVM Setup environment document
>
>     A   doc/setup/kvm.rst
>
>     commit 506889bbfe8f0cf450691c60fe6c17ebf940a53b
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 23 18:09:10 2020 -0500
>
>         docs: fix more formatting issues
>
>     M   doc/arch/provision.rst
>     M   doc/setup/esxi.rst
>
>     commit 58a5d6f3f39d163da263238d45e98fb8e55992f5
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 21:46:42 2020 -0500
>
>         docs: Add ux rel_notes
>
>     M   conf.py
>     A   doc/rel_notes/rackn-ux/.keep-me
>     M   doc/release.rst
>
>     commit de241b55365508c793025a62626ecebec934c404
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 23 14:39:03 2020 -0500
>
>         docs(rel_notes): fix duplicate refs.
>
>     M   tools/build_rel_notes.sh
>
>     commit 5addb080cab51542fe61d3ff7689c0ce62141074
>     Author: Carl Perry <carlp@rackn.com>
>     Date:   Tue Jun 23 14:01:50 2020 -0500
>
>         More minor edits
>
>     M   doc/setup/hyper-v.rst
>
>     commit 7a9a4d2ccb9aa176e67b5ee8d8a993c1b19e1079
>     Author: Carl Perry <carlp@rackn.com>
>     Date:   Tue Jun 23 13:58:32 2020 -0500
>
>         Minor corrections based on feedback
>
>     M   doc/setup/hyper-v.rst
>
>     commit 370187408a059186f2ec0e5f38e4164bf98dfe00
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Jun 22 22:24:01 2020 -0500
>
>         fix(whoami): Ignore obviously terrible DMI information
>
>         So, it turns out there are lists of known lying serial and asset
>         information that we should filter out when calculating a fingerprint.
>         Filter that stuff out.
>
>     M   models/whoami.go
>
>     commit 26e2b2b32a4fd7ca6435be94fbbe3c89420eda02
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Fri Jun 19 09:57:06 2020 -0500
>
>         build: update sprig to v3.1.0 (need get function)
>
>     M   go.mod
>     M   go.sum
>     M   models/templateInfo.go
>
>     commit 94f426972487f350c63fd41e479bd29c1c6de042
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 21:50:29 2020 -0500
>
>         feat(tasks): allow esxi-only templates
>
>     M   models/task.go
>
>     commit 92caa5c9baf6f51922c2ad2863bd6cb47bd213b3
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Jun 18 21:38:42 2020 -0500
>
>         docs: more release notes
>
>         This has lots of errors for the moment.  It will get better.
>
>     M   conf.py
>     A   doc/rel_notes/drp-content/.keep_me
>     A   doc/rel_notes/drp-plugins/.keep_me
>     A   doc/rel_notes/drp-server/.keep_me
>     A   doc/rel_notes/rackn-cohesity/.keep_me
>     A   doc/rel_notes/rackn-plugins/.keep_me
>     A   doc/rel_notes/rackn-solidfire/.keep_me
>     M   doc/release.rst
>
>     commit 3a73d65e9227840fa8d46bb9c7222e16ede8ba5b
>     Author: Carl Perry <carlp@rackn.com>
>     Date:   Thu Jun 18 11:32:04 2020 -0500
>
>         docs: Add example configuration for Hyper-V environment
>
>         Adds detailed description on how to build a DRP environment
>         targeted at Hyper-V for Windows 10 Pro or Enterprise. Can be
>         adapted to work with Hyper-V Hypervisor as well. Builds a complete
>         lab environment with a target machine and a DRP Endpoint using
>         the stable branch installer. The endpoint runs on a Debain 10
>         Virtual Machine, and one or more target Virtual Machines. The
>         target instaces are network isolated to allow for reproduction
>         of other environments with conflicting IP space.
>
>     A   doc/setup/hyper-v.rst
>
>     commit 3c5311211f2fae0c96b453d899ac0ed811ab2345
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jun 17 12:25:30 2020 -0500
>
>         feat(agent): Allow drpcli to auto-update on agent startup.
>
>         This updates drpcli when running in agent mode (via drpcli agent) to
>         automatically update itself if it detects that the currently running
>         binary is not the same as the one provided by dr-provision, and
>         dr-provision has the `agent-auto-update` feature.
>
>     M   api/client.go
>     M   cli/agent.go
>     M   go.mod
>     M   go.sum
>     M   models/modtimesha.go
>
>     commit 66d7e6fec9f6bc2f48e8d491d591beef58b9c4c7
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jun 10 11:46:44 2020 -0500
>
>         feat(whoami): Add some documentation for whoami machine identification
>
>         New whoami feature is new for dr-provision 4.3 and up.  It changes how
>         we do machine identification where available to no longer rely on the
>         machine identifier string that is passed in on the kernel commandline.
>
>         Instead, we fingerprint machines based on a combination of hardware
>         identifiers.
>
>     M   doc/arch/content-package.rst
>     M   doc/arch/design.rst
>     M   doc/arch/sledgehammer.rst
>
>     commit 2ca7026d22fa153e8155825f717b5d6d6e351d1f
>     Author: Michael Rice <michael@michaelrice.org>
>     Date:   Wed Jun 17 13:23:18 2020 -0500
>
>         docs(added ESXi setup doc)
>
>         Added a page to explain what the setup of an ESXi test machine would look like
>
>         Signed-off-by: Michael Rice <michael@michaelrice.org>
>
>     A   doc/setup/esxi.rst
>
>     commit 6da72c569c55e7479f3977c9dee2c23536b58757
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Jun 16 14:49:27 2020 -0700
>
>         Update preparing.rst
>
>     M   doc/setup/preparing.rst
>
>     commit f7e147f5099a1af6e8b732e1de8a8797ac34cd9f
>     Author: Shane Gibson <shane@rackn.com>
>     Date:   Tue Jun 16 14:47:18 2020 -0700
>
>         enhance(doc): Add 'preparing' document for new users
>
>     M   doc/quickstart.rst
>     A   doc/setup/preparing.rst
>
>     commit 372b253a2367c525a792a2b750e74ba6d149d773
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue Jun 16 12:53:07 2020 -0500
>
>         build: tip of tree should use tip server until we get a release
>
>     M   tools/test.sh
>
>     commit 2bc0b301fcf083b63d765e58c99e7e8c86d65639
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Tue May 5 10:54:23 2020 -0500
>
>         feat(pool): Add pooling support
>
>         This commit adds the pooling objects and cli to the core
>         product.
>
>         The CLI and DOCs describe the operational and architectural usage.
>
>         Pools are optionally defined groups of machines that track
>         transition into and out the pools along with allocation and release.
>
>         Pools can define actions (add/remove parameters, add/remove profiles,
>         and change workflow) for entry and exit of pools and allocation and
>         release within the pool
>
>         Pool operations can be done on specific machines or filters that let
>         the user operate with cloud-like methods.
>
>     M   agent/change_stage_test.go
>     M   agent/taskRunner_test.go
>     M   api/events_test.go
>     M   api/objects_test.go
>     M   cli/machines.go
>     A   cli/pools.go
>     M   cli/test-data/output/TestCorePieces/machines.indexes/stdout.expect
>     A   cli/test-data/output/TestCorePieces/pools.indexes/stdout.expect
>     M   doc/arch.rst
>     A   doc/arch/pooling.rst
>     M   doc/cli/drpcli.rst
>     M   doc/cli/drpcli_machines.rst
>     A   doc/cli/drpcli_machines_pause.rst
>     A   doc/cli/drpcli_machines_releaseToPool.rst
>     A   doc/cli/drpcli_machines_run.rst
>     A   doc/cli/drpcli_pools.rst
>     A   doc/cli/drpcli_pools_action.rst
>     A   doc/cli/drpcli_pools_actions.rst
>     A   doc/cli/drpcli_pools_active.rst
>     A   doc/cli/drpcli_pools_create.rst
>     A   doc/cli/drpcli_pools_destroy.rst
>     A   doc/cli/drpcli_pools_exists.rst
>     A   doc/cli/drpcli_pools_indexes.rst
>     A   doc/cli/drpcli_pools_list.rst
>     A   doc/cli/drpcli_pools_manage.rst
>     A   doc/cli/drpcli_pools_manage_add.rst
>     A   doc/cli/drpcli_pools_manage_allocate.rst
>     A   doc/cli/drpcli_pools_manage_release.rst
>     A   doc/cli/drpcli_pools_manage_remove.rst
>     A   doc/cli/drpcli_pools_runaction.rst
>     A   doc/cli/drpcli_pools_show.rst
>     A   doc/cli/drpcli_pools_status.rst
>     A   doc/cli/drpcli_pools_update.rst
>     A   doc/cli/drpcli_pools_wait.rst
>     M   doc/operation.rst
>     A   doc/operations/pooling.rst
>     M   models/machine.go
>     A   models/pool.go
>     M   models/utils.go
>
>     commit b5edbd85731ef7e665097e03771b96dc8bab019f
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 13 23:16:17 2020 -0500
>
>         fix(dhcp) - unit test fix
>
>     M   cli/test-data/output/TestCorePieces/subnets.indexes/stdout.expect
>
>     commit a4e62ef51bd80703dad1809fb05ad647cb8789dd
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Jun 13 15:57:52 2020 -0500
>
>         docs: Update release indicators
>
>     M   README.rst
>
>     commit cc6d24d7a40404809e24f3bf043505063149c5ba
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Thu Apr 2 11:30:31 2020 -0500
>
>         add-key via UX
>
>     M   doc/faq-troubleshooting.rst
>
>     commit 4a6443b3889606ced19d1a00aeecac9228b703cd
>     Author: Rob Hirschfeld <rob@rackn.com>
>     Date:   Mon Feb 10 16:12:31 2020 -0600
>
>         make hard link more obvious
>
>     M   doc/install.rst
>
>     End of Note

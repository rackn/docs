v4.2.12 {#rs_rel_notes_drp_v4.2.12}
=======

>     commit 6751e6bd1bc265e57286dfcf38023f5913259589
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun May 10 14:15:10 2020 -0500
>
>         fix(agent): make the install and runtime path for windows the same
>
>         Also Fix the path to be "correct" for windows2019 server.
>         In general, the path shouldn't matter for normal operations.
>         They just need to be the same.
>
>     M   cli/agent.go
>     M   cli/machines.go
>
>     commit dcb8fe4485be4ef4f175e4ddf303735eb42db725
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sun May 10 14:14:44 2020 -0500
>
>         fix(agent): fix windows shutdown and reboot commands
>
>     M   agent/agent.go
>
>     End of Note

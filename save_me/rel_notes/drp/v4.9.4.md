v4.9.4 {#rs_rel_notes_drp_v4.9.4}
======

>     commit 43f143119f18369c01926de5c3282b1a7145c1fe
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Sat Apr 30 10:27:10 2022 -0500
>
>         fix: test
>
>     M   api/files_test.go
>
>     commit ec2b75facbb866f1b16cd7b5192c8632c57b43fc
>     Author: Zaheena <zaheena@gmail.com>
>     Date:   Fri Apr 29 12:28:41 2022 -0400
>
>         fix(catalog): pre-filter src items to avoid duplicates
>
>     M   cli/utils.go
>
>     commit 97dd6a06dd0aa8887b416619544195e119c562a6
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Apr 21 15:08:55 2022 -0500
>
>         fix: update version for this change
>
>     M   tools/install.sh
>
>     commit aad1ea013599a72f76e3e0e59ceb6e7465940e23
>     Author: Tim Bosse <tim@rackn.com>
>     Date:   Wed Apr 20 13:28:35 2022 -0400
>
>         fix(install): set WorkingDirectory
>
>     M   tools/install.sh
>
>     End of Note

v4.7.16 {#rs_rel_notes_drp_v4.7.16}
=======

>     commit 5e493224007abd1afcadfa0e611411f0ac570f45
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Mon Feb 7 17:29:25 2022 -0600
>
>         fix(consensus): Simplify cert rotation.
>
>         And add a debugging command to allow for easily examining
>         ha-state.json.
>
>     M   cli/system.go
>     M   models/ha-state.go
>     A   models/ha-state_test.go
>
>     End of Note

> doc(release): add OpenSSL patch notice

> M doc/rel\_notes/summaries/release\_v411.rst
>
> commit 1158d2390ebc505a4d4f72d9ae7e38447be884fa Author: Mike McRill
> \<<mike@mcrill.net>\> Date: Mon Oct 31 14:22:00 2022 -0500
>
> > Update docs and add output for RS variables
>
> M doc/install/install-cloud.rst M
> integrations/terraform/libvirt/libvirt.tf
>
> commit 048ac4880e6dec4dc0a64ac35196cd43eb9eadd3 Author: Mike McRill
> \<<mike@mcrill.net>\> Date: Mon Oct 31 13:47:22 2022 -0500
>
> > Add libvirt terraform configuration
>
> A integrations/terraform/.gitignore A
> integrations/terraform/libvirt/libvirt.tf
>
> commit 9ec436b5dc3b8a0d234d915b6306efc3fa67c443 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Wed Oct 26 15:57:23 2022 -0400
>
> > feat(catalog): exposing build catalog via cli
>
> M api/utils.go M cli/airgap.go M cli/catalog.go M
> doc/cli/drpcli\_catalog.rst A doc/cli/drpcli\_catalog\_build.rst A
> models/catalog\_builder.go M models/content.go
>
> commit 3ced76a6d0cfe170e8c92b53f6cec63726a2490b Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Tue Oct 25 15:15:40 2022 -0500
>
> > fix: bump install.sh version
>
> M tools/install.sh
>
> commit 40db62ed768239af954ed37124758a0355d43ffa Author: Rob (zehicle)
> Hirschfeld \<<rob@rackn.com>\> Date: Mon Oct 24 17:03:29 2022 -0400
>
> > doc(provision): release notes draft v411
>
> M doc/rel\_notes/summaries/release\_v411.rst A
> doc/rel\_notes/summaries/release\_v412.rst
>
> commit 4d78afc083824a64f61327c178c1359c81610ab6 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Mon Oct 24 13:12:17 2022 -0500
>
> > Pull in updated gohai
>
> M go.mod M go.sum
>
> commit 38358741cec3e23b6c188d233a89721a1bb3562d Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Sun Oct 23 17:31:52 2022 -0500
>
> > feat(install.sh): Add RS\_SERVER\_HOSTNAME config option
>
> M tools/install.sh
>
> commit 19afe6bbb3c3a259de2aec9903d8f88e82e0a692 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Fri Oct 21 10:34:04 2022 -0500
>
> > feat(connections): Add connection information commands to the CLI.
> >
> > We are adding basic tracking of connection information to
> > dr-provision. Add relevant commands to the CLI to get this
> > information.
>
> A cli/connection.go M cli/machines.go A models/connection.go
>
> commit c8899f61c06cece8fb629dbc9584c2c5067b24e4 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Oct 17 12:48:19 2022 -0500
>
> > fix(cli): Put back pcr support exception, remove support bundle code
> > requiring DRP access feat(cli)): Add crash-bundle command and update
> > bundle to be system info from running system
>
> M cli/startup.go M cli/support.go M doc/cli/drpcli\_support.rst R079
> doc/cli/drpcli\_support\_bundle.rst
> doc/cli/drpcli\_support\_crash-bundle.rst A
> doc/cli/drpcli\_support\_info.rst
>
> commit f9ebe05673f37e11a6cdef65fa0f1a3fcc9a3b78 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Wed Oct 5 14:00:36 2022 -0400
>
> > feat(install): add option to disable isofs
> >
> > closes \#630
>
> M tools/install.sh
>
> commit f4e1fe2a3587322fe80fd20044180e3470cb45f8 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Sat Oct 15 21:23:25 2022 -0400
>
> > fix(support): misc fixes related to npd
> >
> > While investigating nil pointer dereference for \#629, noted a few,
> > non issues that could be addressed.
> >
> > -   use functions in os as io/ioutil is deprecated
> > -   remove redundant check on extras.
> > -   Create returns an error, handle it.
>
> M cli/support.go
>
> commit 50137f551a88a559694b91922c9055b8b033b876 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Sat Oct 15 21:03:47 2022 -0400
>
> > fix(cli): address nil pointer dereference in support.go
> >
> > We need to persist Session for support bundle as we are gathering
> > content from endpoint.
> >
> > Fixes \#629
>
> M cli/startup.go
>
> commit 58155b62afb27b5fdf8ccaf09dd8e68de26c98c7 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Thu Oct 13 13:09:05 2022 -0500
>
> > doc: Document hardware system extensions and usage
>
> M doc/arch.rst A doc/arch/hardware.rst M doc/arch/pipeline.rst
>
> commit a8648d1f94f8b4709ba46d4a7760edcb9300567b Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Wed Oct 12 17:08:57 2022 -0500
>
> > doc: Rework pipeline.rst
>
> M doc/arch/pipeline.rst
>
> commit 6adf1e3c409c3df865c053f6e7769b42021a9e58 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Tue Oct 11 14:50:37 2022 -0500
>
> > fest(vulncheck): Add automated vulnerability scanning to the tests.
> >
> > While we are at it, fix a vulnerability that the scan found.
>
> M go.mod M go.sum M tools/build-one.sh M tools/test.sh
>
> commit bc34a98cdc0ab4a189e09339e361c61e4363653b Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Tue Oct 11 10:28:11 2022 -0500
>
> > fix(catalog): Fix handling of the catalog-source flag.
> >
> > Setting the default catalog source to the one we found via the first
> > API call is not particularly recommended, since it makes the help
> > emitted for a command different depending on whether a catalog
> > command was issued before the current command instead of being more
> > or less static.
>
> M cli/startup.go M cli/utils.go
>
> commit 6e68aa51eca912ca85f69e512e3e83369161dd48 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Wed Sep 28 15:53:09 2022 -0400
>
> > fix(doc): update machine migration doc
> >
> > Update machine output now that profile migration works. Clean up
> > endpoint names.
>
> M doc/operations/machine-migration.rst
>
> commit 1f8c840afff2a991cdfa0a80ce6fb17126f3fe68 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Wed Sep 28 11:53:02 2022 -0500
>
> > fix: allow upgrade to use a changed password with the default user
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M tools/install.sh
>
> commit 1cbce8a65dde37b6c65a57c0a0afba6b882234ed Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Thu Sep 1 16:14:25 2022 -0400
>
> > feat(doc): machine migration operation doc
> >
> > This addresses rackn/product-backlog\#6
>
> A doc/operations/machine-migration.rst
>
> commit 569e308b92c9adf82f97f4b4ce2607b7fa789938 Author: Shane Gibson
> \<<shane@rackn.com>\> Date: Thu Sep 8 18:37:27 2022 +0000
>
> > enhance(doc): Add notes for \'curtin/partitions\' requirements
>
> M doc/operations/imagedeploy-storage.rst
>
> commit d74fdbb6726b4b321c9aeb93ff234c1e3b74d4ae Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Thu Sep 8 11:09:32 2022 -0500
>
> > fix: still more ux-views model fixes
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/ux\_view.go
>
> commit 49d1e987419a5cb6997e5e34f708fd148b35b87d Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Thu Sep 8 11:05:23 2022 -0500
>
> > fix: ux-views init Classifiers
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/ux\_view.go
>
> commit 0fbdbefc0cfc1798caf6e347825b4b9c96958c15 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Thu Sep 8 10:56:53 2022 -0500
>
> > fix: typo
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/ux\_view.go
>
> commit fb2ab3b0e360bde91552c1774ad1f825536c38db Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Thu Sep 8 10:53:43 2022 -0500
>
> > fix(models): update ux-views to actual code
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/ux\_view.go
>
> commit f2b3a661f7affbe802845b49eca4f74416505a86 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Wed Sep 7 16:20:35 2022 -0500
>
> > fix(models): update fill on new models
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/filter.go M models/ux\_option.go M models/ux\_setting.go M
> models/ux\_view.go
>
> commit a0c84b76b185db64cd40a5d216e35b164d5afd43 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Sep 5 15:54:00 2022 -0500
>
> > feat(filter): add more fields to encompass all the options
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M api/client.go M cli/commandHelper.go M doc/cli/drpcli.rst M
> doc/cli/drpcli\_alerts\_count.rst M doc/cli/drpcli\_alerts\_list.rst M
> doc/cli/drpcli\_blueprints\_count.rst M
> doc/cli/drpcli\_blueprints\_list.rst M
> doc/cli/drpcli\_bootenvs\_count.rst M
> doc/cli/drpcli\_bootenvs\_list.rst M
> doc/cli/drpcli\_catalog\_items\_count.rst M
> doc/cli/drpcli\_catalog\_items\_list.rst M
> doc/cli/drpcli\_clusters\_count.rst M
> doc/cli/drpcli\_clusters\_list.rst M
> doc/cli/drpcli\_contexts\_count.rst M
> doc/cli/drpcli\_contexts\_list.rst M
> doc/cli/drpcli\_endpoints\_count.rst M
> doc/cli/drpcli\_endpoints\_list.rst M
> doc/cli/drpcli\_extended\_count.rst M
> doc/cli/drpcli\_extended\_list.rst M
> doc/cli/drpcli\_filters\_count.rst M doc/cli/drpcli\_filters\_list.rst
> M doc/cli/drpcli\_identity\_providers\_count.rst M
> doc/cli/drpcli\_identity\_providers\_list.rst M
> doc/cli/drpcli\_interfaces\_count.rst M
> doc/cli/drpcli\_interfaces\_list.rst M doc/cli/drpcli\_jobs\_count.rst
> M doc/cli/drpcli\_jobs\_list.rst M doc/cli/drpcli\_leases\_count.rst M
> doc/cli/drpcli\_leases\_list.rst M doc/cli/drpcli\_machines\_count.rst
> M doc/cli/drpcli\_machines\_list.rst M
> doc/cli/drpcli\_params\_count.rst M doc/cli/drpcli\_params\_list.rst M
> doc/cli/drpcli\_plugin\_providers\_count.rst M
> doc/cli/drpcli\_plugin\_providers\_list.rst M
> doc/cli/drpcli\_plugins\_count.rst M doc/cli/drpcli\_plugins\_list.rst
> M doc/cli/drpcli\_pools\_count.rst M doc/cli/drpcli\_pools\_list.rst M
> doc/cli/drpcli\_profiles\_count.rst M
> doc/cli/drpcli\_profiles\_list.rst M
> doc/cli/drpcli\_reservations\_count.rst M
> doc/cli/drpcli\_reservations\_list.rst M
> doc/cli/drpcli\_resource\_brokers\_count.rst M
> doc/cli/drpcli\_resource\_brokers\_list.rst M
> doc/cli/drpcli\_roles\_count.rst M doc/cli/drpcli\_roles\_list.rst M
> doc/cli/drpcli\_stages\_count.rst M doc/cli/drpcli\_stages\_list.rst M
> doc/cli/drpcli\_subnets\_count.rst M doc/cli/drpcli\_subnets\_list.rst
> M doc/cli/drpcli\_tasks\_count.rst M doc/cli/drpcli\_tasks\_list.rst M
> doc/cli/drpcli\_templates\_count.rst M
> doc/cli/drpcli\_templates\_list.rst M
> doc/cli/drpcli\_tenants\_count.rst M doc/cli/drpcli\_tenants\_list.rst
> M doc/cli/drpcli\_trigger\_providers\_count.rst M
> doc/cli/drpcli\_trigger\_providers\_list.rst M
> doc/cli/drpcli\_triggers\_count.rst M
> doc/cli/drpcli\_triggers\_list.rst M doc/cli/drpcli\_users\_count.rst
> M doc/cli/drpcli\_users\_list.rst M
> doc/cli/drpcli\_ux\_options\_count.rst M
> doc/cli/drpcli\_ux\_options\_list.rst M
> doc/cli/drpcli\_ux\_settings\_count.rst M
> doc/cli/drpcli\_ux\_settings\_list.rst M
> doc/cli/drpcli\_ux\_views\_count.rst M
> doc/cli/drpcli\_ux\_views\_list.rst M
> doc/cli/drpcli\_version\_sets\_count.rst M
> doc/cli/drpcli\_version\_sets\_list.rst M
> doc/cli/drpcli\_work\_orders\_count.rst M
> doc/cli/drpcli\_work\_orders\_list.rst M
> doc/cli/drpcli\_workflows\_count.rst M
> doc/cli/drpcli\_workflows\_list.rst M models/filter.go M
> models/ux\_option.go M models/ux\_setting.go M models/ux\_view.go
>
> commit 5d0417ea11d08f9df9202c568de6836c080a792b Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Sun Sep 4 17:33:45 2022 -0500
>
> > feat(ux-view): add ux-view models
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M api/objects\_test.go A cli/filters.go A cli/ux\_options.go A
> cli/ux\_settings.go A cli/ux\_views.go M conf.py M doc/cli/drpcli.rst
> M doc/cli/drpcli\_agent.rst M doc/cli/drpcli\_airgap.rst M
> doc/cli/drpcli\_airgap\_build.rst M
> doc/cli/drpcli\_airgap\_explode.rst M doc/cli/drpcli\_alerts.rst M
> doc/cli/drpcli\_alerts\_ack.rst M doc/cli/drpcli\_alerts\_add.rst M
> doc/cli/drpcli\_alerts\_await.rst M doc/cli/drpcli\_alerts\_count.rst
> M doc/cli/drpcli\_alerts\_create.rst M
> doc/cli/drpcli\_alerts\_destroy.rst M doc/cli/drpcli\_alerts\_etag.rst
> M doc/cli/drpcli\_alerts\_exists.rst M doc/cli/drpcli\_alerts\_get.rst
> M doc/cli/drpcli\_alerts\_indexes.rst M
> doc/cli/drpcli\_alerts\_list.rst M doc/cli/drpcli\_alerts\_params.rst
> M doc/cli/drpcli\_alerts\_patch.rst M doc/cli/drpcli\_alerts\_post.rst
> M doc/cli/drpcli\_alerts\_remove.rst M doc/cli/drpcli\_alerts\_set.rst
> M doc/cli/drpcli\_alerts\_show.rst M
> doc/cli/drpcli\_alerts\_update.rst M
> doc/cli/drpcli\_alerts\_uploadiso.rst M
> doc/cli/drpcli\_alerts\_wait.rst M doc/cli/drpcli\_autocomplete.rst M
> doc/cli/drpcli\_blueprints.rst M
> doc/cli/drpcli\_blueprints\_action.rst M
> doc/cli/drpcli\_blueprints\_actions.rst M
> doc/cli/drpcli\_blueprints\_add.rst M
> doc/cli/drpcli\_blueprints\_addprofile.rst M
> doc/cli/drpcli\_blueprints\_addtask.rst M
> doc/cli/drpcli\_blueprints\_await.rst M
> doc/cli/drpcli\_blueprints\_count.rst M
> doc/cli/drpcli\_blueprints\_create.rst M
> doc/cli/drpcli\_blueprints\_destroy.rst M
> doc/cli/drpcli\_blueprints\_etag.rst M
> doc/cli/drpcli\_blueprints\_exists.rst M
> doc/cli/drpcli\_blueprints\_get.rst M
> doc/cli/drpcli\_blueprints\_indexes.rst M
> doc/cli/drpcli\_blueprints\_list.rst M
> doc/cli/drpcli\_blueprints\_meta.rst M
> doc/cli/drpcli\_blueprints\_meta\_add.rst M
> doc/cli/drpcli\_blueprints\_meta\_get.rst M
> doc/cli/drpcli\_blueprints\_meta\_remove.rst M
> doc/cli/drpcli\_blueprints\_meta\_set.rst M
> doc/cli/drpcli\_blueprints\_params.rst M
> doc/cli/drpcli\_blueprints\_patch.rst M
> doc/cli/drpcli\_blueprints\_remove.rst M
> doc/cli/drpcli\_blueprints\_removeprofile.rst M
> doc/cli/drpcli\_blueprints\_removetask.rst M
> doc/cli/drpcli\_blueprints\_runaction.rst M
> doc/cli/drpcli\_blueprints\_set.rst M
> doc/cli/drpcli\_blueprints\_show.rst M
> doc/cli/drpcli\_blueprints\_update.rst M
> doc/cli/drpcli\_blueprints\_uploadiso.rst M
> doc/cli/drpcli\_blueprints\_wait.rst M doc/cli/drpcli\_bootenvs.rst M
> doc/cli/drpcli\_bootenvs\_action.rst M
> doc/cli/drpcli\_bootenvs\_actions.rst M
> doc/cli/drpcli\_bootenvs\_await.rst M
> doc/cli/drpcli\_bootenvs\_count.rst M
> doc/cli/drpcli\_bootenvs\_create.rst M
> doc/cli/drpcli\_bootenvs\_destroy.rst M
> doc/cli/drpcli\_bootenvs\_etag.rst M
> doc/cli/drpcli\_bootenvs\_exists.rst M
> doc/cli/drpcli\_bootenvs\_fromAppleNBI.rst M
> doc/cli/drpcli\_bootenvs\_indexes.rst M
> doc/cli/drpcli\_bootenvs\_install.rst M
> doc/cli/drpcli\_bootenvs\_list.rst M
> doc/cli/drpcli\_bootenvs\_meta.rst M
> doc/cli/drpcli\_bootenvs\_meta\_add.rst M
> doc/cli/drpcli\_bootenvs\_meta\_get.rst M
> doc/cli/drpcli\_bootenvs\_meta\_remove.rst M
> doc/cli/drpcli\_bootenvs\_meta\_set.rst M
> doc/cli/drpcli\_bootenvs\_patch.rst M
> doc/cli/drpcli\_bootenvs\_purgeLocalInstall.rst M
> doc/cli/drpcli\_bootenvs\_runaction.rst M
> doc/cli/drpcli\_bootenvs\_show.rst M
> doc/cli/drpcli\_bootenvs\_update.rst M
> doc/cli/drpcli\_bootenvs\_uploadiso.rst M
> doc/cli/drpcli\_bootenvs\_wait.rst M doc/cli/drpcli\_catalog.rst M
> doc/cli/drpcli\_catalog\_copyLocal.rst M
> doc/cli/drpcli\_catalog\_create.rst M
> doc/cli/drpcli\_catalog\_item.rst M
> doc/cli/drpcli\_catalog\_item\_detail.rst M
> doc/cli/drpcli\_catalog\_item\_download.rst M
> doc/cli/drpcli\_catalog\_item\_install.rst M
> doc/cli/drpcli\_catalog\_item\_show.rst M
> doc/cli/drpcli\_catalog\_items.rst M
> doc/cli/drpcli\_catalog\_items\_await.rst M
> doc/cli/drpcli\_catalog\_items\_count.rst M
> doc/cli/drpcli\_catalog\_items\_create.rst M
> doc/cli/drpcli\_catalog\_items\_destroy.rst M
> doc/cli/drpcli\_catalog\_items\_etag.rst M
> doc/cli/drpcli\_catalog\_items\_exists.rst M
> doc/cli/drpcli\_catalog\_items\_indexes.rst M
> doc/cli/drpcli\_catalog\_items\_list.rst M
> doc/cli/drpcli\_catalog\_items\_patch.rst M
> doc/cli/drpcli\_catalog\_items\_show.rst M
> doc/cli/drpcli\_catalog\_items\_update.rst M
> doc/cli/drpcli\_catalog\_items\_wait.rst M
> doc/cli/drpcli\_catalog\_show.rst M
> doc/cli/drpcli\_catalog\_updateLocal.rst M doc/cli/drpcli\_certs.rst M
> doc/cli/drpcli\_certs\_csr.rst M doc/cli/drpcli\_clusters.rst M
> doc/cli/drpcli\_clusters\_action.rst M
> doc/cli/drpcli\_clusters\_actions.rst M
> doc/cli/drpcli\_clusters\_add.rst M
> doc/cli/drpcli\_clusters\_addprofile.rst M
> doc/cli/drpcli\_clusters\_addtask.rst M
> doc/cli/drpcli\_clusters\_await.rst M
> doc/cli/drpcli\_clusters\_bootenv.rst M
> doc/cli/drpcli\_clusters\_cleanup.rst M
> doc/cli/drpcli\_clusters\_count.rst M
> doc/cli/drpcli\_clusters\_create.rst M
> doc/cli/drpcli\_clusters\_currentlog.rst M
> doc/cli/drpcli\_clusters\_deletejobs.rst M
> doc/cli/drpcli\_clusters\_destroy.rst M
> doc/cli/drpcli\_clusters\_etag.rst M
> doc/cli/drpcli\_clusters\_exists.rst M
> doc/cli/drpcli\_clusters\_get.rst M
> doc/cli/drpcli\_clusters\_group.rst M
> doc/cli/drpcli\_clusters\_group\_add.rst M
> doc/cli/drpcli\_clusters\_group\_addprofile.rst M
> doc/cli/drpcli\_clusters\_group\_get.rst M
> doc/cli/drpcli\_clusters\_group\_params.rst M
> doc/cli/drpcli\_clusters\_group\_remove.rst M
> doc/cli/drpcli\_clusters\_group\_removeprofile.rst M
> doc/cli/drpcli\_clusters\_group\_set.rst M
> doc/cli/drpcli\_clusters\_group\_uploadiso.rst M
> doc/cli/drpcli\_clusters\_indexes.rst M
> doc/cli/drpcli\_clusters\_inserttask.rst M
> doc/cli/drpcli\_clusters\_inspect.rst M
> doc/cli/drpcli\_clusters\_inspect\_jobs.rst M
> doc/cli/drpcli\_clusters\_inspect\_tasks.rst M
> doc/cli/drpcli\_clusters\_jobs.rst M
> doc/cli/drpcli\_clusters\_jobs\_create.rst M
> doc/cli/drpcli\_clusters\_jobs\_current.rst M
> doc/cli/drpcli\_clusters\_jobs\_state.rst M
> doc/cli/drpcli\_clusters\_list.rst M
> doc/cli/drpcli\_clusters\_meta.rst M
> doc/cli/drpcli\_clusters\_meta\_add.rst M
> doc/cli/drpcli\_clusters\_meta\_get.rst M
> doc/cli/drpcli\_clusters\_meta\_remove.rst M
> doc/cli/drpcli\_clusters\_meta\_set.rst M
> doc/cli/drpcli\_clusters\_params.rst M
> doc/cli/drpcli\_clusters\_patch.rst M
> doc/cli/drpcli\_clusters\_pause.rst M
> doc/cli/drpcli\_clusters\_processjobs.rst M
> doc/cli/drpcli\_clusters\_releaseToPool.rst M
> doc/cli/drpcli\_clusters\_remove.rst M
> doc/cli/drpcli\_clusters\_removeprofile.rst M
> doc/cli/drpcli\_clusters\_removetask.rst M
> doc/cli/drpcli\_clusters\_run.rst M
> doc/cli/drpcli\_clusters\_runaction.rst M
> doc/cli/drpcli\_clusters\_set.rst M doc/cli/drpcli\_clusters\_show.rst
> M doc/cli/drpcli\_clusters\_stage.rst M
> doc/cli/drpcli\_clusters\_start.rst M
> doc/cli/drpcli\_clusters\_tasks.rst M
> doc/cli/drpcli\_clusters\_tasks\_add.rst M
> doc/cli/drpcli\_clusters\_tasks\_del.rst M
> doc/cli/drpcli\_clusters\_update.rst M
> doc/cli/drpcli\_clusters\_uploadiso.rst M
> doc/cli/drpcli\_clusters\_wait.rst M
> doc/cli/drpcli\_clusters\_whoami.rst M
> doc/cli/drpcli\_clusters\_work\_order.rst M
> doc/cli/drpcli\_clusters\_work\_order\_add.rst M
> doc/cli/drpcli\_clusters\_work\_order\_off.rst M
> doc/cli/drpcli\_clusters\_work\_order\_on.rst M
> doc/cli/drpcli\_clusters\_workflow.rst M
> doc/cli/drpcli\_completion.rst M doc/cli/drpcli\_contents.rst M
> doc/cli/drpcli\_contents\_bundle.rst M
> doc/cli/drpcli\_contents\_bundlize.rst M
> doc/cli/drpcli\_contents\_convert.rst M
> doc/cli/drpcli\_contents\_create.rst M
> doc/cli/drpcli\_contents\_destroy.rst M
> doc/cli/drpcli\_contents\_document.rst M
> doc/cli/drpcli\_contents\_exists.rst M
> doc/cli/drpcli\_contents\_list.rst M
> doc/cli/drpcli\_contents\_show.rst M
> doc/cli/drpcli\_contents\_unbundle.rst M
> doc/cli/drpcli\_contents\_update.rst M
> doc/cli/drpcli\_contents\_upload.rst M doc/cli/drpcli\_contexts.rst M
> doc/cli/drpcli\_contexts\_await.rst M
> doc/cli/drpcli\_contexts\_count.rst M
> doc/cli/drpcli\_contexts\_create.rst M
> doc/cli/drpcli\_contexts\_destroy.rst M
> doc/cli/drpcli\_contexts\_etag.rst M
> doc/cli/drpcli\_contexts\_exists.rst M
> doc/cli/drpcli\_contexts\_indexes.rst M
> doc/cli/drpcli\_contexts\_list.rst M
> doc/cli/drpcli\_contexts\_meta.rst M
> doc/cli/drpcli\_contexts\_meta\_add.rst M
> doc/cli/drpcli\_contexts\_meta\_get.rst M
> doc/cli/drpcli\_contexts\_meta\_remove.rst M
> doc/cli/drpcli\_contexts\_meta\_set.rst M
> doc/cli/drpcli\_contexts\_patch.rst M
> doc/cli/drpcli\_contexts\_show.rst M
> doc/cli/drpcli\_contexts\_update.rst M
> doc/cli/drpcli\_contexts\_wait.rst M doc/cli/drpcli\_debug.rst M
> doc/cli/drpcli\_endpoints.rst M doc/cli/drpcli\_endpoints\_action.rst
> M doc/cli/drpcli\_endpoints\_actions.rst M
> doc/cli/drpcli\_endpoints\_add.rst M
> doc/cli/drpcli\_endpoints\_await.rst M
> doc/cli/drpcli\_endpoints\_count.rst M
> doc/cli/drpcli\_endpoints\_create.rst M
> doc/cli/drpcli\_endpoints\_destroy.rst M
> doc/cli/drpcli\_endpoints\_etag.rst M
> doc/cli/drpcli\_endpoints\_exists.rst M
> doc/cli/drpcli\_endpoints\_get.rst M
> doc/cli/drpcli\_endpoints\_indexes.rst M
> doc/cli/drpcli\_endpoints\_list.rst M
> doc/cli/drpcli\_endpoints\_meta.rst M
> doc/cli/drpcli\_endpoints\_meta\_add.rst M
> doc/cli/drpcli\_endpoints\_meta\_get.rst M
> doc/cli/drpcli\_endpoints\_meta\_remove.rst M
> doc/cli/drpcli\_endpoints\_meta\_set.rst M
> doc/cli/drpcli\_endpoints\_params.rst M
> doc/cli/drpcli\_endpoints\_patch.rst M
> doc/cli/drpcli\_endpoints\_remove.rst M
> doc/cli/drpcli\_endpoints\_runaction.rst M
> doc/cli/drpcli\_endpoints\_set.rst M
> doc/cli/drpcli\_endpoints\_show.rst M
> doc/cli/drpcli\_endpoints\_update.rst M
> doc/cli/drpcli\_endpoints\_uploadiso.rst M
> doc/cli/drpcli\_endpoints\_wait.rst M doc/cli/drpcli\_events.rst M
> doc/cli/drpcli\_events\_post.rst M doc/cli/drpcli\_events\_watch.rst M
> doc/cli/drpcli\_extended.rst M doc/cli/drpcli\_extended\_action.rst M
> doc/cli/drpcli\_extended\_actions.rst M
> doc/cli/drpcli\_extended\_add.rst M
> doc/cli/drpcli\_extended\_await.rst M
> doc/cli/drpcli\_extended\_count.rst M
> doc/cli/drpcli\_extended\_create.rst M
> doc/cli/drpcli\_extended\_destroy.rst M
> doc/cli/drpcli\_extended\_etag.rst M
> doc/cli/drpcli\_extended\_exists.rst M
> doc/cli/drpcli\_extended\_get.rst M
> doc/cli/drpcli\_extended\_indexes.rst M
> doc/cli/drpcli\_extended\_list.rst M
> doc/cli/drpcli\_extended\_meta.rst M
> doc/cli/drpcli\_extended\_meta\_add.rst M
> doc/cli/drpcli\_extended\_meta\_get.rst M
> doc/cli/drpcli\_extended\_meta\_remove.rst M
> doc/cli/drpcli\_extended\_meta\_set.rst M
> doc/cli/drpcli\_extended\_params.rst M
> doc/cli/drpcli\_extended\_patch.rst M
> doc/cli/drpcli\_extended\_remove.rst M
> doc/cli/drpcli\_extended\_runaction.rst M
> doc/cli/drpcli\_extended\_set.rst M doc/cli/drpcli\_extended\_show.rst
> M doc/cli/drpcli\_extended\_update.rst M
> doc/cli/drpcli\_extended\_uploadiso.rst M
> doc/cli/drpcli\_extended\_wait.rst M doc/cli/drpcli\_files.rst M
> doc/cli/drpcli\_files\_certs.rst M
> doc/cli/drpcli\_files\_certs\_get.rst M
> doc/cli/drpcli\_files\_certs\_set.rst M
> doc/cli/drpcli\_files\_destroy.rst M
> doc/cli/drpcli\_files\_download.rst M
> doc/cli/drpcli\_files\_exists.rst M doc/cli/drpcli\_files\_list.rst M
> doc/cli/drpcli\_files\_upload.rst A doc/cli/drpcli\_filters.rst A
> doc/cli/drpcli\_filters\_action.rst A
> doc/cli/drpcli\_filters\_actions.rst A
> doc/cli/drpcli\_filters\_await.rst A
> doc/cli/drpcli\_filters\_count.rst A
> doc/cli/drpcli\_filters\_create.rst A
> doc/cli/drpcli\_filters\_destroy.rst A
> doc/cli/drpcli\_filters\_etag.rst A
> doc/cli/drpcli\_filters\_exists.rst A
> doc/cli/drpcli\_filters\_indexes.rst A
> doc/cli/drpcli\_filters\_list.rst A doc/cli/drpcli\_filters\_patch.rst
> A doc/cli/drpcli\_filters\_runaction.rst A
> doc/cli/drpcli\_filters\_show.rst A
> doc/cli/drpcli\_filters\_update.rst A
> doc/cli/drpcli\_filters\_wait.rst M doc/cli/drpcli\_fingerprint.rst M
> doc/cli/drpcli\_gohai.rst M doc/cli/drpcli\_identity\_providers.rst M
> doc/cli/drpcli\_identity\_providers\_action.rst M
> doc/cli/drpcli\_identity\_providers\_actions.rst M
> doc/cli/drpcli\_identity\_providers\_await.rst M
> doc/cli/drpcli\_identity\_providers\_count.rst M
> doc/cli/drpcli\_identity\_providers\_create.rst M
> doc/cli/drpcli\_identity\_providers\_destroy.rst M
> doc/cli/drpcli\_identity\_providers\_etag.rst M
> doc/cli/drpcli\_identity\_providers\_exists.rst M
> doc/cli/drpcli\_identity\_providers\_indexes.rst M
> doc/cli/drpcli\_identity\_providers\_list.rst M
> doc/cli/drpcli\_identity\_providers\_meta.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_add.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_get.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_set.rst M
> doc/cli/drpcli\_identity\_providers\_patch.rst M
> doc/cli/drpcli\_identity\_providers\_runaction.rst M
> doc/cli/drpcli\_identity\_providers\_show.rst M
> doc/cli/drpcli\_identity\_providers\_update.rst M
> doc/cli/drpcli\_identity\_providers\_wait.rst M
> doc/cli/drpcli\_info.rst M doc/cli/drpcli\_info\_check.rst M
> doc/cli/drpcli\_info\_get.rst M doc/cli/drpcli\_info\_status.rst M
> doc/cli/drpcli\_interfaces.rst M doc/cli/drpcli\_interfaces\_await.rst
> M doc/cli/drpcli\_interfaces\_count.rst M
> doc/cli/drpcli\_interfaces\_etag.rst M
> doc/cli/drpcli\_interfaces\_exists.rst M
> doc/cli/drpcli\_interfaces\_indexes.rst M
> doc/cli/drpcli\_interfaces\_list.rst M
> doc/cli/drpcli\_interfaces\_show.rst M
> doc/cli/drpcli\_interfaces\_wait.rst M doc/cli/drpcli\_isos.rst M
> doc/cli/drpcli\_isos\_certs.rst M doc/cli/drpcli\_isos\_certs\_get.rst
> M doc/cli/drpcli\_isos\_certs\_set.rst M
> doc/cli/drpcli\_isos\_destroy.rst M doc/cli/drpcli\_isos\_download.rst
> M doc/cli/drpcli\_isos\_exists.rst M doc/cli/drpcli\_isos\_list.rst M
> doc/cli/drpcli\_isos\_upload.rst M doc/cli/drpcli\_jobs.rst M
> doc/cli/drpcli\_jobs\_actions.rst M doc/cli/drpcli\_jobs\_await.rst M
> doc/cli/drpcli\_jobs\_count.rst M doc/cli/drpcli\_jobs\_create.rst M
> doc/cli/drpcli\_jobs\_destroy.rst M doc/cli/drpcli\_jobs\_etag.rst M
> doc/cli/drpcli\_jobs\_exists.rst M doc/cli/drpcli\_jobs\_indexes.rst M
> doc/cli/drpcli\_jobs\_list.rst M doc/cli/drpcli\_jobs\_log.rst M
> doc/cli/drpcli\_jobs\_meta.rst M doc/cli/drpcli\_jobs\_meta\_add.rst M
> doc/cli/drpcli\_jobs\_meta\_get.rst M
> doc/cli/drpcli\_jobs\_meta\_remove.rst M
> doc/cli/drpcli\_jobs\_meta\_set.rst M doc/cli/drpcli\_jobs\_patch.rst
> M doc/cli/drpcli\_jobs\_plugin\_action.rst M
> doc/cli/drpcli\_jobs\_plugin\_actions.rst M
> doc/cli/drpcli\_jobs\_purge.rst M
> doc/cli/drpcli\_jobs\_runplugin\_action.rst M
> doc/cli/drpcli\_jobs\_show.rst M doc/cli/drpcli\_jobs\_unpack.rst M
> doc/cli/drpcli\_jobs\_update.rst M doc/cli/drpcli\_jobs\_wait.rst M
> doc/cli/drpcli\_labs.rst M doc/cli/drpcli\_labs\_document.rst M
> doc/cli/drpcli\_labs\_validate.rst M doc/cli/drpcli\_leases.rst M
> doc/cli/drpcli\_leases\_action.rst M
> doc/cli/drpcli\_leases\_actions.rst M
> doc/cli/drpcli\_leases\_await.rst M doc/cli/drpcli\_leases\_count.rst
> M doc/cli/drpcli\_leases\_destroy.rst M
> doc/cli/drpcli\_leases\_etag.rst M doc/cli/drpcli\_leases\_exists.rst
> M doc/cli/drpcli\_leases\_indexes.rst M
> doc/cli/drpcli\_leases\_list.rst M doc/cli/drpcli\_leases\_meta.rst M
> doc/cli/drpcli\_leases\_meta\_add.rst M
> doc/cli/drpcli\_leases\_meta\_get.rst M
> doc/cli/drpcli\_leases\_meta\_remove.rst M
> doc/cli/drpcli\_leases\_meta\_set.rst M
> doc/cli/drpcli\_leases\_runaction.rst M
> doc/cli/drpcli\_leases\_show.rst M doc/cli/drpcli\_leases\_wait.rst M
> doc/cli/drpcli\_logs.rst M doc/cli/drpcli\_logs\_get.rst M
> doc/cli/drpcli\_logs\_watch.rst M doc/cli/drpcli\_machines.rst M
> doc/cli/drpcli\_machines\_action.rst M
> doc/cli/drpcli\_machines\_actions.rst M
> doc/cli/drpcli\_machines\_add.rst M
> doc/cli/drpcli\_machines\_addprofile.rst M
> doc/cli/drpcli\_machines\_addtask.rst M
> doc/cli/drpcli\_machines\_await.rst M
> doc/cli/drpcli\_machines\_bootenv.rst M
> doc/cli/drpcli\_machines\_cleanup.rst M
> doc/cli/drpcli\_machines\_count.rst M
> doc/cli/drpcli\_machines\_create.rst M
> doc/cli/drpcli\_machines\_currentlog.rst M
> doc/cli/drpcli\_machines\_deletejobs.rst M
> doc/cli/drpcli\_machines\_deleteworkorders.rst M
> doc/cli/drpcli\_machines\_destroy.rst M
> doc/cli/drpcli\_machines\_etag.rst M
> doc/cli/drpcli\_machines\_exists.rst M
> doc/cli/drpcli\_machines\_get.rst M
> doc/cli/drpcli\_machines\_indexes.rst M
> doc/cli/drpcli\_machines\_inserttask.rst M
> doc/cli/drpcli\_machines\_inspect.rst M
> doc/cli/drpcli\_machines\_inspect\_jobs.rst M
> doc/cli/drpcli\_machines\_inspect\_tasks.rst M
> doc/cli/drpcli\_machines\_jobs.rst M
> doc/cli/drpcli\_machines\_jobs\_create.rst M
> doc/cli/drpcli\_machines\_jobs\_current.rst M
> doc/cli/drpcli\_machines\_jobs\_state.rst M
> doc/cli/drpcli\_machines\_list.rst M
> doc/cli/drpcli\_machines\_meta.rst M
> doc/cli/drpcli\_machines\_meta\_add.rst M
> doc/cli/drpcli\_machines\_meta\_get.rst M
> doc/cli/drpcli\_machines\_meta\_remove.rst M
> doc/cli/drpcli\_machines\_meta\_set.rst M
> doc/cli/drpcli\_machines\_migrate.rst M
> doc/cli/drpcli\_machines\_migrateTicket.rst M
> doc/cli/drpcli\_machines\_params.rst M
> doc/cli/drpcli\_machines\_patch.rst M
> doc/cli/drpcli\_machines\_pause.rst M
> doc/cli/drpcli\_machines\_processjobs.rst M
> doc/cli/drpcli\_machines\_releaseToPool.rst M
> doc/cli/drpcli\_machines\_remove.rst M
> doc/cli/drpcli\_machines\_removeprofile.rst M
> doc/cli/drpcli\_machines\_removetask.rst M
> doc/cli/drpcli\_machines\_run.rst M
> doc/cli/drpcli\_machines\_runaction.rst M
> doc/cli/drpcli\_machines\_set.rst M doc/cli/drpcli\_machines\_show.rst
> M doc/cli/drpcli\_machines\_stage.rst M
> doc/cli/drpcli\_machines\_start.rst M
> doc/cli/drpcli\_machines\_tasks.rst M
> doc/cli/drpcli\_machines\_tasks\_add.rst M
> doc/cli/drpcli\_machines\_tasks\_del.rst M
> doc/cli/drpcli\_machines\_update.rst M
> doc/cli/drpcli\_machines\_uploadiso.rst M
> doc/cli/drpcli\_machines\_wait.rst M
> doc/cli/drpcli\_machines\_whoami.rst M
> doc/cli/drpcli\_machines\_work\_order.rst M
> doc/cli/drpcli\_machines\_work\_order\_add.rst M
> doc/cli/drpcli\_machines\_work\_order\_off.rst M
> doc/cli/drpcli\_machines\_work\_order\_on.rst M
> doc/cli/drpcli\_machines\_workflow.rst M doc/cli/drpcli\_net.rst M
> doc/cli/drpcli\_net\_autogen.rst M doc/cli/drpcli\_net\_compile.rst M
> doc/cli/drpcli\_net\_generate.rst M doc/cli/drpcli\_net\_phys.rst M
> doc/cli/drpcli\_objects.rst M doc/cli/drpcli\_objects\_list.rst M
> doc/cli/drpcli\_params.rst M doc/cli/drpcli\_params\_await.rst M
> doc/cli/drpcli\_params\_count.rst M doc/cli/drpcli\_params\_create.rst
> M doc/cli/drpcli\_params\_destroy.rst M
> doc/cli/drpcli\_params\_etag.rst M doc/cli/drpcli\_params\_exists.rst
> M doc/cli/drpcli\_params\_indexes.rst M
> doc/cli/drpcli\_params\_list.rst M doc/cli/drpcli\_params\_meta.rst M
> doc/cli/drpcli\_params\_meta\_add.rst M
> doc/cli/drpcli\_params\_meta\_get.rst M
> doc/cli/drpcli\_params\_meta\_remove.rst M
> doc/cli/drpcli\_params\_meta\_set.rst M
> doc/cli/drpcli\_params\_patch.rst M doc/cli/drpcli\_params\_show.rst M
> doc/cli/drpcli\_params\_update.rst M doc/cli/drpcli\_params\_wait.rst
> M doc/cli/drpcli\_plugin\_providers.rst M
> doc/cli/drpcli\_plugin\_providers\_await.rst M
> doc/cli/drpcli\_plugin\_providers\_count.rst M
> doc/cli/drpcli\_plugin\_providers\_destroy.rst M
> doc/cli/drpcli\_plugin\_providers\_download.rst M
> doc/cli/drpcli\_plugin\_providers\_etag.rst M
> doc/cli/drpcli\_plugin\_providers\_exists.rst M
> doc/cli/drpcli\_plugin\_providers\_indexes.rst M
> doc/cli/drpcli\_plugin\_providers\_list.rst M
> doc/cli/drpcli\_plugin\_providers\_meta.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_add.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_get.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_set.rst M
> doc/cli/drpcli\_plugin\_providers\_show.rst M
> doc/cli/drpcli\_plugin\_providers\_upload.rst M
> doc/cli/drpcli\_plugin\_providers\_wait.rst M
> doc/cli/drpcli\_plugins.rst M doc/cli/drpcli\_plugins\_action.rst M
> doc/cli/drpcli\_plugins\_actions.rst M
> doc/cli/drpcli\_plugins\_add.rst M doc/cli/drpcli\_plugins\_await.rst
> M doc/cli/drpcli\_plugins\_count.rst M
> doc/cli/drpcli\_plugins\_create.rst M
> doc/cli/drpcli\_plugins\_destroy.rst M
> doc/cli/drpcli\_plugins\_etag.rst M
> doc/cli/drpcli\_plugins\_exists.rst M doc/cli/drpcli\_plugins\_get.rst
> M doc/cli/drpcli\_plugins\_indexes.rst M
> doc/cli/drpcli\_plugins\_list.rst M doc/cli/drpcli\_plugins\_meta.rst
> M doc/cli/drpcli\_plugins\_meta\_add.rst M
> doc/cli/drpcli\_plugins\_meta\_get.rst M
> doc/cli/drpcli\_plugins\_meta\_remove.rst M
> doc/cli/drpcli\_plugins\_meta\_set.rst M
> doc/cli/drpcli\_plugins\_params.rst M
> doc/cli/drpcli\_plugins\_patch.rst M
> doc/cli/drpcli\_plugins\_remove.rst M
> doc/cli/drpcli\_plugins\_runaction.rst M
> doc/cli/drpcli\_plugins\_set.rst M doc/cli/drpcli\_plugins\_show.rst M
> doc/cli/drpcli\_plugins\_update.rst M
> doc/cli/drpcli\_plugins\_uploadiso.rst M
> doc/cli/drpcli\_plugins\_wait.rst M doc/cli/drpcli\_pools.rst M
> doc/cli/drpcli\_pools\_action.rst M doc/cli/drpcli\_pools\_actions.rst
> M doc/cli/drpcli\_pools\_active.rst M doc/cli/drpcli\_pools\_await.rst
> M doc/cli/drpcli\_pools\_count.rst M doc/cli/drpcli\_pools\_create.rst
> M doc/cli/drpcli\_pools\_destroy.rst M doc/cli/drpcli\_pools\_etag.rst
> M doc/cli/drpcli\_pools\_exists.rst M
> doc/cli/drpcli\_pools\_indexes.rst M doc/cli/drpcli\_pools\_list.rst M
> doc/cli/drpcli\_pools\_manage.rst M
> doc/cli/drpcli\_pools\_manage\_add.rst M
> doc/cli/drpcli\_pools\_manage\_allocate.rst M
> doc/cli/drpcli\_pools\_manage\_release.rst M
> doc/cli/drpcli\_pools\_manage\_remove.rst M
> doc/cli/drpcli\_pools\_patch.rst M
> doc/cli/drpcli\_pools\_runaction.rst M doc/cli/drpcli\_pools\_show.rst
> M doc/cli/drpcli\_pools\_status.rst M
> doc/cli/drpcli\_pools\_update.rst M doc/cli/drpcli\_pools\_wait.rst M
> doc/cli/drpcli\_preflight.rst M
> doc/cli/drpcli\_preflight\_checkports.rst M doc/cli/drpcli\_prefs.rst
> M doc/cli/drpcli\_prefs\_list.rst M doc/cli/drpcli\_prefs\_set.rst M
> doc/cli/drpcli\_profiles.rst M doc/cli/drpcli\_profiles\_action.rst M
> doc/cli/drpcli\_profiles\_actions.rst M
> doc/cli/drpcli\_profiles\_add.rst M
> doc/cli/drpcli\_profiles\_addprofile.rst M
> doc/cli/drpcli\_profiles\_await.rst M
> doc/cli/drpcli\_profiles\_count.rst M
> doc/cli/drpcli\_profiles\_create.rst M
> doc/cli/drpcli\_profiles\_destroy.rst M
> doc/cli/drpcli\_profiles\_etag.rst M
> doc/cli/drpcli\_profiles\_exists.rst M
> doc/cli/drpcli\_profiles\_genbootenv.rst M
> doc/cli/drpcli\_profiles\_get.rst M
> doc/cli/drpcli\_profiles\_indexes.rst M
> doc/cli/drpcli\_profiles\_list.rst M
> doc/cli/drpcli\_profiles\_meta.rst M
> doc/cli/drpcli\_profiles\_meta\_add.rst M
> doc/cli/drpcli\_profiles\_meta\_get.rst M
> doc/cli/drpcli\_profiles\_meta\_remove.rst M
> doc/cli/drpcli\_profiles\_meta\_set.rst M
> doc/cli/drpcli\_profiles\_params.rst M
> doc/cli/drpcli\_profiles\_patch.rst M
> doc/cli/drpcli\_profiles\_remove.rst M
> doc/cli/drpcli\_profiles\_removeprofile.rst M
> doc/cli/drpcli\_profiles\_runaction.rst M
> doc/cli/drpcli\_profiles\_set.rst M doc/cli/drpcli\_profiles\_show.rst
> M doc/cli/drpcli\_profiles\_update.rst M
> doc/cli/drpcli\_profiles\_uploadiso.rst M
> doc/cli/drpcli\_profiles\_wait.rst M doc/cli/drpcli\_proxy.rst M
> doc/cli/drpcli\_reservations.rst M
> doc/cli/drpcli\_reservations\_action.rst M
> doc/cli/drpcli\_reservations\_actions.rst M
> doc/cli/drpcli\_reservations\_await.rst M
> doc/cli/drpcli\_reservations\_count.rst M
> doc/cli/drpcli\_reservations\_create.rst M
> doc/cli/drpcli\_reservations\_destroy.rst M
> doc/cli/drpcli\_reservations\_etag.rst M
> doc/cli/drpcli\_reservations\_exists.rst M
> doc/cli/drpcli\_reservations\_indexes.rst M
> doc/cli/drpcli\_reservations\_list.rst M
> doc/cli/drpcli\_reservations\_meta.rst M
> doc/cli/drpcli\_reservations\_meta\_add.rst M
> doc/cli/drpcli\_reservations\_meta\_get.rst M
> doc/cli/drpcli\_reservations\_meta\_remove.rst M
> doc/cli/drpcli\_reservations\_meta\_set.rst M
> doc/cli/drpcli\_reservations\_patch.rst M
> doc/cli/drpcli\_reservations\_runaction.rst M
> doc/cli/drpcli\_reservations\_show.rst M
> doc/cli/drpcli\_reservations\_update.rst M
> doc/cli/drpcli\_reservations\_wait.rst M
> doc/cli/drpcli\_resource\_brokers.rst M
> doc/cli/drpcli\_resource\_brokers\_action.rst M
> doc/cli/drpcli\_resource\_brokers\_actions.rst M
> doc/cli/drpcli\_resource\_brokers\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_addprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_addtask.rst M
> doc/cli/drpcli\_resource\_brokers\_await.rst M
> doc/cli/drpcli\_resource\_brokers\_bootenv.rst M
> doc/cli/drpcli\_resource\_brokers\_cleanup.rst M
> doc/cli/drpcli\_resource\_brokers\_count.rst M
> doc/cli/drpcli\_resource\_brokers\_create.rst M
> doc/cli/drpcli\_resource\_brokers\_currentlog.rst M
> doc/cli/drpcli\_resource\_brokers\_deletejobs.rst M
> doc/cli/drpcli\_resource\_brokers\_destroy.rst M
> doc/cli/drpcli\_resource\_brokers\_etag.rst M
> doc/cli/drpcli\_resource\_brokers\_exists.rst M
> doc/cli/drpcli\_resource\_brokers\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_group.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_addprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_params.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_removeprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_uploadiso.rst M
> doc/cli/drpcli\_resource\_brokers\_indexes.rst M
> doc/cli/drpcli\_resource\_brokers\_inserttask.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect\_jobs.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect\_tasks.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_create.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_current.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_state.rst M
> doc/cli/drpcli\_resource\_brokers\_list.rst M
> doc/cli/drpcli\_resource\_brokers\_meta.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_params.rst M
> doc/cli/drpcli\_resource\_brokers\_patch.rst M
> doc/cli/drpcli\_resource\_brokers\_pause.rst M
> doc/cli/drpcli\_resource\_brokers\_processjobs.rst M
> doc/cli/drpcli\_resource\_brokers\_releaseToPool.rst M
> doc/cli/drpcli\_resource\_brokers\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_removeprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_removetask.rst M
> doc/cli/drpcli\_resource\_brokers\_run.rst M
> doc/cli/drpcli\_resource\_brokers\_runaction.rst M
> doc/cli/drpcli\_resource\_brokers\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_show.rst M
> doc/cli/drpcli\_resource\_brokers\_stage.rst M
> doc/cli/drpcli\_resource\_brokers\_start.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks\_del.rst M
> doc/cli/drpcli\_resource\_brokers\_update.rst M
> doc/cli/drpcli\_resource\_brokers\_uploadiso.rst M
> doc/cli/drpcli\_resource\_brokers\_wait.rst M
> doc/cli/drpcli\_resource\_brokers\_whoami.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_off.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_on.rst M
> doc/cli/drpcli\_resource\_brokers\_workflow.rst M
> doc/cli/drpcli\_roles.rst M doc/cli/drpcli\_roles\_await.rst M
> doc/cli/drpcli\_roles\_count.rst M doc/cli/drpcli\_roles\_create.rst M
> doc/cli/drpcli\_roles\_destroy.rst M doc/cli/drpcli\_roles\_etag.rst M
> doc/cli/drpcli\_roles\_exists.rst M doc/cli/drpcli\_roles\_indexes.rst
> M doc/cli/drpcli\_roles\_list.rst M doc/cli/drpcli\_roles\_meta.rst M
> doc/cli/drpcli\_roles\_meta\_add.rst M
> doc/cli/drpcli\_roles\_meta\_get.rst M
> doc/cli/drpcli\_roles\_meta\_remove.rst M
> doc/cli/drpcli\_roles\_meta\_set.rst M
> doc/cli/drpcli\_roles\_patch.rst M doc/cli/drpcli\_roles\_show.rst M
> doc/cli/drpcli\_roles\_update.rst M doc/cli/drpcli\_roles\_wait.rst M
> doc/cli/drpcli\_stages.rst M doc/cli/drpcli\_stages\_action.rst M
> doc/cli/drpcli\_stages\_actions.rst M doc/cli/drpcli\_stages\_add.rst
> M doc/cli/drpcli\_stages\_addprofile.rst M
> doc/cli/drpcli\_stages\_addtask.rst M
> doc/cli/drpcli\_stages\_await.rst M
> doc/cli/drpcli\_stages\_bootenv.rst M
> doc/cli/drpcli\_stages\_count.rst M doc/cli/drpcli\_stages\_create.rst
> M doc/cli/drpcli\_stages\_destroy.rst M
> doc/cli/drpcli\_stages\_etag.rst M doc/cli/drpcli\_stages\_exists.rst
> M doc/cli/drpcli\_stages\_get.rst M
> doc/cli/drpcli\_stages\_indexes.rst M doc/cli/drpcli\_stages\_list.rst
> M doc/cli/drpcli\_stages\_meta.rst M
> doc/cli/drpcli\_stages\_meta\_add.rst M
> doc/cli/drpcli\_stages\_meta\_get.rst M
> doc/cli/drpcli\_stages\_meta\_remove.rst M
> doc/cli/drpcli\_stages\_meta\_set.rst M
> doc/cli/drpcli\_stages\_params.rst M doc/cli/drpcli\_stages\_patch.rst
> M doc/cli/drpcli\_stages\_remove.rst M
> doc/cli/drpcli\_stages\_removeprofile.rst M
> doc/cli/drpcli\_stages\_removetask.rst M
> doc/cli/drpcli\_stages\_runaction.rst M
> doc/cli/drpcli\_stages\_set.rst M doc/cli/drpcli\_stages\_show.rst M
> doc/cli/drpcli\_stages\_update.rst M
> doc/cli/drpcli\_stages\_uploadiso.rst M
> doc/cli/drpcli\_stages\_wait.rst M doc/cli/drpcli\_subnets.rst M
> doc/cli/drpcli\_subnets\_action.rst M
> doc/cli/drpcli\_subnets\_actions.rst M
> doc/cli/drpcli\_subnets\_await.rst M
> doc/cli/drpcli\_subnets\_count.rst M
> doc/cli/drpcli\_subnets\_create.rst M
> doc/cli/drpcli\_subnets\_destroy.rst M
> doc/cli/drpcli\_subnets\_etag.rst M
> doc/cli/drpcli\_subnets\_exists.rst M doc/cli/drpcli\_subnets\_get.rst
> M doc/cli/drpcli\_subnets\_indexes.rst M
> doc/cli/drpcli\_subnets\_leasetimes.rst M
> doc/cli/drpcli\_subnets\_list.rst M doc/cli/drpcli\_subnets\_meta.rst
> M doc/cli/drpcli\_subnets\_meta\_add.rst M
> doc/cli/drpcli\_subnets\_meta\_get.rst M
> doc/cli/drpcli\_subnets\_meta\_remove.rst M
> doc/cli/drpcli\_subnets\_meta\_set.rst M
> doc/cli/drpcli\_subnets\_nextserver.rst M
> doc/cli/drpcli\_subnets\_patch.rst M
> doc/cli/drpcli\_subnets\_pickers.rst M
> doc/cli/drpcli\_subnets\_range.rst M
> doc/cli/drpcli\_subnets\_runaction.rst M
> doc/cli/drpcli\_subnets\_set.rst M doc/cli/drpcli\_subnets\_show.rst M
> doc/cli/drpcli\_subnets\_update.rst M
> doc/cli/drpcli\_subnets\_wait.rst M doc/cli/drpcli\_support.rst M
> doc/cli/drpcli\_support\_bundle.rst M
> doc/cli/drpcli\_support\_machine-bundle.rst M
> doc/cli/drpcli\_system.rst M doc/cli/drpcli\_system\_action.rst M
> doc/cli/drpcli\_system\_actions.rst M
> doc/cli/drpcli\_system\_active.rst M doc/cli/drpcli\_system\_certs.rst
> M doc/cli/drpcli\_system\_certs\_get.rst M
> doc/cli/drpcli\_system\_certs\_set.rst M
> doc/cli/drpcli\_system\_ha.rst M
> doc/cli/drpcli\_system\_ha\_active.rst M
> doc/cli/drpcli\_system\_ha\_dump.rst M
> doc/cli/drpcli\_system\_ha\_enroll.rst M
> doc/cli/drpcli\_system\_ha\_failOverSafe.rst M
> doc/cli/drpcli\_system\_ha\_id.rst M
> doc/cli/drpcli\_system\_ha\_introduction.rst M
> doc/cli/drpcli\_system\_ha\_join.rst M
> doc/cli/drpcli\_system\_ha\_leader.rst M
> doc/cli/drpcli\_system\_ha\_peers.rst M
> doc/cli/drpcli\_system\_ha\_remove.rst M
> doc/cli/drpcli\_system\_ha\_showHa.rst M
> doc/cli/drpcli\_system\_ha\_state.rst M
> doc/cli/drpcli\_system\_passive.rst M
> doc/cli/drpcli\_system\_runaction.rst M
> doc/cli/drpcli\_system\_signurl.rst M
> doc/cli/drpcli\_system\_upgrade.rst M
> doc/cli/drpcli\_system\_upgrade\_remove.rst M
> doc/cli/drpcli\_system\_upgrade\_stage.rst M
> doc/cli/drpcli\_system\_upgrade\_start.rst M
> doc/cli/drpcli\_system\_upgrade\_status.rst M
> doc/cli/drpcli\_tasks.rst M doc/cli/drpcli\_tasks\_action.rst M
> doc/cli/drpcli\_tasks\_actions.rst M doc/cli/drpcli\_tasks\_await.rst
> M doc/cli/drpcli\_tasks\_count.rst M doc/cli/drpcli\_tasks\_create.rst
> M doc/cli/drpcli\_tasks\_destroy.rst M doc/cli/drpcli\_tasks\_etag.rst
> M doc/cli/drpcli\_tasks\_exists.rst M
> doc/cli/drpcli\_tasks\_indexes.rst M doc/cli/drpcli\_tasks\_list.rst M
> doc/cli/drpcli\_tasks\_meta.rst M doc/cli/drpcli\_tasks\_meta\_add.rst
> M doc/cli/drpcli\_tasks\_meta\_get.rst M
> doc/cli/drpcli\_tasks\_meta\_remove.rst M
> doc/cli/drpcli\_tasks\_meta\_set.rst M
> doc/cli/drpcli\_tasks\_patch.rst M
> doc/cli/drpcli\_tasks\_runaction.rst M doc/cli/drpcli\_tasks\_show.rst
> M doc/cli/drpcli\_tasks\_update.rst M doc/cli/drpcli\_tasks\_wait.rst
> M doc/cli/drpcli\_templates.rst M
> doc/cli/drpcli\_templates\_action.rst M
> doc/cli/drpcli\_templates\_actions.rst M
> doc/cli/drpcli\_templates\_await.rst M
> doc/cli/drpcli\_templates\_count.rst M
> doc/cli/drpcli\_templates\_create.rst M
> doc/cli/drpcli\_templates\_destroy.rst M
> doc/cli/drpcli\_templates\_etag.rst M
> doc/cli/drpcli\_templates\_exists.rst M
> doc/cli/drpcli\_templates\_indexes.rst M
> doc/cli/drpcli\_templates\_list.rst M
> doc/cli/drpcli\_templates\_meta.rst M
> doc/cli/drpcli\_templates\_meta\_add.rst M
> doc/cli/drpcli\_templates\_meta\_get.rst M
> doc/cli/drpcli\_templates\_meta\_remove.rst M
> doc/cli/drpcli\_templates\_meta\_set.rst M
> doc/cli/drpcli\_templates\_patch.rst M
> doc/cli/drpcli\_templates\_runaction.rst M
> doc/cli/drpcli\_templates\_show.rst M
> doc/cli/drpcli\_templates\_update.rst M
> doc/cli/drpcli\_templates\_upload.rst M
> doc/cli/drpcli\_templates\_wait.rst M doc/cli/drpcli\_tenants.rst M
> doc/cli/drpcli\_tenants\_await.rst M
> doc/cli/drpcli\_tenants\_count.rst M
> doc/cli/drpcli\_tenants\_create.rst M
> doc/cli/drpcli\_tenants\_destroy.rst M
> doc/cli/drpcli\_tenants\_etag.rst M
> doc/cli/drpcli\_tenants\_exists.rst M
> doc/cli/drpcli\_tenants\_indexes.rst M
> doc/cli/drpcli\_tenants\_list.rst M doc/cli/drpcli\_tenants\_meta.rst
> M doc/cli/drpcli\_tenants\_meta\_add.rst M
> doc/cli/drpcli\_tenants\_meta\_get.rst M
> doc/cli/drpcli\_tenants\_meta\_remove.rst M
> doc/cli/drpcli\_tenants\_meta\_set.rst M
> doc/cli/drpcli\_tenants\_patch.rst M doc/cli/drpcli\_tenants\_show.rst
> M doc/cli/drpcli\_tenants\_update.rst M
> doc/cli/drpcli\_tenants\_wait.rst M
> doc/cli/drpcli\_trigger\_providers.rst M
> doc/cli/drpcli\_trigger\_providers\_action.rst M
> doc/cli/drpcli\_trigger\_providers\_actions.rst M
> doc/cli/drpcli\_trigger\_providers\_add.rst M
> doc/cli/drpcli\_trigger\_providers\_addprofile.rst M
> doc/cli/drpcli\_trigger\_providers\_await.rst M
> doc/cli/drpcli\_trigger\_providers\_count.rst M
> doc/cli/drpcli\_trigger\_providers\_create.rst M
> doc/cli/drpcli\_trigger\_providers\_destroy.rst M
> doc/cli/drpcli\_trigger\_providers\_etag.rst M
> doc/cli/drpcli\_trigger\_providers\_exists.rst M
> doc/cli/drpcli\_trigger\_providers\_get.rst M
> doc/cli/drpcli\_trigger\_providers\_indexes.rst M
> doc/cli/drpcli\_trigger\_providers\_list.rst M
> doc/cli/drpcli\_trigger\_providers\_meta.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_add.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_get.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_set.rst M
> doc/cli/drpcli\_trigger\_providers\_params.rst M
> doc/cli/drpcli\_trigger\_providers\_patch.rst M
> doc/cli/drpcli\_trigger\_providers\_remove.rst M
> doc/cli/drpcli\_trigger\_providers\_removeprofile.rst M
> doc/cli/drpcli\_trigger\_providers\_runaction.rst M
> doc/cli/drpcli\_trigger\_providers\_set.rst M
> doc/cli/drpcli\_trigger\_providers\_show.rst M
> doc/cli/drpcli\_trigger\_providers\_update.rst M
> doc/cli/drpcli\_trigger\_providers\_uploadiso.rst M
> doc/cli/drpcli\_trigger\_providers\_wait.rst M
> doc/cli/drpcli\_triggers.rst M doc/cli/drpcli\_triggers\_action.rst M
> doc/cli/drpcli\_triggers\_actions.rst M
> doc/cli/drpcli\_triggers\_add.rst M
> doc/cli/drpcli\_triggers\_addprofile.rst M
> doc/cli/drpcli\_triggers\_await.rst M
> doc/cli/drpcli\_triggers\_count.rst M
> doc/cli/drpcli\_triggers\_create.rst M
> doc/cli/drpcli\_triggers\_destroy.rst M
> doc/cli/drpcli\_triggers\_etag.rst M
> doc/cli/drpcli\_triggers\_exists.rst M
> doc/cli/drpcli\_triggers\_get.rst M
> doc/cli/drpcli\_triggers\_indexes.rst M
> doc/cli/drpcli\_triggers\_list.rst M
> doc/cli/drpcli\_triggers\_meta.rst M
> doc/cli/drpcli\_triggers\_meta\_add.rst M
> doc/cli/drpcli\_triggers\_meta\_get.rst M
> doc/cli/drpcli\_triggers\_meta\_remove.rst M
> doc/cli/drpcli\_triggers\_meta\_set.rst M
> doc/cli/drpcli\_triggers\_params.rst M
> doc/cli/drpcli\_triggers\_patch.rst M
> doc/cli/drpcli\_triggers\_remove.rst M
> doc/cli/drpcli\_triggers\_removeprofile.rst M
> doc/cli/drpcli\_triggers\_runaction.rst M
> doc/cli/drpcli\_triggers\_set.rst M doc/cli/drpcli\_triggers\_show.rst
> M doc/cli/drpcli\_triggers\_update.rst M
> doc/cli/drpcli\_triggers\_uploadiso.rst M
> doc/cli/drpcli\_triggers\_wait.rst M doc/cli/drpcli\_users.rst M
> doc/cli/drpcli\_users\_action.rst M doc/cli/drpcli\_users\_actions.rst
> M doc/cli/drpcli\_users\_await.rst M doc/cli/drpcli\_users\_count.rst
> M doc/cli/drpcli\_users\_create.rst M
> doc/cli/drpcli\_users\_destroy.rst M doc/cli/drpcli\_users\_etag.rst M
> doc/cli/drpcli\_users\_exists.rst M doc/cli/drpcli\_users\_indexes.rst
> M doc/cli/drpcli\_users\_list.rst M doc/cli/drpcli\_users\_meta.rst M
> doc/cli/drpcli\_users\_meta\_add.rst M
> doc/cli/drpcli\_users\_meta\_get.rst M
> doc/cli/drpcli\_users\_meta\_remove.rst M
> doc/cli/drpcli\_users\_meta\_set.rst M
> doc/cli/drpcli\_users\_password.rst M
> doc/cli/drpcli\_users\_passwordhash.rst M
> doc/cli/drpcli\_users\_patch.rst M
> doc/cli/drpcli\_users\_runaction.rst M doc/cli/drpcli\_users\_show.rst
> M doc/cli/drpcli\_users\_token.rst M doc/cli/drpcli\_users\_update.rst
> M doc/cli/drpcli\_users\_wait.rst A doc/cli/drpcli\_ux\_options.rst A
> doc/cli/drpcli\_ux\_options\_action.rst A
> doc/cli/drpcli\_ux\_options\_actions.rst A
> doc/cli/drpcli\_ux\_options\_await.rst A
> doc/cli/drpcli\_ux\_options\_count.rst A
> doc/cli/drpcli\_ux\_options\_create.rst A
> doc/cli/drpcli\_ux\_options\_destroy.rst A
> doc/cli/drpcli\_ux\_options\_etag.rst A
> doc/cli/drpcli\_ux\_options\_exists.rst A
> doc/cli/drpcli\_ux\_options\_indexes.rst A
> doc/cli/drpcli\_ux\_options\_list.rst A
> doc/cli/drpcli\_ux\_options\_patch.rst A
> doc/cli/drpcli\_ux\_options\_runaction.rst A
> doc/cli/drpcli\_ux\_options\_show.rst A
> doc/cli/drpcli\_ux\_options\_update.rst A
> doc/cli/drpcli\_ux\_options\_wait.rst A
> doc/cli/drpcli\_ux\_settings.rst A
> doc/cli/drpcli\_ux\_settings\_action.rst A
> doc/cli/drpcli\_ux\_settings\_actions.rst A
> doc/cli/drpcli\_ux\_settings\_await.rst A
> doc/cli/drpcli\_ux\_settings\_count.rst A
> doc/cli/drpcli\_ux\_settings\_create.rst A
> doc/cli/drpcli\_ux\_settings\_destroy.rst A
> doc/cli/drpcli\_ux\_settings\_etag.rst A
> doc/cli/drpcli\_ux\_settings\_exists.rst A
> doc/cli/drpcli\_ux\_settings\_indexes.rst A
> doc/cli/drpcli\_ux\_settings\_list.rst A
> doc/cli/drpcli\_ux\_settings\_patch.rst A
> doc/cli/drpcli\_ux\_settings\_runaction.rst A
> doc/cli/drpcli\_ux\_settings\_show.rst A
> doc/cli/drpcli\_ux\_settings\_update.rst A
> doc/cli/drpcli\_ux\_settings\_wait.rst A doc/cli/drpcli\_ux\_views.rst
> A doc/cli/drpcli\_ux\_views\_action.rst A
> doc/cli/drpcli\_ux\_views\_actions.rst A
> doc/cli/drpcli\_ux\_views\_await.rst A
> doc/cli/drpcli\_ux\_views\_count.rst A
> doc/cli/drpcli\_ux\_views\_create.rst A
> doc/cli/drpcli\_ux\_views\_destroy.rst A
> doc/cli/drpcli\_ux\_views\_etag.rst A
> doc/cli/drpcli\_ux\_views\_exists.rst A
> doc/cli/drpcli\_ux\_views\_indexes.rst A
> doc/cli/drpcli\_ux\_views\_list.rst A
> doc/cli/drpcli\_ux\_views\_patch.rst A
> doc/cli/drpcli\_ux\_views\_runaction.rst A
> doc/cli/drpcli\_ux\_views\_show.rst A
> doc/cli/drpcli\_ux\_views\_update.rst A
> doc/cli/drpcli\_ux\_views\_wait.rst M doc/cli/drpcli\_version.rst M
> doc/cli/drpcli\_version\_sets.rst M
> doc/cli/drpcli\_version\_sets\_action.rst M
> doc/cli/drpcli\_version\_sets\_actions.rst M
> doc/cli/drpcli\_version\_sets\_await.rst M
> doc/cli/drpcli\_version\_sets\_count.rst M
> doc/cli/drpcli\_version\_sets\_create.rst M
> doc/cli/drpcli\_version\_sets\_destroy.rst M
> doc/cli/drpcli\_version\_sets\_etag.rst M
> doc/cli/drpcli\_version\_sets\_exists.rst M
> doc/cli/drpcli\_version\_sets\_indexes.rst M
> doc/cli/drpcli\_version\_sets\_list.rst M
> doc/cli/drpcli\_version\_sets\_patch.rst M
> doc/cli/drpcli\_version\_sets\_runaction.rst M
> doc/cli/drpcli\_version\_sets\_show.rst M
> doc/cli/drpcli\_version\_sets\_update.rst M
> doc/cli/drpcli\_version\_sets\_wait.rst M
> doc/cli/drpcli\_work\_orders.rst M
> doc/cli/drpcli\_work\_orders\_action.rst M
> doc/cli/drpcli\_work\_orders\_actions.rst M
> doc/cli/drpcli\_work\_orders\_add.rst M
> doc/cli/drpcli\_work\_orders\_addprofile.rst M
> doc/cli/drpcli\_work\_orders\_addtask.rst M
> doc/cli/drpcli\_work\_orders\_await.rst M
> doc/cli/drpcli\_work\_orders\_count.rst M
> doc/cli/drpcli\_work\_orders\_create.rst M
> doc/cli/drpcli\_work\_orders\_destroy.rst M
> doc/cli/drpcli\_work\_orders\_etag.rst M
> doc/cli/drpcli\_work\_orders\_exists.rst M
> doc/cli/drpcli\_work\_orders\_get.rst M
> doc/cli/drpcli\_work\_orders\_indexes.rst M
> doc/cli/drpcli\_work\_orders\_inserttask.rst M
> doc/cli/drpcli\_work\_orders\_list.rst M
> doc/cli/drpcli\_work\_orders\_meta.rst M
> doc/cli/drpcli\_work\_orders\_meta\_add.rst M
> doc/cli/drpcli\_work\_orders\_meta\_get.rst M
> doc/cli/drpcli\_work\_orders\_meta\_remove.rst M
> doc/cli/drpcli\_work\_orders\_meta\_set.rst M
> doc/cli/drpcli\_work\_orders\_params.rst M
> doc/cli/drpcli\_work\_orders\_patch.rst M
> doc/cli/drpcli\_work\_orders\_purge.rst M
> doc/cli/drpcli\_work\_orders\_remove.rst M
> doc/cli/drpcli\_work\_orders\_removeprofile.rst M
> doc/cli/drpcli\_work\_orders\_removetask.rst M
> doc/cli/drpcli\_work\_orders\_run.rst M
> doc/cli/drpcli\_work\_orders\_runaction.rst M
> doc/cli/drpcli\_work\_orders\_set.rst M
> doc/cli/drpcli\_work\_orders\_show.rst M
> doc/cli/drpcli\_work\_orders\_tasks.rst M
> doc/cli/drpcli\_work\_orders\_tasks\_add.rst M
> doc/cli/drpcli\_work\_orders\_tasks\_del.rst M
> doc/cli/drpcli\_work\_orders\_update.rst M
> doc/cli/drpcli\_work\_orders\_uploadiso.rst M
> doc/cli/drpcli\_work\_orders\_wait.rst M doc/cli/drpcli\_workflows.rst
> M doc/cli/drpcli\_workflows\_action.rst M
> doc/cli/drpcli\_workflows\_actions.rst M
> doc/cli/drpcli\_workflows\_await.rst M
> doc/cli/drpcli\_workflows\_count.rst M
> doc/cli/drpcli\_workflows\_create.rst M
> doc/cli/drpcli\_workflows\_destroy.rst M
> doc/cli/drpcli\_workflows\_etag.rst M
> doc/cli/drpcli\_workflows\_exists.rst M
> doc/cli/drpcli\_workflows\_indexes.rst M
> doc/cli/drpcli\_workflows\_list.rst M
> doc/cli/drpcli\_workflows\_meta.rst M
> doc/cli/drpcli\_workflows\_meta\_add.rst M
> doc/cli/drpcli\_workflows\_meta\_get.rst M
> doc/cli/drpcli\_workflows\_meta\_remove.rst M
> doc/cli/drpcli\_workflows\_meta\_set.rst M
> doc/cli/drpcli\_workflows\_patch.rst M
> doc/cli/drpcli\_workflows\_runaction.rst M
> doc/cli/drpcli\_workflows\_show.rst M
> doc/cli/drpcli\_workflows\_update.rst M
> doc/cli/drpcli\_workflows\_wait.rst M doc/operation.rst A
> doc/operations/ux-views.rst A models/filter.go M models/utils.go A
> models/ux\_option.go A models/ux\_setting.go A models/ux\_view.go
>
> commit da533f3beca7c50bbb01e9b9a8553bb970d9164d Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Sat Sep 3 21:19:34 2022 -0500
>
> > doc: Add google SAML docs
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M doc/operations/saml.rst
>
> commit ea39a55bab516987baaa070b969d3eca5b619256 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Thu Sep 1 00:37:27 2022 +0530
>
> > feat(templates): allow for custom delimiters
>
> M models/bootenv.go M models/template.go M models/templateInfo.go
>
> commit 71254e6c67025837aa134ea662892d17b2b734b8 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Thu Sep 1 14:58:55 2022 -0500
>
> > Derp, fix handling streaming compressed log archive entries
>
> M models/jobLogArchive.go
>
> commit a9029d499e666c4de745d396cf6addf3d9339955 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Tue Aug 30 10:06:21 2022 -0400
>
> > fix(cli): newline for machine migration output
>
> M cli/machines.go
>
> commit ac7d0be512e831b87f9f4b72360f2448d41b24e9 Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 29 15:47:08 2022 -0500
>
> > build: doc update
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M conf.py M tools/build\_rel\_notes.sh A tools/version-docs.sh
>
> commit 89f7e8593146422a9081979f2f9a9252d7b89d45 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Tue Aug 23 16:01:47 2022 -0400
>
> > feat(install-script): install correct version of contents
>
> M cli/catalog.go M tools/install.sh
>
> commit 754686ad7c2d471415eeeafb3d4601ed5efa40df Author: Michael Rice
> \<<michael@michaelrice.org>\> Date: Thu Aug 25 09:36:33 2022 -0500
>
> > doc(kb00009): Added ubuntuhammer passwd
> >
> > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>
>
> M doc/kb/kb-00009.rst
>
> commit 2ca7be902ee8a488787e454839a65ea30bc6d8a3 Author: Michael Rice
> \<<michael@michaelrice.org>\> Date: Mon Aug 22 10:26:10 2022 -0500
>
> > doc(kb): added kb about dhcp ignore
> >
> > Added kb doc about the ignore message users keep thinking is an
> > error
> >
> > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>
>
> A doc/kb/kb-00071.rst
>
> commit f87d90a607250c34f66b068676f51e7bbd5d664d Author: Michael Rice
> \<<michael@michaelrice.org>\> Date: Mon Aug 22 10:26:10 2022 -0500
>
> > doc(kb): added kb about dhcp ignore
> >
> > Added kb doc about the ignore message users keep thinking is an
> > error
> >
> > Signed-off-by: Michael Rice \<<michael@michaelrice.org>\>
>
> A doc/kb/kb-00071.rst
>
> commit f26796631d15c446e1765672c5839b53fa9028bf Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 22 10:20:48 2022 -0500
>
> > feat(triggers): Allow trigger provider to return number of triggers
> > fired
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M models/trigger.go
>
> commit e2aa42cfecb05c29213894c207a03278f22f6cc0 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Thu Jun 30 16:17:46 2022 -0500
>
> > Simplify migrate permissions handling
>
> M cli/machines.go M models/role.go
>
> commit 52db629b069833bb7cb213d8708213ece2b6f655 Author: Victor Lowther
> \<<victor.lowther@gmail.com>\> Date: Tue Jun 28 11:13:57 2022 -0500
>
> > Refactor archive streaming to share mode code.
> >
> > Also have the streams include a final record whenever streaming more
> > than one entry.
>
> M models/jobLogArchive.go
>
> commit 360f62420b736a37a49552c6386ca1a29690048d Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 22 08:20:56 2022 -0500
>
> > feat(cli): Add force new session for debugging unit tests
>
> M cli/startup.go M
> cli/test-data/output/TestCorePieces/gohai.0c113ca6d57519b559ba5a426be3c6b6/stdout.expect
>
> commit 698450224dbe4ed8783855b8b904b9610dbcacdd Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Wed Aug 17 10:31:39 2022 -0500
>
> > feat(cli): Add ignore unix proxy for testing purposes.
>
> M cli/startup.go M
> cli/test-data/output/TestCorePieces/gohai.0c113ca6d57519b559ba5a426be3c6b6/stdout.expect
>
> commit 91d3d39f06f5cf2d31d1038dfd4cea03c214539c Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 15 12:14:55 2022 -0500
>
> > feat(mtls): Add options for cert verification feat(mtls): Add
> > options for client-based certificate authentication
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M api/client.go M cli/startup.go M
> cli/test-data/output/TestCorePieces/gohai.0c113ca6d57519b559ba5a426be3c6b6/stdout.expect
> M doc/cli/drpcli.rst M doc/cli/drpcli\_agent.rst M
> doc/cli/drpcli\_airgap.rst M doc/cli/drpcli\_airgap\_build.rst M
> doc/cli/drpcli\_airgap\_explode.rst M doc/cli/drpcli\_alerts.rst M
> doc/cli/drpcli\_alerts\_ack.rst M doc/cli/drpcli\_alerts\_add.rst M
> doc/cli/drpcli\_alerts\_await.rst M doc/cli/drpcli\_alerts\_count.rst
> M doc/cli/drpcli\_alerts\_create.rst M
> doc/cli/drpcli\_alerts\_destroy.rst M doc/cli/drpcli\_alerts\_etag.rst
> M doc/cli/drpcli\_alerts\_exists.rst M doc/cli/drpcli\_alerts\_get.rst
> M doc/cli/drpcli\_alerts\_indexes.rst M
> doc/cli/drpcli\_alerts\_list.rst M doc/cli/drpcli\_alerts\_params.rst
> M doc/cli/drpcli\_alerts\_patch.rst M doc/cli/drpcli\_alerts\_post.rst
> M doc/cli/drpcli\_alerts\_remove.rst M doc/cli/drpcli\_alerts\_set.rst
> M doc/cli/drpcli\_alerts\_show.rst M
> doc/cli/drpcli\_alerts\_update.rst M
> doc/cli/drpcli\_alerts\_uploadiso.rst M
> doc/cli/drpcli\_alerts\_wait.rst M doc/cli/drpcli\_autocomplete.rst M
> doc/cli/drpcli\_blueprints.rst M
> doc/cli/drpcli\_blueprints\_action.rst M
> doc/cli/drpcli\_blueprints\_actions.rst M
> doc/cli/drpcli\_blueprints\_add.rst M
> doc/cli/drpcli\_blueprints\_addprofile.rst M
> doc/cli/drpcli\_blueprints\_addtask.rst M
> doc/cli/drpcli\_blueprints\_await.rst M
> doc/cli/drpcli\_blueprints\_count.rst M
> doc/cli/drpcli\_blueprints\_create.rst M
> doc/cli/drpcli\_blueprints\_destroy.rst M
> doc/cli/drpcli\_blueprints\_etag.rst M
> doc/cli/drpcli\_blueprints\_exists.rst M
> doc/cli/drpcli\_blueprints\_get.rst M
> doc/cli/drpcli\_blueprints\_indexes.rst M
> doc/cli/drpcli\_blueprints\_list.rst M
> doc/cli/drpcli\_blueprints\_meta.rst M
> doc/cli/drpcli\_blueprints\_meta\_add.rst M
> doc/cli/drpcli\_blueprints\_meta\_get.rst M
> doc/cli/drpcli\_blueprints\_meta\_remove.rst M
> doc/cli/drpcli\_blueprints\_meta\_set.rst M
> doc/cli/drpcli\_blueprints\_params.rst M
> doc/cli/drpcli\_blueprints\_patch.rst M
> doc/cli/drpcli\_blueprints\_remove.rst M
> doc/cli/drpcli\_blueprints\_removeprofile.rst M
> doc/cli/drpcli\_blueprints\_removetask.rst M
> doc/cli/drpcli\_blueprints\_runaction.rst M
> doc/cli/drpcli\_blueprints\_set.rst M
> doc/cli/drpcli\_blueprints\_show.rst M
> doc/cli/drpcli\_blueprints\_update.rst M
> doc/cli/drpcli\_blueprints\_uploadiso.rst M
> doc/cli/drpcli\_blueprints\_wait.rst M doc/cli/drpcli\_bootenvs.rst M
> doc/cli/drpcli\_bootenvs\_action.rst M
> doc/cli/drpcli\_bootenvs\_actions.rst M
> doc/cli/drpcli\_bootenvs\_await.rst M
> doc/cli/drpcli\_bootenvs\_count.rst M
> doc/cli/drpcli\_bootenvs\_create.rst M
> doc/cli/drpcli\_bootenvs\_destroy.rst M
> doc/cli/drpcli\_bootenvs\_etag.rst M
> doc/cli/drpcli\_bootenvs\_exists.rst M
> doc/cli/drpcli\_bootenvs\_fromAppleNBI.rst M
> doc/cli/drpcli\_bootenvs\_indexes.rst M
> doc/cli/drpcli\_bootenvs\_install.rst M
> doc/cli/drpcli\_bootenvs\_list.rst M
> doc/cli/drpcli\_bootenvs\_meta.rst M
> doc/cli/drpcli\_bootenvs\_meta\_add.rst M
> doc/cli/drpcli\_bootenvs\_meta\_get.rst M
> doc/cli/drpcli\_bootenvs\_meta\_remove.rst M
> doc/cli/drpcli\_bootenvs\_meta\_set.rst M
> doc/cli/drpcli\_bootenvs\_patch.rst M
> doc/cli/drpcli\_bootenvs\_purgeLocalInstall.rst M
> doc/cli/drpcli\_bootenvs\_runaction.rst M
> doc/cli/drpcli\_bootenvs\_show.rst M
> doc/cli/drpcli\_bootenvs\_update.rst M
> doc/cli/drpcli\_bootenvs\_uploadiso.rst M
> doc/cli/drpcli\_bootenvs\_wait.rst M doc/cli/drpcli\_catalog.rst M
> doc/cli/drpcli\_catalog\_copyLocal.rst M
> doc/cli/drpcli\_catalog\_create.rst M
> doc/cli/drpcli\_catalog\_item.rst M
> doc/cli/drpcli\_catalog\_item\_detail.rst M
> doc/cli/drpcli\_catalog\_item\_download.rst M
> doc/cli/drpcli\_catalog\_item\_install.rst M
> doc/cli/drpcli\_catalog\_item\_show.rst M
> doc/cli/drpcli\_catalog\_items.rst M
> doc/cli/drpcli\_catalog\_items\_await.rst M
> doc/cli/drpcli\_catalog\_items\_count.rst M
> doc/cli/drpcli\_catalog\_items\_create.rst M
> doc/cli/drpcli\_catalog\_items\_destroy.rst M
> doc/cli/drpcli\_catalog\_items\_etag.rst M
> doc/cli/drpcli\_catalog\_items\_exists.rst M
> doc/cli/drpcli\_catalog\_items\_indexes.rst M
> doc/cli/drpcli\_catalog\_items\_list.rst M
> doc/cli/drpcli\_catalog\_items\_patch.rst M
> doc/cli/drpcli\_catalog\_items\_show.rst M
> doc/cli/drpcli\_catalog\_items\_update.rst M
> doc/cli/drpcli\_catalog\_items\_wait.rst M
> doc/cli/drpcli\_catalog\_show.rst M
> doc/cli/drpcli\_catalog\_updateLocal.rst M doc/cli/drpcli\_certs.rst M
> doc/cli/drpcli\_certs\_csr.rst M doc/cli/drpcli\_clusters.rst M
> doc/cli/drpcli\_clusters\_action.rst M
> doc/cli/drpcli\_clusters\_actions.rst M
> doc/cli/drpcli\_clusters\_add.rst M
> doc/cli/drpcli\_clusters\_addprofile.rst M
> doc/cli/drpcli\_clusters\_addtask.rst M
> doc/cli/drpcli\_clusters\_await.rst M
> doc/cli/drpcli\_clusters\_bootenv.rst M
> doc/cli/drpcli\_clusters\_cleanup.rst M
> doc/cli/drpcli\_clusters\_count.rst M
> doc/cli/drpcli\_clusters\_create.rst M
> doc/cli/drpcli\_clusters\_currentlog.rst M
> doc/cli/drpcli\_clusters\_deletejobs.rst M
> doc/cli/drpcli\_clusters\_destroy.rst M
> doc/cli/drpcli\_clusters\_etag.rst M
> doc/cli/drpcli\_clusters\_exists.rst M
> doc/cli/drpcli\_clusters\_get.rst M
> doc/cli/drpcli\_clusters\_group.rst M
> doc/cli/drpcli\_clusters\_group\_add.rst M
> doc/cli/drpcli\_clusters\_group\_addprofile.rst M
> doc/cli/drpcli\_clusters\_group\_get.rst M
> doc/cli/drpcli\_clusters\_group\_params.rst M
> doc/cli/drpcli\_clusters\_group\_remove.rst M
> doc/cli/drpcli\_clusters\_group\_removeprofile.rst M
> doc/cli/drpcli\_clusters\_group\_set.rst M
> doc/cli/drpcli\_clusters\_group\_uploadiso.rst M
> doc/cli/drpcli\_clusters\_indexes.rst M
> doc/cli/drpcli\_clusters\_inserttask.rst M
> doc/cli/drpcli\_clusters\_inspect.rst M
> doc/cli/drpcli\_clusters\_inspect\_jobs.rst M
> doc/cli/drpcli\_clusters\_inspect\_tasks.rst M
> doc/cli/drpcli\_clusters\_jobs.rst M
> doc/cli/drpcli\_clusters\_jobs\_create.rst M
> doc/cli/drpcli\_clusters\_jobs\_current.rst M
> doc/cli/drpcli\_clusters\_jobs\_state.rst M
> doc/cli/drpcli\_clusters\_list.rst M
> doc/cli/drpcli\_clusters\_meta.rst M
> doc/cli/drpcli\_clusters\_meta\_add.rst M
> doc/cli/drpcli\_clusters\_meta\_get.rst M
> doc/cli/drpcli\_clusters\_meta\_remove.rst M
> doc/cli/drpcli\_clusters\_meta\_set.rst M
> doc/cli/drpcli\_clusters\_params.rst M
> doc/cli/drpcli\_clusters\_patch.rst M
> doc/cli/drpcli\_clusters\_pause.rst M
> doc/cli/drpcli\_clusters\_processjobs.rst M
> doc/cli/drpcli\_clusters\_releaseToPool.rst M
> doc/cli/drpcli\_clusters\_remove.rst M
> doc/cli/drpcli\_clusters\_removeprofile.rst M
> doc/cli/drpcli\_clusters\_removetask.rst M
> doc/cli/drpcli\_clusters\_run.rst M
> doc/cli/drpcli\_clusters\_runaction.rst M
> doc/cli/drpcli\_clusters\_set.rst M doc/cli/drpcli\_clusters\_show.rst
> M doc/cli/drpcli\_clusters\_stage.rst M
> doc/cli/drpcli\_clusters\_start.rst M
> doc/cli/drpcli\_clusters\_tasks.rst M
> doc/cli/drpcli\_clusters\_tasks\_add.rst M
> doc/cli/drpcli\_clusters\_tasks\_del.rst M
> doc/cli/drpcli\_clusters\_update.rst M
> doc/cli/drpcli\_clusters\_uploadiso.rst M
> doc/cli/drpcli\_clusters\_wait.rst M
> doc/cli/drpcli\_clusters\_whoami.rst M
> doc/cli/drpcli\_clusters\_work\_order.rst M
> doc/cli/drpcli\_clusters\_work\_order\_add.rst M
> doc/cli/drpcli\_clusters\_work\_order\_off.rst M
> doc/cli/drpcli\_clusters\_work\_order\_on.rst M
> doc/cli/drpcli\_clusters\_workflow.rst M
> doc/cli/drpcli\_completion.rst M doc/cli/drpcli\_contents.rst M
> doc/cli/drpcli\_contents\_bundle.rst M
> doc/cli/drpcli\_contents\_bundlize.rst M
> doc/cli/drpcli\_contents\_convert.rst M
> doc/cli/drpcli\_contents\_create.rst M
> doc/cli/drpcli\_contents\_destroy.rst M
> doc/cli/drpcli\_contents\_document.rst M
> doc/cli/drpcli\_contents\_exists.rst M
> doc/cli/drpcli\_contents\_list.rst M
> doc/cli/drpcli\_contents\_show.rst M
> doc/cli/drpcli\_contents\_unbundle.rst M
> doc/cli/drpcli\_contents\_update.rst M
> doc/cli/drpcli\_contents\_upload.rst M doc/cli/drpcli\_contexts.rst M
> doc/cli/drpcli\_contexts\_await.rst M
> doc/cli/drpcli\_contexts\_count.rst M
> doc/cli/drpcli\_contexts\_create.rst M
> doc/cli/drpcli\_contexts\_destroy.rst M
> doc/cli/drpcli\_contexts\_etag.rst M
> doc/cli/drpcli\_contexts\_exists.rst M
> doc/cli/drpcli\_contexts\_indexes.rst M
> doc/cli/drpcli\_contexts\_list.rst M
> doc/cli/drpcli\_contexts\_meta.rst M
> doc/cli/drpcli\_contexts\_meta\_add.rst M
> doc/cli/drpcli\_contexts\_meta\_get.rst M
> doc/cli/drpcli\_contexts\_meta\_remove.rst M
> doc/cli/drpcli\_contexts\_meta\_set.rst M
> doc/cli/drpcli\_contexts\_patch.rst M
> doc/cli/drpcli\_contexts\_show.rst M
> doc/cli/drpcli\_contexts\_update.rst M
> doc/cli/drpcli\_contexts\_wait.rst M doc/cli/drpcli\_debug.rst M
> doc/cli/drpcli\_endpoints.rst M doc/cli/drpcli\_endpoints\_action.rst
> M doc/cli/drpcli\_endpoints\_actions.rst M
> doc/cli/drpcli\_endpoints\_add.rst M
> doc/cli/drpcli\_endpoints\_await.rst M
> doc/cli/drpcli\_endpoints\_count.rst M
> doc/cli/drpcli\_endpoints\_create.rst M
> doc/cli/drpcli\_endpoints\_destroy.rst M
> doc/cli/drpcli\_endpoints\_etag.rst M
> doc/cli/drpcli\_endpoints\_exists.rst M
> doc/cli/drpcli\_endpoints\_get.rst M
> doc/cli/drpcli\_endpoints\_indexes.rst M
> doc/cli/drpcli\_endpoints\_list.rst M
> doc/cli/drpcli\_endpoints\_meta.rst M
> doc/cli/drpcli\_endpoints\_meta\_add.rst M
> doc/cli/drpcli\_endpoints\_meta\_get.rst M
> doc/cli/drpcli\_endpoints\_meta\_remove.rst M
> doc/cli/drpcli\_endpoints\_meta\_set.rst M
> doc/cli/drpcli\_endpoints\_params.rst M
> doc/cli/drpcli\_endpoints\_patch.rst M
> doc/cli/drpcli\_endpoints\_remove.rst M
> doc/cli/drpcli\_endpoints\_runaction.rst M
> doc/cli/drpcli\_endpoints\_set.rst M
> doc/cli/drpcli\_endpoints\_show.rst M
> doc/cli/drpcli\_endpoints\_update.rst M
> doc/cli/drpcli\_endpoints\_uploadiso.rst M
> doc/cli/drpcli\_endpoints\_wait.rst M doc/cli/drpcli\_events.rst M
> doc/cli/drpcli\_events\_post.rst M doc/cli/drpcli\_events\_watch.rst M
> doc/cli/drpcli\_extended.rst M doc/cli/drpcli\_extended\_action.rst M
> doc/cli/drpcli\_extended\_actions.rst M
> doc/cli/drpcli\_extended\_add.rst M
> doc/cli/drpcli\_extended\_await.rst M
> doc/cli/drpcli\_extended\_count.rst M
> doc/cli/drpcli\_extended\_create.rst M
> doc/cli/drpcli\_extended\_destroy.rst M
> doc/cli/drpcli\_extended\_etag.rst M
> doc/cli/drpcli\_extended\_exists.rst M
> doc/cli/drpcli\_extended\_get.rst M
> doc/cli/drpcli\_extended\_indexes.rst M
> doc/cli/drpcli\_extended\_list.rst M
> doc/cli/drpcli\_extended\_meta.rst M
> doc/cli/drpcli\_extended\_meta\_add.rst M
> doc/cli/drpcli\_extended\_meta\_get.rst M
> doc/cli/drpcli\_extended\_meta\_remove.rst M
> doc/cli/drpcli\_extended\_meta\_set.rst M
> doc/cli/drpcli\_extended\_params.rst M
> doc/cli/drpcli\_extended\_patch.rst M
> doc/cli/drpcli\_extended\_remove.rst M
> doc/cli/drpcli\_extended\_runaction.rst M
> doc/cli/drpcli\_extended\_set.rst M doc/cli/drpcli\_extended\_show.rst
> M doc/cli/drpcli\_extended\_update.rst M
> doc/cli/drpcli\_extended\_uploadiso.rst M
> doc/cli/drpcli\_extended\_wait.rst M doc/cli/drpcli\_files.rst M
> doc/cli/drpcli\_files\_certs.rst M
> doc/cli/drpcli\_files\_certs\_get.rst M
> doc/cli/drpcli\_files\_certs\_set.rst M
> doc/cli/drpcli\_files\_destroy.rst M
> doc/cli/drpcli\_files\_download.rst M
> doc/cli/drpcli\_files\_exists.rst M doc/cli/drpcli\_files\_list.rst M
> doc/cli/drpcli\_files\_upload.rst M doc/cli/drpcli\_fingerprint.rst M
> doc/cli/drpcli\_gohai.rst M doc/cli/drpcli\_identity\_providers.rst M
> doc/cli/drpcli\_identity\_providers\_action.rst M
> doc/cli/drpcli\_identity\_providers\_actions.rst M
> doc/cli/drpcli\_identity\_providers\_await.rst M
> doc/cli/drpcli\_identity\_providers\_count.rst M
> doc/cli/drpcli\_identity\_providers\_create.rst M
> doc/cli/drpcli\_identity\_providers\_destroy.rst M
> doc/cli/drpcli\_identity\_providers\_etag.rst M
> doc/cli/drpcli\_identity\_providers\_exists.rst M
> doc/cli/drpcli\_identity\_providers\_indexes.rst M
> doc/cli/drpcli\_identity\_providers\_list.rst M
> doc/cli/drpcli\_identity\_providers\_meta.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_add.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_get.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_identity\_providers\_meta\_set.rst M
> doc/cli/drpcli\_identity\_providers\_patch.rst M
> doc/cli/drpcli\_identity\_providers\_runaction.rst M
> doc/cli/drpcli\_identity\_providers\_show.rst M
> doc/cli/drpcli\_identity\_providers\_update.rst M
> doc/cli/drpcli\_identity\_providers\_wait.rst M
> doc/cli/drpcli\_info.rst M doc/cli/drpcli\_info\_check.rst M
> doc/cli/drpcli\_info\_get.rst M doc/cli/drpcli\_info\_status.rst M
> doc/cli/drpcli\_interfaces.rst M doc/cli/drpcli\_interfaces\_await.rst
> M doc/cli/drpcli\_interfaces\_count.rst M
> doc/cli/drpcli\_interfaces\_etag.rst M
> doc/cli/drpcli\_interfaces\_exists.rst M
> doc/cli/drpcli\_interfaces\_indexes.rst M
> doc/cli/drpcli\_interfaces\_list.rst M
> doc/cli/drpcli\_interfaces\_show.rst M
> doc/cli/drpcli\_interfaces\_wait.rst M doc/cli/drpcli\_isos.rst M
> doc/cli/drpcli\_isos\_certs.rst M doc/cli/drpcli\_isos\_certs\_get.rst
> M doc/cli/drpcli\_isos\_certs\_set.rst M
> doc/cli/drpcli\_isos\_destroy.rst M doc/cli/drpcli\_isos\_download.rst
> M doc/cli/drpcli\_isos\_exists.rst M doc/cli/drpcli\_isos\_list.rst M
> doc/cli/drpcli\_isos\_upload.rst M doc/cli/drpcli\_jobs.rst M
> doc/cli/drpcli\_jobs\_actions.rst M doc/cli/drpcli\_jobs\_await.rst M
> doc/cli/drpcli\_jobs\_count.rst M doc/cli/drpcli\_jobs\_create.rst M
> doc/cli/drpcli\_jobs\_destroy.rst M doc/cli/drpcli\_jobs\_etag.rst M
> doc/cli/drpcli\_jobs\_exists.rst M doc/cli/drpcli\_jobs\_indexes.rst M
> doc/cli/drpcli\_jobs\_list.rst M doc/cli/drpcli\_jobs\_log.rst M
> doc/cli/drpcli\_jobs\_meta.rst M doc/cli/drpcli\_jobs\_meta\_add.rst M
> doc/cli/drpcli\_jobs\_meta\_get.rst M
> doc/cli/drpcli\_jobs\_meta\_remove.rst M
> doc/cli/drpcli\_jobs\_meta\_set.rst M doc/cli/drpcli\_jobs\_patch.rst
> M doc/cli/drpcli\_jobs\_plugin\_action.rst M
> doc/cli/drpcli\_jobs\_plugin\_actions.rst M
> doc/cli/drpcli\_jobs\_purge.rst M
> doc/cli/drpcli\_jobs\_runplugin\_action.rst M
> doc/cli/drpcli\_jobs\_show.rst M doc/cli/drpcli\_jobs\_unpack.rst M
> doc/cli/drpcli\_jobs\_update.rst M doc/cli/drpcli\_jobs\_wait.rst M
> doc/cli/drpcli\_labs.rst M doc/cli/drpcli\_labs\_document.rst M
> doc/cli/drpcli\_labs\_validate.rst M doc/cli/drpcli\_leases.rst M
> doc/cli/drpcli\_leases\_action.rst M
> doc/cli/drpcli\_leases\_actions.rst M
> doc/cli/drpcli\_leases\_await.rst M doc/cli/drpcli\_leases\_count.rst
> M doc/cli/drpcli\_leases\_destroy.rst M
> doc/cli/drpcli\_leases\_etag.rst M doc/cli/drpcli\_leases\_exists.rst
> M doc/cli/drpcli\_leases\_indexes.rst M
> doc/cli/drpcli\_leases\_list.rst M doc/cli/drpcli\_leases\_meta.rst M
> doc/cli/drpcli\_leases\_meta\_add.rst M
> doc/cli/drpcli\_leases\_meta\_get.rst M
> doc/cli/drpcli\_leases\_meta\_remove.rst M
> doc/cli/drpcli\_leases\_meta\_set.rst M
> doc/cli/drpcli\_leases\_runaction.rst M
> doc/cli/drpcli\_leases\_show.rst M doc/cli/drpcli\_leases\_wait.rst M
> doc/cli/drpcli\_logs.rst M doc/cli/drpcli\_logs\_get.rst M
> doc/cli/drpcli\_logs\_watch.rst M doc/cli/drpcli\_machines.rst M
> doc/cli/drpcli\_machines\_action.rst M
> doc/cli/drpcli\_machines\_actions.rst M
> doc/cli/drpcli\_machines\_add.rst M
> doc/cli/drpcli\_machines\_addprofile.rst M
> doc/cli/drpcli\_machines\_addtask.rst M
> doc/cli/drpcli\_machines\_await.rst M
> doc/cli/drpcli\_machines\_bootenv.rst M
> doc/cli/drpcli\_machines\_cleanup.rst M
> doc/cli/drpcli\_machines\_count.rst M
> doc/cli/drpcli\_machines\_create.rst M
> doc/cli/drpcli\_machines\_currentlog.rst M
> doc/cli/drpcli\_machines\_deletejobs.rst M
> doc/cli/drpcli\_machines\_deleteworkorders.rst M
> doc/cli/drpcli\_machines\_destroy.rst M
> doc/cli/drpcli\_machines\_etag.rst M
> doc/cli/drpcli\_machines\_exists.rst M
> doc/cli/drpcli\_machines\_get.rst M
> doc/cli/drpcli\_machines\_indexes.rst M
> doc/cli/drpcli\_machines\_inserttask.rst M
> doc/cli/drpcli\_machines\_inspect.rst M
> doc/cli/drpcli\_machines\_inspect\_jobs.rst M
> doc/cli/drpcli\_machines\_inspect\_tasks.rst M
> doc/cli/drpcli\_machines\_jobs.rst M
> doc/cli/drpcli\_machines\_jobs\_create.rst M
> doc/cli/drpcli\_machines\_jobs\_current.rst M
> doc/cli/drpcli\_machines\_jobs\_state.rst M
> doc/cli/drpcli\_machines\_list.rst M
> doc/cli/drpcli\_machines\_meta.rst M
> doc/cli/drpcli\_machines\_meta\_add.rst M
> doc/cli/drpcli\_machines\_meta\_get.rst M
> doc/cli/drpcli\_machines\_meta\_remove.rst M
> doc/cli/drpcli\_machines\_meta\_set.rst M
> doc/cli/drpcli\_machines\_migrate.rst M
> doc/cli/drpcli\_machines\_migrateTicket.rst M
> doc/cli/drpcli\_machines\_params.rst M
> doc/cli/drpcli\_machines\_patch.rst M
> doc/cli/drpcli\_machines\_pause.rst M
> doc/cli/drpcli\_machines\_processjobs.rst M
> doc/cli/drpcli\_machines\_releaseToPool.rst M
> doc/cli/drpcli\_machines\_remove.rst M
> doc/cli/drpcli\_machines\_removeprofile.rst M
> doc/cli/drpcli\_machines\_removetask.rst M
> doc/cli/drpcli\_machines\_run.rst M
> doc/cli/drpcli\_machines\_runaction.rst M
> doc/cli/drpcli\_machines\_set.rst M doc/cli/drpcli\_machines\_show.rst
> M doc/cli/drpcli\_machines\_stage.rst M
> doc/cli/drpcli\_machines\_start.rst M
> doc/cli/drpcli\_machines\_tasks.rst M
> doc/cli/drpcli\_machines\_tasks\_add.rst M
> doc/cli/drpcli\_machines\_tasks\_del.rst M
> doc/cli/drpcli\_machines\_update.rst M
> doc/cli/drpcli\_machines\_uploadiso.rst M
> doc/cli/drpcli\_machines\_wait.rst M
> doc/cli/drpcli\_machines\_whoami.rst M
> doc/cli/drpcli\_machines\_work\_order.rst M
> doc/cli/drpcli\_machines\_work\_order\_add.rst M
> doc/cli/drpcli\_machines\_work\_order\_off.rst M
> doc/cli/drpcli\_machines\_work\_order\_on.rst M
> doc/cli/drpcli\_machines\_workflow.rst M doc/cli/drpcli\_net.rst M
> doc/cli/drpcli\_net\_autogen.rst M doc/cli/drpcli\_net\_compile.rst M
> doc/cli/drpcli\_net\_generate.rst M doc/cli/drpcli\_net\_phys.rst M
> doc/cli/drpcli\_objects.rst M doc/cli/drpcli\_objects\_list.rst M
> doc/cli/drpcli\_params.rst M doc/cli/drpcli\_params\_await.rst M
> doc/cli/drpcli\_params\_count.rst M doc/cli/drpcli\_params\_create.rst
> M doc/cli/drpcli\_params\_destroy.rst M
> doc/cli/drpcli\_params\_etag.rst M doc/cli/drpcli\_params\_exists.rst
> M doc/cli/drpcli\_params\_indexes.rst M
> doc/cli/drpcli\_params\_list.rst M doc/cli/drpcli\_params\_meta.rst M
> doc/cli/drpcli\_params\_meta\_add.rst M
> doc/cli/drpcli\_params\_meta\_get.rst M
> doc/cli/drpcli\_params\_meta\_remove.rst M
> doc/cli/drpcli\_params\_meta\_set.rst M
> doc/cli/drpcli\_params\_patch.rst M doc/cli/drpcli\_params\_show.rst M
> doc/cli/drpcli\_params\_update.rst M doc/cli/drpcli\_params\_wait.rst
> M doc/cli/drpcli\_plugin\_providers.rst M
> doc/cli/drpcli\_plugin\_providers\_await.rst M
> doc/cli/drpcli\_plugin\_providers\_count.rst M
> doc/cli/drpcli\_plugin\_providers\_destroy.rst M
> doc/cli/drpcli\_plugin\_providers\_download.rst M
> doc/cli/drpcli\_plugin\_providers\_etag.rst M
> doc/cli/drpcli\_plugin\_providers\_exists.rst M
> doc/cli/drpcli\_plugin\_providers\_indexes.rst M
> doc/cli/drpcli\_plugin\_providers\_list.rst M
> doc/cli/drpcli\_plugin\_providers\_meta.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_add.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_get.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_plugin\_providers\_meta\_set.rst M
> doc/cli/drpcli\_plugin\_providers\_show.rst M
> doc/cli/drpcli\_plugin\_providers\_upload.rst M
> doc/cli/drpcli\_plugin\_providers\_wait.rst M
> doc/cli/drpcli\_plugins.rst M doc/cli/drpcli\_plugins\_action.rst M
> doc/cli/drpcli\_plugins\_actions.rst M
> doc/cli/drpcli\_plugins\_add.rst M doc/cli/drpcli\_plugins\_await.rst
> M doc/cli/drpcli\_plugins\_count.rst M
> doc/cli/drpcli\_plugins\_create.rst M
> doc/cli/drpcli\_plugins\_destroy.rst M
> doc/cli/drpcli\_plugins\_etag.rst M
> doc/cli/drpcli\_plugins\_exists.rst M doc/cli/drpcli\_plugins\_get.rst
> M doc/cli/drpcli\_plugins\_indexes.rst M
> doc/cli/drpcli\_plugins\_list.rst M doc/cli/drpcli\_plugins\_meta.rst
> M doc/cli/drpcli\_plugins\_meta\_add.rst M
> doc/cli/drpcli\_plugins\_meta\_get.rst M
> doc/cli/drpcli\_plugins\_meta\_remove.rst M
> doc/cli/drpcli\_plugins\_meta\_set.rst M
> doc/cli/drpcli\_plugins\_params.rst M
> doc/cli/drpcli\_plugins\_patch.rst M
> doc/cli/drpcli\_plugins\_remove.rst M
> doc/cli/drpcli\_plugins\_runaction.rst M
> doc/cli/drpcli\_plugins\_set.rst M doc/cli/drpcli\_plugins\_show.rst M
> doc/cli/drpcli\_plugins\_update.rst M
> doc/cli/drpcli\_plugins\_uploadiso.rst M
> doc/cli/drpcli\_plugins\_wait.rst M doc/cli/drpcli\_pools.rst M
> doc/cli/drpcli\_pools\_action.rst M doc/cli/drpcli\_pools\_actions.rst
> M doc/cli/drpcli\_pools\_active.rst M doc/cli/drpcli\_pools\_await.rst
> M doc/cli/drpcli\_pools\_count.rst M doc/cli/drpcli\_pools\_create.rst
> M doc/cli/drpcli\_pools\_destroy.rst M doc/cli/drpcli\_pools\_etag.rst
> M doc/cli/drpcli\_pools\_exists.rst M
> doc/cli/drpcli\_pools\_indexes.rst M doc/cli/drpcli\_pools\_list.rst M
> doc/cli/drpcli\_pools\_manage.rst M
> doc/cli/drpcli\_pools\_manage\_add.rst M
> doc/cli/drpcli\_pools\_manage\_allocate.rst M
> doc/cli/drpcli\_pools\_manage\_release.rst M
> doc/cli/drpcli\_pools\_manage\_remove.rst M
> doc/cli/drpcli\_pools\_patch.rst M
> doc/cli/drpcli\_pools\_runaction.rst M doc/cli/drpcli\_pools\_show.rst
> M doc/cli/drpcli\_pools\_status.rst M
> doc/cli/drpcli\_pools\_update.rst M doc/cli/drpcli\_pools\_wait.rst M
> doc/cli/drpcli\_preflight.rst M
> doc/cli/drpcli\_preflight\_checkports.rst M doc/cli/drpcli\_prefs.rst
> M doc/cli/drpcli\_prefs\_list.rst M doc/cli/drpcli\_prefs\_set.rst M
> doc/cli/drpcli\_profiles.rst M doc/cli/drpcli\_profiles\_action.rst M
> doc/cli/drpcli\_profiles\_actions.rst M
> doc/cli/drpcli\_profiles\_add.rst M
> doc/cli/drpcli\_profiles\_addprofile.rst M
> doc/cli/drpcli\_profiles\_await.rst M
> doc/cli/drpcli\_profiles\_count.rst M
> doc/cli/drpcli\_profiles\_create.rst M
> doc/cli/drpcli\_profiles\_destroy.rst M
> doc/cli/drpcli\_profiles\_etag.rst M
> doc/cli/drpcli\_profiles\_exists.rst M
> doc/cli/drpcli\_profiles\_genbootenv.rst M
> doc/cli/drpcli\_profiles\_get.rst M
> doc/cli/drpcli\_profiles\_indexes.rst M
> doc/cli/drpcli\_profiles\_list.rst M
> doc/cli/drpcli\_profiles\_meta.rst M
> doc/cli/drpcli\_profiles\_meta\_add.rst M
> doc/cli/drpcli\_profiles\_meta\_get.rst M
> doc/cli/drpcli\_profiles\_meta\_remove.rst M
> doc/cli/drpcli\_profiles\_meta\_set.rst M
> doc/cli/drpcli\_profiles\_params.rst M
> doc/cli/drpcli\_profiles\_patch.rst M
> doc/cli/drpcli\_profiles\_remove.rst M
> doc/cli/drpcli\_profiles\_removeprofile.rst M
> doc/cli/drpcli\_profiles\_runaction.rst M
> doc/cli/drpcli\_profiles\_set.rst M doc/cli/drpcli\_profiles\_show.rst
> M doc/cli/drpcli\_profiles\_update.rst M
> doc/cli/drpcli\_profiles\_uploadiso.rst M
> doc/cli/drpcli\_profiles\_wait.rst M doc/cli/drpcli\_proxy.rst M
> doc/cli/drpcli\_reservations.rst M
> doc/cli/drpcli\_reservations\_action.rst M
> doc/cli/drpcli\_reservations\_actions.rst M
> doc/cli/drpcli\_reservations\_await.rst M
> doc/cli/drpcli\_reservations\_count.rst M
> doc/cli/drpcli\_reservations\_create.rst M
> doc/cli/drpcli\_reservations\_destroy.rst M
> doc/cli/drpcli\_reservations\_etag.rst M
> doc/cli/drpcli\_reservations\_exists.rst M
> doc/cli/drpcli\_reservations\_indexes.rst M
> doc/cli/drpcli\_reservations\_list.rst M
> doc/cli/drpcli\_reservations\_meta.rst M
> doc/cli/drpcli\_reservations\_meta\_add.rst M
> doc/cli/drpcli\_reservations\_meta\_get.rst M
> doc/cli/drpcli\_reservations\_meta\_remove.rst M
> doc/cli/drpcli\_reservations\_meta\_set.rst M
> doc/cli/drpcli\_reservations\_patch.rst M
> doc/cli/drpcli\_reservations\_runaction.rst M
> doc/cli/drpcli\_reservations\_show.rst M
> doc/cli/drpcli\_reservations\_update.rst M
> doc/cli/drpcli\_reservations\_wait.rst M
> doc/cli/drpcli\_resource\_brokers.rst M
> doc/cli/drpcli\_resource\_brokers\_action.rst M
> doc/cli/drpcli\_resource\_brokers\_actions.rst M
> doc/cli/drpcli\_resource\_brokers\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_addprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_addtask.rst M
> doc/cli/drpcli\_resource\_brokers\_await.rst M
> doc/cli/drpcli\_resource\_brokers\_bootenv.rst M
> doc/cli/drpcli\_resource\_brokers\_cleanup.rst M
> doc/cli/drpcli\_resource\_brokers\_count.rst M
> doc/cli/drpcli\_resource\_brokers\_create.rst M
> doc/cli/drpcli\_resource\_brokers\_currentlog.rst M
> doc/cli/drpcli\_resource\_brokers\_deletejobs.rst M
> doc/cli/drpcli\_resource\_brokers\_destroy.rst M
> doc/cli/drpcli\_resource\_brokers\_etag.rst M
> doc/cli/drpcli\_resource\_brokers\_exists.rst M
> doc/cli/drpcli\_resource\_brokers\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_group.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_addprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_params.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_removeprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_group\_uploadiso.rst M
> doc/cli/drpcli\_resource\_brokers\_indexes.rst M
> doc/cli/drpcli\_resource\_brokers\_inserttask.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect\_jobs.rst M
> doc/cli/drpcli\_resource\_brokers\_inspect\_tasks.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_create.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_current.rst M
> doc/cli/drpcli\_resource\_brokers\_jobs\_state.rst M
> doc/cli/drpcli\_resource\_brokers\_list.rst M
> doc/cli/drpcli\_resource\_brokers\_meta.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_get.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_meta\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_params.rst M
> doc/cli/drpcli\_resource\_brokers\_patch.rst M
> doc/cli/drpcli\_resource\_brokers\_pause.rst M
> doc/cli/drpcli\_resource\_brokers\_processjobs.rst M
> doc/cli/drpcli\_resource\_brokers\_releaseToPool.rst M
> doc/cli/drpcli\_resource\_brokers\_remove.rst M
> doc/cli/drpcli\_resource\_brokers\_removeprofile.rst M
> doc/cli/drpcli\_resource\_brokers\_removetask.rst M
> doc/cli/drpcli\_resource\_brokers\_run.rst M
> doc/cli/drpcli\_resource\_brokers\_runaction.rst M
> doc/cli/drpcli\_resource\_brokers\_set.rst M
> doc/cli/drpcli\_resource\_brokers\_show.rst M
> doc/cli/drpcli\_resource\_brokers\_stage.rst M
> doc/cli/drpcli\_resource\_brokers\_start.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_tasks\_del.rst M
> doc/cli/drpcli\_resource\_brokers\_update.rst M
> doc/cli/drpcli\_resource\_brokers\_uploadiso.rst M
> doc/cli/drpcli\_resource\_brokers\_wait.rst M
> doc/cli/drpcli\_resource\_brokers\_whoami.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_add.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_off.rst M
> doc/cli/drpcli\_resource\_brokers\_work\_order\_on.rst M
> doc/cli/drpcli\_resource\_brokers\_workflow.rst M
> doc/cli/drpcli\_roles.rst M doc/cli/drpcli\_roles\_await.rst M
> doc/cli/drpcli\_roles\_count.rst M doc/cli/drpcli\_roles\_create.rst M
> doc/cli/drpcli\_roles\_destroy.rst M doc/cli/drpcli\_roles\_etag.rst M
> doc/cli/drpcli\_roles\_exists.rst M doc/cli/drpcli\_roles\_indexes.rst
> M doc/cli/drpcli\_roles\_list.rst M doc/cli/drpcli\_roles\_meta.rst M
> doc/cli/drpcli\_roles\_meta\_add.rst M
> doc/cli/drpcli\_roles\_meta\_get.rst M
> doc/cli/drpcli\_roles\_meta\_remove.rst M
> doc/cli/drpcli\_roles\_meta\_set.rst M
> doc/cli/drpcli\_roles\_patch.rst M doc/cli/drpcli\_roles\_show.rst M
> doc/cli/drpcli\_roles\_update.rst M doc/cli/drpcli\_roles\_wait.rst M
> doc/cli/drpcli\_stages.rst M doc/cli/drpcli\_stages\_action.rst M
> doc/cli/drpcli\_stages\_actions.rst M doc/cli/drpcli\_stages\_add.rst
> M doc/cli/drpcli\_stages\_addprofile.rst M
> doc/cli/drpcli\_stages\_addtask.rst M
> doc/cli/drpcli\_stages\_await.rst M
> doc/cli/drpcli\_stages\_bootenv.rst M
> doc/cli/drpcli\_stages\_count.rst M doc/cli/drpcli\_stages\_create.rst
> M doc/cli/drpcli\_stages\_destroy.rst M
> doc/cli/drpcli\_stages\_etag.rst M doc/cli/drpcli\_stages\_exists.rst
> M doc/cli/drpcli\_stages\_get.rst M
> doc/cli/drpcli\_stages\_indexes.rst M doc/cli/drpcli\_stages\_list.rst
> M doc/cli/drpcli\_stages\_meta.rst M
> doc/cli/drpcli\_stages\_meta\_add.rst M
> doc/cli/drpcli\_stages\_meta\_get.rst M
> doc/cli/drpcli\_stages\_meta\_remove.rst M
> doc/cli/drpcli\_stages\_meta\_set.rst M
> doc/cli/drpcli\_stages\_params.rst M doc/cli/drpcli\_stages\_patch.rst
> M doc/cli/drpcli\_stages\_remove.rst M
> doc/cli/drpcli\_stages\_removeprofile.rst M
> doc/cli/drpcli\_stages\_removetask.rst M
> doc/cli/drpcli\_stages\_runaction.rst M
> doc/cli/drpcli\_stages\_set.rst M doc/cli/drpcli\_stages\_show.rst M
> doc/cli/drpcli\_stages\_update.rst M
> doc/cli/drpcli\_stages\_uploadiso.rst M
> doc/cli/drpcli\_stages\_wait.rst M doc/cli/drpcli\_subnets.rst M
> doc/cli/drpcli\_subnets\_action.rst M
> doc/cli/drpcli\_subnets\_actions.rst M
> doc/cli/drpcli\_subnets\_await.rst M
> doc/cli/drpcli\_subnets\_count.rst M
> doc/cli/drpcli\_subnets\_create.rst M
> doc/cli/drpcli\_subnets\_destroy.rst M
> doc/cli/drpcli\_subnets\_etag.rst M
> doc/cli/drpcli\_subnets\_exists.rst M doc/cli/drpcli\_subnets\_get.rst
> M doc/cli/drpcli\_subnets\_indexes.rst M
> doc/cli/drpcli\_subnets\_leasetimes.rst M
> doc/cli/drpcli\_subnets\_list.rst M doc/cli/drpcli\_subnets\_meta.rst
> M doc/cli/drpcli\_subnets\_meta\_add.rst M
> doc/cli/drpcli\_subnets\_meta\_get.rst M
> doc/cli/drpcli\_subnets\_meta\_remove.rst M
> doc/cli/drpcli\_subnets\_meta\_set.rst M
> doc/cli/drpcli\_subnets\_nextserver.rst M
> doc/cli/drpcli\_subnets\_patch.rst M
> doc/cli/drpcli\_subnets\_pickers.rst M
> doc/cli/drpcli\_subnets\_range.rst M
> doc/cli/drpcli\_subnets\_runaction.rst M
> doc/cli/drpcli\_subnets\_set.rst M doc/cli/drpcli\_subnets\_show.rst M
> doc/cli/drpcli\_subnets\_update.rst M
> doc/cli/drpcli\_subnets\_wait.rst M doc/cli/drpcli\_support.rst M
> doc/cli/drpcli\_support\_bundle.rst M
> doc/cli/drpcli\_support\_machine-bundle.rst M
> doc/cli/drpcli\_system.rst M doc/cli/drpcli\_system\_action.rst M
> doc/cli/drpcli\_system\_actions.rst M
> doc/cli/drpcli\_system\_active.rst M doc/cli/drpcli\_system\_certs.rst
> M doc/cli/drpcli\_system\_certs\_get.rst M
> doc/cli/drpcli\_system\_certs\_set.rst M
> doc/cli/drpcli\_system\_ha.rst M
> doc/cli/drpcli\_system\_ha\_active.rst M
> doc/cli/drpcli\_system\_ha\_dump.rst M
> doc/cli/drpcli\_system\_ha\_enroll.rst M
> doc/cli/drpcli\_system\_ha\_failOverSafe.rst M
> doc/cli/drpcli\_system\_ha\_id.rst M
> doc/cli/drpcli\_system\_ha\_introduction.rst M
> doc/cli/drpcli\_system\_ha\_join.rst M
> doc/cli/drpcli\_system\_ha\_leader.rst M
> doc/cli/drpcli\_system\_ha\_peers.rst M
> doc/cli/drpcli\_system\_ha\_remove.rst M
> doc/cli/drpcli\_system\_ha\_showHa.rst M
> doc/cli/drpcli\_system\_ha\_state.rst M
> doc/cli/drpcli\_system\_passive.rst M
> doc/cli/drpcli\_system\_runaction.rst M
> doc/cli/drpcli\_system\_signurl.rst M
> doc/cli/drpcli\_system\_upgrade.rst M
> doc/cli/drpcli\_system\_upgrade\_remove.rst M
> doc/cli/drpcli\_system\_upgrade\_stage.rst M
> doc/cli/drpcli\_system\_upgrade\_start.rst M
> doc/cli/drpcli\_system\_upgrade\_status.rst M
> doc/cli/drpcli\_tasks.rst M doc/cli/drpcli\_tasks\_action.rst M
> doc/cli/drpcli\_tasks\_actions.rst M doc/cli/drpcli\_tasks\_await.rst
> M doc/cli/drpcli\_tasks\_count.rst M doc/cli/drpcli\_tasks\_create.rst
> M doc/cli/drpcli\_tasks\_destroy.rst M doc/cli/drpcli\_tasks\_etag.rst
> M doc/cli/drpcli\_tasks\_exists.rst M
> doc/cli/drpcli\_tasks\_indexes.rst M doc/cli/drpcli\_tasks\_list.rst M
> doc/cli/drpcli\_tasks\_meta.rst M doc/cli/drpcli\_tasks\_meta\_add.rst
> M doc/cli/drpcli\_tasks\_meta\_get.rst M
> doc/cli/drpcli\_tasks\_meta\_remove.rst M
> doc/cli/drpcli\_tasks\_meta\_set.rst M
> doc/cli/drpcli\_tasks\_patch.rst M
> doc/cli/drpcli\_tasks\_runaction.rst M doc/cli/drpcli\_tasks\_show.rst
> M doc/cli/drpcli\_tasks\_update.rst M doc/cli/drpcli\_tasks\_wait.rst
> M doc/cli/drpcli\_templates.rst M
> doc/cli/drpcli\_templates\_action.rst M
> doc/cli/drpcli\_templates\_actions.rst M
> doc/cli/drpcli\_templates\_await.rst M
> doc/cli/drpcli\_templates\_count.rst M
> doc/cli/drpcli\_templates\_create.rst M
> doc/cli/drpcli\_templates\_destroy.rst M
> doc/cli/drpcli\_templates\_etag.rst M
> doc/cli/drpcli\_templates\_exists.rst M
> doc/cli/drpcli\_templates\_indexes.rst M
> doc/cli/drpcli\_templates\_list.rst M
> doc/cli/drpcli\_templates\_meta.rst M
> doc/cli/drpcli\_templates\_meta\_add.rst M
> doc/cli/drpcli\_templates\_meta\_get.rst M
> doc/cli/drpcli\_templates\_meta\_remove.rst M
> doc/cli/drpcli\_templates\_meta\_set.rst M
> doc/cli/drpcli\_templates\_patch.rst M
> doc/cli/drpcli\_templates\_runaction.rst M
> doc/cli/drpcli\_templates\_show.rst M
> doc/cli/drpcli\_templates\_update.rst M
> doc/cli/drpcli\_templates\_upload.rst M
> doc/cli/drpcli\_templates\_wait.rst M doc/cli/drpcli\_tenants.rst M
> doc/cli/drpcli\_tenants\_await.rst M
> doc/cli/drpcli\_tenants\_count.rst M
> doc/cli/drpcli\_tenants\_create.rst M
> doc/cli/drpcli\_tenants\_destroy.rst M
> doc/cli/drpcli\_tenants\_etag.rst M
> doc/cli/drpcli\_tenants\_exists.rst M
> doc/cli/drpcli\_tenants\_indexes.rst M
> doc/cli/drpcli\_tenants\_list.rst M doc/cli/drpcli\_tenants\_meta.rst
> M doc/cli/drpcli\_tenants\_meta\_add.rst M
> doc/cli/drpcli\_tenants\_meta\_get.rst M
> doc/cli/drpcli\_tenants\_meta\_remove.rst M
> doc/cli/drpcli\_tenants\_meta\_set.rst M
> doc/cli/drpcli\_tenants\_patch.rst M doc/cli/drpcli\_tenants\_show.rst
> M doc/cli/drpcli\_tenants\_update.rst M
> doc/cli/drpcli\_tenants\_wait.rst M
> doc/cli/drpcli\_trigger\_providers.rst M
> doc/cli/drpcli\_trigger\_providers\_action.rst M
> doc/cli/drpcli\_trigger\_providers\_actions.rst M
> doc/cli/drpcli\_trigger\_providers\_add.rst M
> doc/cli/drpcli\_trigger\_providers\_addprofile.rst M
> doc/cli/drpcli\_trigger\_providers\_await.rst M
> doc/cli/drpcli\_trigger\_providers\_count.rst M
> doc/cli/drpcli\_trigger\_providers\_create.rst M
> doc/cli/drpcli\_trigger\_providers\_destroy.rst M
> doc/cli/drpcli\_trigger\_providers\_etag.rst M
> doc/cli/drpcli\_trigger\_providers\_exists.rst M
> doc/cli/drpcli\_trigger\_providers\_get.rst M
> doc/cli/drpcli\_trigger\_providers\_indexes.rst M
> doc/cli/drpcli\_trigger\_providers\_list.rst M
> doc/cli/drpcli\_trigger\_providers\_meta.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_add.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_get.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_remove.rst M
> doc/cli/drpcli\_trigger\_providers\_meta\_set.rst M
> doc/cli/drpcli\_trigger\_providers\_params.rst M
> doc/cli/drpcli\_trigger\_providers\_patch.rst M
> doc/cli/drpcli\_trigger\_providers\_remove.rst M
> doc/cli/drpcli\_trigger\_providers\_removeprofile.rst M
> doc/cli/drpcli\_trigger\_providers\_runaction.rst M
> doc/cli/drpcli\_trigger\_providers\_set.rst M
> doc/cli/drpcli\_trigger\_providers\_show.rst M
> doc/cli/drpcli\_trigger\_providers\_update.rst M
> doc/cli/drpcli\_trigger\_providers\_uploadiso.rst M
> doc/cli/drpcli\_trigger\_providers\_wait.rst M
> doc/cli/drpcli\_triggers.rst M doc/cli/drpcli\_triggers\_action.rst M
> doc/cli/drpcli\_triggers\_actions.rst M
> doc/cli/drpcli\_triggers\_add.rst M
> doc/cli/drpcli\_triggers\_addprofile.rst M
> doc/cli/drpcli\_triggers\_await.rst M
> doc/cli/drpcli\_triggers\_count.rst M
> doc/cli/drpcli\_triggers\_create.rst M
> doc/cli/drpcli\_triggers\_destroy.rst M
> doc/cli/drpcli\_triggers\_etag.rst M
> doc/cli/drpcli\_triggers\_exists.rst M
> doc/cli/drpcli\_triggers\_get.rst M
> doc/cli/drpcli\_triggers\_indexes.rst M
> doc/cli/drpcli\_triggers\_list.rst M
> doc/cli/drpcli\_triggers\_meta.rst M
> doc/cli/drpcli\_triggers\_meta\_add.rst M
> doc/cli/drpcli\_triggers\_meta\_get.rst M
> doc/cli/drpcli\_triggers\_meta\_remove.rst M
> doc/cli/drpcli\_triggers\_meta\_set.rst M
> doc/cli/drpcli\_triggers\_params.rst M
> doc/cli/drpcli\_triggers\_patch.rst M
> doc/cli/drpcli\_triggers\_remove.rst M
> doc/cli/drpcli\_triggers\_removeprofile.rst M
> doc/cli/drpcli\_triggers\_runaction.rst M
> doc/cli/drpcli\_triggers\_set.rst M doc/cli/drpcli\_triggers\_show.rst
> M doc/cli/drpcli\_triggers\_update.rst M
> doc/cli/drpcli\_triggers\_uploadiso.rst M
> doc/cli/drpcli\_triggers\_wait.rst M doc/cli/drpcli\_users.rst M
> doc/cli/drpcli\_users\_action.rst M doc/cli/drpcli\_users\_actions.rst
> M doc/cli/drpcli\_users\_await.rst M doc/cli/drpcli\_users\_count.rst
> M doc/cli/drpcli\_users\_create.rst M
> doc/cli/drpcli\_users\_destroy.rst M doc/cli/drpcli\_users\_etag.rst M
> doc/cli/drpcli\_users\_exists.rst M doc/cli/drpcli\_users\_indexes.rst
> M doc/cli/drpcli\_users\_list.rst M doc/cli/drpcli\_users\_meta.rst M
> doc/cli/drpcli\_users\_meta\_add.rst M
> doc/cli/drpcli\_users\_meta\_get.rst M
> doc/cli/drpcli\_users\_meta\_remove.rst M
> doc/cli/drpcli\_users\_meta\_set.rst M
> doc/cli/drpcli\_users\_password.rst M
> doc/cli/drpcli\_users\_passwordhash.rst M
> doc/cli/drpcli\_users\_patch.rst M
> doc/cli/drpcli\_users\_runaction.rst M doc/cli/drpcli\_users\_show.rst
> M doc/cli/drpcli\_users\_token.rst M doc/cli/drpcli\_users\_update.rst
> M doc/cli/drpcli\_users\_wait.rst M doc/cli/drpcli\_version.rst M
> doc/cli/drpcli\_version\_sets.rst M
> doc/cli/drpcli\_version\_sets\_action.rst M
> doc/cli/drpcli\_version\_sets\_actions.rst M
> doc/cli/drpcli\_version\_sets\_await.rst M
> doc/cli/drpcli\_version\_sets\_count.rst M
> doc/cli/drpcli\_version\_sets\_create.rst M
> doc/cli/drpcli\_version\_sets\_destroy.rst M
> doc/cli/drpcli\_version\_sets\_etag.rst M
> doc/cli/drpcli\_version\_sets\_exists.rst M
> doc/cli/drpcli\_version\_sets\_indexes.rst M
> doc/cli/drpcli\_version\_sets\_list.rst M
> doc/cli/drpcli\_version\_sets\_patch.rst M
> doc/cli/drpcli\_version\_sets\_runaction.rst M
> doc/cli/drpcli\_version\_sets\_show.rst M
> doc/cli/drpcli\_version\_sets\_update.rst M
> doc/cli/drpcli\_version\_sets\_wait.rst M
> doc/cli/drpcli\_work\_orders.rst M
> doc/cli/drpcli\_work\_orders\_action.rst M
> doc/cli/drpcli\_work\_orders\_actions.rst M
> doc/cli/drpcli\_work\_orders\_add.rst M
> doc/cli/drpcli\_work\_orders\_addprofile.rst M
> doc/cli/drpcli\_work\_orders\_addtask.rst M
> doc/cli/drpcli\_work\_orders\_await.rst M
> doc/cli/drpcli\_work\_orders\_count.rst M
> doc/cli/drpcli\_work\_orders\_create.rst M
> doc/cli/drpcli\_work\_orders\_destroy.rst M
> doc/cli/drpcli\_work\_orders\_etag.rst M
> doc/cli/drpcli\_work\_orders\_exists.rst M
> doc/cli/drpcli\_work\_orders\_get.rst M
> doc/cli/drpcli\_work\_orders\_indexes.rst M
> doc/cli/drpcli\_work\_orders\_inserttask.rst M
> doc/cli/drpcli\_work\_orders\_list.rst M
> doc/cli/drpcli\_work\_orders\_meta.rst M
> doc/cli/drpcli\_work\_orders\_meta\_add.rst M
> doc/cli/drpcli\_work\_orders\_meta\_get.rst M
> doc/cli/drpcli\_work\_orders\_meta\_remove.rst M
> doc/cli/drpcli\_work\_orders\_meta\_set.rst M
> doc/cli/drpcli\_work\_orders\_params.rst M
> doc/cli/drpcli\_work\_orders\_patch.rst M
> doc/cli/drpcli\_work\_orders\_purge.rst M
> doc/cli/drpcli\_work\_orders\_remove.rst M
> doc/cli/drpcli\_work\_orders\_removeprofile.rst M
> doc/cli/drpcli\_work\_orders\_removetask.rst M
> doc/cli/drpcli\_work\_orders\_run.rst M
> doc/cli/drpcli\_work\_orders\_runaction.rst M
> doc/cli/drpcli\_work\_orders\_set.rst M
> doc/cli/drpcli\_work\_orders\_show.rst M
> doc/cli/drpcli\_work\_orders\_tasks.rst M
> doc/cli/drpcli\_work\_orders\_tasks\_add.rst M
> doc/cli/drpcli\_work\_orders\_tasks\_del.rst M
> doc/cli/drpcli\_work\_orders\_update.rst M
> doc/cli/drpcli\_work\_orders\_uploadiso.rst M
> doc/cli/drpcli\_work\_orders\_wait.rst M doc/cli/drpcli\_workflows.rst
> M doc/cli/drpcli\_workflows\_action.rst M
> doc/cli/drpcli\_workflows\_actions.rst M
> doc/cli/drpcli\_workflows\_await.rst M
> doc/cli/drpcli\_workflows\_count.rst M
> doc/cli/drpcli\_workflows\_create.rst M
> doc/cli/drpcli\_workflows\_destroy.rst M
> doc/cli/drpcli\_workflows\_etag.rst M
> doc/cli/drpcli\_workflows\_exists.rst M
> doc/cli/drpcli\_workflows\_indexes.rst M
> doc/cli/drpcli\_workflows\_list.rst M
> doc/cli/drpcli\_workflows\_meta.rst M
> doc/cli/drpcli\_workflows\_meta\_add.rst M
> doc/cli/drpcli\_workflows\_meta\_get.rst M
> doc/cli/drpcli\_workflows\_meta\_remove.rst M
> doc/cli/drpcli\_workflows\_meta\_set.rst M
> doc/cli/drpcli\_workflows\_patch.rst M
> doc/cli/drpcli\_workflows\_runaction.rst M
> doc/cli/drpcli\_workflows\_show.rst M
> doc/cli/drpcli\_workflows\_update.rst M
> doc/cli/drpcli\_workflows\_wait.rst M doc/kb/kb-00010.rst M
> doc/operations/certs.rst
>
> commit 30a1bc9b12ee8da9fe33e9a5917d623b0aba63c3 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Fri Aug 12 14:32:43 2022 -0400
>
> > fix(catalog): more catalog fixes for airgap
>
> M api/catalog.go M cli/utils.go
>
> commit 862fc425bef4a6aca3bcfb509ef4eab97050ea52 Author: Tim Bosse
> \<<tim@rackn.com>\> Date: Thu Aug 11 21:55:36 2022 -0400
>
> > feat(model): add ol to bootenv familytype
> >
> > addresses rackn/product-backlog\#249
>
> M models/bootenv.go
>
> commit 00b5101019b0f44972ba223d4ff5ce8fde7c7192 Author: Zaheena
> \<<zaheena@gmail.com>\> Date: Thu Aug 11 01:28:12 2022 -0400
>
> > fix(catalog): some missing pieces for catalog airgap installs
>
> M api/catalog.go M api/utils.go M cli/utils.go M tools/install.sh
>
> commit 53b8f967c0b547c9272f3247a9c10e435e2b57ed Author: Rob Hirschfeld
> \<<rob@rackn.com>\> Date: Thu Aug 11 09:47:36 2022 -0500
>
> > doc(release): add EventToAudit plugin info
>
> M doc/rel\_notes/summaries/release\_v410.rst
>
> commit 0f506ff90e788a1430180e0f6add3a277e71d17f Author: Greg Althaus
> \<<galthaus@austin.rr.com>\> Date: Mon Aug 8 22:18:23 2022 -0500
>
> > doc: Fix typo
> >
> > Signed-off-by: Greg Althaus \<<galthaus@austin.rr.com>\>
>
> M doc/rel\_notes/summaries/release\_v410.rst
>
> End of Note

v4.0.0-pre2 {#rs_rel_notes_drp_v4.0.0-pre2}
===========

>     commit e0f0bdd0bc63cbfb456c60cbed875f243ffa4baa
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Wed Jul 31 10:47:38 2019 -0500
>
>         Fix up a few more expected unit test outputs, and pin cobra at the rev we were using in the unified repo
>
>     M   cli/template_test.go
>     M   cli/test-data/output/TestMachineCli/machines.runaction.3e7031fe-3062-45f1-835c-92541bc9cbd3.increment.576c47cb28f5c9217182dd4ccc70e8a2/stderr.expect
>     M   cli/test-data/output/TestMachineCli/machines.runaction.3e7031fe-3062-45f1-835c-92541bc9cbd3.increment.fred/stderr.expect
>     M   cli/test-data/output/TestPluginProviderCli/extended.cdd9213400fcc6a747e8a60884e044e6/stderr.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.dedc382c32100c7b987f2098da769fe4/stderr.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.john/stderr.expect
>     M   go.mod
>     M   go.sum
>
>     commit d7ac6f386d1777059449e9ee74b5f779113eb4a7
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 30 15:10:07 2019 -0500
>
>         Get CLI tests mostly working properly again
>
>     M   cli/contents_test.go
>     M   cli/fixInteractive.sh
>     M   cli/test-data/output/TestAgent/prefs.set.debugFrontend.debug/stdout.expect
>     M   cli/test-data/output/TestAgent/prefs.set.debugFrontend.warn/stdout.expect
>     M   cli/test-data/output/TestAuth/info.get/stdout.expect
>     M   cli/test-data/output/TestAuth/plugin_providers.list.611601b3efac342fd10027372140fe8c/stdout.expect
>     M   cli/test-data/output/TestAuth/plugin_providers.list.e8e0775e692adbcb8acdf3799178655c/stdout.expect
>     M   cli/test-data/output/TestAuth/templates.list.611601b3efac342fd10027372140fe8c/stdout.expect
>     M   cli/test-data/output/TestAuth/templates.list.e8e0775e692adbcb8acdf3799178655c/stdout.expect
>     M   cli/test-data/output/TestBootEnvCli/bootenvs.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestContentCli/contents.create.348f85563278f65434960e4c279ccb57/stderr.expect
>     M   cli/test-data/output/TestContentCli/contents.create.https/github.com/digitalrebar/provision-content/releases/download/v1.3.0/drp-community-content.yaml/stdout.expect
>     A   cli/test-data/output/TestContentCli/contents.create.https/github.com/digitalrebar/provision/v4-content/releases/download/v1.3.0/drp-community-content.yaml/stderr.expect
>     A   cli/test-data/output/TestContentCli/contents.destroy.drp-community-content/stderr.expect
>     M   cli/test-data/output/TestContentCli/contents.list.2/stdout.expect
>     M   cli/test-data/output/TestContentCli/contents.list.3/stdout.expect
>     M   cli/test-data/output/TestContentCli/contents.list.4/stdout.expect
>     M   cli/test-data/output/TestContentCli/contents.list.5/stdout.expect
>     M   cli/test-data/output/TestContentCli/contents.list/stdout.expect
>     M   cli/test-data/output/TestContentCli/contents.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestContentsFunctionalCli/contents.create.1f5703894e259aac1cceb0ff6196146b/stderr.expect
>     M   cli/test-data/output/TestContentsFunctionalCli/contents.create.caec0e7d1b4b772a78e6dece471da19b/stderr.expect
>     M   cli/test-data/output/TestContentsFunctionalCli/contents.list.2/stdout.expect
>     M   cli/test-data/output/TestContentsFunctionalCli/contents.list/stdout.expect
>     M   cli/test-data/output/TestContentsFunctionalCli/contents.update.Pack1.7e2795c1d3dd2c0dfc2149a79191333b/stderr.expect
>     M   cli/test-data/output/TestCorePieces/b3a8098dcd1bdc402b5f826708863989/stdout.expect
>     M   cli/test-data/output/TestEventsCli/events.post.dquotee1dquote/stderr.expect
>     M   cli/test-data/output/TestFilesCli/files.upload.common.go.as.greg/stdout.expect
>     M   cli/test-data/output/TestIsosCli/isos.upload.common.go.as.greg/stdout.expect
>     M   cli/test-data/output/TestJobCli/jobs.update.00000000-0000-0000-0000-000000000001.2613e56daa4b9dcd02c9e93badc4aa5b/stderr.expect
>     M   cli/test-data/output/TestLogsCli/logs.get/stdout.expect
>     M   cli/test-data/output/TestMachineCli/machines.update.3e7031fe-3062-45f1-835c-92541bc9cbd3.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestMachineCli/prefs.set.defaultStage.none/stdout.expect
>     M   cli/test-data/output/TestMachineCli/prefs.set.defaultStage.stage1/stdout.expect
>     M   cli/test-data/output/TestMultiArch/prefs.set.unknownBootEnv.ignore/stdout.expect
>     M   cli/test-data/output/TestMultiArch/prefs.set.unknownBootEnv.march-discover/stdout.expect
>     M   cli/test-data/output/TestParamCli/params.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestPluginCli/plugins.update.i-woman.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestPluginProviderCli/plugin_providers.list.3/stdout.expect
>     M   cli/test-data/output/TestPluginProviderCli/plugin_providers.list/stdout.expect
>     M   cli/test-data/output/TestPluginProviderCli/plugin_providers.show.incrementer/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.2/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.3/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.4/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.5/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.6/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.7/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list.8/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.list/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.99914b932bd37a50b983c5e7c90ae93b/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.bb8c0a0fb841ffaafc5e45ee1729be27/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.debugRenderer.1.debugDhcp.2.debugBootEnv.1/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.defaultBootEnv.local.debugDhcp.0.debugRenderer.0.debugBootEnv.0/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.defaultBootEnv.local3/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.knownTokenTimeout.5000/stdout.expect
>     M   cli/test-data/output/TestPrefsCli/prefs.set.unknownTokenTimeout.7000/stdout.expect
>     M   cli/test-data/output/TestProfileCli/profiles.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestStageCli/stages.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestSubnetCli/subnets.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestTaskCli/tasks.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestTemplateCli/templates.destroy.etc/stderr.expect
>     M   cli/test-data/output/TestTemplateCli/templates.destroy.usrshare/stderr.expect
>     A   cli/test-data/output/TestTemplateCli/templates.exists.etc/stderr.expect
>     A   cli/test-data/output/TestTemplateCli/templates.exists.usrshare/stderr.expect
>     M   cli/test-data/output/TestTemplateCli/templates.list.2/stdout.expect
>     M   cli/test-data/output/TestTemplateCli/templates.list.3/stdout.expect
>     M   cli/test-data/output/TestTemplateCli/templates.list.4/stdout.expect
>     M   cli/test-data/output/TestTemplateCli/templates.list.5/stdout.expect
>     M   cli/test-data/output/TestTemplateCli/templates.list/stdout.expect
>     M   cli/test-data/output/TestTemplateCli/templates.update.john.asdgasdg/stderr.expect
>     M   cli/test-data/output/TestUserCli/users.token.rocketskates.scope.all.ttl.330.action.list.specific.asdgag/stdout.expect
>     M   cli/test-data/output/TestUserCli/users.token.rocketskates/stdout.expect
>     M   cli/test-data/output/TestUserCli/users.update.john.asdgasdg/stderr.expect
>     A   cli/test-data/output/TestWorkflowAgent/tasks.create.2d2382802d884e02107e27fdcae586f6/stdout.expect
>     A   cli/test-data/output/TestWorkflowAgent/tasks.create.35352bc448d34d45dbe657c7a794989c/stdout.expect
>     A   cli/test-data/output/TestWorkflowAgent/tasks.create.3768f4afc0746f551fb27641a298058c/stdout.expect
>     A   cli/test-data/output/TestWorkflowAgent/tasks.create.cf5ae57e1cc4469ec455589990eb6608/stdout.expect
>     A   cli/test-data/output/TestWorkflowAgent/tasks.create.ecb69863dc62aeeebbb9e571e1a388b0/stdout.expect
>     M   cli/test-data/output/TestWorkflowCli/prefs.set.defaultWorkflow.wf3/stdout.expect
>     M   cli/test-data/output/TestWorkflowCli/prefs.set.defaultWorkflow/stdout.expect
>     M   cli/workflow_test.go
>     D   test-data/etc/dr-provision/templates/etc.yaml
>     D   test-data/usr/share/dr-provision/default.yaml
>     A   test/server.go
>     A   tools/testRe.go
>
>     commit 194fd51fd47ad34c877be322162e73c85df66b77
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 30 11:41:16 2019 -0500
>
>         Add basic testing to see if dr-provision is around and a sane version.
>
>     M   tools/test.sh
>
>     commit 2ef07bb0781f9059edcd5cac67cfc3c776d406c0
>     Author: Victor Lowther <victor.lowther@gmail.com>
>     Date:   Tue Jul 30 10:57:45 2019 -0500
>
>         Make the unit tests run again.
>
>         Still need to fix up the CLI unit tests, and add a sane interlock with 4.0 version of dr-provision
>
>     M   agent/00_agent_test.go
>     D   api/00_aone_test.go
>     M   api/bootenv_test.go
>     M   api/common_test.go
>     M   api/contents_test.go
>     M   api/files_test.go
>     M   api/info_test.go
>     A   api/server_test.go
>     M   api/test-data/fredhammer.yml
>     M   cli/00_aone_test.go
>     M   cli/common_test.go
>     M   go.mod
>     M   go.sum
>     A   tools/test.sh
>
>     End of Note

v3.11.0 {#rs_rel_notes_v3.11.0}
=======

    commit 3e1b6fa93802f0ac8440648d16d36f2713c60ff4
    Author: Rob Hirschfeld <rob@rackn.com>
    Date:   Mon Sep 10 15:45:30 2018 -0500

        only one KRIB docs, this page becomes install hint

    M doc/integrations/krib.rst

    commit fea6a98464de5149d9864aaf96ebb0e392944913
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Tue Aug 28 13:59:16 2018 -0500

        Fix unit tests.

    M cli/test-data/output/TestAuth/plugin_providers.list.611601b3efac342fd10027372140fe8c/stdout.expect
    M cli/test-data/output/TestAuth/plugin_providers.list.e8e0775e692adbcb8acdf3799178655c/stdout.expect
    M cli/test-data/output/TestPluginProviderCli/plugin_providers.list.3/stdout.expect
    M cli/test-data/output/TestPluginProviderCli/plugin_providers.list/stdout.expect
    M cli/test-data/output/TestPluginProviderCli/plugin_providers.show.incrementer/stdout.expect

    commit 482e14f11f970d819d2d202fefca98d1a7b39b3a
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Tue Aug 28 12:46:06 2018 -0500

        Fix the defaults for RequiredFeatures.

    M api/content.go

    commit 4c83535388c464f23cc6db5fd5789a8a3c14037f
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Tue Aug 28 11:13:15 2018 -0500

        Add requiredfeatures to the bundler command

    M api/content.go

    commit 5af3e560815dfaed58a1354101736c173144aa2e
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Mon Aug 27 16:16:50 2018 -0500

        Add sprig feature flag.

    M api/info_test.go
    M cli/test-data/output/TestAuth/info.get/stdout.expect
    M cli/test-data/output/TestUserCli/users.token.rocketskates.scope.all.ttl.330.action.list.specific.asdgag/stdout.expect
    M cli/test-data/output/TestUserCli/users.token.rocketskates/stdout.expect
    M models/info.go

    commit 01df87ff9a3034d213ae1588b6f3b3d21e54da2e
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Aug 27 12:20:31 2018 -0500

        Add oppourtunistic kexec support to the machine agent.

        This support will kick in in two places:

        * If the Agent detects that a machine is changing bootenvs, and the
          bootenv in question supplies a template at
          /machines/<machineUUID>/kexec on the static file server.

          The kexec template must be in the following format:
          kernel http://server/path/to/kernel
          initrd http://server/path/to/initrd
          params new kernel cmdline

          A BOOTIF= parameter will be appended to the params if one is present
          in the current kernel commandline or if the last-boot-macaddr param
          is set on the Machine directly.  Otherwise

        * If the user has loaded a kernel/initrd pair as part of a Task that
          exits with the Reboot status.  It is up to you to make sure the
          new kernel,initrd,and kernel commandline are populated appropriate
          for the new bootenv.

        If either of those conditions is true, the system can otherwise use
        kexec instead of reboot (there is support in the kernel, the kexec
        program exists, etc.), and the machine has the kexec-ok param set to
        true, then the Agent will attempt to use kexec before rebooting the
        system as per normal.

    M api/agent.go
    M api/client.go

    commit 554bd650ada8a76b872caabdd87e93fa41ee89c2
    Author: Rob Hirschfeld <rob@rackn.com>
    Date:   Fri Aug 24 09:57:26 2018 -0500

        update swagger with API note

    M doc/Swagger.rst

    commit c74a59146cc48fcebd91e870e6729923c9299c79
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Thu Aug 23 22:15:01 2018 -0500

        Attempt to fix docs

    M conf.py

    commit 9f0e5d42413b1696d5153dc44a8caf41b261e760
    Author: Shane Gibson <shane@rackn.com>
    Date:   Thu Aug 23 17:56:19 2018 -0700

        add licensing doc and images

    A doc/images/licensing/01-select-org.png
    A doc/images/licensing/02-info-prefs.png
    A doc/images/licensing/03-licensed-org.png
    A doc/images/licensing/04-spinning.png
    A doc/images/licensing/05-generated-license.png
    A doc/images/licensing/06-plugin-providers.png
    A doc/images/licensing/07-org-license-provider.png
    A doc/images/licensing/08-enabled-license-provider.png
    A doc/images/licensing/09-image-deploy-example.png
    A doc/images/licensing/10-installed-plugin-providers.png
    A doc/images/licensing/11-hamburger-menu.png
    A doc/images/licensing/12-select-licenses.png
    A doc/images/licensing/13-license-details.png
    A doc/rackn/license-no-images.rst
    A doc/rackn/license.rst

    commit 9fe2c8b1edc1678149374721c0791a2c242618b9
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Thu Aug 23 15:30:25 2018 -0500

        Add support in the backend for subnets consist of many point2point links.

        This adds a new picker named "point2point", that makes the address
        allocation strategy always create a lease that specifies a network
        config consisting of a /31 that contains the GIADDRas the router and
        the remote node and the other address in that subnet as the address of
        the system making the request.

    M cli/test-data/output/TestReservationCli/reservations.create.75df77d836ef173cf5c09b72568df958/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.create.b44b9b003ee789b023adbf62f74a4fde/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Addr=192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.NextServer=2.2.2.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Strategy=MAC/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Token=john/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.3/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.20886f942745e5752eae7b4e2914de1e/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.8b9440de63375c2f17131897d2ca34e9/stdout.expect
    M cli/test-data/output/TestScopedReservations/reservations.create.7ea7a3b0fc5e459770a133bddbbc5d1d/stdout.expect
    M cli/test-data/output/TestScopedReservations/reservations.create.8031586146ce7a1ab01ece85138f1ddd/stdout.expect
    M cli/test-data/output/TestScopedReservations/reservations.create.8ced2e751bfd5bbf3e14fac546b0eb20/stdout.expect
    M cli/test-data/output/TestScopedReservations/reservations.create.ca8e72d3b692f3319cf6afed15e389a7/stdout.expect
    M cli/test-data/output/TestScopedReservations/reservations.create.f52ec3cfb6f5f8341231456569234377/stdout.expect
    M cli/test-data/output/TestSubnetCli/reservations.create.0e916b75b4317a0ebba7d043e80e2e8a/stdout.expect
    M models/lease.go

    commit a49877bd5952bb2b6f2cc1232115892b39424447
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Thu Aug 23 11:02:56 2018 -0500

        Move option consolidation code into the backend

    M models/lease.go
    M models/reservation.go

    commit 4393d88189233b0728856c9bd000e89e00aea83c
    Author: Rob Hirschfeld <rob@rackn.com>
    Date:   Thu Aug 23 10:02:30 2018 -0500

        docs for sprig

    M doc/arch/provision.rst
    M doc/faq-troubleshooting.rst

    commit 3c92cf052bd352090f8cadfa6c0facfc95197b33
    Author: Shane Gibson <shane@rackn.com>
    Date:   Mon Aug 20 18:08:39 2018 -0700

        added FAQ for docker volume example

    M doc/faq-troubleshooting.rst

    commit 49ccce3ed0b483a3ba0435afb3d7e8d55a0398e7
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Wed Aug 15 17:51:18 2018 -0500

        Better error messages for displaying corrupt data, along with remedial actions

    M cli/test-data/output/TestContentsFunctionalCli/contents.create.caec0e7d1b4b772a78e6dece471da19b/stderr.expect
    M cli/test-data/output/TestContentsFunctionalCli/contents.update.Pack1.7e2795c1d3dd2c0dfc2149a79191333b/stderr.expect

    commit 20ebff9f19ada37d57179ca57647942621edb8e1
    Author: Shane Gibson <shane@rackn.com>
    Date:   Tue Aug 7 15:09:02 2018 -0700

        add env setup info and Packet IPMI setup

    M README.rst
    A doc/setup/index.rst
    A doc/setup/virtualbox.rst

    commit 9c4a486d66e171668ecc1becb767ac2b218d2dec
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Aug 6 16:51:29 2018 -0500

        Start threading Principal information through dr-provision.

        Principal is what we will use to determine who (or what) initiated any
        given action that might result in something being updated.

    M models/event.go

    commit 90c2be931d49381ed69a43b5c2cafa504ab47040
    Author: Shane Gibson <shane@rackn.com>
    Date:   Mon Aug 6 13:55:53 2018 -0700

        add example claim for RBAC usage

    M doc/faq-troubleshooting.rst

    commit 6988e030f050b71127a3b97673d4a669242b7974
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Aug 6 15:29:09 2018 -0500

        Fix up broken lease finding logic

    M models/lease.go

    commit 0b3ba0d09688dd8d53784e23fa0e6e901ffdec26
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Aug 6 13:13:43 2018 -0500

        Fix missing test condition

    M cli/reservation_test.go

    commit a28db77d7357ef4b3512d318528f30a6c02640b6
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Aug 6 12:02:38 2018 -0500

        More Reservation updates and tests.

        Reservations model has grown a Subnet field to allow scoped
        Reservations to keep track of what Subnet they are associated with,
        and the Create validation has been updated to force sane-ish scoping
        rules for when and where Reservations with identical Strategies and
        Tokens can be created.

    M cli/reservation_test.go
    M cli/test-data/output/TestReservationCli/reservations.create.75df77d836ef173cf5c09b72568df958/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.create.b44b9b003ee789b023adbf62f74a4fde/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Addr=192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.NextServer=2.2.2.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Strategy=MAC/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Token=john/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.3/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.20886f942745e5752eae7b4e2914de1e/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.8b9440de63375c2f17131897d2ca34e9/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.0d6273d1c6f4f51a5b449c8a2440e332/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.19ef2a16973c4155e911434a50ad67ee/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.5382d8ca61be54a41cefe0811fe3d3a0/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.5beabf8b4e86a15b1bb923f288e7a06d/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.7ea7a3b0fc5e459770a133bddbbc5d1d.2/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.7ea7a3b0fc5e459770a133bddbbc5d1d/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.8031586146ce7a1ab01ece85138f1ddd/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.8ced2e751bfd5bbf3e14fac546b0eb20.2/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.8ced2e751bfd5bbf3e14fac546b0eb20/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.ae856946e7e1ceec7f060adee2cebbe2/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.c8ba8a4409b87e0562de6eaa30e925a3/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.ca8e72d3b692f3319cf6afed15e389a7/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.dd8d3dbf08baa7731fa297dff678d284/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.de5784c613aad0dbf28123d78e13e17d/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.f52ec3cfb6f5f8341231456569234377/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.f7844c97c24b7b9680050cca05d4d99f/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.create.fb7519bb812936eec88ba419a3b70d38/stderr.expect
    A cli/test-data/output/TestScopedReservations/reservations.destroy.192.168.123.30/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.destroy.192.168.124.1/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.destroy.192.168.124.2/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.destroy.192.168.125.1/stdout.expect
    A cli/test-data/output/TestScopedReservations/reservations.destroy.192.168.125.2/stdout.expect
    A cli/test-data/output/TestScopedReservations/subnets.create.4a0a853566fdb17e413a51061685340c/stderr.expect
    A cli/test-data/output/TestScopedReservations/subnets.create.8c0d45bc3893b782036e1129d63bbcf3/stderr.expect
    A cli/test-data/output/TestScopedReservations/subnets.create.a0ba1eb874b7657e960de69fd7a6f21d/stdout.expect
    A cli/test-data/output/TestScopedReservations/subnets.create.e8c243de83383f47e9a5530d635c98b0/stdout.expect
    A cli/test-data/output/TestScopedReservations/subnets.destroy.aa/stdout.expect
    A cli/test-data/output/TestScopedReservations/subnets.destroy.bb/stdout.expect
    M cli/test-data/output/TestSubnetCli/reservations.create.0e916b75b4317a0ebba7d043e80e2e8a/stdout.expect
    M models/reservation.go

    commit 393ecd703bb8b5bd1882ff9846d1178055ec70e6
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Fri Aug 3 13:07:38 2018 -0500

        Start working on scoped reservations.

        This commit adds a new Scoped flag to Reservations, which signals that
        the Reservation so created is only valid within a Subnet that has a
        matching covered address range.  It will allow the DHCP system to
        properly handle the case where we have multiple Reservations in
        different Subnets for the same Token, which can happen when tagged
        VLAN interfaces are in play on the client.

        This pull request only implements the model and backend validation
        changes, along with an update to the Subnet logic to disallow changes
        to the Subnet field.  The changes to the DHCP system will come later.

    M cli/reservation_test.go
    M cli/subnet.go
    M cli/subnet_test.go
    D cli/test-data/output/TestAgent/machines.deletejobs.c9196b77-deef-4c8e-8130-299b3e3d9a10/stdout.expect
    D cli/test-data/output/TestAgent/tasks.create.657759c5a48b3113f2f1c89038bb1625/stdout.expect
    D cli/test-data/output/TestAgent/tasks.create.a16a61e946a854c986348f93042a8088/stdout.expect
    D cli/test-data/output/TestAgent/tasks.create.b4427852a84e0239d86856c9ab67c133/stdout.expect
    D cli/test-data/output/TestAgent/tasks.create.bc3fdca053f4a517cb087ad56b8eda9d/stdout.expect
    D cli/test-data/output/TestAgent/tasks.create.c818c2c5c82527f91d23a6e7c7ddba0b/stderr.expect
    D cli/test-data/output/TestAgent/tasks.create.cf5e9674b3bc92dae0c82a2e742672c0/stdout.expect
    D cli/test-data/output/TestAuth/contents.upload.23d0fbb5c1b84d63f9cacc197471d71f/stdout.expect
    D cli/test-data/output/TestAuth/contents.upload.5eedc68cb3abfe16ad51ca013300329f/stdout.expect
    D cli/test-data/output/TestAuth/contents.upload.7e3af6cb7b5f6f0b3188774696641aad/stdout.expect
    D cli/test-data/output/TestAuth/preferences.list.031aaa8a3aec0fc7853bf71ea233d293/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.035d870983efb3a7ec7051687e6fe6bb/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.0e07d3a262e5e0ef47b26f4082c5f7bf/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.1701a61176648c697025a2134647eda2/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.19bea7890032d85c84f1398c19dd3536/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.611601b3efac342fd10027372140fe8c/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.b0d2450128d6468280922204054b343b/stderr.expect
    D cli/test-data/output/TestAuth/preferences.list.e8e0775e692adbcb8acdf3799178655c/stderr.expect
    D cli/test-data/output/TestAuth/tenants.create.0c11979a2de5688cc70cef0d610844c9/stdout.expect
    D cli/test-data/output/TestAuth/tenants.create.4d63922d50b2b3f1eafb5aade6710078/stdout.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.exists.john.john2.2/stderr.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.install.bootenvs/no-fredhammer.yml/stderr.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.update.john.4860337096e3ea797e0b88bd5a1da9b0/stderr.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.update.john.b421c795e38520763d25a95e8308d866/stdout.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.update.john.ece8c455d4c2e7c856b7f3a3b926f1e2/stdout.expect
    D cli/test-data/output/TestBootEnvCli/bootenvs.update.john2.b421c795e38520763d25a95e8308d866/stderr.expect
    D cli/test-data/output/TestCorePieces/interfaces.indexes/stderr.expect
    D cli/test-data/output/TestCorePieces/plugin_providers.indexes/stderr.expect
    D cli/test-data/output/TestCorePieces/preferences.indexes/stderr.expect
    D cli/test-data/output/TestJobCli/jobs.create.301de1c48b1b5910dc879e1f545634a8.3/stderr.expect
    D cli/test-data/output/TestJobCli/jobs.create.301de1c48b1b5910dc879e1f545634a8.3/stdout.expect
    D cli/test-data/output/TestJobCli/machines.update.3e7031fe-3062-45f1-835c-92541bc9cbd3.15ef88524f82284ee914fdb15df5a1ef/stdout.expect
    D cli/test-data/output/TestJobCli/tasks.create.52a8e22e99b2a4ff8e5850b85b87f2a6/stdout.expect
    D cli/test-data/output/TestJobOsFilter/tasks.create.b70dcc895a6011f09806aeb19d57bfaa/stderr.expect
    D cli/test-data/output/TestMachineCli/machines.bootenv.3e7031fe-3062-45f1-835c-92541bc9cbd3.local/stdout.expect
    D cli/test-data/output/TestMachineCli/machines.update.3e7031fe-3062-45f1-835c-92541bc9cbd3.15ef88524f82284ee914fdb15df5a1ef.2/stdout.expect
    D cli/test-data/output/TestMachineCli/machines.workflow.3e7031fe-3062-45f1-835c-92541bc9cbd3.ea648e9b71ad49d10dc8ba3570189690/stdout.expect
    D cli/test-data/output/TestMachineCli/machines.workflow.3e7031fe-3062-45f1-835c-92541bc9cbd3.f65ef9dba7d9ad51439bdd1be31bc5ce/stdout.expect
    D cli/test-data/output/TestMachineCli/workflows.create.d1f0112b829ea17d07d4309e723ce41f/stdout.expect
    D cli/test-data/output/TestMachineTaskCli/machines.tasks.del.3e7031fe-3062-45f1-835c-92541bc9cbd3.task3.task1.task2/stdout.expect
    D cli/test-data/output/TestMachineTaskCli/machines.tasks.remove.3e7031fe-3062-45f1-835c-92541bc9cbd3.task2.task4/stdout.expect
    D cli/test-data/output/TestParamCli/params.create.5d3b5482b66452d7c05fc5bb434c6cd8/stderr.expect
    D cli/test-data/output/TestProcessJobsCli/machines.processjobs.3e7031fe-3062-45f1-835c-92541bc9cbd3/stdout.expect
    D cli/test-data/output/TestProcessJobsCli/machines.processjobs.p1.p2.p3/stderr.expect
    D cli/test-data/output/TestProcessJobsCli/machines.processjobs.p1/stderr.expect
    D cli/test-data/output/TestProcessJobsCli/machines.processjobs/stderr.expect
    D cli/test-data/output/TestProfileCli/profiles.list.3ce899dccbb3addf6c5c38dd86c65778/stderr.expect
    D cli/test-data/output/TestProfileCli/profiles.list.3ce899dccbb3addf6c5c38dd86c65778/stdout.expect
    D cli/test-data/output/TestProfileCli/profiles.params.john.24e4989b865f5f18dbf79ba0a4d55c8a/stderr.expect
    D cli/test-data/output/TestProfileCli/profiles.show.john.3ce899dccbb3addf6c5c38dd86c65778/stdout.expect
    D cli/test-data/output/TestProfileCli/profiles.show.john.5c0b4b6b2bb6c1a632cd0f71f2ed1a0b/stderr.expect
    D cli/test-data/output/TestRepos/params.create.fe8ed5802a2bcfa74572db8dfead6029/stdout.expect
    A cli/test-data/output/TestReservationCli/reservations.create.0a0fc32b39cfde87b8a88c7a67ecff73/stderr.expect
    M cli/test-data/output/TestReservationCli/reservations.create.75df77d836ef173cf5c09b72568df958/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.create.b44b9b003ee789b023adbf62f74a4fde/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Addr=192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.NextServer=2.2.2.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Strategy=MAC/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.list.Token=john/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.2/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.3/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100.4/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.show.192.168.100.100/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.20886f942745e5752eae7b4e2914de1e/stdout.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.8b9440de63375c2f17131897d2ca34e9/stdout.expect
    D cli/test-data/output/TestRoleCLI/contents.upload.5eedc68cb3abfe16ad51ca013300329f/stdout.expect
    D cli/test-data/output/TestRoleCLI/contents.upload.7e3af6cb7b5f6f0b3188774696641aad/stdout.expect
    D cli/test-data/output/TestSecureParams/50e06fe5d84846bfe53b825edfeca75f.2/stdout.expect
    D cli/test-data/output/TestSecureParams/8af26b735e1bf829ea9fdffbe744e238.2/stderr.expect
    D cli/test-data/output/TestSecureParams/8daa0bc34e6a3c6c56faed5c3e09d8eb/stdout.expect
    D cli/test-data/output/TestSecureParams/9049052520b9e6882122757b021a2d99/stdout.expect
    D cli/test-data/output/TestSecureParams/bfd5d9a437af6b61a45c4084e4bbfb72/stdout.expect
    D cli/test-data/output/TestSecureParams/c4ada73230f69d4a5a1abfbac29a22ee.2/stdout.expect
    D cli/test-data/output/TestSecureParams/contents.upload.5eedc68cb3abfe16ad51ca013300329f/stdout.expect
    D cli/test-data/output/TestSecureParams/contents.upload.7e3af6cb7b5f6f0b3188774696641aad/stdout.expect
    D cli/test-data/output/TestSecureParams/d0b6e160f87ab091c14a7c327415afa6.2/stderr.expect
    D cli/test-data/output/TestSecureParams/e0616db8b6db5bf8c679af43391b9785.2/stderr.expect
    D cli/test-data/output/TestSecureParams/f6976c6a7aa716b575f4dc84b19f55ba.2/stdout.expect
    D cli/test-data/output/TestSecureParams/machines.get.Name.bob.param.secure.0526e14e902277370e0dccbf146ef3e4.2/stderr.expect
    D cli/test-data/output/TestSecureParams/machines.get.Name.bob.param.secure.0526e14e902277370e0dccbf146ef3e4.2/stdout.expect
    D cli/test-data/output/TestSecureParams/machines.get.bob.param.secure/stderr.expect
    D cli/test-data/output/TestSecureParams/machines.set.Name.bob.param.secure.to.Fred/stderr.expect
    D cli/test-data/output/TestSecureParams/machines.set.Name.bob.param.secure.to.Fred/stdout.expect
    D cli/test-data/output/TestSecureParams/machines.set.bob.param.secure.to.Bob/stderr.expect
    D cli/test-data/output/TestSecureParams/params.create.e480c5e01cc38c3e42aa7ff9a3f9a060/stderr.expect
    D cli/test-data/output/TestSecureParams/params.destroy.secret/stderr.expect
    D cli/test-data/output/TestSecureParams/roles.create.4e4f9c6c69682f2ebab23dfba2588f62/stderr.expect
    D cli/test-data/output/TestSecureParams/roles.create.ba962fe53238f930551d0607532fc892/stderr.expect
    D cli/test-data/output/TestSecureParams/users.update.fred.0d0bf3b91240a3b25fceeae98ef37035/stdout.expect
    A cli/test-data/output/TestSubnetCli/reservations.create.0e916b75b4317a0ebba7d043e80e2e8a/stdout.expect
    A cli/test-data/output/TestSubnetCli/reservations.destroy.192.168.100.100/stdout.expect
    A cli/test-data/output/TestSubnetCli/subnets.destroy.john.3/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.destroy.john.3/stdout.expect
    A cli/test-data/output/TestSubnetCli/subnets.destroy.john.4/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.leasetimes.john.65.7300/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.nextserver.john.1.24.36.16/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.pickers.john.none,nextFree,mostExpired/stdout.expect
    R100  cli/test-data/output/TestAgent/machines.processjobs.c9196b77-deef-4c8e-8130-299b3.b98773fd65e6e32eee9e8a357b6a3e55/stderr.expect    cli/test-data/output/TestSubnetCli/subnets.range.john.192.168.100.10.192.168.100.200/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.range.john.192.168.100.10.192.168.100.200/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.set.john.option.6.to.66/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.set.john.option.6.to.67/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.set.john.option.6.to.null/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.show.john.2/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.show.john.3/stdout.expect
    D cli/test-data/output/TestSubnetCli/subnets.show.john.4/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.subnet.john.1111.11.2223.544/66666/stderr.expect
    A cli/test-data/output/TestSubnetCli/subnets.subnet.john.192.168.100.0/10/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.subnet.john.192.168.100.0/10/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets.subnet.john.june.1.24.36.16/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.subnet/stderr.expect
    D cli/test-data/output/TestSubnetCli/subnets.update.john.0d31ee68d06b9887be58c054b49e9155/stdout.expect
    A cli/test-data/output/TestSubnetCli/subnets.update.john.9bd683d4d27a328470940be5d534aa60/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.update.john.9bd683d4d27a328470940be5d534aa60/stdout.expect
    M cli/test-data/output/TestSubnetCli/subnets/stdout.expect
    D cli/test-data/output/TestSystemCli/plugin_providers.list/stdout.expect
    D cli/test-data/output/TestSystemCli/plugins.list/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.08f924bb7c4088eb2a2e2329c87ca63d/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.1b3fbca2dfacf8df445a16fc741e93d5/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.2aea274bc5deaf40c065acffa295ef54/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.321175492247bdf2e6837a239867cf25/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.406461b183c2bbac8ff84ffc1714ae94/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.6388af3b8af1dd4ab5844adafccd3c2f/stderr.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.dc8d833d36cfbee50576779bb107ca43/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.e667e159e812a708204da3173d8e969b/stdout.expect
    D cli/test-data/output/TestWorkflowAgent/tasks.create.fe0fc158e2050e83f5c69939b302c5fd/stdout.expect
    D cli/test-data/output/TestWorkflowCli/machines.update.Name.m0.0d866daa50b75c29e61b8004d6e171bc/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/machines.create.87a5a04e5df4d9bf26bd690053201bd7/stderr.expect
    D cli/test-data/output/TestWorkflowSwitch/machines.update.Name.m1.c92ed336dca8ab7c3f952d31214b5fb3/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/machines.update.Name.m3.c92ed336dca8ab7c3f952d31214b5fb3/stderr.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.18c0a90484e931d8642373cbc8841a34/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.5cf89fae0c2c2baece25123d7924e52d/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.5d07b76c62dae4fbc8a06495f3f730a5/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.9cb8fd6f85630194299b9471fe7e484d/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.stage1/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.stage2/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.stage3/stdout.expect
    D cli/test-data/output/TestWorkflowSwitch/stages.create.stage4/stdout.expect
    M models/reservation.go

    commit 5204fc53ec981b09f0544ac9b86d1835ca2a7e39
    Author: Greg Althaus <galthaus@austin.rr.com>
    Date:   Sat Aug 4 23:03:26 2018 -0600

        Add filebeat to the docs.

    M conf.py

    commit 695b8b3d4c5bcab8faf7b8c75ca87d1a973f05b0
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Tue Jul 31 16:00:59 2018 -0500

        Update glide.lock and unit tests

    M cli/test-data/output/TestBootEnvCli/bootenvs.install.bootenvs/fredhammer.yml.3/stderr.expect
    M cli/test-data/output/TestBootEnvCli/bootenvs.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestBootEnvCli/bootenvs/stdout.expect
    M cli/test-data/output/TestContentCli/contents.create.348f85563278f65434960e4c279ccb57/stderr.expect
    M cli/test-data/output/TestContentCli/contents.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestContentsFunctionalCli/contents.create.caec0e7d1b4b772a78e6dece471da19b/stderr.expect
    M cli/test-data/output/TestContentsFunctionalCli/contents.update.Pack1.7e2795c1d3dd2c0dfc2149a79191333b/stderr.expect
    M cli/test-data/output/TestEventsCli/events.post.dquotee1dquote/stderr.expect
    M cli/test-data/output/TestJobCli/jobs.update.00000000-0000-0000-0000-000000000001.2613e56daa4b9dcd02c9e93badc4aa5b/stderr.expect
    M cli/test-data/output/TestMachineCli/machines.runaction.3e7031fe-3062-45f1-835c-92541bc9cbd3.increment.576c47cb28f5c9217182dd4ccc70e8a2/stderr.expect
    M cli/test-data/output/TestMachineCli/machines.runaction.3e7031fe-3062-45f1-835c-92541bc9cbd3.increment.fred/stderr.expect
    M cli/test-data/output/TestMachineCli/machines.update.3e7031fe-3062-45f1-835c-92541bc9cbd3.asdgasdg/stderr.expect
    M cli/test-data/output/TestParamCli/params.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestPluginCli/plugins.update.i-woman.asdgasdg/stderr.expect
    M cli/test-data/output/TestPrefsCli/prefs.set.dedc382c32100c7b987f2098da769fe4/stderr.expect
    M cli/test-data/output/TestPrefsCli/prefs.set.john/stderr.expect
    M cli/test-data/output/TestProfileCli/profiles.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestReservationCli/reservations.update.192.168.100.100.asdgasdg/stderr.expect
    M cli/test-data/output/TestStageCli/stages.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestSubnetCli/subnets.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestTaskCli/tasks.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestTemplateCli/templates.update.john.asdgasdg/stderr.expect
    M cli/test-data/output/TestUserCli/users.update.john.asdgasdg/stderr.expect

    commit 207e7045e3583fff8fbd3429d13ab6e58946d743
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Tue Jul 31 15:29:40 2018 -0500

        Apple support netboots High Sierra via NetBoot and NetInstall methods.

    A cli/appleNBI.go
    M cli/bootenv.go
    M models/aaplbsdp.go

    commit ccaacc62f47334d8bd52539acea82a17b3bc619a
    Author: Victor Lowther <victor.lowther@gmail.com>
    Date:   Mon Jul 30 15:59:51 2018 -0500

        Start removal of hardwired chainload into iPXE apple boot.

    A models/aaplbsdp.go

v4.0.2 {#rs_rel_notes_drp_v4.0.2}
======

>     commit 5d38df30df56dca815ab233860ea59b3351416b2
>     Author: Greg Althaus <galthaus@austin.rr.com>
>     Date:   Thu Aug 8 10:57:34 2019 -0500
>
>         Update install.sh to work with the new v4 locations
>         and use the catalog to get the proper versions.
>
>         Create a deprated drbundler that can be installed
>         that will work but show deprecation.
>
>     A   tools/drbundler
>     M   tools/install.sh
>
>     End of Note
